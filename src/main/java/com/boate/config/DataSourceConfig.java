package com.boate.config;

import java.beans.PropertyVetoException;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mchange.v2.c3p0.ComboPooledDataSource;


@Configuration
public class DataSourceConfig {

	@Bean
	public DataSource dataSource() throws PropertyVetoException {
		ComboPooledDataSource cpds = new ComboPooledDataSource("BOATE_POOLING");
		cpds.setDriverClass( "org.postgresql.Driver" ); //loads the jdbc driver            
		cpds.setJdbcUrl( System.getenv("DATABASE_URL") );
		cpds.setUser( System.getenv("DATABASE_USERNAME") );                                  
		cpds.setPassword( System.getenv("DATABASE_PASSWORD") );                                  
			
		// the settings below are optional -- c3p0 can work with defaults
		cpds.setMinPoolSize(5);                                     
		cpds.setAcquireIncrement(10);
		cpds.setMaxPoolSize(500);
		
		return cpds;
	}
}
