package com.boate.security;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.boate.security.entity.UserPrincipal;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.*;

@Component
public class TokenHelper {

	private static final Logger logger = LoggerFactory.getLogger(TokenHelper.class);

	@Value("${app.name}")
	private String APP_NAME;

	@Value("${jwt.secret}")
	private String SECRET;

	@Value("${jwt.expires_in}")
	private int EXPIRES_IN;

	@Value("${jwt.header}")
	private String AUTH_HEADER;

	@Value("${jwt.cookie}")
	private String AUTH_COOKIE;

	private SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;

	public String getUsernameFromToken(String token) {
		String username;
		try {
			final Claims claims = this.getClaimsFromToken(token);
			username = claims.getSubject();
		} catch (Exception e) {
			username = null;
		}
		return username;
	}

	public Claims getClaimsFromToken(String token) {
		Claims claims;
		try {
			claims = Jwts.parser().setSigningKey(this.SECRET).parseClaimsJws(token).getBody();
		} catch (Exception e) {
			claims = null;
		}
		return claims;
	}

	public boolean validateToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(SECRET).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException ex) {
			logger.error("Invalid JWT signature");
		} catch (MalformedJwtException ex) {
			logger.error("Invalid JWT token");
		} catch (ExpiredJwtException ex) {
			logger.error("Expired JWT token");
		} catch (UnsupportedJwtException ex) {
			logger.error("Unsupported JWT token");
		} catch (IllegalArgumentException ex) {
			logger.error("JWT claims string is empty.");
		}
		return false;
	}

	public Integer getUserIdFromJWT(String token) {
		Claims claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();

		return Integer.parseInt(claims.getSubject());
	}

	public String generateToken(Authentication authentication) {

		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

		return Jwts.builder().setSubject(Long.toString(userPrincipal.getId())).setIssuer(APP_NAME).setIssuedAt(new Date())
				.setExpiration(generateExpirationDate()).signWith(SignatureAlgorithm.HS512, SECRET).compact();
	}

	String generateToken(Map<String, Object> claims) {
		return Jwts.builder().setClaims(claims).setExpiration(generateExpirationDate())
				.signWith(SIGNATURE_ALGORITHM, SECRET).compact();
	}

	public Boolean canTokenBeRefreshed(String token) {
		final Date expirationDate = getClaimsFromToken(token).getExpiration();
		return expirationDate.compareTo(generateCurrentDate()) > 0;
	}

	public String refreshToken(String token) {
		String refreshedToken;
		try {
			final Claims claims = getClaimsFromToken(token);
			claims.setIssuedAt(generateCurrentDate());
			refreshedToken = generateToken(claims);
		} catch (Exception e) {
			refreshedToken = null;
		}
		return refreshedToken;
	}

	private long getCurrentTimeMillis() {
		return DateTime.now().getMillis();
	}

	private Date generateCurrentDate() {
		return new Date(getCurrentTimeMillis());
	}

	private Date generateExpirationDate() {
		return new Date(getCurrentTimeMillis() + this.EXPIRES_IN * 1000);
	}

	public String getToken(HttpServletRequest request) {
		/**
		 * Getting the token from Cookie store
		 */
		/*Cookie authCookie = getCookieValueByName(request, "AUTH-TOKEN");
		if (authCookie != null) {
			return authCookie.getValue();
		}*/
		/**
		 * Getting the token from Authentication header e.g Bearer your_token
		 */
		String authHeader = request.getHeader("Authorization");
		if (authHeader != null && authHeader.startsWith("Bearer ")) {
			return authHeader.substring(7);
		}

		return null;
	}

	/**
	 * Find a specific HTTP cookie in a request.
	 *
	 * @param request
	 *            The HTTP request object.
	 * @param name
	 *            The cookie name to look for.
	 * @return The cookie, or <code>null</code> if not found.
	 */
	public Cookie getCookieValueByName(HttpServletRequest request, String name) {
		if (request.getCookies() == null) {
			return null;
		}
		for (int i = 0; i < request.getCookies().length; i++) {
			if (request.getCookies()[i].getName().equals(name)) {
				return request.getCookies()[i];
			}
		}
		return null;
	}

	public static String encryptPassword(String password) {
		String lreturn = "";
		lreturn = DigestUtils.sha256Hex(password);
		return lreturn;
	}
}
