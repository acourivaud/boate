package com.boate.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boate.model.Utilisateur;
import com.boate.ressource.ResourceNotFoundException;
import com.boate.security.entity.UserPrincipal;
import com.boate.service.AuthentificationService;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	AuthentificationService userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		// Let people login with either username or email
		Utilisateur user = userRepository.findIndividuBeanByLogin(login)
				.orElseThrow(() -> new UsernameNotFoundException("User not found with login : " + login));

		return UserPrincipal.create(user);
	}

	@Transactional
	public UserDetails loadUserById(Integer id) {
		Utilisateur user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", id));

		return UserPrincipal.create(user);
	}
}
