package com.boate.security.entity;

import com.boate.model.Utilisateur;
import com.boate.model.UtilisateurPermission;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class UserPrincipal implements UserDetails {

	private static final long serialVersionUID = 1686021081408577271L;

	private Long id;

	private String name;

	private String username;
	
	private List<UtilisateurPermission> permissions;

	@JsonIgnore
	private String email;

	@JsonIgnore
	private String password;

	private Collection<? extends GrantedAuthority> authorities;

	public UserPrincipal(Long id, String name, String username, String email, String password, List<UtilisateurPermission> permissions,
			Collection<? extends GrantedAuthority> authorities) {
		//TODO: Ajouter d'autres éléments nécessaires (permissions
		this.id = id;
		this.name = name;
		this.username = username;
		this.email = email;
		this.password = password;
		this.authorities = authorities;
		this.permissions = permissions;
	}

	public static UserPrincipal create(Utilisateur user) {
		//TODO: A changer par un role affecté à l'utilisateur en BDD
		List<GrantedAuthority> listGrantedAuthorities = new ArrayList<GrantedAuthority>();
		SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_USER");
		listGrantedAuthorities.add(authority);
		
		return new UserPrincipal(Long.parseLong(user.getIdUtilisateur().toString()), user.getNom(), user.getUsername(), user.getEmail(), user.getPassword(), user.getPermissions(),
				listGrantedAuthorities);
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserPrincipal that = (UserPrincipal) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id);
	}

	public List<UtilisateurPermission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<UtilisateurPermission> permissions) {
		this.permissions = permissions;
	}
}
