package com.boate.ressource;

import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.boate.dao.FilterDao;
import com.boate.dto.LazyResultDTO;
import com.boate.dto.ParamValueDTO;
import com.boate.dto.ArchiveBoiteDTO;
import com.boate.dto.ArchiveDossierDTO;
import com.boate.exception.OuvrageException;
import com.boate.exception.UtilisateurException;
import com.boate.exception.ArchiveException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.model.Filter;
import com.boate.security.CurrentUser;
import com.boate.security.entity.UserPrincipal;
import com.boate.service.FilterService;
import com.boate.service.ArchiveService;
import com.boate.utils.ControllerHelper;

@RestController
@RequestMapping(URL.ARCHIVE)
public class ArchiveController extends AbstractController {

	@Autowired
	ArchiveService archiveService;
	
	@Autowired
	FilterService filterService;
	
	@Autowired
	FilterDao filterDao;

	@PostMapping("/filter")
	@ResponseBody
	public ResponseEntity<?> getArchivesFiltered(@RequestBody @Valid final List<ParamValueDTO> requestFilters, @CurrentUser UserPrincipal currentUser)
			throws UtilisateurNotConnectedException, OuvrageException {
		List<ArchiveDossierDTO> archiveDTO = archiveService.getArchives(requestFilters, "listeArchives", currentUser);
		Long count = archiveService.getArchivesCount(requestFilters, currentUser);
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, archiveDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}
	
	@PostMapping("/filterAndSave")
	@ResponseBody
	public ResponseEntity<?> getArchivesFilteredAndSave(@RequestBody @Valid final List<ParamValueDTO> requestFilters, @CurrentUser UserPrincipal currentUser)
			throws UtilisateurNotConnectedException, OuvrageException {
		filterService.saveFiltersForConnectedUser("archives", requestFilters);
		List<ArchiveDossierDTO> archiveDTO = archiveService.getArchives(requestFilters, "listeArchives", currentUser);
		Long count = archiveService.getArchivesCount(requestFilters, currentUser);
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, archiveDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}

	@PostMapping("/export")
	@ResponseBody
	public ResponseEntity<?> exportArchivesFiltered(@RequestBody @Valid final List<ParamValueDTO> requestFilters)
			throws UtilisateurNotConnectedException, ArchiveException {
		
		String guid = null;
		
		for (ParamValueDTO requestFilter : requestFilters) {
			if (requestFilter.getParam().equals("guid")) {
				guid = requestFilter.getValue();
				break;
			}
		}
		
		if (guid == null) {
			throw new ArchiveException("GUID introuvable.");
		}
		
		filterService.saveFiltersForConnectedUser("archive", requestFilters);
		
		List<Filter> filtersToStore = new ArrayList<>();
		
		for (ParamValueDTO requestFilter : requestFilters) {
			if (!requestFilter.getParam().equals("guid")) {
				filtersToStore.add(new Filter(guid, requestFilter.getParam(), requestFilter.getValue()));
			}
		}
		
		filterDao.saveAll(filtersToStore);
		
		return ControllerHelper.getSuccessResponse();
	}
	
	@GetMapping("/export")
	@ResponseBody
	public ResponseEntity<byte[]> extract(@RequestParam("guid") final String guid) throws Exception {
		
		List<Filter> preStoredFilters = filterDao.findByGuid(guid);
		filterDao.deleteAll(preStoredFilters);
		
		// Make the filename unique !
		final File file = new File(""); // = ouvrageService.exportOuvrages(requestFilters);
		
		byte[] contents = null;
		try {
			Files.readAllBytes(file.toPath());
			file.delete();
		} catch (final IOException e) {
			// LOGGER.error(e.toString());
		}
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/octet-stream"));
		headers.setContentDispositionFormData(file.getName(), file.getName());
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		return new ResponseEntity<>(contents, headers, HttpStatus.OK);
	}
	
	@GetMapping("/search")
	@ResponseBody
	public ResponseEntity<?> getBoitesFromSearch(@RequestParam("typedText") final String typedText)
			throws Exception {
		return ControllerHelper.getResponseEntity(archiveService.getBoitesFromSearch(typedText));
	}
	
	@GetMapping(URL.BOITE)
	@ResponseBody
	public ResponseEntity<?> getArchivesBoites() throws UtilisateurNotConnectedException, UtilisateurException {
		try
		{
			List<ArchiveBoiteDTO> archivesDTO = archiveService.getArchivesBoites();
			return ControllerHelper.getResponseEntity(archivesDTO);
		}
		catch(UtilisateurException ue) {
			return ControllerHelper.getErrorResponse(ue.getErrorMessage());
		}
	}
	
	@PostMapping(URL.BOITE)
	@ResponseBody
	public ResponseEntity<?> postArchiveBoite(@RequestBody @Valid final ArchiveBoiteDTO archiveBoiteDTO) throws UtilisateurNotConnectedException, UtilisateurException, ArchiveException {
		try
		{
			List<ArchiveBoiteDTO> archivesDTO = archiveService.postArchiveBoite(archiveBoiteDTO);
			return ControllerHelper.getResponseEntity(archivesDTO);
		}
		catch(UtilisateurException ue) {
			return ControllerHelper.getErrorResponse(ue.getErrorMessage());
		}
		catch(ArchiveException ae) {
			return ControllerHelper.getErrorResponse(ae.getErrorMessage());
		}
	}
	
	@DeleteMapping(URL.BOITE)
	@ResponseBody
	public ResponseEntity<?> deleteArchiveBoite(@RequestBody @Valid final ArchiveBoiteDTO archiveBoiteDTO) throws UtilisateurNotConnectedException, UtilisateurException, ArchiveException {
		try
		{
			List<ArchiveBoiteDTO> archivesDTO = archiveService.deleteArchiveBoite(archiveBoiteDTO);
			return ControllerHelper.getResponseEntity(archivesDTO);
		}
		catch(UtilisateurException ue) {
			return ControllerHelper.getErrorResponse(ue.getErrorMessage());
		}
		catch(ArchiveException ae) {
			return ControllerHelper.getErrorResponse(ae.getErrorMessage());
		}
	}
	
	@GetMapping()
	public ResponseEntity<?> getOuvrage(@RequestParam("id") final Integer id) throws Exception {
		return ControllerHelper.getResponseEntity(archiveService.getArchiveById(id), HttpStatus.OK);
	}
	
	@GetMapping("/ouvrage")
	public ResponseEntity<?> getByOuvrage(@RequestParam("id") final Integer id) throws Exception {
		return ControllerHelper.getResponseEntity(archiveService.getArchiveByOuvrage(id), HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid final ArchiveDossierDTO archiveDossierDTO) throws Exception {
		archiveService.createOrUpdate(archiveDossierDTO);
		return ControllerHelper.getSuccessResponse("Archive enregistrée avec succès");
	}
	
	@DeleteMapping
	public ResponseEntity<?> delete(@RequestBody @Valid final ArchiveDossierDTO archiveDossierDTO) throws Exception {
		archiveService.delete(archiveDossierDTO);
		return ControllerHelper.getSuccessResponse("Archive supprimée avec succès");
	}

}
