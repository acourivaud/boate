package com.boate.ressource;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.boate.dao.FilterDao;
import com.boate.dto.BouclageBatchActionDTO;
import com.boate.dto.BouclageDTO;
import com.boate.dto.DeleteActionDTO;
import com.boate.dto.LazyResultDTO;
import com.boate.dto.ParamValueDTO;
import com.boate.exception.BouclageException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.security.CurrentUser;
import com.boate.security.entity.UserPrincipal;
import com.boate.service.BouclageService;
import com.boate.service.FilterService;
import com.boate.utils.ControllerHelper;

@RestController
@RequestMapping(URL.BOUCLAGE)
public class BouclageController extends AbstractController {

	@Autowired
	BouclageService bouclageService;
	
	@Autowired
	FilterService filterService;
	
	@Autowired
	FilterDao filterDao;
	
	
	@GetMapping
	@ResponseBody
	public ResponseEntity<?> extract(@RequestParam("id") final Integer id, @CurrentUser UserPrincipal currentUser) throws Exception {
		BouclageDTO bouclage = bouclageService.getBouclage(id, currentUser);
		return ControllerHelper.getResponseEntity(bouclage);
	}
	
	@GetMapping("/ouvrage")
	@ResponseBody
	public ResponseEntity<?> getByOuvrage(@RequestParam("id") final Integer id, @CurrentUser UserPrincipal currentUser) throws Exception {
		List<BouclageDTO> bouclages = bouclageService.getBouclagesByOuvrage(id, currentUser);
		return ControllerHelper.getResponseEntity(bouclages);
	}
	
	@PostMapping
	@ResponseBody
	public ResponseEntity<?> saveBouclage(@RequestBody @Valid final BouclageDTO bouclageDTO, @CurrentUser UserPrincipal currentUser)
			throws UtilisateurNotConnectedException, BouclageException {
		BouclageDTO returnDTO = bouclageService.saveBouclage(bouclageDTO, currentUser);
		return ControllerHelper.getResponseEntity(returnDTO);
	}

	@PostMapping("/filter")
	@ResponseBody
	public ResponseEntity<?> getBouclagesFiltered(@RequestBody @Valid final List<ParamValueDTO> requestFilters, @CurrentUser UserPrincipal currentUser)
			throws UtilisateurNotConnectedException, BouclageException {
		String bouclage = null;
		for (ParamValueDTO requestFilter : requestFilters) {
			if (requestFilter.getParam().equals("bouclage")) {
				bouclage = requestFilter.getValue();
				break;
			}
		}
		
		if (bouclage == null) {
			throw new BouclageException("page bouclage introuvable.");
		}
		List<BouclageDTO> bouclageDTO = bouclageService.getBouclages(requestFilters, bouclage, currentUser);
		Long count = bouclageService.getBouclagesCount(requestFilters, bouclage);
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, bouclageDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}
	
	@PostMapping("/filterAndSave")
	@ResponseBody
	public ResponseEntity<?> getBouclagesFilteredAndSave(@RequestBody @Valid final List<ParamValueDTO> requestFilters, @CurrentUser UserPrincipal currentUser)
			throws UtilisateurNotConnectedException, BouclageException {
		String bouclage = null;
		for (ParamValueDTO requestFilter : requestFilters) {
			if (requestFilter.getParam().equals("bouclage")) {
				bouclage = requestFilter.getValue();
				break;
			}
		}
		
		if (bouclage == null) {
			throw new BouclageException("page bouclage introuvable.");
		}
		filterService.saveFiltersForConnectedUser("bouclage/" + bouclage, requestFilters);
		List<BouclageDTO> bouclageDTO = bouclageService.getBouclages(requestFilters, bouclage, currentUser);
		Long count = bouclageService.getBouclagesCount(requestFilters, bouclage);
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, bouclageDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}
	
	@PostMapping("/batchAction")
	@ResponseBody
	public ResponseEntity<?> applyBatchAction(@RequestBody @Valid final BouclageBatchActionDTO bouclageBatchActionDTO, @CurrentUser UserPrincipal currentUser)
			throws UtilisateurNotConnectedException, BouclageException {
		String bouclage = null;
		for (ParamValueDTO requestFilter : bouclageBatchActionDTO.getFiltersForReturn()) {
			if (requestFilter.getParam().equals("bouclage")) {
				bouclage = requestFilter.getValue();
				break;
			}
		}
		if (bouclage == null) {
			throw new BouclageException("page bouclage introuvable.");
		}
		bouclageService.applyBatchAction(bouclageBatchActionDTO);
		List<BouclageDTO> bouclageDTO = bouclageService.getBouclages(bouclageBatchActionDTO.getFiltersForReturn(), bouclage, currentUser);
		Long count = bouclageService.getBouclagesCount(bouclageBatchActionDTO.getFiltersForReturn(), bouclage);
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, bouclageDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}
	
	@DeleteMapping
	@ResponseBody
	public ResponseEntity<?> delete(@RequestBody @Valid final DeleteActionDTO deleteActionDTO, @CurrentUser UserPrincipal currentUser)
			throws UtilisateurNotConnectedException, BouclageException {
		String bouclage = null;
		for (ParamValueDTO requestFilter : deleteActionDTO.getFiltersForReturn()) {
			if (requestFilter.getParam().equals("bouclage")) {
				bouclage = requestFilter.getValue();
				break;
			}
		}
		if (bouclage == null) {
			throw new BouclageException("page bouclage introuvable.");
		}
		bouclageService.deleteBouclage(deleteActionDTO.getIdToDelete());
		List<BouclageDTO> bouclageDTO = bouclageService.getBouclages(deleteActionDTO.getFiltersForReturn(), bouclage, currentUser);
		Long count = bouclageService.getBouclagesCount(deleteActionDTO.getFiltersForReturn(), bouclage);
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, bouclageDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}
}
