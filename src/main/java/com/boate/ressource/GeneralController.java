package com.boate.ressource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.boate.dao.FilterDao;
import com.boate.dto.ParamValueDTO;
import com.boate.service.FilterService;
import com.boate.service.GeneralService;
import com.boate.service.OuvrageService;
import com.boate.utils.ControllerHelper;

@RestController
@RequestMapping(URL.GENERAL)
public class GeneralController extends AbstractController {

	@Autowired
	OuvrageService ouvrageService;

	@Autowired
	GeneralService generalService;

	@Autowired
	FilterService filterService;

	@Autowired
	FilterDao filterDao;

	@PostMapping
	@ResponseBody
	public ResponseEntity<?> getData(@RequestBody @Valid final List<ParamValueDTO> paramValues) throws Exception {
		return ControllerHelper.getResponseEntity(generalService.getData(paramValues));

	}

	@PostMapping("/save")
	@ResponseBody
	public ResponseEntity<?> postData(@RequestBody final List<String> jsonStringList) throws Exception {
		return ControllerHelper.getResponseEntity(generalService.saveDataFromJsonStringList(jsonStringList));
	}

	@DeleteMapping
	@ResponseBody	
	public ResponseEntity<?> deleteData(@RequestBody @Valid final String jsonString)
			throws Exception {
		return ControllerHelper.getResponseEntity(generalService.deleteFromJsonString(jsonString));
	}

}
