package com.boate.ressource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.boate.dto.PermissionDTO;
import com.boate.dto.PlanningCollaborateurDTO;
import com.boate.dto.PlanningEverythingDTO;
import com.boate.dto.PlanningUtilisateurMaterielDTO;
import com.boate.dto.PlanningUtilisateurVehiculeDTO;
import com.boate.dto.UserSummaryDTO;
import com.boate.dto.UtilisateurDTO;
import com.boate.exception.AccessForbiddenException;
import com.boate.exception.UtilisateurException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.exception.VehiculeException;
import com.boate.security.CurrentUser;
import com.boate.security.entity.UserPrincipal;
import com.boate.service.MaterielService;
import com.boate.service.UtilisateurService;
import com.boate.service.VehiculeService;
import com.boate.utils.ControllerHelper;

@RestController
@RequestMapping(URL.UTILISATEUR)
public class UtilisateurController extends AbstractController {

	@Autowired
	UtilisateurService utilisateurService;
	
	@Autowired
	MaterielService materielService;
	
	@Autowired
	VehiculeService vehiculeService;

	@GetMapping(URL.UTILISATEUR_CONNECTED)
	@ResponseBody
	public ResponseEntity<?> getUtilisateurConnected() throws UtilisateurNotConnectedException {
		UtilisateurDTO utilisateurDTO = utilisateurService.getUtilisateurDTOConnected(false);
		return ControllerHelper.getResponseEntity(utilisateurDTO);
	}
	
	@GetMapping("/me")
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
    public UserSummaryDTO getCurrentUser(@CurrentUser UserPrincipal currentUser) {
		UserSummaryDTO userSummary = new UserSummaryDTO(currentUser.getId(), currentUser.getUsername(), currentUser.getName());
        return userSummary;
    }
	
	@GetMapping(URL.UTILISATEUR_CONNECTED_TEST)
	@ResponseBody
	public ResponseEntity<?> getUtilisateurConnectedTest() throws Exception, UtilisateurNotConnectedException {
		utilisateurService.getUtilisateurConnected();
		
		return new ResponseEntity<>("[]", HttpStatus.OK);
	}

	@GetMapping(URL.UTILISATEUR_BY_ID)
	@ResponseBody
	public ResponseEntity<?> getUtilisateurById(@RequestParam("id") Integer idUtilisateur) throws UtilisateurNotConnectedException {
		UtilisateurDTO utilisateurDTO = utilisateurService.getUtilisateurDTOByIdUtilisateur(idUtilisateur);
		return ControllerHelper.getResponseEntity(utilisateurDTO);
	}
	
	@GetMapping(URL.UTILISATEUR_BY_RESPONSABLE)
	@ResponseBody
	public ResponseEntity<?> getUtilisateurByFonction(@RequestParam("fonction") String fonction) throws UtilisateurNotConnectedException {
		List<UtilisateurDTO> utilisateursDTO = utilisateurService.getUtilisateuryByFonction(fonction);
		return ControllerHelper.getResponseEntity(utilisateursDTO);
	}

	@GetMapping
	@ResponseBody
	public ResponseEntity<?> getUtilisateurs() throws UtilisateurNotConnectedException, AccessForbiddenException {
		List<UtilisateurDTO> utilisateursDTO = utilisateurService.getAllUsersDTO(null);
		return ControllerHelper.getResponseEntity(utilisateursDTO);
	}
	
	@GetMapping("/auteurs")
	@ResponseBody
	public ResponseEntity<?> getAuteurs() throws UtilisateurNotConnectedException, AccessForbiddenException {
		List<UtilisateurDTO> utilisateursDTO = utilisateurService.getAuteursDTO();
		return ControllerHelper.getResponseEntity(utilisateursDTO);
	}
	
	@GetMapping("/aoap")
	@ResponseBody
	public ResponseEntity<?> getAoaps() throws UtilisateurNotConnectedException, AccessForbiddenException {
		List<UtilisateurDTO> utilisateursDTO = utilisateurService.getAoapsDTO();
		return ControllerHelper.getResponseEntity(utilisateursDTO);
	}
	
	@GetMapping("/search")
	@ResponseBody
	public ResponseEntity<?> getUtilisateursFromSearch(@RequestParam("typedText") final String typedText)
			throws Exception {
		return ControllerHelper.getResponseEntity(utilisateurService.getAllUsersDTO(typedText));
	}

	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid final UtilisateurDTO utilisateurDTO)
			throws UtilisateurNotConnectedException, AccessForbiddenException, UtilisateurException {
		utilisateurService.createOrUpdate(utilisateurDTO);
		return ControllerHelper.getSuccessResponse("Utilisateur enregistré avec succès.");
	}
	
	@GetMapping(URL.PERMISSIONS_LIST)
	@ResponseBody
	public ResponseEntity<?> getPermissions() throws UtilisateurNotConnectedException, AccessForbiddenException {
		List<PermissionDTO> permissionsDTO = utilisateurService.getAllPermissionsDTO();
		return ControllerHelper.getResponseEntity(permissionsDTO);
	}
	
	@DeleteMapping
	public ResponseEntity<?> delete(@RequestBody @Valid final UtilisateurDTO utilisateurDTO) throws Exception {
		try
		{
			List<UtilisateurDTO> remainingUtilisateursDTO = utilisateurService.delete(utilisateurDTO);
			return ControllerHelper.getResponseEntity(remainingUtilisateursDTO);
		}
		catch(UtilisateurException ue) {
			return ControllerHelper.getErrorResponse(ue.getErrorMessage());
		}
	}
	
	@PostMapping(URL.PLANNING_UTILISATEUR + "/get")
	@ResponseBody
	public ResponseEntity<?> getPlanningForUser(@RequestBody @Valid final PlanningCollaborateurDTO planningCollaborateurDTO) throws VehiculeException, Exception {
		PlanningCollaborateurDTO returnPlanningCollaborateurDTO = utilisateurService.getPlanningForUser(planningCollaborateurDTO.getUtilisateur(), planningCollaborateurDTO.getDateDebut().toLocalDateTime(), planningCollaborateurDTO.getDateFin().toLocalDateTime());
		return ControllerHelper.getResponseEntity(returnPlanningCollaborateurDTO);
	}
	
	@PostMapping(URL.PLANNING_UTILISATEUR + "/get/everything")
	@ResponseBody
	public ResponseEntity<?> getPlanningForEverything(@RequestBody @Valid final PlanningCollaborateurDTO planningCollaborateurDTO) throws VehiculeException, Exception {
		PlanningEverythingDTO planningEverythingDTO = new PlanningEverythingDTO();
		List<PlanningCollaborateurDTO> collaborateurs = utilisateurService.getPlanningForAllUser(planningCollaborateurDTO.getDateDebut().toLocalDateTime(), planningCollaborateurDTO.getDateFin().toLocalDateTime());
		planningEverythingDTO.setCollaborateurs(collaborateurs);
		List<PlanningUtilisateurMaterielDTO> materiels = materielService.getPlanningForAllMateriel(planningCollaborateurDTO.getDateDebut().toLocalDateTime(), planningCollaborateurDTO.getDateFin().toLocalDateTime());
		planningEverythingDTO.setMateriels(materiels);
		List<PlanningUtilisateurVehiculeDTO> vehicules = vehiculeService.getPlanningForAllVehicule(planningCollaborateurDTO.getDateDebut().toLocalDateTime(), planningCollaborateurDTO.getDateFin().toLocalDateTime());
		planningEverythingDTO.setVehicules(vehicules);
		return ControllerHelper.getResponseEntity(planningEverythingDTO);
	}
	
	@PostMapping(URL.PLANNING_UTILISATEUR + "/post")
	public ResponseEntity<?> savePlanningUtilisateur(@RequestBody @Valid final PlanningCollaborateurDTO planningCollaborateurDTO) throws VehiculeException, Exception, UtilisateurNotConnectedException, UtilisateurException {
		try
		{
			PlanningCollaborateurDTO returnPlanningCollaborateurDTO = utilisateurService.createOrUpdatePlanning(planningCollaborateurDTO);
			return ControllerHelper.getResponseEntity(returnPlanningCollaborateurDTO);
		}
		catch(UtilisateurException ue) {
			return ControllerHelper.getErrorResponse(ue.getErrorMessage());
		}
	}
	
	@DeleteMapping(URL.PLANNING_UTILISATEUR)
	public ResponseEntity<?> deletePlanningVehicule(@RequestBody @Valid final PlanningCollaborateurDTO planningCollaborateurDTO) throws Exception {
		try
		{
			PlanningCollaborateurDTO returnPlanningCollaborateurDTO = utilisateurService.deletePlanning(planningCollaborateurDTO);
			return ControllerHelper.getResponseEntity(returnPlanningCollaborateurDTO);
		}
		catch(UtilisateurException ue) {
			return ControllerHelper.getErrorResponse(ue.getErrorMessage());
		}
	}
}
