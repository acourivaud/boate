package com.boate.ressource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.boate.dto.PlanningUtilisateurVehiculeDTO;
import com.boate.dto.VehiculeDTO;
import com.boate.exception.UtilisateurException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.exception.VehiculeException;
import com.boate.service.VehiculeService;
import com.boate.utils.ControllerHelper;

@RestController
@RequestMapping(URL.VEHICULE)
public class VehiculeController extends AbstractController {

	@Autowired
	VehiculeService vehiculeService;

	@GetMapping()
	@ResponseBody
	public ResponseEntity<?> getAll() throws VehiculeException {
		List<VehiculeDTO> vehiculesDTO = vehiculeService.getAllVehiculesDTO();
		return ControllerHelper.getResponseEntity(vehiculesDTO);
	}
	
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid final VehiculeDTO vehiculeDTO) throws VehiculeException {
		try
		{
			vehiculeService.createOrUpdate(vehiculeDTO);
			List<VehiculeDTO> vehiculesDTO = vehiculeService.getAllVehiculesDTO();
			return ControllerHelper.getResponseEntity(vehiculesDTO);
		}
		catch(VehiculeException ve) {
			return ControllerHelper.getErrorResponse(ve.getErrorMessage());
		}
	}
	
	@DeleteMapping
	public ResponseEntity<?> delete(@RequestBody @Valid final VehiculeDTO vehiculeDTO) throws Exception {
		try
		{
			List<VehiculeDTO> remainingVehiculesDTO = vehiculeService.delete(vehiculeDTO);
			return ControllerHelper.getResponseEntity(remainingVehiculesDTO);
		}
		catch(VehiculeException ve) {
			return ControllerHelper.getErrorResponse(ve.getErrorMessage());
		}
	}
	
	@PostMapping(URL.BY_DATE)
	@ResponseBody
	public ResponseEntity<?> getVehiculesByDates(@RequestBody @Valid final PlanningUtilisateurVehiculeDTO planningUtilisateurVehiculeDTO) throws VehiculeException, Exception {
		List<VehiculeDTO> vehicules =
			vehiculeService.getVehiculesByDates(
				planningUtilisateurVehiculeDTO.getDateDebut().toLocalDateTime(),
				planningUtilisateurVehiculeDTO.getDateFin().toLocalDateTime()
			);
		return ControllerHelper.getResponseEntity(vehicules);
	}
	
	@PostMapping(URL.PLANNING_VEHICULE + URL.BY_UTILISATEUR)
	@ResponseBody
	public ResponseEntity<?> getPlanningForUser(@RequestBody @Valid final PlanningUtilisateurVehiculeDTO planningUtilisateurVehiculeDTO) throws VehiculeException, Exception {
		PlanningUtilisateurVehiculeDTO returnplanningUtilisateurVehiculeDTO =
			vehiculeService.getPlanningForUser(
				planningUtilisateurVehiculeDTO.getUtilisateur(),
				planningUtilisateurVehiculeDTO.getDateDebut().toLocalDateTime(),
				planningUtilisateurVehiculeDTO.getDateFin().toLocalDateTime()
			);
		return ControllerHelper.getResponseEntity(returnplanningUtilisateurVehiculeDTO);
	}
	
	@PostMapping(URL.PLANNING_VEHICULE + URL.BY_VEHICULE)
	@ResponseBody
	public ResponseEntity<?> getPlanningForVehicle(@RequestBody @Valid final PlanningUtilisateurVehiculeDTO planningUtilisateurVehiculeDTO) throws VehiculeException, Exception {
		PlanningUtilisateurVehiculeDTO returnPlanningUtilisateurVehiculeDTO = vehiculeService.getPlanningForVehicle(planningUtilisateurVehiculeDTO);
		return ControllerHelper.getResponseEntity(returnPlanningUtilisateurVehiculeDTO);
	}
	
	@PostMapping(URL.PLANNING_VEHICULE)
	public ResponseEntity<?> savePlanningVehicule(@RequestBody @Valid final PlanningUtilisateurVehiculeDTO planningUtilisateurVehiculeDTO) throws VehiculeException, Exception, UtilisateurNotConnectedException, UtilisateurException {
		try
		{
			PlanningUtilisateurVehiculeDTO returnPlanningUtilisateurVehiculeDTO = vehiculeService.createOrUpdatePlanning(planningUtilisateurVehiculeDTO);
			return ControllerHelper.getResponseEntity(returnPlanningUtilisateurVehiculeDTO);
		}
		catch(VehiculeException ve) {
			return ControllerHelper.getErrorResponse(ve.getErrorMessage());
		}
	}
	
	@DeleteMapping(URL.PLANNING_VEHICULE)
	public ResponseEntity<?> deletePlanningVehicule(@RequestBody @Valid final PlanningUtilisateurVehiculeDTO planningUtilisateurVehiculeDTO) throws Exception {
		try
		{
			PlanningUtilisateurVehiculeDTO returnPlanningUtilisateurVehiculeDTO = vehiculeService.deletePlanning(planningUtilisateurVehiculeDTO);
			return ControllerHelper.getResponseEntity(returnPlanningUtilisateurVehiculeDTO);
		}
		catch(VehiculeException ve) {
			return ControllerHelper.getErrorResponse(ve.getErrorMessage());
		}
	}
}
