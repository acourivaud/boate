package com.boate.ressource;

import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.boate.dao.FilterDao;
import com.boate.dto.LazyResultDTO;
import com.boate.dto.ParamValueDTO;
import com.boate.dto.SurveillanceDTO;
import com.boate.exception.OuvrageException;
import com.boate.exception.SurveillanceException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.model.Filter;
import com.boate.service.FilterService;
import com.boate.service.SurveillanceService;
import com.boate.utils.ControllerHelper;

@RestController
@RequestMapping(URL.SURVEILLANCE)
public class SurveillanceController extends AbstractController {

	@Autowired
	SurveillanceService surveillanceService;
	
	@Autowired
	FilterService filterService;
	
	@Autowired
	FilterDao filterDao;

	@PostMapping("/filter")
	@ResponseBody
	public ResponseEntity<?> getSurveillancesFiltered(@RequestBody @Valid final List<ParamValueDTO> requestFilters)
			throws UtilisateurNotConnectedException, OuvrageException {
		List<SurveillanceDTO> surveillanceDTO = surveillanceService.getSurveillances(requestFilters, "listeSurveillances");
		Long count = surveillanceService.getSurveillancesCount(requestFilters);
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, surveillanceDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}
	
	@PostMapping("/filterAndSave")
	@ResponseBody
	public ResponseEntity<?> getSurveillancesFilteredAndSave(@RequestBody @Valid final List<ParamValueDTO> requestFilters)
			throws UtilisateurNotConnectedException, OuvrageException {
		filterService.saveFiltersForConnectedUser("surveillance", requestFilters);
		List<SurveillanceDTO> surveillanceDTO = surveillanceService.getSurveillances(requestFilters, "listeSurveillances");
		Long count = surveillanceService.getSurveillancesCount(requestFilters);
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, surveillanceDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}

	@PostMapping("/export")
	@ResponseBody
	public ResponseEntity<?> exportSurveillancesFiltered(@RequestBody @Valid final List<ParamValueDTO> requestFilters)
			throws UtilisateurNotConnectedException, SurveillanceException {
		
		String guid = null;
		
		for (ParamValueDTO requestFilter : requestFilters) {
			if (requestFilter.getParam().equals("guid")) {
				guid = requestFilter.getValue();
				break;
			}
		}
		
		if (guid == null) {
			throw new SurveillanceException("GUID introuvable.");
		}
		
		filterService.saveFiltersForConnectedUser("surveillance", requestFilters);
		
		List<Filter> filtersToStore = new ArrayList<>();
		
		for (ParamValueDTO requestFilter : requestFilters) {
			if (!requestFilter.getParam().equals("guid")) {
				filtersToStore.add(new Filter(guid, requestFilter.getParam(), requestFilter.getValue()));
			}
		}
		
		filterDao.saveAll(filtersToStore);
		
		return ControllerHelper.getSuccessResponse();
	}
	
	@GetMapping("/export")
	@ResponseBody
	public ResponseEntity<byte[]> extract(@RequestParam("guid") final String guid) throws Exception {
		
		List<Filter> preStoredFilters = filterDao.findByGuid(guid);
		filterDao.deleteAll(preStoredFilters);
		
		// Make the filename unique !
		final File file = new File(""); // = ouvrageService.exportOuvrages(requestFilters);
		
		byte[] contents = null;
		try {
			Files.readAllBytes(file.toPath());
			file.delete();
		} catch (final IOException e) {
			// LOGGER.error(e.toString());
		}
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/octet-stream"));
		headers.setContentDispositionFormData(file.getName(), file.getName());
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		return new ResponseEntity<>(contents, headers, HttpStatus.OK);
	}
	
	@GetMapping()
	public ResponseEntity<?> getSurveillance(@RequestParam("id") final Integer id) throws Exception {
		return ControllerHelper.getResponseEntity(surveillanceService.getSurveillance(id), HttpStatus.OK);
	}
	
	@GetMapping("/ouvrage")
	public ResponseEntity<?> getByOuvrage(@RequestParam("id") final Integer id) throws Exception {
		return ControllerHelper.getResponseEntity(surveillanceService.getSurveillanceByOuvrage(id), HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid final SurveillanceDTO surveillanceDTO) throws Exception {
		surveillanceService.createOrUpdate(surveillanceDTO);
		return ControllerHelper.getSuccessResponse("Surveillance enregistrée avec succès.");
	}
	
	@DeleteMapping
	public ResponseEntity<?> delete(@RequestBody @Valid final SurveillanceDTO surveillanceDTO) throws Exception {
		surveillanceService.archive(surveillanceDTO);
		return ControllerHelper.getSuccessResponse("Surveillance archivée avec succès.");
	}

}
