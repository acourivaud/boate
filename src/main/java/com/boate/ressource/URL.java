package com.boate.ressource;

public class URL {
	// Base api
	public static final String BASE_API = "/api/";
	
	// authentification controller
	public static final String AUTHENTIFICATION = BASE_API + "auth";
	public static final String AUTHENTIFICATION_REFRESH = "/refresh";
	public static final String AUTHENTIFICATION_LOGIN = "/login";
	public static final String AUTHENTIFICATION_LOGOUT = "/logout";
	
	// UtilisateurController
	public static final String UTILISATEUR = BASE_API + "utilisateur";
	public static final String UTILISATEUR_CONNECTED = "/connected";
	public static final String PLANNING_UTILISATEUR = "/planning";
	public static final String UTILISATEUR_BY_RESPONSABLE = "/byResponable";
	public static final String UTILISATEUR_ALL = "/all";
	public static final String UTILISATEUR_BY_ID = "/id";
	public static final String UTILISATEUR_CONNECTED_TEST = "/connectedTest";
	public static final String PERMISSIONS_LIST = "/permissionList";
	
	// OuvrageController
	public static final String OUVRAGE = BASE_API + "ouvrage";
	
	// SurveillanceController
	public static final String SURVEILLANCE = BASE_API + "surveillance";
	
	// BouclageController
	public static final String BOUCLAGE = BASE_API + "bouclage";
	
	// FilterController
	public static final String FILTER = BASE_API + "filter";
	
	// ColumnController
	public static final String COLUMN = BASE_API + "column";
	
	// ProprietaireController
	public static final String PROPRIETAIRE = BASE_API + "proprietaire";
	public static final String PROPRIETAIRE_OUVRAGE = "/ouvrage";
	public static final String PROPRIETAIRE_DISPONIBLE = "/disponible";
	
	// ParamTemplateController
	public static final String PARAM_TEMPLATE = BASE_API + "template";
	
	// GeneralController
	public static final String GENERAL = BASE_API + "/general";
	
	// ArchiveController
	public static final String ARCHIVE = BASE_API + "archive";
	public static final String BOITE = "/boite";
	
	// VehiculeController
	public static final String VEHICULE = BASE_API + "vehicule";
	public static final String PLANNING_VEHICULE = "/planning";
	public static final String BY_VEHICULE = "/vehicule";
	public static final String BY_UTILISATEUR = "/utilisateur";
	public static final String BY_DATE = "/date";
	
	// MaterielController
	public static final String MATERIEL = BASE_API + "materiel";
	public static final String PLANNING_MATERIEL = "/planning";
	public static final String BY_MATERIEL = "/materiel";
}
