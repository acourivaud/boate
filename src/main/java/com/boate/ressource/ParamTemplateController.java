package com.boate.ressource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.boate.service.ParamTemplateService;
import com.boate.utils.ControllerHelper;

@RestController
@RequestMapping(URL.PARAM_TEMPLATE)
public class ParamTemplateController extends AbstractController {

	@Autowired
	ParamTemplateService paramTemplateService;

	@GetMapping
	public ResponseEntity<?> listProprietaire(@RequestParam("id") final String page) throws Exception {
		return ControllerHelper.getResponseEntity(paramTemplateService.findByName(page), HttpStatus.OK);
	}

}
