package com.boate.ressource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import com.boate.exception.AccessForbiddenException;
import com.boate.exception.FilterException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.utils.ControllerHelper;
import com.boate.utils.ErrorResponse;

@RestController
public abstract class AbstractController {
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(final Exception ex) {
		String errorMessage = ex.getMessage();
		HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		if (ex instanceof UtilisateurNotConnectedException) {
			httpStatus = HttpStatus.UNAUTHORIZED;
		} else if (ex instanceof AccessForbiddenException) {
			httpStatus = HttpStatus.UNAUTHORIZED;
		} else if (ex instanceof FilterException) {
			httpStatus = HttpStatus.UNAUTHORIZED;
		}
		
		errorMessage = errorMessage != null ? errorMessage : "Erreur serveur, veuillez contacter l'administrateur";
		return ControllerHelper.getErrorResponse(httpStatus, errorMessage);
	}

}
