package com.boate.ressource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.boate.dto.PlanningUtilisateurMaterielDTO;
import com.boate.dto.MaterielDTO;
import com.boate.exception.UtilisateurException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.exception.MaterielException;
import com.boate.service.MaterielService;
import com.boate.utils.ControllerHelper;

@RestController
@RequestMapping(URL.MATERIEL)
public class MaterielController extends AbstractController {

	@Autowired
	MaterielService materielService;

	@GetMapping()
	@ResponseBody
	public ResponseEntity<?> getAll() throws MaterielException {
		List<MaterielDTO> materielsDTO = materielService.getAllMaterielsDTO();
		return ControllerHelper.getResponseEntity(materielsDTO);
	}
	
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid final MaterielDTO materielDTO) throws MaterielException {
		try
		{
			materielService.createOrUpdate(materielDTO);
			List<MaterielDTO> materielsDTO = materielService.getAllMaterielsDTO();
			return ControllerHelper.getResponseEntity(materielsDTO);
		}
		catch(MaterielException ve) {
			return ControllerHelper.getErrorResponse(ve.getErrorMessage());
		}
	}
	
	@DeleteMapping
	public ResponseEntity<?> delete(@RequestBody @Valid final MaterielDTO materielDTO) throws Exception {
		try
		{
			List<MaterielDTO> remainingMaterielsDTO = materielService.delete(materielDTO);
			return ControllerHelper.getResponseEntity(remainingMaterielsDTO);
		}
		catch(MaterielException ve) {
			return ControllerHelper.getErrorResponse(ve.getErrorMessage());
		}
	}
	
	@PostMapping(URL.PLANNING_MATERIEL + URL.BY_UTILISATEUR)
	@ResponseBody
	public ResponseEntity<?> getPlanningForUser(@RequestBody @Valid final PlanningUtilisateurMaterielDTO planningUtilisateurMaterielDTO) throws Exception {
		PlanningUtilisateurMaterielDTO returnplanningUtilisateurMaterielDTO =
			materielService.getPlanningForUser(
				planningUtilisateurMaterielDTO.getUtilisateur(),
				planningUtilisateurMaterielDTO.getDateDebut().toLocalDateTime(),
				planningUtilisateurMaterielDTO.getDateFin().toLocalDateTime()
			);
		return ControllerHelper.getResponseEntity(returnplanningUtilisateurMaterielDTO);
	}
	
	@PostMapping(URL.PLANNING_MATERIEL + URL.BY_MATERIEL)
	@ResponseBody
	public ResponseEntity<?> getPlanningForVehicle(@RequestBody @Valid final PlanningUtilisateurMaterielDTO planningUtilisateurMaterielDTO) throws MaterielException, Exception {
		PlanningUtilisateurMaterielDTO returnPlanningUtilisateurMaterielDTO = materielService.getPlanningForVehicle(planningUtilisateurMaterielDTO);
		return ControllerHelper.getResponseEntity(returnPlanningUtilisateurMaterielDTO);
	}
	
	@PostMapping(URL.PLANNING_MATERIEL)
	public ResponseEntity<?> savePlanningMateriel(@RequestBody @Valid final PlanningUtilisateurMaterielDTO planningUtilisateurMaterielDTO) throws MaterielException, Exception, UtilisateurNotConnectedException, UtilisateurException {
		try
		{
			PlanningUtilisateurMaterielDTO returnPlanningUtilisateurMaterielDTO = materielService.createOrUpdatePlanning(planningUtilisateurMaterielDTO);
			return ControllerHelper.getResponseEntity(returnPlanningUtilisateurMaterielDTO);
		}
		catch(MaterielException ve) {
			return ControllerHelper.getErrorResponse(ve.getErrorMessage());
		}
	}
	
	@DeleteMapping(URL.PLANNING_MATERIEL)
	public ResponseEntity<?> deletePlanningMateriel(@RequestBody @Valid final PlanningUtilisateurMaterielDTO planningUtilisateurMaterielDTO) throws Exception {
		try
		{
			PlanningUtilisateurMaterielDTO returnPlanningUtilisateurMaterielDTO = materielService.deletePlanning(planningUtilisateurMaterielDTO);
			return ControllerHelper.getResponseEntity(returnPlanningUtilisateurMaterielDTO);
		}
		catch(MaterielException ve) {
			return ControllerHelper.getErrorResponse(ve.getErrorMessage());
		}
	}
}
