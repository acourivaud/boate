package com.boate.ressource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.boate.dto.ApiResponseDTO;
import com.boate.dto.JwtAuthenticationResponseDTO;
import com.boate.dto.LoginRequestDTO;
import com.boate.dto.SignUpRequestDTO;
import com.boate.model.Utilisateur;
import com.boate.security.TokenHelper;
import com.boate.service.UtilisateurService;
import com.boate.utils.ControllerHelper;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UtilisateurService userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    TokenHelper tokenProvider;

    @PostMapping("/signin")
    @ResponseBody
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequestDTO loginRequest) {

    	UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword());
		Authentication authentication = this.authenticationManager.authenticate(token);
		SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ControllerHelper.getResponseEntity(new JwtAuthenticationResponseDTO(jwt), HttpStatus.OK);
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequestDTO signUpRequest) {
        if(userRepository.existsByLogin(signUpRequest.getUsername())) {
            return ControllerHelper.getResponseEntity(new ApiResponseDTO(false, "Login is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        // Creating user's account
        Utilisateur user = new Utilisateur();
        user.setLogin(signUpRequest.getUsername());
        user.setPassword(signUpRequest.getPassword());
        user.setNom(signUpRequest.getName());

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        /*Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new AppException("User Role not set."));*/

        //user.setR

        /*Utilisateur result = userRepository.createOrUpdate(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{username}")
                .buildAndExpand(result.getUsername()).toUri();*/

        return ResponseEntity.ok(new ApiResponseDTO(true, "User registered successfully"));
    }
}
