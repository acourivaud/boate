package com.boate.ressource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.boate.dto.OuvrageProprietaireDTO;
import com.boate.dto.ProprietaireDTO;
import com.boate.exception.UtilisateurException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.service.ProprietaireService;
import com.boate.utils.ControllerHelper;

@RestController
@RequestMapping(URL.PROPRIETAIRE)
public class ProprietaireController extends AbstractController {

	@Autowired
	ProprietaireService proprietaireService;

	@GetMapping
	public ResponseEntity<?> listProprietaire() throws Exception, UtilisateurNotConnectedException {
		return ControllerHelper.getResponseEntity(proprietaireService.findAll(), HttpStatus.OK);
	}
	
	@GetMapping(URL.PROPRIETAIRE_DISPONIBLE)
	public ResponseEntity<?> listProprietaireDisponible(@RequestParam("id") final Integer id) throws Exception {
		return ControllerHelper.getResponseEntity(proprietaireService.findProprietaireDisponible(id), HttpStatus.OK);
	}
	
	@GetMapping(URL.PROPRIETAIRE_OUVRAGE)
	public ResponseEntity<?> listProprietaireOuvrage(@RequestParam("id") final Integer id) throws Exception {
		return ControllerHelper.getResponseEntity(proprietaireService.getByOuvrageId(id), HttpStatus.OK);
	}
	
	@PostMapping(URL.PROPRIETAIRE_OUVRAGE)
	@ResponseBody
	public ResponseEntity<?> saveProprietaireOuvrage(@RequestBody @Valid final OuvrageProprietaireDTO ouvrageProprietaireDTO) throws Exception {
		proprietaireService.createOrUpdateOuvrageProprietaire(ouvrageProprietaireDTO);
		return ControllerHelper.getResponseEntity(proprietaireService.getByOuvrageId(ouvrageProprietaireDTO.getOuvrage().getId()));
	}
	
	@DeleteMapping(URL.PROPRIETAIRE_OUVRAGE)
	public ResponseEntity<?> deleteProprietaireOuvrage(@RequestBody @Valid final OuvrageProprietaireDTO ouvrageProprietaireDTO) throws Exception {
		proprietaireService.deleteOuvrageProprietaire(ouvrageProprietaireDTO);
		return ControllerHelper.getResponseEntity(proprietaireService.getByOuvrageId(ouvrageProprietaireDTO.getOuvrage().getId()));
	}
	
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid final ProprietaireDTO proprietaireDTO) throws Exception {
		try
		{
			List<ProprietaireDTO> proprietairesDTO = proprietaireService.createOrUpdate(proprietaireDTO);
			return ControllerHelper.getResponseEntity(proprietairesDTO);
		}
		catch(UtilisateurException ue) {
			return ControllerHelper.getErrorResponse(ue.getErrorMessage());
		}
	}
	
	@DeleteMapping
	public ResponseEntity<?> delete(@RequestBody @Valid final ProprietaireDTO proprietaireDTO) throws Exception {
		try
		{
			List<ProprietaireDTO> remainingProprietairesDTO = proprietaireService.delete(proprietaireDTO);
			return ControllerHelper.getResponseEntity(remainingProprietairesDTO);
		}
		catch(UtilisateurException ue) {
			return ControllerHelper.getErrorResponse(ue.getErrorMessage());
		}
	}
}
