package com.boate.ressource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.boate.dto.ParamValueDTO;
import com.boate.exception.UtilisateurFilterException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.service.ColumnService;
import com.boate.utils.ControllerHelper;

@RestController
@RequestMapping(URL.COLUMN)
public class ColumnController extends AbstractController {

	@Autowired
	ColumnService columnService;

	@GetMapping
	@ResponseBody
	public ResponseEntity<?> getColumnsForConnectedUser(@RequestParam("page") final String page) throws Exception {
		return ControllerHelper.getResponseEntity(columnService.getColumnsForConnectedUser(page));
	}
	
	@PostMapping
	@ResponseBody
	public ResponseEntity<?> saveColumnsForConnectedUser(@RequestBody @Valid final List<ParamValueDTO> paramValues)
			throws UtilisateurNotConnectedException, UtilisateurFilterException {
		String page = null;
		String param = null;
		String value = null;
		for (ParamValueDTO paramValue : paramValues) {
			if (paramValue.getParam().equals("page")) {
				page = paramValue.getValue();
			}
			if (!paramValue.getParam().equals("page")) {
				param = paramValue.getParam();
				value = paramValue.getValue();
			}
		}
		if (page == null) {
			throw new UtilisateurFilterException("Page introuvable.");
		}
		if (param == null) {
			throw new UtilisateurFilterException("Paramètre de colonne introuvable.");
		}
		columnService.saveColumnsForConnectedUser(page, param, value);
		return ControllerHelper.getSuccessResponse();
	}
	
	@GetMapping("/order")
	@ResponseBody
	public ResponseEntity<?> getColumnsOrderForConnectedUser(@RequestParam("page") final String page) throws Exception {
		return ControllerHelper.getResponseEntity(columnService.getColumnsOrderForConnectedUser(page));
	}
	
	@PostMapping("/order")
	@ResponseBody
	public ResponseEntity<?> saveColumnsOrderForConnectedUser(@RequestBody @Valid final List<ParamValueDTO> paramValues)
			throws UtilisateurNotConnectedException, UtilisateurFilterException {
		String page = null;
		for (ParamValueDTO paramValue : paramValues) {
			if (paramValue.getParam().equals("page")) {
				page = paramValue.getValue();
				break;
			}
		}
		if (page == null) {
			throw new UtilisateurFilterException("Page introuvable.");
		}
		columnService.saveColumnsOrderForConnectedUser(page, paramValues);
		return ControllerHelper.getSuccessResponse();
	}

}
