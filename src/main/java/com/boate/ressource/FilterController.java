package com.boate.ressource;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.boate.dto.CategorieDTO;
import com.boate.dto.GeometrieDTO;
import com.boate.dto.GeometrieTypeDTO;
import com.boate.dto.InfrapoleDTO;
import com.boate.dto.LazyResultDTO;
import com.boate.dto.LigneDTO;
import com.boate.dto.ParamValueDTO;
import com.boate.dto.PortionDTO;
import com.boate.dto.RegionDTO;
import com.boate.dto.SecteurDTO;
import com.boate.dto.TypeDTO;
import com.boate.dto.UniteOperationnelleDTO;
import com.boate.exception.FilterException;
import com.boate.exception.UtilisateurException;
import com.boate.exception.UtilisateurFilterException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.service.FilterService;
import com.boate.utils.ControllerHelper;

@RestController
@RequestMapping(URL.FILTER)
public class FilterController extends AbstractController {

	@Autowired
	FilterService filterService;

	@GetMapping("/utilisateurFilter")
	@ResponseBody
	public ResponseEntity<?> getFiltersForConnectedUser(@RequestParam("page") final String page) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getFiltersForConnectedUser(page));
	}
	
	@PostMapping("/utilisateurFilter")
	@ResponseBody
	public ResponseEntity<?> saveFiltersForConnectedUser(@RequestBody @Valid final List<ParamValueDTO> requestFilters)
			throws UtilisateurNotConnectedException, UtilisateurFilterException {
		String page = null;
		for (ParamValueDTO requestFilter : requestFilters) {
			if (requestFilter.getParam().equals("page")) {
				page = requestFilter.getValue();
				break;
			}
		}
		if (page == null) {
			throw new UtilisateurFilterException("Page introuvable.");
		}
		filterService.saveFiltersForConnectedUser(page, requestFilters);
		return ControllerHelper.getSuccessResponse();
	}
	
	@GetMapping("/region")
	@ResponseBody
	public ResponseEntity<?> getRegions() throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getRegions());
	}
	
	@PostMapping("/region")
	@ResponseBody
	public ResponseEntity<?> saveRegion(@RequestBody @Valid final RegionDTO region)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.saveRegion(region);
		return ControllerHelper.getResponseEntity(filterService.getRegions());
	}
	
	@DeleteMapping("/region")
	@ResponseBody
	public ResponseEntity<?> deleteRegion(@RequestBody @Valid final RegionDTO region)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.deleteRegion(region.getId());
		return ControllerHelper.getResponseEntity(filterService.getRegions());
	}
	
	@GetMapping("/departement")
	@ResponseBody
	public ResponseEntity<?> getDepartements(@RequestParam("regionId") final String regionId) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getDepartements(regionId));
	}
	
	@GetMapping("/infrapole")
	@ResponseBody
	public ResponseEntity<?> getInfrapoles(@RequestParam("id") final String id) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getInfrapoles(id));
	}
	
	@PostMapping("/infrapole")
	@ResponseBody
	public ResponseEntity<?> saveInfrapole(@RequestBody @Valid final InfrapoleDTO infrapole)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.saveInfrapole(infrapole);
		return ControllerHelper.getResponseEntity(filterService.getInfrapoles(infrapole.getRegion().getId().toString()));
	}
	
	@DeleteMapping("/infrapole")
	@ResponseBody
	public ResponseEntity<?> deleteInfrapole(@RequestBody @Valid final InfrapoleDTO infrapole)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.deleteInfrapole(infrapole.getId(), false);
		return ControllerHelper.getResponseEntity(filterService.getInfrapoles(infrapole.getRegion().getId().toString()));
	}
	
	@GetMapping("/secteur")
	@ResponseBody
	public ResponseEntity<?> getSecteurs(@RequestParam("id") final String id) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getSecteurs(id));
	}
	
	@PostMapping("/secteur")
	@ResponseBody
	public ResponseEntity<?> saveSecteur(@RequestBody @Valid final SecteurDTO secteur)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.saveSecteur(secteur);
		return ControllerHelper.getResponseEntity(filterService.getSecteurs(secteur.getInfrapole().getId().toString()));
	}
	
	@DeleteMapping("/secteur")
	@ResponseBody
	public ResponseEntity<?> deleteSecteur(@RequestBody @Valid final SecteurDTO secteur)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.deleteSecteur(secteur.getId());
		return ControllerHelper.getResponseEntity(filterService.getSecteurs(secteur.getInfrapole().getId().toString()));
	}
	
	@GetMapping("/uo")
	@ResponseBody
	public ResponseEntity<?> getUOs(@RequestParam("id") final String secteurId, @RequestParam("id2") final String infrapoleId) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getUOs(secteurId.equals("null") ? null : secteurId, infrapoleId.equals("null") ? null : infrapoleId));
	}
	
	@PostMapping("/uo")
	@ResponseBody
	public ResponseEntity<?> saveUO(@RequestBody @Valid final UniteOperationnelleDTO uo)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.saveUO(uo);
		return ControllerHelper.getResponseEntity(filterService.getUOs(uo.getSecteur() != null ? uo.getSecteur().getId().toString() : null, uo.getInfrapole() != null ? uo.getInfrapole().getId().toString() : null));
	}
	
	@DeleteMapping("/uo")
	@ResponseBody
	public ResponseEntity<?> deleteUO(@RequestBody @Valid final UniteOperationnelleDTO uo)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.deleteUO(uo.getId());
		return ControllerHelper.getResponseEntity(filterService.getUOs(uo.getSecteur() != null ? uo.getSecteur().getId().toString() : null, uo.getInfrapole() != null ? uo.getInfrapole().getId().toString() : null));
	}
	
	@PostMapping("/ligne")
	@ResponseBody
	public ResponseEntity<?> getLignesFiltered(@RequestBody @Valid final List<ParamValueDTO> requestFilters)
			throws UtilisateurNotConnectedException {
		List<LigneDTO> lignesDTO = filterService.getLignes(requestFilters);
		Long count = filterService.getLignesCount();
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, lignesDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}
	
	@GetMapping("/ligne/search")
	@ResponseBody
	public ResponseEntity<?> getLignesLike(@RequestParam("typedText") final String typedText) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getLignesLike(typedText));
	}
	
	@GetMapping("/ligne/portion")
	@ResponseBody
	public ResponseEntity<?> getLignePortions(@RequestParam("id") final String id) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.generatePortionsForLigne(Integer.parseInt(id)));
	}
	
	@PostMapping("/ligne/save")
	@ResponseBody
	public ResponseEntity<?> saveLigne(@RequestBody @Valid final LigneDTO ligne)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.saveLigne(ligne);
		List<ParamValueDTO> requestFilters = new ArrayList<>();
		requestFilters.add(new ParamValueDTO("first","0"));
		requestFilters.add(new ParamValueDTO("rows","10"));
		List<LigneDTO> lignesDTO = filterService.getLignes(requestFilters);
		Long count = filterService.getLignesCount();
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, lignesDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}
	
	@DeleteMapping("/ligne")
	@ResponseBody
	public ResponseEntity<?> deleteLigne(@RequestBody @Valid final LigneDTO ligne)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.deleteLigne(ligne);
		List<ParamValueDTO> requestFilters = new ArrayList<>();
		requestFilters.add(new ParamValueDTO("first","0"));
		requestFilters.add(new ParamValueDTO("rows","10"));
		List<LigneDTO> lignesDTO = filterService.getLignes(requestFilters);
		Long count = filterService.getLignesCount();
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, lignesDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}
	
	@GetMapping("/portion")
	@ResponseBody
	public ResponseEntity<?> getPortions(@RequestParam("id") final String id) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getPortions(id));
	}
	
	@PostMapping("/portion")
	@ResponseBody
	public ResponseEntity<?> savePortion(@RequestBody @Valid final PortionDTO portion)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.savePortion(portion);
		return ControllerHelper.getResponseEntity(filterService.getPortions(portion.getUo().getId().toString()));
	}
	
	@DeleteMapping("/portion")
	@ResponseBody
	public ResponseEntity<?> deletePortion(@RequestBody @Valid final PortionDTO portion)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.deletePortion(portion);
		return ControllerHelper.getResponseEntity(filterService.getPortions(portion.getUo().getId().toString()));
	}
	
	@PostMapping("/geometrie")
	@ResponseBody
	public ResponseEntity<?> getGeometriesFiltered(@RequestBody @Valid final List<ParamValueDTO> requestFilters)
			throws UtilisateurNotConnectedException {
		List<GeometrieDTO> geometriesDTO = filterService.getGeometries(requestFilters);
		Long count = filterService.getGeometriesCount();
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, geometriesDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}
	
	@GetMapping("/geometrie/search")
	@ResponseBody
	public ResponseEntity<?> getGeometriesLike(@RequestParam("typedText") final String typedText) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getGeometrieLike(typedText));
	}
	
	@PostMapping("/geometrie/save")
	@ResponseBody
	public ResponseEntity<?> saveGeometrie(@RequestBody @Valid final GeometrieDTO geometrie)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.saveGeometrie(geometrie);
		List<ParamValueDTO> requestFilters = new ArrayList<>();
		requestFilters.add(new ParamValueDTO("first","0"));
		requestFilters.add(new ParamValueDTO("rows","10"));
		List<GeometrieDTO> geometriesDTO = filterService.getGeometries(requestFilters);
		Long count = filterService.getGeometriesCount();
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, geometriesDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}
	
	@DeleteMapping("/geometrie")
	@ResponseBody
	public ResponseEntity<?> deleteGeometrie(@RequestBody @Valid final GeometrieDTO geometrie)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.deleteGeometrie(geometrie);
		List<ParamValueDTO> requestFilters = new ArrayList<>();
		requestFilters.add(new ParamValueDTO("first","0"));
		requestFilters.add(new ParamValueDTO("rows","10"));
		List<GeometrieDTO> geometriesDTO = filterService.getGeometries(requestFilters);
		Long count = filterService.getGeometriesCount();
		LazyResultDTO lazyResultDTO = new LazyResultDTO(count, geometriesDTO);
		return ControllerHelper.getResponseEntity(lazyResultDTO);
	}
	
	@GetMapping("/categorie")
	@ResponseBody
	public ResponseEntity<?> getCategories() throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getCategories());
	}
	
	@PostMapping("/categorie")
	@ResponseBody
	public ResponseEntity<?> saveCategorie(@RequestBody @Valid final CategorieDTO categorie)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.saveCategorie(categorie);
		List<CategorieDTO> categories = filterService.getCategories();
		return ControllerHelper.getResponseEntity(categories);
	}
	
	@DeleteMapping("/categorie")
	@ResponseBody
	public ResponseEntity<?> deleteCategorie(@RequestBody @Valid final CategorieDTO categorie)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.deleteCategorie(categorie.getId());
		return ControllerHelper.getResponseEntity(filterService.getCategories());
	}
	
	@GetMapping("/type")
	@ResponseBody
	public ResponseEntity<?> getTypes(@RequestParam("id") final String id) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getTypes(id));
	}
	
	@PostMapping("/type")
	@ResponseBody
	public ResponseEntity<?> saveType(@RequestBody @Valid final TypeDTO type)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.saveType(type);
		return ControllerHelper.getResponseEntity(filterService.getTypes(type.getCategorie().getId().toString()));
	}
	
	@DeleteMapping("/type")
	@ResponseBody
	public ResponseEntity<?> deleteType(@RequestBody @Valid final TypeDTO type)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.deleteType(type.getId());
		return ControllerHelper.getResponseEntity(filterService.getTypes(type.getCategorie().getId().toString()));
	}
	
	@GetMapping("/geometrieType")
	@ResponseBody
	public ResponseEntity<?> getGeometriesType(@RequestParam("id") final String id) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getGeometriesType(id));
	}
	
	@PostMapping("/geometrieType")
	@ResponseBody
	public ResponseEntity<?> saveGeometrieType(@RequestBody @Valid final GeometrieTypeDTO geometrieType)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.saveGeometrieType(geometrieType);
		return ControllerHelper.getResponseEntity(filterService.getGeometriesType(geometrieType.getType().getId().toString()));
	}
	
	@DeleteMapping("/geometrieType")
	@ResponseBody
	public ResponseEntity<?> deleteGeometrieType(@RequestBody @Valid final GeometrieTypeDTO geometrieType)
			throws UtilisateurNotConnectedException, UtilisateurFilterException, FilterException {
		filterService.deleteGeometrieType(geometrieType);
		return ControllerHelper.getResponseEntity(filterService.getGeometriesType(geometrieType.getType().getId().toString()));
	}
	
	@GetMapping("/commune")
	@ResponseBody
	public ResponseEntity<?> getCommunes(
				@RequestParam("regionId") final String regionId,
				@RequestParam("departementId") final String departementId,
				@RequestParam("typedText") final String typedText
			) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getCommunes(regionId, departementId, typedText));
	}
	
	@GetMapping("/communeCheck")
	@ResponseBody
	public ResponseEntity<?> checkCommune(
				@RequestParam("regionId") final String regionId,
				@RequestParam("departementId") final String departementId,
				@RequestParam("communeId") final String communeId
			) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.checkCommune(regionId, departementId, communeId));
	}
	
	@GetMapping("/regionInfrapoleSecteurUoLigne")
	@ResponseBody
	public ResponseEntity<?> getInfrapolesSecteursUosLignes(
				@RequestParam("regionId") final String regionId,
				@RequestParam("infrapoleId") final String infrapoleId,
				@RequestParam("secteurId") final String secteurId,
				@RequestParam("uoId") final String uoId
			) throws Exception {
		
		return ControllerHelper.getResponseEntity(filterService.getInfrapolesSecteursUosLignes(
				regionId.equals("null") ? null : regionId,
				infrapoleId.equals("null") ? null : infrapoleId,
				secteurId.equals("null") ? null : secteurId,
				uoId.equals("null") ? null : uoId
		));
	}
	
	@GetMapping("/regionInfrapoleSecteurUo")
	@ResponseBody
	public ResponseEntity<?> getInfrapolesSecteursUos(
				@RequestParam("regionId") final String regionId,
				@RequestParam("infrapoleId") final String infrapoleId,
				@RequestParam("secteurId") final String secteurId
			) throws Exception {
		
		return ControllerHelper.getResponseEntity(filterService.getInfrapolesSecteursUos(
				regionId.equals("null") ? null : regionId,
				infrapoleId.equals("null") ? null : infrapoleId,
				secteurId.equals("null") ? null : secteurId
		));
	}
	
	@GetMapping("/proprietaire")
	@ResponseBody
	public ResponseEntity<?> getProprietaires() throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getProprietaires());
	}
	
	@GetMapping("/ouvrageName")
	@ResponseBody
	public ResponseEntity<?> getOuvragesName(
				@RequestParam("typedText") final String typedText
			) throws Exception {
		return ControllerHelper.getResponseEntity(filterService.getOuvragesName(typedText));
	}
	
	@GetMapping("/refTechRegion")
	@ResponseBody
	public ResponseEntity<?> getReferentsTechniquesRegionaux(@RequestParam("regionId") final String regionId, @RequestParam("fonction") final String fonction) throws UtilisateurException {
		return ControllerHelper.getResponseEntity(filterService.getReferentsTechniquesRegionaux(regionId, fonction));
	}
	
	@GetMapping("/segmentGestion")
	@ResponseBody
	public ResponseEntity<?> getSegmentsGestion() {
		return ControllerHelper.getResponseEntity(filterService.getSegmentsGestion());
	}
	
	@GetMapping("/compteAnalytique")
	@ResponseBody
	public ResponseEntity<?> getComptesAnalytiques() {
		return ControllerHelper.getResponseEntity(filterService.getComptesAnalytiques());
	}

}
