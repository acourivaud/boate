package com.boate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boate.dao.SurveillanceDao;
import com.boate.dto.OuvrageDTO;
import com.boate.dto.ParamValueDTO;
import com.boate.dto.SurveillanceDTO;
import com.boate.exception.OuvrageException;
import com.boate.exception.SurveillanceException;
import com.boate.model.Ouvrage;
import com.boate.model.Surveillance;

@Service
public class SurveillanceService {

	private final Logger LOGGER = LoggerFactory.getLogger(SurveillanceService.class);

	@Autowired
	private SurveillanceDao surveillanceDao;

	public List<SurveillanceDTO> getSurveillances(List<ParamValueDTO> requestFilters, String liste) {
		
		List<Surveillance> surveillances = surveillanceDao.findSurveillancesWithFilters(requestFilters);
		return SurveillanceDTO.toDTOs(surveillances, liste);
	}
	
	public Long getSurveillancesCount(List<ParamValueDTO> requestFilters) {
		Long count = surveillanceDao.countSurveillancesWithFilters(requestFilters);
		return count;
	}
	
	public List<Surveillance> computeSurveillanceWithYear(List<Surveillance> surveillances, Integer annee, Optional<ParamValueDTO> cv, Optional<ParamValueDTO> typeVisite){
		List<Surveillance> list = new ArrayList<Surveillance>();

			if (cv.isPresent() && Boolean.parseBoolean(cv.get().getValue())) {
				//TODO
				/*for (WorkSurveillance obj : objs) {
					if (hasVisitsCVForYear(obj, year)) {
						list.add(obj);
					}
				}*/
			} else {
				String visitType = typeVisite.isPresent() ? typeVisite.get().getValue() : null;

				/*if ("visit_type_vi".equals(visitType)) {
					for (WorkSurveillance obj : objs) {
						if (obj.getEndDate() != null) {
							if (obj.getInitialDate() != null && obj.getInitialDate() <= year
									&& year <= (obj.getEndDate().getYear() + 1900)) {
								if (obj.hasVisitsForYear(year, "VI")) {
									list.add(obj);
								}
							}
						} else {
							if (obj.getInitialDate() != null && obj.getInitialDate() <= year) {
								if (obj.hasVisitsForYear(year, "VI")) {
									list.add(obj);
								}
							}
						}
					}
				} else if ("visit_type_vi_ot".equals(visitType)) {
					for (WorkSurveillance obj : objs) {
						if (obj.getEndDate() != null) {
							if (obj.getInitialDate() != null && obj.getInitialDate() <= year
									&& year <= (obj.getEndDate().getYear() + 1900)) {
								if (obj.hasVisitsForYear(year, "VI_OT")) {
									list.add(obj);
								}
							}
						} else {
							if (obj.getInitialDate() != null && obj.getInitialDate() <= year) {
								if (obj.hasVisitsForYear(year, "VI_OT")) {
									list.add(obj);
								}
							}
						}
					}
				} else if ("visit_type_idi".equals(visitType)) {
					for (WorkSurveillance obj : objs) {
						if (obj.getEndDate() != null) {
							if (obj.getInitialDate() != null && obj.getInitialDate() <= year
									&& year <= (obj.getEndDate().getYear() + 1900)) {
								if (obj.hasVisitsForYear(year, "IDI")) {
									list.add(obj);
								}
							}
						} else {
							if (obj.getInitialDate() != null && obj.getInitialDate() <= year) {
								if (obj.hasVisitsForYear(year, "IDI")) {
									list.add(obj);
								}
							}
						}
					}
				} else if ("visit_type_va_ot".equals(visitType)) {
					for (WorkSurveillance obj : objs) {
						if (obj.getEndDate() != null) {
							if (obj.getInitialDate() != null && obj.getInitialDate() <= year
									&& year <= (obj.getEndDate().getYear() + 1900)) {
								if (obj.hasVisitsForYear(year, "VA_OT")) {
									list.add(obj);
								}
							}
						} else {
							if (obj.getInitialDate() != null && obj.getInitialDate() <= year) {
								if (obj.hasVisitsForYear(year, "VA_OT")) {
									list.add(obj);
								}
							}
						}
					}
				} else {*/
					for (Surveillance uneSurveillance : surveillances) {
						String visitTypeKey = uneSurveillance.getTypeSurveillance();
						if (uneSurveillance.getDateFin() != null) {
							if (uneSurveillance.getAnneeReference() != null && uneSurveillance.getAnneeReference() <= annee
									&& annee <= (uneSurveillance.getDateFin().getYear() + 1900)) {
								if (visitTypeKey != null && uneSurveillance.hasVisitsForYear(annee, visitTypeKey) != null
										&& uneSurveillance.hasVisitsForYear(annee, visitTypeKey)) {
									list.add(uneSurveillance);
								}
							}
						} else {
							if (uneSurveillance.getAnneeReference() != null && uneSurveillance.getAnneeReference() <= annee) {
								if (visitTypeKey != null && uneSurveillance.hasVisitsForYear(annee, visitTypeKey) != null
										&& uneSurveillance.hasVisitsForYear(annee, visitTypeKey)) {
									list.add(uneSurveillance);
								}
							}
						}
					}
			//}
			}
			return list;
	}
	
	public Surveillance getSurveillance(Integer id) {
		return surveillanceDao.findById(id).get();
	}
	
	public List<SurveillanceDTO> getSurveillanceByOuvrage(Integer id) {
		return SurveillanceDTO.toDTOs(surveillanceDao.findByOuvrageId(id), "listeSurveillances");
	}
	
	public void createOrUpdate(final SurveillanceDTO surveillanceDTO) throws Exception {
		Surveillance surveillance = new Surveillance();
		if (surveillanceDTO.getId() != null) { //Surveillance existante
			surveillance = surveillanceDao.findById(surveillanceDTO.getId()).get();
		}
		
		//surveillance.setAnneeReference(surveillanceDTO.getAnneeReference());
		surveillance.setAuteurId(surveillanceDTO.getAuteurId());
		//surveillance.setAuteurIdi(surveillanceDTO.getAuteurIdi());
		//surveillance.setAuteurMaj(surveillanceDTO.getAuteurMaj());
		//surveillance.setAuteurSuppression(surveillanceDTO.getAuteurSuppression());
		surveillance.setAuteurVaOt(surveillanceDTO.getAuteurVaOt());
		surveillance.setAuteurVd(surveillanceDTO.getAuteurVd());
		surveillance.setAuteurVdOt(surveillanceDTO.getAuteurVdOt());
		surveillance.setAuteurVi(surveillanceDTO.getAuteurVi());
		surveillance.setAuteurViOt(surveillanceDTO.getAuteurViOt());
		surveillance.setAuteurVpOt(surveillanceDTO.getAuteurVpOt());
		surveillance.setAuteurVs(surveillanceDTO.getAuteurVs());
		//surveillance.setBefore2007Key(surveillanceDTO.getBefore2007Key());
		//surveillance.setBoolOtVa(surveillanceDTO.getBoolOtVa());
		//surveillance.setCodeAnalytique(surveillanceDTO.getCodeAnalytique());
		surveillance.setCodeSegment(surveillanceDTO.getCodeSegment());
		//surveillance.setComplementaryId(surveillanceDTO.getComplementaryId());
		surveillance.setComplementaryVisit(surveillanceDTO.getComplementaryVisit());
		//surveillance.setConvention(surveillanceDTO.getConvention());
		//surveillance.setConventionInfo(surveillanceDTO.getConventionInfo());
		//surveillance.setCv(surveillanceDTO.getCv());
		//surveillance.setDateDebut(surveillanceDTO.getDateDebut());
		//surveillance.setDateFin(surveillanceDTO.getDateFin());
		//surveillance.setDateMaj(surveillanceDTO.getDateMaj());
		//surveillance.setDateReclassement(surveillanceDTO.getDateReclassement());
		//surveillance.setDateSuppression(surveillanceDTO.getDateSuppression());
		//surveillance.setDiver(surveillanceDTO.getDiver());
		//surveillance.setFirstMeansKey(surveillanceDTO.getFirstMeansKey());
		surveillance.setId(surveillanceDTO.getId());
		//surveillance.setIdi(surveillanceDTO.getIdi());
		//surveillance.setIn1253(surveillanceDTO.getIn1253());
		//surveillance.setLastDateCameraVisit(surveillanceDTO.getLastDateCameraVisit());
		surveillance.setLastDateId(surveillanceDTO.getLastDateId());
		//surveillance.setLastDateIdi(surveillanceDTO.getLastDateIdi());
		//surveillance.setLastDatePv0(surveillanceDTO.getLastDatePv0());
		surveillance.setLastDateVaOt(surveillanceDTO.getLastDateVaOt());
		surveillance.setLastDateVd(surveillanceDTO.getLastDateVd());
		surveillance.setLastDateVdOt(surveillanceDTO.getLastDateVdOt());
		//surveillance.setLastDateVeOt(surveillanceDTO.getLastDateVeOt());
		surveillance.setLastDateVi(surveillanceDTO.getLastDateVi());
		surveillance.setLastDateViOt(surveillanceDTO.getLastDateViOt());
		surveillance.setLastDateVpOt(surveillanceDTO.getLastDateVpOt());
		surveillance.setLastDateVs(surveillanceDTO.getLastDateVs());
		//surveillance.setLastDateVsp(surveillanceDTO.getLastDateVsp());
		//surveillance.setLeveled(surveillanceDTO.getLeveled());
		//surveillance.setMesureInclino(surveillanceDTO.getMesureInclino());
		//surveillance.setMesureTopo(surveillanceDTO.getMesureTopo());
		//surveillance.setMesuresConservatoireOt(surveillanceDTO.getMesuresConservatoireOt());
		//surveillance.setMonitored(surveillanceDTO.getMonitored());
		//surveillance.setMotifReclassement(surveillanceDTO.getMotifReclassement());
		surveillance.setNextDateId(surveillanceDTO.getNextDateId());
		//surveillance.setNextDateIdi(surveillanceDTO.getNextDateIdi());
		surveillance.setNextDateVaOt(surveillanceDTO.getNextDateVaOt());
		surveillance.setNextDateVd(surveillanceDTO.getNextDateVd());
		surveillance.setNextDateVdOt(surveillanceDTO.getNextDateVdOt());
		surveillance.setNextDateVi(surveillanceDTO.getNextDateVi());
		surveillance.setNextDateViOt(surveillanceDTO.getNextDateViOt());
		surveillance.setNextDateVpOt(surveillanceDTO.getNextDateVpOt());
		surveillance.setNextDateVs(surveillanceDTO.getNextDateVs());
		//surveillance.setOuvrage(surveillanceDTO.getOuvrage());
		surveillance.setPeriodiciteId(surveillanceDTO.getPeriodiciteId());
		//surveillance.setPeriodiciteIdi(surveillanceDTO.getPeriodiciteIdi());
		surveillance.setPeriodiciteVaOt(surveillanceDTO.getPeriodiciteVaOt());
		surveillance.setPeriodiciteVd(surveillanceDTO.getPeriodiciteVd());
		surveillance.setPeriodiciteVdOt(surveillanceDTO.getPeriodiciteVdOt());
		surveillance.setPeriodiciteVi(surveillanceDTO.getPeriodiciteVi());
		surveillance.setPeriodiciteViOt(surveillanceDTO.getPeriodiciteViOt());
		surveillance.setPeriodiciteVpOt(surveillanceDTO.getPeriodiciteVpOt());
		surveillance.setPeriodiciteVs(surveillanceDTO.getPeriodiciteVs());
		//surveillance.setReferentAoap(surveillanceDTO.getReferentAoap());
		//surveillance.setReferentRegional(surveillanceDTO.getReferentRegional());
		//surveillance.setRoadSignage(surveillanceDTO.getRoadSignage());
		//surveillance.setSecondMeansKey(surveillanceDTO.getSecondMeansKey());
		//surveillance.setStatut(surveillanceDTO.getStatut());
		//surveillance.setSurveillanceAquatique(surveillanceDTO.getSurveillanceAquatique());
		//surveillance.setThirdMeansKey(surveillanceDTO.getThirdMeansKey());
		//surveillance.setTourneeIntemperieOt(surveillanceDTO.getTourneeIntemperieOt());
		//surveillance.setTypeSurveillance(surveillanceDTO.getTypeSurveillance());
		
		surveillanceDao.save(surveillance);
	}
	
	public void archive(final SurveillanceDTO surveillanceDTO) throws Exception {
		Optional<Surveillance> surveillance = null;
		if (surveillanceDTO.getId() != null) {
			surveillance = surveillanceDao.findById(surveillanceDTO.getId());
			if (surveillance.isPresent()) {
				Surveillance currentSurveillance = surveillance.get();
				// currentSurveillance.setArchive(surveillanceDTO.getArchive());
				surveillanceDao.save(currentSurveillance);
			}else {
				throw new SurveillanceException("La surveillance est introuvable");
			}
		}else {
			throw new SurveillanceException("La surveillance est introuvable");
		}
	}
}
