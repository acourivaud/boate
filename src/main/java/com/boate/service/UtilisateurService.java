package com.boate.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.boate.dao.PermissionDao;
import com.boate.dao.PlanningCollaborateurDao;
import com.boate.dao.PlanningVehiculeDao;
import com.boate.dao.UniteOperationnelleDao;
import com.boate.dao.UtilisateurDao;
import com.boate.dao.UtilisateurPermissionDao;
import com.boate.dto.OuvrageDTO;
import com.boate.dto.PermissionDTO;
import com.boate.dto.PlanningCollaborateurDTO;
import com.boate.dto.PlanningUtilisateurVehiculeDTO;
import com.boate.dto.ReservationCollaborateurDTO;
import com.boate.dto.ReservationVehiculeUtilisateurDTO;
import com.boate.dto.UtilisateurDTO;
import com.boate.dto.VehiculeDTO;
import com.boate.enums.EntiteUtilisateurEnum;
import com.boate.enums.FonctionUtilisateurEnum;
import com.boate.exception.AccessForbiddenException;
import com.boate.exception.OuvrageException;
import com.boate.exception.UtilisateurException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.exception.VehiculeException;
import com.boate.model.Ouvrage;
import com.boate.model.Permission;
import com.boate.model.PlanningCollaborateur;
import com.boate.model.PlanningVehicule;
import com.boate.model.UniteOperationnelle;
import com.boate.model.Utilisateur;
import com.boate.model.UtilisateurPermission;
import com.boate.model.Vehicule;
import com.boate.security.TokenHelper;
import com.sun.media.sound.InvalidDataException;

@Service
public class UtilisateurService {

	private final Logger LOGGER = LoggerFactory.getLogger(UtilisateurService.class);

	@Autowired
	private UtilisateurDao utilisateurDao;
	
	@Autowired
	private PermissionDao permissionDao;
	
	@Autowired
	private UtilisateurPermissionDao utilisateurPermissionDao;
	
	@Autowired
	private UniteOperationnelleDao uniteOperationnelleDao;
	
	@Autowired
	private PlanningCollaborateurDao planningCollaborateurDao;

	public UserDetails getUserDetailsConnected() {
		return (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	public List<UtilisateurDTO> getAllUsersDTO(String typedText) {
		if (typedText != null) {
			return UtilisateurDTO.toDTOs(utilisateurDao.findSearchUtilisateurs(typedText), true);
		}else {
			return UtilisateurDTO.toDTOs(utilisateurDao.findAllOrderByNom(), true);
		}
	}
	
	public List<UtilisateurDTO> getAuteursDTO() {
		return UtilisateurDTO.toDTOs(utilisateurDao.findAuteursOrderByNom(), true);
	}
	
	public List<UtilisateurDTO> getAoapsDTO() {
		return UtilisateurDTO.toDTOs(utilisateurDao.findAoapsOrderByNom(), true);
	}
	
	public Boolean existsByLogin(String username) {
		return utilisateurDao.existsByLogin(username);
	}

	public Utilisateur getUtilisateurConnected() throws UtilisateurNotConnectedException {
		UserDetails userDetails = this.getUserDetailsConnected();
		if (userDetails == null) {
			throw new UtilisateurNotConnectedException();
		}
		return utilisateurDao.findByLogin(userDetails.getUsername()).get();
	}

	public Utilisateur getUtilisateurByIdUtilisateur(Integer idUtilisateur) {
		if (idUtilisateur == null) {
			return null;
		}
		return utilisateurDao.findByIdUtilisateur(idUtilisateur);
	}

	public Utilisateur getUtilisateurByLogin(String mail) {
		if (mail == null) {
			return null;
		}
		return utilisateurDao.findByLogin(mail).get();
	}

	public Utilisateur getUtilisateurFromDTO(UtilisateurDTO utilisateurDTO) {
		if (utilisateurDTO == null) {
			return null;
		}
		Utilisateur utilisateur = getUtilisateurByIdUtilisateur(utilisateurDTO.getIdUtilisateur());
		if (utilisateur == null) {
			utilisateur = getUtilisateurByLogin(utilisateurDTO.getLogin());
		}
		return utilisateur;
	}

	public UtilisateurDTO getUtilisateurDTOConnected(boolean versionSimplifiee) throws UtilisateurNotConnectedException {
		Utilisateur utilisateur = getUtilisateurConnected();
		if (utilisateur == null) {
			throw new UtilisateurNotConnectedException();
		}
		return UtilisateurDTO.toDTO(utilisateur, versionSimplifiee);
	}

	public UtilisateurDTO getUtilisateurDTOByLogin(String login) {
		return UtilisateurDTO.toDTO(getUtilisateurByLogin(login), false);
	}

	public UtilisateurDTO getUtilisateurDTOByIdUtilisateur(Integer idUtilisateur) {
		return UtilisateurDTO.toDTO(getUtilisateurByIdUtilisateur(idUtilisateur), false);
	}
	
	public List<UtilisateurDTO> getUtilisateuryByFonction(String fonction) {
		List<Utilisateur> users;
		if (fonction.equals("null")) {
			users = utilisateurDao.findAll();
		} else {
			users= utilisateurDao.findAllByFonctionOrderByNom(FonctionUtilisateurEnum.valueOf(fonction.toUpperCase().trim()));
		}
		return UtilisateurDTO.toDTOs(users, true);
	}

	public Utilisateur updatePasswordAndSave(Utilisateur utilisateur, String newPassword) {
		utilisateur.setPassword(TokenHelper.encryptPassword(newPassword));
		utilisateur = utilisateurDao.save(utilisateur);
		return utilisateur;
	}

	public void createOrUpdate(final UtilisateurDTO utilisateurDTO)
			throws UtilisateurException, AccessForbiddenException {
		Utilisateur utilisateur = null;
		if (utilisateurDTO.getIdUtilisateur() != null) {
			utilisateur = utilisateurDao.findById(utilisateurDTO.getIdUtilisateur()).get();
			if (utilisateur == null) {
				throw new UtilisateurException("Utilisateur introuvable");
			}
		} else {
			utilisateur = new Utilisateur();
		}
		Utilisateur utilisateurByLogin = utilisateurDao.findByLogin(utilisateurDTO.getLogin()).orElse(null);
		if (utilisateurByLogin != null && utilisateurByLogin.getIdUtilisateur() != utilisateurDTO.getIdUtilisateur()) {
			throw new UtilisateurException("Le login de l'utilisateur doit être unique");
		}
		
		utilisateur.setAffichageAlerte(utilisateurDTO.getAffichageAlerte());
		utilisateur.setCellphone(utilisateurDTO.getTelephoneMobile());
		utilisateur.setChangementPassword(false);
		utilisateur.setConsultationReleaseNotes(utilisateurDTO.getConsultationReleaseNotes() != null ? utilisateurDTO.getConsultationReleaseNotes() : false);
		if (utilisateurDTO.getDateFinMission() != null && utilisateurDTO.getDateFinMission().getDto() != null) {
			String[] split = utilisateurDTO.getDateFinMission().getDto().split("[|]");
			utilisateur.setDateFinMission(LocalDate.of(Integer.parseInt(split[0]), Integer.parseInt(split[1])+1, Integer.parseInt(split[2])));
		} else {
			utilisateur.setDateFinMission(null);
		}
		utilisateur.setEmail(utilisateurDTO.getEmail());
		utilisateur.setEntite(utilisateurDTO.getEntite() != null ? EntiteUtilisateurEnum.valueOf(utilisateurDTO.getEntite().toUpperCase()) : null);
		utilisateur.setFax(utilisateurDTO.getFax());
		utilisateur.setFonction(utilisateurDTO.getFonction() != null ? FonctionUtilisateurEnum.valueOf(utilisateurDTO.getFonction().toUpperCase()) : null);
		utilisateur.setLogin(utilisateurDTO.getLogin());
		utilisateur.setNbLignesTableau(utilisateurDTO.getNbLignesTableau());
		utilisateur.setNom(utilisateurDTO.getNom());
		utilisateur.setPageAccueil(utilisateurDTO.getPageAccueil());
		if (utilisateurDTO.getPassword() != null) {
			utilisateur.setPassword(utilisateurDTO.getPassword());
		}
		utilisateur.setPrenom(utilisateurDTO.getPrenom());
		utilisateur.setSsid(utilisateurDTO.getSsid());
		utilisateur.setTelephone(utilisateurDTO.getTelephoneFixe());
		if (utilisateurDTO.getUp() != null) {
			UniteOperationnelle uniteOperationnelle = uniteOperationnelleDao.findById(utilisateurDTO.getUp().getId()).get();
			utilisateur.setUp(uniteOperationnelle);
		} else {
			utilisateur.setUp(null);
		}
		utilisateur.setUrlSig(utilisateurDTO.getUrlSig());
		
		utilisateurDao.saveAndFlush(utilisateur);
		
		if (utilisateurDTO.getPermissions() != null && utilisateurDTO.getPermissions().size() > 0) {
			List<UtilisateurPermission> permissions;
			if (utilisateurDTO.getIdUtilisateur() != null) {
				permissions = utilisateurPermissionDao.findByUtilisateur(utilisateur);
				utilisateurPermissionDao.deleteAll(permissions);
			}
			permissions = new ArrayList<>();
			for (PermissionDTO permissionDTO : utilisateurDTO.getPermissions()) {
				Permission permission = permissionDao.findById(permissionDTO.getId()).get();
				permissions.add(new UtilisateurPermission(utilisateur, permission));
			}
			utilisateurPermissionDao.saveAll(permissions);
		}
	}
	
	public List<PermissionDTO> getAllPermissionsDTO() {
		return PermissionDTO.toDTOs(permissionDao.findAll());
	}
	
	public List<UtilisateurDTO> delete(final UtilisateurDTO utilisateurDTO) throws Exception {
		Optional<Utilisateur> utilisateur = null;
		if (utilisateurDTO.getIdUtilisateur() != null) {
			utilisateur = utilisateurDao.findById(utilisateurDTO.getIdUtilisateur());
			if (utilisateur.isPresent()) {
				try
				{
					utilisateurDao.delete(utilisateur.get());
				} catch(Exception e) {
					throw new UtilisateurException("Suppression impossible. L'utilisateur est probablement utilisé.");
				}
				
				return getAllUsersDTO(null);
			}
		}
		throw new UtilisateurException("L'utilisateur est introuvable");
	}
	
	public PlanningCollaborateurDTO getPlanningForUser(UtilisateurDTO utilisateurDTO, LocalDateTime dateDebut, LocalDateTime dateFin) {
		return PlanningCollaborateurDTO.toDTO(planningCollaborateurDao.findByUtilisateurIdBetweenDates(utilisateurDTO.getIdUtilisateur(), dateDebut, dateFin), dateDebut, dateFin);
	}
	
	public List<PlanningCollaborateurDTO> getPlanningForAllUser(LocalDateTime dateDebut, LocalDateTime dateFin) {
		List<PlanningCollaborateur> planningCollaborateurs = planningCollaborateurDao.findBetweenDates(dateDebut, dateFin);
		List<PlanningCollaborateurDTO> planningCollaborateursDTO = new ArrayList<PlanningCollaborateurDTO>();
		if (planningCollaborateurs != null && planningCollaborateurs.size() > 0) {
			Utilisateur loopUser = null;
			PlanningCollaborateurDTO loopPlanning = null;
			int remaining = planningCollaborateurs.size();
			for (PlanningCollaborateur planningCollaborateur : planningCollaborateurs) {
				if (loopUser == null || loopUser.getIdUtilisateur() != planningCollaborateur.getUtilisateur().getIdUtilisateur()) {
					if (loopUser != null) {
						planningCollaborateursDTO.add(loopPlanning);
					}
					loopUser = planningCollaborateur.getUtilisateur();
					loopPlanning = new PlanningCollaborateurDTO();
					loopPlanning.setUtilisateur(UtilisateurDTO.toDTO(loopUser, false));
					loopPlanning.setReservationsCollaborateur(new ArrayList<ReservationCollaborateurDTO>());
				}
				loopPlanning.getReservationsCollaborateur().add(ReservationCollaborateurDTO.toDTO(planningCollaborateur));
				remaining--;
				if (remaining < 1) {
					planningCollaborateursDTO.add(loopPlanning);
				}
			}
		}
		return planningCollaborateursDTO;
	}
	
	public PlanningCollaborateurDTO createOrUpdatePlanning(final PlanningCollaborateurDTO planningCollaborateurDTO) throws Exception {
		Utilisateur connectedUser = getUtilisateurConnected();
		
		// Should only contain one date range element to create or update
		if (planningCollaborateurDTO.getReservationsCollaborateur() == null ||
				planningCollaborateurDTO.getReservationsCollaborateur().size() < 1 ||
				planningCollaborateurDTO.getReservationsCollaborateur().size() > 1)
		{
			throw new UtilisateurException("Données de réservation incohérentes");
		}
		
		LocalDateTime dateDebutReservation = planningCollaborateurDTO.getReservationsCollaborateur().get(0).getDateDebut().toLocalDateTime();
		LocalDateTime dateFinReservation = planningCollaborateurDTO.getReservationsCollaborateur().get(0).getDateFin().toLocalDateTime();
		
		List<PlanningCollaborateur> planningsCollaborateur = planningCollaborateurDao.findByUtilisateurIdBetweenDates(connectedUser.getIdUtilisateur(), dateDebutReservation, dateFinReservation);
		Integer planningCollaborateurId = planningCollaborateurDTO.getReservationsCollaborateur().get(0).getIdPlanningCollaborateur();
		if (planningsCollaborateur != null && planningsCollaborateur.size() > 0)
		{
			for (PlanningCollaborateur planningCollaborateur : planningsCollaborateur)
			{
				if (planningCollaborateur.getId() != planningCollaborateurId)
				{
					planningCollaborateurDTO.setConflictOnUserResponse(true);
					planningCollaborateurDTO.setReservationsCollaborateur(new ArrayList<ReservationCollaborateurDTO>());
					planningCollaborateurDTO.getReservationsCollaborateur().add(ReservationCollaborateurDTO.toDTO(planningCollaborateur));
				}
			}
		}
		
		// If all controls went OK then save and return the new entire list of results
		if (!planningCollaborateurDTO.isConflictOnUserResponse())
		{
			PlanningCollaborateur planningCollaborateur;
			if (planningCollaborateurId != null) {
				planningCollaborateur = planningCollaborateurDao.findById(planningCollaborateurId).orElse(null);
				if (planningCollaborateur == null) {
					throw new UtilisateurException("Réservation introuvable");
				}
			} else {
				planningCollaborateur = new PlanningCollaborateur();
			}
			planningCollaborateur.setUtilisateur(connectedUser);
			planningCollaborateur.setLieu(planningCollaborateurDTO.getReservationsCollaborateur().get(0).getLieu());
			planningCollaborateur.setNuit(planningCollaborateurDTO.getReservationsCollaborateur().get(0).getNuit());
			planningCollaborateur.setType(planningCollaborateurDTO.getReservationsCollaborateur().get(0).getType());
			planningCollaborateur.setDateDebut(dateDebutReservation);
			planningCollaborateur.setDateFin(dateFinReservation);
			planningCollaborateurDao.save(planningCollaborateur);
		}
		
		return planningCollaborateurDTO;
	}
	
	public PlanningCollaborateurDTO deletePlanning(final PlanningCollaborateurDTO planningCollaborateurDTO) throws Exception {
		Utilisateur connectedUser = getUtilisateurConnected();
		List<PlanningCollaborateur> planningsCollaborateur = null;
		if (planningCollaborateurDTO.getReservationsCollaborateur() != null && planningCollaborateurDTO.getReservationsCollaborateur().size() > 0) {
			List<Integer> ids = new ArrayList<>();
			for (ReservationCollaborateurDTO reservation : planningCollaborateurDTO.getReservationsCollaborateur())
			{
				ids.add(reservation.getIdPlanningCollaborateur());
			}
			planningsCollaborateur = planningCollaborateurDao.findByIdInList(ids);
			if (planningsCollaborateur != null && planningsCollaborateur.size() > 0)
			{
				for (PlanningCollaborateur planningCollaborateur : planningsCollaborateur) {
					if (planningCollaborateur.getUtilisateur().getIdUtilisateur() != connectedUser.getIdUtilisateur()) {
						throw new UtilisateurException("Non autorisé.");
					}
				}
				planningCollaborateurDao.deleteAll(planningsCollaborateur);
			}
		}
		planningsCollaborateur = planningCollaborateurDao.findByUtilisateurIdBetweenDates(
				connectedUser.getIdUtilisateur(),
				planningCollaborateurDTO.getDateDebut().toLocalDateTime(),
				planningCollaborateurDTO.getDateFin().toLocalDateTime());
		planningCollaborateurDTO.setReservationsCollaborateur(ReservationCollaborateurDTO.toDTOs(planningsCollaborateur));
		return planningCollaborateurDTO;
	}
}
