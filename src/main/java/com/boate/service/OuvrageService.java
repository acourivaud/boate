package com.boate.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boate.dao.OuvrageDao;
import com.boate.dao.SurveillanceDao;
import com.boate.dto.OuvrageDTO;
import com.boate.dto.ParamValueDTO;
import com.boate.exception.OuvrageException;
import com.boate.model.Ouvrage;
import com.boate.model.Surveillance;

@Service
public class OuvrageService {

	private final Logger LOGGER = LoggerFactory.getLogger(OuvrageService.class);

	@Autowired
	private OuvrageDao ouvrageDao;
	
	@Autowired
	private SurveillanceDao surveillanceDao;

	public List<OuvrageDTO> getOuvrages(List<ParamValueDTO> requestFilters, String liste) throws OuvrageException {
		List<Ouvrage> ouvrages = ouvrageDao.findOuvragesWithFilters(requestFilters);
		return OuvrageDTO.toDTOs(ouvrages, liste);
	}
	
	public Long getOuvragesCount(List<ParamValueDTO> requestFilters) {
		Long count = ouvrageDao.countOuvragesWithFilters(requestFilters);
		return count;
	}
	
	public OuvrageDTO getOuvrageById(Integer id) {
		OuvrageDTO ouvrageDTO = OuvrageDTO.toDTO(ouvrageDao.findById(id).get(), "ficheOuvrage");
		List<Surveillance> surv = surveillanceDao.findByOuvrageId(ouvrageDTO.getId());
		Surveillance surveillance = surv != null && surv.size() > 0 ? surv.get(0) : null;
		ouvrageDTO.setSurveillanceId(surveillance != null ? surveillance.getId() : null);
		return ouvrageDTO;
	}
	
	public Ouvrage getOuvrage(Integer id) {
		return ouvrageDao.findById(id).get();
	}
	
	public void createOrUpdate(final OuvrageDTO ouvrageDTO) throws Exception {
		Ouvrage ouvrage = new Ouvrage();
		if (ouvrageDTO.getId() != null) { //Ouvrage existant
			ouvrage = ouvrageDao.findById(ouvrageDTO.getId()).get();
		}
		
		ouvrage.setPk(Double.parseDouble(ouvrageDTO.getPk().replace(",", ".")));
		ouvrage.setCategoryKey(ouvrageDTO.getCategorie());
		
		ouvrage.setArchive(ouvrageDTO.getArchive());
		ouvrage.setSupprime(ouvrageDTO.getSupprime());
		
		ouvrageDao.save(ouvrage);
	}
	
	public void deleteOrArchive(final OuvrageDTO ouvrageDTO) throws Exception {
		Optional<Ouvrage> ouvrage = null;
		if (ouvrageDTO.getId() != null) {
			ouvrage = ouvrageDao.findById(ouvrageDTO.getId());
			if (ouvrage.isPresent()) {
				Ouvrage currentOuvrage = ouvrage.get();
				currentOuvrage.setArchive(ouvrageDTO.getArchive());
				currentOuvrage.setSupprime(ouvrageDTO.getSupprime());
				ouvrageDao.save(currentOuvrage);
			}else {
				throw new OuvrageException("L'ouvrage est introuvable");
			}
		}else {
			throw new OuvrageException("L'ouvrage est introuvable");
		}
	}
}
