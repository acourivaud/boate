package com.boate.service;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boate.dao.ArchiveEmpruntDao;
import com.boate.dao.OuvrageDao;
import com.boate.dao.OuvrageProprietaireDao;
import com.boate.dao.UtilisateurDao;
import com.boate.dto.ArchiveEmpruntDTO;
import com.boate.dto.OuvrageProprietaireDTO;
import com.boate.dto.ParamValueDTO;
import com.boate.enums.GeneralServiceActionEnum;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class GeneralService {

	private final Logger LOGGER = LoggerFactory.getLogger(GeneralService.class);

	@Autowired
	private OuvrageProprietaireDao ouvrageProprietaireDao;
	
	@Autowired
	private OuvrageDao ouvrageDao;
	
	@Autowired
	private UtilisateurDao utilisateurDao;
	
	@Autowired
	private ArchiveEmpruntDao archiveEmpruntDao;
	
	@Autowired
	private ProprietaireService proprietaireService;

	public List<?> getData(List<ParamValueDTO> paramValues) throws Exception {
		String value = paramValues.stream().filter(paramValue -> "className".equals(paramValue.getParam())).findFirst().get().getValue();
		String paramId = paramValues.stream().filter(paramValue -> "paramId".equals(paramValue.getParam())).findFirst().get().getValue();
		
		switch (value) {
			case "OuvrageProprietaire":
				return OuvrageProprietaireDTO.toDTOs(ouvrageProprietaireDao.findByOuvrageOrderByOrdre(ouvrageDao.findById(Integer.parseInt(paramId)).get()));
			case "ArchiveEmprunt":
				return ArchiveEmpruntDTO.toDTOs(archiveEmpruntDao.findByUtilisateurOrderByDateEmpruntDesc(utilisateurDao.findByIdUtilisateur(Integer.parseInt(paramId))), "mesEmprunts");
			default:
				throw new Exception();
		}
	}
	
	public String getClassNameFromJsonString(List<String> jsonStringList, String jsonString) {
		String rst = "";
		if (jsonStringList != null && jsonStringList.size() > 0) {
			// Récupération du className
			JSONObject jsonObject = new JSONObject(jsonStringList.get(0));
			rst = "com.boate.dto." + jsonObject.getString("className") + "DTO";
		}else if (jsonString != null) {
			JSONObject jsonObject = new JSONObject(jsonString);
			rst = "com.boate.dto." + jsonObject.getString("className") + "DTO";
		}
		return rst;
	}
	
	public List<?> saveDataFromJsonStringList(List<String> jsonStringList) throws Exception {
		String className = getClassNameFromJsonString(jsonStringList, null);
		return getDTOFromClassNameAndAction(GeneralServiceActionEnum.SAVE_FROM_JSON_STRING_LIST, className, jsonStringList, null);
	}
	
	public Object deleteFromJsonString(String jsonString) throws Exception {
		return getDTOFromClassNameAndAction(GeneralServiceActionEnum.DELETE_FROM_JSON_STRING, getClassNameFromJsonString(null, jsonString), null, jsonString);
	}
	
	public List<?> getDTOFromClassNameAndAction(GeneralServiceActionEnum action, String className, List<String> jsonStringList, String jsonString) throws Exception {
		
		if (className != null) {
			switch (action) {
				case SAVE_FROM_JSON_STRING_LIST :
					List<Object> listObjectDTO = new ArrayList<>();
					for (String element : jsonStringList) {
						ObjectMapper mapper = new ObjectMapper();
						mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
						Object rendererResultDto = mapper.readValue(element, Class.forName(className));
						listObjectDTO.add(rendererResultDto);
					}
					return saveAllDTO(listObjectDTO, className);
				case SAVE_FROM_JSON_STRING :
					break;
				case GET_OBJECTS :
					break;
				case SAVE_OBJECTS :
					break;
				case DELETE_FROM_JSON_STRING :
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
					Object objectDTO = mapper.readValue(jsonString, Class.forName(className));
					return deleteDTO(objectDTO, className);
			}
		}else {
			throw new Exception("ClassName introuvable");
		}
		return jsonStringList;
	}
	
	public List<?> saveAllDTO(List<Object> listObjectDTO, String className) throws Exception {
		switch (className) {
			case "com.boate.dto.OuvrageProprietaireDTO" :
				proprietaireService.createOrUpdateOuvrageProprietaire(listObjectDTO);
				OuvrageProprietaireDTO ouvrageProprietaireDTO = (OuvrageProprietaireDTO) listObjectDTO.get(0);
				return proprietaireService.getByOuvrageId(ouvrageProprietaireDTO.getOuvrage().getId());
			default:
				throw new Exception("ClassName non reconnue");
			
		}
	}
	
	public List<?> deleteDTO(Object objectDTO, String className) throws Exception {
		switch (className) {
			case "com.boate.dto.OuvrageProprietaireDTO" :
				OuvrageProprietaireDTO ouvrageProprietaireDTO = (OuvrageProprietaireDTO) objectDTO;
				proprietaireService.deleteOuvrageProprietaire(ouvrageProprietaireDTO);
				return proprietaireService.getByOuvrageId(ouvrageProprietaireDTO.getOuvrage().getId());
			default:
				throw new Exception("ClassName non reconnue");
			
		}
	}
}
