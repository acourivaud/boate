package com.boate.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boate.dao.BouclageDao;
import com.boate.dao.BouclageUtilisateurDao;
import com.boate.dto.BouclageBatchActionDTO;
import com.boate.dto.BouclageDTO;
import com.boate.dto.DTO;
import com.boate.dto.ParamValueDTO;
import com.boate.enums.TypeVisiteEnum;
import com.boate.exception.BouclageException;
import com.boate.model.Bouclage;
import com.boate.model.BouclageUtilisateur;
import com.boate.security.entity.UserPrincipal;

@Service
public class BouclageService {

	private final Logger LOGGER = LoggerFactory.getLogger(BouclageService.class);

	@Autowired
	private BouclageDao bouclageDao;

	@Autowired
	private BouclageUtilisateurDao bouclageUtilisateurDao;

	public BouclageDTO getBouclage(Integer id, UserPrincipal currentUser) throws BouclageException {
		Bouclage bouclage = bouclageDao.findById(id).orElse(null);
		if (bouclage == null)
			throw new BouclageException("Bouclage introuvable.");
		return BouclageDTO.toDTO(bouclage, "ficheBouclage", currentUser);
	}

	public List<BouclageDTO> getBouclagesByOuvrage(Integer id, UserPrincipal currentUser) throws BouclageException {
		List<Bouclage> bouclages = bouclageDao.findBySurveillanceOuvrageId(id);
		if (bouclages == null)
			throw new BouclageException("Bouclage introuvable.");
		return BouclageDTO.toDTOs(bouclages, "listeBouclages", currentUser);
	}

	public BouclageDTO saveBouclage(BouclageDTO bouclageDTO, UserPrincipal currentUser) throws BouclageException {
		Bouclage bouclage = null;
		if (bouclageDTO.getId() != null) {
			bouclage = bouclageDao.findById(bouclageDTO.getId()).orElse(null);
			if (bouclage == null)
				throw new BouclageException("Bouclage introuvable.");
		}
		if (bouclage == null) {
			bouclage = new Bouclage();
		}

		// bouclage.setAuteurMaj(bouclageDTO.getAuteurMaj());
		bouclage.setAuteurSuppression(bouclageDTO.getAuteurSuppression());
		// bouclage.setAuthor(bouclageDTO.getAuthor());
		// bouclage.setAuthorSurveillance(bouclageDTO.getAuthorSurveillance());
		bouclage.setCommentaires(bouclageDTO.getCommentaires());
		bouclage.setCostConfortement0(bouclageDTO.getCostConfortement0());
		bouclage.setCostConfortement1(bouclageDTO.getCostConfortement1());
		bouclage.setCostConfortement2(bouclageDTO.getCostConfortement2());
		bouclage.setCostCurage0(bouclageDTO.getCostCurage0());
		bouclage.setCostCurage1(bouclageDTO.getCostCurage1());
		bouclage.setCostCurage2(bouclageDTO.getCostCurage2());
		bouclage.setCostDebroussaillage0(bouclageDTO.getCostDebroussaillage0());
		bouclage.setCostDebroussaillage1(bouclageDTO.getCostDebroussaillage1());
		bouclage.setCostDebroussaillage2(bouclageDTO.getCostDebroussaillage2());
		bouclage.setCostDivers0(bouclageDTO.getCostDivers0());
		bouclage.setCostDivers1(bouclageDTO.getCostDivers1());
		bouclage.setCostDivers2(bouclageDTO.getCostDivers2());
		bouclage.setCostInstrumentation0(bouclageDTO.getCostInstrumentation0());
		bouclage.setCostInstrumentation1(bouclageDTO.getCostInstrumentation1());
		bouclage.setCostInstrumentation2(bouclageDTO.getCostInstrumentation2());
		bouclage.setCostMaconnerie0(bouclageDTO.getCostMaconnerie0());
		bouclage.setCostMaconnerie1(bouclageDTO.getCostMaconnerie1());
		bouclage.setCostMaconnerie2(bouclageDTO.getCostMaconnerie2());
		bouclage.setCostPeinture0(bouclageDTO.getCostPeinture0());
		bouclage.setCostPeinture1(bouclageDTO.getCostPeinture1());
		bouclage.setCostPeinture2(bouclageDTO.getCostPeinture2());
		bouclage.setCostPurge0(bouclageDTO.getCostPurge0());
		bouclage.setCostPurge1(bouclageDTO.getCostPurge1());
		bouclage.setCostPurge2(bouclageDTO.getCostPurge2());
		bouclage.setCostSerrurerie0(bouclageDTO.getCostSerrurerie0());
		bouclage.setCostSerrurerie1(bouclageDTO.getCostSerrurerie1());
		bouclage.setCostSerrurerie2(bouclageDTO.getCostSerrurerie2());
		bouclage.setCostTerrassement0(bouclageDTO.getCostTerrassement0());
		bouclage.setCostTerrassement1(bouclageDTO.getCostTerrassement1());
		bouclage.setCostTerrassement2(bouclageDTO.getCostTerrassement2());
		bouclage.setCostTopographie0(bouclageDTO.getCostTopographie0());
		bouclage.setCostTopographie1(bouclageDTO.getCostTopographie1());
		bouclage.setCostTopographie2(bouclageDTO.getCostTopographie2());
		bouclage.setCoteConfortement0(bouclageDTO.getCoteConfortement0());
		bouclage.setCoteConfortement1(bouclageDTO.getCoteConfortement1());
		bouclage.setCoteConfortement2(bouclageDTO.getCoteConfortement2());
		bouclage.setCoteCurage0(bouclageDTO.getCoteCurage0());
		bouclage.setCoteCurage1(bouclageDTO.getCoteCurage1());
		bouclage.setCoteCurage2(bouclageDTO.getCoteCurage2());
		bouclage.setCoteDebroussaillage0(bouclageDTO.getCoteDebroussaillage0());
		bouclage.setCoteDebroussaillage1(bouclageDTO.getCoteDebroussaillage1());
		bouclage.setCoteDebroussaillage2(bouclageDTO.getCoteDebroussaillage2());
		bouclage.setCoteDivers0(bouclageDTO.getCoteDivers0());
		bouclage.setCoteDivers1(bouclageDTO.getCoteDivers1());
		bouclage.setCoteDivers2(bouclageDTO.getCoteDivers2());
		bouclage.setCoteInstrumentation0(bouclageDTO.getCoteInstrumentation0());
		bouclage.setCoteInstrumentation1(bouclageDTO.getCoteInstrumentation1());
		bouclage.setCoteInstrumentation2(bouclageDTO.getCoteInstrumentation2());
		bouclage.setCotePurge0(bouclageDTO.getCotePurge0());
		bouclage.setCotePurge1(bouclageDTO.getCotePurge1());
		bouclage.setCotePurge2(bouclageDTO.getCotePurge2());
		bouclage.setCoteTerrassement0(bouclageDTO.getCoteTerrassement0());
		bouclage.setCoteTerrassement1(bouclageDTO.getCoteTerrassement1());
		bouclage.setCoteTerrassement2(bouclageDTO.getCoteTerrassement2());
		bouclage.setCoteTopographie0(bouclageDTO.getCoteTopographie0());
		bouclage.setCoteTopographie1(bouclageDTO.getCoteTopographie1());
		bouclage.setCoteTopographie2(bouclageDTO.getCoteTopographie2());
		/*
		 * bouclage.setDate00(bouclageDTO.getDate00());
		 * bouclage.setDate01(bouclageDTO.getDate01());
		 * bouclage.setDate02(bouclageDTO.getDate02());
		 * bouclage.setDate03(bouclageDTO.getDate03());
		 * bouclage.setDate04(bouclageDTO.getDate04());
		 * bouclage.setDate05(bouclageDTO.getDate05());
		 * bouclage.setDate06(bouclageDTO.getDate06());
		 * bouclage.setDate07(bouclageDTO.getDate07());
		 * bouclage.setDate08(bouclageDTO.getDate08());
		 * bouclage.setDate09(bouclageDTO.getDate09());
		 * bouclage.setDate10(bouclageDTO.getDate10());
		 * bouclage.setDate11(bouclageDTO.getDate11());
		 * bouclage.setDate12(bouclageDTO.getDate12());
		 * bouclage.setDate13(bouclageDTO.getDate13());
		 * bouclage.setDate14(bouclageDTO.getDate14());
		 * bouclage.setDate15(bouclageDTO.getDate15());
		 * bouclage.setDate16(bouclageDTO.getDate16());
		 * bouclage.setDate17(bouclageDTO.getDate17());
		 * bouclage.setDate18(bouclageDTO.getDate18());
		 * bouclage.setDate19(bouclageDTO.getDate19());
		 * bouclage.setDate20(bouclageDTO.getDate20());
		 * bouclage.setDate21(bouclageDTO.getDate21());
		 * bouclage.setDate22(bouclageDTO.getDate22());
		 * bouclage.setDate23(bouclageDTO.getDate23());
		 * bouclage.setDate24(bouclageDTO.getDate24());
		 * bouclage.setDate25(bouclageDTO.getDate25());
		 * bouclage.setDate26(bouclageDTO.getDate26());
		 * bouclage.setDate27(bouclageDTO.getDate27());
		 * bouclage.setDate28(bouclageDTO.getDate28());
		 * bouclage.setDate29(bouclageDTO.getDate29());
		 * bouclage.setDate30(bouclageDTO.getDate30());
		 * bouclage.setDate31(bouclageDTO.getDate31());
		 * bouclage.setDate32(bouclageDTO.getDate32());
		 * bouclage.setDate33(bouclageDTO.getDate33());
		 * bouclage.setDate34(bouclageDTO.getDate34());
		 * bouclage.setDate35(bouclageDTO.getDate35());
		 * bouclage.setDate36(bouclageDTO.getDate36());
		 * bouclage.setDate37(bouclageDTO.getDate37());
		 * bouclage.setDate38(bouclageDTO.getDate38());
		 * bouclage.setDate39(bouclageDTO.getDate39());
		 * bouclage.setDate40(bouclageDTO.getDate40());
		 */
		// bouclage.setDateSuppression(bouclageDTO.getDateSuppression());
		bouclage.setDate_maj(bouclageDTO.getDate_maj());
		bouclage.setDelaiTravauxAgent0(bouclageDTO.getDelaiTravauxAgent0());
		bouclage.setDelaiTravauxAgent1(bouclageDTO.getDelaiTravauxAgent1());
		bouclage.setDelaiTravauxExpert0(bouclageDTO.getDelaiTravauxExpert0());
		bouclage.setDelaiTravauxExpert1(bouclageDTO.getDelaiTravauxExpert1());
		bouclage.setEcr1(bouclageDTO.getEcr1());
		bouclage.setEcr1_pkd(bouclageDTO.getEcr1_pkd());
		bouclage.setEcr1_pkf(bouclageDTO.getEcr1_pkf());
		bouclage.setEcr2(bouclageDTO.getEcr2());
		bouclage.setEcr2_pkd(bouclageDTO.getEcr2_pkd());
		bouclage.setEcr2_pkf(bouclageDTO.getEcr2_pkf());
		bouclage.setEcr3(bouclageDTO.getEcr3());
		bouclage.setEcr3_pkd(bouclageDTO.getEcr3_pkd());
		bouclage.setEcr3_pkf(bouclageDTO.getEcr3_pkf());
		bouclage.setEcr4(bouclageDTO.getEcr4());
		bouclage.setEcr4_pkd(bouclageDTO.getEcr4_pkd());
		bouclage.setEcr4_pkf(bouclageDTO.getEcr4_pkf());
		bouclage.setEcr5(bouclageDTO.getEcr5());
		bouclage.setEcr5_pkd(bouclageDTO.getEcr5_pkd());
		bouclage.setEcr5_pkf(bouclageDTO.getEcr5_pkf());
		bouclage.setEcr6(bouclageDTO.getEcr6());
		bouclage.setEcr6_pkd(bouclageDTO.getEcr6_pkd());
		bouclage.setEcr6_pkf(bouclageDTO.getEcr6_pkf());
		bouclage.setEnvoiSt0(bouclageDTO.getEnvoiSt0());
		bouclage.setEnvoiSt1(bouclageDTO.getEnvoiSt1());
		bouclage.setEnvoiSt2(bouclageDTO.getEnvoiSt2());
		bouclage.setEtatCV(bouclageDTO.getEtatCV());
		bouclage.setEtatOuvrage(bouclageDTO.getEtatOuvrage());
		bouclage.setFicheChuteBloc(bouclageDTO.getFicheChuteBloc());
		bouclage.setHasCourriers0(bouclageDTO.getHasCourriers0());
		bouclage.setHasCourriers1(bouclageDTO.getHasCourriers1());
		bouclage.setHasCourriers2(bouclageDTO.getHasCourriers2());
		bouclage.setId(bouclageDTO.getId());
		bouclage.setIncident(bouclageDTO.getIncident());
		// bouclage.setIncidentDate(bouclageDTO.getIncidentDate());
		bouclage.setIncidentDescription(bouclageDTO.getIncidentDescription());
		bouclage.setIncidentImportanceKey(bouclageDTO.getIncidentImportanceKey());
		bouclage.setLaborAcceptation0Key(bouclageDTO.getLaborAcceptation0Key());
		bouclage.setLaborAcceptation1Key(bouclageDTO.getLaborAcceptation1Key());
		bouclage.setLaborAcceptation2Key(bouclageDTO.getLaborAcceptation2Key());
		bouclage.setLaborAnalyseRisque0(bouclageDTO.getLaborAnalyseRisque0());
		bouclage.setLaborAnalyseRisque1(bouclageDTO.getLaborAnalyseRisque1());
		bouclage.setLaborAnalyseRisque2(bouclageDTO.getLaborAnalyseRisque2());
		bouclage.setLaborCostAgent0(bouclageDTO.getLaborCostAgent0());
		bouclage.setLaborCostAgent1(bouclageDTO.getLaborCostAgent1());
		bouclage.setLaborCostAgent2(bouclageDTO.getLaborCostAgent2());
		bouclage.setLaborCostSupervisor0(bouclageDTO.getLaborCostSupervisor0());
		bouclage.setLaborCostSupervisor1(bouclageDTO.getLaborCostSupervisor1());
		bouclage.setLaborCostSupervisor2(bouclageDTO.getLaborCostSupervisor2());
		bouclage.setLaborMesuresConservatoire0(bouclageDTO.getLaborMesuresConservatoire0());
		bouclage.setLaborMesuresConservatoire1(bouclageDTO.getLaborMesuresConservatoire1());
		bouclage.setLaborMesuresConservatoire2(bouclageDTO.getLaborMesuresConservatoire2());
		bouclage.setLaborTodoAgent0(bouclageDTO.getLaborTodoAgent0());
		bouclage.setLaborTodoAgent1(bouclageDTO.getLaborTodoAgent1());
		bouclage.setLaborTodoAgent2(bouclageDTO.getLaborTodoAgent2());
		bouclage.setLaborTodoSupervisor0(bouclageDTO.getLaborTodoSupervisor0().getProposition());
		bouclage.setLaborTodoSupervisor1(bouclageDTO.getLaborTodoSupervisor1().getProposition());
		bouclage.setLaborTodoSupervisor2(bouclageDTO.getLaborTodoSupervisor2().getProposition());
		bouclage.setMotifAnnulationCV(bouclageDTO.getMotifAnnulationCV());
		bouclage.setMotifCV(bouclageDTO.getMotifCV());
		bouclage.setMotifControle(bouclageDTO.getMotifControle());
		bouclage.setNoteAppui(bouclageDTO.getNoteAppui());
		bouclage.setNoteAppuiPrec(bouclageDTO.getNoteAppuiPrec());
		bouclage.setNoteGlobale(bouclageDTO.getNoteGlobale());
		bouclage.setNoteGlobalePrec(bouclageDTO.getNoteGlobalePrec());
		bouclage.setNoteMax(bouclageDTO.getNoteMax());
		bouclage.setNoteMin(bouclageDTO.getNoteMin());
		bouclage.setNoteTablier(bouclageDTO.getNoteTablier());
		bouclage.setNoteTablierPrec(bouclageDTO.getNoteTablierPrec());
		bouclage.setNumeroFicheRex(bouclageDTO.getNumeroFicheRex());
		bouclage.setOperation(bouclageDTO.getOperation());
		bouclage.setPrevisionDate(bouclageDTO.getPrevisionDate());
		bouclage.setSolutionCV(bouclageDTO.getSolutionCV());
		bouclage.setStatut(bouclageDTO.getStatut());
		// bouclage.setSurveillance(bouclageDTO.getSurveillance());
		bouclage.setTypeKey(TypeVisiteEnum.valueOf(bouclageDTO.getTypeKey()));
		bouclage.setTypeSurvCompl(bouclageDTO.getTypeSurvCompl());
		bouclage.setU0Integrite(bouclageDTO.getU0Integrite());
		bouclage.setU0SecuritePersonne(bouclageDTO.getU0SecuritePersonne());
		bouclage.setU0SecuriteTiers(bouclageDTO.getU0SecuriteTiers());
		bouclage.setU0_etude(Boolean.valueOf(bouclageDTO.getU0_etude().toString()));
		bouclage.setU0_expertise(Boolean.valueOf(bouclageDTO.getU0_expertise().toString()));
		bouclage.setU0_typeExpertise(bouclageDTO.getU0_typeExpertise());
		bouclage.setU1Integrite(bouclageDTO.getU1Integrite());
		bouclage.setU1SecuritePersonne(bouclageDTO.getU1SecuritePersonne());
		bouclage.setU1SecuriteTiers(bouclageDTO.getU1SecuriteTiers());
		bouclage.setU1_etude(Boolean.valueOf(bouclageDTO.getU1_etude().toString()));
		bouclage.setU1_expertise(Boolean.valueOf(bouclageDTO.getU1_expertise().toString()));
		bouclage.setU1_typeExpertise(bouclageDTO.getU1_typeExpertise());
		bouclage.setU2Integrite(bouclageDTO.getU2Integrite());
		bouclage.setU2SecuritePersonne(bouclageDTO.getU2SecuritePersonne());
		bouclage.setU2SecuriteTiers(bouclageDTO.getU2SecuriteTiers());
		bouclage.setU2_etude(Boolean.valueOf(bouclageDTO.getU2_etude().toString()));
		bouclage.setU2_expertise(Boolean.valueOf(bouclageDTO.getU2_expertise().toString()));
		bouclage.setU2_typeExpertise(bouclageDTO.getU2_typeExpertise());

		bouclageDao.saveAndFlush(bouclage);

		return BouclageDTO.toDTO(bouclage, "ficheBouclage", currentUser);
	}

	public List<BouclageDTO> getBouclages(List<ParamValueDTO> requestFilters, String liste, UserPrincipal currentUser) {
		List<Bouclage> bouclages = bouclageDao.findBouclagesWithFilters(requestFilters, liste);
		return BouclageDTO.toDTOs(bouclages, "listeBouclages", currentUser);
	}

	public Long getBouclagesCount(List<ParamValueDTO> requestFilters, String liste) {
		Long count = bouclageDao.countBouclagesWithFilters(requestFilters, liste);
		return count;
	}

	public void applyBatchAction(BouclageBatchActionDTO bouclageBatchActionDTO) throws BouclageException {
		List<Bouclage> bouclages = bouclageDao.findByIdInList(bouclageBatchActionDTO.getBouclagesIds());
		for (Bouclage bouclage : bouclages) {
			doApplyBatchAction(bouclage, bouclageBatchActionDTO.getBatchType(), bouclageBatchActionDTO.getDate(),
					bouclageBatchActionDTO.getNumeroOperation());
		}
		bouclageDao.saveAll(bouclages);
	}

	private String exceptionBatch(String typeError) throws BouclageException {
		switch (typeError) {
		case "date_posterieure":
			throw new BouclageException("La date saisie doit être postérieure à la précédente");
		default:
			return "Une erreur est survenue";
		}
	}

	private void doApplyBatchAction(Bouclage bouclage, String batchType, LocalDate date, Integer numeroOperation)
			throws BouclageException {

		if (batchType.equals("AJOUT_DATE")) {
			switch (bouclage.getTypeKey()) {
			case visit_type_id:
			case visit_type_id_cv:
				if (bouclage.getDate00() == null) {
					bouclage.setDate00(date);
					// TODO: A ajouter l'auteur, liste déroulante --> bouclage.setAuthor();
					break;
				}

				if (bouclage.getDate01() == null) {
					if (date.isEqual(bouclage.getDate00()) || date.isAfter(bouclage.getDate00())) {
						bouclage.setDate01(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate02() == null) {
					if (date.isEqual(bouclage.getDate01()) || date.isAfter(bouclage.getDate01())) {
						bouclage.setDate02(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate03() == null) {
					if (date.isEqual(bouclage.getDate02()) || date.isAfter(bouclage.getDate02())) {
						bouclage.setDate03(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate04() == null) {
					if (date.isEqual(bouclage.getDate03()) || date.isAfter(bouclage.getDate03())) {
						bouclage.setDate04(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate05() == null) {
					if (date.isEqual(bouclage.getDate04()) || date.isAfter(bouclage.getDate04())) {
						bouclage.setDate05(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate06() == null) {
					if (date.isEqual(bouclage.getDate05()) || date.isAfter(bouclage.getDate05())) {
						bouclage.setDate06(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate07() == null) {
					if (date.isEqual(bouclage.getDate06()) || date.isAfter(bouclage.getDate06())) {
						bouclage.setDate07(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate13() == null) {
					if (date.isEqual(bouclage.getDate07()) || date.isAfter(bouclage.getDate07())) {
						bouclage.setDate13(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate10() == null) {
					if (date.isEqual(bouclage.getDate13()) || date.isAfter(bouclage.getDate13())) {
						bouclage.setDate10(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate11() == null) {
					if (date.isEqual(bouclage.getDate10()) || date.isAfter(bouclage.getDate10())) {
						bouclage.setDate11(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate12() == null) {
					if (date.isEqual(bouclage.getDate11()) || date.isAfter(bouclage.getDate11())) {
						bouclage.setDate12(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}
				break;

			case visit_type_vd_ot: // TODO: For multiple case in one line --> case visit_type_idi:

				if (bouclage.getDate00() == null) {
					bouclage.setDate00(date);
					// TODO: A ajouter l'auteur, liste déroulante --> bouclage.setAuthor();
					break;
				}

				if (bouclage.getDate01() == null) {
					if (date.isEqual(bouclage.getDate00()) || date.isAfter(bouclage.getDate00())) {
						bouclage.setDate01(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate02() == null) {
					if (date.isEqual(bouclage.getDate01()) || date.isAfter(bouclage.getDate01())) {
						bouclage.setDate02(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate03() == null) {
					if (date.isEqual(bouclage.getDate02()) || date.isAfter(bouclage.getDate02())) {
						bouclage.setDate03(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate04() == null) {
					if (date.isEqual(bouclage.getDate03()) || date.isAfter(bouclage.getDate03())) {
						bouclage.setDate04(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate15() == null) {
					if (date.isEqual(bouclage.getDate04()) || date.isAfter(bouclage.getDate04())) {
						bouclage.setDate15(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate16() == null) {
					if (date.isEqual(bouclage.getDate15()) || date.isAfter(bouclage.getDate15())) {
						bouclage.setDate16(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate08() == null) {
					if (date.isEqual(bouclage.getDate16()) || date.isAfter(bouclage.getDate16())) {
						bouclage.setDate08(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate30() == null) {
					if (date.isEqual(bouclage.getDate08()) || date.isAfter(bouclage.getDate08())) {
						bouclage.setDate30(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate13() == null) {
					if (date.isEqual(bouclage.getDate30()) || date.isAfter(bouclage.getDate30())) {
						bouclage.setDate13(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate10() == null) {
					if (date.isEqual(bouclage.getDate13()) || date.isAfter(bouclage.getDate13())) {
						bouclage.setDate10(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate11() == null) {
					if (date.isEqual(bouclage.getDate10()) || date.isAfter(bouclage.getDate10())) {
						bouclage.setDate11(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate12() == null) {
					if (date.isEqual(bouclage.getDate11()) || date.isAfter(bouclage.getDate11())) {
						bouclage.setDate12(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}
				break;

			case visit_type_cl_ot: // TODO: For multiple case in one line --> case visit_type_idi:

				if (bouclage.getDate31() == null) {
					bouclage.setDate31(date);
					// TODO: A ajouter l'auteur, liste déroulante --> bouclage.setAuthor();
					break;
				}

				if (bouclage.getDate21() == null) {
					if (date.isEqual(bouclage.getDate31()) || date.isAfter(bouclage.getDate31())) {
						bouclage.setDate21(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate22() == null) {
					if (date.isEqual(bouclage.getDate21()) || date.isAfter(bouclage.getDate21())) {
						bouclage.setDate22(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate32() == null) {
					if (date.isEqual(bouclage.getDate22()) || date.isAfter(bouclage.getDate22())) {
						bouclage.setDate32(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate13() == null) {
					if (date.isEqual(bouclage.getDate32()) || date.isAfter(bouclage.getDate32())) {
						bouclage.setDate13(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}
				if (bouclage.getDate03() == null) {
					if (date.isEqual(bouclage.getDate13()) || date.isAfter(bouclage.getDate13())) {
						bouclage.setDate03(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate12() == null) {
					if (date.isEqual(bouclage.getDate03()) || date.isAfter(bouclage.getDate03())) {
						bouclage.setDate12(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}
				break;

			case visit_type_vp_ot: // TODO: For multiple case in one line --> case visit_type_idi:

				if (bouclage.getDate00() == null) {
					bouclage.setDate00(date);
					// TODO: A ajouter l'auteur, liste déroulante --> bouclage.setAuthor();
					break;
				}

				if (bouclage.getDate01() == null) {
					if (date.isEqual(bouclage.getDate00()) || date.isAfter(bouclage.getDate00())) {
						bouclage.setDate01(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate32() == null) {
					if (date.isEqual(bouclage.getDate01()) || date.isAfter(bouclage.getDate01())) {
						bouclage.setDate32(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate13() == null) {
					if (date.isEqual(bouclage.getDate32()) || date.isAfter(bouclage.getDate32())) {
						bouclage.setDate13(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}
				if (bouclage.getDate03() == null) {
					if (date.isEqual(bouclage.getDate13()) || date.isAfter(bouclage.getDate13())) {
						bouclage.setDate03(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate12() == null) {
					if (date.isEqual(bouclage.getDate03()) || date.isAfter(bouclage.getDate03())) {
						bouclage.setDate12(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}
				break;

			case visit_type_idi:

				if (bouclage.getDate00() == null) {
					bouclage.setDate00(date);
					// TODO: A ajouter l'auteur, liste déroulante --> bouclage.setAuthor();
					break;
				}

				if (bouclage.getDate14() == null) {
					if (date.isEqual(bouclage.getDate00()) || date.isAfter(bouclage.getDate00())) {
						bouclage.setDate14(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate04() == null) {
					if (date.isEqual(bouclage.getDate14()) || date.isAfter(bouclage.getDate14())) {
						bouclage.setDate04(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate05() == null) {
					if (date.isEqual(bouclage.getDate04()) || date.isAfter(bouclage.getDate04())) {
						bouclage.setDate05(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate06() == null) {
					if (date.isEqual(bouclage.getDate05()) || date.isAfter(bouclage.getDate05())) {
						bouclage.setDate06(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate26() == null) {
					if (date.isEqual(bouclage.getDate06()) || date.isAfter(bouclage.getDate06())) {
						bouclage.setDate26(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate13() == null) {
					if (date.isEqual(bouclage.getDate26()) || date.isAfter(bouclage.getDate26())) {
						bouclage.setDate13(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate12() == null) {
					if (date.isEqual(bouclage.getDate13()) || date.isAfter(bouclage.getDate13())) {
						bouclage.setDate12(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate03() == null) {
					if (date.isEqual(bouclage.getDate12()) || date.isAfter(bouclage.getDate12())) {
						bouclage.setDate03(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}
				break;

			case visit_type_vi_ot:

				if (bouclage.getDate00() == null) {
					bouclage.setDate00(date);
					// TODO: A ajouter l'auteur, liste déroulante --> bouclage.setAuthor();
					break;
				}

				if (bouclage.getDate01() == null) {
					if (date.isEqual(bouclage.getDate00()) || date.isAfter(bouclage.getDate00())) {
						bouclage.setDate01(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate29() == null) {
					if (date.isEqual(bouclage.getDate01()) || date.isAfter(bouclage.getDate01())) {
						bouclage.setDate29(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate13() == null) {
					if (date.isEqual(bouclage.getDate29()) || date.isAfter(bouclage.getDate29())) {
						bouclage.setDate13(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate33() == null) {
					if (date.isEqual(bouclage.getDate13()) || date.isAfter(bouclage.getDate13())) {
						bouclage.setDate33(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate03() == null) {
					if (date.isEqual(bouclage.getDate33()) || date.isAfter(bouclage.getDate33())) {
						bouclage.setDate03(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}
				break;

			case visit_type_vi:

				if (bouclage.getDate00() == null) {
					bouclage.setDate00(date);
					// TODO: A ajouter l'auteur, liste déroulante --> bouclage.setAuthor();
					break;
				}

				if (bouclage.getDate14() == null) {
					if (date.isEqual(bouclage.getDate00()) || date.isAfter(bouclage.getDate00())) {
						bouclage.setDate14(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate23() == null) {
					if (date.isEqual(bouclage.getDate14()) || date.isAfter(bouclage.getDate14())) {
						bouclage.setDate23(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate29() == null) {
					if (date.isEqual(bouclage.getDate23()) || date.isAfter(bouclage.getDate23())) {
						bouclage.setDate29(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate13() == null) {
					if (date.isEqual(bouclage.getDate29()) || date.isAfter(bouclage.getDate29())) {
						bouclage.setDate13(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate33() == null) {
					if (date.isEqual(bouclage.getDate13()) || date.isAfter(bouclage.getDate13())) {
						bouclage.setDate33(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate03() == null) {
					if (date.isEqual(bouclage.getDate33()) || date.isAfter(bouclage.getDate33())) {
						bouclage.setDate03(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}
				break;

			case visit_type_vs:

				if (bouclage.getDate00() == null) {
					bouclage.setDate00(date);
					// TODO: A ajouter l'auteur, liste déroulante --> bouclage.setAuthor();
					break;
				}

				if (bouclage.getDate14() == null) {
					if (date.isEqual(bouclage.getDate00()) || date.isAfter(bouclage.getDate00())) {
						bouclage.setDate14(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate29() == null) {
					if (date.isEqual(bouclage.getDate14()) || date.isAfter(bouclage.getDate14())) {
						bouclage.setDate29(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate13() == null) {
					if (date.isEqual(bouclage.getDate29()) || date.isAfter(bouclage.getDate29())) {
						bouclage.setDate13(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate33() == null) {
					if (date.isEqual(bouclage.getDate13()) || date.isAfter(bouclage.getDate13())) {
						bouclage.setDate33(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}
				break;

			case visit_type_vd:
			case visit_type_vd_cv:

				if (bouclage.getDate00() == null) {
					bouclage.setDate00(date);
					// TODO: A ajouter l'auteur, liste déroulante --> bouclage.setAuthor();
					break;
				}

				if (bouclage.getDate21() == null) {
					if (date.isEqual(bouclage.getDate00()) || date.isAfter(bouclage.getDate00())) {
						bouclage.setDate21(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate22() == null) {
					if (date.isEqual(bouclage.getDate21()) || date.isAfter(bouclage.getDate21())) {
						bouclage.setDate22(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate23() == null) {
					if (date.isEqual(bouclage.getDate22()) || date.isAfter(bouclage.getDate22())) {
						bouclage.setDate23(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate24() == null) {
					if (date.isEqual(bouclage.getDate23()) || date.isAfter(bouclage.getDate23())) {
						bouclage.setDate24(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate25() == null) {
					if (date.isEqual(bouclage.getDate24()) || date.isAfter(bouclage.getDate24())) {
						bouclage.setDate25(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate26() == null) {
					if (date.isEqual(bouclage.getDate25()) || date.isAfter(bouclage.getDate25())) {
						bouclage.setDate26(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate27() == null) {
					if (date.isEqual(bouclage.getDate26()) || date.isAfter(bouclage.getDate26())) {
						bouclage.setDate27(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate28() == null) {
					if (date.isEqual(bouclage.getDate27()) || date.isAfter(bouclage.getDate27())) {
						bouclage.setDate28(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}
				break;

			case visit_type_va_ot:

				if (bouclage.getDate00() == null) {
					bouclage.setDate00(date);
					// TODO: A ajouter l'auteur, liste déroulante --> bouclage.setAuthor();
					break;
				}

				if (bouclage.getDate01() == null) {
					if (date.isEqual(bouclage.getDate00()) || date.isAfter(bouclage.getDate00())) {
						bouclage.setDate01(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate29() == null) {
					if (date.isEqual(bouclage.getDate01()) || date.isAfter(bouclage.getDate01())) {
						bouclage.setDate29(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate32() == null) {
					if (date.isEqual(bouclage.getDate29()) || date.isAfter(bouclage.getDate29())) {
						bouclage.setDate32(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate13() == null) {
					if (date.isEqual(bouclage.getDate32()) || date.isAfter(bouclage.getDate32())) {
						bouclage.setDate13(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate33() == null) {
					if (date.isEqual(bouclage.getDate13()) || date.isAfter(bouclage.getDate13())) {
						bouclage.setDate33(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}

				if (bouclage.getDate03() == null) {
					if (date.isEqual(bouclage.getDate33()) || date.isAfter(bouclage.getDate33())) {
						bouclage.setDate03(date);
						break;
					} else {
						exceptionBatch("date_posterieure");
						break;
					}
				}
				break;

			default:
				throw new BouclageException("Type de visite non reconnue " + bouclage.getTypeKey());
			}
		} else if (batchType.equals("SUPPRESSION_DATE")) {
			switch (bouclage.getTypeKey()) {
			case visit_type_id:
			case visit_type_id_cv:
				
				if (bouclage.getDate12() != null) {
					bouclage.setDate12(null);
					break;
				}
				
				if (bouclage.getDate11() != null) {
					bouclage.setDate11(null);
					break;
				}
				
				if (bouclage.getDate10() != null) {
					bouclage.setDate10(null);
					break;
				}
				
				if (bouclage.getDate13() != null) {
					bouclage.setDate13(null);
					break;
				}
				
				if (bouclage.getDate07() != null) {
					bouclage.setDate07(null);
					break;
				}
				
				if (bouclage.getDate06() != null) {
					bouclage.setDate06(null);
					break;
				}
				
				if (bouclage.getDate05() != null) {
					bouclage.setDate05(null);
					break;
				}
				
				if (bouclage.getDate04() != null) {
					bouclage.setDate04(null);
					break;
				}
				
				if (bouclage.getDate03() != null) {
					bouclage.setDate03(null);
					break;
				}
				
				if (bouclage.getDate02() != null) {
					bouclage.setDate02(null);
					break;
				}
				
				if (bouclage.getDate01() != null) {
					bouclage.setDate01(null);
					break;
				}
				
				if (bouclage.getDate00() != null) {
					bouclage.setDate00(null);
					break;
				}
				break;

			case visit_type_vd_ot: // TODO: For multiple case in one line --> case visit_type_idi:
				
				if (bouclage.getDate12() != null) {
					bouclage.setDate12(null);
					break;
				}
				
				if (bouclage.getDate11() != null) {
					bouclage.setDate11(null);
					break;
				}
				
				if (bouclage.getDate10() != null) {
					bouclage.setDate10(null);
					break;
				}
				
				if (bouclage.getDate13() != null) {
					bouclage.setDate13(null);
					break;
				}
				
				if (bouclage.getDate30() != null) {
					bouclage.setDate30(null);
					break;
				}
				
				if (bouclage.getDate08() != null) {
					bouclage.setDate08(null);
					break;
				}
				
				if (bouclage.getDate16() != null) {
					bouclage.setDate16(null);
					break;
				}
				
				if (bouclage.getDate15() != null) {
					bouclage.setDate15(null);
					break;
				}
				
				if (bouclage.getDate04() != null) {
					bouclage.setDate04(null);
					break;
				}
				
				if (bouclage.getDate03() != null) {
					bouclage.setDate03(null);
					break;
				}
				
				if (bouclage.getDate02() != null) {
					bouclage.setDate02(null);
					break;
				}
				
				if (bouclage.getDate01() != null) {
					bouclage.setDate01(null);
					break;
				}
				
				if (bouclage.getDate00() != null) {
					bouclage.setDate00(null);
					break;
				}
				break;

			case visit_type_cl_ot: // TODO: For multiple case in one line --> case visit_type_idi:
				
				if (bouclage.getDate12() != null) {
					bouclage.setDate12(null);
					break;
				}
				
				if (bouclage.getDate03() != null) {
					bouclage.setDate03(null);
					break;
				}
				
				if (bouclage.getDate13() != null) {
					bouclage.setDate13(null);
					break;
				}
				
				if (bouclage.getDate32() != null) {
					bouclage.setDate32(null);
					break;
				}
				
				if (bouclage.getDate22() != null) {
					bouclage.setDate22(null);
					break;
				}
				
				if (bouclage.getDate21() != null) {
					bouclage.setDate21(null);
					break;
				}
				
				if (bouclage.getDate31() != null) {
					bouclage.setDate31(null);
					break;
				}
				break;

			case visit_type_vp_ot: // TODO: For multiple case in one line --> case visit_type_idi:
				
				if (bouclage.getDate12() != null) {
					bouclage.setDate12(null);
					break;
				}
				
				if (bouclage.getDate03() != null) {
					bouclage.setDate03(null);
					break;
				}
				
				if (bouclage.getDate13() != null) {
					bouclage.setDate13(null);
					break;
				}
				
				if (bouclage.getDate32() != null) {
					bouclage.setDate32(null);
					break;
				}
				
				if (bouclage.getDate01() != null) {
					bouclage.setDate01(null);
					break;
				}
				
				if (bouclage.getDate00() != null) {
					bouclage.setDate00(null);
					break;
				}
				break;

			case visit_type_idi:
				
				if (bouclage.getDate03() != null) {
					bouclage.setDate03(null);
					break;
				}
				
				if (bouclage.getDate12() != null) {
					bouclage.setDate12(null);
					break;
				}
				
				if (bouclage.getDate13() != null) {
					bouclage.setDate13(null);
					break;
				}
				
				if (bouclage.getDate26() != null) {
					bouclage.setDate26(null);
					break;
				}
				
				if (bouclage.getDate05() != null) {
					bouclage.setDate05(null);
					break;
				}
				
				if (bouclage.getDate04() != null) {
					bouclage.setDate04(null);
					break;
				}
				
				if (bouclage.getDate14() != null) {
					bouclage.setDate14(null);
					break;
				}
				
				if (bouclage.getDate00() != null) {
					bouclage.setDate00(null);
					break;
				}
				break;

			case visit_type_vi_ot:
				
				if (bouclage.getDate03() != null) {
					bouclage.setDate03(null);
					break;
				}
				
				if (bouclage.getDate33() != null) {
					bouclage.setDate33(null);
					break;
				}
				
				if (bouclage.getDate13() != null) {
					bouclage.setDate13(null);
					break;
				}
				
				if (bouclage.getDate29() != null) {
					bouclage.setDate29(null);
					break;
				}
				
				if (bouclage.getDate01() != null) {
					bouclage.setDate01(null);
					break;
				}
				
				if (bouclage.getDate00() != null) {
					bouclage.setDate00(null);
					break;
				}
				break;

			case visit_type_vi:
				
				if (bouclage.getDate03() != null) {
					bouclage.setDate03(null);
					break;
				}
				
				if (bouclage.getDate33() != null) {
					bouclage.setDate33(null);
					break;
				}
				
				if (bouclage.getDate13() != null) {
					bouclage.setDate13(null);
					break;
				}
				
				if (bouclage.getDate29() != null) {
					bouclage.setDate29(null);
					break;
				}
				
				if (bouclage.getDate23() != null) {
					bouclage.setDate23(null);
					break;
				}
				
				if (bouclage.getDate14() != null) {
					bouclage.setDate14(null);
					break;
				}
				
				if (bouclage.getDate00() != null) {
					bouclage.setDate00(null);
					break;
				}
				break;

			case visit_type_vs:
				
				if (bouclage.getDate33() != null) {
					bouclage.setDate33(null);
					break;
				}
				
				if (bouclage.getDate13() != null) {
					bouclage.setDate13(null);
					break;
				}
				
				if (bouclage.getDate29() != null) {
					bouclage.setDate29(null);
					break;
				}
				
				if (bouclage.getDate14() != null) {
					bouclage.setDate14(null);
					break;
				}
				
				if (bouclage.getDate00() != null) {
					bouclage.setDate00(null);
					break;
				}
				break;

			case visit_type_vd:
			case visit_type_vd_cv:

				if (bouclage.getDate28() != null) {
					bouclage.setDate28(null);
					break;
				}
				
				if (bouclage.getDate27() != null) {
					bouclage.setDate27(null);
					break;
				}
				
				if (bouclage.getDate26() != null) {
					bouclage.setDate26(null);
					break;
				}
				
				if (bouclage.getDate25() != null) {
					bouclage.setDate25(null);
					break;
				}
				
				if (bouclage.getDate24() != null) {
					bouclage.setDate24(null);
					break;
				}
				
				if (bouclage.getDate23() != null) {
					bouclage.setDate23(null);
					break;
				}
				
				if (bouclage.getDate22() != null) {
					bouclage.setDate22(null);
					break;
				}
				
				if (bouclage.getDate21() != null) {
					bouclage.setDate21(null);
					break;
				}
				
				if (bouclage.getDate00() != null) {
					bouclage.setDate00(null);
					break;
				}
				break;

			case visit_type_va_ot:
				
				if (bouclage.getDate03() != null) {
					bouclage.setDate03(null);
					break;
				}
				
				if (bouclage.getDate33() != null) {
					bouclage.setDate33(null);
					break;
				}
				
				if (bouclage.getDate13() != null) {
					bouclage.setDate13(null);
					break;
				}
				
				if (bouclage.getDate32() != null) {
					bouclage.setDate32(null);
					break;
				}
				
				if (bouclage.getDate29() != null) {
					bouclage.setDate29(null);
					break;
				}
				
				if (bouclage.getDate01() != null) {
					bouclage.setDate01(null);
					break;
				}
				
				if (bouclage.getDate00() != null) {
					bouclage.setDate00(null);
					break;
				}
				break;

			default:
				throw new BouclageException("Type de visite non reconnue " + bouclage.getTypeKey());
			}
		} else if (batchType.equals("COMPLEMENT_VISITE")) {
			// Do logic
		} else if (batchType.equals("NUMERO_OPERATION")) {
			// Do logic
		} else {
			throw new BouclageException("Unknown BatchType");
		}
	}

	public void deleteBouclage(Integer idToDelete) throws BouclageException {
		Bouclage bouclage = bouclageDao.findById(idToDelete).orElse(null);
		if (bouclage != null) {
			bouclage.setStatut(bouclage.getStatut().equalsIgnoreCase("A") ? "I" : "A");
			bouclageDao.save(bouclage);
		} else {
			throw new BouclageException("Bouclage introuvable.");
		}
	}
}
