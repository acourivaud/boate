package com.boate.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boate.dao.PlanningMaterielDao;
import com.boate.dao.UtilisateurDao;
import com.boate.dao.MaterielDao;
import com.boate.dto.PlanningUtilisateurMaterielDTO;
import com.boate.dto.ReservationCollaborateurDTO;
import com.boate.dto.ReservationMaterielUtilisateurDTO;
import com.boate.dto.UtilisateurDTO;
import com.boate.dto.MaterielDTO;
import com.boate.dto.PlanningCollaborateurDTO;
import com.boate.exception.UtilisateurException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.exception.MaterielException;
import com.boate.model.PlanningMateriel;
import com.boate.model.Utilisateur;
import com.boate.model.Materiel;
import com.boate.model.PlanningCollaborateur;
import com.sun.media.sound.InvalidDataException;

@Service
public class MaterielService {

	@Autowired
	private MaterielDao materielDao;
	
	@Autowired
	private PlanningMaterielDao planningMaterielDao;
	
	@Autowired
	private UtilisateurDao utilisateurDao;
	
	@Autowired
	private UtilisateurService utilisateurService;

	public List<MaterielDTO> getAllMaterielsDTO() {
		return MaterielDTO.toDTOs(materielDao.findAll());
	}
	
	public void createOrUpdate(final MaterielDTO materielDTO)
			throws MaterielException {
		Materiel materiel = null;
		if (materielDTO.getId() != null) {
			materiel = materielDao.findById(materielDTO.getId()).orElse(null);
			if (materiel == null) {
				throw new MaterielException("Matériel introuvable");
			}
		} else {
			materiel = new Materiel();
		}
		Materiel materielByNom = materielDao.findByNom(materielDTO.getNom().trim());
		if (materielByNom != null && materielByNom.getId() != materielDTO.getId()) {
			throw new MaterielException("Un nom de matériel doit être unique.");
		}
		materiel.setNom(materielDTO.getNom().trim());
		materielDao.save(materiel);
	}
	
	public List<MaterielDTO> delete(final MaterielDTO materielDTO) throws Exception {
		Optional<Materiel> materiel = null;
		if (materielDTO.getId() != null) {
			materiel = materielDao.findById(materielDTO.getId());
			if (materiel.isPresent()) {
				try
				{
					materielDao.delete(materiel.get());
				} catch(Exception e) {
					throw new MaterielException("Suppression impossible. Le materiel est probablement utilisé.");
				}
				return getAllMaterielsDTO();
			}
		}
		throw new MaterielException("Le materiel est introuvable");
	}
	
	public PlanningUtilisateurMaterielDTO getPlanningForUser(UtilisateurDTO utilisateurDTO, LocalDateTime dateDebut, LocalDateTime dateFin) {
		return PlanningUtilisateurMaterielDTO.toDTO(planningMaterielDao.findByUtilisateurIdBetweenDates(utilisateurDTO.getIdUtilisateur(), dateDebut, dateFin), dateDebut, dateFin, false);
	}
	
	public PlanningUtilisateurMaterielDTO getPlanningForVehicle(PlanningUtilisateurMaterielDTO planningUtilisateurMaterielDTO) throws Exception {
		planningUtilisateurMaterielDTO.setReservationMaterielUtilisateur(
			ReservationMaterielUtilisateurDTO.toDTOs(
				planningMaterielDao.findByMaterielIdBetweenDates(
						planningUtilisateurMaterielDTO.getMateriel().getId(),
						planningUtilisateurMaterielDTO.getDateDebut().toLocalDateTime(),
						planningUtilisateurMaterielDTO.getDateFin().toLocalDateTime()
				),
				planningUtilisateurMaterielDTO.isTrueIfListOfUsersFalseIfListOfMateriels()
			)
				
		);
		return planningUtilisateurMaterielDTO;
	}
	
	public List<PlanningUtilisateurMaterielDTO> getPlanningForAllMateriel(LocalDateTime dateDebut, LocalDateTime dateFin) throws Exception {
		List<PlanningMateriel> planningMateriels = planningMaterielDao.findBetweenDates(dateDebut, dateFin);
		List<PlanningUtilisateurMaterielDTO> planningUtilisateurMaterielsDTO = new ArrayList<PlanningUtilisateurMaterielDTO>();
		if (planningMateriels != null && planningMateriels.size() > 0) {
			Materiel loopMat = null;
			PlanningUtilisateurMaterielDTO loopPlanning = null;
			int remaining = planningMateriels.size();
			for (PlanningMateriel planningMateriel : planningMateriels) {
				if (loopMat == null || loopMat.getId() != planningMateriel.getMateriel().getId()) {
					if (loopMat != null) {
						planningUtilisateurMaterielsDTO.add(loopPlanning);
					}
					loopMat = planningMateriel.getMateriel();
					loopPlanning = new PlanningUtilisateurMaterielDTO();
					loopPlanning.setTrueIfListOfUsersFalseIfListOfMateriels(true);
					loopPlanning.setMateriel(MaterielDTO.toDTO(loopMat));
					loopPlanning.setReservationMaterielUtilisateur(new ArrayList<ReservationMaterielUtilisateurDTO>());
				}
				loopPlanning.getReservationMaterielUtilisateur().add(ReservationMaterielUtilisateurDTO.toDTO(planningMateriel, true));
				remaining--;
				if (remaining < 1) {
					planningUtilisateurMaterielsDTO.add(loopPlanning);
				}
			}
		}
		return planningUtilisateurMaterielsDTO;
	}
	
	public PlanningUtilisateurMaterielDTO createOrUpdatePlanning(final PlanningUtilisateurMaterielDTO planningUtilisateurMaterielDTO) throws Exception {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		
		boolean isAdmin = connectedUser.hasPermission("Admin Planning Materiel");
		
		PlanningMateriel planningMateriel = null;
		
		// Should only contain one date range element to create or update
		if (planningUtilisateurMaterielDTO.getReservationMaterielUtilisateur() == null ||
				planningUtilisateurMaterielDTO.getReservationMaterielUtilisateur().size() < 1 ||
				planningUtilisateurMaterielDTO.getReservationMaterielUtilisateur().size() > 1)
		{
			throw new MaterielException("Données de réservation incohérentes");
		}
		
		Integer planningMaterielId = planningUtilisateurMaterielDTO.getReservationMaterielUtilisateur().get(0).getIdPlanningMateriel();
		if (planningMaterielId != null) {
			planningMateriel = planningMaterielDao.findById(planningMaterielId).orElse(null);
			if (planningMateriel == null) {
				throw new MaterielException("Réservation de véhicule introuvable");
			}
		} else {
			planningMateriel = new PlanningMateriel();
		}
		
		UtilisateurDTO utilisateurDTO = planningUtilisateurMaterielDTO.getReservationMaterielUtilisateur().get(0).getUtilisateur();
		Utilisateur utilisateur;
		if (utilisateurDTO != null) {
			utilisateur = utilisateurDao.findByIdUtilisateur(utilisateurDTO.getIdUtilisateur());
			if (utilisateur == null) {
				throw new MaterielException("Utilisateur introuvable");
			}
		} else {
			utilisateur = planningMateriel.getUtilisateur();
		}
		
		if (!isAdmin) {
			if (connectedUser.getIdUtilisateur() != utilisateur.getIdUtilisateur()) {
				throw new UtilisateurException("Non autorisé.");
			}
		}
		
		MaterielDTO materielDTO = planningUtilisateurMaterielDTO.getReservationMaterielUtilisateur().get(0).getMateriel();
		Materiel materiel;
		if (materielDTO != null) {
			materiel = materielDao.findById(materielDTO.getId()).orElse(null);
			if (materiel == null) {
				throw new MaterielException("Véhicule introuvable");
			}
		} else {
			materiel = planningMateriel.getMateriel();
		}
		
		LocalDateTime dateDebutReservation = planningUtilisateurMaterielDTO.getReservationMaterielUtilisateur().get(0).getDateDebut().toLocalDateTime();
		LocalDateTime dateFinReservation = planningUtilisateurMaterielDTO.getReservationMaterielUtilisateur().get(0).getDateFin().toLocalDateTime();
		
		List<PlanningMateriel> planningsMaterielsByMateriel = planningMaterielDao.findByMaterielIdBetweenDates(materielDTO.getId(), dateDebutReservation, dateFinReservation);
		planningUtilisateurMaterielDTO.setTrueIfListOfUsersFalseIfListOfMateriels(true);
		planningUtilisateurMaterielDTO.setReservationMaterielUtilisateur(new ArrayList<ReservationMaterielUtilisateurDTO>());
		if (planningsMaterielsByMateriel != null && planningsMaterielsByMateriel.size() > 0)
		{
			for (PlanningMateriel planningMaterielByMateriel : planningsMaterielsByMateriel)
			{
				if (planningMaterielByMateriel.getId() != planningMaterielId)
				{
					planningUtilisateurMaterielDTO.setConflictOnMaterielResponse(true);
					planningUtilisateurMaterielDTO.getReservationMaterielUtilisateur().add(ReservationMaterielUtilisateurDTO.toDTO(planningMaterielByMateriel, true));
				}
			}
		}
		
		if (!planningUtilisateurMaterielDTO.isForceUpdateIfPossible() && !planningUtilisateurMaterielDTO.isConflictOnMaterielResponse())
		{
			List<PlanningMateriel> planningsMaterielsByUtilisateur = planningMaterielDao.findByUtilisateurIdBetweenDates(utilisateurDTO.getIdUtilisateur(), dateDebutReservation, dateFinReservation);
			planningUtilisateurMaterielDTO.setTrueIfListOfUsersFalseIfListOfMateriels(false);
			planningUtilisateurMaterielDTO.setReservationMaterielUtilisateur(new ArrayList<ReservationMaterielUtilisateurDTO>());
			if (planningsMaterielsByUtilisateur != null && planningsMaterielsByUtilisateur.size() > 0)
			{
				for (PlanningMateriel planningMaterielByUtilisateur : planningsMaterielsByUtilisateur)
				{
					if (planningMaterielByUtilisateur.getId() != planningMaterielId)
					{
						planningUtilisateurMaterielDTO.setConflictOnUserResponse(true);
						planningUtilisateurMaterielDTO.getReservationMaterielUtilisateur().add(ReservationMaterielUtilisateurDTO.toDTO(planningMaterielByUtilisateur, false));
					}
				}
			}
		}
		
		// If all controls went OK then save and return the new entire list of results
		if (!planningUtilisateurMaterielDTO.isConflictOnMaterielResponse() && !planningUtilisateurMaterielDTO.isConflictOnUserResponse())
		{
			planningMateriel.setUtilisateur(utilisateur);
			planningMateriel.setMateriel(materiel);
			planningMateriel.setDateDebut(dateDebutReservation);
			planningMateriel.setDateFin(dateFinReservation);
			planningMaterielDao.save(planningMateriel);
			List<PlanningMateriel> planningsMateriels;
			if (planningUtilisateurMaterielDTO.isTrueIfListOfUsersFalseIfListOfMateriels())
			{
				planningsMateriels = planningMaterielDao.findByMaterielIdBetweenDates(
						materiel.getId(),
						planningUtilisateurMaterielDTO.getDateDebut().toLocalDateTime(),
						planningUtilisateurMaterielDTO.getDateFin().toLocalDateTime());
			}
			else
			{
				planningsMateriels = planningMaterielDao.findByUtilisateurIdBetweenDates(
						utilisateur.getIdUtilisateur(),
						planningUtilisateurMaterielDTO.getDateDebut().toLocalDateTime(),
						planningUtilisateurMaterielDTO.getDateFin().toLocalDateTime());
			}
			planningUtilisateurMaterielDTO.setReservationMaterielUtilisateur(ReservationMaterielUtilisateurDTO.toDTOs(planningsMateriels, planningUtilisateurMaterielDTO.isTrueIfListOfUsersFalseIfListOfMateriels()));
			return planningUtilisateurMaterielDTO;
		}
		
		return planningUtilisateurMaterielDTO;
	}
	
	public PlanningUtilisateurMaterielDTO deletePlanning(final PlanningUtilisateurMaterielDTO planningUtilisateurMaterielDTO) throws Exception {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		
		boolean isAdmin = connectedUser.hasPermission("Admin Planning Materiel");
		
		if (!isAdmin) {
			if (connectedUser.getIdUtilisateur() != planningUtilisateurMaterielDTO.getUtilisateur().getIdUtilisateur()) {
				throw new UtilisateurException("Non autorisé.");
			}
		}
		
		List<PlanningMateriel> planningsMateriels = null;
		if (planningUtilisateurMaterielDTO.getReservationMaterielUtilisateur() != null && planningUtilisateurMaterielDTO.getReservationMaterielUtilisateur().size() > 0) {
			List<Integer> ids = new ArrayList<>();
			for (ReservationMaterielUtilisateurDTO reservation : planningUtilisateurMaterielDTO.getReservationMaterielUtilisateur())
			{
				ids.add(reservation.getIdPlanningMateriel());
			}
			planningsMateriels = planningMaterielDao.findByIdInList(ids);
			if (planningsMateriels != null && planningsMateriels.size() > 0)
			{
				planningMaterielDao.deleteAll(planningsMateriels);
			}
		}
		
		if (planningUtilisateurMaterielDTO.isTrueIfListOfUsersFalseIfListOfMateriels())
		{
			MaterielDTO materielDTO = planningUtilisateurMaterielDTO.getMateriel();
			planningsMateriels = planningMaterielDao.findByMaterielIdBetweenDates(
					materielDTO.getId(),
					planningUtilisateurMaterielDTO.getDateDebut().toLocalDateTime(),
					planningUtilisateurMaterielDTO.getDateFin().toLocalDateTime());
		}
		else
		{
			UtilisateurDTO utilisateurDTO = planningUtilisateurMaterielDTO.getUtilisateur();
			planningsMateriels = planningMaterielDao.findByUtilisateurIdBetweenDates(
					utilisateurDTO.getIdUtilisateur(),
					planningUtilisateurMaterielDTO.getDateDebut().toLocalDateTime(),
					planningUtilisateurMaterielDTO.getDateFin().toLocalDateTime());
		}
		planningUtilisateurMaterielDTO.setReservationMaterielUtilisateur(ReservationMaterielUtilisateurDTO.toDTOs(planningsMateriels, planningUtilisateurMaterielDTO.isTrueIfListOfUsersFalseIfListOfMateriels()));
		return planningUtilisateurMaterielDTO;
	}
}
