package com.boate.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boate.dao.UtilisateurDao;
import com.boate.model.Utilisateur;

@Service
public class AuthentificationService {

	@Autowired
	private UtilisateurDao utilisateurDao;

	public Optional<Utilisateur> findIndividuBeanByLogin(String login) {
		return utilisateurDao.findByLogin(login);
	}
	
	public Optional<Utilisateur> findById(Integer id) {
		return utilisateurDao.findById(id);
	}
}
