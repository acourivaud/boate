package com.boate.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.boate.dao.CategorieDao;
import com.boate.dao.CommuneDao;
import com.boate.dao.DepartementDao;
import com.boate.dao.GeometrieDao;
import com.boate.dao.GeometrieTypeDao;
import com.boate.dao.InfrapoleDao;
import com.boate.dao.LigneDao;
import com.boate.dao.LigneSegmentDao;
import com.boate.dao.OuvrageDao;
import com.boate.dao.ProprietaireDao;
import com.boate.dao.RegionDao;
import com.boate.dao.SecteurDao;
import com.boate.dao.SurveillanceDao;
import com.boate.dao.TypeDao;
import com.boate.dao.UniteOperationnelleDao;
import com.boate.dao.UniteOperationnelleLigneDao;
import com.boate.dao.UtilisateurDao;
import com.boate.dao.UtilisateurFilterDao;
import com.boate.dto.CategorieDTO;
import com.boate.dto.CommuneDTO;
import com.boate.dto.DTO;
import com.boate.dto.DepartementDTO;
import com.boate.dto.GeometrieDTO;
import com.boate.dto.GeometrieTypeDTO;
import com.boate.dto.InfrapoleDTO;
import com.boate.dto.LigneDTO;
import com.boate.dto.OuvrageDTO;
import com.boate.dto.ParamValueDTO;
import com.boate.dto.PortionDTO;
import com.boate.dto.ProprietaireDTO;
import com.boate.dto.RegionDTO;
import com.boate.dto.SecteurDTO;
import com.boate.dto.TypeDTO;
import com.boate.dto.UniteOperationnelleDTO;
import com.boate.dto.UtilisateurDTO;
import com.boate.enums.FonctionUtilisateurEnum;
import com.boate.exception.FilterException;
import com.boate.exception.UtilisateurException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.model.Categorie;
import com.boate.model.Commune;
import com.boate.model.Geometrie;
import com.boate.model.GeometrieType;
import com.boate.model.Infrapole;
import com.boate.model.Ligne;
import com.boate.model.Region;
import com.boate.model.Secteur;
import com.boate.model.Type;
import com.boate.model.UniteOperationnelle;
import com.boate.model.UniteOperationnelleLigne;
import com.boate.model.Utilisateur;
import com.boate.model.UtilisateurFilter;

@Service
public class FilterService {
	
	@Autowired
	UtilisateurService utilisateurService;
	
	@Autowired
	private UtilisateurFilterDao utilisateurFilterDao;
	
	@Autowired
	private UtilisateurDao utilisateurDao;
	
	@Autowired
	private RegionDao regionDao;
	
	@Autowired
	private DepartementDao departementDao;
	
	@Autowired
	private CommuneDao communeDao;
	
	@Autowired
	private InfrapoleDao infrapoleDao;
	
	@Autowired
	private SecteurDao secteurDao;
	
	@Autowired
	private UniteOperationnelleDao uniteOperationnelleDao;
	
	@Autowired
	private UniteOperationnelleLigneDao uniteOperationnelleLigneDao;
	
	@Autowired
	private LigneDao ligneDao;
	
	@Autowired
	private ProprietaireDao proprietaireDao;
	
	@Autowired
	private OuvrageDao ouvrageDao;
	
	@Autowired
	private LigneSegmentDao ligneSegmentDao;
	
	@Autowired
	private SurveillanceDao surveillanceDao;
	
	@Autowired
	private TypeDao typeDao;
	
	@Autowired
	private CategorieDao categorieDao;
	
	@Autowired
	private GeometrieDao geometrieDao;
	
	@Autowired
	private GeometrieTypeDao geometrieTypeDao;

	public List<ParamValueDTO> getFiltersForConnectedUser(String page) throws UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		List<UtilisateurFilter> utilisateurFilters = utilisateurFilterDao.findByUtilisateurAndPage(connectedUser, page);
		return ParamValueDTO.toDTOs(utilisateurFilters);
	}
	
	public void saveFiltersForConnectedUser(String page, List<ParamValueDTO> paramsValues) throws UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		List<UtilisateurFilter> utilisateurFilters = utilisateurFilterDao.findByUtilisateurAndPage(connectedUser, page);
		if (utilisateurFilters != null & utilisateurFilters.size() > 0) {
			List<Integer> ids = new ArrayList<>();
			for (UtilisateurFilter utilisateurFilter : utilisateurFilters) {
				ids.add(utilisateurFilter.getId());
			}
			utilisateurFilterDao.deleteWhereUtilisateurFilterIdInList(ids);
		}
		if (paramsValues != null && paramsValues.size() > 0) {
			utilisateurFilters = UtilisateurFilter.toUtilisateurFilters(connectedUser, page, paramsValues);
			utilisateurFilterDao.saveAll(utilisateurFilters);
		}
	}
	
	public void dropAllFiltersForConnectedUser() throws UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		List<UtilisateurFilter> utilisateurFilters = utilisateurFilterDao.findByUtilisateur(connectedUser);
		if (utilisateurFilters != null & utilisateurFilters.size() > 0) {
			utilisateurFilterDao.deleteAll(utilisateurFilters);
		}
	}
	
	public List<RegionDTO> getRegions() {
		return RegionDTO.toDTOs(regionDao.findAllByOrderByNomAsc());
	}
	
	public void saveRegion(RegionDTO regionDTO) throws UtilisateurNotConnectedException, FilterException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Region region = null;
		if (regionDTO.getId() != null) {
			region = regionDao.findById(regionDTO.getId()).get();
		}
		if (region == null) {
			region = new Region();
		}
		region.setCode(regionDTO.getCode());
		List<Region> regionForNameChecking = regionDao.findByNom(regionDTO.getName());
		if (regionForNameChecking != null && regionForNameChecking.size() > 0) {
			for (Region regionChecked : regionForNameChecking) {
				if (region.getId() == null || regionChecked.getId() != region.getId()) {
					throw new FilterException("Enregistrement impossible. Le nom doit être unique.");
				}
			}
		}
		List<Region> regionForCodeChecking = regionDao.findByCode(regionDTO.getCode());
		if (regionForCodeChecking != null && regionForCodeChecking.size() > 0) {
			for (Region regionChecked : regionForCodeChecking) {
				if (region.getId() == null || regionChecked.getId() != region.getId()) {
					throw new FilterException("Enregistrement impossible. Le code doit être unique.");
				}
			}
		}
		region.setNom(regionDTO.getName());
		regionDao.save(region);
	}
	
	public void deleteRegion(Integer regionId) throws FilterException, UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Region region = regionDao.findById(regionId).get();
		if (region == null) {
			throw new FilterException("Région introuvable.");
		}
		List<Infrapole> infrapoles = infrapoleDao.findByRegionId(regionId);
		if (infrapoles != null && infrapoles.size() > 0) {
			for (Infrapole infrapole : infrapoles) {
				deleteInfrapole(infrapole.getId(), false);
			}
		}
		try {
			regionDao.delete(region);
		} catch (Exception e) {
			throw new FilterException("Suppression impossible de la Région : " + region.getNom() + " - (" + region.getCode() + "). Celle-ci est probablement utilisée.");
		}
	}
	
	public List<InfrapoleDTO> getInfrapoles(String id) {
		List<Infrapole> infrapoles = infrapoleDao.findByRegionId(Integer.parseUnsignedInt(id));
		return InfrapoleDTO.toDTOs(infrapoles, false);
	}
	
	public void saveInfrapole(InfrapoleDTO infrapoleDTO) throws UtilisateurNotConnectedException, FilterException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Infrapole infrapole = null;
		if (infrapoleDTO.getId() != null) {
			infrapole = infrapoleDao.findById(infrapoleDTO.getId()).get();
		}
		if (infrapole == null) {
			infrapole = new Infrapole();
		}
		infrapole.setCode(infrapoleDTO.getCode());
		Region region = regionDao.findById(infrapoleDTO.getRegion().getId()).get();
		if (region == null) {
			throw new FilterException("Region introuvable.");
		}
		infrapole.setRegion(region);
		List<Infrapole> infrapoleForNameChecking = infrapoleDao.findByNom(infrapoleDTO.getName());
		if (infrapoleForNameChecking != null && infrapoleForNameChecking.size() > 0) {
			for (Infrapole infrapoleChecked : infrapoleForNameChecking) {
				if (infrapole.getId() == null || infrapoleChecked.getId() != infrapole.getId()) {
					throw new FilterException("Enregistrement impossible. Le nom doit être unique.");
				}
			}
		}
		List<Infrapole> infrapoleForCodeChecking = infrapoleDao.findByCode(infrapoleDTO.getCode());
		if (infrapoleForCodeChecking != null && infrapoleForCodeChecking.size() > 0) {
			for (Infrapole infrapoleChecked : infrapoleForCodeChecking) {
				if (infrapole.getId() == null || infrapoleChecked.getId() != infrapole.getId()) {
					throw new FilterException("Enregistrement impossible. Le code doit être unique.");
				}
			}
		}
		infrapole.setNom(infrapoleDTO.getName());
		if (infrapoleDTO.getId() != null && infrapole.getUsesSecteurSubLevel() != infrapoleDTO.getUsesSecteurSubLevel()) {
			deleteInfrapole(infrapole.getId(), true);
		}
		infrapole.setUsesSecteurSubLevel(infrapoleDTO.getUsesSecteurSubLevel());
		infrapoleDao.save(infrapole);
	}
	
	public void deleteInfrapole(Integer infrapoleId, Boolean deleteOnlySubObjects) throws FilterException, UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Infrapole infrapole = infrapoleDao.findById(infrapoleId).get();
		if (infrapole == null) {
			throw new FilterException("Infrapole introuvable.");
		}
		List<UniteOperationnelle> uos = uniteOperationnelleDao.findByInfrapoleId(infrapoleId);
		if (uos != null && uos.size() > 0) {
			for (UniteOperationnelle uo : uos) {
				deleteUO(uo.getId());
			}
		}
		List<Secteur> secteurs = secteurDao.findByInfrapoleId(infrapoleId);
		if (secteurs != null && secteurs.size() > 0) {
			for (Secteur secteur : secteurs) {
				deleteSecteur(secteur.getId());
			}
		}
		if (deleteOnlySubObjects == null || deleteOnlySubObjects == false) {
			try {
				infrapoleDao.delete(infrapole);
			} catch (Exception e) {
				throw new FilterException("Suppression impossible de l'Infrapole : " + infrapole.getNom() + " - (" + infrapole.getCode() + "). Celui-ci est probablement utilisé.");
			}
		}
	}
	
	public List<SecteurDTO> getSecteurs(String id) {
		List<Secteur> secteurs = secteurDao.findByInfrapoleId(Integer.parseUnsignedInt(id));
		return SecteurDTO.toDTOs(secteurs, false, false);
	}
	
	public void saveSecteur(SecteurDTO secteurDTO) throws UtilisateurNotConnectedException, FilterException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Secteur secteur = null;
		if (secteurDTO.getId() != null) {
			secteur = secteurDao.findById(secteurDTO.getId()).get();
		}
		if (secteur == null) {
			secteur = new Secteur();
		}
		List<Secteur> secteurForNameChecking = secteurDao.findByNom(secteurDTO.getNom());
		if (secteurForNameChecking != null && secteurForNameChecking.size() > 0) {
			for (Secteur secteurChecked : secteurForNameChecking) {
				if (secteur.getId() == null || secteurChecked.getId() != secteur.getId()) {
					throw new FilterException("Enregistrement impossible. Le nom doit être unique.");
				}
			}
		}
		secteur.setNom(secteurDTO.getNom());
		Infrapole infrapole = infrapoleDao.findById(secteurDTO.getInfrapole().getId()).get();
		if (infrapole == null) {
			throw new FilterException("Infrapole introuvable.");
		}
		secteur.setInfrapole(infrapole);
		secteurDao.save(secteur);
	}
	
	public void deleteSecteur(Integer secteurId) throws FilterException, UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Secteur secteur = secteurDao.findById(secteurId).get();
		if (secteur == null) {
			throw new FilterException("Secteur introuvable.");
		}
		List<UniteOperationnelle> uos = uniteOperationnelleDao.findBySecteurId(secteurId);
		if (uos != null && uos.size() > 0) {
			for (UniteOperationnelle uo : uos) {
				deleteUO(uo.getId());
			}
		}
		try {
			secteurDao.delete(secteur);
		} catch (Exception e) {
			throw new FilterException("Suppression impossible du Secteur : " + secteur.getNom() + ". Celui-ci est probablement utilisé.");
		}
	}
	
	public List<UniteOperationnelleDTO> getUOs(String secteurId, String infrapoleId) {
		List<UniteOperationnelle> unitesOperationnelle = null;
		if (secteurId != null) {
			unitesOperationnelle = uniteOperationnelleDao.findBySecteurId(Integer.parseUnsignedInt(secteurId));
		} else {
			unitesOperationnelle = uniteOperationnelleDao.findByInfrapoleId(Integer.parseUnsignedInt(infrapoleId));
		}
		return UniteOperationnelleDTO.toDTOs(unitesOperationnelle, false, false, false);
	}
	
	public void saveUO(UniteOperationnelleDTO uoDTO) throws UtilisateurNotConnectedException, FilterException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		UniteOperationnelle uo = null;
		if (uoDTO.getId() != null) {
			uo = uniteOperationnelleDao.findById(uoDTO.getId()).get();
		}
		if (uo == null) {
			uo = new UniteOperationnelle();
		}
		uo.setCode(uoDTO.getCode());
		List<UniteOperationnelle> uoForNameChecking = uniteOperationnelleDao.findByName(uoDTO.getName());
		if (uoForNameChecking != null && uoForNameChecking.size() > 0) {
			for (UniteOperationnelle uoChecked : uoForNameChecking) {
				if (uo.getId() == null || uoChecked.getId() != uo.getId()) {
					throw new FilterException("Enregistrement impossible. Le nom doit être unique.");
				}
			}
		}
		List<UniteOperationnelle> uoForCodeChecking = uniteOperationnelleDao.findByCode(uoDTO.getCode());
		if (uoForCodeChecking != null && uoForCodeChecking.size() > 0) {
			for (UniteOperationnelle uoChecked : uoForCodeChecking) {
				if (uo.getId() == null || uoChecked.getId() != uo.getId()) {
					throw new FilterException("Enregistrement impossible. Le code doit être unique.");
				}
			}
		}
		uo.setName(uoDTO.getName());
		Infrapole infrapole = null;
		if (uoDTO.getInfrapole() != null) {
			infrapole = infrapoleDao.findById(uoDTO.getInfrapole().getId()).get();
			if (infrapole == null) {
				throw new FilterException("Infrapole introuvable.");
			}
			uo.setInfrapole(infrapole);
		}
		Secteur secteur = null;
		if (uoDTO.getSecteur() != null) {
			secteur = secteurDao.findById(uoDTO.getSecteur().getId()).get();
			if (secteur == null) {
				throw new FilterException("Secteur introuvable.");
			}
			if (uo.getInfrapole() != null) {
				throw new FilterException("Soit Infrapole, soit Secteur. Pas les deux.");
			}
			uo.setSecteur(secteur);
		}
		if (secteur == null && infrapole == null) {
			throw new FilterException("Secteur ou Infrapole introuvable.");
		}
		uniteOperationnelleDao.save(uo);
	}
	
	public void deleteUO(Integer uoId) throws FilterException, UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		UniteOperationnelle uniteOperationnelle = uniteOperationnelleDao.findById(uoId).get();
		if (uniteOperationnelle == null) {
			throw new FilterException("Unité d'Operationnelle introuvable.");
		}
		List<UniteOperationnelleLigne> unitesOperationnelleLignes = uniteOperationnelleLigneDao.findByUniteOperationnelle(uniteOperationnelle);
		if (unitesOperationnelleLignes != null && unitesOperationnelleLignes.size() > 0) {
			for (UniteOperationnelleLigne uol : unitesOperationnelleLignes) {
				PortionDTO portionDTO = PortionDTO.toDTO(uol);
				deletePortion(portionDTO);
			}
		}
		try {
			uniteOperationnelleDao.delete(uniteOperationnelle);
		} catch (Exception e) {
			throw new FilterException("Suppression impossible de l'Unité d'Operationnelle : " + uniteOperationnelle.getName() + " - (" + uniteOperationnelle.getCode() + "). Celle-ci est probablement utilisée.");
		}
	}
	
	public List<LigneDTO> getLignes(List<ParamValueDTO> requestFilters) {
		Integer first = 0;
		Integer rows = 10;
		for (ParamValueDTO paramValue : requestFilters) {
			if (paramValue.getParam().equals("first")) {
				first = Integer.parseInt(paramValue.getValue());
			}
			if (paramValue.getParam().equals("rows")) {
				rows = Integer.parseInt(paramValue.getValue());
			}
		}
		Integer page = first / rows;
		Pageable pageable = PageRequest.of(page, rows, Sort.by(Direction.ASC, "name"));
		Page<Ligne> lignesPage = ligneDao.findAll(pageable);
		List<Ligne> lignes = lignesPage.getContent();
		List<LigneDTO> lignesDTO = LigneDTO.toDTOs(lignes);
		return lignesDTO;
	}
	
	public List<PortionDTO> generatePortionsForLigne(Integer ligneId) throws FilterException {
		Ligne ligne = ligneDao.findById(ligneId).get();
		if (ligne == null) {
			throw new FilterException("Ligne introuvable.");
		}
		Double pkDebutDoubleLigne = ligne.getBeginPk() != null ? Double.parseDouble(ligne.getBeginPk().replace(",", ".")) : null;
		Double pkFinDoubleLigne = ligne.getEndPk() != null ? Double.parseDouble(ligne.getEndPk().replace(",", ".")) : null;
		List<UniteOperationnelleLigne> portions = uniteOperationnelleLigneDao.findByLigne(ligne);
		List<PortionDTO> portionDTOs = new ArrayList<>();
		if (portions != null && portions.size() > 0) {
			Double lastElementDouble = pkDebutDoubleLigne;
			String lastElement = ligne.getBeginPk();
			for (UniteOperationnelleLigne uol : portions) {
				Double pkDebutDoubleUol = uol.getPkDebut() != null ? Double.parseDouble(uol.getPkDebut().replace(",", ".")) : null;
				Double pkFinDoubleUol = uol.getPkFin() != null ? Double.parseDouble(uol.getPkFin().replace(",", ".")) : null;
				if (lastElementDouble < pkDebutDoubleUol) {
					PortionDTO portionDTO = new PortionDTO();
					portionDTO.setPkDebut(lastElement);
					portionDTO.setPkFin(uol.getPkDebut());
					portionDTOs.add(portionDTO);
				}
				PortionDTO portionDTO = new PortionDTO();
				portionDTO.setId(uol.getId());
				portionDTO.setUo(UniteOperationnelleDTO.toDTO(uol.getUniteOperationnelle(), false, false, false));
				portionDTO.setPkDebut(uol.getPkDebut());
				portionDTO.setPkFin(uol.getPkFin());
				portionDTO.setVoie(uol.getVoie());
				portionDTOs.add(portionDTO);
				lastElementDouble = pkFinDoubleUol;
				lastElement = uol.getPkFin();
			}
			String lastPkOfTheList = portionDTOs.get(portionDTOs.size()-1).getPkFin();
			Double lastPkOfTheListDouble = lastPkOfTheList != null ? Double.parseDouble(lastPkOfTheList.replace(",", ".")) : null; 
			if (lastPkOfTheListDouble < pkFinDoubleLigne) {
				PortionDTO portionDTO = new PortionDTO();
				portionDTO.setPkDebut(lastPkOfTheList);
				portionDTO.setPkFin(ligne.getEndPk());
				portionDTOs.add(portionDTO);
			}
		} else {
			PortionDTO portionDTO = new PortionDTO();
			portionDTO.setPkDebut(ligne.getBeginPk());
			portionDTO.setPkFin(ligne.getEndPk());
			portionDTOs.add(portionDTO);
		}
		return portionDTOs;
	}
	
	public Long getLignesCount() {
		Long count = ligneDao.count();
		return count;
	}
	
	public void saveLigne(LigneDTO ligneDTO) throws UtilisateurNotConnectedException, FilterException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Ligne ligne = null;
		if (ligneDTO.getId() != null) {
			ligne = ligneDao.findById(ligneDTO.getId()).get();
		}
		if (ligne == null) {
			ligne = new Ligne();
		}
		List<Ligne> ligneForNameChecking = ligneDao.findByName(ligneDTO.getNom());
		if (ligneForNameChecking != null && ligneForNameChecking.size() > 0) {
			for (Ligne ligneChecked : ligneForNameChecking) {
				if (ligne.getId() == null || ligneChecked.getId() != ligne.getId()) {
					throw new FilterException("Enregistrement impossible. Le nom doit être unique.");
				}
			}
		}
		ligne.setName(ligneDTO.getNom());
		List<Ligne> ligneForCodeChecking = ligneDao.findByCode(ligneDTO.getCode());
		if (ligneForCodeChecking != null && ligneForCodeChecking.size() > 0) {
			for (Ligne ligneChecked : ligneForCodeChecking) {
				if (ligne.getId() == null || ligneChecked.getId() != ligne.getId()) {
					throw new FilterException("Enregistrement impossible. Le code doit être unique.");
				}
			}
		}
		ligne.setCode(ligneDTO.getCode());
		ligne.setBeginPk(ligneDTO.getPkDebut());
		ligne.setEndPk(ligneDTO.getPkFin());
		ligneDao.save(ligne);
	}
	
	public List<LigneDTO> getLignesLike(String likeClause) throws FilterException {
		List<Ligne> lignes = ligneDao.findAllByNameLikeOrCodeEquals(likeClause, PageRequest.of(0,10));
		List<LigneDTO> lignesDTO = LigneDTO.toDTOs(lignes);
		return lignesDTO;
	}
	
	public void deleteLigne(LigneDTO ligneDTO) throws FilterException, UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Ligne ligne = ligneDao.findById(ligneDTO.getId()).get();
		if (ligne == null) {
			throw new FilterException("Ligne introuvable.");
		}
		try {
			ligneDao.delete(ligne);
		} catch (Exception e) {
			throw new FilterException("Suppression impossible de la Ligne : " + ligne.getName() + " - (" + ligne.getCode() + "). Celle-ci est probablement utilisée.");
		}
	}
	
	public List<PortionDTO> getPortions(String id) throws FilterException {
		UniteOperationnelle uo = uniteOperationnelleDao.findById(Integer.parseUnsignedInt(id)).get();
		if (uo == null) {
			throw new FilterException("Unité d'Operationnelle introuvable.");
		}
		List<Integer> ids = new ArrayList<>(1);
		ids.add(Integer.parseUnsignedInt(id));
		List<UniteOperationnelleLigne> portions = uniteOperationnelleLigneDao.findByUniteOperationnelleIdInList(ids);
		List<PortionDTO> portionDTOs = PortionDTO.toDTOs(portions);
		return portionDTOs;
	}
	
	public void savePortion(PortionDTO portionDTO) throws UtilisateurNotConnectedException, FilterException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		UniteOperationnelleLigne uol = null;
		if (portionDTO.getId() != null) {
			uol = uniteOperationnelleLigneDao.findById(portionDTO.getId()).get();
		}
		if (uol == null) {
			uol = new UniteOperationnelleLigne();
		}
		if (portionDTO.getUo() == null) {
			throw new FilterException("Unité Opérationnelle introuvable.");
		}
		UniteOperationnelle uo = uniteOperationnelleDao.findById(portionDTO.getUo().getId()).get();
		if (uo == null) {
			throw new FilterException("Unité Opérationnelle introuvable.");
		}
		uol.setUniteOperationnelle(uo);
		if (portionDTO.getLigne() == null) {
			throw new FilterException("Ligne introuvable.");
		}
		Ligne ligne = ligneDao.findById(portionDTO.getLigne().getId()).get();
		if (ligne == null) {
			throw new FilterException("Ligne introuvable.");
		}
		uol.setLigne(ligne);
		if (portionDTO.getPkDebut() == null || portionDTO.getPkFin() == null ) {
			throw new FilterException("Les PKs Début et Fin sont obligatoires.");
		}
		Double pkDebutDoublePortion = portionDTO.getPkDebut() != null ? Double.parseDouble(portionDTO.getPkDebut().replace(",", ".")) : null;
		Double pkFinDoublePortion = portionDTO.getPkFin() != null ? Double.parseDouble(portionDTO.getPkFin().replace(",", ".")) : null;
		if (pkDebutDoublePortion >= pkFinDoublePortion) {
			throw new FilterException("La plage de PKs renseignée est incohérente. Le PK Fin doit être supérieur au PK Début.");
		}
		if (!isPkPortionOkForSaving(ligne, portionDTO)) {
			throw new FilterException("La plage de PKs renseignée est déjà attribuée pour cette Ligne.");
		}
		uol.setPkDebut(portionDTO.getPkDebut());
		uol.setPkFin(portionDTO.getPkFin());
		uol.setVoie(portionDTO.getVoie());
		uniteOperationnelleLigneDao.save(uol);
	}
	
	private boolean isPkPortionOkForSaving(Ligne ligne, PortionDTO portion) {
		Double pkDebutDoubleLigne = ligne.getBeginPk() != null ? Double.parseDouble(ligne.getBeginPk().replace(",", ".")) : null;
		Double pkFinDoubleLigne = ligne.getEndPk() != null ? Double.parseDouble(ligne.getEndPk().replace(",", ".")) : null;
		Double pkDebutDoublePortion = portion.getPkDebut() != null ? Double.parseDouble(portion.getPkDebut().replace(",", ".")) : null;
		Double pkFinDoublePortion = portion.getPkFin() != null ? Double.parseDouble(portion.getPkFin().replace(",", ".")) : null;
		if (pkDebutDoublePortion < pkDebutDoubleLigne || pkFinDoublePortion > pkFinDoubleLigne) {
			return false;
		}
		List<UniteOperationnelleLigne> uols = uniteOperationnelleLigneDao.findByLigne(ligne);
		if (uols != null && uols.size() > 0) {
			for (UniteOperationnelleLigne uol : uols) {
				Double pkDebutDoubleUol = uol.getPkDebut() != null ? Double.parseDouble(uol.getPkDebut().replace(",", ".")) : null;
				Double pkFinDoubleUol = uol.getPkFin() != null ? Double.parseDouble(uol.getPkFin().replace(",", ".")) : null;
				if ((pkDebutDoublePortion < pkFinDoubleUol && pkDebutDoublePortion > pkDebutDoubleUol) || (pkFinDoublePortion < pkFinDoubleUol && pkFinDoublePortion > pkDebutDoubleUol)) {
					if (portion.getId() == null) {
						return false;
					} else {
						if (portion.getId() != uol.getId()) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}
	
	public void deletePortion(PortionDTO portionDTO) throws FilterException, UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		UniteOperationnelleLigne uniteOperationnelleLigne = uniteOperationnelleLigneDao.findById(portionDTO.getId()).get();
		if (uniteOperationnelleLigne == null) {
			throw new FilterException("Unité d'Operationnelle introuvable.");
		}
		try {
			uniteOperationnelleLigneDao.delete(uniteOperationnelleLigne);
		} catch (Exception e) {
			throw new FilterException("Suppression impossible de la Portion : " + uniteOperationnelleLigne.getLigne().getCode() + " - " + uniteOperationnelleLigne.getLigne().getName() + " - [" + uniteOperationnelleLigne.getPkDebut() + " - " + uniteOperationnelleLigne.getPkFin() + "]. Celle-ci est probablement utilisée.");
		}
	}
	
	public List<GeometrieDTO> getGeometries(List<ParamValueDTO> requestFilters) {
		Integer first = 0;
		Integer rows = 10;
		for (ParamValueDTO paramValue : requestFilters) {
			if (paramValue.getParam().equals("first")) {
				first = Integer.parseInt(paramValue.getValue());
			}
			if (paramValue.getParam().equals("rows")) {
				rows = Integer.parseInt(paramValue.getValue());
			}
		}
		Integer page = first / rows;
		Pageable pageable = PageRequest.of(page, rows, Sort.by(Direction.ASC, "libelle"));
		Page<Geometrie> geometriesPage = geometrieDao.findAll(pageable);
		List<Geometrie> geometries = geometriesPage.getContent();
		List<GeometrieDTO> geometriesDTO = GeometrieDTO.toDTOs(geometries);
		return geometriesDTO;
	}
	
	public Long getGeometriesCount() {
		Long count = geometrieDao.count();
		return count;
	}
	
	public void saveGeometrie(GeometrieDTO geometrieDTO) throws UtilisateurNotConnectedException, FilterException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Geometrie geometrie = null;
		if (geometrieDTO.getId() != null) {
			geometrie = geometrieDao.findById(geometrieDTO.getId()).get();
		}
		if (geometrie == null) {
			geometrie = new Geometrie();
		}
		List<Geometrie> geometrieForNameChecking = geometrieDao.findByLibelle(geometrieDTO.getLibelle());
		if (geometrieForNameChecking != null && geometrieForNameChecking.size() > 0) {
			for (Geometrie geometrieChecked : geometrieForNameChecking) {
				if (geometrie.getId() == null || geometrieChecked.getId() != geometrie.getId()) {
					throw new FilterException("Enregistrement impossible. Le libellé doit être unique.");
				}
			}
		}
		geometrie.setLibelle(geometrieDTO.getLibelle());
		geometrieDao.save(geometrie);
	}
	
	public List<GeometrieDTO> getGeometrieLike(String likeClause) throws FilterException {
		List<Geometrie> geometries = geometrieDao.findAllByLibelleLike(likeClause, PageRequest.of(0,10));
		List<GeometrieDTO> geometriesDTO = GeometrieDTO.toDTOs(geometries);
		return geometriesDTO;
	}
	
	public void deleteGeometrie(GeometrieDTO geometrieDTO) throws FilterException, UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Geometrie geometrie = geometrieDao.findById(geometrieDTO.getId()).get();
		if (geometrie == null) {
			throw new FilterException("Géometrie introuvable.");
		}
		try {
			geometrieDao.delete(geometrie);
		} catch (Exception e) {
			throw new FilterException("Suppression impossible de la Géometrie : " + geometrie.getLibelle() + ". Celle-ci est probablement utilisée.");
		}
	}
	
	public List<CategorieDTO> getCategories() {
		return CategorieDTO.toDTOs(categorieDao.findAllByOrderByLibelleAsc());
	}
	
	public void saveCategorie(CategorieDTO categorieDTO) throws UtilisateurNotConnectedException, FilterException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Categorie categorie = null;
		if (categorieDTO.getId() != null) {
			categorie = categorieDao.findById(categorieDTO.getId()).get();
		}
		if (categorie == null) {
			categorie = new Categorie();
		}
		List<Categorie> categorieForNameChecking = categorieDao.findByLibelle(categorieDTO.getLibelle());
		if (categorieForNameChecking != null && categorieForNameChecking.size() > 0) {
			for (Categorie categorieChecked : categorieForNameChecking) {
				if (categorie.getId() == null || categorieChecked.getId() != categorie.getId()) {
					throw new FilterException("Enregistrement impossible. Le libellé doit être unique.");
				}
			}
		}
		categorie.setLibelle(categorieDTO.getLibelle());
		categorieDao.save(categorie);
	}
	
	public void deleteCategorie(Integer categorieId) throws FilterException, UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Categorie categorie = categorieDao.findById(categorieId).get();
		if (categorie == null) {
			throw new FilterException("Catégorie introuvable.");
		}
		List<Type> types = typeDao.findByCategorieId(categorieId);
		if (types != null && types.size() > 0) {
			for (Type type : types) {
				deleteType(type.getId());
			}
		}
		try {
			categorieDao.delete(categorie);
		} catch (Exception e) {
			throw new FilterException("Suppression impossible de la Catégorie : " + categorie.getLibelle() + ". Celle-ci est probablement utilisée.");
		}
	}
	
	public List<TypeDTO> getTypes(String id) {
		List<Type> types = typeDao.findByCategorieId(Integer.parseUnsignedInt(id));
		return TypeDTO.toDTOs(types);
	}
	
	public void saveType(TypeDTO typeDTO) throws UtilisateurNotConnectedException, FilterException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Type type = null;
		if (typeDTO.getId() != null) {
			type = typeDao.findById(typeDTO.getId()).get();
		}
		if (type == null) {
			type = new Type();
		}
		List<Type> typeForNameChecking = typeDao.findByLibelle(typeDTO.getLibelle());
		if (typeForNameChecking != null && typeForNameChecking.size() > 0) {
			for (Type typeChecked : typeForNameChecking) {
				if (type.getId() == null || typeChecked.getId() != type.getId()) {
					throw new FilterException("Enregistrement impossible. Le libellé doit être unique.");
				}
			}
		}
		type.setLibelle(typeDTO.getLibelle());
		if (typeDTO.getCategorie() == null) {
			throw new FilterException("Catégorie introuvable.");
		}
		Categorie categorie = categorieDao.findById(typeDTO.getCategorie().getId()).get();
		if (categorie == null) {
			throw new FilterException("Catégorie introuvable.");
		}
		type.setCategorie(categorie);
		typeDao.save(type);
	}
	
	public void deleteType(Integer typeId) throws FilterException, UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		Type type = typeDao.findById(typeId).get();
		if (type == null) {
			throw new FilterException("Type introuvable.");
		}
		List<GeometrieType> geometriesType = geometrieTypeDao.findByType(type);
		if (geometriesType != null && geometriesType.size() > 0) {
			for (GeometrieType geometrieType : geometriesType) {
				deleteGeometrieType(GeometrieTypeDTO.toDTO(geometrieType));
			}
		}
		try {
			typeDao.delete(type);
		} catch (Exception e) {
			throw new FilterException("Suppression impossible du Type : " + type.getLibelle() + ". Celui-ci est probablement utilisé.");
		}
	}
	
	public List<GeometrieTypeDTO> getGeometriesType(String id) throws FilterException {
		Type type = typeDao.findById(Integer.parseUnsignedInt(id)).get();
		if (type == null) {
			throw new FilterException("Type introuvable.");
		}
		List<GeometrieType> geometriesType = geometrieTypeDao.findByType(type);
		return GeometrieTypeDTO.toDTOs(geometriesType);
	}
	
	public void saveGeometrieType(GeometrieTypeDTO geometrieTypeDTO) throws UtilisateurNotConnectedException, FilterException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		GeometrieType geometrieType = null;
		if (geometrieTypeDTO.getId() != null) {
			geometrieType = geometrieTypeDao.findById(geometrieTypeDTO.getId()).get();
		}
		if (geometrieType == null) {
			geometrieType = new GeometrieType();
		}
		if (geometrieTypeDTO.getType() == null) {
			throw new FilterException("Type introuvable.");
		}
		Type type = typeDao.findById(geometrieTypeDTO.getType().getId()).get();
		if (type == null) {
			throw new FilterException("Type introuvable.");
		}
		geometrieType.setType(type);
		if (geometrieTypeDTO.getGeometrie() == null) {
			throw new FilterException("Géométrie introuvable.");
		}
		Geometrie geometrie = geometrieDao.findById(geometrieTypeDTO.getGeometrie().getId()).get();
		if (geometrie == null) {
			throw new FilterException("Géométrie introuvable.");
		}
		GeometrieType geometrieTypeForDuplicateChecking = geometrieTypeDao.findByGeometrieAndType(geometrie, type);
		if (geometrieTypeForDuplicateChecking != null) {
			throw new FilterException("L'association existe déjà.");
		}
		geometrieType.setGeometrie(geometrie);
		geometrieTypeDao.save(geometrieType);
	}
	
	public void deleteGeometrieType(GeometrieTypeDTO geometrieTypeDTO) throws FilterException, UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		GeometrieType geometrieType = geometrieTypeDao.findById(geometrieTypeDTO.getId()).get();
		if (geometrieType == null) {
			throw new FilterException("Association Type-Géométrie introuvable.");
		}
		try {
			geometrieTypeDao.delete(geometrieType);
		} catch (Exception e) {
			throw new FilterException("Suppression impossible de l'association Type-Géométrie : " + geometrieType.getType().getLibelle() + " - " + geometrieType.getGeometrie().getLibelle() + ". Celle-ci est probablement utilisée.");
		}
	}
	
	public List<DepartementDTO> getDepartements(String regionId) {
		Integer integerRegionId = !regionId.equals("null") ? Integer.parseUnsignedInt(regionId) : null;
		if (integerRegionId != null) {
			return DepartementDTO.toDTOs(departementDao.findByRegionId(integerRegionId));
		} else {
			return DepartementDTO.toDTOs(departementDao.findAllByOrderByLibelleAsc());
		}
	}
	
	public List<CommuneDTO> getCommunes(String regionId, String departementId, String typedText) {
		Integer integerRegionId = !regionId.equals("null") ? Integer.parseUnsignedInt(regionId) : null;
		Integer integerDepartementId = !departementId.equals("null") ? Integer.parseUnsignedInt(departementId) : null;
		String likeClause;
		if (!typedText.equals("null")) {
			if (typedText.substring(0, 1).equals("#")) {
				likeClause = typedText.substring(1).toUpperCase();
			} else {
				return new ArrayList<>();
			}
		} else {
			return new ArrayList<>();
		}
		if (integerDepartementId != null) {
			return CommuneDTO.toDTOs(communeDao.findByDepartementIdAndLibelleLikeOrCodePostalEquals(integerDepartementId, likeClause), true);
		} else if (integerRegionId != null) {
			return CommuneDTO.toDTOs(communeDao.findByRegionIdAndLibelleLikeOrCodePostalEquals(integerRegionId, likeClause), true);
		} else {
			return CommuneDTO.toDTOs(communeDao.findAllByLibelleLikeOrCodePostalEquals(likeClause), true);
		}
	}
	
	public DTO checkCommune(String regionId, String departementId, String communeId) {
		Integer integerRegionId = !regionId.equals("null") ? Integer.parseUnsignedInt(regionId) : null;
		Integer integerDepartementId = !departementId.equals("null") ? Integer.parseUnsignedInt(departementId) : null;
		Integer integerCommuneId = Integer.parseUnsignedInt(communeId);
		if (integerDepartementId != null) {
			Commune commune = communeDao.findByDepartementIdAndCommuneId(integerDepartementId, integerCommuneId);
			return new DTO(commune != null ? "ok" : "nok");
		} else if (integerRegionId != null) {
			Commune commune = communeDao.findByRegionIdAndCommuneId(integerRegionId, integerCommuneId);
			return new DTO(commune != null ? "ok" : "nok");
		} else {
			return new DTO("nok");
		}
	}
	
	public ContainerDTO getInfrapolesSecteursUos(String regionId, String infrapoleId, String secteurId) {
		ContainerDTO containerDTO = new ContainerDTO();
		
		List<Infrapole> infrapoles;
		List<InfrapoleDTO> infrapolesDTO;
		if (regionId != null) {
			infrapoles = infrapoleDao.findByRegionId(Integer.parseUnsignedInt(regionId));
		} else {
			infrapoles = infrapoleDao.findAllByOrderByNomAsc();
		}
		infrapolesDTO = InfrapoleDTO.toDTOs(infrapoles, false);
		containerDTO.setInfrapoles(infrapolesDTO);
		
		List<Secteur> secteurs;
		List<SecteurDTO> secteursDTO;
		List<UniteOperationnelle> unitesOperationnelle = new ArrayList<>();
		boolean infrapoleIdExistsInList = false;
		if (infrapoleId != null) {
			if (infrapolesDTO != null && infrapolesDTO.size() > 0) {
				for (InfrapoleDTO infrapole : infrapolesDTO) {
					if (infrapole.getId() == Integer.parseUnsignedInt(infrapoleId)) {
						infrapoleIdExistsInList = true;
						break;
					}
				}
			}
		}
		if (infrapoleId != null && infrapoleIdExistsInList) {
			secteurs = secteurDao.findByInfrapoleId(Integer.parseUnsignedInt(infrapoleId));
			List<UniteOperationnelle> unitesOperationnelleToAdd = uniteOperationnelleDao.findByInfrapoleId(Integer.parseUnsignedInt(infrapoleId));
			if (unitesOperationnelleToAdd != null && unitesOperationnelleToAdd.size() > 0) {
				unitesOperationnelle.addAll(unitesOperationnelleToAdd);
			}
		} else {
			List<Integer> ids = containerDTO.parseInfrapolesIds();
			secteurs = ids != null && ids.size() > 0 ? secteurDao.findByInfrapoleIdInList(ids) : null;
			List<UniteOperationnelle> unitesOperationnelleToAdd = ids != null && ids.size() > 0 ? uniteOperationnelleDao.findByInfrapoleIdInList(ids) : null;
			if (unitesOperationnelleToAdd != null && unitesOperationnelleToAdd.size() > 0) {
				unitesOperationnelle.addAll(unitesOperationnelleToAdd);
			}
		}
		secteursDTO = SecteurDTO.toDTOs(secteurs, false, false);
		containerDTO.setSecteurs(secteurs != null && secteurs.size() > 0 ? secteursDTO : new ArrayList<>());
		
		List<UniteOperationnelleDTO> unitesOperationnelleDTO;
		boolean secteurIdExistsInList = false;
		if (secteurId != null) {
			if (secteursDTO != null && secteursDTO.size() > 0) {
				for (SecteurDTO secteur : secteursDTO) {
					if (secteur.getId() == Integer.parseUnsignedInt(secteurId)) {
						secteurIdExistsInList = true;
						break;
					}
				}
			}
		}
		if (secteurId != null && secteurIdExistsInList) {
			List<UniteOperationnelle> unitesOperationnelleToAdd = uniteOperationnelleDao.findBySecteurId(Integer.parseUnsignedInt(secteurId));
			if (unitesOperationnelleToAdd != null && unitesOperationnelleToAdd.size() > 0) {
				unitesOperationnelle.addAll(unitesOperationnelleToAdd);
			}
		} else {
			List<Integer> ids = containerDTO.parseSecteursIds();
			if (ids != null && ids.size() > 0) {
				List<UniteOperationnelle> unitesOperationnelleToAdd = uniteOperationnelleDao.findBySecteurIdInList(ids);
				if (unitesOperationnelleToAdd != null && unitesOperationnelleToAdd.size() > 0) {
					unitesOperationnelle.addAll(unitesOperationnelleToAdd);
				}
			}
		}
		unitesOperationnelleDTO = UniteOperationnelleDTO.toDTOs(unitesOperationnelle, false, false, false);
		containerDTO.setUnitesOperationnelle(unitesOperationnelle != null && unitesOperationnelle.size() > 0 ? unitesOperationnelleDTO : new ArrayList<>());
		
		return containerDTO;
	}
	
	public ContainerDTO getInfrapolesSecteursUosLignes(String regionId, String infrapoleId, String secteurId, String uoId) {
		ContainerDTO containerDTO = getInfrapolesSecteursUos(regionId, infrapoleId, secteurId);
		
		List<UniteOperationnelleDTO> unitesOperationnelleDTO = containerDTO.getUnitesOperationnelle();
		
		List<Ligne> lignes;
		List<LigneDTO> lignesDTO;
		boolean uoIdExistsInList = false;
		if (uoId != null) {
			if (unitesOperationnelleDTO != null && unitesOperationnelleDTO.size() > 0) {
				for (UniteOperationnelleDTO uniteOperationnelle : unitesOperationnelleDTO) {
					if (uniteOperationnelle.getId() == Integer.parseUnsignedInt(uoId)) {
						uoIdExistsInList = true;
						break;
					}
				}
			}
		}
		if (uoId != null && uoIdExistsInList) {
			lignes = ligneDao.findByUniteOperationnelleId(Integer.parseUnsignedInt(uoId));
		} else {
			List<Integer> ids = containerDTO.parseUnitesOperationnelleIds();
			lignes = ids != null && ids.size() > 0 ? ligneDao.findByUniteOperationnelleIdInList(ids) : null;
		}
		lignesDTO = LigneDTO.toDTOs(lignes);
		containerDTO.setLignes(lignes != null && lignes.size() > 0 ? lignesDTO : new ArrayList<>());
		
		return containerDTO;
	}
	
	public List<ProprietaireDTO> getProprietaires() {
		return ProprietaireDTO.toDTOs(proprietaireDao.findAllByOrderByNameAsc());
	}
	
	public List<OuvrageDTO> getOuvragesName(String typedText) {
		String likeClause;
		if (!typedText.equals("null")) {
			if (typedText.substring(0, 1).equals("#")) {
				likeClause = typedText.substring(1).toUpperCase();
			} else {
				return new ArrayList<>();
			}
		} else {
			return new ArrayList<>();
		}
		return OuvrageDTO.toDTOs(ouvrageDao.findAllByNameLike(likeClause), "onlyName");
	}
	
	public List<UtilisateurDTO> getReferentsTechniquesRegionaux(String regionId, String fonction) throws UtilisateurException {
		Integer integerRegionId = !regionId.equals("null") ? Integer.parseUnsignedInt(regionId) : null;
		if (fonction == null) {
			throw new UtilisateurException("Fonction introuvable");
		}
		FonctionUtilisateurEnum fonctionRecherchee = FonctionUtilisateurEnum.FUNCTION_OTHER;
		if (fonction.equals("SOAR")) {
			fonctionRecherchee = FonctionUtilisateurEnum.FUNCTION_SOAR;
		} else if (fonction.equals("AOAP")) {
			fonctionRecherchee = FonctionUtilisateurEnum.FUNCTION_AOAP;
		}
		if (integerRegionId != null) {
			// TODO @Alban : Implémenter la recherche par région en paramètre
			return UtilisateurDTO.toDTOs(utilisateurDao.findByFonctionAndDateFinMissionIsNullAndPrenomIsNullOrderByNomAscPrenomAsc(fonctionRecherchee), true);
		} else {
			// TODO @Alban : Implémenter la recherche par région par défaut de l'utilsateur connecté
			return UtilisateurDTO.toDTOs(utilisateurDao.findByFonctionAndDateFinMissionIsNullAndPrenomIsNullOrderByNomAscPrenomAsc(fonctionRecherchee), true);
		}
	}
	
	public List<DTO> getSegmentsGestion() {
		return DTO.toDTOs(ligneSegmentDao.findAllDistinctCodeOrderByCode());
	}
	
	public List<DTO> getComptesAnalytiques() {
		return DTO.toDTOs(surveillanceDao.findAllDistinctCodeOrderByCode());
	}
	
	private class ContainerDTO {
		List<InfrapoleDTO> infrapoles;
		List<SecteurDTO> secteurs;
		List<UniteOperationnelleDTO> unitesOperationnelle;
		List<LigneDTO> lignes;
		
		public ContainerDTO() {
			
		}

		public List<InfrapoleDTO> getInfrapoles() {
			return infrapoles;
		}
		
		public List<Integer> parseInfrapolesIds() {
			List<Integer> ids = new ArrayList<>();
			for (InfrapoleDTO dto : this.infrapoles) {
				ids.add(dto.getId());
			}
			return ids;
		}
		
		public void setInfrapoles(List<InfrapoleDTO> infrapoles) {
			this.infrapoles = infrapoles;
		}
		
		public List<SecteurDTO> getSecteurs() {
			return secteurs;
		}
		
		public List<Integer> parseSecteursIds() {
			List<Integer> ids = new ArrayList<>();
			for (SecteurDTO dto : this.secteurs) {
				ids.add(dto.getId());
			}
			return ids;
		}
		
		public void setSecteurs(List<SecteurDTO> secteurs) {
			this.secteurs = secteurs;
		}
		
		public List<UniteOperationnelleDTO> getUnitesOperationnelle() {
			return unitesOperationnelle;
		}
		
		public List<Integer> parseUnitesOperationnelleIds() {
			List<Integer> ids = new ArrayList<>();
			for (UniteOperationnelleDTO dto : this.unitesOperationnelle) {
				ids.add(dto.getId());
			}
			return ids;
		}
		
		public void setUnitesOperationnelle(List<UniteOperationnelleDTO> unitesOperationnelle) {
			this.unitesOperationnelle = unitesOperationnelle;
		}
		
		public List<LigneDTO> getLignes() {
			return lignes;
		}
		
		public void setLignes(List<LigneDTO> lignes) {
			this.lignes = lignes;
		}
	}
}
