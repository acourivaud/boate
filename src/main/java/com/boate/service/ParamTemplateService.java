package com.boate.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boate.dao.ParamTemplateDao;
import com.boate.dto.ParamTemplateDTO;

@Service
public class ParamTemplateService {

	private final Logger LOGGER = LoggerFactory.getLogger(ParamTemplateService.class);

	@Autowired
	private ParamTemplateDao paramTemplateDao;


	public List<ParamTemplateDTO> findByName(String templateName) {
		return ParamTemplateDTO.toDTOs(paramTemplateDao.findByLibelle(templateName));
	}
}
