package com.boate.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.boate.dao.ArchiveBoiteDao;
import com.boate.dao.ArchiveDossierDao;
import com.boate.dto.ParamValueDTO;
import com.boate.exception.ArchiveException;
import com.boate.exception.UtilisateurException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.dto.ArchiveBoiteDTO;
import com.boate.dto.ArchiveDossierDTO;
import com.boate.model.ArchiveBoite;
import com.boate.model.ArchiveDossier;
import com.boate.model.Utilisateur;
import com.boate.security.entity.UserPrincipal;


@Service
public class ArchiveService {

	private final Logger LOGGER = LoggerFactory.getLogger(ArchiveService.class);

	@Autowired
	private ArchiveDossierDao archiveDossierDao;
	
	@Autowired
	private ArchiveBoiteDao archiveBoiteDao;
	
	@Autowired
	private UtilisateurService utilisateurService;
	
	@Autowired
	private OuvrageService ouvrageService;

	public List<ArchiveDossierDTO> getArchives(List<ParamValueDTO> requestFilters, String liste, UserPrincipal currentUser) {		
		List<ArchiveDossier> archives = archiveDossierDao.findArchivesWithFilters(requestFilters, currentUser);
		return ArchiveDossierDTO.toDTOs(archives, liste);
	}
	
	public Long getArchivesCount(List<ParamValueDTO> requestFilters, UserPrincipal currentUser) {
		Long count = archiveDossierDao.countArchivesWithFilters(requestFilters, currentUser);
		return count;
	}
	
	public List<ArchiveBoiteDTO> getBoitesFromSearch(String typedText) {
		return ArchiveBoiteDTO.toDTOs(archiveBoiteDao.findTop50ByNomContainingOrderByNomAsc(typedText));
	}
	
	private void verifyArchiveBoiteAccess() throws UtilisateurNotConnectedException, UtilisateurException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		
		boolean isAdmin = connectedUser.hasPermission("Admin Boite");
		
		if (!isAdmin) {
			throw new UtilisateurException("Non autorisé.");
		}
	}
	
	public List<ArchiveBoiteDTO> getArchivesBoites() throws UtilisateurNotConnectedException, UtilisateurException {
		//verifyArchiveBoiteAccess();
		List<ArchiveBoite> archives = archiveBoiteDao.findAll();
		return ArchiveBoiteDTO.toDTOs(archives);
	}
	
	public List<ArchiveBoiteDTO> postArchiveBoite(ArchiveBoiteDTO archiveBoiteDTO) throws UtilisateurException, UtilisateurNotConnectedException, ArchiveException {
		verifyArchiveBoiteAccess();
		
		ArchiveBoite archive = null;
		if (archiveBoiteDTO.getId() != null) {
			archive = archiveBoiteDao.findById(archiveBoiteDTO.getId()).orElse(null);
			if (archive == null) {
				throw new ArchiveException("Archive introuvable.");
			}
		} else {
			archive = new ArchiveBoite();
		}
		ArchiveBoite archiveByNom = archiveBoiteDao.findByNom(archiveBoiteDTO.getNom());
		if (archiveByNom != null && archiveByNom.getId() != archiveBoiteDTO.getId()) {
			throw new ArchiveException("Un nom de boite doit être unique.");
		}
		
		archive.setLocalisation(archiveBoiteDTO.getLocalisation());
		archive.setNom(archiveBoiteDTO.getNom());
		
		archiveBoiteDao.save(archive);
		
		return getArchivesBoites();
	}
	
	public List<ArchiveBoiteDTO> deleteArchiveBoite(ArchiveBoiteDTO archiveBoiteDTO) throws UtilisateurException, UtilisateurNotConnectedException, ArchiveException {
		verifyArchiveBoiteAccess();
		
		Optional<ArchiveBoite> archive = null;
		if (archiveBoiteDTO.getId() != null) {
			archive = archiveBoiteDao.findById(archiveBoiteDTO.getId());
			if (archive.isPresent()) {
				try
				{
					archiveBoiteDao.delete(archive.get());
				} catch(Exception e) {
					throw new UtilisateurException("Suppression impossible. La boite est probablement utilisée.");
				}
				
				return getArchivesBoites();
			}
		}
		throw new ArchiveException("Boite introuvable.");
	}
	
	public ArchiveDossierDTO getArchiveById(Integer id) {
		return ArchiveDossierDTO.toDTO(archiveDossierDao.findById(id).get(), "ficheArchive");
	}
	
	public List<ArchiveDossierDTO> getArchiveByOuvrage(Integer id) {
		return ArchiveDossierDTO.toDTOs(archiveDossierDao.findByOuvrageId(id), "ficheArchive");
	}
	
	public void createOrUpdate(final ArchiveDossierDTO archiveDTO) throws Exception {
		ArchiveDossier archive = new ArchiveDossier();
		if (archiveDTO.getId() != null) { //Archive existant
			archive = archiveDossierDao.findById(archiveDTO.getId()).get();
		}
		
		archive.setAnneeDebut(archiveDTO.getAnneeDebut());
		archive.setAnneeFin(archiveDTO.getAnneeFin());
		archive.setBoite(archiveBoiteDao.findById(archiveDTO.getBoite().getId()).get());
		archive.setCommentaires(archiveDTO.getCommentaires());
		archive.setDescription(archiveDTO.getDescription());
		archive.setDua(archiveDTO.getDua());

		archive.setEnd_pk(archive.getEndPk());
		archive.setLigneNonRattachee(archive.getLigneNonRattachee());
		archive.setNumeroDossier(archiveDTO.getNumeroDossier());
		
		if (archiveDTO.getOuvrage() != null) {
			archive.setOuvrage(ouvrageService.getOuvrage(archiveDTO.getOuvrage().getId()));
		}
		
		archive.setPk(archiveDTO.getPk());
		archive.setService(archiveDTO.getService());
		archive.setSortFinale(archiveDTO.getSortFinale());
		archive.setTypeArchive(archiveDTO.getTypeArchive());
		
		archiveDossierDao.save(archive);
	}
	
	public void delete(final ArchiveDossierDTO archiveDTO) throws Exception {
		Optional<ArchiveDossier> archive = null;
		if (archiveDTO.getId() != null) {
			archive = archiveDossierDao.findById(archiveDTO.getId());
			if (archive.isPresent()) {
				ArchiveDossier currentArchive = archive.get();
				archiveDossierDao.delete(archive.get());
			}else {
				throw new ArchiveException("L'archive est introuvable");
			}
		}else {
			throw new ArchiveException("L'archive est introuvable");
		}
	}
}
