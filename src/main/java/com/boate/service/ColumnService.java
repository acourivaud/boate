package com.boate.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boate.dao.UtilisateurColonneDao;
import com.boate.dto.ParamValueDTO;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.model.Utilisateur;
import com.boate.model.UtilisateurColonne;

@Service
public class ColumnService {
	
	@Autowired
	UtilisateurService utilisateurService;
	
	@Autowired
	private UtilisateurColonneDao utilisateurColonneDao;

	public List<ParamValueDTO> getColumnsForConnectedUser(String page) throws UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		List<UtilisateurColonne> utilisateurColonnes = utilisateurColonneDao.findByUtilisateurAndPage(connectedUser, page);
		return ParamValueDTO.toDTOs(utilisateurColonnes);
	}
	
	public void saveColumnsForConnectedUser(String page, String param, String value) throws UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		UtilisateurColonne utilisateurColonne = utilisateurColonneDao.findByUtilisateurAndPageAndParam(connectedUser, page, param);
		if (value.equals("null")) {
			if (utilisateurColonne != null) {
				utilisateurColonneDao.delete(utilisateurColonne);
			}
		} else {
			if (utilisateurColonne != null) {
				utilisateurColonne.setValue(value);
				
			} else {
				utilisateurColonne = new UtilisateurColonne(connectedUser, page, param, value);
			}
			utilisateurColonneDao.save(utilisateurColonne);
		}
	}
	
	public List<ParamValueDTO> getColumnsOrderForConnectedUser(String page) throws UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		List<UtilisateurColonne> utilisateurColonnes = utilisateurColonneDao.findByUtilisateurAndPageOrderByParam(connectedUser, page + "_ORDER");
		return ParamValueDTO.toDTOs(utilisateurColonnes);
	}
	
	public void saveColumnsOrderForConnectedUser(String page, List<ParamValueDTO> paramValues) throws UtilisateurNotConnectedException {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		if (connectedUser == null) {
			throw new UtilisateurNotConnectedException();
		}
		List<UtilisateurColonne> utilisateurColonnes = utilisateurColonneDao.findByUtilisateurAndPage(connectedUser, page);
		if (utilisateurColonnes != null && utilisateurColonnes.size() > 0) {
			utilisateurColonneDao.deleteAll(utilisateurColonnes);
		}
		if (paramValues != null && paramValues.size() > 1) {
			utilisateurColonnes = UtilisateurColonne.toUtilisateurColonnes(connectedUser, page, paramValues);
			utilisateurColonneDao.saveAll(utilisateurColonnes);
		}
	}
}
