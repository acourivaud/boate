package com.boate.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boate.dao.OuvrageDao;
import com.boate.dao.OuvrageProprietaireDao;
import com.boate.dao.ProprietaireDao;
import com.boate.dto.OuvrageProprietaireDTO;
import com.boate.dto.ProprietaireDTO;
import com.boate.exception.OuvrageException;
import com.boate.exception.ProprietaireException;
import com.boate.model.OuvrageProprietaire;
import com.boate.model.Proprietaire;

@Service
public class ProprietaireService {

	private final Logger LOGGER = LoggerFactory.getLogger(ProprietaireService.class);

	@Autowired
	private OuvrageProprietaireDao ouvrageProprietaireDao;
	
	@Autowired
	private ProprietaireDao proprietaireDao;
	
	@Autowired
	private OuvrageDao ouvrageDao;

	
	public List<OuvrageProprietaireDTO> getByOuvrageId(Integer id) {
		return OuvrageProprietaireDTO.toDTOs(ouvrageProprietaireDao.findByOuvrageOrderByOrdre(ouvrageDao.findById(id).get()));
	}
	
	public List<ProprietaireDTO> findAll() {
		return ProprietaireDTO.toDTOs(proprietaireDao.findAllByOrderByNameAsc());
	}
	
	public List<ProprietaireDTO> createOrUpdate(final ProprietaireDTO proprietaireDTO) throws Exception {
		Proprietaire proprietaire = null;
		if (proprietaireDTO.getId() != null) proprietaire = proprietaireDao.findById(proprietaireDTO.getId()).orElse(null);
		if (proprietaire == null) proprietaire = new Proprietaire();
		proprietaire.setName(proprietaireDTO.getNom());
		proprietaireDao.save(proprietaire);
		return findAll();
	}
	
	public List<ProprietaireDTO> delete(final ProprietaireDTO proprietaireDTO) throws Exception {
		Proprietaire proprietaire = proprietaireDao.findById(proprietaireDTO.getId()).orElse(null);
		if (proprietaire == null) throw new ProprietaireException("Propriétaire introuvable");
		proprietaireDao.delete(proprietaire);
		return findAll();
	}
	
	public List<ProprietaireDTO> findProprietaireDisponible(Integer id) {
		return ProprietaireDTO.toDTOs(proprietaireDao.findProprietaireDisponible(ouvrageDao.findById(id).get()));
	}
	
	public void createOrUpdateOuvrageProprietaire(final Object oOuvrageProprietaireDTO) throws Exception {
		OuvrageProprietaire ouvrageProprietaire = new OuvrageProprietaire();
		OuvrageProprietaireDTO ouvrageProprietaireDTO = (OuvrageProprietaireDTO) oOuvrageProprietaireDTO;
		ouvrageProprietaire = new OuvrageProprietaire();
		if (ouvrageProprietaireDTO.getId() != null) {
			ouvrageProprietaire = ouvrageProprietaireDao.findById(ouvrageProprietaireDTO.getId()).get();
		}
		ouvrageProprietaire.setOrdre(ouvrageProprietaireDTO.getOrdre());
		ouvrageProprietaire.setPourcentage(ouvrageProprietaireDTO.getPourcentage());
		ouvrageProprietaire.setOuvrage(ouvrageDao.findById(ouvrageProprietaireDTO.getOuvrage().getId()).get());
		ouvrageProprietaire.setProprietaire(proprietaireDao.findById(ouvrageProprietaireDTO.getProprietaire().getId()).get());
		ouvrageProprietaireDao.save(ouvrageProprietaire);
	}
	
	public void deleteOuvrageProprietaire(final OuvrageProprietaireDTO ouvrageProprietaireDTO) throws Exception {
		Optional<OuvrageProprietaire> ouvrageProprietaire = null;
		if (ouvrageProprietaireDTO.getId() != null) {
			ouvrageProprietaire = ouvrageProprietaireDao.findById(ouvrageProprietaireDTO.getId());
			if (ouvrageProprietaire.isPresent()) {
				ouvrageProprietaireDao.delete(ouvrageProprietaire.get());
			}else {
				throw new OuvrageException("Le propriétaire de l'ouvrage est introuvable");
			}
		}else {
			throw new OuvrageException("Le propriétaire de l'ouvrage est introuvable");
		}
	}
}
