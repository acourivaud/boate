package com.boate.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boate.dao.PlanningVehiculeDao;
import com.boate.dao.UtilisateurDao;
import com.boate.dao.VehiculeDao;
import com.boate.dto.DTO;
import com.boate.dto.MaterielDTO;
import com.boate.dto.PlanningUtilisateurMaterielDTO;
import com.boate.dto.PlanningUtilisateurVehiculeDTO;
import com.boate.dto.ReservationMaterielUtilisateurDTO;
import com.boate.dto.ReservationVehiculeUtilisateurDTO;
import com.boate.dto.UtilisateurDTO;
import com.boate.dto.VehiculeDTO;
import com.boate.exception.UtilisateurException;
import com.boate.exception.UtilisateurNotConnectedException;
import com.boate.exception.VehiculeException;
import com.boate.model.Materiel;
import com.boate.model.PlanningMateriel;
import com.boate.model.PlanningVehicule;
import com.boate.model.Utilisateur;
import com.boate.model.Vehicule;
import com.sun.media.sound.InvalidDataException;

@Service
public class VehiculeService {

	@Autowired
	private VehiculeDao vehiculeDao;
	
	@Autowired
	private PlanningVehiculeDao planningVehiculeDao;
	
	@Autowired
	private UtilisateurDao utilisateurDao;
	
	@Autowired
	private UtilisateurService utilisateurService;

	public List<VehiculeDTO> getAllVehiculesDTO() {
		return VehiculeDTO.toDTOs(vehiculeDao.findAll());
	}
	
	public List<VehiculeDTO> getVehiculesByDates(LocalDateTime dateDebut, LocalDateTime dateFin) {
		List<Vehicule> availableVehicules;
		List<Vehicule> bookedVehicules = planningVehiculeDao.findDistinctBetweenDates(dateDebut, dateFin);
		List<Integer> bookedVehiculesIds = new ArrayList<>();
		if (bookedVehicules != null && bookedVehicules.size() > 0) {
			for (Vehicule bookedVehicule : bookedVehicules) {
				bookedVehiculesIds.add(bookedVehicule.getId());
			}
			availableVehicules = vehiculeDao.findByIdNotInList(bookedVehiculesIds);
		} else {
			availableVehicules = vehiculeDao.findAll();
		}
		return VehiculeDTO.toDTOs(availableVehicules);
	}
	
	public void createOrUpdate(final VehiculeDTO vehiculeDTO)
			throws VehiculeException {
		Vehicule vehicule = null;
		if (vehiculeDTO.getId() != null) {
			vehicule = vehiculeDao.findById(vehiculeDTO.getId()).orElse(null);
			if (vehicule == null) {
				throw new VehiculeException("Véhicule introuvable");
			}
		} else {
			vehicule = new Vehicule();
		}
		Vehicule vehiculeByImmat = vehiculeDao.findByImmatriculation(vehiculeDTO.getImmatriculation()).orElse(null);
		if (vehiculeByImmat != null && vehiculeByImmat.getId() != vehiculeDTO.getId()) {
			throw new VehiculeException("Une immatriculation doit être unique.");
		}
		vehicule.setMarque(vehiculeDTO.getMarque());
		vehicule.setModele(vehiculeDTO.getModele());
		vehicule.setImmatriculation(vehiculeDTO.getImmatriculation());
		vehicule.setPlaces(vehiculeDTO.getPlaces());
		vehiculeDao.save(vehicule);
	}
	
	public List<VehiculeDTO> delete(final VehiculeDTO vehiculeDTO) throws Exception {
		Optional<Vehicule> vehicule = null;
		if (vehiculeDTO.getId() != null) {
			vehicule = vehiculeDao.findById(vehiculeDTO.getId());
			if (vehicule.isPresent()) {
				try
				{
					vehiculeDao.delete(vehicule.get());
				} catch(Exception e) {
					throw new VehiculeException("Suppression impossible. Le vehicule est probablement utilisé.");
				}
				return getAllVehiculesDTO();
			}
		}
		throw new VehiculeException("Le vehicule est introuvable");
	}
	
	public PlanningUtilisateurVehiculeDTO getPlanningForUser(UtilisateurDTO utilisateurDTO, LocalDateTime dateDebut, LocalDateTime dateFin) {
		return PlanningUtilisateurVehiculeDTO.toDTO(planningVehiculeDao.findByUtilisateurIdBetweenDates(utilisateurDTO.getIdUtilisateur(), dateDebut, dateFin), dateDebut, dateFin, false);
	}
	
	public PlanningUtilisateurVehiculeDTO getPlanningForVehicle(PlanningUtilisateurVehiculeDTO planningUtilisateurVehiculeDTO) throws Exception {
		planningUtilisateurVehiculeDTO.setReservationVehiculeUtilisateur(
			ReservationVehiculeUtilisateurDTO.toDTOs(
				planningVehiculeDao.findByVehiculeIdBetweenDates(
						planningUtilisateurVehiculeDTO.getVehicule().getId(),
						planningUtilisateurVehiculeDTO.getDateDebut().toLocalDateTime(),
						planningUtilisateurVehiculeDTO.getDateFin().toLocalDateTime()
				),
				planningUtilisateurVehiculeDTO.isTrueIfListOfUsersFalseIfListOfVehicules()
			)
				
		);
		return planningUtilisateurVehiculeDTO;
	}
	
	public List<PlanningUtilisateurVehiculeDTO> getPlanningForAllVehicule(LocalDateTime dateDebut, LocalDateTime dateFin) {
		List<PlanningVehicule> planningVehicules = planningVehiculeDao.findBetweenDates(dateDebut, dateFin);
		List<PlanningUtilisateurVehiculeDTO> planningUtilisateurVehiculesDTO = new ArrayList<PlanningUtilisateurVehiculeDTO>();
		if (planningVehicules != null && planningVehicules.size() > 0) {
			Vehicule loopVehicule = null;
			PlanningUtilisateurVehiculeDTO loopPlanning = null;
			int remaining = planningVehicules.size();
			for (PlanningVehicule planningVehicule : planningVehicules) {
				if (loopVehicule == null || loopVehicule.getId() != planningVehicule.getVehicule().getId()) {
					if (loopVehicule != null) {
						planningUtilisateurVehiculesDTO.add(loopPlanning);
					}
					loopVehicule = planningVehicule.getVehicule();
					loopPlanning = new PlanningUtilisateurVehiculeDTO();
					loopPlanning.setTrueIfListOfUsersFalseIfListOfVehicules(true);
					loopPlanning.setVehicule(VehiculeDTO.toDTO(loopVehicule));
					loopPlanning.setReservationVehiculeUtilisateur(new ArrayList<ReservationVehiculeUtilisateurDTO>());
				}
				loopPlanning.getReservationVehiculeUtilisateur().add(ReservationVehiculeUtilisateurDTO.toDTO(planningVehicule, true));
				remaining--;
				if (remaining < 1) {
					planningUtilisateurVehiculesDTO.add(loopPlanning);
				}
			}
		}
		return planningUtilisateurVehiculesDTO;
	}
	
	public PlanningUtilisateurVehiculeDTO createOrUpdatePlanning(final PlanningUtilisateurVehiculeDTO planningUtilisateurVehiculeDTO) throws Exception {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		
		boolean isAdmin = connectedUser.hasPermission("Admin Planning Vehicule");
		
		PlanningVehicule planningVehicule = null;
		
		// Should only contain one date range element to create or update
		if (planningUtilisateurVehiculeDTO.getReservationVehiculeUtilisateur() == null ||
				planningUtilisateurVehiculeDTO.getReservationVehiculeUtilisateur().size() < 1 ||
				planningUtilisateurVehiculeDTO.getReservationVehiculeUtilisateur().size() > 1)
		{
			throw new VehiculeException("Données de réservation incohérentes");
		}
		
		Integer planningVehiculeId = planningUtilisateurVehiculeDTO.getReservationVehiculeUtilisateur().get(0).getIdPlanningVehicule();
		if (planningVehiculeId != null) {
			planningVehicule = planningVehiculeDao.findById(planningVehiculeId).orElse(null);
			if (planningVehicule == null) {
				throw new VehiculeException("Réservation de véhicule introuvable");
			}
		} else {
			planningVehicule = new PlanningVehicule();
		}
		
		UtilisateurDTO utilisateurDTO = planningUtilisateurVehiculeDTO.getReservationVehiculeUtilisateur().get(0).getUtilisateur();
		Utilisateur utilisateur;
		if (utilisateurDTO != null) {
			utilisateur = utilisateurDao.findByIdUtilisateur(utilisateurDTO.getIdUtilisateur());
			if (utilisateur == null) {
				throw new VehiculeException("Utilisateur introuvable");
			}
		} else {
			utilisateur = planningVehicule.getUtilisateur();
		}
		
		if (!isAdmin) {
			if (connectedUser.getIdUtilisateur() != utilisateur.getIdUtilisateur()) {
				throw new UtilisateurException("Non autorisé.");
			}
		}
		
		VehiculeDTO vehiculeDTO = planningUtilisateurVehiculeDTO.getReservationVehiculeUtilisateur().get(0).getVehicule();
		Vehicule vehicule;
		if (vehiculeDTO != null) {
			vehicule = vehiculeDao.findById(vehiculeDTO.getId()).orElse(null);
			if (vehicule == null) {
				throw new VehiculeException("Véhicule introuvable");
			}
		} else {
			vehicule = planningVehicule.getVehicule();
		}
		
		LocalDateTime dateDebutReservation = planningUtilisateurVehiculeDTO.getReservationVehiculeUtilisateur().get(0).getDateDebut().toLocalDateTime();
		LocalDateTime dateFinReservation = planningUtilisateurVehiculeDTO.getReservationVehiculeUtilisateur().get(0).getDateFin().toLocalDateTime();
		
		List<PlanningVehicule> planningsVehiculesByVehicule = planningVehiculeDao.findByVehiculeIdBetweenDates(vehiculeDTO.getId(), dateDebutReservation, dateFinReservation);
		planningUtilisateurVehiculeDTO.setTrueIfListOfUsersFalseIfListOfVehicules(true);
		planningUtilisateurVehiculeDTO.setReservationVehiculeUtilisateur(new ArrayList<ReservationVehiculeUtilisateurDTO>());
		if (planningsVehiculesByVehicule != null && planningsVehiculesByVehicule.size() > 0)
		{
			for (PlanningVehicule planningVehiculeByVehicule : planningsVehiculesByVehicule)
			{
				if (planningVehiculeByVehicule.getId() != planningVehiculeId)
				{
					planningUtilisateurVehiculeDTO.setConflictOnVehicleResponse(true);
					planningUtilisateurVehiculeDTO.getReservationVehiculeUtilisateur().add(ReservationVehiculeUtilisateurDTO.toDTO(planningVehiculeByVehicule, true));
				}
			}
		}
		
		if (!planningUtilisateurVehiculeDTO.isForceUpdateIfPossible() && !planningUtilisateurVehiculeDTO.isConflictOnVehicleResponse())
		{
			List<PlanningVehicule> planningsVehiculesByUtilisateur = planningVehiculeDao.findByUtilisateurIdBetweenDates(utilisateurDTO.getIdUtilisateur(), dateDebutReservation, dateFinReservation);
			planningUtilisateurVehiculeDTO.setTrueIfListOfUsersFalseIfListOfVehicules(false);
			planningUtilisateurVehiculeDTO.setReservationVehiculeUtilisateur(new ArrayList<ReservationVehiculeUtilisateurDTO>());
			if (planningsVehiculesByUtilisateur != null && planningsVehiculesByUtilisateur.size() > 0)
			{
				for (PlanningVehicule planningVehiculeByUtilisateur : planningsVehiculesByUtilisateur)
				{
					if (planningVehiculeByUtilisateur.getId() != planningVehiculeId)
					{
						planningUtilisateurVehiculeDTO.setConflictOnUserResponse(true);
						planningUtilisateurVehiculeDTO.getReservationVehiculeUtilisateur().add(ReservationVehiculeUtilisateurDTO.toDTO(planningVehiculeByUtilisateur, false));
					}
				}
			}
		}
		
		// If all controls went OK then save and return the new entire list of results
		if (!planningUtilisateurVehiculeDTO.isConflictOnVehicleResponse() && !planningUtilisateurVehiculeDTO.isConflictOnUserResponse())
		{
			planningVehicule.setUtilisateur(utilisateur);
			planningVehicule.setVehicule(vehicule);
			planningVehicule.setDateDebut(dateDebutReservation);
			planningVehicule.setDateFin(dateFinReservation);
			planningVehiculeDao.save(planningVehicule);
			
			/*List<PlanningVehicule> planningsVehicules;
			if (planningUtilisateurVehiculeDTO.isTrueIfListOfUsersFalseIfListOfVehicules())
			{
				planningsVehicules = planningVehiculeDao.findByVehiculeIdBetweenDates(
						vehicule.getId(),
						planningUtilisateurVehiculeDTO.getDateDebut().toLocalDateTime(),
						planningUtilisateurVehiculeDTO.getDateFin().toLocalDateTime());
			}
			else
			{
				planningsVehicules = planningVehiculeDao.findByUtilisateurIdBetweenDates(
						utilisateur.getIdUtilisateur(),
						planningUtilisateurVehiculeDTO.getDateDebut().toLocalDateTime(),
						planningUtilisateurVehiculeDTO.getDateFin().toLocalDateTime());
			}
			planningUtilisateurVehiculeDTO.setReservationVehiculeUtilisateur(ReservationVehiculeUtilisateurDTO.toDTOs(planningsVehicules, planningUtilisateurVehiculeDTO.isTrueIfListOfUsersFalseIfListOfVehicules()));*/
			List<VehiculeDTO> availableVehicules = getVehiculesByDates(
					dateDebutReservation,
					dateFinReservation
			);
			if (availableVehicules != null && availableVehicules.size() > 0) {
				List<ReservationVehiculeUtilisateurDTO> resas = new ArrayList<>();
				for (VehiculeDTO availableVehicule : availableVehicules) {
					ReservationVehiculeUtilisateurDTO resa = new ReservationVehiculeUtilisateurDTO();
					resa.setVehicule(availableVehicule);
					resas.add(resa);
				}
				planningUtilisateurVehiculeDTO.setReservationVehiculeUtilisateur(resas);
			}
			return planningUtilisateurVehiculeDTO;
			
		}
		
		return planningUtilisateurVehiculeDTO;
	}
	
	public PlanningUtilisateurVehiculeDTO deletePlanning(final PlanningUtilisateurVehiculeDTO planningUtilisateurVehiculeDTO) throws Exception {
		Utilisateur connectedUser = utilisateurService.getUtilisateurConnected();
		
		boolean isAdmin = connectedUser.hasPermission("Admin Planning Vehicule");
		
		if (!isAdmin) {
			if (connectedUser.getIdUtilisateur() != planningUtilisateurVehiculeDTO.getUtilisateur().getIdUtilisateur()) {
				throw new UtilisateurException("Non autorisé.");
			}
		}
		
		List<PlanningVehicule> planningsVehicules = null;
		if (planningUtilisateurVehiculeDTO.getReservationVehiculeUtilisateur() != null && planningUtilisateurVehiculeDTO.getReservationVehiculeUtilisateur().size() > 0) {
			List<Integer> ids = new ArrayList<>();
			for (ReservationVehiculeUtilisateurDTO reservation : planningUtilisateurVehiculeDTO.getReservationVehiculeUtilisateur())
			{
				ids.add(reservation.getIdPlanningVehicule());
			}
			planningsVehicules = planningVehiculeDao.findByIdInList(ids);
			if (planningsVehicules != null && planningsVehicules.size() > 0)
			{
				planningVehiculeDao.deleteAll(planningsVehicules);
			}
		}
		
		if (planningUtilisateurVehiculeDTO.isTrueIfListOfUsersFalseIfListOfVehicules())
		{
			VehiculeDTO vehiculeDTO = planningUtilisateurVehiculeDTO.getVehicule();
			planningsVehicules = planningVehiculeDao.findByVehiculeIdBetweenDates(
					vehiculeDTO.getId(),
					planningUtilisateurVehiculeDTO.getDateDebut().toLocalDateTime(),
					planningUtilisateurVehiculeDTO.getDateFin().toLocalDateTime());
		}
		else
		{
			UtilisateurDTO utilisateurDTO = planningUtilisateurVehiculeDTO.getUtilisateur();
			planningsVehicules = planningVehiculeDao.findByUtilisateurIdBetweenDates(
					utilisateurDTO.getIdUtilisateur(),
					planningUtilisateurVehiculeDTO.getDateDebut().toLocalDateTime(),
					planningUtilisateurVehiculeDTO.getDateFin().toLocalDateTime());
		}
		planningUtilisateurVehiculeDTO.setReservationVehiculeUtilisateur(ReservationVehiculeUtilisateurDTO.toDTOs(planningsVehicules, planningUtilisateurVehiculeDTO.isTrueIfListOfUsersFalseIfListOfVehicules()));
		return planningUtilisateurVehiculeDTO;
	}
}
