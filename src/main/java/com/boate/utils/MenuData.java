package com.boate.utils;

import java.util.HashSet;
import java.util.Set;

public class MenuData {
    public static final String ALL = "all_items";
    
    /** Constant for user functions. */
    public static final String FUNCTION_ADMIN       = "function_admin";
    public static final String FUNCTION_ADMIN_ARCHIVE   = "function_admin_archive";
    public static final String FUNCTION_AH          = "function_ah";
    public static final String FUNCTION_AOAP        = "function_aoap";
    public static final String FUNCTION_AOAU        = "function_aoau";
    public static final String FUNCTION_CU_DUO      = "function_cu/duo";
    public static final String FUNCTION_DDI         = "function_ddi";
    public static final String FUNCTION_DET         = "function_det";
    public static final String FUNCTION_INVITE      = "function_invite";
    public static final String FUNCTION_RI_OA_OT    = "function_ri_oa/ot";
    public static final String FUNCTION_RM_OA_OT    = "function_rm_oa/ot";
    public static final String FUNCTION_RRMI        = "function_rrmi";
    public static final String FUNCTION_RMPL        = "function_rmpl";
    public static final String FUNCTION_ROTP        = "function_rotp";
    public static final String FUNCTION_R_PRILY_Y   = "function_r_prily_y";
    public static final String FUNCTION_R_PRILY     = "function_r_prily";
    public static final String FUNCTION_SOAR        = "function_soar";
    public static final String FUNCTION_ABE         = "function_abe";
    public static final String FUNCTION_TIERS       = "function_tiers";
    public static final String FUNCTION_COT         = "function_cot";
    public static final String FUNCTION_ADJRI_OA_OT = "function_adjri_oa/ot";
    public static final String FUNCTION_OTHER       = "function_other";
    public static final String FUNCTION_ARCHIVE     = "function_archive";
    public static final String FUNCTION_TOPO        = "function_topo";
    public static final String FUNCTION_HYDRO       = "function_hydro";
    public static final String FUNCTION_BETA       = "function_beta";
    public static final String FUNCTION_CHEF_EG     = "function_chef_eg";

    public static final String SURVEILLANCE_HEAD_ABE = "sur_head_abe";
    public static final String SURVEILLANCE_HEAD_AH = "sur_head_ah";
    public static final String SURVEILLANCE_HEAD_AOAP = "sur_head_aoap";
    public static final String SURVEILLANCE_HEAD_AOAU = "sur_head_aoau";
    public static final String SURVEILLANCE_HEAD_COT = "sur_head_cot";
    public static final String SURVEILLANCE_HEAD_SOAR = "sur_head_soar";
    public static final String SURVEILLANCE_HEAD_TIERS = "sur_head_tiers";
    public static final String SURVEILLANCE_HEAD_TOPO = "sur_head_topo";
    
    public static String[] WORK_SURVEILLANCE_HEAD_ARRAY= new String[]{
    	SURVEILLANCE_HEAD_ABE,
    	SURVEILLANCE_HEAD_AH,
        SURVEILLANCE_HEAD_AOAP,
        SURVEILLANCE_HEAD_AOAU,
        SURVEILLANCE_HEAD_COT,
        SURVEILLANCE_HEAD_SOAR,
        SURVEILLANCE_HEAD_TIERS,
        SURVEILLANCE_HEAD_TOPO
    };
    
    public static String[] TYPE_COMPL_ARRAY= new String[]{
    	"type_compl_fsa",
    	"type_compl_nivellement",
    	"type_compl_inclino",
    	"type_compl_autres"
    };
    
    public static String[] TYPE_TOPO_ARRAY= new String[]{
    	"type_compl_nivellement"
    };
    
    public static String[] TYPE_INCLINO_ARRAY= new String[]{
    	"type_compl_inclino"
    };
    
    public static String[] TUBE_INCLINO_ARRAY= new String[]{
    	"0",
    	"1",
    	"2",
    	"3",
    	"4",
    	"5",
    	"6"
    };
    
    
    public static String[] TYPE_COMPL_SP_ARRAY= new String[]{
    	"type_compl_fsa",
    	"type_compl_nivellement",
    	"type_compl_inclino",
    	"type_compl_autres"
    };
    
    public static String[] TYPE_COMPL_SR_ARRAY= new String[]{
    	"type_compl_nivellement",
    	"type_compl_inclino",
    	"type_compl_autres"
    };
    
    public static String[] WORK_SURVEILLANCE_OT_HEAD_ARRAY= new String[]{
        SURVEILLANCE_HEAD_AOAP,
        SURVEILLANCE_HEAD_COT,
    };
    
    public static String[] BOITE_TYPE = new String[]{
        "EG",
        "OA",
        "PR",
        "TU",
        "SANS BOITE",
    };
    
    
    public static String[] INFRAPOLE_NULL = new String[]{
    	"Alpes",
	    "EMB",
	    "LGV",
    	"Rhodanien",
         	    
         	     
         	    
    }; 
    
    public static String[] INFRAPOLE_RHONE_ALPES = new String[]{
                    		"Rhodanien",
                    	    "LGV"
    };
    
    public static String[] INFRAPOLE_ALPES = new String[]{
		"Alpes",
	    "EMB"
	};
    
    public static String[] REGIONS = new String[]{
    	"Alpes",
    	"Rhone-Alpes"
        	    
    };
   /*
	 * public static String[] REGIONS= new String[]{ "Rhone-Alpes" };
	 */
    
    public static String[] BOOLEAN_CHOICE= new String[]{    	
        "Oui",
        "Non"
    };
    
    public static String[] LOT= new String[]{    	
        "Lot_A",
        "Lot_B"
    };

    public static String[] DEPARTEMENTS = new String[]{
                "00",
                "01",
                "03",
                "07",
                "13",
                "26",
                "30",
                "38",
                "42",
                "43",
                "69",
                "71",
                "73",
                "74",
                "84",
                
    };
    
    public static String[] DEPARTEMENTS_RHONE = new String[]{
                "01",
                "03",
                "07",
                "13",
                "26",
                "30",
                "38",
                "42",
                "43",
                "69",
                "71",
                "84"
    };
    
    public static String[] DEPARTEMENTS_ALPES = new String[]{
    			"01",
    			"26",
                "38",
                "73",
                "74"
    };
    
    public static String[] DEPARTEMENTS_REGION_ALPES = new String[]{
		"00",
		"01",
		"26",
        "38",
        "73",
        "74"
};
    
    public static String[] DEPARTEMENTS_EMB = new String[]{
		"00"
};
    
    public static String[] CITIES_REGION_ALPES = new String[]{
    	"38.Beaufort",
        "38.Beaurepaire",
        "38.Beauvoir-de-Marc",
        "38.Bonnefamille",
        "38.Bouge-Chambalud",
        "38.Bourgoin-Jallieu",
        "38.Brezins",
        "38.Cessieu",
        "38.Chanas",
        "38.Charantonnay",
        "38.Chasse-sur-Rhone",
        "38.Chonas-l-Amballan",
        "38.Clonas-sur-Vareze",
        "38.Cour-et-Buis",
        "38.Diemoz",
        "38.Domarin",
        "38.Grenay",
        "38.La-Cote-Saint-Andre",
        "38.La-Tour-du-Pin",
        "38.La-Verpilliere",
        "38.Le-Peage-de-Roussillon",
        "38.Les-Roches",
        "38.l-Isle-d-Abeau",
        "38.Marcilloles",
        "38.Meyssies",
        "38.Moissieu-sur-Dolon",
        "38.Nivolas-Vermelle",
        "38.Pact",
        "38.Penol",
        "38.Primarette",
        "38.Reventin-Vaugris",
        "38.Roussillon",
        "38.Sablons",
        "38.Saint-Alban-de-Roche",
        "38.Saint-Bartelemy",
        "38.Saint-Clair-du-Rhone",
        "38.Saint-Didier-de-la-Tour",
        "38.Saint-Etienne-de-saint-geoirs",
        "38.Saint-Georges-d-Esperanche",
        "38.Saint-Jean-de-Soudain",
        "38.Saint-Lattier",
        "38.Saint-Maurice-l-Exil",
        "38.Saint-Prim",
        "38.Saint-Quentin-Fallavier",
        "38.Saint-Simeon-de-Bressieux",
        "38.Saint-Victor-de-Cessieu",
        "38.Salaise-sur-Sanne",
        "38.Sardieu",
        "38.Savas-Mepin",
        "38.Serezin-de-la-Tour",
        "38.Seyssuel",
        "38.Sillans",
        "38.Vaulx-Milieu",
        "38.Vienne",
        "38.Vilette-d-Anthon",
        "38.Villefontaine",
};
    
    public static String[] CITIES_ALPES = new String[]{
    	"38.Beaufort",
        "38.Beaurepaire",
        "38.Beauvoir-de-Marc",
        "38.Bonnefamille",
        "38.Bouge-Chambalud",
        "38.Bourgoin-Jallieu",
        "38.Brezins",
        "38.Cessieu",
        "38.Chanas",
        "38.Charantonnay",
        "38.Chasse-sur-Rhone",
        "38.Chonas-l-Amballan",
        "38.Clonas-sur-Vareze",
        "38.Cour-et-Buis",
        "38.Diemoz",
        "38.Domarin",
        "38.Grenay",
        "38.La-Cote-Saint-Andre",
        "38.La-Tour-du-Pin",
        "38.La-Verpilliere",
        "38.Le-Peage-de-Roussillon",
        "38.Les-Roches",
        "38.l-Isle-d-Abeau",
        "38.Marcilloles",
        "38.Meyssies",
        "38.Moissieu-sur-Dolon",
        "38.Nivolas-Vermelle",
        "38.Pact",
        "38.Penol",
        "38.Primarette",
        "38.Reventin-Vaugris",
        "38.Roussillon",
        "38.Sablons",
        "38.Saint-Alban-de-Roche",
        "38.Saint-Bartelemy",
        "38.Saint-Clair-du-Rhone",
        "38.Saint-Didier-de-la-Tour",
        "38.Saint-Etienne-de-saint-geoirs",
        "38.Saint-Georges-d-Esperanche",
        "38.Saint-Jean-de-Soudain",
        "38.Saint-Lattier",
        "38.Saint-Maurice-l-Exil",
        "38.Saint-Prim",
        "38.Saint-Quentin-Fallavier",
        "38.Saint-Simeon-de-Bressieux",
        "38.Saint-Victor-de-Cessieu",
        "38.Salaise-sur-Sanne",
        "38.Sardieu",
        "38.Savas-Mepin",
        "38.Serezin-de-la-Tour",
        "38.Seyssuel",
        "38.Sillans",
        "38.Vaulx-Milieu",
        "38.Vienne",
        "38.Vilette-d-Anthon",
        "38.Villefontaine",
};
    
    public static String[] CITIES_EMB = new String[]{
    	"00.Beaufort",
        "00.Beaurepaire",
};
    
    public static String[] CITIES_RHONE_ALPES = new String[]{
        "42.Ailleux",
        "26.Aix-en-Diois",
		"13.Aix-en-Provence",
		"26.Aixan",
		"07.Alba",
		"69.Albigny-sur-Saone",
		"26.Alixan",
		"26.Allan",
		"13.Alleins",
		"26.Allex",
		"69.Amberieux",
		"42.Ambierle",
		"69.Amplepuis",
		"69.Ampuis",
		"07.Andance",
		"26.Andancette",
		"42.Andrezieux-Boutheon",
		"26.Anneyron",
		"69.Anse",
        "26.Aouste-sur-Sye",
        "30.Aramon",
        "69.Arnas",
        "07.Arras-sur-Rhone",
        "26.Aubenasson",
        "07.Aubignas",
        "84.Avignon",
        "07.Baix",
        "01.Balan",   
        "42.Balbigny",
        "26.Barnave",               
        "07.Beauchastel",        
        "38.Beaufort",               
        "38.Beaurepaire",            
        "26.Beaurieres",            
        "38.Beauvoir-de-Marc",     
        "42.Belleroche",
        "69.Belleville-sur-Saone",
        "01.Beligneux",    
        "69.Bessenay",
        "01.Beynost",    
        "42.Boen-sur-Lignon",
        "42.Boisset-les-Montrond",
        "84.Bollene",
        "26.Bonlieu-sur-Roubion",
        "38.Bonnefamille",   
        "42.Bonson",
        "38.Bouge-Chambalud",    
        "26.Bourg-les-Valence",          
        "07.Bourg-Saint-Andeol",            
        "38.Bourgoin-Jallieu",          
        "26.Bren",
        "38.Brezins",  
        "69.Brignais",
        "69.Brussieu",
        "13.Cabries",     
        "84.Caderousse",
        "69.Cailloux-sur-Fontaines",
        "69.Caluire-et-Cuire",
        "84.Caumont-sur-Durance",
        "84.Cavaillon",
        "42.Cervieres",
        "38.Cessieu",             
        "26.Chabeuil",            
        "26.Chabrillan",  
        "42.Chalain-d-Uzore",
        "42.Chamboeuf",
        "69.Chambost-Allieres",
        "69.Chamelet",
        "07.Champagne",    
        "42.Champdieu",
        "38.Chanas",
        "42.Changy",
        "69.Chaponost",
        "38.Charantonnay",   
        "69.Charbonnieres-les-Bains",
        "42.Charlieu",
        "07.Charmes-sur-Rhone",      
        "07.Charnas",               
        "38.Chasse-sur-Rhone",
        "69.Chasse-sur-Rhone",
        "69.Chasselay",
        "07.Chateaubourg", 
        "42.Chateauneuf",
        "26.Chateauneuf-de-Galaure",
        "26.Chateauneuf-du-Rhone",            
        "26.Chateauneuf-sur-Isere",   
        "69.Chatillon",
        "42.Chavanay",
        "26.Chavannes",  
        "69.Chessy",
        "84.Cheval-Blanc",
        "69.Chevinay",
        "07.Chomerac",         
        "38.Chonas-l-Amballan",   
        "42.Civens",
        "01.Civrieux",
        "69.Civrieux-d-Azergues",
        "69.Claveisolles",
        "26.Claveyson",             
        "26.Clerieux",       
        "38.Clonas-sur-Vareze",  
        "69.Collonges-au-Mont-d-Or",
        "69.Colombier-Saugnieu",
        "42.Commelle-Vernay",
        "30.Comps",  
        "69.Condrieu",
        "69.Corcelles",
        "07.Cornas",      
        "38.Cour-et-Buis",
        "69.Courzieu",
        "69.Couzon-au-Mont-d-Or",
        "26.Crest",           
        "26.Crozes-Hermitage",         
        "07.Cruas",
        "69.Curis-au-Mont-d-Or",
        "42.Cuzieu",
        "01.Dagneux",   
        "69.Dardilly",
        "42.Debats",
        "69.Decines",
        "26.Die",          
        "38.Diemoz",
        "26.Divajeu", 
        "38.Domarin",
        "30.Domazan",   
        "69.Dommartin",
        "26.Donzere",   
        "43.Dunieres",
        "69.Echalas",
        "69.Ecully",
        "13.Eguilles",  
        "42.Epercieux-Saint-Paul",
        "26.Epinouze",
        "26.Erome",              
        "26.Espeluche",
        "26.Espenel",              
        "26.Etoile-sur-Rhone",
        "26.Eurre",  
        "69.Eveux",
        "42.Feurs",
        "69.Feyzin",
        "42.Firminy",
        "69.Fleurieu-sur-Saone",
        "69.Fleurieux-sur-l-Arbresle",
        "69.Fontaines-Saint-Martin",
        "69.Fontaines-sur-Saone",
        "26.Fourcinet",
        "42.Fraisses",
        "69.Francheville",
        "69.Genay",
        "26.Gervans",    
        "69.Givors",
        "07.Glun",              
        "26.Granges-les-Beaumont",   
        "38.Grenay",
        "69.Grigny",
        "07.Guilherand",   
        "69.Irigny",
        "26.Jansac",               
        "30.Jonquierres-Saint-Vincent",   
        "69.Jons",
        "69.L-Arbresle",
        "42.L-Hopital-sous-Rochefort",
        "38.l-Isle-d-Abeau",
        "13.La-Barben",             
        "01.La-Boisse",     
        "38.La-Cote-Saint-Andre",
        "26.La-Coucourde",    
        "42.La-Fouillouse",
        "26.La-Garde-Adhemar",     
        "42.La-Grand-Croix",
        "26.La-Laupie",              
        "26.La-Motte-de-Galaure",   
        "69.La-Mulatiere",
        "42.La-Pacaudiere",
        "42.La-Ricamarie",
        "43.La-Seauve-sur-Semene",
        "69.La-Tour-de-Salvagny",
        "38.La-Tour-du-Pin",
        "38.La-Verpilliere",
        "07.La-Voulte-sur-Rhone",    
        "84.Lamotte-du-Rhone",
        "13.Lambesc", 
        "69.Lamure-sur-Azergues",
        "69.Lancie",
        "84.Lapalud",
        "01.Lapeyrouse",             
        "26.Lapeyrouse-Mornay",            
        "26.La-Roche-de-Glun",        
        "26.La-Roche-sur-Grane", 
        "26.Laval-d-Aix",            
        "26.Laveyron",   
        "69.Le-Bois-d-Oingt",
        "69.Le-Breuil",
        "42.Le-Chambon-Feugerolles",
        "42.Le-Coteau",
        "42.Le-Crozet",
        "38.Le-Peage-de-Roussillon",
        "01.Le-Plantay",            
        "07.Le-Pouzin",        
        "07.Le-Teil",  
        "69.Legny",
        "42.Leigneux",
        "07.Lemps",      
        "26.Lens-Lestang",
        "69.Lentilly",
        "30.Les-Angles",   
        "69.Les-Cheres",
        "26.Les-Granges-Gontardes",             
        "13.Les-Pennes-Mirabeau",   
        "38.Les-Roches",
        "69.Les-Sauvages",
        "26.Les-Tourrettes",               
        "26.Lesches-en-Diois",    
        "69.Letra",
        "69.Limas",
        "69.Limonest",
        "07.Limony",
        "69.Lissieu",
        "26.Livron",
        "69.Loire-sur-Rhone",
        "69.Longes",
        "42.Lorette",
        "26.Loriol-sur-Drome",
        "69.Lozanne",
        "26.Luc-en-Diois",
        "69.Lyon",
        "42.Mably",
        "26.Malataverne",
        "13.Mallemort",
        "30.Manduel",
        "26.Manthes",
        "38.Marcilloles",
        "69.Marcilly-d-Azergues",
        "42.Marcilly-le-Chatel",
        "42.Marclopt",
        "69.Marcy-l-Etoile",
        "01.Marlieux",
        "26.Marsanne",
        "26.Marsaz",
        "13.Marseille",
        "01.Massieux",
        "07.Mauves",
        "26.Menglon",
        "26.Mercurol",
        "01.Meximieux",
        "07.Meysse",
        "38.Meyssies",
        "69.Meyzieux",
        "69.Millery",
        "01.Mionnay",
        "01.Miribel",
        "69.Moins",
        "38.Moissieu-sur-Dolon",
        "26.Molieres-Glandaz",
        "84.Mondragon",
        "69.Montagny",
        "69.Montanay",
        "26.Montboucher-sur-Jabron",
        "42.Montbrison",
        "26.Montelier",
        "26.Montelimar",
        "30.Montfrin",
        "01.Montluel",
        "26.Montmeyran",
        "42.Montrond-les-Bains",
        "26.Montvendre",
        "42.Montverdun",
        "26.Moras-en-Valloire",
        "84.Mornas",
        "26.Mureils",
        "42.Neaux",
        "42.Neulise",
        "69.Neuville-sur-Saone",
        "01.Neyron",
        "01.Nievroz",
        "38.Nivolas-Vermelle",
        "42.Noiretable",
        "42.Notre-Dame-de-Boisset",
        "69.Nuelles",
        "84.Orange",
        "69.Oullins",
        "13.Orgon",
        "07.Ozon",
        "38.Pact",
        "01.Parcieux",
        "42.Parigny",
        "38.Penol",
        "01.Peronnas",
        "01.Perouges",
        "42.Perreux",
        "07.Peyraud",
        "26.Piegrod-la-Clastre",
        "69.Pierre-Benite",
        "26.Pierrelatte",
        "42.Pinay",
        "84.Piolenc",
        "13.Plan-d-Orgon",
        "69.Pommiers",
        "26.Ponet-et-Saint-Auban",
        "26.Ponsas",
        "26.Pontaix",
        "26.Pont-de-l-Isere",
        "43.Pont-Salomon",
        "69.Pontcharra-sur-Turdine",
        "26.Portes",
        "42.Pouilly-les-Feurs",
        "42.Pouilly-sous-Charlieu",
        "69.Poule-les-Echarmeaux",
        "42.Pradines",
        "38.Primarette",
        "07.Privas",      
        "30.Pujaut",
        "69.Pusignan",                
        "69.Quincieux",
        "26.Recourbeau",
        "30.Redessan",
        "42.Regny",
        "38.Reventin-Vaugris",
        "01.Reyrieux",
        "01.Rignieu-le-desert",
        "69.Rilleux-la-Pape",
        "42.Riorges",
        "42.Rive-de-Gier",
        "42.Roanne",
        "30.Rochefort-du-Gard",
        "07.Rochemaure",
        "07.Rochessauve",
        "69.Rochetaillee",
        "71.Romaneche",
        "26.Romans-sur-Isere",
        "07.Rompon",
        "30.Roquemaure",
        "26.Roussas",
        "38.Roussillon",
        "26.Roynac",
        "38.Sablons",
        "26.Saillans",
        "69.Sain-Bel",
        "38.Saint-Alban-de-Roche",
        "01.Saint-Andre-de-Corcy",
        "42.Saint-Andre-le-Puy",
        "38.Saint-Bartelemy",
        "13.Saint-Cannat",
        "42.Saint-Chamond",
        "38.Saint-Clair-du-Rhone",
        "42.Saint-Cyr-de-Favieres",
        "07.Saint-Desirat",
        "38.Saint-Didier-de-la-Tour",
        "43.Saint-Didier-en-Velay",
        "42.Saint-Didier-sur-Rochefort",
        "42.Saint-Etienne",
        "38.Saint-Etienne-de-saint-geoirs",
        "30.Saint-Etienne-des-Sorts",
        "43.Saint-Ferreol-d-Auroure",
        "69.Saint-Fons",
        "69.Saint-Forgeux",
        "42.Saint-Galmier",
        "69.Saint-Genis-l-Argentiere",
        "38.Saint-Georges-d-Esperanche",
        "69.Saint-Georges-de-Reneins",
        "42.Saint-Georges-en-Couzan",
        "42.Saint-Georges-Haute-Ville",
        "07.Saint-Georges-les-Bains",
        "69.Saint-Germain-au-Mont-d-Or",
        "42.Saint-Germain-la-Montagne",
        "42.Saint-Germain-Lespinasse",
        "01.Saint-Germain-sur-Renon",
        "42.Saint-Haon-le-Vieux",
        "69.Saint-Jean-d-Ardieres",
        "07.Saint-Jean-de-Muzols",
        "38.Saint-Jean-de-Soudain",
        "42.Saint-Jean-la-Vetre",
        "42.Saint-Jodard",
        "42.Saint-Julien-la-Vetre",
        "69.Saint-Just-d-Avray",
        "43.Saint-Just-Malmont",
        "42.Saint-Just-Saint-Rambert",
        "42.Saint-Just-sur-Loire",
        "07.Saint-Lager-Bressac",
        "38.Saint-Lattier",
        "69.Saint-Laurent-de-Mure",
        "69.Saint-Laurent-d-Oingt",
        "42.Saint-Laurent-la-Conche",
        "42.Saint-Laurent-Rochefort",
        "01.Saint-Marcel",
        "07.Saint-Marcel-d-Ardeche",
        "42.Saint-Marcel-de-Felines",
        "69.Saint-Marcel-l-Eclaire",
        "26.Saint-Marcel-les-Valence",
        "42.Saint-Marcellin-en-Forez",
        "42.Saint-Martin-d-Estreaux",
        "01.Saint-Maurice-de-Beynost",
        "38.Saint-Maurice-l-Exil",
        "42.Saint-Michel-sur-Rhone",
        "07.Saint-Montant",
        "69.Saint-Nizier-d-Azergues",
        "42.Saint-Nizier-sous-Charlieu",
        "43.Saint-Pal-de-Mons",
        "01.Saint-Paul-de-Varax",
        "42.Saint-Paul-en-Cornillon",
        "42.Saint-Paul-en-Jarez",
        "26.Saint-Paul-les-Romans",
        "07.Saint-Peray",
        "42.Saint-Pierre-de-Boeuf",
        "69.Saint-Pierre-de-Chandieu",
        "42.Saint-Pierre-la-Noaille",
        "03.Saint-Pierre-Laval",
        "07.Saint-Priest",
        "69.Saint-Priest",
        "42.Saint-Priest-en-Jarez",
        "42.Saint-Priest-la-Roche",
        "42.Saint-Priest-la-Vetre",
        "38.Saint-Prim",
        "38.Saint-Quentin-Fallavier",
        "26.Saint-Rambert-d-Albon",
        "69.Saint-Romain-au-Mont-d-Or",
        "69.Saint-Romain-de-Popey",
        "69.Saint-Romain-en-Gal",
        "69.Saint-Romain-en-Gier",
        "42.Saint-Romain-la-Motte",
        "42.Saint-Romain-le-Puy",
        "43.Saint-Romain-Lachalm",
        "26.Saint-Roman",
        "26.Saint-Sauveur-en-Diois",
        "38.Saint-Simeon-de-Bressieux",
        "26.Saint-Sorlin-en-Valloire",
        "42.Saint-Symphorien-de-Lay",
        "42.Saint-Thurin",
        "26.Saint-Vallier",
        "38.Saint-Victor-de-Cessieu",
        "42.Saint-Victor-sur-Rhins",
        "69.Sainte-Colombe",
        "26.Sainte-Croix",
        "69.Sainte-Foy-l-Argentiere",
        "38.Salaise-sur-Sanne",
        "38.Sardieu",
        "07.Sarras",
        "69.Sathonay-Camp",
        "69.Sathonay-Village",
        "38.Savas-Mepin",
        "42.Savignieux",
        "69.Savigny",
        "30.Saze",
        "26.Saulce-sur-Rhone",
        "26.Sauzet",
        "26.Savasse",
        "13.Senas",
        "38.Serezin-de-la-Tour",
        "69.Serezin-du-Rhone",
        "07.Serrieres",
        "01.Servas",
        "26.Serves",
        "38.Seyssuel",
        "38.Sillans",
        "69.Solaize",
        "69.Souzy",
        "07.Soyons",
        "42.Sury-le-Comtal",
        "26.Tain-l-Hermitage",
        "69.Tarare",
        "42.Tartaras",
        "69.Tassin-la-Demi-Lune",
        "30.Tavel",
        "69.Ternand",
        "69.Ternay",
        "42.Terrenoire",
        "30.Theziers",
        "01.Thil",
        "07.Tournon-sur-Rhone",
        "01.Tramoyes",
        "42.Trelins",
        "69.Treves",
        "01.Trevoux",
        "69.Tupin-et-Semons",
        "42.Unieux",
        "26.Upie",
        "26.Valence",
        "69.Vaulx-en-Velin",
        "30.Vallabregues",
        "38.Vaulx-Milieu",
        "26.Vaunaveys-la-Rochette",
        "42.Veauche",
        "42.Vendranges",
        "30.Venejan",
        "69.Venissieux",
        "13.Ventabren",
        "26.Vercheny",
        "42.Verin",
        "69.Vernaison",
        "13.Vernegues",
        "38.Vienne",
        "38.Vilette-d-Anthon",
        "42.Villars",
        "01.Villars-les-Dombes",
        "38.Villefontaine",
        "69.Villefranche-sur-Saone",
        "69.Villeurbanne",
        "01.Villieu-Loyes-Mollon",
        "07.Vion",
        "07.Viviers",
        "42.Vougy",
        "69.Vourles",
};
    
    
    public static String[] CITIES_DEPARTEMENTS_NULL = new String[]{
                "42.Ailleux",
                "26.Aix-en-Diois",
        		"13.Aix-en-Provence",
        		"26.Aixan",
        		"07.Alba",
        		"69.Albigny-sur-Saone",
        		"26.Alixan",
        		"26.Allan",
        		"13.Alleins",
        		"26.Allex",
        		"69.Amberieux",
        		"42.Ambierle",
        		"69.Amplepuis",
        		"69.Ampuis",
        		"07.Andance",
        		"26.Andancette",
        		"42.Andrezieux-Boutheon",
        		"26.Anneyron",
        		"69.Anse",
                "26.Aouste-sur-Sye",
                "30.Aramon",
                "69.Arnas",
                "07.Arras-sur-Rhone",
                "26.Aubenasson",
                "07.Aubignas",
                "84.Avignon",
                "07.Baix",
                "01.Balan",   
                "42.Balbigny",
                "26.Barnave",               
                "07.Beauchastel",        
                "38.Beaufort",               
                "38.Beaurepaire",            
                "26.Beaurieres",            
                "38.Beauvoir-de-Marc",     
                "42.Belleroche",
                "69.Belleville-sur-Saone",
                "01.Beligneux",    
                "69.Bessenay",
                "01.Beynost",    
                "42.Boen-sur-Lignon",
                "42.Boisset-les-Montrond",
                "84.Bollene",
                "26.Bonlieu-sur-Roubion",
                "38.Bonnefamille",   
                "42.Bonson",
                "38.Bouge-Chambalud",    
                "26.Bourg-les-Valence",          
                "07.Bourg-Saint-Andeol",            
                "38.Bourgoin-Jallieu",          
                "26.Bren",
                "38.Brezins",  
                "69.Brignais",
                "69.Brussieu",
                "13.Cabries",     
                "84.Caderousse",
                "69.Cailloux-sur-Fontaines",
                "69.Caluire-et-Cuire",
                "84.Caumont-sur-Durance",
                "84.Cavaillon",
                "42.Cervieres",
                "38.Cessieu",             
                "26.Chabeuil",            
                "26.Chabrillan",  
                "42.Chalain-d-Uzore",
                "42.Chamboeuf",
                "69.Chambost-Allieres",
                "69.Chamelet",
                "07.Champagne",    
                "42.Champdieu",
                "38.Chanas",
                "42.Changy",
                "69.Chaponost",
                "38.Charantonnay",   
                "69.Charbonnieres-les-Bains",
                "42.Charlieu",
                "07.Charmes-sur-Rhone",      
                "07.Charnas",               
                "38.Chasse-sur-Rhone",
                "69.Chasse-sur-Rhone",
                "69.Chasselay",
                "07.Chateaubourg", 
                "42.Chateauneuf",
                "26.Chateauneuf-de-Galaure",
                "26.Chateauneuf-du-Rhone",            
                "26.Chateauneuf-sur-Isere",   
                "69.Chatillon",
                "42.Chavanay",
                "26.Chavannes",  
                "69.Chessy",
                "84.Cheval-Blanc",
                "69.Chevinay",
                "07.Chomerac",         
                "38.Chonas-l-Amballan",   
                "42.Civens",
                "01.Civrieux",
                "69.Civrieux-d-Azergues",
                "69.Claveisolles",
                "26.Claveyson",             
                "26.Clerieux",       
                "38.Clonas-sur-Vareze",  
                "69.Collonges-au-Mont-d-Or",
                "69.Colombier-Saugnieu",
                "42.Commelle-Vernay",
                "30.Comps",  
                "69.Condrieu",
                "69.Corcelles",
                "07.Cornas",      
                "38.Cour-et-Buis",
                "69.Courzieu",
                "69.Couzon-au-Mont-d-Or",
                "26.Crest",           
                "26.Crozes-Hermitage",         
                "07.Cruas",
                "69.Curis-au-Mont-d-Or",
                "42.Cuzieu",
                "01.Dagneux",   
                "69.Dardilly",
                "42.Debats",
                "69.Decines",
                "26.Die",          
                "38.Diemoz",
                "26.Divajeu", 
                "38.Domarin",
                "30.Domazan",   
                "69.Dommartin",
                "26.Donzere",   
                "43.Dunieres",
                "69.Echalas",
                "69.Ecully",
                "13.Eguilles",  
                "42.Epercieux-Saint-Paul",
                "26.Epinouze",
                "26.Erome",              
                "26.Espeluche",
                "26.Espenel",              
                "26.Etoile-sur-Rhone",
                "26.Eurre",  
                "69.Eveux",
                "42.Feurs",
                "69.Feyzin",
                "42.Firminy",
                "69.Fleurieu-sur-Saone",
                "69.Fleurieux-sur-l-Arbresle",
                "69.Fontaines-Saint-Martin",
                "69.Fontaines-sur-Saone",
                "26.Fourcinet",
                "42.Fraisses",
                "69.Francheville",
                "69.Genay",
                "26.Gervans",    
                "69.Givors",
                "07.Glun",              
                "26.Granges-les-Beaumont",   
                "38.Grenay",
                "69.Grigny",
                "07.Guilherand",   
                "69.Irigny",
                "26.Jansac",               
                "30.Jonquierres-Saint-Vincent",   
                "69.Jons",
                "69.L-Arbresle",
                "42.L-Hopital-sous-Rochefort",
                "38.l-Isle-d-Abeau",
                "13.La-Barben",             
                "01.La-Boisse",     
                "38.La-Cote-Saint-Andre",
                "26.La-Coucourde",    
                "42.La-Fouillouse",
                "26.La-Garde-Adhemar",     
                "42.La-Grand-Croix",
                "26.La-Laupie",              
                "26.La-Motte-de-Galaure",   
                "69.La-Mulatiere",
                "42.La-Pacaudiere",
                "42.La-Ricamarie",
                "43.La-Seauve-sur-Semene",
                "69.La-Tour-de-Salvagny",
                "38.La-Tour-du-Pin",
                "38.La-Verpilliere",
                "07.La-Voulte-sur-Rhone",    
                "84.Lamotte-du-Rhone",
                "13.Lambesc", 
                "69.Lamure-sur-Azergues",
                "69.Lancie",
                "84.Lapalud",
                "01.Lapeyrouse",             
                "26.Lapeyrouse-Mornay",            
                "26.La-Roche-de-Glun",        
                "26.La-Roche-sur-Grane", 
                "26.Laval-d-Aix",            
                "26.Laveyron",   
                "69.Le-Bois-d-Oingt",
                "69.Le-Breuil",
                "42.Le-Chambon-Feugerolles",
                "42.Le-Coteau",
                "42.Le-Crozet",
                "38.Le-Peage-de-Roussillon",
                "01.Le-Plantay",            
                "07.Le-Pouzin",        
                "07.Le-Teil",  
                "69.Legny",
                "42.Leigneux",
                "07.Lemps",      
                "26.Lens-Lestang",
                "69.Lentilly",
                "30.Les-Angles",   
                "69.Les-Cheres",
                "26.Les-Granges-Gontardes",             
                "13.Les-Pennes-Mirabeau",   
                "38.Les-Roches",
                "69.Les-Sauvages",
                "26.Les-Tourrettes",               
                "26.Lesches-en-Diois",    
                "69.Letra",
                "69.Limas",
                "69.Limonest",
                "07.Limony",
                "69.Lissieu",
                "26.Livron",
                "69.Loire-sur-Rhone",
                "69.Longes",
                "42.Lorette",
                "26.Loriol-sur-Drome",
                "69.Lozanne",
                "26.Luc-en-Diois",
                "69.Lyon",
                "42.Mably",
                "26.Malataverne",
                "13.Mallemort",
                "30.Manduel",
                "26.Manthes",
                "38.Marcilloles",
                "69.Marcilly-d-Azergues",
                "42.Marcilly-le-Chatel",
                "42.Marclopt",
                "69.Marcy-l-Etoile",
                "01.Marlieux",
                "26.Marsanne",
                "26.Marsaz",
                "13.Marseille",
                "01.Massieux",
                "07.Mauves",
                "26.Menglon",
                "26.Mercurol",
                "01.Meximieux",
                "07.Meysse",
                "38.Meyssies",
                "69.Meyzieux",
                "69.Millery",
                "01.Mionnay",
                "01.Miribel",
                "69.Moins",
                "38.Moissieu-sur-Dolon",
                "26.Molieres-Glandaz",
                "84.Mondragon",
                "69.Montagny",
                "69.Montanay",
                "26.Montboucher-sur-Jabron",
                "42.Montbrison",
                "26.Montelier",
                "26.Montelimar",
                "30.Montfrin",
                "01.Montluel",
                "26.Montmeyran",
                "42.Montrond-les-Bains",
                "26.Montvendre",
                "42.Montverdun",
                "26.Moras-en-Valloire",
                "84.Mornas",
                "26.Mureils",
                "42.Neaux",
                "42.Neulise",
                "69.Neuville-sur-Saone",
                "01.Neyron",
                "01.Nievroz",
                "38.Nivolas-Vermelle",
                "42.Noiretable",
                "42.Notre-Dame-de-Boisset",
                "69.Nuelles",
                "84.Orange",
                "69.Oullins",
                "13.Orgon",
                "07.Ozon",
                "38.Pact",
                "01.Parcieux",
                "42.Parigny",
                "38.Penol",
                "01.Peronnas",
                "01.Perouges",
                "42.Perreux",
                "07.Peyraud",
                "26.Piegrod-la-Clastre",
                "69.Pierre-Benite",
                "26.Pierrelatte",
                "42.Pinay",
                "84.Piolenc",
                "13.Plan-d-Orgon",
                "69.Pommiers",
                "26.Ponet-et-Saint-Auban",
                "26.Ponsas",
                "26.Pontaix",
                "26.Pont-de-l-Isere",
                "43.Pont-Salomon",
                "69.Pontcharra-sur-Turdine",
                "26.Portes",
                "42.Pouilly-les-Feurs",
                "42.Pouilly-sous-Charlieu",
                "69.Poule-les-Echarmeaux",
                "42.Pradines",
                "38.Primarette",
                "07.Privas",      
                "30.Pujaut",
                "69.Pusignan",                
                "69.Quincieux",
                "26.Recourbeau",
                "30.Redessan",
                "42.Regny",
                "38.Reventin-Vaugris",
                "01.Reyrieux",
                "01.Rignieu-le-desert",
                "69.Rilleux-la-Pape",
                "42.Riorges",
                "42.Rive-de-Gier",
                "42.Roanne",
                "30.Rochefort-du-Gard",
                "07.Rochemaure",
                "07.Rochessauve",
                "69.Rochetaillee",
                "71.Romaneche",
                "26.Romans-sur-Isere",
                "07.Rompon",
                "30.Roquemaure",
                "26.Roussas",
                "38.Roussillon",
                "26.Roynac",
                "38.Sablons",
                "26.Saillans",
                "69.Sain-Bel",
                "38.Saint-Alban-de-Roche",
                "01.Saint-Andre-de-Corcy",
                "42.Saint-Andre-le-Puy",
                "38.Saint-Bartelemy",
                "13.Saint-Cannat",
                "42.Saint-Chamond",
                "38.Saint-Clair-du-Rhone",
                "42.Saint-Cyr-de-Favieres",
                "07.Saint-Desirat",
                "38.Saint-Didier-de-la-Tour",
                "43.Saint-Didier-en-Velay",
                "42.Saint-Didier-sur-Rochefort",
                "42.Saint-Etienne",
                "38.Saint-Etienne-de-saint-geoirs",
                "30.Saint-Etienne-des-Sorts",
                "43.Saint-Ferreol-d-Auroure",
                "69.Saint-Fons",
                "69.Saint-Forgeux",
                "42.Saint-Galmier",
                "69.Saint-Genis-l-Argentiere",
                "38.Saint-Georges-d-Esperanche",
                "69.Saint-Georges-de-Reneins",
                "42.Saint-Georges-en-Couzan",
                "42.Saint-Georges-Haute-Ville",
                "07.Saint-Georges-les-Bains",
                "69.Saint-Germain-au-Mont-d-Or",
                "42.Saint-Germain-la-Montagne",
                "42.Saint-Germain-Lespinasse",
                "01.Saint-Germain-sur-Renon",
                "42.Saint-Haon-le-Vieux",
                "69.Saint-Jean-d-Ardieres",
                "07.Saint-Jean-de-Muzols",
                "38.Saint-Jean-de-Soudain",
                "42.Saint-Jean-la-Vetre",
                "42.Saint-Jodard",
                "42.Saint-Julien-la-Vetre",
                "69.Saint-Just-d-Avray",
                "43.Saint-Just-Malmont",
                "42.Saint-Just-Saint-Rambert",
                "42.Saint-Just-sur-Loire",
                "07.Saint-Lager-Bressac",
                "38.Saint-Lattier",
                "69.Saint-Laurent-de-Mure",
                "69.Saint-Laurent-d-Oingt",
                "42.Saint-Laurent-la-Conche",
                "42.Saint-Laurent-Rochefort",
                "01.Saint-Marcel",
                "07.Saint-Marcel-d-Ardeche",
                "42.Saint-Marcel-de-Felines",
                "69.Saint-Marcel-l-Eclaire",
                "26.Saint-Marcel-les-Valence",
                "42.Saint-Marcellin-en-Forez",
                "42.Saint-Martin-d-Estreaux",
                "01.Saint-Maurice-de-Beynost",
                "38.Saint-Maurice-l-Exil",
                "42.Saint-Michel-sur-Rhone",
                "07.Saint-Montant",
                "69.Saint-Nizier-d-Azergues",
                "42.Saint-Nizier-sous-Charlieu",
                "43.Saint-Pal-de-Mons",
                "01.Saint-Paul-de-Varax",
                "42.Saint-Paul-en-Cornillon",
                "42.Saint-Paul-en-Jarez",
                "26.Saint-Paul-les-Romans",
                "07.Saint-Peray",
                "42.Saint-Pierre-de-Boeuf",
                "69.Saint-Pierre-de-Chandieu",
                "42.Saint-Pierre-la-Noaille",
                "03.Saint-Pierre-Laval",
                "07.Saint-Priest",
                "69.Saint-Priest",
                "42.Saint-Priest-en-Jarez",
                "42.Saint-Priest-la-Roche",
                "42.Saint-Priest-la-Vetre",
                "38.Saint-Prim",
                "38.Saint-Quentin-Fallavier",
                "26.Saint-Rambert-d-Albon",
                "69.Saint-Romain-au-Mont-d-Or",
                "69.Saint-Romain-de-Popey",
                "69.Saint-Romain-en-Gal",
                "69.Saint-Romain-en-Gier",
                "42.Saint-Romain-la-Motte",
                "42.Saint-Romain-le-Puy",
                "43.Saint-Romain-Lachalm",
                "26.Saint-Roman",
                "26.Saint-Sauveur-en-Diois",
                "38.Saint-Simeon-de-Bressieux",
                "26.Saint-Sorlin-en-Valloire",
                "42.Saint-Symphorien-de-Lay",
                "42.Saint-Thurin",
                "26.Saint-Vallier",
                "38.Saint-Victor-de-Cessieu",
                "42.Saint-Victor-sur-Rhins",
                "69.Sainte-Colombe",
                "26.Sainte-Croix",
                "69.Sainte-Foy-l-Argentiere",
                "38.Salaise-sur-Sanne",
                "38.Sardieu",
                "07.Sarras",
                "69.Sathonay-Camp",
                "69.Sathonay-Village",
                "38.Savas-Mepin",
                "42.Savignieux",
                "69.Savigny",
                "30.Saze",
                "26.Saulce-sur-Rhone",
                "26.Sauzet",
                "26.Savasse",
                "13.Senas",
                "38.Serezin-de-la-Tour",
                "69.Serezin-du-Rhone",
                "07.Serrieres",
                "01.Servas",
                "26.Serves",
                "38.Seyssuel",
                "38.Sillans",
                "69.Solaize",
                "69.Souzy",
                "07.Soyons",
                "42.Sury-le-Comtal",
                "26.Tain-l-Hermitage",
                "69.Tarare",
                "42.Tartaras",
                "69.Tassin-la-Demi-Lune",
                "30.Tavel",
                "69.Ternand",
                "69.Ternay",
                "42.Terrenoire",
                "30.Theziers",
                "01.Thil",
                "07.Tournon-sur-Rhone",
                "01.Tramoyes",
                "42.Trelins",
                "69.Treves",
                "01.Trevoux",
                "69.Tupin-et-Semons",
                "42.Unieux",
                "26.Upie",
                "26.Valence",
                "69.Vaulx-en-Velin",
                "30.Vallabregues",
                "38.Vaulx-Milieu",
                "26.Vaunaveys-la-Rochette",
                "42.Veauche",
                "42.Vendranges",
                "30.Venejan",
                "69.Venissieux",
                "13.Ventabren",
                "26.Vercheny",
                "42.Verin",
                "69.Vernaison",
                "13.Vernegues",
                "38.Vienne",
                "38.Vilette-d-Anthon",
                "42.Villars",
                "01.Villars-les-Dombes",
                "38.Villefontaine",
                "69.Villefranche-sur-Saone",
                "69.Villeurbanne",
                "01.Villieu-Loyes-Mollon",
                "07.Vion",
                "07.Viviers",
                "42.Vougy",
                "69.Vourles",
    };
	
    public static String[] CITIES_DEPARTEMENTS_00 = new String[]{
    	"00.Chene-Bougeries",
    	"00.Geneve",
    	"00.Thonex"

	 };
    
    public static String[] CITIES_DEPARTEMENTS_01_ALPES = new String[]{
    	"01.Amberieu-en-Bugey",
    	"01.Ambronay",
    	"01.Ambutrix",
    	"01.Anglefort",
    	"01.Arbent",
    	"01.Argis",
    	"01.Artemare",
    	"01.Bellegarde-sur-Valserine",
    	"01.Belley",
    	"01.Bellignat",
    	"01.Belmont-Luthezieu",
    	"01.Beny",
    	"01.Beon",
    	"01.Bettant",
    	"01.Billiat",
    	"01.Bolozon",
    	"01.Bourg-en-Bresse",
    	"01.Brens",
    	"01.Brion",
    	"01.Certines",
    	"01.Cessy",
    	"01.Ceyzeriat",
    	"01.Challex",
    	"01.Chanay",
    	"01.Chatillon-en-Michaille",
    	"01.Chazey-Bons",
    	"01.Cheignieu-la-Balme",
    	"01.Chevry",
    	"01.Clarafond-Arcine",
    	"01.Coligny",
    	"01.Collonges",
    	"01.Corbonod",
    	"01.Corveissiat",
    	"01.Crottet",
    	"01.Crozet",
    	"01.Culoz",
    	"01.Divonne-les-Bains",
    	"01.Dortan",
    	"01.Druillat",
    	"01.Echenevex",
    	"01.Farges",
    	"01.Gex",
    	"01.Grieges",
    	"01.Grilly",
    	"01.Groissiat",
    	"01.Injoux-Genissiat",
    	"01.La-Burbanche",
    	"01.Lagnieu",
    	"01.Lalleyriat",
    	"01.Le-Poizat",
    	"01.Leaz",
    	"01.Les-Neyrolles",
    	"01.Leyment",
    	"01.Magnieu",
    	"01.Martignat",
    	"01.Mezeriat",
    	"01.Montagnat",
    	"01.Montreal-la-Cluse",
    	"01.Nantua",
    	"01.Nurieux-Volognat",
    	"01.Oyonnax",
    	"01.Peron",
    	"01.Peronnas",
    	"01.Perrex",
    	"01.Polliat",
    	"01.Pont-d'Ain",
    	"01.Port",
    	"01.Pougny",
    	"01.Pugieu",
    	"01.Ramasse",
    	"01.Revonnas",
    	"01.Rossillon",
    	"01.Saint-Denis-en-Bugey",
    	"01.Saint-Etienne-du-Bois",
    	"01.Saint-Genis-Pouilly",
    	"01.Saint-Germain-de-Joux",
    	"01.Saint-Jean-de-Gonville",
    	"01.Saint-Jean-le-Vieux",
    	"01.Saint-Jean-sur-Veyle",
    	"01.Saint-Just",
    	"01.Saint-Martin-de-Bavel",
    	"01.Saint-Martin-du-Mont",
    	"01.Saint-Rambert-en-Bugey",
    	"01.Saint-Sorlin-en-Bugey",
    	"01.Salavre",
    	"01.Sault-Brenaz",
    	"01.Sergy",
    	"01.Seyssel",
    	"01.Simandre-sur-Suran",
    	"01.Surjoux",
    	"01.Talissieu",
    	"01.Tenay",
    	"01.Thoiry",
    	"01.Torcieu",
    	"01.Tossiat",
    	"01.Vaux-en-Bugey",
    	"01.Villemotier",
    	"01.Villereversure",
    	"01.Viriat",
    	"01.Virieu-le-Grand",
    	"01.Vonnas"
    };
    	
    
    public static String[] CITIES_DEPARTEMENTS_01_RHONE = new String[]{
        	 "01.Balan",
	         "01.Beligneux",
	         "01.Beynost",
	         "01.Civrieux",
	         "01.Dagneux",
	         "01.La-Boisse",
	         "01.Lapeyrouse",
	         "01.Le-Plantay",
	         "01.Marlieux",
	         "01.Massieux",
	         "01.Meximieux",
	         "01.Mionnay",
	         "01.Miribel",
	         "01.Montluel",
	         "01.Neyron",
	         "01.Nievroz",
	         "01.Parcieux",
	         "01.Peronnas",
	         "01.Perouges",
	         "01.Reyrieux",
	         "01.Rignieu-le-desert",
	         "01.Saint-Andre-de-Corcy",
	         "01.Saint-Germain-sur-Renon",
	         "01.Saint-Marcel",
	         "01.Saint-Maurice-de-Beynost",
	         "01.Saint-Paul-de-Varax",
	         "01.Servas",
	         "01.Thil",
	         "01.Tramoyes",
	         "01.Trevoux",
	         "01.Villars-les-Dombes",
	         "01.Villieu-Loyes-Mollon"
	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_03 = new String[]{
		 "03.Saint-Pierre-Laval"
	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_07 = new String[]{
		 "07.Alba",
 		"07.Andance",
         "07.Arras-sur-Rhone",
         "07.Aubignas",
         "07.Baix",
         "07.Beauchastel",
         "07.Bourg-Saint-Andeol",
         "07.Champagne",
         "07.Charmes-sur-Rhone",
         "07.Charnas",
         "07.Chateaubourg",
         "07.Chomerac",
         "07.Cornas",
         "07.Cruas",
         "07.Glun",
         "07.Guilherand",
         "07.La-Voulte-sur-Rhone",
         "07.Lemps",
         "07.Le-Pouzin",
         "07.Le-Teil",
         "07.Limony",
         "07.Mauves",
         "07.Meysse",
         "07.Ozon",
         "07.Peyraud",
         "07.Privas",
         "07.Rochemaure",
         "07.Rochessauve",
         "07.Rompon",
         "07.Saint-Desirat",
         "07.Saint-Georges-les-Bains",
         "07.Saint-Jean-de-Muzols",
         "07.Saint-Lager-Bressac",
         "07.Saint-Marcel-d-Ardeche",
         "07.Saint-Montant",
         "07.Saint-Peray",
         "07.Saint-Priest",
         "07.Sarras",
         "07.Serrieres",
         "07.Soyons",
         "07.Tournon-sur-Rhone",
         "07.Vion",
         "07.Viviers",
	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_13 = new String[]{
		 "13.Aix-en-Provence",
         "13.Alleins",
         "13.Cabries",
         "13.Eguilles",
         "13.La-Barben",
         "13.Lambesc",
         "13.Les-Pennes-Mirabeau",
         "13.Mallemort",
         "13.Marseille",
         "13.Orgon",
         "13.Plan-d-Orgon",
         "13.Saint-Cannat",
         "13.Senas",
         "13.Ventabren",
         "13.Vernegues"
	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_26_RHONE = new String[]{
		 "26.Aixan",
         "26.Aix-en-Diois",
         "26.Alixan",
         "26.Allan",
         "26.Allex",
         "26.Andancette",
         "26.Anneyron",
         "26.Aouste-sur-Sye",
         "26.Aubenasson",
         "26.Barnave",
         "26.Beaurieres",
         "26.Bonlieu-sur-Roubion",
         "26.Bourg-les-Valence",
         "26.Bren",
         "26.Chabeuil",
         "26.Chabrillan",
         "26.Chateauneuf-de-Galaure",
         "26.Chateauneuf-du-Rhone",
         "26.Chateauneuf-sur-Isere",
         "26.Chavannes",
         "26.Claveyson",
         "26.Clerieux",
         "26.Crest",
         "26.Crozes-Hermitage",
         "26.Die",
         "26.Divajeu",
         "26.Donzere",
         "26.Epinouze",
         "26.Erome",
         "26.Espeluche",
         "26.Espenel",
         "26.Etoile-sur-Rhone",
         "26.Eurre",
         "26.Fourcinet",
         "26.Gervans",
         "26.Granges-les-Beaumont",
         "26.Jansac",
         "26.La-Coucourde",
         "26.La-Garde-Adhemar",
         "26.La-Laupie",
         "26.La-Motte-de-Galaure",
         "26.Lapeyrouse-Mornay",
         "26.La-Roche-de-Glun",
         "26.La-Roche-sur-Grane",
         "26.Laval-d-Aix",
         "26.Laveyron",
         "26.Lens-Lestang",
         "26.Lesches-en-Diois",
         "26.Les-Granges-Gontardes",
         "26.Les-Tourrettes",
         "26.Livron",
         "26.Loriol-sur-Drome",
         "26.Luc-en-Diois",
         "26.Malataverne",
         "26.Manthes",
         "26.Marsanne",
         "26.Marsaz",
         "26.Menglon",
         "26.Mercurol",
         "26.Molieres-Glandaz",
         "26.Montboucher-sur-Jabron",
         "26.Montelier",
         "26.Montelimar",
         "26.Montmeyran",
         "26.Montvendre",
         "26.Moras-en-Valloire",
         "26.Mureils",
         "26.Piegrod-la-Clastre",
         "26.Pierrelatte",
         "26.Ponet-et-Saint-Auban",
         "26.Ponsas",
         "26.Pontaix",
         "26.Pont-de-l-Isere",
         "26.Portes",
         "26.Recourbeau",
         "26.Romans-sur-Isere",
         "26.Roussas",
         "26.Roynac",
         "26.Saillans",
         "26.Sainte-Croix",
         "26.Saint-Marcel-les-Valence",
         "26.Saint-Paul-les-Romans",
         "26.Saint-Rambert-d-Albon",
         "26.Saint-Roman",
         "26.Saint-Sauveur-en-Diois",
         "26.Saint-Sorlin-en-Valloire",
         "26.Saint-Vallier",
         "26.Saulce-sur-Rhone",
         "26.Sauzet",
         "26.Savasse",
         "26.Serves",
         "26.Tain-l-Hermitage",
         "26.Upie",
         "26.Valence",
         "26.Vaunaveys-la-Rochette",
         "26.Vercheny",
	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_26_ALPES = new String[]{
		 "26.Lus-la-Croix-Haute"
	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_30 = new String[]{
		 "30.Aramon",
         "30.Comps",
         "30.Domazan",
         "30.Jonquierres-Saint-Vincent",
         "30.Les-Angles",
         "30.Manduel",
         "30.Montfrin",
         "30.Pujaut",
         "30.Redessan",
         "30.Rochefort-du-Gard",
         "30.Roquemaure",
         "30.Saint-Etienne-des-Sorts",
         "30.Saze",
         "30.Tavel",
         "30.Theziers",
         "30.Vallabregues",
         "30.Venejan"
	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_38_ALPES = new String[]{
		 "38.Avignonet",
		 "38.Beaucroissant",
		 "38.Beaulieu",
		 "38.Blandin",
		 "38.Blandin",
		 "38.Chabons",
		 "38.Champagnier",
		 "38.Champ-sur-Drac",
		 "38.Chelieu",
		 "38.Clelles",
		 "38.Coublevie",
		 "38.Domene",
		 "38.Echirolles",
		 "38.Eybens",
		 "38.Fitilieu",
		 "38.Fontanil-Cornillon",
		 "38.Froges",
		 "38.Gieres",
		 "38.Goncelin",
		 "38.Grenoble",
		 "38.Izeaux",
		 "38.Jarrie",
		 "38.La-Pierre",
		 "38.La-Sone",
		 "38.L'Albenc",
		 "38.Lalley",
		 "38.Le-Champ-pres-Froges",
		 "38.Le-Cheylas",
		 "38.Le-Grand-Lemps",
		 "38.Le-Monestier-du-Percy",
		 "38.Le-Passage",
		 "38.Le-Pont-de-Beauvoisin",
		 "38.Le-Pont-de-Claix",
		 "38.Les-Abrets",
		 "38.Moirans",
		 "38.Monestier-de-Clermont",
		 "38.Murianette",
		 "38.Panissage",
		 "38.Percy",
		 "38.Polienas",
		 "38.Pontcharra",
		 "38.Pressins",
		 "38.Reaumont",
		 "38.Rives",
		 "38.Roissard",
		 "38.Saint-Andre-le-Gaz",
		 "38.Saint-Blaise-du-Buis",
		 "38.Saint-Cassien",
		 "38.Saint-Didier-de-la-Tour",
		 "38.Saint-Egreve",
		 "38.Saint-Georges-de-Commiers",
		 "38.Saint-Hilaire-du-Rosier",
		 "38.Saint-Jean-de-Moirans",
		 "38.Saint-Lattier",
		 "38.Saint-Marcellin",
		 "38.Saint-Martin-de-Clelles",
		 "38.Saint-Martin-de-la-Cluze",
		 "38.Saint-Martin-d'Heres",
		 "38.Saint-Martin-le-Vinoux",
		 "38.Saint-Maurice-en-Trieves",
		 "38.Saint-Michel-les-Portes",
		 "38.Saint-Sauveur",
		 "38.Sinard",
		 "38.Teche",
		 "38.Tencin",
		 "38.Tullins",
		 "38.Vif",
		 "38.Villard-Bonnot",
		 "38.Vinay",
		 "38.Voiron",
		 "38.Voreppe",
		 "38.Vourey"
	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_38_RHONE = new String[]{
		 "38.Beaufort",
         "38.Beaurepaire",
         "38.Beauvoir-de-Marc",
         "38.Bonnefamille",
         "38.Bouge-Chambalud",
         "38.Bourgoin-Jallieu",
         "38.Brezins",
         "38.Cessieu",
         "38.Chanas",
         "38.Charantonnay",
         "38.Chasse-sur-Rhone",
         "38.Chonas-l-Amballan",
         "38.Clonas-sur-Vareze",
         "38.Cour-et-Buis",
         "38.Diemoz",
         "38.Domarin",
         "38.Grenay",
         "38.La-Cote-Saint-Andre",
         "38.La-Tour-du-Pin",
         "38.La-Verpilliere",
         "38.Le-Peage-de-Roussillon",
         "38.Les-Roches",
         "38.l-Isle-d-Abeau",
         "38.Marcilloles",
         "38.Meyssies",
         "38.Moissieu-sur-Dolon",
         "38.Nivolas-Vermelle",
         "38.Pact",
         "38.Penol",
         "38.Primarette",
         "38.Reventin-Vaugris",
         "38.Roussillon",
         "38.Sablons",
         "38.Saint-Alban-de-Roche",
         "38.Saint-Bartelemy",
         "38.Saint-Clair-du-Rhone",
         "38.Saint-Didier-de-la-Tour",
         "38.Saint-Etienne-de-saint-geoirs",
         "38.Saint-Georges-d-Esperanche",
         "38.Saint-Jean-de-Soudain",
         "38.Saint-Lattier",
         "38.Saint-Maurice-l-Exil",
         "38.Saint-Prim",
         "38.Saint-Quentin-Fallavier",
         "38.Saint-Simeon-de-Bressieux",
         "38.Saint-Victor-de-Cessieu",
         "38.Salaise-sur-Sanne",
         "38.Sardieu",
         "38.Savas-Mepin",
         "38.Serezin-de-la-Tour",
         "38.Seyssuel",
         "38.Sillans",
         "38.Vaulx-Milieu",
         "38.Vienne",
         "38.Vilette-d-Anthon",
         "38.Villefontaine"
	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_42 = new String[]{
		 "42.Ailleux",
         "42.Ambierle",
         "42.Andrezieux-Boutheon",
         "42.Balbigny",
         "42.Belleroche",
         "42.Boen-sur-Lignon",
         "42.Boisset-les-Montrond",
         "42.Bonson",
         "42.Cervieres",
         "42.Chalain-d-Uzore",
         "42.Chamboeuf",
         "42.Champdieu",
         "42.Changy",
         "42.Charlieu",
         "42.Chateauneuf",
         "42.Chavanay",
         "42.Civens",
         "42.Commelle-Vernay",
         "42.Cuzieu",
         "42.Debats",
         "42.Epercieux-Saint-Paul",
         "42.Feurs",
         "42.Firminy",
         "42.Fraisses",
         "42.La-Fouillouse",
         "42.La-Grand-Croix",
         "42.La-Pacaudiere",
         "42.La-Ricamarie",
         "42.Le-Chambon-Feugerolles",
         "42.Le-Coteau",
         "42.Le-Crozet",
         "42.Leigneux",
         "42.L-Hopital-sous-Rochefort",
         "42.Lorette",
         "42.Mably",
         "42.Marcilly-le-Chatel",
         "42.Marclopt",
         "42.Montbrison",
         "42.Montrond-les-Bains",
         "42.Montverdun",
         "42.Neaux",
         "42.Neulise",
         "42.Noiretable",
         "42.Notre-Dame-de-Boisset",
         "42.Parigny",
         "42.Perreux",
         "42.Pinay",
         "42.Pouilly-les-Feurs",
         "42.Pouilly-sous-Charlieu",
         "42.Pradines",
         "42.Regny",
         "42.Riorges",
         "42.Rive-de-Gier",
         "42.Roanne",
         "42.Saint-Andre-le-Puy",
         "42.Saint-Chamond",
         "42.Saint-Cyr-de-Favieres",
         "42.Saint-Didier-sur-Rochefort",
         "42.Saint-Etienne",
         "42.Saint-Galmier",
         "42.Saint-Georges-en-Couzan",
         "42.Saint-Georges-Haute-Ville",
         "42.Saint-Germain-la-Montagne",
         "42.Saint-Germain-Lespinasse",
         "42.Saint-Haon-le-Vieux",
         "42.Saint-Jean-la-Vetre",
         "42.Saint-Jodard",
         "42.Saint-Julien-la-Vetre",
         "42.Saint-Just-Saint-Rambert",
         "42.Saint-Just-sur-Loire",
         "42.Saint-Laurent-la-Conche",
         "42.Saint-Laurent-Rochefort",
         "42.Saint-Marcel-de-Felines",
         "42.Saint-Marcellin-en-Forez",
         "42.Saint-Martin-d-Estreaux",
         "42.Saint-Michel-sur-Rhone",
         "42.Saint-Nizier-sous-Charlieu",
         "42.Saint-Paul-en-Cornillon",
         "42.Saint-Paul-en-Jarez",
         "42.Saint-Pierre-de-Boeuf",
         "42.Saint-Pierre-la-Noaille",
         "42.Saint-Priest-en-Jarez",
         "42.Saint-Priest-la-Roche",
         "42.Saint-Priest-la-Vetre",
         "42.Saint-Romain-la-Motte",
         "42.Saint-Romain-le-Puy",
         "42.Saint-Symphorien-de-Lay",
         "42.Saint-Thurin",
         "42.Saint-Victor-sur-Rhins",
         "42.Savignieux",
         "42.Sury-le-Comtal",
         "42.Tartaras",
         "42.Terrenoire",
         "42.Trelins",
         "42.Unieux",
         "42.Veauche",
         "42.Vendranges",
         "42.Verin",
         "42.Villars",
         "42.Vougy"
	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_43 = new String[]{
		 "43.Dunieres",
         "43.La-Seauve-sur-Semene",
         "43.Pont-Salomon",
         "43.Saint-Didier-en-Velay",
         "43.Saint-Ferreol-d-Auroure",
         "43.Saint-Just-Malmont",
         "43.Saint-Pal-de-Mons",
         "43.Saint-Romain-Lachalm"
	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_69 = new String[]{
		 "69.Albigny-sur-Saone",
         "69.Amberieux",
         "69.Amplepuis",
         "69.Ampuis",
         "69.Anse",
         "69.Arnas",
         "69.Belleville-sur-Saone",
         "69.Bessenay",
         "69.Brignais",
         "69.Brussieu",
         "69.Cailloux-sur-Fontaines",
         "69.Caluire-et-Cuire",
         "69.Chambost-Allieres",
         "69.Chamelet",
         "69.Chaponost",
         "69.Charbonnieres-les-Bains",
         "69.Chasse-sur-Rhone",
         "69.Chasselay",
         "69.Chatillon",
         "69.Chessy",
         "69.Chevinay",
         "69.Civrieux-d-Azergues",
         "69.Claveisolles",
         "69.Collonges-au-Mont-d-Or",
         "69.Colombier-Saugnieu",
         "69.Condrieu",
         "69.Corcelles",
         "69.Courzieu",
         "69.Couzon-au-Mont-d-Or",
         "69.Curis-au-Mont-d-Or",
         "69.Dardilly",
         "69.Decines",
         "69.Dommartin",
         "69.Echalas",
         "69.Ecully",
         "69.Eveux",
         "69.Feyzin",
         "69.Fleurieu-sur-Saone",
         "69.Fleurieux-sur-l-Arbresle",
         "69.Fontaines-Saint-Martin",
         "69.Fontaines-sur-Saone",
         "69.Francheville",
         "69.Genay",
         "69.Givors",
         "69.Grigny",
         "69.Irigny",
         "69.Jons",
         "69.La-Mulatiere",
         "69.Lamure-sur-Azergues",
         "69.Lancie",
         "69.L-Arbresle",
         "69.La-Tour-de-Salvagny",
         "69.Le-Bois-d-Oingt",
         "69.Le-Breuil",
         "69.Legny",
         "69.Lentilly",
         "69.Les-Cheres",
         "69.Les-Sauvages",
         "69.Letra",
         "69.Limas",
         "69.Limonest",
         "69.Lissieu",
         "69.Loire-sur-Rhone",
         "69.Longes",
         "69.Lozanne",
         "69.Lyon",
         "69.Marcilly-d-Azergues",
         "69.Marcy-l-Etoile",
         "69.Meyzieux",
         "69.Millery",
         "69.Moins",
         "69.Montagny",
         "69.Montanay",
         "69.Neuville-sur-Saone",
         "69.Nuelles",
         "69.Oullins",
         "69.Pierre-Benite",
         "69.Pommiers",
         "69.Pontcharra-sur-Turdine",
         "69.Poule-les-Echarmeaux",
         "69.Pusignan",
         "69.Quincieux",
         "69.Rilleux-la-Pape",
         "69.Rochetaillee",
         "69.Sain-Bel",
         "69.Sainte-Colombe",
         "69.Sainte-Foy-l-Argentiere",
         "69.Saint-Fons",
         "69.Saint-Forgeux",
         "69.Saint-Genis-l-Argentiere",
         "69.Saint-Georges-de-Reneins",
         "69.Saint-Germain-au-Mont-d-Or",
         "69.Saint-Jean-d-Ardieres",
         "69.Saint-Just-d-Avray",
         "69.Saint-Laurent-de-Mure",
         "69.Saint-Laurent-d-Oingt",
         "69.Saint-Marcel-l-Eclaire",
         "69.Saint-Nizier-d-Azergues",
         "69.Saint-Pierre-de-Chandieu",
         "69.Saint-Priest",
         "69.Saint-Romain-au-Mont-d-Or",
         "69.Saint-Romain-de-Popey",
         "69.Saint-Romain-en-Gal",
         "69.Saint-Romain-en-Gier",
         "69.Sathonay-Camp",
         "69.Sathonay-Village",
         "69.Savigny",
         "69.Serezin-du-Rhone",
         "69.Solaize",
         "69.Souzy",
         "69.Tarare",
         "69.Tassin-la-Demi-Lune",
         "69.Ternand",
         "69.Ternay",
         "69.Treves",
         "69.Tupin-et-Semons",
         "69.Vaulx-en-Velin",
         "69.Venissieux",
         "69.Vernaison",
         "69.Villefranche-sur-Saone",
         "69.Villeurbanne",
         "69.Vourles"
	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_71 = new String[]{
		 "71.Romaneche"
	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_73 = new String[]{
		 "73.Aiguebelette-le-Lac",
		 "73.Aiguebelle",
		 "73.Aigueblanche",
		 "73.Aime",
		 "73.Aix-les-Bains",
		 "73.Albens",
		 "73.Albertville",
		 "73.Arbin",
		 "73.Argentine",
		 "73.Barberaz",
		 "73.Bellentre",
		 "73.Bourgneuf",
		 "73.Bourg-Saint-Maurice",
		 "73.Brison-Saint-Innocent",
		 "73.Cevins",
		 "73.Chambery",
		 "73.Chamousset",
		 "73.Chignin",
		 "73.Chindrieux",
		 "73.Cognin",
		 "73.Cruet",
		 "73.Culoz",
		 "73.Domessin",
		 "73.Epierre",
		 "73.Feissons-sur-Isere",
		 "73.Fourneaux",
		 "73.Francin",
		 "73.Freney",
		 "73.Freterive",
		 "73.Frontenex",
		 "73.Gilly-sur-Isere",
		 "73.Gresy-sur-Aix",
		 "73.Gresy-sur-Isere",
		 "73.Hermillon",
		 "73.La-Bathie",
		 "73.La-Biolle",
		 "73.La-Bridoire",
		 "73.La-Chambre",
		 "73.La-Chapelle",
		 "73.La-Lechere",
		 "73.La-Ravoire",
		 "73.Landry",
		 "73.Lepin-le-Lac",
		 "73.Les-Chavannes-en-Maurienne",
		 "73.Macot-la-Plagne",
		 "73.Marthod",
		 "73.Modane",
		 "73.Mognard",
		 "73.Montailleur",
		 "73.Montgirod",
		 "73.Montmelian",
		 "73.Montricher-Albanne",
		 "73.Moutiers",
		 "73.Myans",
		 "73.Notre-Dame-du-Pre",
		 "73.Orelle",
		 "73.Pallud",
		 "73.Pontamafrey-Montpascal",
		 "73.Rognaix",
		 "73.Saint-Avre",
		 "73.Saint-Beron",
		 "73.Saint-Cassin",
		 "73.Sainte-Helene-du-Lac",
		 "73.Sainte-Marie-de-Cuines",
		 "73.Saint-Germain-la-Chambotte",
		 "73.Saint-Jean-de-la-Porte",
		 "73.Saint-Jean-de-Maurienne",
		 "73.Saint-Jeoire-Prieure",
		 "73.Saint-Julien-Mont-Denis",
		 "73.Saint-Marcel",
		 "73.Saint-Martin-de-la-Porte",
		 "73.Saint-Michel-de-Maurienne",
		 "73.Saint-Pierre-d'Albigny",
		 "73.Saint-Vital",
		 "73.Thenesol",
		 "73.Tournon",
		 "73.Tours-en-Savoie",
		 "73.Tresserve",
		 "73.Ugine",
		 "73.Villargondran",
		 "73.Vimines",
		 "73.Vions",
		 "73.Viviers-du-Lac",
		 "73.Voglans",

	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_74 = new String[]{
		 "74.Aiguebelle",
		 "74.Aigueblanche",
		 "74.Aime",
		 "74.Aix-les-Bains",
		 "74.Albens",
		 "74.Albertville",
		 "74.Arbin",
		 "74.Argentine",
		 "74.Barberaz",
		 "74.Bellentre",
		 "74.Bourgneuf",
		 "74.Bourg-Saint-Maurice",
		 "74.Brison-Saint-Innocent",
		 "74.Cevins",
		 "74.Chambery",
		 "74.Chamousset",
		 "74.Chignin",
		 "74.Chindrieux",
		 "74.Cognin",
		 "74.Cruet",
		 "74.Culoz",
		 "74.Domessin",
		 "74.Epierre",
		 "74.Feissons-sur-Isere",
		 "74.Fourneaux",
		 "74.Francin",
		 "74.Freney",
		 "74.Freterive",
		 "74.Frontenex",
		 "74.Gilly-sur-Isere",
		 "74.Gresy-sur-Aix",
		 "74.Gresy-sur-Isere",
		 "74.Hermillon",
		 "74.La-Bathie",
		 "74.La-Biolle",
		 "74.La-Bridoire",
		 "74.La-Chambre",
		 "74.La-Chapelle",
		 "74.La-Lechere",
		 "74.La-Ravoire",
		 "74.Landry",
		 "74.Lepin-le-Lac",
		 "74.Les-Chavannes-en-Maurienne",
		 "74.Macot-la-Plagne",
		 "74.Marthod",
		 "74.Modane",
		 "74.Mognard",
		 "74.Montailleur",
		 "74.Montgirod",
		 "74.Montmelian",
		 "74.Montricher-Albanne",
		 "74.Moutiers",
		 "74.Myans",
		 "74.Notre-Dame-du-Pre",
		 "74.Orelle",
		 "74.Pallud",
		 "74.Pontamafrey-Montpascal",
		 "74.Rognaix",
		 "74.Saint-Avre",
		 "74.Saint-Beron",
		 "74.Saint-Cassin",
		 "74.Sainte-Helene-du-Lac",
		 "74.Sainte-Marie-de-Cuines",
		 "74.Saint-Germain-la-Chambotte",
		 "74.Saint-Jean-de-la-Porte",
		 "74.Saint-Jean-de-Maurienne",
		 "74.Saint-Jeoire-Prieure",
		 "74.Saint-Julien-Mont-Denis",
		 "74.Saint-Marcel",
		 "74.Saint-Martin-de-la-Porte",
		 "74.Saint-Michel-de-Maurienne",
		 "74.Saint-Pierre-d'Albigny",
		 "74.Saint-Vital",
		 "74.Thenesol",
		 "74.Tournon",
		 "74.Tours-en-Savoie",
		 "74.Tresserve",
		 "74.Ugine",
		 "74.Villargondran",
		 "74.Vimines",
		 "74.Vions",
		 "74.Viviers-du-Lac",
		 "74.Voglans",

	 };
	 
	 public static String[] CITIES_DEPARTEMENTS_84 = new String[]{
		 "84.Avignon",
         "84.Bollene",
         "84.Caderousse",
         "84.Caumont-sur-Durance",
         "84.Cavaillon",
         "84.Cheval-Blanc",
         "84.Lamotte-du-Rhone",
         "84.Lapalud",
         "84.Mondragon",
         "84.Mornas",
         "84.Orange",
         "84.Piolenc"
	 };

    /*public static String[][][] CITIES = new String[][][]{
        new String[][]{
                new String[]{ // 01. Ain
                		"01.Balan",
                        "01.Beligneux",
                        "01.Beynost",
                        "01.Civrieux",
                        "01.Dagneux",
                        "01.La-Boisse",
                        "01.Lapeyrouse",
                        "01.Le-Plantay",
                        "01.Marlieux",
                        "01.Massieux",
                        "01.Meximieux",
                        "01.Mionnay",
                        "01.Miribel",
                        "01.Montluel",
                        "01.Neyron",
                        "01.Nievroz",
                        "01.Parcieux",
                        "01.Peronnas",
                        "01.Perouges",
                        "01.Reyrieux",
                        "01.Rignieu-le-desert",
                        "01.Saint-Andre-de-Corcy",
                        "01.Saint-Germain-sur-Renon",
                        "01.Saint-Marcel",
                        "01.Saint-Maurice-de-Beynost",
                        "01.Saint-Paul-de-Varax",
                        "01.Servas",
                        "01.Thil",
                        "01.Tramoyes",
                        "01.Trevoux",
                        "01.Villars-les-Dombes",
                        "01.Villieu-Loyes-Mollon",
                },
                new String[]{ // 03.Allier
                        "03.Saint-Pierre-Laval",
                },
                new String[]{ // 07. Ardeche
                		"07.Alba",
                		"07.Andance",
                        "07.Arras-sur-Rhone",
                        "07.Aubignas",
                        "07.Baix",
                        "07.Beauchastel",
                        "07.Bourg-Saint-Andeol",
                        "07.Champagne",
                        "07.Charmes-sur-Rhone",
                        "07.Charnas",
                        "07.Chateaubourg",
                        "07.Chomerac",
                        "07.Cornas",
                        "07.Cruas",
                        "07.Glun",
                        "07.Guilherand",
                        "07.La-Voulte-sur-Rhone",
                        "07.Lemps",
                        "07.Le-Pouzin",
                        "07.Le-Teil",
                        "07.Limony",
                        "07.Mauves",
                        "07.Meysse",
                        "07.Ozon",
                        "07.Peyraud",
                        "07.Privas",
                        "07.Rochemaure",
                        "07.Rochessauve",
                        "07.Rompon",
                        "07.Saint-Desirat",
                        "07.Saint-Georges-les-Bains",
                        "07.Saint-Jean-de-Muzols",
                        "07.Saint-Lager-Bressac",
                        "07.Saint-Marcel-d-Ardeche",
                        "07.Saint-Montant",
                        "07.Saint-Peray",
                        "07.Saint-Priest",
                        "07.Sarras",
                        "07.Serrieres",
                        "07.Soyons",
                        "07.Tournon-sur-Rhone",
                        "07.Vion",
                        "07.Viviers",
                },
                new String[]{ // 13. Bouches du Rhône
                        "13.Aix-en-Provence",
                        "13.Alleins",
                        "13.Cabries",
                        "13.Eguilles",
                        "13.La-Barben",
                        "13.Lambesc",
                        "13.Les-Pennes-Mirabeau",
                        "13.Mallemort",
                        "13.Marseille",
                        "13.Orgon",
                        "13.Plan-d-Orgon",
                        "13.Saint-Cannat",
                        "13.Senas",
                        "13.Ventabren",
                        "13.Vernegues",
                },
                new String[]{ // 26. Drôme
                        "26.Aixan",
                        "26.Aix-en-Diois",
                        "26.Alixan",
                        "26.Allan",
                        "26.Allex",
                        "26.Andancette",
                        "26.Anneyron",
                        "26.Aouste-sur-Sye",
                        "26.Aubenasson",
                        "26.Barnave",
                        "26.Beaurieres",
                        "26.Bonlieu-sur-Roubion",
                        "26.Bourg-les-Valence",
                        "26.Bren",
                        "26.Chabeuil",
                        "26.Chabrillan",
                        "26.Chateauneuf-de-Galaure",
                        "26.Chateauneuf-du-Rhone",
                        "26.Chateauneuf-sur-Isere",
                        "26.Chavannes",
                        "26.Claveyson",
                        "26.Clerieux",
                        "26.Crest",
                        "26.Crozes-Hermitage",
                        "26.Die",
                        "26.Divajeu",
                        "26.Donzere",
                        "26.Epinouze",
                        "26.Erome",
                        "26.Espeluche",
                        "26.Espenel",
                        "26.Etoile-sur-Rhone",
                        "26.Eurre",
                        "26.Fourcinet",
                        "26.Gervans",
                        "26.Granges-les-Beaumont",
                        "26.Jansac",
                        "26.La-Coucourde",
                        "26.La-Garde-Adhemar",
                        "26.La-Laupie",
                        "26.La-Motte-de-Galaure",
                        "26.Lapeyrouse-Mornay",
                        "26.La-Roche-de-Glun",
                        "26.La-Roche-sur-Grane",
                        "26.Laval-d-Aix",
                        "26.Laveyron",
                        "26.Lens-Lestang",
                        "26.Lesches-en-Diois",
                        "26.Les-Granges-Gontardes",
                        "26.Les-Tourrettes",
                        "26.Livron",
                        "26.Loriol-sur-Drome",
                        "26.Luc-en-Diois",
                        "26.Malataverne",
                        "26.Manthes",
                        "26.Marsanne",
                        "26.Marsaz",
                        "26.Menglon",
                        "26.Mercurol",
                        "26.Molieres-Glandaz",
                        "26.Montboucher-sur-Jabron",
                        "26.Montelier",
                        "26.Montelimar",
                        "26.Montmeyran",
                        "26.Montvendre",
                        "26.Moras-en-Valloire",
                        "26.Mureils",
                        "26.Piegrod-la-Clastre",
                        "26.Pierrelatte",
                        "26.Ponet-et-Saint-Auban",
                        "26.Ponsas",
                        "26.Pontaix",
                        "26.Pont-de-l-Isere",
                        "26.Portes",
                        "26.Recourbeau",
                        "26.Romans-sur-Isere",
                        "26.Roussas",
                        "26.Roynac",
                        "26.Saillans",
                        "26.Sainte-Croix",
                        "26.Saint-Marcel-les-Valence",
                        "26.Saint-Paul-les-Romans",
                        "26.Saint-Rambert-d-Albon",
                        "26.Saint-Roman",
                        "26.Saint-Sauveur-en-Diois",
                        "26.Saint-Sorlin-en-Valloire",
                        "26.Saint-Vallier",
                        "26.Saulce-sur-Rhone",
                        "26.Sauzet",
                        "26.Savasse",
                        "26.Serves",
                        "26.Tain-l-Hermitage",
                        "26.Upie",
                        "26.Valence",
                        "26.Vaunaveys-la-Rochette",
                        "26.Vercheny",
                },
                new String[]{ // 30. Gard
                        "30.Aramon",
                        "30.Comps",
                        "30.Domazan",
                        "30.Jonquierres-Saint-Vincent",
                        "30.Les-Angles",
                        "30.Manduel",
                        "30.Montfrin",
                        "30.Pujaut",
                        "30.Redessan",
                        "30.Rochefort-du-Gard",
                        "30.Roquemaure",
                        "30.Saint-Etienne-des-Sorts",
                        "30.Saze",
                        "30.Tavel",
                        "30.Theziers",
                        "30.Vallabregues",
                        "30.Venejan",
                },
                new String[]{ // 38. Isere
                        "38.Beaufort",
                        "38.Beaurepaire",
                        "38.Beauvoir-de-Marc",
                        "38.Bonnefamille",
                        "38.Bouge-Chambalud",
                        "38.Bourgoin-Jallieu",
                        "38.Brezins",
                        "38.Cessieu",
                        "38.Chanas",
                        "38.Charantonnay",
                        "38.Chasse-sur-Rhone",
                        "38.Chonas-l-Amballan",
                        "38.Clonas-sur-Vareze",
                        "38.Cour-et-Buis",
                        "38.Diemoz",
                        "38.Domarin",
                        "38.Grenay",
                        "38.La-Cote-Saint-Andre",
                        "38.La-Tour-du-Pin",
                        "38.La-Verpilliere",
                        "38.Le-Peage-de-Roussillon",
                        "38.Les-Roches",
                        "38.l-Isle-d-Abeau",
                        "38.Marcilloles",
                        "38.Meyssies",
                        "38.Moissieu-sur-Dolon",
                        "38.Nivolas-Vermelle",
                        "38.Pact",
                        "38.Penol",
                        "38.Primarette",
                        "38.Reventin-Vaugris",
                        "38.Roussillon",
                        "38.Sablons",
                        "38.Saint-Alban-de-Roche",
                        "38.Saint-Bartelemy",
                        "38.Saint-Clair-du-Rhone",
                        "38.Saint-Didier-de-la-Tour",
                        "38.Saint-Etienne-de-saint-geoirs",
                        "38.Saint-Georges-d-Esperanche",
                        "38.Saint-Jean-de-Soudain",
                        "38.Saint-Lattier",
                        "38.Saint-Maurice-l-Exil",
                        "38.Saint-Prim",
                        "38.Saint-Quentin-Fallavier",
                        "38.Saint-Simeon-de-Bressieux",
                        "38.Saint-Victor-de-Cessieu",
                        "38.Salaise-sur-Sanne",
                        "38.Sardieu",
                        "38.Savas-Mepin",
                        "38.Serezin-de-la-Tour",
                        "38.Seyssuel",
                        "38.Sillans",
                        "38.Vaulx-Milieu",
                        "38.Vienne",
                        "38.Vilette-d-Anthon",
                        "38.Villefontaine",
                },
                new String[]{ // 42. Loire
                        "42.Ailleux",
                        "42.Ambierle",
                        "42.Andrezieux-Boutheon",
                        "42.Balbigny",
                        "42.Belleroche",
                        "42.Boen-sur-Lignon",
                        "42.Boisset-les-Montrond",
                        "42.Bonson",
                        "42.Cervieres",
                        "42.Chalain-d-Uzore",
                        "42.Chamboeuf",
                        "42.Champdieu",
                        "42.Changy",
                        "42.Charlieu",
                        "42.Chateauneuf",
                        "42.Chavanay",
                        "42.Civens",
                        "42.Commelle-Vernay",
                        "42.Cuzieu",
                        "42.Debats",
                        "42.Epercieux-Saint-Paul",
                        "42.Feurs",
                        "42.Firminy",
                        "42.Fraisses",
                        "42.La-Fouillouse",
                        "42.La-Grand-Croix",
                        "42.La-Pacaudiere",
                        "42.La-Ricamarie",
                        "42.Le-Chambon-Feugerolles",
                        "42.Le-Coteau",
                        "42.Le-Crozet",
                        "42.Leigneux",
                        "42.L-Hopital-sous-Rochefort",
                        "42.Lorette",
                        "42.Mably",
                        "42.Marcilly-le-Chatel",
                        "42.Marclopt",
                        "42.Montbrison",
                        "42.Montrond-les-Bains",
                        "42.Montverdun",
                        "42.Neaux",
                        "42.Neulise",
                        "42.Noiretable",
                        "42.Notre-Dame-de-Boisset",
                        "42.Parigny",
                        "42.Perreux",
                        "42.Pinay",
                        "42.Pouilly-les-Feurs",
                        "42.Pouilly-sous-Charlieu",
                        "42.Pradines",
                        "42.Regny",
                        "42.Riorges",
                        "42.Rive-de-Gier",
                        "42.Roanne",
                        "42.Saint-Andre-le-Puy",
                        "42.Saint-Chamond",
                        "42.Saint-Cyr-de-Favieres",
                        "42.Saint-Didier-sur-Rochefort",
                        "42.Saint-Etienne",
                        "42.Saint-Galmier",
                        "42.Saint-Georges-en-Couzan",
                        "42.Saint-Georges-Haute-Ville",
                        "42.Saint-Germain-la-Montagne",
                        "42.Saint-Germain-Lespinasse",
                        "42.Saint-Haon-le-Vieux",
                        "42.Saint-Jean-la-Vetre",
                        "42.Saint-Jodard",
                        "42.Saint-Julien-la-Vetre",
                        "42.Saint-Just-Saint-Rambert",
                        "42.Saint-Just-sur-Loire",
                        "42.Saint-Laurent-la-Conche",
                        "42.Saint-Laurent-Rochefort",
                        "42.Saint-Marcel-de-Felines",
                        "42.Saint-Marcellin-en-Forez",
                        "42.Saint-Martin-d-Estreaux",
                        "42.Saint-Michel-sur-Rhone",
                        "42.Saint-Nizier-sous-Charlieu",
                        "42.Saint-Paul-en-Cornillon",
                        "42.Saint-Paul-en-Jarez",
                        "42.Saint-Pierre-de-Boeuf",
                        "42.Saint-Pierre-la-Noaille",
                        "42.Saint-Priest-en-Jarez",
                        "42.Saint-Priest-la-Roche",
                        "42.Saint-Priest-la-Vetre",
                        "42.Saint-Romain-la-Motte",
                        "42.Saint-Romain-le-Puy",
                        "42.Saint-Symphorien-de-Lay",
                        "42.Saint-Thurin",
                        "42.Saint-Victor-sur-Rhins",
                        "42.Savignieux",
                        "42.Sury-le-Comtal",
                        "42.Tartaras",
                        "42.Terrenoire",
                        "42.Trelins",
                        "42.Unieux",
                        "42.Veauche",
                        "42.Vendranges",
                        "42.Verin",
                        "42.Villars",
                        "42.Vougy",
                },
                new String[]{ // 43. Haute-Loire
                        "43.Dunieres",
                        "43.La-Seauve-sur-Semene",
                        "43.Pont-Salomon",
                        "43.Saint-Didier-en-Velay",
                        "43.Saint-Ferreol-d-Auroure",
                        "43.Saint-Just-Malmont",
                        "43.Saint-Pal-de-Mons",
                        "43.Saint-Romain-Lachalm",
                },
                new String[]{ // 69. Rhône
                        "69.Albigny-sur-Saone",
                        "69.Amberieux",
                        "69.Amplepuis",
                        "69.Ampuis",
                        "69.Anse",
                        "69.Arnas",
                        "69.Belleville-sur-Saone",
                        "69.Bessenay",
                        "69.Brignais",
                        "69.Brussieu",
                        "69.Cailloux-sur-Fontaines",
                        "69.Caluire-et-Cuire",
                        "69.Chambost-Allieres",
                        "69.Chamelet",
                        "69.Chaponost",
                        "69.Charbonnieres-les-Bains",
                        "69.Chasse-sur-Rhone",
                        "69.Chasselay",
                        "69.Chatillon",
                        "69.Chessy",
                        "69.Chevinay",
                        "69.Civrieux-d-Azergues",
                        "69.Claveisolles",
                        "69.Collonges-au-Mont-d-Or",
                        "69.Colombier-Saugnieu",
                        "69.Condrieu",
                        "69.Corcelles",
                        "69.Courzieu",
                        "69.Couzon-au-Mont-d-Or",
                        "69.Curis-au-Mont-d-Or",
                        "69.Dardilly",
                        "69.Decines",
                        "69.Dommartin",
                        "69.Echalas",
                        "69.Ecully",
                        "69.Eveux",
                        "69.Feyzin",
                        "69.Fleurieu-sur-Saone",
                        "69.Fleurieux-sur-l-Arbresle",
                        "69.Fontaines-Saint-Martin",
                        "69.Fontaines-sur-Saone",
                        "69.Francheville",
                        "69.Genay",
                        "69.Givors",
                        "69.Grigny",
                        "69.Irigny",
                        "69.Jons",
                        "69.La-Mulatiere",
                        "69.Lamure-sur-Azergues",
                        "69.Lancie",
                        "69.L-Arbresle",
                        "69.La-Tour-de-Salvagny",
                        "69.Le-Bois-d-Oingt",
                        "69.Le-Breuil",
                        "69.Legny",
                        "69.Lentilly",
                        "69.Les-Cheres",
                        "69.Les-Sauvages",
                        "69.Letra",
                        "69.Limas",
                        "69.Limonest",
                        "69.Lissieu",
                        "69.Loire-sur-Rhone",
                        "69.Longes",
                        "69.Lozanne",
                        "69.Lyon",
                        "69.Marcilly-d-Azergues",
                        "69.Marcy-l-Etoile",
                        "69.Meyzieux",
                        "69.Millery",
                        "69.Moins",
                        "69.Montagny",
                        "69.Montanay",
                        "69.Neuville-sur-Saone",
                        "69.Nuelles",
                        "69.Oullins",
                        "69.Pierre-Benite",
                        "69.Pommiers",
                        "69.Pontcharra-sur-Turdine",
                        "69.Poule-les-Echarmeaux",
                        "69.Pusignan",
                        "69.Quincieux",
                        "69.Rilleux-la-Pape",
                        "69.Rochetaillee",
                        "69.Sain-Bel",
                        "69.Sainte-Colombe",
                        "69.Sainte-Foy-l-Argentiere",
                        "69.Saint-Fons",
                        "69.Saint-Forgeux",
                        "69.Saint-Genis-l-Argentiere",
                        "69.Saint-Georges-de-Reneins",
                        "69.Saint-Germain-au-Mont-d-Or",
                        "69.Saint-Jean-d-Ardieres",
                        "69.Saint-Just-d-Avray",
                        "69.Saint-Laurent-de-Mure",
                        "69.Saint-Laurent-d-Oingt",
                        "69.Saint-Marcel-l-Eclaire",
                        "69.Saint-Nizier-d-Azergues",
                        "69.Saint-Pierre-de-Chandieu",
                        "69.Saint-Priest",
                        "69.Saint-Romain-au-Mont-d-Or",
                        "69.Saint-Romain-de-Popey",
                        "69.Saint-Romain-en-Gal",
                        "69.Saint-Romain-en-Gier",
                        "69.Sathonay-Camp",
                        "69.Sathonay-Village",
                        "69.Savigny",
                        "69.Serezin-du-Rhone",
                        "69.Solaize",
                        "69.Souzy",
                        "69.Tarare",
                        "69.Tassin-la-Demi-Lune",
                        "69.Ternand",
                        "69.Ternay",
                        "69.Treves",
                        "69.Tupin-et-Semons",
                        "69.Vaulx-en-Velin",
                        "69.Venissieux",
                        "69.Vernaison",
                        "69.Villefranche-sur-Saone",
                        "69.Villeurbanne",
                        "69.Vourles",
                },
                new String[]{ // 71. Saône et Loire
                        "71.Romaneche",
                },
                new String[]{ // 84. Vaucluse
                        "84.Avignon",
                        "84.Bollene",
                        "84.Caderousse",
                        "84.Caumont-sur-Durance",
                        "84.Cavaillon",
                        "84.Cheval-Blanc",
                        "84.Lamotte-du-Rhone",
                        "84.Lapalud",
                        "84.Mondragon",
                        "84.Mornas",
                        "84.Orange",
                        "84.Piolenc",
                }
        },
        new String[][]{
                new String[]{ // 96
                	"96.Test96",
                	"96.Test962",
                },
                new String[]{ // 97
                    "97.Test97",
                    "97.Test972",
                }
        }
    };*/
    
    /**
	 * build linked options.
	 * 
	 * @param key
	 * @param matrix
	 * @return
	 */
    public static String[] buildLinkedOptions(String key, String[] parentOptions, String[][] childOptions){
        int position = findMenuPosition(key, parentOptions);
        String[] result = null;
        if (position >= 0 && childOptions.length > position) {
            result =  childOptions[position];
        } 
        return result;
    }

    public static int findMenuPosition(String key, String[] options){
        int position = -1;
        // System.out.println("-- key : "+key);
        if (options != null){

            for (int i = 0; i < options.length; i++) {
                // System.out.println("-- options : " + options[i]);
                if (key.equals(options[i])){
                    position = i;

                }

            }

        }
        return position;
    }

    public static String[] FONCTION_ARRAY= new String[]{ // Warning : all the
															// string values are
															// lowercase, check
															// the constants at
															// the beginning of
															// the file.
        FUNCTION_AH,
        FUNCTION_AOAP,
        FUNCTION_AOAU,
        FUNCTION_CU_DUO,
        FUNCTION_DDI,
        FUNCTION_DET,
        FUNCTION_INVITE,
        FUNCTION_RI_OA_OT,
        FUNCTION_RM_OA_OT,
        FUNCTION_RRMI,
        FUNCTION_RMPL,
        FUNCTION_ROTP,
        FUNCTION_R_PRILY_Y,
        FUNCTION_R_PRILY,
        FUNCTION_SOAR,
        FUNCTION_COT,
        FUNCTION_ADJRI_OA_OT,
        FUNCTION_OTHER,
        FUNCTION_ARCHIVE,
        FUNCTION_HYDRO,
        FUNCTION_BETA
    };

    public static String[] INDICATOR_FUNCTION_ARRAY= new String[]{ // Warning :
																	// all the
																	// string
																	// values
																	// are
																	// lowercase,
																	// check the
																	// constants
																	// at the
																	// beginning
																	// of the
																	// file.
        FUNCTION_SOAR,
        FUNCTION_AOAP,
        FUNCTION_AH,
        FUNCTION_COT,
    };
    
    public static String[] INDICATOR_TYPE_ARRAY= new String[]{
        "I",
        "PV",
        "SP",
        "SR"
    };
    
    public static String[] INDICATOR_IG_TYPE_ARRAY= new String[]{
        "KOA1",
        "KOA2",
        "KOA3"
    };
    
    public static String[] WORK_TYPE_PATRIMOINE = new String[]{
        "work_type_patrimoine_debmeu",
        "work_type_patrimoine_remblai",
        "work_type_patrimoine_debroch"
    };
    
    public static String[] ENTITY_ARRAY= new String[]{
        "entity_drimoa/ot",
        "entity_even",
        "entity_iglg",
        "entity_igoa",
        "entity_imt",
        "entity_otp",
        "entity_prily",
        "entity_prichy"
    };

    public static String[] ACCOUNT_OBJECTIVE_ARRAY= new String[]{
        "account_objective_entre",
        "account_objective_global",
        "account_objective_surv", 
        "account_objective_tra"
    };

    public static String[] WORK_ASSEMBLY_ARRAY= new String[]{
        "assembly_Boul",
        "assembly_Riv",
        "assembly_Sou" 
    }; 

    public static String[] WORK_ASSEMBLY_ARRAY_1= new String[]{
        "assembly_Boul",
        "assembly_Sou" 
    };
    
    public static String[] WORK_ASSEMBLY_ARRAY_2= new String[]{
        "assembly_Boul",
        "assembly_Riv",
        "assembly_Sou" 
    };
    
    public static String[] WORK_RISQUE_OBJET= new String[]{
    	"plate_forme",
    	"tiers"
    };
    
    public static String[][] WORK_RISQUE_NATURE = new String[][]{
        new String[]{ 
                "deformation",
                "instabilite_de_berge",
                "recouvrement"
                
        },
        new String[]{ 
        		"deformation",
                "instabilite_de_berge",
                "recouvrement"
        }
    };
    
    public static String[][][] WORK_RISQUE_MECANISME = new String[][][]{        
        new String [][] { // plate forme
                new String [] { // deformation
                		"effondrement_de_cavite",
                		"glissement",
                		"tassement" 
                		},
                new String [] { // instabilite_de_berge
                		                		                   
                },
                new String [] { // recouvrement
                		"coulee_de_boue",
                		"innondation",   
                		"propagation_masse_meuble",
                		"propagation_masse_rocheuse",   
                }
        },
        new String [][] { // tiers
        		new String [] { // deformation
                		"effondrement_de_cavite",
                		"glissement",
                		"tassement" 
                		},
                new String [] { // instabilite_de_berge
                		                		                   
                },
                new String [] { // recouvrement
                		"coulee_de_boue",
                		"innondation",   
                		"propagation_masse_meuble",
                		"propagation_masse_rocheuse",   
                }
        }
    };

    
    public static final String WORK_CATEGORY_SITE = "category_Site";
    public static final String WORK_CATEGORY_BAT = "category_Bat";
    public static final String WORK_CATEGORY_OA = "category_OA";
    public static final String WORK_CATEGORY_OT = "category_OT";
    public static final String WORK_CATEGORY_OTHERS = "category_Autre";
    public static final String WORK_CATEGORY_HYDRO = "category_Hydro";
   
    public static String[] WORK_CATEGORY_ARRAY= new String[]{
    	WORK_CATEGORY_OTHERS,
    	WORK_CATEGORY_BAT,
        WORK_CATEGORY_OA,
        WORK_CATEGORY_OT,
        WORK_CATEGORY_HYDRO
    };
    
    public static String[] MISSION_CATEGORY_ARRAY= new String[]{
        WORK_CATEGORY_OA,
        WORK_CATEGORY_OT,
        WORK_CATEGORY_SITE
    };
    
    public static String[] WORK_TYPE_CATEGORY_NULL_ARRAY= new String[]{
    	"work_type_aqc",
    	"work_type_aqc_buse",
    	"work_type_autre",
    	"work_type_bg",
    	"work_type_bnc",
        "work_type_bse",
        "work_type_dlt",
        "work_type_deblai", 
        "work_type_depot",
        "work_type_digoa",
        "work_type_digot", 
        "work_type_dcp",
        "work_type_divers",
        "work_type_eab",
        "work_type_efpb",
        "work_type_gal",
        "work_type_gp",
        "work_type_gt",
        "work_type_mgh",
        "work_type_ms",
        "work_type_mgr",
        "work_type_pc",
        "work_type_prev",
        "work_type_pr",
        "work_type_pas",
        "work_type_per",
        "work_type_pcl",
        "work_type_pra",
        "work_type_pro",
        "work_type_portique",
        "work_type_potence",
        "work_type_mxt", 
        "work_type_profil_rasant",
        "work_type_pyl",
        "work_type_remblai", 
        "work_type_sdm",
        "work_type_tete_tunnel",
        "work_type_tranchee", 
        "work_type_tc", 
        "work_type_td", 
        "work_type_trm",
        "work_type_tun",
        "work_type_versant",  
        "work_type_via",  
    };   
    

    public static String[][] WORK_TYPE_ARRAY = new String[][]{
    	new String[]{ // work_category_Others
        		"work_type_divers",
        		"work_type_eab",
                "work_type_mgh",
                "work_type_portique",
                "work_type_potence",
                "work_type_pyl",
        },
        new String[]{ // work_category_BAT
                "work_type_bnc"
        },
        new String[]{  // work_category_OA
                "work_type_aqc",
                "work_type_aqc_buse",
                "work_type_bse",
                "work_type_dcp",
                "work_type_digoa",
                "work_type_dlt",
                "work_type_gal",
                "work_type_gp",
                "work_type_gt",
                "work_type_ms",
                "work_type_mgr",
                "work_type_pas",
                "work_type_pc",
                "work_type_prev",
                "work_type_pcl",
                "work_type_per",
                "work_type_pra",
                "work_type_pro",
                "work_type_sdm",
                "work_type_tc", 
                "work_type_td",
                "work_type_trm",
                "work_type_tun",
                "work_type_via",
        },
        new String[]{ // work_category_OT
    			"work_type_bg",
        		"work_type_deblai", 
        		"work_type_efpb",
                "work_type_mxt", 
                "work_type_pr", 
                "work_type_profil_rasant",
                "work_type_remblai", 
                "work_type_tranchee", 
                "work_type_versant"     
        },
        new String[]{ // work_category_HYDRO
    			"work_type_hydro"     
        }
    };

    public static String[][][] WORK_GEOMETRY_ARRAY= new String[][][]{        
    	new String [][] { // work_category_Others
    			new String [] { // work_type_divers
                        "geometry_tripode"
                },
                new String [] { // work_type_eab
    					"geometry_tripode"
                },
                new String [] { // work_type_mgh
    					"geometry_cyl",
    					"geometry_qua",
                        "geometry_tripode"
                },
                new String [] { // work_type_portique
                        "geometry_tripode"
                },
                new String [] { // work_type_potence
                },
                new String [] { // work_type_pyl
    					"geometry_cyl",
    					"geometry_qua",
                        "geometry_tripode"
                }
        },
        new String [][] { // work_category_Bat
                new String [] { // work_type_bnc
                        // There is no geometry data for bnc
                }
        },
        new String [][] { // work_category_OA
                new String [] { // work_type_aqc
                        "geometry_cdr",     		
                        "geometry_pc",     		
                        "geometry_prt",    											    
                        "geometry_psd",    		
                        "geometry_srb",
                        "geometry_dba",
                },
                new String [] { // work_type_aqc_buse
                        "geometry_cdr",     		
                        "geometry_pc",     		
                        "geometry_prt",    											    
                        "geometry_psd",    		
                        "geometry_srb",
                        "geometry_dba",
                },
                new String [] { // work_type_bse
                        "geometry_circ" ,
                        "geometry_ov",
                },
                new String [] { // work_type_dcp
                        // There is no geometry data for dcp
                },
                new String [] { // work_type_digoa
                        // There is no geometry data for digoa
                },
                new String [] { // work_type_dlt
                        // There is no geometry data for dlt
                },
                new String [] { // work_type_gal
                        "geometry_pc", 
                        "geometry_srb" , 
                },
                new String [] { // work_type_gp
                        "geometry_pc", 
                        "geometry_srb" , 
                },
                new String [] { // work_type_gt
                        "geometry_pc", 
                        "geometry_srb" , 
                },
                new String [] { // work_type_ms
                        // There is no geometry data for ms
                },
                new String [] { // work_type_mgr
                        // There is no geometry data for mgr
                },
                new String [] { // work_type_pas
                        "geometry_cdr",
                        "geometry_srb",
                        "geometry_prt",
                        "geometry_psd",
                        "geometry_dba",
                },
                new String [] { // work_type_pc
                        // There is no geometry data for pc
                },
                new String [] { // work_type_prev
                        // There is no geometry data for prev
                },
                new String [] { // work_type_pcl
                        "geometry_cdr",
                        "geometry_srb",
                        "geometry_pc",
                        "geometry_psd",
                },
                new String [] { // work_type_per
                        // There is no geometry data for per
                },
                new String [] { // work_type_pra
                		"geometry_cdr",
                		"geometry_dba",
                        "geometry_srb", 
                        "geometry_pc",
                        "geometry_prt",
                        "geometry_psd",
                        
                },
                new String [] { // work_type_pro
                		"geometry_cdr",
                		"geometry_dba",
                        "geometry_srb",
                        "geometry_pc",
                        "geometry_prt",
                        "geometry_psd",
                },
                new String [] { // work_type_sdm
                		"geometry_cdr",
                        "geometry_srb",
                        "geometry_pc",
                        "geometry_prt",
                        "geometry_psd",
                        "geometry_dba",
                },
                new String [] { // work_type_tc
                        "geometry_srb"
                },
                new String [] { // work_type_td
                        "geometry_srb"
                },
                new String [] { // work_type_trm
                },
                new String [] { // work_type_tun
                        "geometry_pc", 
                        "geometry_srb",
                },
                new String [] { // work_type_via
                		"geometry_cdr",
                        "geometry_srb",
                        "geometry_pc",
                        "geometry_psd",
                        "geometry_prt",
                }
        },
        new String [][] { // work_category_OT  
                new String [] { // work_type_deblai
                        // There is no geometry data for deblai
                },
                new String [] { // work_type_digot
                        // There is no geometry data for digot
                },
                new String [] { // work_type_mxt
                        // There is no geometry data for mxt
                },
                new String [] { // work_type_pr
                        // There is no geometry data for pr
                },
                new String [] { // work_type_remblai
                        // There is no geometry data for remblai
                },
                new String [] { // work_type_tranchee
                        // There is no geometry data for trnachee
                },
                new String [] { // work_type_tranchee
                        // There is no geometry data for trnachee
                },
                new String [] { // work_type_trv
                        // There is no geometry data for trv
                }
        },
        new String [][] { // work_category_Hydro 
            new String [] { // work_type_deblai
                    // There is no geometry data for deblai
            },
            new String [] { // work_type_digot
                    // There is no geometry data for digot
            },
            new String [] { // work_type_mxt
                    // There is no geometry data for mxt
            },
            new String [] { // work_type_pr
                    // There is no geometry data for pr
            },
            new String [] { // work_type_remblai
                    // There is no geometry data for remblai
            },
            new String [] { // work_type_tranchee
                    // There is no geometry data for trnachee
            },
            new String [] { // work_type_tranchee
                    // There is no geometry data for trnachee
            },
            new String [] { // work_type_trv
                    // There is no geometry data for trv
            }
    }
        
    };
    
    public static String[][][] WORK_GEOMETRY_ARRAYAutre = new String[][][]{        
        new String [][] { // work_category_Bat
                new String [] { // work_type_bnc
                        // There is no geometry data for bnc
                }
        },
        new String [][] { // work_category_OA
                new String [] { // work_type_aqc
                        "geometry_cdr",     		
                        "geometry_pc",     		
                        "geometry_prt",    											    
                        "geometry_psd",    		
                        "geometry_srb",
                },
                new String [] { // work_type_bse
                        "geometry_circ" ,
                        "geometry_ov",
                },
                new String [] { // work_type_dcp
                        // There is no geometry data for dcp
                },
                new String [] { // work_type_digoa
                        // There is no geometry data for digoa
                },
                new String [] { // work_type_dlt
                        // There is no geometry data for dlt
                },
                new String [] { // work_type_gal
                        "geometry_pc", 
                        "geometry_srb" ,
                },
                new String [] { // work_type_gt
                        "geometry_pc", 
                        "geometry_srb" , 
                },
                new String [] { // work_type_ms
                        // There is no geometry data for ms
                },
                new String [] { // work_type_pas
                        "geometry_cdr",
                        "geometry_srb",
                        "geometry_prt",
                        "geometry_psd",
                },
                new String [] { // work_type_pc
                        // There is no geometry data for pc
                },
                new String [] { // work_type_pcl
                        "geometry_cdr",
                        "geometry_srb",
                        "geometry_pc",
                        "geometry_prt",
                        "geometry_psd",
                },
                new String [] { // work_type_per
                        // There is no geometry data for per
                },
                new String [] { // work_type_pra
                		"geometry_cdr",
                        "geometry_srb", 
                        "geometry_pc",
                        "geometry_prt",
                        "geometry_psd",
                },
                new String [] { // work_type_pro
                		"geometry_cdr",
                        "geometry_srb",
                        "geometry_pc",
                        "geometry_prt",
                        "geometry_psd",
                },
                new String [] { // work_type_sdm
                		"geometry_cdr",
                        "geometry_srb",
                        "geometry_pc",
                        "geometry_prt",
                        "geometry_psd",
                },
                new String [] { // work_type_tc
                        "geometry_srb"
                },
                new String [] { // work_type_tun
                        "geometry_pc", 
                        "geometry_srb",
                },
                new String [] { // work_type_via
                		"geometry_cdr",
                        "geometry_srb",
                        "geometry_pc",
                        "geometry_psd",
                        "geometry_prt",
                }
        },
        new String [][] { // work_category_OT
                new String [] { // work_type_deblai
                        // There is no geometry data for deblai
                },
                new String [] { // work_type_digot
                        // There is no geometry data for digot
                },
                new String [] { // work_type_mxt
                        // There is no geometry data for mxt
                },
                new String [] { // work_type_pr
                        // There is no geometry data for pr
                },
                new String [] { // work_type_remblai
                        // There is no geometry data for remblai
                },
                new String [] { // work_type_tranchee
                        // There is no geometry data for trnachee
                },
                new String [] { // work_type_trv
                        // There is no geometry data for trv
                },
                new String [] { // work_type_versant
                        // There is no geometry data for versant
                }
        },
        new String [][] { // work_category_Others
                new String [] { // work_type_eab
                        "geometry_tripode"
                },
                new String [] { // work_type_mgh
                		"geometry_cyl",
                		"geometry_qua",
                        "geometry_tripode"
                },
                new String [] { // work_type_portique
                        "geometry_tripode"
                },
                new String [] { // work_type_potence
                },
                new String [] { // work_type_pyl
                		"geometry_cyl",
                		"geometry_qua",
                        "geometry_tripode"
                },
                new String [] { // work_drainage,
                        "geometry_cdr",
                        "geometry_circ",
                },
                new String [] { // work_traversee
                        "geometry_cdr",
                        "geometry_circ",
                }
        }
    };

    public static String[] WORK_FSA_ARRAY= new String[]{
    	"fsa_oui",
    	"fsa_special_exposed",
    	"fsa_none",
    };
    
    public static String[] AUTEUR_FSA_ARRAY= new String[]{
    	"fsa_aoap",
    	"fsa_soar",
    	"fsa_tiers",
    };
       

    public static String[] WORK_MATERIAL_ARRAY= new String[]{
        "material_Autres",
        "material_Al",
        "material_Beton",
        "material_Ba",
        "material_Bp",
        "material_Bpro",
        "material_Bv",
        "material_Bs",
        "material_Ci",
        "material_Div",
        "material_Fibro",
        "material_Gb",
        "material_Mie",
        "material_Mtl",
        "material_Mix",
        "material_Ps",
        "material_Pise",
        "material_Tpe",  
        "material_Pvc",
        "material_Re" ,        
    };
    
    public static String[] MATERIAU_ARRAY= new String[]{
        "material_Acier",
        "material_Fp",
        "material_Fonte"  
    };

    public static String[] WORK_MATERIAL_ARRAY_1= new String[]{ // List of
																// materials for
																// bnc
        "material_Ba",        
        "material_Bp",
        "material_Bs",        
        "material_Div",        
        "material_Mie",
        "material_Mtl",
        "material_Mix",
    };

    public static String[] WORK_MATERIAL_ARRAY_2= new String[]{ // List of
																// materials for
																// aqc, dcp,
																// dlt, gal, gt,
																// oce, pcl, tr,
																// tun
        "material_Ba",      
        "material_Mie",
    };
    public static String[] WORK_MATERIAL_ARRAY_3= new String[]{ // List of
																// materials for
																// bse
        "material_Ba",				
        "material_Beton",
        "material_Ci",
        "material_Fibro",		
        "material_Mie",				
        "material_Mtl",
        "material_Pvc",
    };
    public static String[] WORK_MATERIAL_ARRAY_4= new String[]{ // List of
																// materials for
																// digoa, pc
        "material_Ba",     

    };
    public static String[] WORK_MATERIAL_ARRAY_5= new String[]{ // List of
																// materials for
																// ms
        "material_Ba",
        "material_Mie",
        "material_Pise"

    };

    public static String[] WORK_MATERIAL_ARRAY_6= new String[]{ // List of
																// materials for
																// pas
        "material_Ba",						
        "material_Mie",					
        "material_Mtl",					
        "material_Mix",					
        "material_Tpe",					
        "material_Re" 
    };

    public static String[] WORK_MATERIAL_ARRAY_7= new String[]{ // List of
																// materials for
																// per
        "material_Ba",	
        "material_Bv",
        "material_Mie",
        "material_Ps"
    };

    public static String[] WORK_MATERIAL_ARRAY_8= new String[]{ // List of
																// materials for
																// pra, pro, sdm
        "material_Ba",
        "material_Bp",
        "material_Mie",					
        "material_Mtl",					
        "material_Mix",
        "material_Tpe",					
        "material_Re" 
    };

    public static String[] WORK_MATERIAL_ARRAY_9= new String[]{ // List of
																// materials for
																// via
        "material_Ba",	
        "material_Bp",
        "material_Mie",					
        "material_Mtl",					
        "material_Mix",
        "material_Tpe"
    };

    public static String[] WORK_MATERIAL_ARRAY_10= new String[]{ // List of
																	// materials
																	// for
																	// drainage,
																	// traversee
        "material_Ba",  
        "material_Beton",
        "material_Div",                 
        "material_Mie"
    };
    
    public static String[] WORK_MATERIAL_ARRAY_AUTRES= new String[]{ 
		"material_Al",
		"material_Mtl",
		"material_Autres"
	};

    public static String[] WORK_OCCUPATION_ARRAY= new String[]{
        "occupation_sncf_infra",
        "occupation_sncf_other",
        "occupation_tiers",
        "occupation_transports_Robineau",
        "occupation_stockage_v",
        "occupation_materiel_v",
        "occupation_emt_vaise",
        "occupation_stockage_materiaux_demolition",
        "occupation_parking",
        "occupation_stockage_service_electrique",
        "occupation_emr",
        "occupation_inoccupe",
        "occupation_evlog_+_upses",
        "occupation_crem",
        "occupation_depot"
    };

    public static String[] WORK_SITUATION_ARRAY= new String[]{
        "situation_up_left",
        "situation_left",
        "situation_down_left",
        "situation_up_right",
        "situation_right",
        "situation_down_right",
        "situation_up",
        "situation_down",
    };

    public static String[] WORK_SITUATION_ARRAY_1= new String[]{  // List of
																	// location
																	// for bnc,
																	// eab,
																	// flood,
																	// mgh,
																	// portique,
																	// potence,
																	// pyl
        "situation_up",        
        "situation_right",        
        "situation_down",        
        "situation_left"
    };
    public static String[] WORK_SITUATION_ARRAY_2= new String[]{ // List of
																	// location
																	// for ms,
																	// ota, pc
        "situation_up_left",        
        "situation_up_right",        
        "situation_down_right",        
        "situation_down_left",       
    };

    public static String[] WORK_SITUATION_ARRAY_3= new String[]{ // List of
																	// location
																	// for OT
																	// mixt
        "situation_left_right",        
        "situation_right_left"       
    };

    public static String[] WORK_MAINTENANCE_NATURE_ARRAY= new String[]{
        "nature_autre",
        "nature_debroussaillage",
        "nature_curage",
        "nature_peint_gc"
    };

    public static String[] WORK_MAINTENANCE_EMERGENCY_ARRAY= new String[]{
        "emergency_0",
        "emergency_1",
        "emergency_2" 
    };
    
    public static String[] WORK_MAINTENANCE_EMERGENCY_ENTIER_ARRAY= new String[]{
        "U0",
        "U1",
        "U2" 
    };

    public static String[] WORK_SUPPORTING_TYPE_ARRAY= new String[]{
        "support_type_Anc",
        "support_type_Aut",
        "support_type_Bp",
        "support_type_Cb",
        "support_type_Ce",
        "support_type_Clous",
        "support_type_Efd",
        "support_type_Efp",
        "support_type_Enroc",
        "support_type_Ert",
        "support_type_Gab",
        "support_type_Gp",
        "support_type_Gpa",
        "support_type_Md",
        "support_type_Mp",
        "support_type_Peller",
        "support_type_Pieux",
        "support_type_pneusol",
        "support_type_td"
    };

    public static String[] WORK_INCIDENT_IMPORTANCE_ARRAY= new String[]{
        "inc_imp_1a",
        "inc_imp_1b",
        "inc_imp_2",
        "inc_imp_3",
        "inc_imp_4a",
        "inc_imp_4b"
    };
    
    public static String[] TYPE_ECR_ARRAY= new String[]{
        "ecr1",
        "ecr2",
        "ecr3",
        "ecr4",
        "ecr5",
        "ecr6"
    };
    
    public static String[] TYPE_CAMPAGNE_ARRAY= new String[]{
        "Autres",
        "Géophysique",
        "Sondage"
    };
    
    public static String[] WORK_SURVEILLANCE_MEANS_ARRAY= new String[]{
    	ALL,
    	"means_4axes",
    	"means_acrobate",
        "means_Brg",
        "means_Brq",
        "means_divers",
        "means_drone",
        "means_Ech",
        "means_Elise",
        "means_endoscope",
        "means_Mg1",
        "means_Mg2",
        "means_nacelle",
        "means_PF245",
        "means_PF2",
        "means_PF3",
        "means_PF4",
        "means_PF5",
        "means_PF6",
        "means_PF7",
        "means_plongeur",
        "means_Robot",
        "means_signal",
        "means_S65/3",
        "means_S65/4",
        "means_topo",
        "means_U26",
        "means_U32",
        "means_vision",
        "means_WIT"
    }; 

    public static String[] WORK_SURVEILLANCE_BEFORE2007_ARRAY= new String[]{        
        "before_2007_0/5",
        "before_2007_1/6",
        "before_2007_2/7",
        "before_2007_3/8",
        "before_2007_4/9",
        "before_2007_an"
    };

    public static String[] WORK_VISIT_TYPE_OA_ARRAY = new String[]{
        "visit_type_id",
        "visit_type_idi",
        "visit_type_vd",
        "visit_type_vi",
        "visit_type_vs",
        "visit_type_vsp",
        "visit_type_fsa",
        "visit_type_pv0",
        "visit_type_hydro",
        "visit_type_id_cv",
        "visit_type_vd_cv"
    };     
    
    public static String[] WORK_VISIT_TYPE_OA_PERIODIQUE_ARRAY = new String[]{
        "visit_type_id",
        "visit_type_idi",
        "visit_type_vd",
        "visit_type_vi",
        "visit_type_vs",
        "visit_type_pv0",
        "visit_type_id_cv",
        "visit_type_vd_cv"
    }; 
    
    public static String[] WORK_VISIT_TYPE_OA_EXPERTISE_ARRAY = new String[]{
        "visit_type_vsp",
        "visit_type_hydro"
    }; 
    
    public static String[] WORK_VISIT_TYPE_OA_COMPLEMENTAIRE_ARRAY = new String[]{
        "visit_type_sr",
        "visit_type_sp"
    }; 
    
    public static String[] WORK_VISIT_TYPE_OT_ARRAY = new String[]{
    	"visit_type_cl_ot",
    	"visit_type_vd_ot",
        "visit_type_vi_ot",
        "visit_type_vp_ot",
        "visit_type_va_ot"
    }; 
    public static String[] WORK_VISIT_TYPE_OT_PERIODIQUE_ARRAY = new String[]{
    	"visit_type_cl_ot",
    	"visit_type_vd_ot",
        "visit_type_vi_ot",
        "visit_type_vp_ot",
        "visit_type_va_ot"
    }; 
    public static String[] WORK_VISIT_TYPE_OT_COMPLEMENTAIRE_ARRAY = new String[]{
    	"visit_type_sr",
        "visit_type_sp"
    };
    
    public static String[] WORK_VISIT_TYPE_OT_EXPERTISE_ARRAY = new String[]{
        "visit_type_ve_ot",
        "visit_type_hydro"
    };
    
    public static String[] WORK_MAINTENANCE_URGENCE_ARRAY = new String[]{
        "urgence_0",
        "urgence_1",
        "urgence_2",
    }; 
    
    public static String[] WORK_VISIT_TYPE_AUTRES_ARRAY = new String[]{
        "visit_type_id",
        "visit_type_idi",
        "visit_type_vd",
        "visit_type_vi",
        "visit_type_vs",
        "visit_type_vsp",
        "visit_type_fsa",
        "visit_type_pv0",
        "visit_type_hydro",
        "visit_type_id_cv",
        "visit_type_vd_cv"
    }; 
    public static String[] WORK_VISIT_TYPE_AUTRES_PERIODIQUE_ARRAY = new String[]{
        "visit_type_id",
        "visit_type_idi",
        "visit_type_vd",
        "visit_type_vi",
        "visit_type_vs",
        "visit_type_pv0",
        "visit_type_id_cv",
        "visit_type_vd_cv"
    }; 
    public static String[] WORK_VISIT_TYPE_AUTRES_EXPERTISE_ARRAY = new String[]{
        "visit_type_vsp",
        "visit_type_hydro"
    }; 
    public static String[] WORK_VISIT_TYPE_AUTRES_COMPLEMENTAIRE_ARRAY = new String[]{
    	"visit_type_sr",
        "visit_type_sp"
    }; 
    
    public static String[] WORK_VISIT_TYPE_BNC_ARRAY = new String[]{
        "visit_type_id",
        "visit_type_idi",
        "visit_type_vd",
        "visit_type_vi",
        "visit_type_vsp",
        "visit_type_fsa",
        "visit_type_pv0",
        "visit_type_hydro",
        "visit_type_id_cv",
        "visit_type_vd_cv"
    }; 
    public static String[] WORK_VISIT_TYPE_BNC_PERIODIQUE_ARRAY = new String[]{
        "visit_type_id",
        "visit_type_idi",
        "visit_type_vd",
        "visit_type_vi",
        "visit_type_pv0",
        "visit_type_id_cv",
        "visit_type_vd_cv"
    }; 
    public static String[] WORK_VISIT_TYPE_BNC_EXPERTISE_ARRAY = new String[]{
        "visit_type_vsp",
        "visit_type_hydro"
    }; 
    public static String[] WORK_VISIT_TYPE_BNC_COMPLEMENTAIRE_ARRAY = new String[]{
        "visit_type_sr",
        "visit_type_sp"
    }; 
    
    public static String[] WORK_VISIT_TYPE_ADMIN_ARRAY = new String[]{
        "visit_type_id",
        "visit_type_idi",
        "visit_type_cl_ot",
        "visit_type_vd",
        "visit_type_vd_ot",
        "visit_type_vi",
        "visit_type_vi_ot",
        "visit_type_vp_ot",
        "visit_type_va_ot",
        "visit_type_vs",
        "visit_type_vsp",
        "visit_type_fsa",
        "visit_type_pv0",
        "visit_type_ve_ot",
        "visit_type_hydro",
        "visit_type_id_cv",
        "visit_type_vd_cv",
        "visit_type_sp",
        "visit_type_sr",
        "visit_type_topo",
        "visit_type_inclino"
    };
    
    public static String[] WORK_VISIT_TYPE_ARRAY = new String[]{
        "visit_type_id",
        "visit_type_idi",
        "visit_type_cl_ot",
        "visit_type_vd",
        "visit_type_vd_ot",
        "visit_type_vi",
        "visit_type_vi_ot",
        "visit_type_vp_ot",
        "visit_type_va_ot",
        "visit_type_vs",
        "visit_type_pv0",
        "visit_type_id_cv",
        "visit_type_vd_cv"
    };
    
    public static String[] WORK_VISIT_TYPE_COMPLEMENTAIRE_ARRAY = new String[]{
        "visit_type_sp",
        "visit_type_sr",
        "visit_type_topo",
        "visit_type_inclino"
    }; 
    
    public static String[] WORK_VISIT_TYPE_ENTREPRISE_ARRAY = new String[]{
        "visit_type_fsa",
        "visit_type_fsa_exposed",
    }; 
    
    public static String[] WORK_VISIT_TYPE_ENTREPRISE_NEW_ARRAY = new String[]{
        "visit_type_fsa",
    }; 
    
    public static String[] WORK_VISIT_TYPE_EXPERTISE_ARRAY = new String[]{
        "visit_type_vsp",
        "visit_type_ve_ot",
        "visit_type_hydro"
    }; 
    
    public static String[] WORK_VISIT_TYPE_DOUBLON_ARRAY = new String[]{
        "visit_type_id",
        "visit_type_idi",
        "visit_type_sp",
        "visit_type_sr",
        "visit_type_cl_ot",
        "visit_type_vd",
        "visit_type_vd_ot",
        "visit_type_vi",
        "visit_type_vi_ot",
        "visit_type_vp_ot",
        "visit_type_va_ot",
        "visit_type_vs",
        "visit_type_id_cv",
        "visit_type_vd_cv",
        "visit_type_vd_camera",
        "visit_type_id_camera",
        "visit_type_id_cv_camera",   
        "visit_type_vsp",
        "visit_type_fsa",
        "visit_type_pv0",
        "visit_type_ve_ot"
    }; 
    
    public static String[] WORK_VISIT_TYPE_SOAR_ARRAY = new String[]{
        "visit_type_vsp",
    }; 
    
    public static String[] STATUT_ARCHIVES_ARRAY = new String[]{
        "libre",
        "emprunte",
        "emprunte_byself"
    }; 
    


    public static String[] WORK_LABOR_TYPE_ARRAY= new String[]{
        "labor_type_olr",
        "labor_type_oln",
        "labor_type_oge",
        "labor_type_tiif",
        "labor_type_current_ent",
    };

    public static String[] LABOR_VALIDATION = new String[] {
        "lab_vil_unvalidated",
        "lab_vil_positive",
        "lab_vil_negative"
    };
    
    public static String[] DELAI_TRAVAUX_U0 = new String[] {
        "delai_0_3m",
        "delai_3_6m",
        "delai_6_9m",
        "delai_9_12m"
    };
    
    public static String[] DELAI_TRAVAUX_U1 = new String[] {
        "delai_1_2a",
        "delai_2_3a",
        "delai_3_4a",
        "delai_4_5a",
        "delai_5_6a"
    };
    
    public static String[] DELAI_TRAVAUX_U2 = new String[] {
        "delai_sup6a"
    };
    
    public static String[] DELAI_TRAVAUX_FULL = new String[] {
    	 "delai_0_3m",
         "delai_3_6m",
         "delai_6_9m",
         "delai_9_12m",
         "delai_1_2a",
         "delai_2_3a",
         "delai_3_4a",
         "delai_4_5a",
         "delai_5_6a",
         "delai_sup6a"
    };
    
    public static String[] MOTIF_CV = new String[] {
        "absence_engin",
        "presence_vegetation",
        "curage_a_faire",
        "impossibilite_acces",
        "necessite_robot",
        "necessite_fsa"
    };
    
    public static String[][] SOLUTION_CV = new String[][]{
        new String[]{ // absence_engin
                "reprogrammation_engin",
                "visite_cordiste"
        },
        new String[]{  // presence_vegetation
                "debroussaillage"
        },
        new String[]{  // curage_a_faire
                "ouvrage_a_curer"
        },
        new String[]{ // impossibilite_acces
        		"demande_acces",
        		"entreprise_specialisee"
        },
        new String[]{ // necessite_robot
        		"programmation_robot"
        },
        new String[]{ // necessite_fsa
        		"programmation_fsa"
        }
    };

    public static String[] WORK_EVEN_TYPE_ARRAY= new String[]{
        "Option1",
        "Option2",
        "Option3" 
    };

    public static String[] WORK_SOAR_ARRAY= new String[]{
        // WARNING : This menu is incorrectly defined as a static menu (with no
		// content),
        // it should be a dynamic list of SOAR users from the USER_TABLE table.
    };

    public static String[] WORK_LEVEL = new String[] {
        // "parentWork",
        "parentAndChildWork",
        "childWork",

    };

    public static String[] OT_MIXTE_TYPE = new String[] {
        "OMT_01",
        "OMT_02",
        "OMT_03",
        "OMT_05",
        "OMT_05",
        "OMT_06",
        "OMT_07",
        "OMT_08",
        "OMT_09",
        "OMT_10",
    };

    public static final String[] UIC_ARRAY = new String[] {
        "uic_3",
        "uic_4",
        "uic_5",
        "uic_6",
        "uic_6AV",
        "uic_7AV",
        "uic_7SV",
        "uic_8AV",
        "uic_8SV",
        "uic_9AV",
        "uic_9SV",
        "uic_emb",
        "uic_neut",
        "uic_none"    
    };

    public static final String[] RATTACHEMENT_ARRAY = new String[] {
        "rattachement_drainage",
        "rattachement_mur",
        "rattachement_ot",
        "rattachement_souterrain",
        "rattachement_petit",
        "rattachement_inferieur",
        "rattachement_superieur",
        "rattachement_titre3",
        "rattachement_sou_voie",
        
    };
    
    public static final String[] RATTACHEMENT_ARRAY_OT = new String[] {
        "rattachement_otc",
        "rattachement_otp",
        "rattachement_ots",
        
    };
    
    public static final String[] SPEED_ARRAY = new String[] {
	    "speed_20",
	    "speed_30",
	    "speed_40",
	    "speed_50",
	    "speed_55",
	    "speed_60",
	    "speed_70",
	    "speed_80",
	    "speed_85",
	    "speed_90",
	    "speed_95",
	    "speed_100",
	    "speed_105",
	    "speed_110",
	    "speed_115",
	    "speed_120",
	    "speed_125",
	    "speed_130",
	    "speed_140",
	    "speed_150",
	    "speed_160",
	    "speed_170",
	    "speed_200",
	    "speed_220",
	    "speed_230",
	    "speed_270",
	    "speed_300",
	    "speed_320",
	    "speed_350",
    };

    public static String[] Boolean_Array = new String[] {
    	"true",
    	"false",
    };
    
    public static String[] ETAT_CV = new String[] {
    	"actif",
    	"inactif"
    };
    
    public static String[] ETAT_CV_VAL = new String[] {
    	"actif",
    	"inactif",
    	"inactif_val"
    };
    
    public static String[] ETAT_OUVRAGE = new String[] {
    	"Bon",
    	"Moyen",
    	"Mauvais"
    };
    
    public static String[] TYPE_EXPERTISE = new String[] {
    	"Etancheite",
    	"Peinture",
    	"Hydro",
    	"Soar",
    	"Autres"
    };
    
    public static String[] OT_NATURE_ARRAY = new String[] {
    	"meuble",
    	"rocheux",
    	"meuble_rocheux",
    };
    
    public static String[] NIVEAU_ETAT = new String[] {
    	"Non défini",
    	"N1",
    	"N2",
    	"N3",
    	"N4",
    	"N5"
    };
    
    public static String[] NIVEAU_GRAVITE = new String[] {
    	"Non défini",
    	"G1",
    	"G2",
    	"G3",
    	"G4",
    	"G5"
    };
    
    public static String[] LIST_DOSSIER = new String[] {
    	"1-Compte rendu",
    	"2-Fiche Signalétique",
    	"3-Photos",
    	"4-PV VD",
    	"5-Travaux",
    	"6-Transmis",
    	"7-Divers",
    	"8-Confidentiel",
    	"9-Fiche de reclassement",
    	"10-Fiche de visite particulière",
    };
    
    public static String[] array2To1(String[][] str ) {
        int i = 0;
        int j = 0;
        int k = 0;
        int index = 0;
        k = arrayLength(str);

        String[] strReturn = new String[k];    	
        for(i=0;i<str.length;i++) {
            for (j=0;j<str[i].length;j++) {    			
                strReturn[index++] = str[i][j];
            }
        }     	
        return strReturn;
    }

    /*
	 * calculate the length of String[][] when changed to String[] @return
	 * Integer length
	 */
    private static int arrayLength(String[][] str) {
        int i = 0;
        int k = 0;
        if (str == null) {
            return k = 0;
        }
        for(i=0;i<str.length;i++) {
            k += str[i].length;	
        } 
        return k; 
    }


   /* public static int findRegionIndex(String s) {
        int i = -1;
        int j = 0; 
        int k ;
        String[][] str =MenuData.DEPARTEMENTS;

        for(k=0;k<str.length;k++) {
            for (j=0;j<str[k].length;j++) {
                if (str[k][j]== s) {
                    return k;
                }
            }
        }
        return i;
    }*/
    
  /*  public static int findInfrapoleIndex(String s) {
        int i = -1;
        int j = 0; 
        int k ;
        String[][] str =MenuData.REGIONS;

        for(k=0;k<str.length;k++) {
            for (j=0;j<str[k].length;j++) {
                if (str[k][j]== s) {
                    return k;
                }
            }
        }
        return i;
    }*/
    
    public static int findCategoryIndex(String s) {
        int i = -1;
        int j = 0; 
        int k ;
        String[][] str =MenuData.WORK_TYPE_ARRAY;

        for(k=0;k<str.length;k++) {
            for (j=0;j<str[k].length;j++) {
                if (str[k][j]== s) {
                    return k;
                }
            }
        }
        return i;
    }
    

    /*
	 * change Array String[][][] to String[][] in order to make a link menu
	 */
    public static String[][] array3To2(String[][][] str){
        int i;
        int m;
        int n;
        int l = 0;
        int first = 0;
        if (str == null) {
            return null;
        }
        for(i=0;i<str.length;i++) {
            first += str[i].length;
        } 

        String[][] strReturn = new String[first][];

        for(m=0;m<str.length;m++){
            for(n=0;n<str[m].length;n++) {
                strReturn[l++] = str[m][n];
            }
        }   	   	

        return strReturn;
    }
    
    /**
	 * Array to Set ,then Set to Array
	 * 
	 * @param obj
	 * @return
	 */
    public static String[] array2Set2Array(String[] array) {
    	String[] str = array;
    	Set<String> stringSet = new HashSet<String>();
    	int strLength = str.length;
    	for (int i=0;i<strLength;i++) {
    		stringSet.add(str[i]);
    	}
    	if (stringSet != null) {
    		str = (String[]) stringSet.toArray(new String[0]);
    	}else {
    		str = null;
    	}
    	return str;
    }
    
    // /////////for EditWorkSurveillance page //////////////
    
    public static String  VISIT_TYPE_SURV_TIERS ="surv_tiers";
    public static String VISIT_TYPE_SURV_CURRENT ="surv_current";
    
    public static final String[] VISIT_TYPE_ARRAY = new String[] {
        "ID",
        "VD",
        "VS",
        VISIT_TYPE_SURV_TIERS,
        VISIT_TYPE_SURV_CURRENT,
    };
    
    public static final String[] VISIT_TYPE_ARRAY_OT = new String[] {
        "VD_OT",
        "VP_OT",
        "VA_OT",
        VISIT_TYPE_SURV_CURRENT,
    };
    
    public static final String[] HOME_VISIT_TYPE_ARRAY = new String[] {
    	"visit_type_id",
    	"visit_type_vd",
    	"visit_type_vd_ot",
    	"visit_type_vs",
    	"visit_type_vi",
    	"visit_type_vi_ot",
    	"visit_type_idi",
    	"visit_type_vp_ot",
    	"visit_type_va_ot",
        "visit_type_sp",
        "visit_type_sr",
        VISIT_TYPE_SURV_TIERS,
        VISIT_TYPE_SURV_CURRENT,
    };
    
    public static final String[] HOME_VISIT_TYPE_OA_ARRAY = new String[] {
    	"visit_type_id",
    	"visit_type_vd",
    	"visit_type_vs",
    	"visit_type_vi",
    	"visit_type_idi",
        "visit_type_sp",
        "visit_type_sr",
        VISIT_TYPE_SURV_TIERS,
        VISIT_TYPE_SURV_CURRENT,
    };
    
    public static final String[] HOME_VISIT_TYPE_OT_ARRAY = new String[] {
    	"visit_type_vd_ot",
    	"visit_type_vi_ot",
    	"visit_type_vp_ot",
    	"visit_type_va_ot",
    	VISIT_TYPE_SURV_TIERS,
        VISIT_TYPE_SURV_CURRENT,
    };
    
    public static final String[] HOME_VISIT_TYPE_BNC_ARRAY = new String[] {
    	"visit_type_id",
    	"visit_type_vd",
    	"visit_type_vi",
        "visit_type_sp",
        "visit_type_sr",
        VISIT_TYPE_SURV_TIERS,
        VISIT_TYPE_SURV_CURRENT,
    };
    
    public static final String[] HOME_VISIT_TYPE_AUTRES_ARRAY = new String[] {
    	"visit_type_id",
    	"visit_type_vd",
    	"visit_type_vi",
    	"visit_type_idi",
        "visit_type_sp",
        "visit_type_sr",
        VISIT_TYPE_SURV_TIERS,
        VISIT_TYPE_SURV_CURRENT,
    };

    public static final String[] COMPLEMENTARY_VISIT_ARRAY = new String[] {
        "SP",
        "SR",
        "SP&SR"
    };
    
    
    public static final String[] PERIOD_ARRAY = new String[] {
        "period_1d",
        "period_2d",
        "period_3d",
        "period_4d",
        "period_5d",
        "period_1w",
        "period_2w",
        "period_3w",
        "period_1m",
        "period_2m",
        "period_3m",
        "period_4m",
        "period_6m",
        "period_1y",
        "period_2y",
        "period_3y",
        "period_6y"
    };
    
    public static final Integer[] MONTH_ARRAY = new Integer[] {
        1,
        2,
        3,
        4, 
        7, 
        6, 
        7, 
        8, 
        9, 
        10, 
        11, 
        12
    };
    
    /*
	 * public static String[] STATUS_ARRAY = new String[]{ "red",//red 0
	 * "orange",//orange 1 "green" //green 2 };
	 */
    
    public static Integer[] SR_NUMBER_ARRAY = new Integer[]{
    	1,// SR
    	2,// SR2
    	3// SR3
    };
    
    
    
    public static String[] AUTHOR_TYPE_ARRAY = new String[]{
    	"FUNCTION_AOAP",
    	"FUNCTION_SOAR",
    	"FUNCTION_ABE",
    	"FUNCTION_AH",
    	"FUNCTION_COT",
    	"FUNCTION_TIERS"
    };
    
    public static String[] REQUETE = new String[]{
    	"BoolRequeteIN3044",
    	"BoolRequeteIN3044Infrapole",
    	"OA_Bilan_VD_AOAP_realisees",
    	"OA_Bilan_VD_AOAP_todo",
    	"OA_Bilan_IDI_realisees",
    	"OA_Bilan_IDI_SOAR_realisees",
    	"OA_Bilan_propositions_travaux_toutes_urgences",
    	"OA_Bilan_propositions_travaux_toutes_urgences_inf9k",
    	"OA_Bilan_propositions_travaux_toutes_urgences_sup9kinf40k",
    	"OA_Bilan_propositions_travaux_toutes_urgences_sup40kinf150k",
    	"OA_Bilan_propositions_travaux_toutes_urgences_sup150k",
    	"OA_Liste_OA_instrumentes",
    	"OT_Bilan_VI",
    	"OT_Patrimoine",
    	"OA_Patrimoine"
    };
    
    public static String[] PROPOSITION_TRAVAUX = new String[]{
    	"Proposition_travaux_U0",
    	"Proposition_travaux_U1",
    	"Proposition_travaux_U2",
    
    };
}
