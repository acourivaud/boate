package com.boate.utils;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import org.json.JSONObject;

import com.boate.enums.TypeVisiteEnum;
import com.boate.model.Ouvrage;
import com.boate.model.Proprietaire;
import com.boate.model.Surveillance;

public class WhereClauseRequestHelper {

	CriteriaBuilder cb;

	public WhereClauseRequestHelper(CriteriaBuilder cb) {
		this.cb = cb;
	}

	public Predicate idMinParam(String value, Path<Number> id) {
		Integer min = Integer.parseUnsignedInt(value);
		Predicate predicate = cb.gt(id, min);
		return predicate;
	}

	public Predicate idMaxParam(String value, Path<Number> id) {
		Integer max = Integer.parseUnsignedInt(value);
		Predicate predicate = cb.lt(id, max);
		return predicate;
	}

	public Predicate valueEquals(Expression<?> attribut, Class<?> typeDonneeModel, String value) {
		Predicate predicate = cb.equal(attribut, typeDonneeModel.equals(Double.class) ? convertStringToDouble(value)
				: typeDonneeModel.equals(Boolean.class) ? convertStringToBoolean(value) : value);
		return predicate;
	}

	public Predicate valueLike(Path<String> attribut, Class<?> typeDonneeModel, String value) {
		Predicate predicate = cb.like(attribut, "%" + value + "%");
		return predicate;
	}

	public Predicate isNotNull(Expression<?> attribut) {
		Predicate predicate = cb.isNotNull(attribut);
		return predicate;
	}
	
	public Predicate isNull(Expression<?> attribut) {
		Predicate predicate = cb.isNull(attribut);
		return predicate;
	}

	public Predicate greaterEquals(Path<Number> attribut, Class<?> typeDonneeModel, String value) {
		Predicate predicate = cb.ge(attribut, convertStringToDouble(value));
		return predicate;
	}

	public Predicate lowerEquals(Path<Number> attribut, Class<?> typeDonneeModel, String value) {
		Predicate predicate = cb.le(attribut,
				typeDonneeModel.equals(Double.class) ? convertStringToDouble(value)
						: typeDonneeModel.equals(Integer.class) ? convertStringToInteger(value)
								: convertStringToInteger(value));
		return predicate;
	}
	
	public Predicate attributeMultipleEquals(List<Path<Number>> listAttributes, Class<?> typeDonneeModel, String value) {
		
		Predicate[] listPredicates = new Predicate[listAttributes.size()];
		
		for (int i = 0; i < listAttributes.size(); i++) {
			listPredicates[i] = cb.equal(listAttributes.get(i), typeDonneeModel.equals(Double.class) ? convertStringToDouble(value)
					: typeDonneeModel.equals(Integer.class) ? convertStringToInteger(value)
							: typeDonneeModel.equals(Boolean.class) ? convertStringToBoolean(value) : value);
		}
		
		return cb.or(listPredicates);
	}
	
	public Predicate valueMultipleEquals(Path<Number> listAttributes, List<TypeVisiteEnum> value) {
		
		Predicate[] listPredicates = new Predicate[value.size()];
		
		for (int i = 0; i < value.size(); i++) {
			listPredicates[i] = cb.equal(listAttributes, value.get(i));
		}
		
		return cb.or(listPredicates);
	}

	public Predicate proprietaireSearch(Path<Proprietaire> attributFirstOwner, Path<Proprietaire> attributSecondOwner, Path<Proprietaire> attributThirdOwner, String typeProprietaire, String nomProprietaire) {
		Predicate predicate = null;
		
		if (typeProprietaire != null && nomProprietaire == null) {
			if (typeProprietaire.equalsIgnoreCase("SNCF")) {
				predicate = cb.or(cb.equal(attributFirstOwner.get("name"), typeProprietaire.toUpperCase()), 
						cb.equal(attributSecondOwner.get("name"), typeProprietaire.toUpperCase()), 
						cb.equal(attributThirdOwner.get("name"), typeProprietaire.toUpperCase()));
			}else {
				predicate = cb.or(cb.notEqual(attributFirstOwner.get("name"), typeProprietaire.toUpperCase()), 
						cb.notEqual(attributSecondOwner.get("name"), typeProprietaire.toUpperCase()), 
						cb.notEqual(attributThirdOwner.get("name"), typeProprietaire.toUpperCase()));
			}
		}else {
			predicate = cb.or(cb.equal(attributFirstOwner.get("id"), nomProprietaire), 
					cb.equal(attributSecondOwner.get("id"), nomProprietaire), 
					cb.equal(attributThirdOwner.get("id"), nomProprietaire));
		}
		
		return predicate;
	}

	public Predicate pkSearch(Path<Ouvrage> ouvrage, String pkDebut, String pkFin) {

		Expression<Number> attributPk = ouvrage.get("pk");
		Expression<Number> attributMiddlePk = ouvrage.get("pk");
		Expression<Number> attributEndPk = ouvrage.get("pk");
		Predicate predicate = null;
		Double pkDebutDouble = pkDebut != null ? Double.parseDouble(pkDebut.replace(",", ".")) : null;
		Double pkFinDouble = pkFin != null ? Double.parseDouble(pkFin.replace(",", ".")) : null;
		if (pkDebutDouble != null && pkFinDouble == null) {
			predicate = cb.or(cb.ge(attributPk, pkDebutDouble), cb.ge(attributMiddlePk, pkDebutDouble),
					cb.ge(attributEndPk, pkDebutDouble));
		} else if (pkDebutDouble == null && pkFinDouble != null) {
			predicate = cb.or(cb.le(attributPk, pkFinDouble), cb.le(attributMiddlePk, pkFinDouble),
					cb.le(attributEndPk, pkFinDouble));
		} else {
			predicate = cb.or(cb.and(cb.le(attributPk, pkFinDouble), cb.ge(attributPk, pkDebutDouble)),
					cb.and(cb.le(attributMiddlePk, pkFinDouble), cb.ge(attributMiddlePk, pkDebutDouble)),
					cb.and(cb.le(attributEndPk, pkFinDouble), cb.ge(attributEndPk, pkDebutDouble)));
		}

		return predicate;
	}
	
	public List<Predicate> typeSurveillanceSearch(Path<Surveillance> surveillance, String typeVisiteValue, String periodiciteValue, String responsableSurveillanceValue) {

		List<Predicate> listPredicates = new ArrayList<Predicate>();
		periodiciteValue = periodiciteValue != null ? periodiciteValue.split("_")[1].substring(0, 1) : null;
		if ("visit_type_vi".equals(typeVisiteValue)) {
			listPredicates.add(cb.or(cb.equal(surveillance.get("typeSurveillance"), ConvertData.VISIT_TYPE_ID),cb.equal(surveillance.get("typeSurveillance"), ConvertData.VISIT_TYPE_VD)));
			
			if (periodiciteValue != null) {
				listPredicates.add(cb.equal(surveillance.get("periodiciteVi"), periodiciteValue));
			}
			
			if (responsableSurveillanceValue != null) {
				listPredicates.add(cb.equal(surveillance.get("auteurVi"), responsableSurveillanceValue));
			}
		} else if ("visit_type_vi_ot".equals(typeVisiteValue)) {
			listPredicates.add(cb.equal(surveillance.get("typeSurveillance"), ConvertData.VISIT_TYPE_VD_OT));
			
			if (periodiciteValue != null) {
				listPredicates.add(this.valueEquals(surveillance.get("periodiciteViOt"), Integer.class, periodiciteValue));
			}
			
			if (responsableSurveillanceValue != null) {
				listPredicates.add(cb.equal(surveillance.get("auteurViOt"), responsableSurveillanceValue));
			}
		} else if ("visit_type_vp_ot".equals(typeVisiteValue)) {
			listPredicates.add(cb.equal(surveillance.get("typeSurveillance"), ConvertData.VISIT_TYPE_VP_OT));
			
			if (periodiciteValue != null) {
				listPredicates.add(this.valueEquals(surveillance.get("periodiciteVpOt"), Integer.class, periodiciteValue));
			}
			
			if (responsableSurveillanceValue != null) {
				listPredicates.add(cb.equal(surveillance.get("auteurVpOt"), responsableSurveillanceValue));
			}
		} else if ("visit_type_id".equals(typeVisiteValue)) {
			listPredicates.add(cb.equal(surveillance.get("typeSurveillance"), ConvertData.VISIT_TYPE_ID));
			
			if (periodiciteValue != null) {
				
				listPredicates.add(this.valueEquals(surveillance.get("periodiciteId"), Integer.class, periodiciteValue));
			}
			
			if (responsableSurveillanceValue != null) {
				listPredicates.add(cb.equal(surveillance.get("auteurId"), responsableSurveillanceValue));
			}
		} else if ("visit_type_vd".equals(typeVisiteValue)) {
			listPredicates.add(cb.equal(surveillance.get("typeSurveillance"), ConvertData.VISIT_TYPE_VD));
			
			if (periodiciteValue != null) {
				listPredicates.add(this.valueEquals(surveillance.get("periodiciteVd"), Integer.class, periodiciteValue));
			}
			
			if (responsableSurveillanceValue != null) {
				listPredicates.add(cb.equal(surveillance.get("auteurVd"), responsableSurveillanceValue));
			}
		} else if ("visit_type_vd_ot".equals(typeVisiteValue)) {
			listPredicates.add(cb.equal(surveillance.get("typeSurveillance"), ConvertData.VISIT_TYPE_VD_OT));
			
			if (periodiciteValue != null) {
				listPredicates.add(this.valueEquals(surveillance.get("periodiciteVdOt"), Integer.class, periodiciteValue));
			}
			
			if (responsableSurveillanceValue != null) {
				listPredicates.add(cb.equal(surveillance.get("auteurVdOt"), responsableSurveillanceValue));
			}
		} else if ("visit_type_vs".equals(typeVisiteValue)) {
			listPredicates.add(cb.equal(surveillance.get("typeSurveillance"), ConvertData.VISIT_TYPE_VS));
			
			if (periodiciteValue != null) {
				listPredicates.add(this.valueEquals(surveillance.get("periodiciteVs"), Integer.class, periodiciteValue));
			}
			
			if (responsableSurveillanceValue != null) {
				listPredicates.add(cb.equal(surveillance.get("auteurVs"), responsableSurveillanceValue));
			}
		} else if ("visit_type_vsp".equals(typeVisiteValue)) {
			listPredicates.add(cb.equal(surveillance.get("typeSurveillance"), ConvertData.VISIT_TYPE_VSP));
			
			if (periodiciteValue != null) {
				listPredicates.add(this.valueEquals(surveillance.get("periodiciteVsp"), Integer.class, periodiciteValue));
			}
			
			if (responsableSurveillanceValue != null) {
				listPredicates.add(cb.equal(surveillance.get("auteurVsp"), responsableSurveillanceValue));
			}
		} else if ("visit_type_idi".equals(typeVisiteValue)) {
			listPredicates.add(cb.and(cb.isNotNull(surveillance.get("periodiciteIdi")),cb.isTrue(surveillance.get("idi"))));
			
			if (periodiciteValue != null) {
				listPredicates.add(this.valueEquals(surveillance.get("periodiciteIdi"), Integer.class, periodiciteValue));
			}
			
			if (responsableSurveillanceValue != null) {
				listPredicates.add(cb.equal(surveillance.get("auteurIdi"), responsableSurveillanceValue));
			}
		} else if ("visit_type_va_ot".equals(typeVisiteValue)) {
			listPredicates.add(cb.isTrue(surveillance.get("boolOtVa")));
			
			if (periodiciteValue != null) {
				listPredicates.add(this.valueEquals(surveillance.get("periodiciteVaOt"), Integer.class, periodiciteValue));
			}
			
			if (responsableSurveillanceValue != null) {
				listPredicates.add(cb.equal(surveillance.get("auteurVaOt"), responsableSurveillanceValue));
			}
		} else if ("visit_type_sr".equals(typeVisiteValue)) {
			listPredicates.add(cb.equal(surveillance.get("complementaryVisit"), ConvertData.VISIT_TYPE_SR));
			
			/* TODO: A l'ajout du model SurveillanceComplementaire
			 if (periodiciteValue != null) {
				listPredicates.add(cb.or(cb.equal(surveillanceComplementaire.get("periodiciteSr"), periodiciteValue),
						cb.equal(surveillanceComplementaire.get("periodiciteSr2"), periodiciteValue),
						cb.equal(surveillanceComplementaire.get("periodiciteSr3"), periodiciteValue)));
			}
			
			if (responsableSurveillanceValue != null) {
				listPredicates.add(cb.or(cb.equal(surveillanceComplementaire.get("auteurSr"), responsableSurveillanceValue),
						cb.equal(surveillanceComplementaire.get("auteurSr2"), responsableSurveillanceValue),
						cb.equal(surveillanceComplementaire.get("auteurSr3"), responsableSurveillanceValue)));
			}*/
		} else if ("visit_type_sp".equals(typeVisiteValue)) {
			listPredicates.add(cb.equal(surveillance.get("complementaryVisit"), ConvertData.VISIT_TYPE_SP));
			
			/* TODO: A l'ajout du model SurveillanceComplementaire
			 if (periodiciteValue != null) {
				listPredicates.add(cb.or(cb.equal(surveillanceComplementaire.get("periodiciteSp"), periodiciteValue),
						cb.equal(surveillanceComplementaire.get("periodiciteSp2"), periodiciteValue),
						cb.equal(surveillanceComplementaire.get("periodiciteSp3"), periodiciteValue)));
			}
			
			if (responsableSurveillanceValue != null) {
				listPredicates.add(cb.or(cb.equal(surveillanceComplementaire.get("auteurSp"), responsableSurveillanceValue),
						cb.equal(surveillanceComplementaire.get("auteurSp2"), responsableSurveillanceValue),
						cb.equal(surveillanceComplementaire.get("auteurSp3"), responsableSurveillanceValue)));
			}*/
		} else if (ConvertData.VISIT_TYPE_SURV_TIERS.equals(typeVisiteValue) || ConvertData.VISIT_TYPE_SURV_CURRENT.equals(typeVisiteValue)) {
			listPredicates.add(cb.equal(surveillance.get("typeSurveillance"), typeVisiteValue));

		}
		
		return listPredicates;
	}

	public Double convertStringToDouble(String value) {
		return value != null ? Double.parseDouble(value.replace(",", ".")) : null;
	}

	public Integer convertStringToInteger(String value) {
		return value != null ? Integer.parseInt(value) : null;
	}

	public Boolean convertStringToBoolean(String value) {
		return value != null ? Boolean.valueOf(value) : null;
	}
	
	public String valueFromJsonString(String jsonString) {
		JSONObject value = new JSONObject(jsonString);
		return value.getString("raw");
	}

}
