package com.boate.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.boate.enums.TypeVisiteEnum;

public class ConvertData {

	private static Map<String, ConvertDataValue> categoryMap = new HashMap<>();
	private static Map<String, String> typeOuvrageMap = new HashMap<>();
	private static Map<String, String> natureOuvrageMap = new HashMap<>();
	private static Map<String, String> geometrieOuvrageMap = new HashMap<>();
	private static Map<String, String> situationOuvrageMap = new HashMap<>();
	private static Map<String, String> uicMap = new HashMap<>();
	private static Map<String, String> periodiciteSurveillanceMap = new HashMap<>();
	private static Map<String, String> ligneVitesseMap = new HashMap<>();
	private static Map<String, String> fsaMap = new HashMap<>();
	private static Map<String, String> moyensSurveillanceMap = new HashMap<>();
	private static Map<String, String> auteurSurveillanceMap = new HashMap<>();
	private static Map<String, String> fonctionUtilisateurMap = new HashMap<>();
	private static Map<String, String> entiteMap = new HashMap<>();
	private static Map<TypeVisiteEnum, ConvertDataValue> typeVisiteMap = new HashMap<>();

	public static final String WORK_CATEGORY_BAT = "category_Bat";
	public static final String WORK_CATEGORY_OA = "category_OA";
	public static final String WORK_CATEGORY_OT = "category_OT";
	public static final String WORK_CATEGORY_OTHERS = "category_Autre";
	public static final String WORK_CATEGORY_HYDRO = "category_Hydro";
	
	public static String VISIT_TYPE_ID ="ID";
	public static String VISIT_TYPE_IDI ="IDI";
	public static String VISIT_TYPE_VD ="VD";
	public static String VISIT_TYPE_VI ="VI";
	public static String VISIT_TYPE_VI_OT ="VI_OT";
	public static String VISIT_TYPE_VP_OT ="VP_OT";
	public static String VISIT_TYPE_VA_OT ="VA_OT";
	public static String VISIT_TYPE_SP ="SP";
	public static String VISIT_TYPE_SR ="SR";
	public static String VISIT_TYPE_VS ="VS";
	public static String VISIT_TYPE_VSP ="VSP";
	public static String VISIT_TYPE_VD_OT ="VD_OT";
	public static String VISIT_TYPE_SURV_TIERS ="surv_tiers";
    public static String VISIT_TYPE_SURV_CURRENT ="surv_current";

	public ConvertData() {
		initCategorieOuvrage();
		initTypeOuvrage();
		initNatureOuvrage();
		initGeometrieOuvrage();
		initSituationOuvrage();
		initUic();
		initPeriodiciteSurveillance();
		initLigneVitesse();
		initFsa();
		initMoyensSurveillance();
		initAuteurSurveillance();
		initFonctionUtilisateur();
		initEntite();
		initTypeVisite();
	}

	public void initCategorieOuvrage() {
		categoryMap.put(WORK_CATEGORY_OTHERS, new ConvertDataValue("Autres", "Autres"));
		categoryMap.put(WORK_CATEGORY_BAT, new ConvertDataValue("BNC", "Bâtiment Non Courant"));
		categoryMap.put(WORK_CATEGORY_OA, new ConvertDataValue("OA", "Ouvrage d'Art"));
		categoryMap.put(WORK_CATEGORY_OT, new ConvertDataValue("OT", "Ouvrage en Terre"));
		categoryMap.put(WORK_CATEGORY_HYDRO, new ConvertDataValue("Hydro", "Ouvrage Hydraulique"));
	}
	
	public void initTypeVisite() {
		typeVisiteMap.put(TypeVisiteEnum.visit_type_id, new ConvertDataValue("ID", "Inspection Détaillée OA"));
		typeVisiteMap.put(TypeVisiteEnum.visit_type_idi, new ConvertDataValue("IDI", "ID intermédiaire OA"));
		typeVisiteMap.put(TypeVisiteEnum.visit_type_cl_ot, new ConvertDataValue("CL OT", "Visite de classement OT"));
		typeVisiteMap.put(TypeVisiteEnum.visit_type_vd, new ConvertDataValue("VD", "Visite détaillée OA"));
		typeVisiteMap.put(TypeVisiteEnum.visit_type_vd_ot, new ConvertDataValue("VD OT", "Visite détaillée OT"));
		typeVisiteMap.put(TypeVisiteEnum.visit_type_vi, new ConvertDataValue("VI", "Visite intermédiaire OA"));
		typeVisiteMap.put(TypeVisiteEnum.visit_type_vi_ot, new ConvertDataValue("VI OT", "Visite intermédiaire OT"));
		typeVisiteMap.put(TypeVisiteEnum.visit_type_vp_ot, new ConvertDataValue("VP OT", "Visite particulière OT"));
		typeVisiteMap.put(TypeVisiteEnum.visit_type_va_ot, new ConvertDataValue("EFPB", "Visite annuelle EFPB OT"));
		typeVisiteMap.put(TypeVisiteEnum.visit_type_vs, new ConvertDataValue("VS", "Visite simplifiée"));
		typeVisiteMap.put(TypeVisiteEnum.visit_type_pv0, new ConvertDataValue("PV0", "Visite de référence (PV0)"));
		typeVisiteMap.put(TypeVisiteEnum.visit_type_id_cv, new ConvertDataValue("CVID", "Complément de visite d'inspection détaillée"));
		typeVisiteMap.put(TypeVisiteEnum.visit_type_vd_cv, new ConvertDataValue("CVVD", "Complément de visite de la visite détaillée"));
	}
	
	public void initFsa() {
		fsaMap.put("fsa_oui","Oui");
		fsaMap.put("fsa_none","Non");
		fsaMap.put("fsa_special_exposed","Particulièrement exposé");
	}
	
	public void initPeriodiciteSurveillance() {
		periodiciteSurveillanceMap.put("period_1d","1 jour");
		periodiciteSurveillanceMap.put("period_2d","2 jours");
		periodiciteSurveillanceMap.put("period_3d","3 jours");
		periodiciteSurveillanceMap.put("period_4d","4 jours");
		periodiciteSurveillanceMap.put("period_5d","5 jours");
		periodiciteSurveillanceMap.put("period_1w","1 semaine");
		periodiciteSurveillanceMap.put("period_2w","2 semaines");
		periodiciteSurveillanceMap.put("period_3w","3 semaines");
		periodiciteSurveillanceMap.put("period_1m","1 mois");
		periodiciteSurveillanceMap.put("period_2m","2 mois");
		periodiciteSurveillanceMap.put("period_3m","3 mois");
		periodiciteSurveillanceMap.put("period_4m","4 mois");
		periodiciteSurveillanceMap.put("period_6m","6 mois");
		periodiciteSurveillanceMap.put("period_1y","1 an");
		periodiciteSurveillanceMap.put("period_2y","2 ans");
		periodiciteSurveillanceMap.put("period_3y","3 ans");
		periodiciteSurveillanceMap.put("period_6y","6 ans");
	}
	
	public void initSituationOuvrage() {
		situationOuvrageMap.put("situation_up_left",">G");
		situationOuvrageMap.put("situation_up",">");
		situationOuvrageMap.put("situation_up_right",">D");
		situationOuvrageMap.put("situation_right","D");
		situationOuvrageMap.put("situation_down_right","<D");
		situationOuvrageMap.put("situation_down","<");
		situationOuvrageMap.put("situation_down_left","<G");
		situationOuvrageMap.put("situation_left","G");
		situationOuvrageMap.put("situation_left_right","G/D");
		situationOuvrageMap.put("situation_right_left","D/G");
	}
	
	public void initUic() {
		uicMap.put("uic_3","3");
		uicMap.put("uic_4","4");
		uicMap.put("uic_5","5");
		uicMap.put("uic_6","6");
		uicMap.put("uic_6AV","6AV");
		uicMap.put("uic_7AV","7AV");
		uicMap.put("uic_7SV","7SV");
		uicMap.put("uic_8AV","8AV");
		uicMap.put("uic_8SV","8SV");
		uicMap.put("uic_9AV","9AV");
		uicMap.put("uic_9SV","9SV");
		uicMap.put("uic_emb","Emb");
		uicMap.put("uic_F","F");
		uicMap.put("uic_neut","Neut");
		uicMap.put("uic_none","Sans");
	}

	public void initTypeOuvrage() {
		typeOuvrageMap.put("work_type_aqc", "Aqueduc");
		typeOuvrageMap.put("work_type_bnc", "Bâtiment Non Courant");
		typeOuvrageMap.put("work_type_bse", "Buse");
		typeOuvrageMap.put("work_type_dcp", "Dispositif Confortatif Parois");
		typeOuvrageMap.put("work_type_deblai", "Déblai meuble");
		typeOuvrageMap.put("work_type_digoa", "Digue OA");
		typeOuvrageMap.put("work_type_digot", "Digue OT");
		typeOuvrageMap.put("work_type_dlt", "Dalot");
		typeOuvrageMap.put("work_type_descente", "Descente");
		typeOuvrageMap.put("work_drainage", "Drainage");
		typeOuvrageMap.put("work_type_eab", "Ecran anti bruit");
		typeOuvrageMap.put("work_type_efpb", "Ecran de filet pare-blocs");
		typeOuvrageMap.put("work_type_flood", "Flood");
		typeOuvrageMap.put("work_type_gal", "Galerie");
		typeOuvrageMap.put("work_type_gt", "Galerie technique");
		typeOuvrageMap.put("work_type_mgh", "Mat de grande hauteur");
		typeOuvrageMap.put("work_type_mxt", "Profil mixte");
		typeOuvrageMap.put("work_type_ms", "Mur de soutènement");
		typeOuvrageMap.put("work_type_oce", "Ouvrage de couverture");
		typeOuvrageMap.put("work_type_ota", "Ota");
		typeOuvrageMap.put("work_type_pas", "Passerelle");
		typeOuvrageMap.put("work_type_pc", "Paroi clouée");
		typeOuvrageMap.put("work_type_pcl", "Pont canal");
		typeOuvrageMap.put("work_type_per", "Perré");
		typeOuvrageMap.put("work_type_portique", "Portique");
		typeOuvrageMap.put("work_type_potence", "Potence");
		typeOuvrageMap.put("work_type_pr", "Paroi rocheuse");
		typeOuvrageMap.put("work_type_pra", "Pont rail");
		typeOuvrageMap.put("work_type_pro", "Pont route");
		typeOuvrageMap.put("work_type_pyl", "Pylône");
		typeOuvrageMap.put("work_type_remblai", "Remblai");
		typeOuvrageMap.put("work_type_sdm", "Saut de mouton");
		typeOuvrageMap.put("work_type_tc", "Tranchée couverte");
		typeOuvrageMap.put("work_type_tranchee", "Talus rocheux");
		typeOuvrageMap.put("work_type_traversee", "Traversée");
		typeOuvrageMap.put("work_type_trv", "Tranchée rocheuse revêtue");
		typeOuvrageMap.put("work_type_tun", "Tunnel");
		typeOuvrageMap.put("work_type_versant", "Versant");
		typeOuvrageMap.put("work_type_via", "Viaduc");
		typeOuvrageMap.put("work_type_td", "Tranchée drainante");
		typeOuvrageMap.put("work_type_divers", "Divers");
		typeOuvrageMap.put("work_type_profil_rasant", "Profil rasant");
		typeOuvrageMap.put("work_type_tete_tunnel", "Tête tunnel");
		typeOuvrageMap.put("work_type_autre", "Autres");
		typeOuvrageMap.put("work_type_depot", "Dépôt");
		typeOuvrageMap.put("work_type_bg", "Barrière grillagée");
		typeOuvrageMap.put("work_type_hydro", "Hydraulique");
		typeOuvrageMap.put("work_type_patrimoine_debmeu", "Déblai meuble");
		typeOuvrageMap.put("work_type_patrimoine_remblai", "Remblai");
		typeOuvrageMap.put("work_type_patrimoine_debroch", "Déblai rocheux");
		typeOuvrageMap.put("work_type_gp", "Galerie de protection");
		typeOuvrageMap.put("work_type_mgr", "Mur en gradin");
		typeOuvrageMap.put("work_type_prev", "Paroi revêtue");
		typeOuvrageMap.put("work_type_trm", "Trémie");
		typeOuvrageMap.put("work_type_aqc_buse", "Aqueduc busé");
	}

	public void initNatureOuvrage() {
		natureOuvrageMap.put("material_Al", "Aluminium");
		natureOuvrageMap.put("material_Ba", "Béton armé");
		natureOuvrageMap.put("material_Beton", "Béton");
		natureOuvrageMap.put("material_Bp", "Béton précontraint");
		natureOuvrageMap.put("material_Bv", "Blocs végétalisables");
		natureOuvrageMap.put("material_Bs", "Bois");
		natureOuvrageMap.put("material_Ci", "Ciment");
		natureOuvrageMap.put("material_Div", "Div");
		natureOuvrageMap.put("material_Fibro", "Fibro");
		natureOuvrageMap.put("material_Mie", "Maçonnerie");
		natureOuvrageMap.put("material_Mtl", "Métallique");
		natureOuvrageMap.put("material_Mix", "Mixte");
		natureOuvrageMap.put("material_Pise", "Pisé");
		natureOuvrageMap.put("material_Ps", "Pierres sèches");
		natureOuvrageMap.put("material_Tpe", "Poutrelles enrobées");
		natureOuvrageMap.put("material_Pcv", "PVC");
		natureOuvrageMap.put("material_Re", "Rails enrobés");
		natureOuvrageMap.put("material_Acier", "Acier");
		natureOuvrageMap.put("material_Fp", "Fer puddlé");
		natureOuvrageMap.put("material_Fonte", "Fonte");
		natureOuvrageMap.put("material_Autres", "Autres");
		natureOuvrageMap.put("material_Gb", "Gabion");
		natureOuvrageMap.put("material_Bpro", "Béton projeté");
	}
	
	public void initGeometrieOuvrage() {
		geometrieOuvrageMap.put("geometry_cdr","Cadre");
		geometrieOuvrageMap.put("geometry_Csn","Caisson");
		geometrieOuvrageMap.put("geometry_ov","Ovoïde");
		geometrieOuvrageMap.put("geometry_circ","Circ");
		geometrieOuvrageMap.put("geometry_pc","Plein cintre");
		geometrieOuvrageMap.put("geometry_prt","Portique");
		geometrieOuvrageMap.put("geometry_psd","Poutre sous dalle");
		geometrieOuvrageMap.put("geometry_srb","Surbaissé");
		geometrieOuvrageMap.put("geometry_tripode","Tripode");
		geometrieOuvrageMap.put("geometry_tpcv","Tpcv");
		geometrieOuvrageMap.put("geometry_tpci","Tpci");
		geometrieOuvrageMap.put("geometry_tpcc","Tpcc");
		geometrieOuvrageMap.put("geometry_tfc","Tfc");
		geometrieOuvrageMap.put("geometry_tpcl","Tpcl");
		geometrieOuvrageMap.put("geometry_dba","Dalle BA");
		geometrieOuvrageMap.put("geometry_cyl","Cylindrique");
		geometrieOuvrageMap.put("geometry_qua","Quadripode");
		geometrieOuvrageMap.put("geometry_rapum","RAPUM");
	}
	
	public void initLigneVitesse() {
		ligneVitesseMap.put("speed_20","20");
		ligneVitesseMap.put("speed_30","30");
		ligneVitesseMap.put("speed_40","40");
		ligneVitesseMap.put("speed_50","50");
		ligneVitesseMap.put("speed_55","55");
		ligneVitesseMap.put("speed_60","60");
		ligneVitesseMap.put("speed_70","70");
		ligneVitesseMap.put("speed_80","80");
		ligneVitesseMap.put("speed_85","85");
		ligneVitesseMap.put("speed_90","90");
		ligneVitesseMap.put("speed_95","95");
		ligneVitesseMap.put("speed_100","100");
		ligneVitesseMap.put("speed_105","105");
		ligneVitesseMap.put("speed_110","110");
		ligneVitesseMap.put("speed_115","115");
		ligneVitesseMap.put("speed_120","120");
		ligneVitesseMap.put("speed_125","125");
		ligneVitesseMap.put("speed_130","130");
		ligneVitesseMap.put("speed_140","140");
		ligneVitesseMap.put("speed_150","150");
		ligneVitesseMap.put("speed_160","160");
		ligneVitesseMap.put("speed_170","170");
		ligneVitesseMap.put("speed_200","200");
		ligneVitesseMap.put("speed_220","220");
		ligneVitesseMap.put("speed_230","230");
		ligneVitesseMap.put("speed_270","270");
		ligneVitesseMap.put("speed_300","300");
		ligneVitesseMap.put("speed_320","320");
		ligneVitesseMap.put("speed_350","350");
	}
	
	public void initMoyensSurveillance() {
		//TODO:
		moyensSurveillanceMap.put("means_WIT","WIT");
	}
	
	public void initFonctionUtilisateur() {
		fonctionUtilisateurMap.put("FUNCTION_INVITE","Invité");
		fonctionUtilisateurMap.put("FUNCTION_ADMIN","Administrateur");
		fonctionUtilisateurMap.put("FUNCTION_ADMIN_ARCHIVE","Administrateur Archive");
		fonctionUtilisateurMap.put("FUNCTION_AH","AH");
		fonctionUtilisateurMap.put("FUNCTION_AOAP","AOAP");
		fonctionUtilisateurMap.put("FUNCTION_AOAU","AOAU");
		fonctionUtilisateurMap.put("FUNCTION_CU_DUO","DUO");
		fonctionUtilisateurMap.put("FUNCTION_DDI","DDI");
		fonctionUtilisateurMap.put("FUNCTION_DET","DET");
		fonctionUtilisateurMap.put("FUNCTION_DRONE_PILOTE","Pilote Drone");
		fonctionUtilisateurMap.put("FUNCTION_RI_OA_OT","RI OA/OT");
		fonctionUtilisateurMap.put("FUNCTION_RM_OA_OT","RM OA/OT");
		fonctionUtilisateurMap.put("FUNCTION_RRMI","RRMI");
		fonctionUtilisateurMap.put("FUNCTION_RMPL","RMPL");
		fonctionUtilisateurMap.put("FUNCTION_ROTP","ROTP");
		fonctionUtilisateurMap.put("FUNCTION_R_PRILY_Y","R PRILY Y");
		fonctionUtilisateurMap.put("FUNCTION_R_PRILY","R PRILY");
		fonctionUtilisateurMap.put("FUNCTION_SOAR","SOAR");
		fonctionUtilisateurMap.put("FUNCTION_ABE","ABE");
		fonctionUtilisateurMap.put("FUNCTION_TIERS","Tiers");
		fonctionUtilisateurMap.put("FUNCTION_COT","COT");
		fonctionUtilisateurMap.put("FUNCTION_ADJRI_OA_OT","ADJRI OA/OT");
		fonctionUtilisateurMap.put("FUNCTION_ARCHIVE","Archive");
		fonctionUtilisateurMap.put("FUNCTION_TOPO","Topographe");
		fonctionUtilisateurMap.put("FUNCTION_HYDRO","Hydro");
		fonctionUtilisateurMap.put("FUNCTION_CHEF_EG","Chef EG");
		fonctionUtilisateurMap.put("FUNCTION_OTHER","Autres");
	}
	
	public void initEntite() {
		entiteMap.put("ENTITY_DRIMOA_OT","DRIMOA/OT");
		entiteMap.put("ENTITY_EVEN","INFRAPOLE");
		entiteMap.put("ENTITY_IGLG","IGLG");
		entiteMap.put("ENTITY_IGOA","IGOA");
		entiteMap.put("ENTITY_IMT","IMT");
		entiteMap.put("ENTITY_OTP","OTP");
		entiteMap.put("ENTITY_PRILY","PRILY");
		entiteMap.put("ENTITY_PRICHY","PRICHY");
	}
	
	public void initAuteurSurveillance() {
		auteurSurveillanceMap.put("sur_head_abe","ABE");
		auteurSurveillanceMap.put("sur_head_ah","AH");
		auteurSurveillanceMap.put("sur_head_aoap","AOAP");
		auteurSurveillanceMap.put("sur_head_aoau","AOAU");
		auteurSurveillanceMap.put("sur_head_cot","COT");
		auteurSurveillanceMap.put("sur_head_soar","SOAR");
		auteurSurveillanceMap.put("sur_head_tiers","Tiers");
		auteurSurveillanceMap.put("sur_head_topo","Topographe");
	}

	public static Map<String, ConvertDataValue> getCategoryMap() {
		return categoryMap;
	}

	public static void setCategoryMap(Map<String, ConvertDataValue> categoryMap) {
		ConvertData.categoryMap = categoryMap;
	}

	public <K, V> K getKeyFromMap(Map<K, V> map, V value) {
		for (Entry<K, V> entry : map.entrySet()) {
			if (entry.getValue().equals(value)) {
				return entry.getKey();
			}
		}
		return null;
	}

	public static Map<String, String> getTypeOuvrageMap() {
		return typeOuvrageMap;
	}

	public static void setTypeOuvrageMap(Map<String, String> typeOuvrageMap) {
		ConvertData.typeOuvrageMap = typeOuvrageMap;
	}

	public static Map<String, String> getNatureOuvrageMap() {
		return natureOuvrageMap;

	}

	public static void setNatureOuvrageMap(Map<String, String> natureOuvrageMap) {
		ConvertData.natureOuvrageMap = natureOuvrageMap;
	}

	public static Map<String, String> getGeometrieOuvrageMap() {
		return geometrieOuvrageMap;
	}

	public static void setGeometrieOuvrageMap(Map<String, String> geometrieOuvrageMap) {
		ConvertData.geometrieOuvrageMap = geometrieOuvrageMap;
	}

	public static Map<String, String> getSituationOuvrageMap() {
		return situationOuvrageMap;
	}

	public static void setSituationOuvrageMap(Map<String, String> situationOuvrageMap) {
		ConvertData.situationOuvrageMap = situationOuvrageMap;
	}

	public static Map<String, String> getUicMap() {
		return uicMap;
	}

	public static void setUicMap(Map<String, String> uicMap) {
		ConvertData.uicMap = uicMap;
	}

	public static Map<String, String> getPeriodiciteSurveillanceMap() {
		return periodiciteSurveillanceMap;
	}

	public static void setPeriodiciteSurveillanceMap(Map<String, String> periodiciteSurveillanceMap) {
		ConvertData.periodiciteSurveillanceMap = periodiciteSurveillanceMap;
	}

	public static Map<String, String> getLigneVitesseMap() {
		return ligneVitesseMap;
	}

	public static void setLigneVitesseMap(Map<String, String> ligneVitesseMap) {
		ConvertData.ligneVitesseMap = ligneVitesseMap;
	}

	public static Map<String, String> getFsaMap() {
		return fsaMap;
	}

	public static void setFsaMap(Map<String, String> fsaMap) {
		ConvertData.fsaMap = fsaMap;
	}

	public static Map<String, String> getMoyensSurveillanceMap() {
		return moyensSurveillanceMap;
	}

	public static void setMoyensSurveillanceMap(Map<String, String> moyensSurveillanceMap) {
		ConvertData.moyensSurveillanceMap = moyensSurveillanceMap;
	}

	public static Map<String, String> getAuteurSurveillanceMap() {
		return auteurSurveillanceMap;
	}

	public static void setAuteurSurveillanceMap(Map<String, String> auteurSurveillanceMap) {
		ConvertData.auteurSurveillanceMap = auteurSurveillanceMap;
	}

	public static Map<String, String> getFonctionUtilisateurMap() {
		return fonctionUtilisateurMap;
	}

	public static void setFonctionUtilisateurMap(Map<String, String> fonctionUtilisateurMap) {
		ConvertData.fonctionUtilisateurMap = fonctionUtilisateurMap;
	}

	public static Map<String, String> getEntiteMap() {
		return entiteMap;
	}

	public static void setEntiteMap(Map<String, String> entiteMap) {
		ConvertData.entiteMap = entiteMap;
	}

	public static Map<TypeVisiteEnum, ConvertDataValue> getTypeVisiteMap() {
		return typeVisiteMap;
	}

	public static void setTypeVisiteMap(Map<TypeVisiteEnum, ConvertDataValue> typeVisiteMap) {
		ConvertData.typeVisiteMap = typeVisiteMap;
	}
	
	public static String convertBooleanToString(Boolean value) {
		if (value != null) {
			switch (value.toString()) {
			case "true":
				return "Oui";
			case "false":
				return "Non";
			default:
				return "Problème";
			}
		}else {
			return null;
		}
	}

}
