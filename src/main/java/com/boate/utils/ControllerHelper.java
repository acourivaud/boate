package com.boate.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

public class ControllerHelper {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(ControllerHelper.class);

	public static ResponseEntity<SuccessResponse> getSuccessResponse() {
		return getSuccessResponse(HttpStatus.OK, "Action réalisée avec succès");
	}

	public static ResponseEntity<SuccessResponse> getSuccessResponse(HttpStatus status) {
		return getSuccessResponse(status, "Action réalisée avec succès");
	}

	public static ResponseEntity<SuccessResponse> getSuccessResponse(String message) {
		return getSuccessResponse(HttpStatus.OK, message);
	}

	public static ResponseEntity<SuccessResponse> getSuccessResponse(HttpStatus status, String message) {
		final SuccessResponse successResponse = new SuccessResponse();
		successResponse.setCode(status.value());
		successResponse.setMessage(message);
		return new ResponseEntity<>(successResponse, status);
	}

	public static ResponseEntity<ErrorResponse> getErrorResponse() {
		return getErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Une erreur est survenue, veuillez réessayer.");
	}

	public static ResponseEntity<ErrorResponse> getErrorResponse(HttpStatus status) {
		return getErrorResponse(status, "Une erreur est survenue, veuillez réessayer.");
	}

	public static ResponseEntity<ErrorResponse> getErrorResponse(String message) {
		return getErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, message);
	}

	public static ResponseEntity<ErrorResponse> getErrorResponse(HttpStatus status, String message) {
		final ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(status.value());
		errorResponse.setMessage(message);
		return new ResponseEntity<>(errorResponse, status);
	}

	public static ResponseEntity<?> getResponseEntity(Object object) {
		return getResponseEntity(object, HttpStatus.OK);
	}

	public static ResponseEntity<?> getResponseEntity(Object object, HttpStatus status) {
		return new ResponseEntity<>(object, status);
	}
	
	public static ResponseEntity<byte[]> getResponseEntity(File file) {
		byte[] contents = null;
		try {
			contents = Files.readAllBytes(file.toPath());
			file.delete();
		} catch (final IOException e) {
			LOGGER.error(e.toString());
		}
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/octet-stream"));
		headers.setContentDispositionFormData(file.getName(), file.getName());
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		return new ResponseEntity<>(contents, headers, HttpStatus.OK);
	}

}
