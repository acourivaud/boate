package com.boate.utils;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class LocalDateConverter implements AttributeConverter<LocalDate, Timestamp> {

	@Override
	public Timestamp convertToDatabaseColumn(final LocalDate entityValue) {
		if (entityValue == null) {
			return null;
		}
		return Timestamp.valueOf(LocalDateTime.of(entityValue, LocalTime.MIN));
	}

	@Override
	public LocalDate convertToEntityAttribute(final Timestamp databaseValue) {
		if (databaseValue == null) {
			return null;
		}
		return databaseValue.toLocalDateTime().toLocalDate();
	}
}