package com.boate.utils;

public class DataFormatUtils {

	public static String checkPk(String priKey) {

		char num[];
		char num2[] = { '0', '0', '0', ',', '0', '0', '0' };
		int position = 0;
		int position1;
		int position2;
		int numlength;
		int leftSize;
		int rightSize;
		boolean splitOr = true;
		num = priKey.toCharArray();
		numlength = num.length;
		String numFormat;
		for (int i = 0; i < numlength; i++) {

			position1 = priKey.indexOf('.');
			position2 = priKey.indexOf(',');
			if (position1 != -1)
				position = position1;
			if (position2 != -1)
				position = position2;
			if (position1 == -1 && position2 == -1)
				splitOr = false;
			if (splitOr == true) {
				rightSize = numlength - position - 1;
				leftSize = position - 0;
				if (leftSize <= 3)
					for (int j = 1; j < leftSize + 1; j++) {
						num2[3 - j] = num[position - j];
					}
				num2[3] = ',';
				if (rightSize <= 3)
					for (int k = 1; k < rightSize + 1; k++) {
						num2[3 + k] = num[position + k];
					}
			} else {
				if (numlength == 1)
					num2[2] = num[0];

				else if (numlength == 2) {
					num2[1] = num[0];
					num2[2] = num[1];
				} else if (numlength == 3) {
					num2[0] = num[0];
					num2[1] = num[1];
					num2[2] = num[2];
				}
			}
		}

		numFormat = new String(num2);

		return (String) numFormat;

	}

}
