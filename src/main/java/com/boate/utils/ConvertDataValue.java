package com.boate.utils;

public class ConvertDataValue {

	private String libelleFull;
	private String libelleSimplifie;

	public ConvertDataValue(String libelleSimplifie, String libelleFull) {
		this.libelleSimplifie = libelleSimplifie;
		this.libelleFull = libelleFull;
	}

	public String getLibelleFull() {
		return libelleFull;
	}

	public void setLibelleFull(String libelleFull) {
		this.libelleFull = libelleFull;
	}

	public String getLibelleSimplifie() {
		return libelleSimplifie;
	}

	public void setLibelleSimplifie(String libelleSimplifie) {
		this.libelleSimplifie = libelleSimplifie;
	}
}
