package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Vehicule;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VehiculeDTO {

	private Integer id;
	private String marque;
	private String modele;
	private String immatriculation;
	private Integer places;

	public VehiculeDTO() {

	}

	private VehiculeDTO(Vehicule vehicule) {
		this.id = vehicule.getId();
		this.marque = vehicule.getMarque();
		this.modele = vehicule.getModele();
		this.immatriculation = vehicule.getImmatriculation();
		this.places = vehicule.getPlaces();
	}

	public static VehiculeDTO toDTO(Vehicule vehicule) {
		if (vehicule == null) {
			return null;
		} else {
			return new VehiculeDTO(vehicule);
		}
	}

	public static List<VehiculeDTO> toDTOs(List<Vehicule> vehicules) {
		if (vehicules == null) {
			return new ArrayList<>(0);
		}
		List<VehiculeDTO> vehiculeDTOs = new ArrayList<>(vehicules.size());
		for (Vehicule vehicule : vehicules) {
			vehiculeDTOs.add(VehiculeDTO.toDTO(vehicule));
		}
		return vehiculeDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public String getImmatriculation() {
		return immatriculation;
	}

	public void setImmatriculation(String immatriculation) {
		this.immatriculation = immatriculation;
	}

	public Integer getPlaces() {
		return places;
	}

	public void setPlaces(Integer places) {
		this.places = places;
	}
}
