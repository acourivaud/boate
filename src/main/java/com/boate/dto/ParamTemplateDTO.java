package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.enums.TemplateStatutEnum;
import com.boate.model.ParamTemplate;

public class ParamTemplateDTO {

	private Integer id;
	private String libelle;
	private String statut;
	private List<ParamOngletDTO> paramOnglets;

	public ParamTemplateDTO() {
		
	}
	
	public ParamTemplateDTO(ParamTemplate paramTemplate) {
		this.id = paramTemplate.getId();
		this.libelle = paramTemplate.getLibelle();
		this.statut = paramTemplate.getStatut() != null ? paramTemplate.getStatut().toString() : TemplateStatutEnum.BROUILLON.toString();
		this.paramOnglets = ParamOngletDTO.toDTOs(paramTemplate.getParamOnglets());
	}

	public static ParamTemplateDTO toDTO(ParamTemplate paramTemplate) {
		if (paramTemplate == null) {
			return null;
		} else {
			return new ParamTemplateDTO(paramTemplate);
		}
	}

	public static List<ParamTemplateDTO> toDTOs(List<ParamTemplate> paramTemplates) {
		if (paramTemplates == null) {
			return new ArrayList<>(0);
		}
		List<ParamTemplateDTO> paramTemplateDTOs = new ArrayList<>(paramTemplates.size());
		for (ParamTemplate paramTemplate : paramTemplates) {
			paramTemplateDTOs.add(ParamTemplateDTO.toDTO(paramTemplate));
		}
		return paramTemplateDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public List<ParamOngletDTO> getParamOnglets() {
		return paramOnglets;
	}

	public void setParamOnglets(List<ParamOngletDTO> paramOnglets) {
		this.paramOnglets = paramOnglets;
	}

}
