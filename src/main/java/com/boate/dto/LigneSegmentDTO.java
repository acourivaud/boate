package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.LigneSegment;

public class LigneSegmentDTO {

	private Integer id;
	private String codeSegment;
	private String nomSegment;
	private String pkDebut;
	private String pkFin;
	private String longueur;
	private LigneDTO ligne;

	public LigneSegmentDTO() {
		
	}
	
	public LigneSegmentDTO(LigneSegment ligneSegment) {
		this.id = ligneSegment.getId();
		this.codeSegment = ligneSegment.getCodeSegment();
		this.nomSegment = ligneSegment.getNomSegment();
		this.pkDebut = ligneSegment.getPkDebut();
		this.pkFin = ligneSegment.getPkFin();
		this.longueur = ligneSegment.getLongueur();
		this.ligne = LigneDTO.toDTO(ligneSegment.getLigne(), true);
	}

	public static LigneSegmentDTO toDTO(LigneSegment ligneSegment) {
		if (ligneSegment == null) {
			return null;
		} else {
			return new LigneSegmentDTO(ligneSegment);
		}
	}

	public static List<LigneSegmentDTO> toDTOs(List<LigneSegment> ligneSegments) {
		if (ligneSegments == null) {
			return new ArrayList<>(0);
		}
		List<LigneSegmentDTO> ligneSegmentDTOs = new ArrayList<>(ligneSegments.size());
		for (LigneSegment ligneSegment : ligneSegments) {
			ligneSegmentDTOs.add(LigneSegmentDTO.toDTO(ligneSegment));
		}
		return ligneSegmentDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodeSegment() {
		return codeSegment;
	}

	public void setCodeSegment(String codeSegment) {
		this.codeSegment = codeSegment;
	}

	public String getNomSegment() {
		return nomSegment;
	}

	public void setNomSegment(String nomSegment) {
		this.nomSegment = nomSegment;
	}

	public String getPkDebut() {
		return pkDebut;
	}

	public void setPkDebut(String pkDebut) {
		this.pkDebut = pkDebut;
	}

	public String getPkFin() {
		return pkFin;
	}

	public void setPkFin(String pkFin) {
		this.pkFin = pkFin;
	}

	public String getLongueur() {
		return longueur;
	}

	public void setLongueur(String longueur) {
		this.longueur = longueur;
	}

	public LigneDTO getLigne() {
		return ligne;
	}

	public void setLigne(LigneDTO ligne) {
		this.ligne = ligne;
	}

}
