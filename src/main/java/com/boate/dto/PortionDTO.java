package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.UniteOperationnelleLigne;

public class PortionDTO {

	private Integer id;
	private LigneDTO ligne;
	private UniteOperationnelleDTO uo;
	private String pkDebut;
	private String pkFin;
	private String voie;

	public PortionDTO() {

	}

	public static PortionDTO toDTO(UniteOperationnelleLigne uol) {
		if (uol == null) {
			return null;
		} else {
			final PortionDTO portionDTO = new PortionDTO();
			portionDTO.setId(uol.getId());
			portionDTO.setLigne(LigneDTO.toDTO(uol.getLigne(), false));
			portionDTO.setUo(UniteOperationnelleDTO.toDTO(uol.getUniteOperationnelle(), false, false, false));
			portionDTO.setPkDebut(uol.getPkDebut());
			portionDTO.setPkFin(uol.getPkFin());
			portionDTO.setVoie(uol.getVoie());
			return portionDTO;
		}
	}

	public static List<PortionDTO> toDTOs(List<UniteOperationnelleLigne> uols) {
		if (uols == null) {
			return new ArrayList<>(0);
		}
		List<PortionDTO> portionDTOs = new ArrayList<>(uols.size());
		for (UniteOperationnelleLigne uol : uols) {
			portionDTOs.add(PortionDTO.toDTO(uol));
		}
		return portionDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPkDebut() {
		return pkDebut;
	}

	public void setPkDebut(String pkDebut) {
		this.pkDebut = pkDebut;
	}

	public String getPkFin() {
		return pkFin;
	}

	public void setPkFin(String pkFin) {
		this.pkFin = pkFin;
	}

	public LigneDTO getLigne() {
		return ligne;
	}

	public void setLigne(LigneDTO ligne) {
		this.ligne = ligne;
	}

	public String getVoie() {
		return voie;
	}

	public void setVoie(String voie) {
		this.voie = voie;
	}

	public UniteOperationnelleDTO getUo() {
		return uo;
	}

	public void setUo(UniteOperationnelleDTO uo) {
		this.uo = uo;
	}

}
