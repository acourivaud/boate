package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Departement;

public class DepartementDTO {

	private Integer id;
	private String nom;

	public DepartementDTO() {

	}

	public static DepartementDTO toDTO(Departement departement) {
		if (departement == null) {
			return null;
		} else {
			final DepartementDTO departementDTO = new DepartementDTO();
			departementDTO.setId(departement.getId());
			departementDTO.setNom(departement.getLibelle());
			return departementDTO;
		}
	}

	public static List<DepartementDTO> toDTOs(List<Departement> departements) {
		if (departements == null) {
			return new ArrayList<>(0);
		}
		List<DepartementDTO> departementDTOs = new ArrayList<>(departements.size());
		for (Departement departement : departements) {
			departementDTOs.add(DepartementDTO.toDTO(departement));
		}
		return departementDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
