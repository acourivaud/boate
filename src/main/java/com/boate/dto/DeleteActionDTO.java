package com.boate.dto;

import java.util.List;

public class DeleteActionDTO {
	
	private Integer idToDelete;
	private List<ParamValueDTO> filtersForReturn;

	public DeleteActionDTO() {
		
	}

	public Integer getIdToDelete() {
		return idToDelete;
	}

	public void setIdToDelete(Integer idToDelete) {
		this.idToDelete = idToDelete;
	}

	public List<ParamValueDTO> getFiltersForReturn() {
		return filtersForReturn;
	}

	public void setFiltersForReturn(List<ParamValueDTO> filtersForReturn) {
		this.filtersForReturn = filtersForReturn;
	}
}
