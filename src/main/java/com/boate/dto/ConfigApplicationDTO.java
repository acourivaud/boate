package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.ConfigApplication;

public class ConfigApplicationDTO {

	private Integer id;
	private String paramName;
	private String value;

	public ConfigApplicationDTO() {
		
	}
	
	public ConfigApplicationDTO(ConfigApplication configApplication) {
		this.id = configApplication.getId();
		this.paramName = configApplication.getParamName();
		this.value = configApplication.getValue();
	}

	public static ConfigApplicationDTO toDTO(ConfigApplication configApplication) {
		if (configApplication == null) {
			return null;
		} else {
			return new ConfigApplicationDTO(configApplication);
		}
	}

	public static List<ConfigApplicationDTO> toDTOs(List<ConfigApplication> configApplications) {
		if (configApplications == null) {
			return new ArrayList<>(0);
		}
		List<ConfigApplicationDTO> configApplicationDTOs = new ArrayList<>(configApplications.size());
		for (ConfigApplication configApplication : configApplications) {
			configApplicationDTOs.add(ConfigApplicationDTO.toDTO(configApplication));
		}
		return configApplicationDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
