package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Site;

public class SiteDTO {

	private Integer id;
	private String nom;
	private String coordonnees;

	public SiteDTO() {
		
	}
	
	public SiteDTO(Site site) {
		this.id = site.getId();
		this.nom = site.getNom();
		this.coordonnees = site.getCoordonnees();
	}

	public static SiteDTO toDTO(Site site) {
		if (site == null) {
			return null;
		} else {
			return new SiteDTO(site);
		}
	}

	public static List<SiteDTO> toDTOs(List<Site> sites) {
		if (sites == null) {
			return new ArrayList<>(0);
		}
		List<SiteDTO> siteDTOs = new ArrayList<>(sites.size());
		for (Site site : sites) {
			siteDTOs.add(SiteDTO.toDTO(site));
		}
		return siteDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCoordonnees() {
		return coordonnees;
	}

	public void setCoordonnees(String coordonnees) {
		this.coordonnees = coordonnees;
	}

}
