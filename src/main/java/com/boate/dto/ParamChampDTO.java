package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.enums.ParamChampTypeEnum;
import com.boate.model.ParamChamp;

public class ParamChampDTO {

	private Integer id;
	private String type;
	private String libelle;
	private ParamChampDTO paramChampEnfant;
	private String modelAttribut;
	private String requete;

	public ParamChampDTO() {
		
	}
	
	public ParamChampDTO(ParamChamp paramChamp) {
		this.id = paramChamp.getId();
		this.modelAttribut = paramChamp.getModelAttribut();
		this.type = paramChamp.getType() != null ? paramChamp.getType().toString() : ParamChampTypeEnum.INPUT_TEXT.toString();
		this.requete = paramChamp.getRequete();
		this.libelle = paramChamp.getLibelle();
		this.paramChampEnfant = ParamChampDTO.toDTO(paramChamp.getParamChampEnfant());
	}

	public static ParamChampDTO toDTO(ParamChamp paramChamp) {
		if (paramChamp == null) {
			return null;
		} else {
			return new ParamChampDTO(paramChamp);
		}
	}

	public static List<ParamChampDTO> toDTOs(List<ParamChamp> paramChamps) {
		if (paramChamps == null) {
			return new ArrayList<>(0);
		}
		List<ParamChampDTO> paramChampDTOs = new ArrayList<>(paramChamps.size());
		for (ParamChamp paramChamp : paramChamps) {
			paramChampDTOs.add(ParamChampDTO.toDTO(paramChamp));
		}
		return paramChampDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModelAttribut() {
		return modelAttribut;
	}

	public void setModelAttribut(String modelAttribut) {
		this.modelAttribut = modelAttribut;
	}

	public String getRequete() {
		return requete;
	}

	public void setRequete(String requete) {
		this.requete = requete;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public ParamChampDTO getParamChampEnfant() {
		return paramChampEnfant;
	}

	public void setParamChampEnfant(ParamChampDTO paramChampEnfant) {
		this.paramChampEnfant = paramChampEnfant;
	}

}
