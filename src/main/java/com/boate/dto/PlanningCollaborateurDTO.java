package com.boate.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.boate.model.PlanningCollaborateur;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanningCollaborateurDTO {
	
	private UtilisateurDTO utilisateur;
	private List<ReservationCollaborateurDTO> reservationsCollaborateur;
	
	// Début et fin de la plage de requête.
	private DTO dateDebut;
	private DTO dateFin;
	
	// Used to show the popup when the requested reservation is in conflict with an other existing one.
	// If that's the case, then the result will contain the incriminated reservation only instead of the full list after update.
	// Conflict on user is non-blocking.
	// Conflict on vehicle is BLOCKING.
	private boolean conflictOnUserResponse = false;
	
	public PlanningCollaborateurDTO() {
		
	}
	
	public PlanningCollaborateurDTO(List<PlanningCollaborateur> planningsCollaborateurs, LocalDateTime dateDebut, LocalDateTime dateFin) {
		this.dateDebut = DTO.parseFromLocalDateTime(dateDebut);
		this.dateFin = DTO.parseFromLocalDateTime(dateFin);
		if (planningsCollaborateurs != null && planningsCollaborateurs.size() > 0) {
			this.utilisateur = UtilisateurDTO.toDTO(planningsCollaborateurs.get(0).getUtilisateur(), true);
		}
		this.reservationsCollaborateur = ReservationCollaborateurDTO.toDTOs(planningsCollaborateurs);
	}

	public static PlanningCollaborateurDTO toDTO(List<PlanningCollaborateur> planningsCollaborateurs, LocalDateTime dateDebut, LocalDateTime dateFin) {
		if (planningsCollaborateurs == null) {
			return null;
		} else {
			return new PlanningCollaborateurDTO(planningsCollaborateurs, dateDebut, dateFin);
		}
	}

	public UtilisateurDTO getUtilisateur() {
		return utilisateur;
	}
	
	public void setUtilisateur(UtilisateurDTO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public List<ReservationCollaborateurDTO> getReservationsCollaborateur() {
		return reservationsCollaborateur;
	}

	public void setReservationsCollaborateur(List<ReservationCollaborateurDTO> reservationsCollaborateur) {
		this.reservationsCollaborateur = reservationsCollaborateur;
	}

	public DTO getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(DTO dateDebut) {
		this.dateDebut = dateDebut;
	}

	public DTO getDateFin() {
		return dateFin;
	}

	public void setDateFin(DTO dateFin) {
		this.dateFin = dateFin;
	}

	public boolean isConflictOnUserResponse() {
		return conflictOnUserResponse;
	}

	public void setConflictOnUserResponse(boolean conflictOnUserResponse) {
		this.conflictOnUserResponse = conflictOnUserResponse;
	}
	
}
