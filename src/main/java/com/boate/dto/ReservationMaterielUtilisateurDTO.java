package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.PlanningMateriel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReservationMaterielUtilisateurDTO {

	private Integer idPlanningMateriel;
	
	// True if this object is used in a request to get all Users having booked a specific Vehicle.
	// False if this object is used in a request to get all Vehicles booked by a User.
	// Avoid loading the grouping object multiple times in loop (whether it's a User or Vehicle).
	private boolean trueIfListOfUsersFalseIfListOfMateriels;
	
	private MaterielDTO materiel;
	private UtilisateurDTO utilisateur;
	private DTO dateDebut;
	private DTO dateFin;
	
	public ReservationMaterielUtilisateurDTO() {
		
	}
	
	public ReservationMaterielUtilisateurDTO(PlanningMateriel planningMateriel, boolean trueIfListOfUsersFalseIfListOfMateriels) {
		this.trueIfListOfUsersFalseIfListOfMateriels = trueIfListOfUsersFalseIfListOfMateriels;
		this.idPlanningMateriel = planningMateriel.getId();
		if (trueIfListOfUsersFalseIfListOfMateriels)
		{
			this.utilisateur = UtilisateurDTO.toDTO(planningMateriel.getUtilisateur(), true);
		}
		else
		{
			this.materiel = MaterielDTO.toDTO(planningMateriel.getMateriel());
		}
		this.dateDebut = DTO.parseFromLocalDateTime(planningMateriel.getDateDebut());
		this.dateFin = DTO.parseFromLocalDateTime(planningMateriel.getDateFin());
	}

	public static ReservationMaterielUtilisateurDTO toDTO(PlanningMateriel planningMateriel, boolean trueIfListOfUsersFalseIfListOfMateriels) {
		if (planningMateriel == null) {
			return null;
		} else {
			return new ReservationMaterielUtilisateurDTO(planningMateriel, trueIfListOfUsersFalseIfListOfMateriels);
		}
	}

	public static List<ReservationMaterielUtilisateurDTO> toDTOs(List<PlanningMateriel> planningMateriels, boolean trueIfListOfUsersFalseIfListOfMateriels) {
		if (planningMateriels == null) {
			return new ArrayList<>(0);
		}
		List<ReservationMaterielUtilisateurDTO> datesMaterielDTO = new ArrayList<>(planningMateriels.size());
		for (PlanningMateriel planningMateriel : planningMateriels) {
			datesMaterielDTO.add(ReservationMaterielUtilisateurDTO.toDTO(planningMateriel, trueIfListOfUsersFalseIfListOfMateriels));
		}
		return datesMaterielDTO;
	}

	public Integer getIdPlanningMateriel() {
		return idPlanningMateriel;
	}

	public void setIdPlanningMateriel(Integer idPlanningMateriel) {
		this.idPlanningMateriel = idPlanningMateriel;
	}

	public boolean isTrueIfListOfUsersFalseIfListOfMateriels() {
		return trueIfListOfUsersFalseIfListOfMateriels;
	}

	public void setTrueIfListOfUsersFalseIfListOfMateriels(boolean trueIfListOfUsersFalseIfListOfMateriels) {
		this.trueIfListOfUsersFalseIfListOfMateriels = trueIfListOfUsersFalseIfListOfMateriels;
	}

	public MaterielDTO getMateriel() {
		return materiel;
	}

	public void setMateriel(MaterielDTO materiel) {
		this.materiel = materiel;
	}

	public UtilisateurDTO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurDTO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public DTO getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(DTO dateDebut) {
		this.dateDebut = dateDebut;
	}

	public DTO getDateFin() {
		return dateFin;
	}

	public void setDateFin(DTO dateFin) {
		this.dateFin = dateFin;
	}
}
