package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.UniteOperationnelle;

public class UniteOperationnelleDTO {

	private Integer id;
	private String code;
	private String name;
	private InfrapoleDTO infrapole;
	private SecteurDTO secteur;
	
	public UniteOperationnelleDTO() {

	}

	public UniteOperationnelleDTO(UniteOperationnelle up, Boolean includeInfrapole, Boolean includeRegion, Boolean includeSecteur) {
		this.id = up.getId();
		this.code = up.getCode();
		this.name = up.getName();
		this.secteur = includeSecteur ? SecteurDTO.toDTO(up.getSecteur(), includeInfrapole, includeRegion) : null;
		this.infrapole = this.secteur == null ? InfrapoleDTO.toDTO(up.getInfrapole(), includeRegion) : null;

	}

	public static UniteOperationnelleDTO toDTO(UniteOperationnelle up, Boolean includeInfrapole, Boolean includeRegion,
			Boolean includeSecteur) {
		if (up == null) {
			return null;
		} else {
			return new UniteOperationnelleDTO(up, includeInfrapole, includeRegion, includeSecteur);
		}
	}

	public static List<UniteOperationnelleDTO> toDTOs(List<UniteOperationnelle> ups, Boolean includeInfrapole, Boolean includeRegion,
			Boolean includeSecteur) {
		if (ups == null) {
			return new ArrayList<>(0);
		}
		List<UniteOperationnelleDTO> upDTOs = new ArrayList<>(ups.size());
		for (UniteOperationnelle up : ups) {
			upDTOs.add(UniteOperationnelleDTO.toDTO(up, includeInfrapole, includeRegion, includeSecteur));
		}
		return upDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public InfrapoleDTO getInfrapole() {
		return infrapole;
	}

	public void setInfrapole(InfrapoleDTO infrapole) {
		this.infrapole = infrapole;
	}

	public SecteurDTO getSecteur() {
		return secteur;
	}

	public void setSecteur(SecteurDTO secteur) {
		this.secteur = secteur;
	}

}
