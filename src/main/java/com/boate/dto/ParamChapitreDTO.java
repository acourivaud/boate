package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.enums.TemplateStatutEnum;
import com.boate.model.ParamChapitre;

public class ParamChapitreDTO {

	private Integer id;
	private ParamModuleDTO paramModule;
	private Integer position;
	private String libelle;
	private Boolean specifique;
	private String statut;
	private List<ParamLigneDTO> paramLignes;

	public ParamChapitreDTO() {
		
	}
	
	public ParamChapitreDTO(ParamChapitre paramChapitre) {
		this.id = paramChapitre.getId();
		this.position = paramChapitre.getPosition();
		this.statut = paramChapitre.getStatut() != null ? paramChapitre.getStatut().toString() : TemplateStatutEnum.BROUILLON.toString();
		this.paramModule = ParamModuleDTO.toDTO(paramChapitre.getModule());
		this.libelle = paramChapitre.getLibelle();
		this.specifique = paramChapitre.getSpecifique();
		this.paramLignes = ParamLigneDTO.toDTOs(paramChapitre.getParamLignes());
	}

	public static ParamChapitreDTO toDTO(ParamChapitre paramChapitre) {
		if (paramChapitre == null) {
			return null;
		} else {
			return new ParamChapitreDTO(paramChapitre);
		}
	}

	public static List<ParamChapitreDTO> toDTOs(List<ParamChapitre> paramChapitres) {
		if (paramChapitres == null) {
			return new ArrayList<>(0);
		}
		List<ParamChapitreDTO> paramChapitreDTOs = new ArrayList<>(paramChapitres.size());
		for (ParamChapitre paramChapitre : paramChapitres) {
			paramChapitreDTOs.add(ParamChapitreDTO.toDTO(paramChapitre));
		}
		return paramChapitreDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public ParamModuleDTO getParamModule() {
		return paramModule;
	}

	public void setParamModule(ParamModuleDTO paramModule) {
		this.paramModule = paramModule;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Boolean getSpecifique() {
		return specifique;
	}

	public void setSpecifique(Boolean specifique) {
		this.specifique = specifique;
	}

	public List<ParamLigneDTO> getParamLignes() {
		return paramLignes;
	}

	public void setParamLignes(List<ParamLigneDTO> paramLignes) {
		this.paramLignes = paramLignes;
	}

}
