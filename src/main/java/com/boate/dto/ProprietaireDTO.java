package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Proprietaire;

public class ProprietaireDTO {

	private Integer id;
	private String nom;

	public ProprietaireDTO() {
		
	}
	
	public ProprietaireDTO(Proprietaire proprietaire) {
		this.id = proprietaire.getId();
		this.nom = proprietaire.getName();
	}

	public static ProprietaireDTO toDTO(Proprietaire proprietaire) {
		if (proprietaire == null) {
			return null;
		} else {
			return new ProprietaireDTO(proprietaire);
		}
	}

	public static List<ProprietaireDTO> toDTOs(List<Proprietaire> proprietaires) {
		if (proprietaires == null) {
			return new ArrayList<>(0);
		}
		List<ProprietaireDTO> proprietaireDTOs = new ArrayList<>(proprietaires.size());
		for (Proprietaire proprietaire : proprietaires) {
			proprietaireDTOs.add(ProprietaireDTO.toDTO(proprietaire));
		}
		return proprietaireDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
