package com.boate.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import com.boate.model.ArchiveEmprunt;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

public class ArchiveEmpruntDTO {

	private Integer id;
	private ArchiveDossierDTO dossier;
	private UtilisateurDTO utilisateur;
	
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonFormat(pattern="dd/MM/yyyy hh:mm")
	private LocalDateTime dateEmprunt;
	
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonFormat(pattern="dd/MM/yyyy hh:mm")
	private LocalDateTime dateRestitution;

	public ArchiveEmpruntDTO() {
		
	}
	
	public ArchiveEmpruntDTO(ArchiveEmprunt archiveEmprunt, String page) {
		this.id = archiveEmprunt.getId();
		this.dossier = ArchiveDossierDTO.toDTO(archiveEmprunt.getDossier(), page);
		this.utilisateur = UtilisateurDTO.toDTO(archiveEmprunt.getUtilisateur(), true);
		this.dateEmprunt = archiveEmprunt.getDateEmprunt();
		this.dateRestitution = archiveEmprunt.getDateRestitution();
	}

	public static ArchiveEmpruntDTO toDTO(ArchiveEmprunt archiveEmprunt, String page) {
		if (archiveEmprunt == null) {
			return null;
		} else {
			return new ArchiveEmpruntDTO(archiveEmprunt, page);
		}
	}

	public static List<ArchiveEmpruntDTO> toDTOs(List<ArchiveEmprunt> archiveEmprunts, String page) {
		if (archiveEmprunts == null) {
			return new ArrayList<>(0);
		}
		List<ArchiveEmpruntDTO> archiveEmpruntDTOs = new ArrayList<>(archiveEmprunts.size());
		for (ArchiveEmprunt archiveEmprunt : archiveEmprunts) {
			archiveEmpruntDTOs.add(ArchiveEmpruntDTO.toDTO(archiveEmprunt, page));
		}
		return archiveEmpruntDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ArchiveDossierDTO getDossier() {
		return dossier;
	}

	public void setDossier(ArchiveDossierDTO dossier) {
		this.dossier = dossier;
	}

	public UtilisateurDTO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurDTO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public LocalDateTime getDateEmprunt() {
		return dateEmprunt;
	}

	public void setDateEmprunt(LocalDateTime dateEmprunt) {
		this.dateEmprunt = dateEmprunt;
	}

	public LocalDateTime getDateRestitution() {
		return dateRestitution;
	}

	public void setDateRestitution(LocalDateTime dateRestitution) {
		this.dateRestitution = dateRestitution;
	}

}
