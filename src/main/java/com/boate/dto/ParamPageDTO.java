package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.ParamPage;

public class ParamPageDTO {

	private Integer id;
	private String libelle;

	public ParamPageDTO() {
		
	}
	
	public ParamPageDTO(ParamPage paramPage) {
		this.id = paramPage.getId();
		this.libelle = paramPage.getLibelle();
	}

	public static ParamPageDTO toDTO(ParamPage paramPage) {
		if (paramPage == null) {
			return null;
		} else {
			return new ParamPageDTO(paramPage);
		}
	}

	public static List<ParamPageDTO> toDTOs(List<ParamPage> paramPages) {
		if (paramPages == null) {
			return new ArrayList<>(0);
		}
		List<ParamPageDTO> paramPageDTOs = new ArrayList<>(paramPages.size());
		for (ParamPage paramPage : paramPages) {
			paramPageDTOs.add(ParamPageDTO.toDTO(paramPage));
		}
		return paramPageDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

}
