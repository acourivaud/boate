package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Type;

public class TypeDTO {

	private Integer id;
	private String libelle;
	private CategorieDTO categorie;

	public TypeDTO() {

	}

	public static TypeDTO toDTO(Type type) {
		if (type == null) {
			return null;
		} else {
			final TypeDTO typeDTO = new TypeDTO();
			typeDTO.setId(type.getId());
			typeDTO.setLibelle(type.getLibelle());
			typeDTO.setCategorie(CategorieDTO.toDTO(type.getCategorie()));
			return typeDTO;
		}
	}

	public static List<TypeDTO> toDTOs(List<Type> types) {
		if (types == null) {
			return new ArrayList<>(0);
		}
		List<TypeDTO> typeDTOs = new ArrayList<>(types.size());
		for (Type type : types) {
			typeDTOs.add(TypeDTO.toDTO(type));
		}
		return typeDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public CategorieDTO getCategorie() {
		return categorie;
	}

	public void setCategorie(CategorieDTO categorie) {
		this.categorie = categorie;
	}

}
