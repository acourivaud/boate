package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.TypeOuvrage;

public class TypeOuvrageDTO {

	private Integer id;
	private String nomBoate;
	private String nomPatrimoine;

	public TypeOuvrageDTO() {

	}

	public static TypeOuvrageDTO toDTO(TypeOuvrage typeOuvrage, Boolean simplifie) {
		if (typeOuvrage == null) {
			return null;
		} else {
			final TypeOuvrageDTO typeOuvrageDTO = new TypeOuvrageDTO();
			if (simplifie) {
				typeOuvrageDTO.setNomBoate(typeOuvrage.getNomBoate());
			} else {
				typeOuvrageDTO.setId(typeOuvrage.getId());
				typeOuvrageDTO.setNomBoate(typeOuvrage.getNomBoate());
				typeOuvrageDTO.setNomPatrimoine(typeOuvrage.getNomPatrimoine());
			}
			return typeOuvrageDTO;
		}
	}

	public static List<TypeOuvrageDTO> toDTOs(List<TypeOuvrage> typesOuvrage) {
		if (typesOuvrage == null) {
			return new ArrayList<>(0);
		}
		List<TypeOuvrageDTO> typeOuvrageDTOs = new ArrayList<>(typesOuvrage.size());
		for (TypeOuvrage line : typesOuvrage) {
			typeOuvrageDTOs.add(TypeOuvrageDTO.toDTO(line, false));
		}
		return typeOuvrageDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomBoate() {
		return nomBoate;
	}

	public void setNomBoate(String nomBoate) {
		this.nomBoate = nomBoate;
	}

	public String getNomPatrimoine() {
		return nomPatrimoine;
	}

	public void setNomPatrimoine(String nomPatrimoine) {
		this.nomPatrimoine = nomPatrimoine;
	}

}
