package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Secteur;

public class SecteurDTO {

	private Integer id;
	private String nom;
	private InfrapoleDTO infrapole;

	public SecteurDTO() {
		
	}
	
	public SecteurDTO(Secteur secteur, Boolean includeInfrapole, Boolean includeRegion) {
		this.id = secteur.getId();
		this.nom = secteur.getNom();
		this.infrapole = includeInfrapole ? InfrapoleDTO.toDTO(secteur.getInfrapole(), includeRegion) : null;
	}

	public static SecteurDTO toDTO(Secteur secteur, Boolean includeInfrapole, Boolean includeRegion) {
		if (secteur == null) {
			return null;
		} else {
			return new SecteurDTO(secteur, includeInfrapole, includeRegion);
		}
	}

	public static List<SecteurDTO> toDTOs(List<Secteur> infrapoles, Boolean includeInfrapole, Boolean includeRegion) {
		if (infrapoles == null) {
			return new ArrayList<>(0);
		}
		List<SecteurDTO> infrapoleDTOs = new ArrayList<>(infrapoles.size());
		for (Secteur secteur : infrapoles) {
			infrapoleDTOs.add(SecteurDTO.toDTO(secteur, includeInfrapole, includeRegion));
		}
		return infrapoleDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public InfrapoleDTO getInfrapole() {
		return infrapole;
	}

	public void setInfrapole(InfrapoleDTO infrapole) {
		this.infrapole = infrapole;
	}

}
