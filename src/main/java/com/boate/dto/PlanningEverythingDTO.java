package com.boate.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanningEverythingDTO {
	
	private List<PlanningUtilisateurMaterielDTO> materiels;
	private List<PlanningUtilisateurVehiculeDTO> vehicules;
	private List<PlanningCollaborateurDTO> collaborateurs;
	
	public PlanningEverythingDTO() {
		
	}

	public List<PlanningUtilisateurMaterielDTO> getMateriels() {
		return materiels;
	}

	public void setMateriels(List<PlanningUtilisateurMaterielDTO> materiels) {
		this.materiels = materiels;
	}

	public List<PlanningUtilisateurVehiculeDTO> getVehicules() {
		return vehicules;
	}

	public void setVehicules(List<PlanningUtilisateurVehiculeDTO> vehicules) {
		this.vehicules = vehicules;
	}

	public List<PlanningCollaborateurDTO> getCollaborateurs() {
		return collaborateurs;
	}

	public void setCollaborateurs(List<PlanningCollaborateurDTO> collaborateurs) {
		this.collaborateurs = collaborateurs;
	}
	
}
