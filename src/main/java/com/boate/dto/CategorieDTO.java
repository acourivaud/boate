package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Categorie;

public class CategorieDTO {

	private Integer id;
	private String libelle;

	public CategorieDTO() {

	}

	public static CategorieDTO toDTO(Categorie categorie) {
		if (categorie == null) {
			return null;
		} else {
			final CategorieDTO categorieDTO = new CategorieDTO();
			categorieDTO.setId(categorie.getId());
			categorieDTO.setLibelle(categorie.getLibelle());
			return categorieDTO;
		}
	}

	public static List<CategorieDTO> toDTOs(List<Categorie> categories) {
		if (categories == null) {
			return new ArrayList<>(0);
		}
		List<CategorieDTO> categorieDTOs = new ArrayList<>(categories.size());
		for (Categorie categorie : categories) {
			categorieDTOs.add(CategorieDTO.toDTO(categorie));
		}
		return categorieDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

}
