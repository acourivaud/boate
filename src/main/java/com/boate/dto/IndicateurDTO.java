package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Indicateur;

public class IndicateurDTO {

	private Integer id;
	private Integer annee;
	private String typeVisite;
	private UtilisateurDTO utilisateur;
	private Integer nombreRealise;
	private Integer nombrePrevisionnel;
	private Double t1;
	private Double t2;
	private Double t3;
	private Double t4;
	private String globalIndicator;
	private UniteOperationnelleDTO up;

	public IndicateurDTO() {
		
	}
	
	public IndicateurDTO(Indicateur indicateur) {
		this.id = indicateur.getId();
		this.annee = indicateur.getAnnee();
		this.typeVisite = indicateur.getTypeVisite();
		this.utilisateur = UtilisateurDTO.toDTO(indicateur.getUtilisateur(), true);
		this.nombreRealise = indicateur.getNombreRealise();
		this.nombrePrevisionnel = indicateur.getNombrePrevisionnel();
		this.t1 = indicateur.getTrimestre1();
		this.t2 = indicateur.getTrimestre2();
		this.t3 = indicateur.getTrimestre3();
		this.t4 = indicateur.getTrimestre4();
		this.globalIndicator = indicateur.getGlobalIndicator();
		this.up = UniteOperationnelleDTO.toDTO(indicateur.getUp(), false, false, false);
	}

	public static IndicateurDTO toDTO(Indicateur indicateur) {
		if (indicateur == null) {
			return null;
		} else {
			return new IndicateurDTO(indicateur);
		}
	}

	public static List<IndicateurDTO> toDTOs(List<Indicateur> indicateurs) {
		if (indicateurs == null) {
			return new ArrayList<>(0);
		}
		List<IndicateurDTO> indicateurDTOs = new ArrayList<>(indicateurs.size());
		for (Indicateur indicateur : indicateurs) {
			indicateurDTOs.add(IndicateurDTO.toDTO(indicateur));
		}
		return indicateurDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAnnee() {
		return annee;
	}

	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

	public String getTypeVisite() {
		return typeVisite;
	}

	public void setTypeVisite(String typeVisite) {
		this.typeVisite = typeVisite;
	}

	public UtilisateurDTO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurDTO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Integer getNombreRealise() {
		return nombreRealise;
	}

	public void setNombreRealise(Integer nombreRealise) {
		this.nombreRealise = nombreRealise;
	}

	public Integer getNombrePrevisionnel() {
		return nombrePrevisionnel;
	}

	public void setNombrePrevisionnel(Integer nombrePrevisionnel) {
		this.nombrePrevisionnel = nombrePrevisionnel;
	}

	public Double getT1() {
		return t1;
	}

	public void setT1(Double t1) {
		this.t1 = t1;
	}

	public Double getT2() {
		return t2;
	}

	public void setT2(Double t2) {
		this.t2 = t2;
	}

	public Double getT3() {
		return t3;
	}

	public void setT3(Double t3) {
		this.t3 = t3;
	}

	public Double getT4() {
		return t4;
	}

	public void setT4(Double t4) {
		this.t4 = t4;
	}

	public String getGlobalIndicator() {
		return globalIndicator;
	}

	public void setGlobalIndicator(String globalIndicator) {
		this.globalIndicator = globalIndicator;
	}

	public UniteOperationnelleDTO getUp() {
		return up;
	}

	public void setUp(UniteOperationnelleDTO up) {
		this.up = up;
	}
}
