package com.boate.dto;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class ParamValueDTO {

	private String param;
	private String value;

	public ParamValueDTO() {

	}

	public ParamValueDTO(String param, String value) {
		this.param = param;
		this.value = value;
	}
	
	public ParamValueDTO(Object object) {
		this.param = getStringValueFromObjectAndFieldName(object, "param");
		this.value = getStringValueFromObjectAndFieldName(object, "value");
	}
	
	public String getStringValueFromObjectAndFieldName(Object object, String fieldName) {
		Field field;
		try {
			field = object.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			return field.get(object) != null ? field.get(object).toString() : null;
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public static ParamValueDTO toDTO(Object object) {
		if (object == null) {
			return null;
		} else {
			return new ParamValueDTO(object);
		}
	}
	
	public static List<ParamValueDTO> toDTOs(List<?> objectArray) {
		if (objectArray == null) {
			return new ArrayList<>(0);
		}
		List<ParamValueDTO> utilisateurFilterDTOs = new ArrayList<>(objectArray.size());
		
		for (Object object : objectArray) {
				utilisateurFilterDTOs.add(ParamValueDTO.toDTO(object));
			}
		return utilisateurFilterDTOs;
	}
}
