package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.LigneStatut;

public class LigneStatutDTO {

	private Integer id;
	private String statut;
	private String pkDebut;
	private String pkFin;
	private LigneDTO ligne;

	public LigneStatutDTO() {
		
	}
	
	public LigneStatutDTO(LigneStatut ligneStatut) {
		this.id = ligneStatut.getId();
		this.statut = ligneStatut.getStatut();
		this.pkDebut = ligneStatut.getPkDebut();
		this.pkFin = ligneStatut.getPkFin();
		this.ligne = LigneDTO.toDTO(ligneStatut.getLigne(), true);
	}

	public static LigneStatutDTO toDTO(LigneStatut ligneStatut) {
		if (ligneStatut == null) {
			return null;
		} else {
			return new LigneStatutDTO(ligneStatut);
		}
	}

	public static List<LigneStatutDTO> toDTOs(List<LigneStatut> ligneStatuts) {
		if (ligneStatuts == null) {
			return new ArrayList<>(0);
		}
		List<LigneStatutDTO> ligneStatutDTOs = new ArrayList<>(ligneStatuts.size());
		for (LigneStatut ligneStatut : ligneStatuts) {
			ligneStatutDTOs.add(LigneStatutDTO.toDTO(ligneStatut));
		}
		return ligneStatutDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getPkDebut() {
		return pkDebut;
	}

	public void setPkDebut(String pkDebut) {
		this.pkDebut = pkDebut;
	}

	public String getPkFin() {
		return pkFin;
	}

	public void setPkFin(String pkFin) {
		this.pkFin = pkFin;
	}

	public LigneDTO getLigne() {
		return ligne;
	}

	public void setLigne(LigneDTO ligne) {
		this.ligne = ligne;
	}

}
