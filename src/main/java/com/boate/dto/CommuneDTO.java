package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Commune;

public class CommuneDTO {

	private Integer id;
	private String codePostal;
	private String nom;

	public CommuneDTO() {
		
	}
	
	public CommuneDTO(Commune commune, Boolean includeCodePostal) {
		this.id = commune.getId();
		this.nom = commune.getLibelle();
		if (includeCodePostal) {
			this.codePostal = commune.getCodePostal();
		}
	}

	public static CommuneDTO toDTO(Commune commune, Boolean includeCodePostal) {
		if (commune == null) {
			return null;
		} else {
			return new CommuneDTO(commune, includeCodePostal);
		}
	}

	public static List<CommuneDTO> toDTOs(List<Commune> communes, Boolean includeCodePostal) {
		if (communes == null) {
			return new ArrayList<>(0);
		}
		List<CommuneDTO> communeDTOs = new ArrayList<>(communes.size());
		for (Commune commune : communes) {
			communeDTOs.add(CommuneDTO.toDTO(commune, includeCodePostal));
		}
		return communeDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

}
