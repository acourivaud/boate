package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.In1253;

public class In1253DTO {

	private Integer id;
	private String target;
	private Integer periodiciteId;
	private String auteurId;
	private String auteurId2;
	private Integer periodiciteVi;
	private String auteurVi;
	private String auteurVi2;
	private Integer periodiciteVd;
	private String auteurVd;
	private String auteurVd2;
	private Boolean idi;
	private Boolean cv;
	private Boolean sp;
	private Boolean sr;

	public In1253DTO() {
		
	}
	
	public In1253DTO(In1253 in1253) {
		this.id = in1253.getId();
		this.target = in1253.getTarget();
		this.periodiciteId = in1253.getPeriodiciteId();
		this.periodiciteVd = in1253.getPeriodiciteVd();
		this.periodiciteVi = in1253.getPeriodiciteVi();
		this.auteurId = in1253.getAuteurId();
		this.auteurVd = in1253.getAuteurVd();
		this.auteurVi = in1253.getAuteurVi();
		this.auteurId2 = in1253.getAuteurId2();
		this.auteurVd2 = in1253.getAuteurVd2();
		this.auteurVi2 = in1253.getAuteurVi2();
		this.idi = in1253.getIdi();
		this.cv = in1253.getCv();
		this.sp = in1253.getSp();
		this.sr = in1253.getSr();
	}

	public static In1253DTO toDTO(In1253 in1253) {
		if (in1253 == null) {
			return null;
		} else {
			return new In1253DTO(in1253);
		}
	}

	public static List<In1253DTO> toDTOs(List<In1253> in1253s) {
		if (in1253s == null) {
			return new ArrayList<>(0);
		}
		List<In1253DTO> in1253DTOs = new ArrayList<>(in1253s.size());
		for (In1253 in1253 : in1253s) {
			in1253DTOs.add(In1253DTO.toDTO(in1253));
		}
		return in1253DTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getPeriodiciteId() {
		return periodiciteId;
	}

	public void setPeriodiciteId(Integer periodiciteId) {
		this.periodiciteId = periodiciteId;
	}

	public String getAuteurId() {
		return auteurId;
	}

	public void setAuteurId(String auteurId) {
		this.auteurId = auteurId;
	}

	public String getAuteurId2() {
		return auteurId2;
	}

	public void setAuteurId2(String auteurId2) {
		this.auteurId2 = auteurId2;
	}

	public Integer getPeriodiciteVi() {
		return periodiciteVi;
	}

	public void setPeriodiciteVi(Integer periodiciteVi) {
		this.periodiciteVi = periodiciteVi;
	}

	public String getAuteurVi() {
		return auteurVi;
	}

	public void setAuteurVi(String auteurVi) {
		this.auteurVi = auteurVi;
	}

	public String getAuteurVi2() {
		return auteurVi2;
	}

	public void setAuteurVi2(String auteurVi2) {
		this.auteurVi2 = auteurVi2;
	}

	public Integer getPeriodiciteVd() {
		return periodiciteVd;
	}

	public void setPeriodiciteVd(Integer periodiciteVd) {
		this.periodiciteVd = periodiciteVd;
	}

	public String getAuteurVd() {
		return auteurVd;
	}

	public void setAuteurVd(String auteurVd) {
		this.auteurVd = auteurVd;
	}

	public String getAuteurVd2() {
		return auteurVd2;
	}

	public void setAuteurVd2(String auteurVd2) {
		this.auteurVd2 = auteurVd2;
	}

	public Boolean getIdi() {
		return idi;
	}

	public void setIdi(Boolean idi) {
		this.idi = idi;
	}

	public Boolean getCv() {
		return cv;
	}

	public void setCv(Boolean cv) {
		this.cv = cv;
	}

	public Boolean getSp() {
		return sp;
	}

	public void setSp(Boolean sp) {
		this.sp = sp;
	}

	public Boolean getSr() {
		return sr;
	}

	public void setSr(Boolean sr) {
		this.sr = sr;
	}

}
