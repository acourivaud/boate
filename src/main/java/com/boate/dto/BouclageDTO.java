package com.boate.dto;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import com.boate.enums.PropositionBouclageStatusEnum;
import com.boate.model.Bouclage;
import com.boate.model.BouclageUtilisateur;
import com.boate.security.CurrentUser;
import com.boate.security.entity.UserPrincipal;
import com.boate.utils.ConvertData;

public class BouclageDTO {

	private Integer id;
	private Boolean ecr1;
	private Boolean ecr2;
	private Boolean ecr3;
	private Boolean ecr4;
	private Boolean ecr5;
	private Boolean ecr6;
	private Boolean envoiSt0;
	private Boolean envoiSt1;
	private Boolean envoiSt2;
	private Boolean hasCourriers0;
	private Boolean hasCourriers1;
	private Boolean hasCourriers2;
	private Boolean incident;
	private Boolean u0Integrite;
	private Boolean u0SecuritePersonne;
	private Boolean u0SecuriteTiers;
	private Object u0_etude;
	private Object u0_expertise;
	private Boolean u1Integrite;
	private Boolean u1SecuritePersonne;
	private Boolean u1SecuriteTiers;
	private Object u1_etude;
	private Object u1_expertise;
	private Boolean u2Integrite;
	private Boolean u2SecuritePersonne;
	private Boolean u2SecuriteTiers;
	private Object u2_etude;
	private Object u2_expertise;
	private Boolean propositionTravauxU0;
	private Boolean propositionTravauxU1;
	private Boolean propositionTravauxU2;
	private Double noteAppui;
	private Double noteAppuiPrec;
	private Double noteGlobale;
	private Double noteGlobalePrec;
	private Double noteMax;
	private Double noteMin;
	private Double noteTablier;
	private Double noteTablierPrec;
	private Integer costConfortement0;
	private Integer costConfortement1;
	private Integer costConfortement2;
	private Integer costCurage0;
	private Integer costCurage1;
	private Integer costCurage2;
	private Integer costDebroussaillage0;
	private Integer costDebroussaillage1;
	private Integer costDebroussaillage2;
	private Integer costDivers0;
	private Integer costDivers1;
	private Integer costDivers2;
	private Integer costInstrumentation0;
	private Integer costInstrumentation1;
	private Integer costInstrumentation2;
	private Integer costMaconnerie0;
	private Integer costMaconnerie1;
	private Integer costMaconnerie2;
	private Integer costPeinture0;
	private Integer costPeinture1;
	private Integer costPeinture2;
	private Integer costPurge0;
	private Integer costPurge1;
	private Integer costPurge2;
	private Integer costSerrurerie0;
	private Integer costSerrurerie1;
	private Integer costSerrurerie2;
	private Integer costTerrassement0;
	private Integer costTerrassement1;
	private Integer costTerrassement2;
	private Integer costTopographie0;
	private Integer costTopographie1;
	private Integer costTopographie2;
	private Integer laborCostAgent0;
	private Integer laborCostAgent1;
	private Integer laborCostAgent2;
	private Integer laborCostSupervisor0;
	private Integer laborCostSupervisor1;
	private Integer laborCostSupervisor2;
	private Integer previsionDate;
	private DateBouclageDTO date00;
	private DateBouclageDTO date01;
	private DateBouclageDTO date02;
	private DateBouclageDTO date03;
	private DateBouclageDTO date04;
	private DateBouclageDTO date05;
	private DateBouclageDTO date06;
	private DateBouclageDTO date07;
	private DateBouclageDTO date08;
	private DateBouclageDTO date09;
	private DateBouclageDTO date10;
	private DateBouclageDTO date11;
	private DateBouclageDTO date12;
	private DateBouclageDTO date13;
	private DateBouclageDTO date14;
	private DateBouclageDTO date15;
	private DateBouclageDTO date16;
	private DateBouclageDTO date17;
	private DateBouclageDTO date18;
	private DateBouclageDTO date19;
	private DateBouclageDTO date20;
	private DateBouclageDTO date21;
	private DateBouclageDTO date22;
	private DateBouclageDTO date23;
	private DateBouclageDTO date24;
	private DateBouclageDTO date25;
	private DateBouclageDTO date26;
	private DateBouclageDTO date27;
	private DateBouclageDTO date28;
	private DateBouclageDTO date29;
	private DateBouclageDTO date30;
	private DateBouclageDTO date31;
	private DateBouclageDTO date32;
	private DateBouclageDTO date33;
	private DateBouclageDTO date34;
	private DateBouclageDTO date35;
	private DateBouclageDTO date36;
	private DateBouclageDTO date37;
	private DateBouclageDTO date38;
	private DateBouclageDTO date39;
	private DateBouclageDTO date40;
	private DateBouclageDTO incidentDate;
	private String commentaires;
	private String coteConfortement0;
	private String coteConfortement1;
	private String coteConfortement2;
	private String coteCurage0;
	private String coteCurage1;
	private String coteCurage2;
	private String coteDebroussaillage0;
	private String coteDebroussaillage1;
	private String coteDebroussaillage2;
	private String coteDivers0;
	private String coteDivers1;
	private String coteDivers2;
	private String coteInstrumentation0;
	private String coteInstrumentation1;
	private String coteInstrumentation2;
	private String cotePurge0;
	private String cotePurge1;
	private String cotePurge2;
	private String coteTerrassement0;
	private String coteTerrassement1;
	private String coteTerrassement2;
	private String coteTopographie0;
	private String coteTopographie1;
	private String coteTopographie2;
	private String date_maj;
	private String ecr1_pkd;
	private String ecr1_pkf;
	private String ecr2_pkd;
	private String ecr2_pkf;
	private String ecr3_pkd;
	private String ecr3_pkf;
	private String ecr4_pkd;
	private String ecr4_pkf;
	private String ecr5_pkd;
	private String ecr5_pkf;
	private String ecr6_pkd;
	private String ecr6_pkf;
	private String etatOuvrage;
	private String ficheChuteBloc;
	private String incidentDescription;
	private String incidentImportanceKey;
	private String laborAcceptation0Key;
	private String laborAcceptation1Key;
	private String laborAcceptation2Key;
	private String laborAnalyseRisque0;
	private String laborAnalyseRisque1;
	private String laborAnalyseRisque2;
	private String laborMesuresConservatoire0;
	private String laborMesuresConservatoire1;
	private String laborMesuresConservatoire2;
	private String laborTodoAgent0;
	private String laborTodoAgent1;
	private String laborTodoAgent2;
	private PropositionBouclageDTO laborTodoSupervisor0;
	private PropositionBouclageDTO laborTodoSupervisor1;
	private PropositionBouclageDTO laborTodoSupervisor2;
	private String motifCV;
	private String motifControle;
	private String numeroFicheRex;
	private String solutionCV;
	private String typeSurvCompl;
	private String u0_typeExpertise;
	private String u1_typeExpertise;
	private String u2_typeExpertise;
	private SurveillanceDTO surveillance;
	private String typeKey;
	private UtilisateurDTO auteurMaj;
	private UtilisateurDTO author;
	private UtilisateurDTO authorSurveillance;
	
	private String delaiTravauxAgent0;
    private String delaiTravauxAgent1;
    private String delaiTravauxExpert0;
    private String delaiTravauxExpert1;
	private Integer operation;
	private String etatCV;
	private String motifAnnulationCV;
	private String statut;
	private String auteurSuppression;
	private DateBouclageDTO dateSuppression;
	
	private List<BouclageUtilisateur> auteursEtapeBouclage;

	public BouclageDTO() {
		
	}
	
	public BouclageDTO(Bouclage bouclage, String page, @CurrentUser UserPrincipal currentUser) {
		try {
			this.id = bouclage.getId();
			
			this.ecr1 = bouclage.getEcr1();
			this.ecr2 = bouclage.getEcr2();
			this.ecr3 = bouclage.getEcr3();
			this.ecr4 = bouclage.getEcr4();
			this.ecr5 = bouclage.getEcr5();
			this.ecr6 = bouclage.getEcr6();
			this.envoiSt0 = bouclage.getEnvoiSt0();
			this.envoiSt1 = bouclage.getEnvoiSt1();
			this.envoiSt2 = bouclage.getEnvoiSt2();
			this.hasCourriers0 = bouclage.getHasCourriers0();
			this.hasCourriers1 = bouclage.getHasCourriers1();
			this.hasCourriers2 = bouclage.getHasCourriers2();
			this.incident = bouclage.getIncident();
			this.u0Integrite = bouclage.getU0Integrite();
			this.u0SecuritePersonne = bouclage.getU0SecuritePersonne();
			this.u0SecuriteTiers = bouclage.getU0SecuriteTiers();
			this.u1Integrite = bouclage.getU1Integrite();
			this.u1SecuritePersonne = bouclage.getU1SecuritePersonne();
			this.u1SecuriteTiers = bouclage.getU1SecuriteTiers();
			this.u2Integrite = bouclage.getU2Integrite();
			this.u2SecuritePersonne = bouclage.getU2SecuritePersonne();
			this.u2SecuriteTiers = bouclage.getU2SecuriteTiers();
			this.noteAppui = bouclage.getNoteAppui();
			this.noteAppuiPrec = bouclage.getNoteAppuiPrec();
			this.noteGlobale = bouclage.getNoteGlobale();
			this.noteGlobalePrec = bouclage.getNoteGlobalePrec();
			this.noteMax = bouclage.getNoteMax();
			this.noteMin = bouclage.getNoteMin();
			this.noteTablier = bouclage.getNoteTablier();
			this.noteTablierPrec = bouclage.getNoteTablierPrec();
			this.costConfortement0 = bouclage.getCostConfortement0();
			this.costConfortement1 = bouclage.getCostConfortement1();
			this.costConfortement2 = bouclage.getCostConfortement2();
			this.costCurage0 = bouclage.getCostCurage0();
			this.costCurage1 = bouclage.getCostCurage1();
			this.costCurage2 = bouclage.getCostCurage2();
			this.costDebroussaillage0 = bouclage.getCostDebroussaillage0();
			this.costDebroussaillage1 = bouclage.getCostDebroussaillage1();
			this.costDebroussaillage2 = bouclage.getCostDebroussaillage2();
			this.costDivers0 = bouclage.getCostDivers0();
			this.costDivers1 = bouclage.getCostDivers1();
			this.costDivers2 = bouclage.getCostDivers2();
			this.costInstrumentation0 = bouclage.getCostInstrumentation0();
			this.costInstrumentation1 = bouclage.getCostInstrumentation1();
			this.costInstrumentation2 = bouclage.getCostInstrumentation2();
			this.costMaconnerie0 = bouclage.getCostMaconnerie0();
			this.costMaconnerie1 = bouclage.getCostMaconnerie1();
			this.costMaconnerie2 = bouclage.getCostMaconnerie2();
			this.costPeinture0 = bouclage.getCostPeinture0();
			this.costPeinture1 = bouclage.getCostPeinture1();
			this.costPeinture2 = bouclage.getCostPeinture2();
			this.costPurge0 = bouclage.getCostPurge0();
			this.costPurge1 = bouclage.getCostPurge1();
			this.costPurge2 = bouclage.getCostPurge2();
			this.costSerrurerie0 = bouclage.getCostSerrurerie0();
			this.costSerrurerie1 = bouclage.getCostSerrurerie1();
			this.costSerrurerie2 = bouclage.getCostSerrurerie2();
			this.costTerrassement0 = bouclage.getCostTerrassement0();
			this.costTerrassement1 = bouclage.getCostTerrassement1();
			this.costTerrassement2 = bouclage.getCostTerrassement2();
			this.costTopographie0 = bouclage.getCostTopographie0();
			this.costTopographie1 = bouclage.getCostTopographie1();
			this.costTopographie2 = bouclage.getCostTopographie2();
			this.laborCostAgent0 = bouclage.getLaborCostAgent0();
			this.laborCostAgent1 = bouclage.getLaborCostAgent1();
			this.laborCostAgent2 = bouclage.getLaborCostAgent2();
			this.laborCostSupervisor0 = bouclage.getLaborCostSupervisor0();
			this.laborCostSupervisor1 = bouclage.getLaborCostSupervisor1();
			this.laborCostSupervisor2 = bouclage.getLaborCostSupervisor2();
			this.previsionDate = bouclage.getPrevisionDate();
			
			this.auteursEtapeBouclage = bouclage.getBouclageUtilisateurs();
			this.date00 = new DateBouclageDTO(currentUser, bouclage.getDate00(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate00() != null ? this.auteursEtapeBouclage.get(0).getDate00().getFullName() : null : null);
			this.date01 = new DateBouclageDTO(currentUser, bouclage.getDate01(), diffDate00And01(bouclage), testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate01() != null ? this.auteursEtapeBouclage.get(0).getDate01().getFullName() : null : null);
			this.date02 = new DateBouclageDTO(currentUser, bouclage.getDate02(), diffDate00And02(bouclage), testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate02() != null ? this.auteursEtapeBouclage.get(0).getDate02().getFullName() : null : null);
			this.date03 = new DateBouclageDTO(currentUser, bouclage.getDate03(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate03() != null ? this.auteursEtapeBouclage.get(0).getDate03().getFullName() : null : null);
			this.date04 = new DateBouclageDTO(currentUser, bouclage.getDate04(), diffDate00And04(bouclage), testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate04() != null ? this.auteursEtapeBouclage.get(0).getDate04().getFullName() : null : null);
			this.date05 = new DateBouclageDTO(currentUser, bouclage.getDate05(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate05() != null ? this.auteursEtapeBouclage.get(0).getDate05().getFullName() : null : null);
			this.date06 = new DateBouclageDTO(currentUser, bouclage.getDate06(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate06() != null ? this.auteursEtapeBouclage.get(0).getDate06().getFullName() : null : null);
			this.date07 = new DateBouclageDTO(currentUser, bouclage.getDate07(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate07() != null ? this.auteursEtapeBouclage.get(0).getDate07().getFullName() : null : null);
			this.date08 = new DateBouclageDTO(currentUser, bouclage.getDate08(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate08() != null ? this.auteursEtapeBouclage.get(0).getDate08().getFullName() : null : null);
			this.date09 = new DateBouclageDTO(currentUser, bouclage.getDate09(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate09() != null ? this.auteursEtapeBouclage.get(0).getDate09().getFullName() : null : null);
			this.date10 = new DateBouclageDTO(currentUser, bouclage.getDate10(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate10() != null ? this.auteursEtapeBouclage.get(0).getDate10().getFullName() : null : null);
			this.date11 = new DateBouclageDTO(currentUser, bouclage.getDate11(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate11() != null ? this.auteursEtapeBouclage.get(0).getDate11().getFullName() : null : null);
			this.date12 = new DateBouclageDTO(currentUser, bouclage.getDate12(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate12() != null ? this.auteursEtapeBouclage.get(0).getDate12().getFullName() : null : null);
			this.date13 = new DateBouclageDTO(currentUser, bouclage.getDate13(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate13() != null ? this.auteursEtapeBouclage.get(0).getDate13().getFullName() : null : null);
			this.date14 = new DateBouclageDTO(currentUser, bouclage.getDate14(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate14() != null ? this.auteursEtapeBouclage.get(0).getDate14().getFullName() : null : null);
			this.date15 = new DateBouclageDTO(currentUser, bouclage.getDate15(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate15() != null ? this.auteursEtapeBouclage.get(0).getDate15().getFullName() : null : null);
			this.date16 = new DateBouclageDTO(currentUser, bouclage.getDate16(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate16() != null ? this.auteursEtapeBouclage.get(0).getDate16().getFullName() : null : null);
			this.date17 = new DateBouclageDTO(currentUser, bouclage.getDate17(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate17() != null ? this.auteursEtapeBouclage.get(0).getDate17().getFullName() : null : null);
			this.date18 = new DateBouclageDTO(currentUser, bouclage.getDate18(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate18() != null ? this.auteursEtapeBouclage.get(0).getDate18().getFullName() : null : null);
			this.date19 = new DateBouclageDTO(currentUser, bouclage.getDate19(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate19() != null ? this.auteursEtapeBouclage.get(0).getDate19().getFullName() : null : null);
			this.date20 = new DateBouclageDTO(currentUser, bouclage.getDate20(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate20() != null ? this.auteursEtapeBouclage.get(0).getDate20().getFullName() : null : null);
			this.date21 = new DateBouclageDTO(currentUser, bouclage.getDate21(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate21() != null ? this.auteursEtapeBouclage.get(0).getDate21().getFullName() : null : null);
			this.date22 = new DateBouclageDTO(currentUser, bouclage.getDate22(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate22() != null ? this.auteursEtapeBouclage.get(0).getDate22().getFullName() : null : null);
			this.date23 = new DateBouclageDTO(currentUser, bouclage.getDate23(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate23() != null ? this.auteursEtapeBouclage.get(0).getDate23().getFullName() : null : null);
			this.date24 = new DateBouclageDTO(currentUser, bouclage.getDate24(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate24() != null ? this.auteursEtapeBouclage.get(0).getDate24().getFullName() : null : null);
			this.date25 = new DateBouclageDTO(currentUser, bouclage.getDate25(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate25() != null ? this.auteursEtapeBouclage.get(0).getDate25().getFullName() : null : null);
			this.date26 = new DateBouclageDTO(currentUser, bouclage.getDate26(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate26() != null ? this.auteursEtapeBouclage.get(0).getDate26().getFullName() : null : null);
			this.date27 = new DateBouclageDTO(currentUser, bouclage.getDate27(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate27() != null ? this.auteursEtapeBouclage.get(0).getDate27().getFullName() : null : null);
			this.date28 = new DateBouclageDTO(currentUser, bouclage.getDate28(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate28() != null ? this.auteursEtapeBouclage.get(0).getDate28().getFullName() : null : null);
			this.date29 = new DateBouclageDTO(currentUser, bouclage.getDate29(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate29() != null ? this.auteursEtapeBouclage.get(0).getDate29().getFullName() : null : null);
			this.date30 = new DateBouclageDTO(currentUser, bouclage.getDate30(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate30() != null ? this.auteursEtapeBouclage.get(0).getDate30().getFullName() : null : null);
			this.date31 = new DateBouclageDTO(currentUser, bouclage.getDate31(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate31() != null ? this.auteursEtapeBouclage.get(0).getDate31().getFullName() : null : null);
			this.date32 = new DateBouclageDTO(currentUser, bouclage.getDate32(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate32() != null ? this.auteursEtapeBouclage.get(0).getDate32().getFullName() : null : null);
			this.date33 = new DateBouclageDTO(currentUser, bouclage.getDate33(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate33() != null ? this.auteursEtapeBouclage.get(0).getDate33().getFullName() : null : null);
			this.date34 = new DateBouclageDTO(currentUser, bouclage.getDate34(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate34() != null ? this.auteursEtapeBouclage.get(0).getDate34().getFullName() : null : null);
			this.date35 = new DateBouclageDTO(currentUser, bouclage.getDate35(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate35() != null ? this.auteursEtapeBouclage.get(0).getDate35().getFullName() : null : null);
			this.date36 = new DateBouclageDTO(currentUser, bouclage.getDate36(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate36() != null ? this.auteursEtapeBouclage.get(0).getDate36().getFullName() : null : null);
			this.date37 = new DateBouclageDTO(currentUser, bouclage.getDate37(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate37() != null ? this.auteursEtapeBouclage.get(0).getDate37().getFullName() : null : null);
			this.date38 = new DateBouclageDTO(currentUser, bouclage.getDate38(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate38() != null ? this.auteursEtapeBouclage.get(0).getDate38().getFullName() : null : null);
			this.date39 = new DateBouclageDTO(currentUser, bouclage.getDate39(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate39() != null ? this.auteursEtapeBouclage.get(0).getDate39().getFullName() : null : null);
			this.date40 = new DateBouclageDTO(currentUser, bouclage.getDate40(), null, testUtilisateurBouclage(this.auteursEtapeBouclage) ? this.auteursEtapeBouclage.get(0).getDate40() != null ? this.auteursEtapeBouclage.get(0).getDate40().getFullName() : null : null);
			this.incidentDate = new DateBouclageDTO(currentUser, bouclage.getIncidentDate(), null, null);
			this.commentaires = bouclage.getCommentaires();
			this.coteConfortement0 = bouclage.getCoteConfortement0();
			this.coteConfortement1 = bouclage.getCoteConfortement1();
			this.coteConfortement2 = bouclage.getCoteConfortement2();
			this.coteCurage0 = bouclage.getCoteCurage0();
			this.coteCurage1 = bouclage.getCoteCurage1();
			this.coteCurage2 = bouclage.getCoteCurage2();
			this.coteDebroussaillage0 = bouclage.getCoteDebroussaillage0();
			this.coteDebroussaillage1 = bouclage.getCoteDebroussaillage1();
			this.coteDebroussaillage2 = bouclage.getCoteDebroussaillage2();
			this.coteDivers0 = bouclage.getCoteDivers0();
			this.coteDivers1 = bouclage.getCoteDivers1();
			this.coteDivers2 = bouclage.getCoteDivers2();
			this.coteInstrumentation0 = bouclage.getCoteInstrumentation0();
			this.coteInstrumentation1 = bouclage.getCoteInstrumentation1();
			this.coteInstrumentation2 = bouclage.getCoteInstrumentation2();
			this.cotePurge0 = bouclage.getCotePurge0();
			this.cotePurge1 = bouclage.getCotePurge1();
			this.cotePurge2 = bouclage.getCotePurge2();
			this.coteTerrassement0 = bouclage.getCoteTerrassement0();
			this.coteTerrassement1 = bouclage.getCoteTerrassement1();
			this.coteTerrassement2 = bouclage.getCoteTerrassement2();
			this.coteTopographie0 = bouclage.getCoteTopographie0();
			this.coteTopographie1 = bouclage.getCoteTopographie1();
			this.coteTopographie2 = bouclage.getCoteTopographie2();
			this.date_maj = bouclage.getDate_maj();
			this.ecr1_pkd = bouclage.getEcr1_pkd();
			this.ecr1_pkf = bouclage.getEcr1_pkf();
			this.ecr2_pkd = bouclage.getEcr2_pkd();
			this.ecr2_pkf = bouclage.getEcr2_pkf();
			this.ecr3_pkd = bouclage.getEcr3_pkd();
			this.ecr3_pkf = bouclage.getEcr3_pkf();
			this.ecr4_pkd = bouclage.getEcr4_pkd();
			this.ecr4_pkf = bouclage.getEcr4_pkf();
			this.ecr5_pkd = bouclage.getEcr5_pkd();
			this.ecr5_pkf = bouclage.getEcr5_pkf();
			this.ecr6_pkd = bouclage.getEcr6_pkd();
			this.ecr6_pkf = bouclage.getEcr6_pkf();
			this.etatOuvrage = bouclage.getEtatOuvrage();
			this.ficheChuteBloc = bouclage.getFicheChuteBloc();
			this.incidentDescription = bouclage.getIncidentDescription();
			this.incidentImportanceKey = bouclage.getIncidentImportanceKey();
			this.laborAcceptation0Key = bouclage.getLaborAcceptation0Key();
			this.laborAcceptation1Key = bouclage.getLaborAcceptation1Key();
			this.laborAcceptation2Key = bouclage.getLaborAcceptation2Key();
			this.laborAnalyseRisque0 = bouclage.getLaborAnalyseRisque0();
			this.laborAnalyseRisque1 = bouclage.getLaborAnalyseRisque1();
			this.laborAnalyseRisque2 = bouclage.getLaborAnalyseRisque2();
			this.laborMesuresConservatoire0 = bouclage.getLaborMesuresConservatoire0();
			this.laborMesuresConservatoire1 = bouclage.getLaborMesuresConservatoire1();
			this.laborMesuresConservatoire2 = bouclage.getLaborMesuresConservatoire2();
			this.laborTodoAgent0 = bouclage.getLaborTodoAgent0();
			this.laborTodoAgent1 = bouclage.getLaborTodoAgent1();
			this.laborTodoAgent2 = bouclage.getLaborTodoAgent2();
			this.laborTodoSupervisor0 = new PropositionBouclageDTO(bouclage.getLaborTodoSupervisor0(), getPropositionBouclageStatusEnum(bouclage.getLaborAcceptation0Key()));
			this.laborTodoSupervisor1 = new PropositionBouclageDTO(bouclage.getLaborTodoSupervisor1(), getPropositionBouclageStatusEnum(bouclage.getLaborAcceptation1Key()));
			this.laborTodoSupervisor2 = new PropositionBouclageDTO(bouclage.getLaborTodoSupervisor2(), getPropositionBouclageStatusEnum(bouclage.getLaborAcceptation2Key()));
			this.motifCV = bouclage.getMotifCV();
			this.motifControle = bouclage.getMotifControle();
			this.numeroFicheRex = bouclage.getNumeroFicheRex();
			this.solutionCV = bouclage.getSolutionCV();
			this.typeSurvCompl = bouclage.getTypeSurvCompl();
			this.u0_typeExpertise = bouclage.getU0_typeExpertise();
			this.u1_typeExpertise = bouclage.getU1_typeExpertise();
			this.u2_typeExpertise = bouclage.getU2_typeExpertise();
			this.surveillance = SurveillanceDTO.toDTO(bouclage.getSurveillance(), "listeBouclages");
			
			this.auteurMaj = UtilisateurDTO.toDTO(bouclage.getAuteurMaj(), true);
			this.author = UtilisateurDTO.toDTO(bouclage.getAuthor(), true);
			this.authorSurveillance = UtilisateurDTO.toDTO(bouclage.getAuthorSurveillance(), true);
			this.delaiTravauxAgent0 = bouclage.getDelaiTravauxAgent0();
			this.delaiTravauxAgent1 = bouclage.getDelaiTravauxAgent1();
			this.delaiTravauxExpert0 = bouclage.getDelaiTravauxExpert0();
			this.delaiTravauxExpert1 = bouclage.getDelaiTravauxExpert1();
			this.operation = bouclage.getOperation();
			this.etatCV = bouclage.getEtatCV();
			this.motifAnnulationCV = bouclage.getMotifAnnulationCV();
			this.statut = bouclage.getStatut();
			this.auteurSuppression = bouclage.getAuteurSuppression();
			this.dateSuppression = new DateBouclageDTO(currentUser, bouclage.getDateSuppression(), null, null);
			
			switch (page) {
			case "listeBouclages":
				this.typeKey = ConvertData.getTypeVisiteMap().get(bouclage.getTypeKey()).getLibelleSimplifie();
				this.u0_expertise = ConvertData.convertBooleanToString(bouclage.getU0_expertise());
				this.u1_expertise = ConvertData.convertBooleanToString(bouclage.getU1_expertise());
				this.u2_expertise = ConvertData.convertBooleanToString(bouclage.getU2_expertise());
				this.u0_etude = ConvertData.convertBooleanToString(bouclage.getU0_etude());
				this.u1_etude = ConvertData.convertBooleanToString(bouclage.getU1_etude());
				this.u2_etude = ConvertData.convertBooleanToString(bouclage.getU2_etude());
				break;
			case "ficheBouclage":
				this.typeKey = bouclage.getTypeKey().toString();
				break;
			default:
				this.typeKey = bouclage.getTypeKey().toString();
				break;
			}
			
			if (
				this.envoiSt0 != null ||
				this.hasCourriers0 == true ||
				this.u0Integrite == true ||
				this.u0SecuritePersonne == true ||
				this.u0SecuriteTiers == true ||
				this.u0_etude != null ||
				this.u0_expertise != null ||
				this.propositionTravauxU0 != null ||
				this.costConfortement0 != null ||
				this.costCurage0 != null ||
				this.costDebroussaillage0 != null ||
				this.costDivers0 != null ||
				this.costInstrumentation0 != null ||
				this.costMaconnerie0 != null ||
				this.costPeinture0 != null ||
				this.costPurge0 != null ||
				this.costSerrurerie0 != null ||
				this.costTerrassement0 != null ||
				this.costTopographie0 != null ||
				this.laborCostAgent0 != null ||
				this.laborCostSupervisor0 != null ||
				this.coteConfortement0 != null ||
				this.coteCurage0 != null ||
				this.coteDebroussaillage0 != null ||
				this.coteDivers0 != null ||
				this.coteInstrumentation0 != null ||
				this.cotePurge0 != null ||
				this.coteTerrassement0 != null ||
				this.coteTopographie0 != null ||
				this.laborAcceptation0Key != null ||
				this.laborAnalyseRisque0 != null ||
				this.laborMesuresConservatoire0 != null ||
				this.laborTodoAgent0 != null ||
				this.laborTodoSupervisor0.getProposition() != null ||
				this.u0_typeExpertise != null ||
				this.delaiTravauxAgent0 != null ||
				this.delaiTravauxExpert0 != null
			) {
				this.propositionTravauxU0 = true;
			} else {
				this.propositionTravauxU0 = false;
			}
			
			if (
				this.envoiSt1 != null ||
				this.hasCourriers1 == true ||
				this.u1Integrite == true ||
				this.u1SecuritePersonne == true ||
				this.u1SecuriteTiers == true ||
				this.u1_etude != null ||
				this.u1_expertise != null ||
				this.propositionTravauxU1 != null ||
				this.costConfortement1 != null ||
				this.costCurage1 != null ||
				this.costDebroussaillage1 != null ||
				this.costDivers1 != null ||
				this.costInstrumentation1 != null ||
				this.costMaconnerie1 != null ||
				this.costPeinture1 != null ||
				this.costPurge1 != null ||
				this.costSerrurerie1 != null ||
				this.costTerrassement1 != null ||
				this.costTopographie1 != null ||
				this.laborCostAgent1 != null ||
				this.laborCostSupervisor1 != null ||
				this.coteConfortement1 != null ||
				this.coteCurage1 != null ||
				this.coteDebroussaillage1 != null ||
				this.coteDivers1 != null ||
				this.coteInstrumentation1 != null ||
				this.cotePurge1 != null ||
				this.coteTerrassement1 != null ||
				this.coteTopographie1 != null ||
				this.laborAcceptation1Key != null ||
				this.laborAnalyseRisque1 != null ||
				this.laborMesuresConservatoire1 != null ||
				this.laborTodoAgent1 != null ||
				this.laborTodoSupervisor1.getProposition() != null ||
				this.u1_typeExpertise != null ||
				this.delaiTravauxAgent1 != null ||
				this.delaiTravauxExpert1 != null
			) {
				this.propositionTravauxU1 = true;
			} else {
				this.propositionTravauxU1 = false;
			}
			
			if (
				this.envoiSt2 != null ||
				this.hasCourriers2 == true ||
				this.u2Integrite == true ||
				this.u2SecuritePersonne == true ||
				this.u2SecuriteTiers == true ||
				this.u2_etude != null ||
				this.u2_expertise != null ||
				this.propositionTravauxU2 != null ||
				this.costConfortement2 != null ||
				this.costCurage2 != null ||
				this.costDebroussaillage2 != null ||
				this.costDivers2 != null ||
				this.costInstrumentation2 != null ||
				this.costMaconnerie2 != null ||
				this.costPeinture2 != null ||
				this.costPurge2 != null ||
				this.costSerrurerie2 != null ||
				this.costTerrassement2 != null ||
				this.costTopographie2 != null ||
				this.laborCostAgent2 != null ||
				this.laborCostSupervisor2 != null ||
				this.coteConfortement2 != null ||
				this.coteCurage2 != null ||
				this.coteDebroussaillage2 != null ||
				this.coteDivers2 != null ||
				this.coteInstrumentation2 != null ||
				this.cotePurge2 != null ||
				this.coteTerrassement2 != null ||
				this.coteTopographie2 != null ||
				this.laborAcceptation2Key != null ||
				this.laborAnalyseRisque2 != null ||
				this.laborMesuresConservatoire2 != null ||
				this.laborTodoAgent2 != null ||
				this.laborTodoSupervisor2.getProposition() != null ||
				this.u2_typeExpertise != null
			) {
				this.propositionTravauxU2 = true;
			} else {
				this.propositionTravauxU2 = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public String getPropositionBouclageStatusEnum(String acceptationKey) {
		if (acceptationKey != null) {
			switch (acceptationKey) {
			case "lab_vil_negative":
				return PropositionBouclageStatusEnum.REJECTED.toString();
			case "lab_vil_positive":
				return PropositionBouclageStatusEnum.APPROVED.toString();
			case "lab_vil_unvalidated":
				return PropositionBouclageStatusEnum.PENDING.toString();
			default:
				return PropositionBouclageStatusEnum.PENDING.toString();
			}
		}else {
			return PropositionBouclageStatusEnum.PENDING.toString();
		}
	}
	
	public Integer diffDate00And01(Bouclage bouclage) {
		if (bouclage.getDate00() != null && bouclage.getDate01() != null) {
			return (int) ChronoUnit.DAYS.between(bouclage.getDate00(), bouclage.getDate01());
		}else {
			return null;
		}
	}
	
	public Integer diffDate00And02(Bouclage bouclage) {
		if (bouclage.getDate00() != null && bouclage.getDate02() != null) {
			return (int) ChronoUnit.DAYS.between(bouclage.getDate00(), bouclage.getDate02());
		}else {
			return null;
		}
	}
	
	public Integer diffDate00And04(Bouclage bouclage) {
		if (bouclage.getDate00() != null && bouclage.getDate04() != null) {
			return (int) ChronoUnit.DAYS.between(bouclage.getDate00(), bouclage.getDate04());
		}else {
			return null;
		}
	}
	
	public Boolean testUtilisateurBouclage(List<BouclageUtilisateur> auteursEtapeBouclage) {
		if (auteursEtapeBouclage != null && auteursEtapeBouclage.size() > 0) {
			return true;
		}else {
			return false;
		}
	}

	public static BouclageDTO toDTO(Bouclage bouclage, String page, UserPrincipal currentUser) {
		if (bouclage == null) {
			return null;
		} else {
			return new BouclageDTO(bouclage, page, currentUser);
		}
	}

	public static List<BouclageDTO> toDTOs(List<Bouclage> bouclages, String page, @CurrentUser UserPrincipal currentUser) {
		if (bouclages == null) {
			return new ArrayList<>(0);
		}
		List<BouclageDTO> bouclageDTOs = new ArrayList<>(bouclages.size());
		for (Bouclage bouclage : bouclages) {
			bouclageDTOs.add(BouclageDTO.toDTO(bouclage, page, currentUser));
		}
		return bouclageDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getEcr1() {
		return ecr1;
	}

	public void setEcr1(Boolean ecr1) {
		this.ecr1 = ecr1;
	}

	public Boolean getEcr2() {
		return ecr2;
	}

	public void setEcr2(Boolean ecr2) {
		this.ecr2 = ecr2;
	}

	public Boolean getEcr3() {
		return ecr3;
	}

	public void setEcr3(Boolean ecr3) {
		this.ecr3 = ecr3;
	}

	public Boolean getEcr4() {
		return ecr4;
	}

	public void setEcr4(Boolean ecr4) {
		this.ecr4 = ecr4;
	}

	public Boolean getEcr5() {
		return ecr5;
	}

	public void setEcr5(Boolean ecr5) {
		this.ecr5 = ecr5;
	}

	public Boolean getEcr6() {
		return ecr6;
	}

	public void setEcr6(Boolean ecr6) {
		this.ecr6 = ecr6;
	}

	public Boolean getEnvoiSt0() {
		return envoiSt0;
	}

	public void setEnvoiSt0(Boolean envoiSt0) {
		this.envoiSt0 = envoiSt0;
	}

	public Boolean getEnvoiSt1() {
		return envoiSt1;
	}

	public void setEnvoiSt1(Boolean envoiSt1) {
		this.envoiSt1 = envoiSt1;
	}

	public Boolean getEnvoiSt2() {
		return envoiSt2;
	}

	public void setEnvoiSt2(Boolean envoiSt2) {
		this.envoiSt2 = envoiSt2;
	}

	public Boolean getHasCourriers0() {
		return hasCourriers0;
	}

	public void setHasCourriers0(Boolean hasCourriers0) {
		this.hasCourriers0 = hasCourriers0;
	}

	public Boolean getHasCourriers1() {
		return hasCourriers1;
	}

	public void setHasCourriers1(Boolean hasCourriers1) {
		this.hasCourriers1 = hasCourriers1;
	}

	public Boolean getHasCourriers2() {
		return hasCourriers2;
	}

	public void setHasCourriers2(Boolean hasCourriers2) {
		this.hasCourriers2 = hasCourriers2;
	}

	public Boolean getIncident() {
		return incident;
	}

	public void setIncident(Boolean incident) {
		this.incident = incident;
	}

	public Boolean getU0Integrite() {
		return u0Integrite;
	}

	public void setU0Integrite(Boolean u0Integrite) {
		this.u0Integrite = u0Integrite;
	}

	public Boolean getU0SecuritePersonne() {
		return u0SecuritePersonne;
	}

	public void setU0SecuritePersonne(Boolean u0SecuritePersonne) {
		this.u0SecuritePersonne = u0SecuritePersonne;
	}

	public Boolean getU0SecuriteTiers() {
		return u0SecuriteTiers;
	}

	public void setU0SecuriteTiers(Boolean u0SecuriteTiers) {
		this.u0SecuriteTiers = u0SecuriteTiers;
	}

	

	public Boolean getU1Integrite() {
		return u1Integrite;
	}

	public void setU1Integrite(Boolean u1Integrite) {
		this.u1Integrite = u1Integrite;
	}

	public Boolean getU1SecuritePersonne() {
		return u1SecuritePersonne;
	}

	public void setU1SecuritePersonne(Boolean u1SecuritePersonne) {
		this.u1SecuritePersonne = u1SecuritePersonne;
	}

	public Boolean getU1SecuriteTiers() {
		return u1SecuriteTiers;
	}

	public void setU1SecuriteTiers(Boolean u1SecuriteTiers) {
		this.u1SecuriteTiers = u1SecuriteTiers;
	}

	public Boolean getU2Integrite() {
		return u2Integrite;
	}

	public void setU2Integrite(Boolean u2Integrite) {
		this.u2Integrite = u2Integrite;
	}

	public Boolean getU2SecuritePersonne() {
		return u2SecuritePersonne;
	}

	public void setU2SecuritePersonne(Boolean u2SecuritePersonne) {
		this.u2SecuritePersonne = u2SecuritePersonne;
	}

	public Boolean getU2SecuriteTiers() {
		return u2SecuriteTiers;
	}

	public void setU2SecuriteTiers(Boolean u2SecuriteTiers) {
		this.u2SecuriteTiers = u2SecuriteTiers;
	}

	public Boolean getPropositionTravauxU0() {
		return propositionTravauxU0;
	}

	public void setPropositionTravauxU0(Boolean propositionTravauxU0) {
		this.propositionTravauxU0 = propositionTravauxU0;
	}

	public Boolean getPropositionTravauxU1() {
		return propositionTravauxU1;
	}

	public void setPropositionTravauxU1(Boolean propositionTravauxU1) {
		this.propositionTravauxU1 = propositionTravauxU1;
	}

	public Boolean getPropositionTravauxU2() {
		return propositionTravauxU2;
	}

	public void setPropositionTravauxU2(Boolean propositionTravauxU2) {
		this.propositionTravauxU2 = propositionTravauxU2;
	}

	public Double getNoteAppui() {
		return noteAppui;
	}

	public void setNoteAppui(Double noteAppui) {
		this.noteAppui = noteAppui;
	}

	public Double getNoteAppuiPrec() {
		return noteAppuiPrec;
	}

	public void setNoteAppuiPrec(Double noteAppuiPrec) {
		this.noteAppuiPrec = noteAppuiPrec;
	}

	public Double getNoteGlobale() {
		return noteGlobale;
	}

	public void setNoteGlobale(Double noteGlobale) {
		this.noteGlobale = noteGlobale;
	}

	public Double getNoteGlobalePrec() {
		return noteGlobalePrec;
	}

	public void setNoteGlobalePrec(Double noteGlobalePrec) {
		this.noteGlobalePrec = noteGlobalePrec;
	}

	public Double getNoteMax() {
		return noteMax;
	}

	public void setNoteMax(Double noteMax) {
		this.noteMax = noteMax;
	}

	public Double getNoteMin() {
		return noteMin;
	}

	public void setNoteMin(Double noteMin) {
		this.noteMin = noteMin;
	}

	public Double getNoteTablier() {
		return noteTablier;
	}

	public void setNoteTablier(Double noteTablier) {
		this.noteTablier = noteTablier;
	}

	public Double getNoteTablierPrec() {
		return noteTablierPrec;
	}

	public void setNoteTablierPrec(Double noteTablierPrec) {
		this.noteTablierPrec = noteTablierPrec;
	}

	public Integer getCostConfortement0() {
		return costConfortement0;
	}

	public void setCostConfortement0(Integer costConfortement0) {
		this.costConfortement0 = costConfortement0;
	}

	public Integer getCostConfortement1() {
		return costConfortement1;
	}

	public void setCostConfortement1(Integer costConfortement1) {
		this.costConfortement1 = costConfortement1;
	}

	public Integer getCostConfortement2() {
		return costConfortement2;
	}

	public void setCostConfortement2(Integer costConfortement2) {
		this.costConfortement2 = costConfortement2;
	}

	public Integer getCostCurage0() {
		return costCurage0;
	}

	public void setCostCurage0(Integer costCurage0) {
		this.costCurage0 = costCurage0;
	}

	public Integer getCostCurage1() {
		return costCurage1;
	}

	public void setCostCurage1(Integer costCurage1) {
		this.costCurage1 = costCurage1;
	}

	public Integer getCostCurage2() {
		return costCurage2;
	}

	public void setCostCurage2(Integer costCurage2) {
		this.costCurage2 = costCurage2;
	}

	public Integer getCostDebroussaillage0() {
		return costDebroussaillage0;
	}

	public void setCostDebroussaillage0(Integer costDebroussaillage0) {
		this.costDebroussaillage0 = costDebroussaillage0;
	}

	public Integer getCostDebroussaillage1() {
		return costDebroussaillage1;
	}

	public void setCostDebroussaillage1(Integer costDebroussaillage1) {
		this.costDebroussaillage1 = costDebroussaillage1;
	}

	public Integer getCostDebroussaillage2() {
		return costDebroussaillage2;
	}

	public void setCostDebroussaillage2(Integer costDebroussaillage2) {
		this.costDebroussaillage2 = costDebroussaillage2;
	}

	public Integer getCostDivers0() {
		return costDivers0;
	}

	public void setCostDivers0(Integer costDivers0) {
		this.costDivers0 = costDivers0;
	}

	public Integer getCostDivers1() {
		return costDivers1;
	}

	public void setCostDivers1(Integer costDivers1) {
		this.costDivers1 = costDivers1;
	}

	public Integer getCostDivers2() {
		return costDivers2;
	}

	public void setCostDivers2(Integer costDivers2) {
		this.costDivers2 = costDivers2;
	}

	public Integer getCostInstrumentation0() {
		return costInstrumentation0;
	}

	public void setCostInstrumentation0(Integer costInstrumentation0) {
		this.costInstrumentation0 = costInstrumentation0;
	}

	public Integer getCostInstrumentation1() {
		return costInstrumentation1;
	}

	public void setCostInstrumentation1(Integer costInstrumentation1) {
		this.costInstrumentation1 = costInstrumentation1;
	}

	public Integer getCostInstrumentation2() {
		return costInstrumentation2;
	}

	public void setCostInstrumentation2(Integer costInstrumentation2) {
		this.costInstrumentation2 = costInstrumentation2;
	}

	public Integer getCostMaconnerie0() {
		return costMaconnerie0;
	}

	public void setCostMaconnerie0(Integer costMaconnerie0) {
		this.costMaconnerie0 = costMaconnerie0;
	}

	public Integer getCostMaconnerie1() {
		return costMaconnerie1;
	}

	public void setCostMaconnerie1(Integer costMaconnerie1) {
		this.costMaconnerie1 = costMaconnerie1;
	}

	public Integer getCostMaconnerie2() {
		return costMaconnerie2;
	}

	public void setCostMaconnerie2(Integer costMaconnerie2) {
		this.costMaconnerie2 = costMaconnerie2;
	}

	public Integer getCostPeinture0() {
		return costPeinture0;
	}

	public void setCostPeinture0(Integer costPeinture0) {
		this.costPeinture0 = costPeinture0;
	}

	public Integer getCostPeinture1() {
		return costPeinture1;
	}

	public void setCostPeinture1(Integer costPeinture1) {
		this.costPeinture1 = costPeinture1;
	}

	public Integer getCostPeinture2() {
		return costPeinture2;
	}

	public void setCostPeinture2(Integer costPeinture2) {
		this.costPeinture2 = costPeinture2;
	}

	public Integer getCostPurge0() {
		return costPurge0;
	}

	public void setCostPurge0(Integer costPurge0) {
		this.costPurge0 = costPurge0;
	}

	public Integer getCostPurge1() {
		return costPurge1;
	}

	public void setCostPurge1(Integer costPurge1) {
		this.costPurge1 = costPurge1;
	}

	public Integer getCostPurge2() {
		return costPurge2;
	}

	public void setCostPurge2(Integer costPurge2) {
		this.costPurge2 = costPurge2;
	}

	public Integer getCostSerrurerie0() {
		return costSerrurerie0;
	}

	public void setCostSerrurerie0(Integer costSerrurerie0) {
		this.costSerrurerie0 = costSerrurerie0;
	}

	public Integer getCostSerrurerie1() {
		return costSerrurerie1;
	}

	public void setCostSerrurerie1(Integer costSerrurerie1) {
		this.costSerrurerie1 = costSerrurerie1;
	}

	public Integer getCostSerrurerie2() {
		return costSerrurerie2;
	}

	public void setCostSerrurerie2(Integer costSerrurerie2) {
		this.costSerrurerie2 = costSerrurerie2;
	}

	public Integer getCostTerrassement0() {
		return costTerrassement0;
	}

	public void setCostTerrassement0(Integer costTerrassement0) {
		this.costTerrassement0 = costTerrassement0;
	}

	public Integer getCostTerrassement1() {
		return costTerrassement1;
	}

	public void setCostTerrassement1(Integer costTerrassement1) {
		this.costTerrassement1 = costTerrassement1;
	}

	public Integer getCostTerrassement2() {
		return costTerrassement2;
	}

	public void setCostTerrassement2(Integer costTerrassement2) {
		this.costTerrassement2 = costTerrassement2;
	}

	public Integer getCostTopographie0() {
		return costTopographie0;
	}

	public void setCostTopographie0(Integer costTopographie0) {
		this.costTopographie0 = costTopographie0;
	}

	public Integer getCostTopographie1() {
		return costTopographie1;
	}

	public void setCostTopographie1(Integer costTopographie1) {
		this.costTopographie1 = costTopographie1;
	}

	public Integer getCostTopographie2() {
		return costTopographie2;
	}

	public void setCostTopographie2(Integer costTopographie2) {
		this.costTopographie2 = costTopographie2;
	}

	public Integer getLaborCostAgent0() {
		return laborCostAgent0;
	}

	public void setLaborCostAgent0(Integer laborCostAgent0) {
		this.laborCostAgent0 = laborCostAgent0;
	}

	public Integer getLaborCostAgent1() {
		return laborCostAgent1;
	}

	public void setLaborCostAgent1(Integer laborCostAgent1) {
		this.laborCostAgent1 = laborCostAgent1;
	}

	public Integer getLaborCostAgent2() {
		return laborCostAgent2;
	}

	public void setLaborCostAgent2(Integer laborCostAgent2) {
		this.laborCostAgent2 = laborCostAgent2;
	}

	public Integer getLaborCostSupervisor0() {
		return laborCostSupervisor0;
	}

	public void setLaborCostSupervisor0(Integer laborCostSupervisor0) {
		this.laborCostSupervisor0 = laborCostSupervisor0;
	}

	public Integer getLaborCostSupervisor1() {
		return laborCostSupervisor1;
	}

	public void setLaborCostSupervisor1(Integer laborCostSupervisor1) {
		this.laborCostSupervisor1 = laborCostSupervisor1;
	}

	public Integer getLaborCostSupervisor2() {
		return laborCostSupervisor2;
	}

	public void setLaborCostSupervisor2(Integer laborCostSupervisor2) {
		this.laborCostSupervisor2 = laborCostSupervisor2;
	}

	public Integer getPrevisionDate() {
		return previsionDate;
	}

	public void setPrevisionDate(Integer previsionDate) {
		this.previsionDate = previsionDate;
	}

	public DateBouclageDTO getDate00() {
		return date00;
	}

	public void setDate00(DateBouclageDTO date00) {
		this.date00 = date00;
	}

	public DateBouclageDTO getDate01() {
		return date01;
	}

	public void setDate01(DateBouclageDTO date01) {
		this.date01 = date01;
	}

	public DateBouclageDTO getDate02() {
		return date02;
	}

	public void setDate02(DateBouclageDTO date02) {
		this.date02 = date02;
	}

	public DateBouclageDTO getDate03() {
		return date03;
	}

	public void setDate03(DateBouclageDTO date03) {
		this.date03 = date03;
	}

	public DateBouclageDTO getDate04() {
		return date04;
	}

	public void setDate04(DateBouclageDTO date04) {
		this.date04 = date04;
	}

	public DateBouclageDTO getDate05() {
		return date05;
	}

	public void setDate05(DateBouclageDTO date05) {
		this.date05 = date05;
	}

	public DateBouclageDTO getDate06() {
		return date06;
	}

	public void setDate06(DateBouclageDTO date06) {
		this.date06 = date06;
	}

	public DateBouclageDTO getDate07() {
		return date07;
	}

	public void setDate07(DateBouclageDTO date07) {
		this.date07 = date07;
	}

	public DateBouclageDTO getDate08() {
		return date08;
	}

	public void setDate08(DateBouclageDTO date08) {
		this.date08 = date08;
	}

	public DateBouclageDTO getDate09() {
		return date09;
	}

	public void setDate09(DateBouclageDTO date09) {
		this.date09 = date09;
	}

	public DateBouclageDTO getDate10() {
		return date10;
	}

	public void setDate10(DateBouclageDTO date10) {
		this.date10 = date10;
	}

	public DateBouclageDTO getDate11() {
		return date11;
	}

	public void setDate11(DateBouclageDTO date11) {
		this.date11 = date11;
	}

	public DateBouclageDTO getDate12() {
		return date12;
	}

	public void setDate12(DateBouclageDTO date12) {
		this.date12 = date12;
	}

	public DateBouclageDTO getDate13() {
		return date13;
	}

	public void setDate13(DateBouclageDTO date13) {
		this.date13 = date13;
	}

	public DateBouclageDTO getDate14() {
		return date14;
	}

	public void setDate14(DateBouclageDTO date14) {
		this.date14 = date14;
	}

	public DateBouclageDTO getDate15() {
		return date15;
	}

	public void setDate15(DateBouclageDTO date15) {
		this.date15 = date15;
	}

	public DateBouclageDTO getDate16() {
		return date16;
	}

	public void setDate16(DateBouclageDTO date16) {
		this.date16 = date16;
	}

	public DateBouclageDTO getDate17() {
		return date17;
	}

	public void setDate17(DateBouclageDTO date17) {
		this.date17 = date17;
	}

	public DateBouclageDTO getDate18() {
		return date18;
	}

	public void setDate18(DateBouclageDTO date18) {
		this.date18 = date18;
	}

	public DateBouclageDTO getDate19() {
		return date19;
	}

	public void setDate19(DateBouclageDTO date19) {
		this.date19 = date19;
	}

	public DateBouclageDTO getDate20() {
		return date20;
	}

	public void setDate20(DateBouclageDTO date20) {
		this.date20 = date20;
	}

	public DateBouclageDTO getDate21() {
		return date21;
	}

	public void setDate21(DateBouclageDTO date21) {
		this.date21 = date21;
	}

	public DateBouclageDTO getDate22() {
		return date22;
	}

	public void setDate22(DateBouclageDTO date22) {
		this.date22 = date22;
	}

	public DateBouclageDTO getDate23() {
		return date23;
	}

	public void setDate23(DateBouclageDTO date23) {
		this.date23 = date23;
	}

	public DateBouclageDTO getDate24() {
		return date24;
	}

	public void setDate24(DateBouclageDTO date24) {
		this.date24 = date24;
	}

	public DateBouclageDTO getDate25() {
		return date25;
	}

	public void setDate25(DateBouclageDTO date25) {
		this.date25 = date25;
	}

	public DateBouclageDTO getDate26() {
		return date26;
	}

	public void setDate26(DateBouclageDTO date26) {
		this.date26 = date26;
	}

	public DateBouclageDTO getDate27() {
		return date27;
	}

	public void setDate27(DateBouclageDTO date27) {
		this.date27 = date27;
	}

	public DateBouclageDTO getDate28() {
		return date28;
	}

	public void setDate28(DateBouclageDTO date28) {
		this.date28 = date28;
	}

	public DateBouclageDTO getDate29() {
		return date29;
	}

	public void setDate29(DateBouclageDTO date29) {
		this.date29 = date29;
	}

	public DateBouclageDTO getDate30() {
		return date30;
	}

	public void setDate30(DateBouclageDTO date30) {
		this.date30 = date30;
	}

	public DateBouclageDTO getDate31() {
		return date31;
	}

	public void setDate31(DateBouclageDTO date31) {
		this.date31 = date31;
	}

	public DateBouclageDTO getDate32() {
		return date32;
	}

	public void setDate32(DateBouclageDTO date32) {
		this.date32 = date32;
	}

	public DateBouclageDTO getDate33() {
		return date33;
	}

	public void setDate33(DateBouclageDTO date33) {
		this.date33 = date33;
	}

	public DateBouclageDTO getDate34() {
		return date34;
	}

	public void setDate34(DateBouclageDTO date34) {
		this.date34 = date34;
	}

	public DateBouclageDTO getDate35() {
		return date35;
	}

	public void setDate35(DateBouclageDTO date35) {
		this.date35 = date35;
	}

	public DateBouclageDTO getDate36() {
		return date36;
	}

	public void setDate36(DateBouclageDTO date36) {
		this.date36 = date36;
	}

	public DateBouclageDTO getDate37() {
		return date37;
	}

	public void setDate37(DateBouclageDTO date37) {
		this.date37 = date37;
	}

	public DateBouclageDTO getDate38() {
		return date38;
	}

	public void setDate38(DateBouclageDTO date38) {
		this.date38 = date38;
	}

	public DateBouclageDTO getDate39() {
		return date39;
	}

	public void setDate39(DateBouclageDTO date39) {
		this.date39 = date39;
	}

	public DateBouclageDTO getDate40() {
		return date40;
	}

	public void setDate40(DateBouclageDTO date40) {
		this.date40 = date40;
	}

	public DateBouclageDTO getIncidentDate() {
		return incidentDate;
	}

	public void setIncidentDate(DateBouclageDTO incidentDate) {
		this.incidentDate = incidentDate;
	}

	public String getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}

	public String getCoteConfortement0() {
		return coteConfortement0;
	}

	public void setCoteConfortement0(String coteConfortement0) {
		this.coteConfortement0 = coteConfortement0;
	}

	public String getCoteConfortement1() {
		return coteConfortement1;
	}

	public void setCoteConfortement1(String coteConfortement1) {
		this.coteConfortement1 = coteConfortement1;
	}

	public String getCoteConfortement2() {
		return coteConfortement2;
	}

	public void setCoteConfortement2(String coteConfortement2) {
		this.coteConfortement2 = coteConfortement2;
	}

	public String getCoteCurage0() {
		return coteCurage0;
	}

	public void setCoteCurage0(String coteCurage0) {
		this.coteCurage0 = coteCurage0;
	}

	public String getCoteCurage1() {
		return coteCurage1;
	}

	public void setCoteCurage1(String coteCurage1) {
		this.coteCurage1 = coteCurage1;
	}

	public String getCoteCurage2() {
		return coteCurage2;
	}

	public void setCoteCurage2(String coteCurage2) {
		this.coteCurage2 = coteCurage2;
	}

	public String getCoteDebroussaillage0() {
		return coteDebroussaillage0;
	}

	public void setCoteDebroussaillage0(String coteDebroussaillage0) {
		this.coteDebroussaillage0 = coteDebroussaillage0;
	}

	public String getCoteDebroussaillage1() {
		return coteDebroussaillage1;
	}

	public void setCoteDebroussaillage1(String coteDebroussaillage1) {
		this.coteDebroussaillage1 = coteDebroussaillage1;
	}

	public String getCoteDebroussaillage2() {
		return coteDebroussaillage2;
	}

	public void setCoteDebroussaillage2(String coteDebroussaillage2) {
		this.coteDebroussaillage2 = coteDebroussaillage2;
	}

	public String getCoteDivers0() {
		return coteDivers0;
	}

	public void setCoteDivers0(String coteDivers0) {
		this.coteDivers0 = coteDivers0;
	}

	public String getCoteDivers1() {
		return coteDivers1;
	}

	public void setCoteDivers1(String coteDivers1) {
		this.coteDivers1 = coteDivers1;
	}

	public String getCoteDivers2() {
		return coteDivers2;
	}

	public void setCoteDivers2(String coteDivers2) {
		this.coteDivers2 = coteDivers2;
	}

	public String getCoteInstrumentation0() {
		return coteInstrumentation0;
	}

	public void setCoteInstrumentation0(String coteInstrumentation0) {
		this.coteInstrumentation0 = coteInstrumentation0;
	}

	public String getCoteInstrumentation1() {
		return coteInstrumentation1;
	}

	public void setCoteInstrumentation1(String coteInstrumentation1) {
		this.coteInstrumentation1 = coteInstrumentation1;
	}

	public String getCoteInstrumentation2() {
		return coteInstrumentation2;
	}

	public void setCoteInstrumentation2(String coteInstrumentation2) {
		this.coteInstrumentation2 = coteInstrumentation2;
	}

	public String getCotePurge0() {
		return cotePurge0;
	}

	public void setCotePurge0(String cotePurge0) {
		this.cotePurge0 = cotePurge0;
	}

	public String getCotePurge1() {
		return cotePurge1;
	}

	public void setCotePurge1(String cotePurge1) {
		this.cotePurge1 = cotePurge1;
	}

	public String getCotePurge2() {
		return cotePurge2;
	}

	public void setCotePurge2(String cotePurge2) {
		this.cotePurge2 = cotePurge2;
	}

	public String getCoteTerrassement0() {
		return coteTerrassement0;
	}

	public void setCoteTerrassement0(String coteTerrassement0) {
		this.coteTerrassement0 = coteTerrassement0;
	}

	public String getCoteTerrassement1() {
		return coteTerrassement1;
	}

	public void setCoteTerrassement1(String coteTerrassement1) {
		this.coteTerrassement1 = coteTerrassement1;
	}

	public String getCoteTerrassement2() {
		return coteTerrassement2;
	}

	public void setCoteTerrassement2(String coteTerrassement2) {
		this.coteTerrassement2 = coteTerrassement2;
	}

	public String getCoteTopographie0() {
		return coteTopographie0;
	}

	public void setCoteTopographie0(String coteTopographie0) {
		this.coteTopographie0 = coteTopographie0;
	}

	public String getCoteTopographie1() {
		return coteTopographie1;
	}

	public void setCoteTopographie1(String coteTopographie1) {
		this.coteTopographie1 = coteTopographie1;
	}

	public String getCoteTopographie2() {
		return coteTopographie2;
	}

	public void setCoteTopographie2(String coteTopographie2) {
		this.coteTopographie2 = coteTopographie2;
	}

	public String getDate_maj() {
		return date_maj;
	}

	public void setDate_maj(String date_maj) {
		this.date_maj = date_maj;
	}

	public String getEcr1_pkd() {
		return ecr1_pkd;
	}

	public void setEcr1_pkd(String ecr1_pkd) {
		this.ecr1_pkd = ecr1_pkd;
	}

	public String getEcr1_pkf() {
		return ecr1_pkf;
	}

	public void setEcr1_pkf(String ecr1_pkf) {
		this.ecr1_pkf = ecr1_pkf;
	}

	public String getEcr2_pkd() {
		return ecr2_pkd;
	}

	public void setEcr2_pkd(String ecr2_pkd) {
		this.ecr2_pkd = ecr2_pkd;
	}

	public String getEcr2_pkf() {
		return ecr2_pkf;
	}

	public void setEcr2_pkf(String ecr2_pkf) {
		this.ecr2_pkf = ecr2_pkf;
	}

	public String getEcr3_pkd() {
		return ecr3_pkd;
	}

	public void setEcr3_pkd(String ecr3_pkd) {
		this.ecr3_pkd = ecr3_pkd;
	}

	public String getEcr3_pkf() {
		return ecr3_pkf;
	}

	public void setEcr3_pkf(String ecr3_pkf) {
		this.ecr3_pkf = ecr3_pkf;
	}

	public String getEcr4_pkd() {
		return ecr4_pkd;
	}

	public void setEcr4_pkd(String ecr4_pkd) {
		this.ecr4_pkd = ecr4_pkd;
	}

	public String getEcr4_pkf() {
		return ecr4_pkf;
	}

	public void setEcr4_pkf(String ecr4_pkf) {
		this.ecr4_pkf = ecr4_pkf;
	}

	public String getEcr5_pkd() {
		return ecr5_pkd;
	}

	public void setEcr5_pkd(String ecr5_pkd) {
		this.ecr5_pkd = ecr5_pkd;
	}

	public String getEcr5_pkf() {
		return ecr5_pkf;
	}

	public void setEcr5_pkf(String ecr5_pkf) {
		this.ecr5_pkf = ecr5_pkf;
	}

	public String getEcr6_pkd() {
		return ecr6_pkd;
	}

	public void setEcr6_pkd(String ecr6_pkd) {
		this.ecr6_pkd = ecr6_pkd;
	}

	public String getEcr6_pkf() {
		return ecr6_pkf;
	}

	public void setEcr6_pkf(String ecr6_pkf) {
		this.ecr6_pkf = ecr6_pkf;
	}

	public String getEtatOuvrage() {
		return etatOuvrage;
	}

	public void setEtatOuvrage(String etatOuvrage) {
		this.etatOuvrage = etatOuvrage;
	}

	public String getFicheChuteBloc() {
		return ficheChuteBloc;
	}

	public void setFicheChuteBloc(String ficheChuteBloc) {
		this.ficheChuteBloc = ficheChuteBloc;
	}

	public String getIncidentDescription() {
		return incidentDescription;
	}

	public void setIncidentDescription(String incidentDescription) {
		this.incidentDescription = incidentDescription;
	}

	public String getIncidentImportanceKey() {
		return incidentImportanceKey;
	}

	public void setIncidentImportanceKey(String incidentImportanceKey) {
		this.incidentImportanceKey = incidentImportanceKey;
	}

	public String getLaborAcceptation0Key() {
		return laborAcceptation0Key;
	}

	public void setLaborAcceptation0Key(String laborAcceptation0Key) {
		this.laborAcceptation0Key = laborAcceptation0Key;
	}

	public String getLaborAcceptation1Key() {
		return laborAcceptation1Key;
	}

	public void setLaborAcceptation1Key(String laborAcceptation1Key) {
		this.laborAcceptation1Key = laborAcceptation1Key;
	}

	public String getLaborAcceptation2Key() {
		return laborAcceptation2Key;
	}

	public void setLaborAcceptation2Key(String laborAcceptation2Key) {
		this.laborAcceptation2Key = laborAcceptation2Key;
	}

	public String getLaborAnalyseRisque0() {
		return laborAnalyseRisque0;
	}

	public void setLaborAnalyseRisque0(String laborAnalyseRisque0) {
		this.laborAnalyseRisque0 = laborAnalyseRisque0;
	}

	public String getLaborAnalyseRisque1() {
		return laborAnalyseRisque1;
	}

	public void setLaborAnalyseRisque1(String laborAnalyseRisque1) {
		this.laborAnalyseRisque1 = laborAnalyseRisque1;
	}

	public String getLaborAnalyseRisque2() {
		return laborAnalyseRisque2;
	}

	public void setLaborAnalyseRisque2(String laborAnalyseRisque2) {
		this.laborAnalyseRisque2 = laborAnalyseRisque2;
	}

	public String getLaborMesuresConservatoire0() {
		return laborMesuresConservatoire0;
	}

	public void setLaborMesuresConservatoire0(String laborMesuresConservatoire0) {
		this.laborMesuresConservatoire0 = laborMesuresConservatoire0;
	}

	public String getLaborMesuresConservatoire1() {
		return laborMesuresConservatoire1;
	}

	public void setLaborMesuresConservatoire1(String laborMesuresConservatoire1) {
		this.laborMesuresConservatoire1 = laborMesuresConservatoire1;
	}

	public String getLaborMesuresConservatoire2() {
		return laborMesuresConservatoire2;
	}

	public void setLaborMesuresConservatoire2(String laborMesuresConservatoire2) {
		this.laborMesuresConservatoire2 = laborMesuresConservatoire2;
	}

	public String getLaborTodoAgent0() {
		return laborTodoAgent0;
	}

	public void setLaborTodoAgent0(String laborTodoAgent0) {
		this.laborTodoAgent0 = laborTodoAgent0;
	}

	public String getLaborTodoAgent1() {
		return laborTodoAgent1;
	}

	public void setLaborTodoAgent1(String laborTodoAgent1) {
		this.laborTodoAgent1 = laborTodoAgent1;
	}

	public String getLaborTodoAgent2() {
		return laborTodoAgent2;
	}

	public void setLaborTodoAgent2(String laborTodoAgent2) {
		this.laborTodoAgent2 = laborTodoAgent2;
	}

	public PropositionBouclageDTO getLaborTodoSupervisor0() {
		return laborTodoSupervisor0;
	}

	public void setLaborTodoSupervisor0(PropositionBouclageDTO laborTodoSupervisor0) {
		this.laborTodoSupervisor0 = laborTodoSupervisor0;
	}

	public PropositionBouclageDTO getLaborTodoSupervisor1() {
		return laborTodoSupervisor1;
	}

	public void setLaborTodoSupervisor1(PropositionBouclageDTO laborTodoSupervisor1) {
		this.laborTodoSupervisor1 = laborTodoSupervisor1;
	}

	public PropositionBouclageDTO getLaborTodoSupervisor2() {
		return laborTodoSupervisor2;
	}

	public void setLaborTodoSupervisor2(PropositionBouclageDTO laborTodoSupervisor2) {
		this.laborTodoSupervisor2 = laborTodoSupervisor2;
	}

	public String getMotifCV() {
		return motifCV;
	}

	public void setMotifCV(String motifCV) {
		this.motifCV = motifCV;
	}

	public String getMotifControle() {
		return motifControle;
	}

	public void setMotifControle(String motifControle) {
		this.motifControle = motifControle;
	}

	public String getNumeroFicheRex() {
		return numeroFicheRex;
	}

	public void setNumeroFicheRex(String numeroFicheRex) {
		this.numeroFicheRex = numeroFicheRex;
	}

	public String getSolutionCV() {
		return solutionCV;
	}

	public void setSolutionCV(String solutionCV) {
		this.solutionCV = solutionCV;
	}

	public String getTypeSurvCompl() {
		return typeSurvCompl;
	}

	public void setTypeSurvCompl(String typeSurvCompl) {
		this.typeSurvCompl = typeSurvCompl;
	}

	public String getU0_typeExpertise() {
		return u0_typeExpertise;
	}

	public void setU0_typeExpertise(String u0_typeExpertise) {
		this.u0_typeExpertise = u0_typeExpertise;
	}

	public String getU1_typeExpertise() {
		return u1_typeExpertise;
	}

	public void setU1_typeExpertise(String u1_typeExpertise) {
		this.u1_typeExpertise = u1_typeExpertise;
	}

	public String getU2_typeExpertise() {
		return u2_typeExpertise;
	}

	public void setU2_typeExpertise(String u2_typeExpertise) {
		this.u2_typeExpertise = u2_typeExpertise;
	}

	public SurveillanceDTO getSurveillance() {
		return surveillance;
	}

	public void setSurveillance(SurveillanceDTO surveillance) {
		this.surveillance = surveillance;
	}

	public String getTypeKey() {
		return typeKey;
	}

	public void setTypeKey(String typeKey) {
		this.typeKey = typeKey;
	}

	public UtilisateurDTO getAuteurMaj() {
		return auteurMaj;
	}

	public void setAuteurMaj(UtilisateurDTO auteurMaj) {
		this.auteurMaj = auteurMaj;
	}

	public UtilisateurDTO getAuthor() {
		return author;
	}

	public void setAuthor(UtilisateurDTO author) {
		this.author = author;
	}

	public UtilisateurDTO getAuthorSurveillance() {
		return authorSurveillance;
	}

	public void setAuthorSurveillance(UtilisateurDTO authorSurveillance) {
		this.authorSurveillance = authorSurveillance;
	}

	public String getDelaiTravauxAgent0() {
		return delaiTravauxAgent0;
	}

	public void setDelaiTravauxAgent0(String delaiTravauxAgent0) {
		this.delaiTravauxAgent0 = delaiTravauxAgent0;
	}

	public String getDelaiTravauxAgent1() {
		return delaiTravauxAgent1;
	}

	public void setDelaiTravauxAgent1(String delaiTravauxAgent1) {
		this.delaiTravauxAgent1 = delaiTravauxAgent1;
	}

	public String getDelaiTravauxExpert0() {
		return delaiTravauxExpert0;
	}

	public void setDelaiTravauxExpert0(String delaiTravauxExpert0) {
		this.delaiTravauxExpert0 = delaiTravauxExpert0;
	}

	public String getDelaiTravauxExpert1() {
		return delaiTravauxExpert1;
	}

	public void setDelaiTravauxExpert1(String delaiTravauxExpert1) {
		this.delaiTravauxExpert1 = delaiTravauxExpert1;
	}

	public Integer getOperation() {
		return operation;
	}

	public void setOperation(Integer operation) {
		this.operation = operation;
	}

	public String getEtatCV() {
		return etatCV;
	}

	public void setEtatCV(String etatCV) {
		this.etatCV = etatCV;
	}

	public String getMotifAnnulationCV() {
		return motifAnnulationCV;
	}

	public void setMotifAnnulationCV(String motifAnnulationCV) {
		this.motifAnnulationCV = motifAnnulationCV;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getAuteurSuppression() {
		return auteurSuppression;
	}

	public void setAuteurSuppression(String auteurSuppression) {
		this.auteurSuppression = auteurSuppression;
	}

	public DateBouclageDTO getDateSuppression() {
		return dateSuppression;
	}

	public void setDateSuppression(DateBouclageDTO dateSuppression) {
		this.dateSuppression = dateSuppression;
	}

	public List<BouclageUtilisateur> getAuteursEtapeBouclage() {
		return auteursEtapeBouclage;
	}

	public void setAuteursEtapeBouclage(List<BouclageUtilisateur> auteursEtapeBouclage) {
		this.auteursEtapeBouclage = auteursEtapeBouclage;
	}

	public Object getU0_expertise() {
		return u0_expertise;
	}

	public void setU0_expertise(Object u0_expertise) {
		this.u0_expertise = u0_expertise;
	}

	public Object getU0_etude() {
		return u0_etude;
	}

	public void setU0_etude(Object u0_etude) {
		this.u0_etude = u0_etude;
	}

	public Object getU1_etude() {
		return u1_etude;
	}

	public void setU1_etude(Object u1_etude) {
		this.u1_etude = u1_etude;
	}

	public Object getU1_expertise() {
		return u1_expertise;
	}

	public void setU1_expertise(Object u1_expertise) {
		this.u1_expertise = u1_expertise;
	}

	public Object getU2_etude() {
		return u2_etude;
	}

	public void setU2_etude(Object u2_etude) {
		this.u2_etude = u2_etude;
	}

	public Object getU2_expertise() {
		return u2_expertise;
	}

	public void setU2_expertise(Object u2_expertise) {
		this.u2_expertise = u2_expertise;
	}
	
}
