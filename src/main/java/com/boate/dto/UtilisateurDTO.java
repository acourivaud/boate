package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Utilisateur;
import com.boate.model.UtilisateurPermission;
import com.boate.utils.ConvertData;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UtilisateurDTO {

	private Integer idUtilisateur;
	private String login;
	private String password;
	private String nom;
	private String prenom;
	private String fonction;
	private String entite;
	private String telephoneFixe;
	private String telephoneMobile;
	private String fax;
	private String email;
	private UniteOperationnelleDTO up;
	private DTO dateFinMission;
	private String urlSig;
	private String ssid;
	private Boolean changementPassword;
	private Integer nbLignesTableau;
	private Boolean affichageAlerte;
	private Boolean consultationReleaseNotes;
	private String pageAccueil;
	private List<PermissionDTO> permissions;

	public UtilisateurDTO() {

	}

	private UtilisateurDTO(Utilisateur utilisateur, Boolean simplifie) {
		if (simplifie) {
			this.idUtilisateur = utilisateur.getIdUtilisateur();
			this.login = utilisateur.getLogin();
			this.nom = utilisateur.getNom();
			this.prenom = utilisateur.getPrenom();
			this.email = utilisateur.getEmail();
			this.nbLignesTableau = utilisateur.getNbLignesTableau();
			this.fonction = utilisateur.getFonction() != null ? ConvertData.getFonctionUtilisateurMap().get(utilisateur.getFonction().toString()) : null;
			this.pageAccueil = utilisateur.getPageAccueil();
			this.up = UniteOperationnelleDTO.toDTO(utilisateur.getUp(), true, true, true);
			this.telephoneFixe = utilisateur.getTelephone();
			this.telephoneMobile = utilisateur.getCellphone();
			this.fax = utilisateur.getFax();
			this.entite = utilisateur.getEntite() != null ? ConvertData.getEntiteMap().get(utilisateur.getEntite().toString()) : null;
		}else {
			this.idUtilisateur = utilisateur.getIdUtilisateur();
			this.login = utilisateur.getLogin();
			this.nom = utilisateur.getNom();
			this.prenom = utilisateur.getPrenom();
			//this.password = utilisateur.getPassword();
			this.fonction = utilisateur.getFonction() != null ? utilisateur.getFonction().toString() : null;
			this.entite = utilisateur.getEntite() != null ? utilisateur.getEntite().toString() : null;
			this.telephoneFixe = utilisateur.getTelephone();
			this.telephoneMobile = utilisateur.getCellphone();
			this.fax = utilisateur.getFax();
			this.email = utilisateur.getEmail();
			this.up = UniteOperationnelleDTO.toDTO(utilisateur.getUp(), true, true, true);
			this.dateFinMission = utilisateur.getDateFinMission() != null ? DTO.parseFromLocalDate(utilisateur.getDateFinMission()) : null;
			this.urlSig = utilisateur.getUrlSig();
			this.ssid = utilisateur.getSsid();
			this.changementPassword = utilisateur.getChangementPassword();
			this.nbLignesTableau = utilisateur.getNbLignesTableau();
			this.affichageAlerte = utilisateur.getAffichageAlerte();
			this.consultationReleaseNotes = utilisateur.getConsultationReleaseNotes();
			this.pageAccueil = utilisateur.getPageAccueil();
			final List<PermissionDTO> permissions = new ArrayList<>();
			for (final UtilisateurPermission utilisateurPermission : utilisateur.getPermissions()) {
				permissions.add(PermissionDTO.toDTO(utilisateurPermission.getPermission()));
			}
			this.permissions = permissions;
		}
	}

	public static UtilisateurDTO toDTO(Utilisateur utilisateur, Boolean simplifie) {
		if (utilisateur == null) {
			return null;
		} else {
			return new UtilisateurDTO(utilisateur, simplifie);
		}
	}

	public static List<UtilisateurDTO> toDTOs(List<Utilisateur> utilisateurs, Boolean simplifie) {
		if (utilisateurs == null) {
			return new ArrayList<>(0);
		}
		List<UtilisateurDTO> utilisateurDTOs = new ArrayList<>(utilisateurs.size());
		for (Utilisateur utilisateur : utilisateurs) {
			utilisateurDTOs.add(UtilisateurDTO.toDTO(utilisateur, simplifie));
		}
		return utilisateurDTOs;
	}

	public Integer getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(Integer idAccount) {
		this.idUtilisateur = idAccount;
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}

	public String getEntite() {
		return entite;
	}

	public void setEntite(String entite) {
		this.entite = entite;
	}

	public String getTelephoneFixe() {
		return telephoneFixe;
	}

	public void setTelephoneFixe(String telephoneFixe) {
		this.telephoneFixe = telephoneFixe;
	}

	public String getTelephoneMobile() {
		return telephoneMobile;
	}

	public void setTelephoneMobile(String telephoneMobile) {
		this.telephoneMobile = telephoneMobile;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UniteOperationnelleDTO getUp() {
		return up;
	}

	public void setUp(UniteOperationnelleDTO up) {
		this.up = up;
	}

	public DTO getDateFinMission() {
		return dateFinMission;
	}

	public void setDateFinMission(DTO dateFinMission) {
		this.dateFinMission = dateFinMission;
	}

	public String getUrlSig() {
		return urlSig;
	}

	public void setUrlSig(String urlSig) {
		this.urlSig = urlSig;
	}

	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	public Boolean getChangementPassword() {
		return changementPassword;
	}

	public void setChangementPassword(Boolean changementPassword) {
		this.changementPassword = changementPassword;
	}

	public Integer getNbLignesTableau() {
		return nbLignesTableau;
	}

	public void setNbLignesTableau(Integer nbLignesTableau) {
		this.nbLignesTableau = nbLignesTableau;
	}

	public Boolean getAffichageAlerte() {
		return affichageAlerte;
	}

	public void setAffichageAlerte(Boolean affichageAlerte) {
		this.affichageAlerte = affichageAlerte;
	}

	public Boolean getConsultationReleaseNotes() {
		return consultationReleaseNotes;
	}

	public void setConsultationReleaseNotes(Boolean consultationReleaseNotes) {
		this.consultationReleaseNotes = consultationReleaseNotes;
	}

	public String getPageAccueil() {
		return pageAccueil;
	}

	public void setPageAccueil(String pageAccueil) {
		this.pageAccueil = pageAccueil;
	}

	public List<PermissionDTO> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<PermissionDTO> permissions) {
		this.permissions = permissions;
	}
}
