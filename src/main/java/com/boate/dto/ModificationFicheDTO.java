package com.boate.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.boate.model.ModificationFiche;

public class ModificationFicheDTO {

	private Integer id;
	private OuvrageDTO ouvrage;
	private SurveillanceDTO surveillance;
	// private TravauxDTO travaux;
	// private SurveillanceComplementaireDTO surveillanceComplementaire;
	private String attribut;
	private String ancienneValeur;
	private String nouvelleValeur;
	private LocalDateTime dateMaj;

	public ModificationFicheDTO() {
		
	}
	
	public ModificationFicheDTO(ModificationFiche modificationFiche) {
		this.id = modificationFiche.getId();
		this.ouvrage = OuvrageDTO.toDTO(modificationFiche.getOuvrageModifie(), "modificationFiche");
		this.surveillance = SurveillanceDTO.toDTO(modificationFiche.getSurveillanceModifiee(), "modificationFiche");
		//this.travaux = TravauxDTO.toDTO(modificationFiche.getTravaux(), "modificationFiche");
		//this.surveillanceComplementaire = SurveillanceComplementaireDTO.toDTO(modificationFiche.getSurveillanceComplementaire(), "modificationFiche");
		this.attribut = modificationFiche.getAttribut();
		this.ancienneValeur = modificationFiche.getAncienneValeur();
		this.nouvelleValeur = modificationFiche.getNouvelleValeur();
		this.dateMaj = modificationFiche.getDateMaj();
	}

	public static ModificationFicheDTO toDTO(ModificationFiche modificationFiche) {
		if (modificationFiche == null) {
			return null;
		} else {
			return new ModificationFicheDTO(modificationFiche);
		}
	}

	public static List<ModificationFicheDTO> toDTOs(List<ModificationFiche> modificationFiches) {
		if (modificationFiches == null) {
			return new ArrayList<>(0);
		}
		List<ModificationFicheDTO> modificationFicheDTOs = new ArrayList<>(modificationFiches.size());
		for (ModificationFiche modificationFiche : modificationFiches) {
			modificationFicheDTOs.add(ModificationFicheDTO.toDTO(modificationFiche));
		}
		return modificationFicheDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public OuvrageDTO getOuvrage() {
		return ouvrage;
	}

	public void setOuvrage(OuvrageDTO ouvrage) {
		this.ouvrage = ouvrage;
	}

	public SurveillanceDTO getSurveillance() {
		return surveillance;
	}

	public void setSurveillance(SurveillanceDTO surveillance) {
		this.surveillance = surveillance;
	}

	public String getAttribut() {
		return attribut;
	}

	public void setAttribut(String attribut) {
		this.attribut = attribut;
	}

	public String getAncienneValeur() {
		return ancienneValeur;
	}

	public void setAncienneValeur(String ancienneValeur) {
		this.ancienneValeur = ancienneValeur;
	}

	public String getNouvelleValeur() {
		return nouvelleValeur;
	}

	public void setNouvelleValeur(String nouvelleValeur) {
		this.nouvelleValeur = nouvelleValeur;
	}

	public LocalDateTime getDateMaj() {
		return dateMaj;
	}

	public void setDateMaj(LocalDateTime dateMaj) {
		this.dateMaj = dateMaj;
	}

}
