package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.ArchiveBoite;

public class ArchiveBoiteDTO {

	private Integer id;
	private String nom;
	private String localisation;

	public ArchiveBoiteDTO() {
		
	}
	
	public ArchiveBoiteDTO(ArchiveBoite archiveBoite) {
		this.id = archiveBoite.getId();
		this.nom = archiveBoite.getNom();
		this.localisation = archiveBoite.getLocalisation();
	}

	public static ArchiveBoiteDTO toDTO(ArchiveBoite archiveBoite) {
		if (archiveBoite == null) {
			return null;
		} else {
			return new ArchiveBoiteDTO(archiveBoite);
		}
	}

	public static List<ArchiveBoiteDTO> toDTOs(List<ArchiveBoite> archiveBoites) {
		if (archiveBoites == null) {
			return new ArrayList<>(0);
		}
		List<ArchiveBoiteDTO> archiveBoiteDTOs = new ArrayList<>(archiveBoites.size());
		for (ArchiveBoite archiveBoite : archiveBoites) {
			archiveBoiteDTOs.add(ArchiveBoiteDTO.toDTO(archiveBoite));
		}
		return archiveBoiteDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getLocalisation() {
		return localisation;
	}

	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}

}
