package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.GeometrieType;

public class GeometrieTypeDTO {

	private Integer id;
	private TypeDTO type;
	private GeometrieDTO geometrie;

	public GeometrieTypeDTO() {

	}

	public static GeometrieTypeDTO toDTO(GeometrieType geometrieType) {
		if (geometrieType == null) {
			return null;
		} else {
			final GeometrieTypeDTO geometrieDTO = new GeometrieTypeDTO();
			geometrieDTO.setId(geometrieType.getId());
			geometrieDTO.setGeometrie(GeometrieDTO.toDTO(geometrieType.getGeometrie()));
			geometrieDTO.setType(TypeDTO.toDTO(geometrieType.getType()));
			return geometrieDTO;
		}
	}

	public static List<GeometrieTypeDTO> toDTOs(List<GeometrieType> geometriesTypes) {
		if (geometriesTypes == null) {
			return new ArrayList<>(0);
		}
		List<GeometrieTypeDTO> geometriesTypesDTO = new ArrayList<>(geometriesTypes.size());
		for (GeometrieType geometrieType : geometriesTypes) {
			geometriesTypesDTO.add(GeometrieTypeDTO.toDTO(geometrieType));
		}
		return geometriesTypesDTO;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TypeDTO getType() {
		return type;
	}

	public void setType(TypeDTO type) {
		this.type = type;
	}

	public GeometrieDTO getGeometrie() {
		return geometrie;
	}

	public void setGeometrie(GeometrieDTO geometrie) {
		this.geometrie = geometrie;
	}
}
