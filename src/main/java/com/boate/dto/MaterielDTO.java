package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Materiel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MaterielDTO {

	private Integer id;
	private String nom;

	public MaterielDTO() {

	}

	private MaterielDTO(Materiel materiel) {
		this.id = materiel.getId();
		this.nom = materiel.getNom();
	}

	public static MaterielDTO toDTO(Materiel materiel) {
		if (materiel == null) {
			return null;
		} else {
			return new MaterielDTO(materiel);
		}
	}

	public static List<MaterielDTO> toDTOs(List<Materiel> materiels) {
		if (materiels == null) {
			return new ArrayList<>(0);
		}
		List<MaterielDTO> materielDTOs = new ArrayList<>(materiels.size());
		for (Materiel materiel : materiels) {
			materielDTOs.add(MaterielDTO.toDTO(materiel));
		}
		return materielDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
}
