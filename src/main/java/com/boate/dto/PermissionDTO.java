package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Permission;

public class PermissionDTO {

    private Integer id;
    private String libelle;
    
    public PermissionDTO() {
        
    }
    
    public PermissionDTO(Permission permission) {
        this.id = permission.getId();
        this.libelle = permission.getLibelle();
    }

    public static PermissionDTO toDTO(Permission permission) {
        if (permission == null) {
            return null;
        } else {
            return new PermissionDTO(permission);
        }
    }

    public static List<PermissionDTO> toDTOs(List<Permission> permissions) {
        if (permissions == null) {
            return new ArrayList<>(0);
        }
        List<PermissionDTO> permissionDTOs = new ArrayList<>(permissions.size());
        for (Permission permission : permissions) {
            permissionDTOs.add(PermissionDTO.toDTO(permission));
        }
        return permissionDTOs;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

}
