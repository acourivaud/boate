package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Ouvrage;
import com.boate.utils.DataFormatUtils;

public class OuvrageDTO {

	private Integer id;
	private String name;
	private String pk;
	private String middlePk;
	private String endPk;
	private UniteOperationnelleDTO up;
	private LigneDTO line;
	private String categorie;
	private TypeOuvrageDTO typeOuvrage;
	private String indice;
	private String nature;
	private String geometrie;
	private String situation;
	private String materiau;
	private Double oaOuverture;
	private Double oaHauteur;
	private Double oaHauteurMini;
	private Double couverture;
	private Double longueur;
	private Double distanceRail;
	private Double fruitParement;
	private String fsa;
	private String uic;
	private String aboveBelow;
	private Integer year;
	private Boolean supprime;
	private Boolean archive;
	private DepartementDTO firstDepartement;
	private DepartementDTO secondDepartement;
	private CommuneDTO firstCity;
	private CommuneDTO secondCity;
	private Integer surveillanceId;
	private Boolean ouvrageEnveloppe;
	private OuvrageDTO parentWork;
	private String otClassement;
	
	public OuvrageDTO() {

	}

	public OuvrageDTO(Ouvrage ouvrage, String page) {
		switch (page) {
		case "listeOA":
			this.id = ouvrage.getId();
			this.name = ouvrage.getName();
			this.pk = DataFormatUtils.checkPk(ouvrage.getPk().toString());
			this.middlePk = DataFormatUtils.checkPk(ouvrage.getMiddlePk().toString());
			this.endPk = DataFormatUtils.checkPk(ouvrage.getEndPk().toString());
			this.up = UniteOperationnelleDTO.toDTO(ouvrage.getUp(), true, true, true);
			this.line = LigneDTO.toDTO(ouvrage.getLine(), true);
			this.categorie = ouvrage.getCategoryKey(true, true);
			this.typeOuvrage = TypeOuvrageDTO.toDTO(ouvrage.getTypeKey(), true);
			this.indice = ouvrage.getIndice();
			this.nature = ouvrage.getMaterialKey();
			this.geometrie = ouvrage.getGeometryKey();
			this.situation = ouvrage.getSituationKey();
			this.oaOuverture = ouvrage.getOaOuverture();
			this.couverture = ouvrage.getOaCouverture();
			this.longueur = ouvrage.getOaLongueur();
			break;
		case "listeArchive":
			this.id = ouvrage.getId();
			this.pk = DataFormatUtils.checkPk(ouvrage.getPk().toString());
			this.middlePk = DataFormatUtils.checkPk(ouvrage.getMiddlePk().toString());
			this.endPk = DataFormatUtils.checkPk(ouvrage.getEndPk().toString());
			this.line = LigneDTO.toDTO(ouvrage.getLine(), true);
			this.categorie = ouvrage.getCategoryKey(true, true);
			break;
		case "listeSurveillances":
			this.uic = ouvrage.getUic();
			this.up = UniteOperationnelleDTO.toDTO(ouvrage.getUp(), true, true, true);
			this.pk = DataFormatUtils.checkPk(ouvrage.getPk().toString());
			this.middlePk = DataFormatUtils.checkPk(ouvrage.getMiddlePk().toString());
			this.endPk = DataFormatUtils.checkPk(ouvrage.getEndPk().toString());
			this.line = LigneDTO.toDTO(ouvrage.getLine(), true);
			this.categorie = ouvrage.getCategoryKey(true, true);
			this.name = ouvrage.getName();
			this.indice = ouvrage.getIndice();
			this.nature = ouvrage.getMaterialKey();
			this.typeOuvrage = TypeOuvrageDTO.toDTO(ouvrage.getTypeKey(), true);
			this.situation = ouvrage.getSituationKey();
			this.geometrie = ouvrage.getGeometryKey();
			this.oaOuverture = ouvrage.getOaOuverture();
			this.oaHauteur = ouvrage.getOaHauteur();
			this.longueur = ouvrage.getOaLongueur();
			this.fsa = ouvrage.getFsaKey();
			this.aboveBelow = ouvrage.getAboveBelow();
			this.year = ouvrage.getYear();
			break;
		case "listeBouclages":
			this.id = ouvrage.getId();
			this.uic = ouvrage.getUic();
			this.up = UniteOperationnelleDTO.toDTO(ouvrage.getUp(), true, true, true);
			this.pk = DataFormatUtils.checkPk(ouvrage.getPk().toString());
			this.middlePk = DataFormatUtils.checkPk(ouvrage.getMiddlePk().toString());
			this.endPk = DataFormatUtils.checkPk(ouvrage.getEndPk().toString());
			this.line = LigneDTO.toDTO(ouvrage.getLine(), true);
			this.categorie = ouvrage.getCategoryKey(true, true);
			this.name = ouvrage.getName();
			this.indice = ouvrage.getIndice();
			this.nature = ouvrage.getMaterialKey();
			this.typeOuvrage = TypeOuvrageDTO.toDTO(ouvrage.getTypeKey(), true);
			this.situation = ouvrage.getSituationKey();
			this.geometrie = ouvrage.getGeometryKey();
			this.oaOuverture = ouvrage.getOaOuverture();
			this.oaHauteur = ouvrage.getOaHauteur();
			this.longueur = ouvrage.getOaLongueur();
			this.fsa = ouvrage.getFsaKey();
			this.aboveBelow = ouvrage.getAboveBelow();
			this.year = ouvrage.getYear();
			break;
		case "onlyName":
			this.id = ouvrage.getId();
			this.name = ouvrage.getName();
			break;
		case "ficheOuvrage":
			this.id = ouvrage.getId();
			this.name = ouvrage.getName();
			this.pk = DataFormatUtils.checkPk(ouvrage.getPk().toString());
			this.middlePk = DataFormatUtils.checkPk(ouvrage.getMiddlePk().toString());
			this.endPk = DataFormatUtils.checkPk(ouvrage.getEndPk().toString());
			this.up = UniteOperationnelleDTO.toDTO(ouvrage.getUp(), true, true, true);
			this.line = LigneDTO.toDTO(ouvrage.getLine(), true);
			this.categorie = ouvrage.getCategoryKey(false, false);
			this.typeOuvrage = TypeOuvrageDTO.toDTO(ouvrage.getTypeKey(), true);
			this.indice = ouvrage.getIndice();
			this.nature = ouvrage.getMaterialKey();
			this.geometrie = ouvrage.getGeometryKey();
			this.situation = ouvrage.getSituationKey();
			this.oaOuverture = ouvrage.getOaOuverture();
			this.couverture = ouvrage.getOaCouverture();
			this.longueur = ouvrage.getOaLongueur();
			this.supprime = ouvrage.getSupprime();
			this.archive = ouvrage.getArchive();
			this.uic = ouvrage.getUic();
			this.firstDepartement = DepartementDTO.toDTO(ouvrage.getFirstDepartement());
			this.secondDepartement = DepartementDTO.toDTO(ouvrage.getFirstDepartement());
			this.firstCity = CommuneDTO.toDTO(ouvrage.getFirstCity(), false);
			this.secondCity = CommuneDTO.toDTO(ouvrage.getSecondCity(), false);
			this.situation = ouvrage.getSituationKey();
			this.materiau = ouvrage.getMateriauKey();
			this.oaHauteur = ouvrage.getOaHauteur();
			this.oaHauteurMini = ouvrage.getOaHauteurMini();
			this.distanceRail = ouvrage.getDistanceRail();
			this.fruitParement = ouvrage.getOaInclination();
			this.ouvrageEnveloppe = ouvrage.getOuvrageEnveloppe();
			this.parentWork = OuvrageDTO.toDTO(ouvrage.getParentWork(), "simplifie");
			this.otClassement = ouvrage.getOtClassement();
			break;
		case "simplifie":
			this.pk = DataFormatUtils.checkPk(ouvrage.getPk().toString());
			this.middlePk = DataFormatUtils.checkPk(ouvrage.getMiddlePk().toString());
			this.endPk = DataFormatUtils.checkPk(ouvrage.getEndPk().toString());
			break;
		default:
			this.id = ouvrage.getId();
			this.name = ouvrage.getName();
			this.pk = DataFormatUtils.checkPk(ouvrage.getPk().toString());
			this.middlePk = DataFormatUtils.checkPk(ouvrage.getMiddlePk().toString());
			this.endPk = DataFormatUtils.checkPk(ouvrage.getEndPk().toString());
			this.up = UniteOperationnelleDTO.toDTO(ouvrage.getUp(), true, true, true);
			this.line = LigneDTO.toDTO(ouvrage.getLine(), true);
			this.categorie = ouvrage.getCategoryKey(true, true);
			this.typeOuvrage = TypeOuvrageDTO.toDTO(ouvrage.getTypeKey(), true);
			this.indice = ouvrage.getIndice();
			this.nature = ouvrage.getMaterialKey();
			this.geometrie = ouvrage.getGeometryKey();
			this.situation = ouvrage.getSituationKey();
			this.oaOuverture = ouvrage.getOaOuverture();
			this.couverture = ouvrage.getOaCouverture();
			this.longueur = ouvrage.getOaLongueur();
			this.supprime = ouvrage.getSupprime();
			this.archive = ouvrage.getArchive();
			break;
		}
	}

	public static OuvrageDTO toDTO(Ouvrage ouvrage, String element) {
		if (ouvrage == null) {
			return null;
		} else {
			return new OuvrageDTO(ouvrage, element);
		}
	}

	public static List<OuvrageDTO> toDTOs(List<Ouvrage> ouvrages, String page) {
		if (ouvrages == null) {
			return new ArrayList<>(0);
		}
		List<OuvrageDTO> ouvrageDTOs = new ArrayList<>(ouvrages.size());
		for (Ouvrage ouvrage : ouvrages) {
			ouvrageDTOs.add(OuvrageDTO.toDTO(ouvrage, page));
		}
		return ouvrageDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPk() {
		return pk;
	}

	public void setPk(String pk) {
		this.pk = pk;
	}

	public String getMiddlePk() {
		return middlePk;
	}

	public void setMiddlePk(String middlePk) {
		this.middlePk = middlePk;
	}

	public String getEndPk() {
		return endPk;
	}

	public void setEndPk(String endPk) {
		this.endPk = endPk;
	}

	public LigneDTO getLine() {
		return line;
	}

	public void setLine(LigneDTO line) {
		this.line = line;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public TypeOuvrageDTO getTypeOuvrage() {
		return typeOuvrage;
	}

	public void setTypeOuvrage(TypeOuvrageDTO typeOuvrage) {
		this.typeOuvrage = typeOuvrage;
	}

	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	public String getNature() {
		return nature;
	}

	public void setNature(String nature) {
		this.nature = nature;
	}

	public String getGeometrie() {
		return geometrie;
	}

	public void setGeometrie(String geometrie) {
		this.geometrie = geometrie;
	}

	public String getSituation() {
		return situation;
	}

	public void setSituation(String situation) {
		this.situation = situation;
	}

	public Double getOaOuverture() {
		return oaOuverture;
	}

	public void setOaOuverture(Double oaOuverture) {
		this.oaOuverture = oaOuverture;
	}

	public Double getCouverture() {
		return couverture;
	}

	public void setCouverture(Double couverture) {
		this.couverture = couverture;
	}

	public Double getLongueur() {
		return longueur;
	}

	public void setLongueur(Double longueur) {
		this.longueur = longueur;
	}

	public UniteOperationnelleDTO getUp() {
		return up;
	}

	public void setUp(UniteOperationnelleDTO up) {
		this.up = up;
	}

	public String getUic() {
		return uic;
	}

	public void setUic(String uic) {
		this.uic = uic;
	}

	public Double getOaHauteur() {
		return oaHauteur;
	}

	public void setOaHauteur(Double oaHauteur) {
		this.oaHauteur = oaHauteur;
	}

	public String getFsa() {
		return fsa;
	}

	public void setFsa(String fsa) {
		this.fsa = fsa;
	}

	public String getAboveBelow() {
		return aboveBelow;
	}

	public void setAboveBelow(String aboveBelow) {
		this.aboveBelow = aboveBelow;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Boolean getSupprime() {
		return supprime;
	}

	public void setSupprime(Boolean supprime) {
		this.supprime = supprime;
	}

	public Boolean getArchive() {
		return archive;
	}

	public void setArchive(Boolean archive) {
		this.archive = archive;
	}

	public DepartementDTO getFirstDepartement() {
		return firstDepartement;
	}

	public void setFirstDepartement(DepartementDTO firstDepartement) {
		this.firstDepartement = firstDepartement;
	}

	public DepartementDTO getSecondDepartement() {
		return secondDepartement;
	}

	public void setSecondDepartement(DepartementDTO secondDepartement) {
		this.secondDepartement = secondDepartement;
	}

	public CommuneDTO getFirstCity() {
		return firstCity;
	}

	public void setFirstCity(CommuneDTO firstCity) {
		this.firstCity = firstCity;
	}

	public CommuneDTO getSecondCity() {
		return secondCity;
	}

	public void setSecondCity(CommuneDTO secondCity) {
		this.secondCity = secondCity;
	}

	public String getMateriau() {
		return materiau;
	}

	public void setMateriau(String materiau) {
		this.materiau = materiau;
	}

	public Double getOaHauteurMini() {
		return oaHauteurMini;
	}

	public void setOaHauteurMini(Double oaHauteurMini) {
		this.oaHauteurMini = oaHauteurMini;
	}

	public Double getDistanceRail() {
		return distanceRail;
	}

	public void setDistanceRail(Double distanceRail) {
		this.distanceRail = distanceRail;
	}

	public Double getFruitParement() {
		return fruitParement;
	}

	public void setFruitParement(Double fruitParement) {
		this.fruitParement = fruitParement;
	}

	public Integer getSurveillanceId() {
		return surveillanceId;
	}

	public void setSurveillanceId(Integer surveillanceId) {
		this.surveillanceId = surveillanceId;
	}

	public Boolean getOuvrageEnveloppe() {
		return ouvrageEnveloppe;
	}

	public void setOuvrageEnveloppe(Boolean ouvrageEnveloppe) {
		this.ouvrageEnveloppe = ouvrageEnveloppe;
	}

	public OuvrageDTO getParentWork() {
		return parentWork;
	}

	public void setParentWork(OuvrageDTO parentWork) {
		this.parentWork = parentWork;
	}

	public String getOtClassement() {
		return otClassement;
	}

	public void setOtClassement(String otClassement) {
		this.otClassement = otClassement;
	}

}
