package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.IndicateurSecurite;

public class IndicateurSecuriteDTO {

	private Integer id;
	private String code;
	private RegionDTO region;
	private Integer t1;
	private Integer t2;
	private Integer t3;
	private Integer t4;
	private Integer t5;
	private Integer annee;
	private String infrapole;
	private String observation;
	
	public IndicateurSecuriteDTO() {
		
	}
	
	public IndicateurSecuriteDTO(IndicateurSecurite indicateurSecurite) {
		this.id = indicateurSecurite.getId();
		this.annee = indicateurSecurite.getAnnee();
		this.code = indicateurSecurite.getCode();
		this.region = RegionDTO.toDTO(indicateurSecurite.getRegion());
		this.t1 = indicateurSecurite.getT1();
		this.t2 = indicateurSecurite.getT2();
		this.t3 = indicateurSecurite.getT3();
		this.t4 = indicateurSecurite.getT4();
		this.t5 = indicateurSecurite.getT5();
		this.infrapole = indicateurSecurite.getInfrapole();
		this.observation = indicateurSecurite.getObservation();
	}

	public static IndicateurSecuriteDTO toDTO(IndicateurSecurite indicateurSecurite) {
		if (indicateurSecurite == null) {
			return null;
		} else {
			return new IndicateurSecuriteDTO(indicateurSecurite);
		}
	}

	public static List<IndicateurSecuriteDTO> toDTOs(List<IndicateurSecurite> indicateurs) {
		if (indicateurs == null) {
			return new ArrayList<>(0);
		}
		List<IndicateurSecuriteDTO> indicateurDTOs = new ArrayList<>(indicateurs.size());
		for (IndicateurSecurite indicateurSecurite : indicateurs) {
			indicateurDTOs.add(IndicateurSecuriteDTO.toDTO(indicateurSecurite));
		}
		return indicateurDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAnnee() {
		return annee;
	}

	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public RegionDTO getRegion() {
		return region;
	}

	public void setRegion(RegionDTO region) {
		this.region = region;
	}

	public Integer getT1() {
		return t1;
	}

	public void setT1(Integer t1) {
		this.t1 = t1;
	}

	public Integer getT2() {
		return t2;
	}

	public void setT2(Integer t2) {
		this.t2 = t2;
	}

	public Integer getT3() {
		return t3;
	}

	public void setT3(Integer t3) {
		this.t3 = t3;
	}

	public Integer getT4() {
		return t4;
	}

	public void setT4(Integer t4) {
		this.t4 = t4;
	}

	public Integer getT5() {
		return t5;
	}

	public void setT5(Integer t5) {
		this.t5 = t5;
	}

	public String getInfrapole() {
		return infrapole;
	}

	public void setInfrapole(String infrapole) {
		this.infrapole = infrapole;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}
}
