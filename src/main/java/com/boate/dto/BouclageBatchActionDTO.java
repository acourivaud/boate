package com.boate.dto;

import java.time.LocalDate;
import java.util.List;

public class BouclageBatchActionDTO {
	
	private List<Integer> bouclagesIds;
	private LocalDate date;
	private Integer numeroOperation;
	private String batchType;
	private List<ParamValueDTO> filtersForReturn;
	
	public BouclageBatchActionDTO() {
		
	}
	
	public List<Integer> getBouclagesIds() {
		return bouclagesIds;
	}
	public void setBouclagesIds(List<Integer> bouclagesIds) {
		this.bouclagesIds = bouclagesIds;
	}
	
	public Integer getNumeroOperation() {
		return numeroOperation;
	}

	public void setNumeroOperation(Integer numeroOperation) {
		this.numeroOperation = numeroOperation;
	}

	public String getBatchType() {
		return batchType;
	}
	public void setBatchType(String batchType) {
		this.batchType = batchType;
	}
	public List<ParamValueDTO> getFiltersForReturn() {
		return filtersForReturn;
	}
	public void setFiltersForReturn(List<ParamValueDTO> filtersForReturn) {
		this.filtersForReturn = filtersForReturn;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}
}
