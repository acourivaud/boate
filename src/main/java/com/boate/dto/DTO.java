package com.boate.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DTO {

	private String dto;
	
	public DTO() {
		
	}

	public DTO(String dtoValue) {
		this.dto = dtoValue;
	}

	public String getDto() {
		return dto;
	}

	public void setDto(String dto) {
		this.dto = dto;
	}

	public static DTO toDTO(String dtoValue) {
		if (dtoValue == null) {
			return null;
		} else {
			return new DTO(dtoValue);
		}
	}

	public static List<DTO> toDTOs(List<String> dtoValues) {
		if (dtoValues == null) {
			return new ArrayList<>(0);
		}
		List<DTO> dtoValueDTOs = new ArrayList<>(dtoValues.size());
		for (String dtoValue : dtoValues) {
			dtoValueDTOs.add(DTO.toDTO(dtoValue));
		}
		return dtoValueDTOs;
	}
	
	public LocalDateTime toLocalDateTime() throws Exception 
	{
		String[] split = this.dto.split("\\|");
		int year = 0, month = 0, dayOfMonth = 0, hour = 0, minute = 0, second = 0;
		if (split != null && split.length != 6)
		{
			throw new Exception();
		}
		year = Integer.parseInt(split[0]);
		month = Integer.parseInt(split[1]);
		dayOfMonth = Integer.parseInt(split[2]);
		hour = Integer.parseInt(split[3]);
		minute = Integer.parseInt(split[4]);
		second = Integer.parseInt(split[5]);
		return LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);
	}
	
	public LocalDate toLocalDate() throws Exception 
	{
		String[] split = this.dto.split("\\|");
		int year = 0, month = 0, dayOfMonth = 0;
		if (split != null && split.length != 3)
		{
			throw new Exception();
		}
		year = Integer.parseInt(split[0]);
		month = Integer.parseInt(split[1]);
		dayOfMonth = Integer.parseInt(split[2]);
		return LocalDate.of(year, month, dayOfMonth);
	}
	
	public static DTO parseFromLocalDateTime(LocalDateTime localDateTime) {
		return localDateTime != null ? new DTO(String.valueOf(localDateTime.getYear()) + "|" +
						String.valueOf(localDateTime.getMonthValue()) + "|" +
						String.valueOf(localDateTime.getDayOfMonth()) + "|" +
						String.valueOf(localDateTime.getHour()) + "|" +
						String.valueOf(localDateTime.getMinute()) + "|" +
						String.valueOf(localDateTime.getSecond())) : new DTO();
	}
	
	public static DTO parseFromLocalDate(LocalDate localDate) {
		return localDate != null ? new DTO(String.valueOf(localDate.getYear()) + "|" +
						String.valueOf(localDate.getMonthValue()) + "|" +
						String.valueOf(localDate.getDayOfMonth())) : new DTO();
	}
}
