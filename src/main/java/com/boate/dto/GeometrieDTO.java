package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Geometrie;

public class GeometrieDTO {

	private Integer id;
	private String libelle;
	private TypeDTO type;

	public GeometrieDTO() {

	}

	public static GeometrieDTO toDTO(Geometrie geometrie) {
		if (geometrie == null) {
			return null;
		} else {
			final GeometrieDTO geometrieDTO = new GeometrieDTO();
			geometrieDTO.setId(geometrie.getId());
			geometrieDTO.setLibelle(geometrie.getLibelle());
			return geometrieDTO;
		}
	}

	public static List<GeometrieDTO> toDTOs(List<Geometrie> geometries) {
		if (geometries == null) {
			return new ArrayList<>(0);
		}
		List<GeometrieDTO> geometrieDTOs = new ArrayList<>(geometries.size());
		for (Geometrie geometrie : geometries) {
			geometrieDTOs.add(GeometrieDTO.toDTO(geometrie));
		}
		return geometrieDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public TypeDTO getType() {
		return type;
	}

	public void setType(TypeDTO type) {
		this.type = type;
	}
}
