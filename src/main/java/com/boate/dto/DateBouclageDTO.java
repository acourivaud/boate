package com.boate.dto;

import java.time.LocalDate;

import com.boate.security.entity.UserPrincipal;

public class DateBouclageDTO {

	private LocalDate date;
	private Integer diff;
	private String auteur;
	
	public DateBouclageDTO() {
		
	}

	public DateBouclageDTO(UserPrincipal currentUser, LocalDate date, Integer diff, String auteur) {
		super();
		Boolean accessSignatureAutorise = currentUser.getPermissions().stream()
				  .filter(permission -> "Signature Bouclage".equals(permission.getPermission().getLibelle()))
				  .findFirst().isPresent();
		this.date = date;
		this.diff = accessSignatureAutorise ? diff : null;
		this.auteur = accessSignatureAutorise ? auteur : null;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Integer getDiff() {
		return diff;
	}

	public void setDiff(Integer diff) {
		this.diff = diff;
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
}
