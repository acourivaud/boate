package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Ligne;

public class LigneDTO {

	private Integer id;
	private String code;
	private String nom;
	private String pkDebut;
	private String pkFin;

	public LigneDTO() {

	}

	public static LigneDTO toDTO(Ligne line, Boolean simplifie) {
		if (line == null) {
			return null;
		} else {
			final LigneDTO lineDTO = new LigneDTO();
			if (simplifie) {
				lineDTO.setCode(line.getCode());
			} else {
				lineDTO.setId(line.getId());
				lineDTO.setNom(line.getName());
				lineDTO.setCode(line.getCode());
				lineDTO.setPkDebut(line.getBeginPk());
				lineDTO.setPkFin(line.getEndPk());
			}
			return lineDTO;
		}
	}

	public static List<LigneDTO> toDTOs(List<Ligne> lines) {
		if (lines == null) {
			return new ArrayList<>(0);
		}
		List<LigneDTO> lineDTOs = new ArrayList<>(lines.size());
		for (Ligne line : lines) {
			lineDTOs.add(LigneDTO.toDTO(line, false));
		}
		return lineDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPkDebut() {
		return pkDebut;
	}

	public void setPkDebut(String pkDebut) {
		this.pkDebut = pkDebut;
	}

	public String getPkFin() {
		return pkFin;
	}

	public void setPkFin(String pkFin) {
		this.pkFin = pkFin;
	}

}
