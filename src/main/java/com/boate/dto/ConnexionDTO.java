package com.boate.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.boate.model.Connexion;

public class ConnexionDTO {

	private Integer id;
	private String login;
	private String heure;
	private LocalDate date;

	public ConnexionDTO() {
		
	}
	
	public ConnexionDTO(Connexion connexion) {
		this.id = connexion.getId();
		this.login = connexion.getLogin();
		this.heure = connexion.getHeure();
		this.date = connexion.getDate();
	}

	public static ConnexionDTO toDTO(Connexion connexion) {
		if (connexion == null) {
			return null;
		} else {
			return new ConnexionDTO(connexion);
		}
	}

	public static List<ConnexionDTO> toDTOs(List<Connexion> connexions) {
		if (connexions == null) {
			return new ArrayList<>(0);
		}
		List<ConnexionDTO> connexionDTOs = new ArrayList<>(connexions.size());
		for (Connexion connexion : connexions) {
			connexionDTOs.add(ConnexionDTO.toDTO(connexion));
		}
		return connexionDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getHeure() {
		return heure;
	}

	public void setHeure(String heure) {
		this.heure = heure;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

}
