package com.boate.dto;

import com.boate.enums.PropositionBouclageStatusEnum;

public class PropositionBouclageDTO {

	private String proposition;
	private String level;
	
	public PropositionBouclageDTO() {
		
	}

	public PropositionBouclageDTO(String proposition, String level) {
		this.proposition = proposition;
		this.level = level != null ? PropositionBouclageStatusEnum.valueOf(level).toString() : null;
	}

	public String getProposition() {
		return proposition;
	}

	public void setProposition(String proposition) {
		this.proposition = proposition;
	}

	public String getLevel() {
		return PropositionBouclageStatusEnum.valueOf(level).toString();
	}

	public void setLevel(String level) {
		this.level = PropositionBouclageStatusEnum.valueOf(level).toString();
	}
}
