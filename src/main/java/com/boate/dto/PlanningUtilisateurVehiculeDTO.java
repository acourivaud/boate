package com.boate.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.boate.model.PlanningVehicule;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanningUtilisateurVehiculeDTO {

	// True if this object is used in a request to get all Users having booked a specific Vehicle.
	// False if this object is used in a request to get all Vehicles booked by a User.
	// Avoid loading the grouping object multiple times in loop (whether it's a User or Vehicle).
	private boolean trueIfListOfUsersFalseIfListOfVehicules;
	
	private UtilisateurDTO utilisateur;
	private VehiculeDTO vehicule;
	private List<ReservationVehiculeUtilisateurDTO> reservationVehiculeUtilisateur;
	
	// Début et fin de la plage de requête.
	private DTO dateDebut;
	private DTO dateFin;
	
	// Used to show the popup when the requested reservation is in conflict with an other existing one.
	// If that's the case, then the result will contain the incriminated reservation only instead of the full list after update.
	// Conflict on user is non-blocking.
	// Conflict on vehicle is BLOCKING.
	private boolean conflictOnUserResponse = false;
	private boolean conflictOnVehicleResponse = false;
	
	// If set to true by the front, then authorize reservation for the same user, the same date and several vehicles.
	private boolean forceUpdateIfPossible = false;
	
	public PlanningUtilisateurVehiculeDTO() {
		
	}
	
	public PlanningUtilisateurVehiculeDTO(List<PlanningVehicule> planningsVehicules, LocalDateTime dateDebut, LocalDateTime dateFin, boolean isTrueIfListOfUsersFalseIfListOfVehicules) {
		this.dateDebut = DTO.parseFromLocalDateTime(dateDebut);
		this.dateFin = DTO.parseFromLocalDateTime(dateFin);
		if (planningsVehicules != null && planningsVehicules.size() > 0) {
			if (isTrueIfListOfUsersFalseIfListOfVehicules)
			{
				// This is reversed in the sub-listed object ReservationVehiculeUtilisateurDTO.
				// Here is the object we don't want to load multiple times in loop.
				this.vehicule = VehiculeDTO.toDTO(planningsVehicules.get(0).getVehicule());
			}
			else
			{
				// This is reversed in the sub-listed object ReservationVehiculeUtilisateurDTO.
				// Here is the object we don't want to load multiple times in loop.
				this.utilisateur = UtilisateurDTO.toDTO(planningsVehicules.get(0).getUtilisateur(), true);
			}
		}
		this.reservationVehiculeUtilisateur = ReservationVehiculeUtilisateurDTO.toDTOs(planningsVehicules, isTrueIfListOfUsersFalseIfListOfVehicules);
	}

	public static PlanningUtilisateurVehiculeDTO toDTO(List<PlanningVehicule> planningsVehicules, LocalDateTime dateDebut, LocalDateTime dateFin, boolean isTrueIfListOfUsersFalseIfListOfVehicules) {
		if (planningsVehicules == null) {
			return null;
		} else {
			return new PlanningUtilisateurVehiculeDTO(planningsVehicules, dateDebut, dateFin, isTrueIfListOfUsersFalseIfListOfVehicules);
		}
	}
	
	public boolean isTrueIfListOfUsersFalseIfListOfVehicules() {
		return trueIfListOfUsersFalseIfListOfVehicules;
	}

	public void setTrueIfListOfUsersFalseIfListOfVehicules(boolean trueIfListOfUsersFalseIfListOfVehicules) {
		this.trueIfListOfUsersFalseIfListOfVehicules = trueIfListOfUsersFalseIfListOfVehicules;
	}

	public UtilisateurDTO getUtilisateur() {
		return utilisateur;
	}
	
	public void setUtilisateur(UtilisateurDTO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public VehiculeDTO getVehicule() {
		return vehicule;
	}

	public void setVehicule(VehiculeDTO vehicule) {
		this.vehicule = vehicule;
	}

	public List<ReservationVehiculeUtilisateurDTO> getReservationVehiculeUtilisateur() {
		return reservationVehiculeUtilisateur;
	}

	public void setReservationVehiculeUtilisateur(List<ReservationVehiculeUtilisateurDTO> reservationVehiculeUtilisateur) {
		this.reservationVehiculeUtilisateur = reservationVehiculeUtilisateur;
	}

	public DTO getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(DTO dateDebut) {
		this.dateDebut = dateDebut;
	}

	public DTO getDateFin() {
		return dateFin;
	}

	public void setDateFin(DTO dateFin) {
		this.dateFin = dateFin;
	}

	public boolean isConflictOnUserResponse() {
		return conflictOnUserResponse;
	}

	public void setConflictOnUserResponse(boolean conflictOnUserResponse) {
		this.conflictOnUserResponse = conflictOnUserResponse;
	}

	public boolean isConflictOnVehicleResponse() {
		return conflictOnVehicleResponse;
	}

	public void setConflictOnVehicleResponse(boolean conflictOnVehicleResponse) {
		this.conflictOnVehicleResponse = conflictOnVehicleResponse;
	}

	public boolean isForceUpdateIfPossible() {
		return forceUpdateIfPossible;
	}

	public void setForceUpdateIfPossible(boolean forceUpdateIfPossible) {
		this.forceUpdateIfPossible = forceUpdateIfPossible;
	}
	
}
