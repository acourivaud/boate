package com.boate.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.boate.model.PlanningMateriel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanningUtilisateurMaterielDTO {

	// True if this object is used in a request to get all Users having booked a specific Vehicle.
	// False if this object is used in a request to get all Vehicles booked by a User.
	// Avoid loading the grouping object multiple times in loop (whether it's a User or Vehicle).
	private boolean trueIfListOfUsersFalseIfListOfMateriels;
	
	private UtilisateurDTO utilisateur;
	private MaterielDTO materiel;
	private List<ReservationMaterielUtilisateurDTO> reservationMaterielUtilisateur;
	
	// Début et fin de la plage de requête.
	private DTO dateDebut;
	private DTO dateFin;
	
	// Used to show the popup when the requested reservation is in conflict with an other existing one.
	// If that's the case, then the result will contain the incriminated reservation only instead of the full list after update.
	// Conflict on user is non-blocking.
	// Conflict on vehicle is BLOCKING.
	private boolean conflictOnUserResponse = false;
	private boolean conflictOnMaterielResponse = false;
	
	// If set to true by the front, then authorize reservation for the same user, the same date and several vehicles.
	private boolean forceUpdateIfPossible = false;
	
	public PlanningUtilisateurMaterielDTO() {
		
	}
	
	public PlanningUtilisateurMaterielDTO(List<PlanningMateriel> planningsMateriels, LocalDateTime dateDebut, LocalDateTime dateFin, boolean trueIfListOfUsersFalseIfListOfMateriels) {
		this.dateDebut = DTO.parseFromLocalDateTime(dateDebut);
		this.dateFin = DTO.parseFromLocalDateTime(dateFin);
		if (planningsMateriels != null && planningsMateriels.size() > 0) {
			if (trueIfListOfUsersFalseIfListOfMateriels)
			{
				// This is reversed in the sub-listed object ReservationVehiculeUtilisateurDTO.
				// Here is the object we don't want to load multiple times in loop.
				this.materiel = MaterielDTO.toDTO(planningsMateriels.get(0).getMateriel());
			}
			else
			{
				// This is reversed in the sub-listed object ReservationVehiculeUtilisateurDTO.
				// Here is the object we don't want to load multiple times in loop.
				this.utilisateur = UtilisateurDTO.toDTO(planningsMateriels.get(0).getUtilisateur(), true);
			}
		}
		this.reservationMaterielUtilisateur = ReservationMaterielUtilisateurDTO.toDTOs(planningsMateriels, trueIfListOfUsersFalseIfListOfMateriels);
	}

	public static PlanningUtilisateurMaterielDTO toDTO(List<PlanningMateriel> planningsMateriels, LocalDateTime dateDebut, LocalDateTime dateFin, boolean trueIfListOfUsersFalseIfListOfMateriels) {
		if (planningsMateriels == null) {
			return null;
		} else {
			return new PlanningUtilisateurMaterielDTO(planningsMateriels, dateDebut, dateFin, trueIfListOfUsersFalseIfListOfMateriels);
		}
	}

	public boolean isTrueIfListOfUsersFalseIfListOfMateriels() {
		return trueIfListOfUsersFalseIfListOfMateriels;
	}

	public void setTrueIfListOfUsersFalseIfListOfMateriels(boolean trueIfListOfUsersFalseIfListOfMateriels) {
		this.trueIfListOfUsersFalseIfListOfMateriels = trueIfListOfUsersFalseIfListOfMateriels;
	}

	public UtilisateurDTO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurDTO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public MaterielDTO getMateriel() {
		return materiel;
	}

	public void setMateriel(MaterielDTO materiel) {
		this.materiel = materiel;
	}

	public List<ReservationMaterielUtilisateurDTO> getReservationMaterielUtilisateur() {
		return reservationMaterielUtilisateur;
	}

	public void setReservationMaterielUtilisateur(List<ReservationMaterielUtilisateurDTO> reservationMaterielUtilisateur) {
		this.reservationMaterielUtilisateur = reservationMaterielUtilisateur;
	}

	public DTO getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(DTO dateDebut) {
		this.dateDebut = dateDebut;
	}

	public DTO getDateFin() {
		return dateFin;
	}

	public void setDateFin(DTO dateFin) {
		this.dateFin = dateFin;
	}

	public boolean isConflictOnUserResponse() {
		return conflictOnUserResponse;
	}

	public void setConflictOnUserResponse(boolean conflictOnUserResponse) {
		this.conflictOnUserResponse = conflictOnUserResponse;
	}

	public boolean isConflictOnMaterielResponse() {
		return conflictOnMaterielResponse;
	}

	public void setConflictOnMaterielResponse(boolean conflictOnMaterielResponse) {
		this.conflictOnMaterielResponse = conflictOnMaterielResponse;
	}

	public boolean isForceUpdateIfPossible() {
		return forceUpdateIfPossible;
	}

	public void setForceUpdateIfPossible(boolean forceUpdateIfPossible) {
		this.forceUpdateIfPossible = forceUpdateIfPossible;
	}	
}
