package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.enums.ParamModuleTypeEnum;
import com.boate.enums.ParamStatutEnum;
import com.boate.model.ParamModule;

public class ParamModuleDTO {

	private Integer id;
	private String libelleFonctionnel;
	private String libelleTechnique;
	private String statut;
	private String type;
	private String className;
	private List<ParamValueDTO> paramValues;
	private List<ParamTableColumnDefinitionDTO> tableColumnDefinitions;

	public ParamModuleDTO() {
		
	}
	
	public ParamModuleDTO(ParamModule paramModule) {
		this.id = paramModule.getId();
		this.libelleFonctionnel = paramModule.getLibelleFonctionnel();
		this.libelleTechnique = paramModule.getLibelleTechnique();
		this.statut = paramModule.getStatut() != null ? paramModule.getStatut().toString() : ParamStatutEnum.PREVISUALISATION.toString();
		this.type = paramModule.getType() != null ? paramModule.getType().toString() : ParamModuleTypeEnum.SPECIFIQUE.toString();
		this.paramValues = ParamValueDTO.toDTOs(paramModule.getParamValues());
		this.tableColumnDefinitions = ParamTableColumnDefinitionDTO.toDTOs(paramModule.getParamTableColumnDefinitions());
		this.className = paramModule.getClassName();
	}

	public static ParamModuleDTO toDTO(ParamModule paramModule) {
		if (paramModule == null) {
			return null;
		} else {
			return new ParamModuleDTO(paramModule);
		}
	}

	public static List<ParamModuleDTO> toDTOs(List<ParamModule> paramModules) {
		if (paramModules == null) {
			return new ArrayList<>(0);
		}
		List<ParamModuleDTO> paramModuleDTOs = new ArrayList<>(paramModules.size());
		for (ParamModule paramModule : paramModules) {
			paramModuleDTOs.add(ParamModuleDTO.toDTO(paramModule));
		}
		return paramModuleDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getLibelleFonctionnel() {
		return libelleFonctionnel;
	}

	public void setLibelleFonctionnel(String libelleFonctionnel) {
		this.libelleFonctionnel = libelleFonctionnel;
	}

	public String getLibelleTechnique() {
		return libelleTechnique;
	}

	public void setLibelleTechnique(String libelleTechnique) {
		this.libelleTechnique = libelleTechnique;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<ParamTableColumnDefinitionDTO> getTableColumnDefinitions() {
		return tableColumnDefinitions;
	}

	public void setTableColumnDefinitions(List<ParamTableColumnDefinitionDTO> tableColumnDefinitions) {
		this.tableColumnDefinitions = tableColumnDefinitions;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public List<ParamValueDTO> getParamValues() {
		return paramValues;
	}

	public void setParamValues(List<ParamValueDTO> paramValues) {
		this.paramValues = paramValues;
	}

}
