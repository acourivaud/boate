package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Region;

public class RegionDTO {

	private Integer id;
	private Integer code;
	private String name;

	public RegionDTO() {
		
	}
	
	public RegionDTO(Region region) {
		this.id = region.getId();
		this.code = region.getCode();
		this.name = region.getNom();
	}

	public static RegionDTO toDTO(Region region) {
		if (region == null) {
			return null;
		} else {
			return new RegionDTO(region);
		}
	}

	public static List<RegionDTO> toDTOs(List<Region> regions) {
		if (regions == null) {
			return new ArrayList<>(0);
		}
		List<RegionDTO> regionDTOs = new ArrayList<>(regions.size());
		for (Region region : regions) {
			regionDTOs.add(RegionDTO.toDTO(region));
		}
		return regionDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
