package com.boate.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.boate.model.Surveillance;

public class SurveillanceDTO {

	private Integer id;
	private OuvrageDTO ouvrage;
	private String compteAnalytique;
	private String codeSegment;
	private String moyensSurveillance;
	private String cv;
	private Boolean archive;
	
	private LocalDate dateDebut;
	private LocalDate dateFin;
	
	private LocalDate dateReclassement;
	private String motifReclassement;
	
	private Integer periodiciteId;
	private LocalDate lastDateId;
	private String auteurId;
	private Integer nextDateId;
	
	private Integer periodiciteVd;
	private LocalDate lastDateVd;
	private String auteurVd;
	private Integer nextDateVd;
	
	private Integer periodiciteVi;
	private LocalDate lastDateVi;
	private String auteurVi;
	private Integer nextDateVi;
	
	private Integer periodiciteVs;
	private LocalDate lastDateVs;
	private String auteurVs;
	private Integer nextDateVs;
	
	private Integer periodiciteVdOt;
	private LocalDate lastDateVdOt;
	private String auteurVdOt;
	private Integer nextDateVdOt;
	
	private Integer periodiciteViOt;
	private LocalDate lastDateViOt;
	private String auteurViOt;
	private Integer nextDateViOt;
	
	private Integer periodiciteVpOt;
	private LocalDate lastDateVpOt;
	private String auteurVpOt;
	private Integer nextDateVpOt;
	
	private Integer periodiciteVaOt;
	private LocalDate lastDateVaOt;
	private String auteurVaOt;
	private Integer nextDateVaOt;
	
	private String complementaryVisit;
	
	private String visiteN0;
	private String visiteN1;
	private String visiteN2;
	private String visiteN3;
	private String visiteN4;
	private String visiteN5;
	private String visiteN6;
	private String visiteN7;
	private String visiteN8;
	private String visiteN9;
	
	private String firstMeansKey;
	private String secondMeansKey;
	private String thirdMeansKey;
	
	private String typeSurveillance;

	public SurveillanceDTO() {
		
	}
	
	public SurveillanceDTO(Surveillance surveillance, String page) {
		switch (page) {
		case "listeSurveillances":
			this.id = surveillance.getId();
			this.ouvrage = OuvrageDTO.toDTO(surveillance.getOuvrage(), page);
			this.compteAnalytique = surveillance.getCodeAnalytique();
			this.codeSegment = surveillance.getCodeSegment();
			this.moyensSurveillance = surveillance.getFullMoyensSurveillance();
			this.cv = surveillance.getCv() != null ? surveillance.getCv().toString() : "";
			this.periodiciteId = surveillance.getPeriodiciteId();
			this.lastDateId = surveillance.getLastDateId();
			this.auteurId = surveillance.getAuteurId();
			this.nextDateId = surveillance.getNextDateId();
			
			this.dateDebut = surveillance.getDateDebut();
			this.dateFin = surveillance.getDateFin();
			this.dateReclassement = surveillance.getDateReclassement();
			this.motifReclassement = surveillance.getMotifReclassement();
			
			this.periodiciteVd = surveillance.getPeriodiciteVd();
			this.lastDateVd = surveillance.getLastDateVd();
			this.auteurVd = surveillance.getAuteurVd();
			this.nextDateVd = surveillance.getNextDateVd();
			
			this.periodiciteVi = surveillance.getPeriodiciteVi();
			this.lastDateVi = surveillance.getLastDateVi();
			this.auteurVi = surveillance.getAuteurVi();
			this.nextDateVi = surveillance.getNextDateVi();
			
			this.periodiciteVs = surveillance.getPeriodiciteVs();
			this.lastDateVs = surveillance.getLastDateVs();
			this.auteurVs = surveillance.getAuteurVs();
			this.nextDateVs = surveillance.getNextDateVs();
			
			this.periodiciteVdOt = surveillance.getPeriodiciteVdOt();
			this.lastDateVdOt = surveillance.getLastDateVdOt();
			this.auteurVdOt = surveillance.getAuteurVdOt();
			this.nextDateVdOt = surveillance.getNextDateVdOt();
			
			this.periodiciteViOt = surveillance.getPeriodiciteViOt();
			this.lastDateViOt = surveillance.getLastDateViOt();
			this.auteurViOt = surveillance.getAuteurViOt();
			this.nextDateViOt = surveillance.getNextDateViOt();
			
			this.periodiciteVpOt = surveillance.getPeriodiciteVpOt();
			this.lastDateVpOt = surveillance.getLastDateVpOt();
			this.auteurVpOt = surveillance.getAuteurVpOt();
			this.nextDateVpOt = surveillance.getNextDateVpOt();
			
			this.periodiciteVaOt = surveillance.getPeriodiciteVaOt();
			this.lastDateVaOt = surveillance.getLastDateVaOt();
			this.auteurVaOt = surveillance.getAuteurVaOt();
			this.nextDateVaOt = surveillance.getNextDateVaOt();
			
			this.complementaryVisit = surveillance.getComplementaryVisit();
			
			Integer currentYear = LocalDate.now().getYear()+1900;
			this.visiteN0 = surveillance.getVisitsForYear(currentYear);
			this.visiteN1 = surveillance.getVisitsForYear(currentYear+1);
			this.visiteN2 = surveillance.getVisitsForYear(currentYear+2);
			this.visiteN3 = surveillance.getVisitsForYear(currentYear+3);
			this.visiteN4 = surveillance.getVisitsForYear(currentYear+4);
			this.visiteN5 = surveillance.getVisitsForYear(currentYear+5);
			this.visiteN6 = surveillance.getVisitsForYear(currentYear+6);
			this.visiteN7 = surveillance.getVisitsForYear(currentYear+7);
			this.visiteN8 = surveillance.getVisitsForYear(currentYear+8);
			this.visiteN9 = surveillance.getVisitsForYear(currentYear+9);
			
			this.firstMeansKey = surveillance.getFirstMeansKey();
			this.secondMeansKey = surveillance.getSecondMeansKey();
			this.thirdMeansKey = surveillance.getThirdMeansKey();
			
			this.typeSurveillance = surveillance.getTypeSurveillance();
			break;
		case "listeBouclages":
			this.id = surveillance.getId();
			this.ouvrage = OuvrageDTO.toDTO(surveillance.getOuvrage(), page);
			this.cv = surveillance.getCv() != null && surveillance.getCv().equals(true) ? "Oui" : "Non";
			break;
		default :
			this.id = surveillance.getId();
		}
		
	}

	public static SurveillanceDTO toDTO(Surveillance surveillance, String page) {
		if (surveillance == null) {
			return null;
		} else {
			return new SurveillanceDTO(surveillance, page);
		}
	}

	public static List<SurveillanceDTO> toDTOs(List<Surveillance> surveillances, String page) {
		if (surveillances == null) {
			return new ArrayList<>(0);
		}
		List<SurveillanceDTO> surveillanceDTOs = new ArrayList<>(surveillances.size());
		for (Surveillance surveillance : surveillances) {
			surveillanceDTOs.add(SurveillanceDTO.toDTO(surveillance, page));
		}
		return surveillanceDTOs;
	}

	public Integer getId() {
		return id;
	}

	public OuvrageDTO getOuvrage() {
		return ouvrage;
	}

	public void setOuvrage(OuvrageDTO ouvrage) {
		this.ouvrage = ouvrage;
	}

	public String getCompteAnalytique() {
		return compteAnalytique;
	}

	public void setCompteAnalytique(String compteAnalytique) {
		this.compteAnalytique = compteAnalytique;
	}

	public String getCodeSegment() {
		return codeSegment;
	}

	public void setCodeSegment(String codeSegment) {
		this.codeSegment = codeSegment;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMoyensSurveillance() {
		return moyensSurveillance;
	}

	public void setMoyensSurveillance(String moyensSurveillance) {
		this.moyensSurveillance = moyensSurveillance;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public Boolean getArchive() {
		return archive;
	}

	public void setArchive(Boolean archive) {
		this.archive = archive;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public LocalDate getDateReclassement() {
		return dateReclassement;
	}

	public void setDateReclassement(LocalDate dateReclassement) {
		this.dateReclassement = dateReclassement;
	}

	public String getMotifReclassement() {
		return motifReclassement;
	}

	public void setMotifReclassement(String motifReclassement) {
		this.motifReclassement = motifReclassement;
	}

	public Integer getPeriodiciteId() {
		return periodiciteId;
	}

	public void setPeriodiciteId(Integer periodiciteId) {
		this.periodiciteId = periodiciteId;
	}

	public LocalDate getLastDateId() {
		return lastDateId;
	}

	public void setLastDateId(LocalDate lastDateId) {
		this.lastDateId = lastDateId;
	}

	public String getAuteurId() {
		return auteurId;
	}

	public void setAuteurId(String auteurId) {
		this.auteurId = auteurId;
	}

	public Integer getNextDateId() {
		return nextDateId;
	}

	public void setNextDateId(Integer nextDateId) {
		this.nextDateId = nextDateId;
	}

	public Integer getPeriodiciteVd() {
		return periodiciteVd;
	}

	public void setPeriodiciteVd(Integer periodiciteVd) {
		this.periodiciteVd = periodiciteVd;
	}

	public LocalDate getLastDateVd() {
		return lastDateVd;
	}

	public void setLastDateVd(LocalDate lastDateVd) {
		this.lastDateVd = lastDateVd;
	}

	public String getAuteurVd() {
		return auteurVd;
	}

	public void setAuteurVd(String auteurVd) {
		this.auteurVd = auteurVd;
	}

	public Integer getNextDateVd() {
		return nextDateVd;
	}

	public void setNextDateVd(Integer nextDateVd) {
		this.nextDateVd = nextDateVd;
	}

	public Integer getPeriodiciteVi() {
		return periodiciteVi;
	}

	public void setPeriodiciteVi(Integer periodiciteVi) {
		this.periodiciteVi = periodiciteVi;
	}

	public LocalDate getLastDateVi() {
		return lastDateVi;
	}

	public void setLastDateVi(LocalDate lastDateVi) {
		this.lastDateVi = lastDateVi;
	}

	public String getAuteurVi() {
		return auteurVi;
	}

	public void setAuteurVi(String auteurVi) {
		this.auteurVi = auteurVi;
	}

	public Integer getNextDateVi() {
		return nextDateVi;
	}

	public void setNextDateVi(Integer nextDateVi) {
		this.nextDateVi = nextDateVi;
	}

	public Integer getPeriodiciteVs() {
		return periodiciteVs;
	}

	public void setPeriodiciteVs(Integer periodiciteVs) {
		this.periodiciteVs = periodiciteVs;
	}

	public LocalDate getLastDateVs() {
		return lastDateVs;
	}

	public void setLastDateVs(LocalDate lastDateVs) {
		this.lastDateVs = lastDateVs;
	}

	public String getAuteurVs() {
		return auteurVs;
	}

	public void setAuteurVs(String auteurVs) {
		this.auteurVs = auteurVs;
	}

	public Integer getNextDateVs() {
		return nextDateVs;
	}

	public void setNextDateVs(Integer nextDateVs) {
		this.nextDateVs = nextDateVs;
	}

	public Integer getPeriodiciteVdOt() {
		return periodiciteVdOt;
	}

	public void setPeriodiciteVdOt(Integer periodiciteVdOt) {
		this.periodiciteVdOt = periodiciteVdOt;
	}

	public LocalDate getLastDateVdOt() {
		return lastDateVdOt;
	}

	public void setLastDateVdOt(LocalDate lastDateVdOt) {
		this.lastDateVdOt = lastDateVdOt;
	}

	public String getAuteurVdOt() {
		return auteurVdOt;
	}

	public void setAuteurVdOt(String auteurVdOt) {
		this.auteurVdOt = auteurVdOt;
	}

	public Integer getNextDateVdOt() {
		return nextDateVdOt;
	}

	public void setNextDateVdOt(Integer nextDateVdOt) {
		this.nextDateVdOt = nextDateVdOt;
	}

	public Integer getPeriodiciteViOt() {
		return periodiciteViOt;
	}

	public void setPeriodiciteViOt(Integer periodiciteViOt) {
		this.periodiciteViOt = periodiciteViOt;
	}

	public LocalDate getLastDateViOt() {
		return lastDateViOt;
	}

	public void setLastDateViOt(LocalDate lastDateViOt) {
		this.lastDateViOt = lastDateViOt;
	}

	public String getAuteurViOt() {
		return auteurViOt;
	}

	public void setAuteurViOt(String auteurViOt) {
		this.auteurViOt = auteurViOt;
	}

	public Integer getNextDateViOt() {
		return nextDateViOt;
	}

	public void setNextDateViOt(Integer nextDateViOt) {
		this.nextDateViOt = nextDateViOt;
	}

	public Integer getPeriodiciteVpOt() {
		return periodiciteVpOt;
	}

	public void setPeriodiciteVpOt(Integer periodiciteVpOt) {
		this.periodiciteVpOt = periodiciteVpOt;
	}

	public LocalDate getLastDateVpOt() {
		return lastDateVpOt;
	}

	public void setLastDateVpOt(LocalDate lastDateVpOt) {
		this.lastDateVpOt = lastDateVpOt;
	}

	public String getAuteurVpOt() {
		return auteurVpOt;
	}

	public void setAuteurVpOt(String auteurVpOt) {
		this.auteurVpOt = auteurVpOt;
	}

	public Integer getNextDateVpOt() {
		return nextDateVpOt;
	}

	public void setNextDateVpOt(Integer nextDateVpOt) {
		this.nextDateVpOt = nextDateVpOt;
	}

	public Integer getPeriodiciteVaOt() {
		return periodiciteVaOt;
	}

	public void setPeriodiciteVaOt(Integer periodiciteVaOt) {
		this.periodiciteVaOt = periodiciteVaOt;
	}

	public LocalDate getLastDateVaOt() {
		return lastDateVaOt;
	}

	public void setLastDateVaOt(LocalDate lastDateVaOt) {
		this.lastDateVaOt = lastDateVaOt;
	}

	public String getAuteurVaOt() {
		return auteurVaOt;
	}

	public void setAuteurVaOt(String auteurVaOt) {
		this.auteurVaOt = auteurVaOt;
	}

	public Integer getNextDateVaOt() {
		return nextDateVaOt;
	}

	public void setNextDateVaOt(Integer nextDateVaOt) {
		this.nextDateVaOt = nextDateVaOt;
	}

	public String getComplementaryVisit() {
		return complementaryVisit;
	}

	public void setComplementaryVisit(String complementaryVisit) {
		this.complementaryVisit = complementaryVisit;
	}

	public String getVisiteN0() {
		return visiteN0;
	}

	public void setVisiteN0(String visiteN0) {
		this.visiteN0 = visiteN0;
	}

	public String getVisiteN1() {
		return visiteN1;
	}

	public void setVisiteN1(String visiteN1) {
		this.visiteN1 = visiteN1;
	}

	public String getVisiteN2() {
		return visiteN2;
	}

	public void setVisiteN2(String visiteN2) {
		this.visiteN2 = visiteN2;
	}

	public String getVisiteN3() {
		return visiteN3;
	}

	public void setVisiteN3(String visiteN3) {
		this.visiteN3 = visiteN3;
	}

	public String getVisiteN4() {
		return visiteN4;
	}

	public void setVisiteN4(String visiteN4) {
		this.visiteN4 = visiteN4;
	}

	public String getVisiteN5() {
		return visiteN5;
	}

	public void setVisiteN5(String visiteN5) {
		this.visiteN5 = visiteN5;
	}

	public String getVisiteN6() {
		return visiteN6;
	}

	public void setVisiteN6(String visiteN6) {
		this.visiteN6 = visiteN6;
	}

	public String getVisiteN7() {
		return visiteN7;
	}

	public void setVisiteN7(String visiteN7) {
		this.visiteN7 = visiteN7;
	}

	public String getVisiteN8() {
		return visiteN8;
	}

	public void setVisiteN8(String visiteN8) {
		this.visiteN8 = visiteN8;
	}

	public String getVisiteN9() {
		return visiteN9;
	}

	public void setVisiteN9(String visiteN9) {
		this.visiteN9 = visiteN9;
	}

	public String getFirstMeansKey() {
		return firstMeansKey;
	}

	public void setFirstMeansKey(String firstMeansKey) {
		this.firstMeansKey = firstMeansKey;
	}

	public String getSecondMeansKey() {
		return secondMeansKey;
	}

	public void setSecondMeansKey(String secondMeansKey) {
		this.secondMeansKey = secondMeansKey;
	}

	public String getThirdMeansKey() {
		return thirdMeansKey;
	}

	public void setThirdMeansKey(String thirdMeansKey) {
		this.thirdMeansKey = thirdMeansKey;
	}

	public String getTypeSurveillance() {
		return typeSurveillance;
	}

	public void setTypeSurveillance(String typeSurveillance) {
		this.typeSurveillance = typeSurveillance;
	}

}
