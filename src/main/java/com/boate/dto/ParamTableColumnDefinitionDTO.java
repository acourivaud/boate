package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.ParamTableColumnDefinition;

public class ParamTableColumnDefinitionDTO {

	private Integer id;
	private List<ParamValueDTO> paramValues;
	private ParamChampDTO paramChamp;

	public ParamTableColumnDefinitionDTO() {
		
	}
	
	public ParamTableColumnDefinitionDTO(ParamTableColumnDefinition paramTableColumnDefinition) {
		this.id = paramTableColumnDefinition.getId();
		this.paramChamp = ParamChampDTO.toDTO(paramTableColumnDefinition.getParamChamp());
		this.paramValues = ParamValueDTO.toDTOs(paramTableColumnDefinition.getParamValues());
	}

	public static ParamTableColumnDefinitionDTO toDTO(ParamTableColumnDefinition paramTableColumnDefinition) {
		if (paramTableColumnDefinition == null) {
			return null;
		} else {
			return new ParamTableColumnDefinitionDTO(paramTableColumnDefinition);
		}
	}

	public static List<ParamTableColumnDefinitionDTO> toDTOs(List<ParamTableColumnDefinition> paramTableColumnDefinitions) {
		if (paramTableColumnDefinitions == null) {
			return new ArrayList<>(0);
		}
		List<ParamTableColumnDefinitionDTO> paramTableColumnDefinitionDTOs = new ArrayList<>(paramTableColumnDefinitions.size());
		for (ParamTableColumnDefinition paramTableColumnDefinition : paramTableColumnDefinitions) {
			paramTableColumnDefinitionDTOs.add(ParamTableColumnDefinitionDTO.toDTO(paramTableColumnDefinition));
		}
		return paramTableColumnDefinitionDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ParamChampDTO getParamChamp() {
		return paramChamp;
	}

	public void setParamChamp(ParamChampDTO paramChamp) {
		this.paramChamp = paramChamp;
	}

	public List<ParamValueDTO> getParamValues() {
		return paramValues;
	}

	public void setParamValues(List<ParamValueDTO> paramValues) {
		this.paramValues = paramValues;
	}

}
