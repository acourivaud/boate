package com.boate.dto;

public class LazyResultDTO {

	private Long count;
	private Object result;
	
	public LazyResultDTO() {
		
	}

	public LazyResultDTO(Long count, Object result) {
		this.count = count;
		this.result = result;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
}
