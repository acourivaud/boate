package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.enums.TemplateStatutEnum;
import com.boate.model.ParamOnglet;

public class ParamOngletDTO {

	private Integer id;
	private Integer position;
	private String libelle;
	private String statut;
	private List<ParamChapitreDTO> paramChapitres;

	public ParamOngletDTO() {
		
	}
	
	public ParamOngletDTO(ParamOnglet paramOnglet) {
		this.id = paramOnglet.getId();
		this.libelle = paramOnglet.getLibelle();
		this.statut = paramOnglet.getStatut() != null ? paramOnglet.getStatut().toString() : TemplateStatutEnum.BROUILLON.toString();
		this.position = paramOnglet.getPosition();
		this.paramChapitres = ParamChapitreDTO.toDTOs(paramOnglet.getParamChapitres());
	}

	public static ParamOngletDTO toDTO(ParamOnglet paramOnglet) {
		if (paramOnglet == null) {
			return null;
		} else {
			return new ParamOngletDTO(paramOnglet);
		}
	}

	public static List<ParamOngletDTO> toDTOs(List<ParamOnglet> paramOnglets) {
		if (paramOnglets == null) {
			return new ArrayList<>(0);
		}
		List<ParamOngletDTO> paramOngletDTOs = new ArrayList<>(paramOnglets.size());
		for (ParamOnglet paramOnglet : paramOnglets) {
			paramOngletDTOs.add(ParamOngletDTO.toDTO(paramOnglet));
		}
		return paramOngletDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public List<ParamChapitreDTO> getParamChapitres() {
		return paramChapitres;
	}

	public void setParamChapitres(List<ParamChapitreDTO> paramChapitres) {
		this.paramChapitres = paramChapitres;
	}

}
