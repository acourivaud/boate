package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.enums.TemplateStatutEnum;
import com.boate.model.ParamLigneDefinition;

public class ParamLigneDefinitionDTO {

	private Integer id;
	private Integer position;
	private String statut;
	private ParamChampDTO paramChamp;
	private Integer tailleGridLabel;
	private Integer tailleGridValue;
	private String labelValue;

	public ParamLigneDefinitionDTO() {
		
	}
	
	public ParamLigneDefinitionDTO(ParamLigneDefinition paramChampDefinition) {
		this.id = paramChampDefinition.getId();
		this.position = paramChampDefinition.getPosition();
		this.statut = paramChampDefinition.getStatut() != null ? paramChampDefinition.getStatut().toString() : TemplateStatutEnum.BROUILLON.toString();
		this.paramChamp = ParamChampDTO.toDTO(paramChampDefinition.getParamChamp());
		this.tailleGridLabel = paramChampDefinition.getTailleGridLabel();
		this.tailleGridValue = paramChampDefinition.getTailleGridValue();
		this.labelValue = paramChampDefinition.getLabelValue();
	}

	public static ParamLigneDefinitionDTO toDTO(ParamLigneDefinition paramChampDefinition) {
		if (paramChampDefinition == null) {
			return null;
		} else {
			return new ParamLigneDefinitionDTO(paramChampDefinition);
		}
	}

	public static List<ParamLigneDefinitionDTO> toDTOs(List<ParamLigneDefinition> paramChampDefinitions) {
		if (paramChampDefinitions == null) {
			return new ArrayList<>(0);
		}
		List<ParamLigneDefinitionDTO> paramChampDefinitionDTOs = new ArrayList<>(paramChampDefinitions.size());
		for (ParamLigneDefinition paramChampDefinition : paramChampDefinitions) {
			paramChampDefinitionDTOs.add(ParamLigneDefinitionDTO.toDTO(paramChampDefinition));
		}
		return paramChampDefinitionDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public ParamChampDTO getParamChamp() {
		return paramChamp;
	}

	public void setParamChamp(ParamChampDTO paramChamp) {
		this.paramChamp = paramChamp;
	}

	public Integer getTailleGridLabel() {
		return tailleGridLabel;
	}

	public void setTailleGridLabel(Integer tailleGridLabel) {
		this.tailleGridLabel = tailleGridLabel;
	}

	public Integer getTailleGridValue() {
		return tailleGridValue;
	}

	public void setTailleGridValue(Integer tailleGridValue) {
		this.tailleGridValue = tailleGridValue;
	}

	public String getLabelValue() {
		return labelValue;
	}

	public void setLabelValue(String labelValue) {
		this.labelValue = labelValue;
	}

}
