package com.boate.dto;

import java.util.ArrayList;
import java.util.List;
import com.boate.model.OuvrageConfortementDetection;

public class OuvrageConfortementDetectionDTO {

    private Integer id;
    private OuvrageDTO ouvrage;
	private Boolean ancrages;
	private Boolean betonProjete;
	private Boolean contrefortBeton;
	private Boolean chambreDeboulis;
	private Boolean clouage;
	private Boolean ecranFiletDetecteur;
	private Boolean ecranFiletProtecteur;
	private Boolean enrochements;
	private Boolean ecranRailTraverse;
	private Boolean gabion;
	private Boolean grillagePendu;
	private Boolean grillagePlaqueAncre;
	private Boolean instrumentation;
	private Boolean injection;
	private Boolean masqueDrainant;
	private Boolean masquePoids;
	private Boolean murPeller;
	private Boolean pieux;
	private Boolean merlonPneusol;
	private Boolean trancheeDrainante;
	private Boolean maconnerie;

	public OuvrageConfortementDetectionDTO() {
		
	}
	
    public OuvrageConfortementDetectionDTO(OuvrageConfortementDetection ouvrageConfortementDetection) {
    	this.id = ouvrageConfortementDetection.getId();
        //this.ouvrage = OuvrageDTO.toDTO(ouvrageConfortementDetection.getOuvrage(), "simplifie");
    	this.ancrages = ouvrageConfortementDetection.getAncrages();
    	this.betonProjete = ouvrageConfortementDetection.getBetonProjete();
    	this.contrefortBeton = ouvrageConfortementDetection.getContrefortBeton();
    	this.chambreDeboulis = ouvrageConfortementDetection.getChambreDeboulis();
    	this.clouage = ouvrageConfortementDetection.getClouage();
    	this.ecranFiletDetecteur = ouvrageConfortementDetection.getEcranFiletDetecteur();
    	this.ecranFiletProtecteur = ouvrageConfortementDetection.getEcranFiletProtecteur();
    	this.enrochements = ouvrageConfortementDetection.getEnrochements();
    	this.ecranRailTraverse = ouvrageConfortementDetection.getEcranRailTraverse();
    	this.gabion = ouvrageConfortementDetection.getGabion();
    	this.grillagePendu = ouvrageConfortementDetection.getGrillagePendu();
    	this.grillagePlaqueAncre = ouvrageConfortementDetection.getGrillagePlaqueAncre();
    	this.instrumentation = ouvrageConfortementDetection.getInstrumentation();
    	this.injection = ouvrageConfortementDetection.getInjection();
    	this.masqueDrainant = ouvrageConfortementDetection.getMasqueDrainant();
    	this.masquePoids = ouvrageConfortementDetection.getMasquePoids();
    	this.murPeller = ouvrageConfortementDetection.getMurPeller();
    	this.pieux = ouvrageConfortementDetection.getPieux();
    	this.merlonPneusol = ouvrageConfortementDetection.getMerlonPneusol();
    	this.trancheeDrainante = ouvrageConfortementDetection.getTrancheeDrainante();
    	this.maconnerie = ouvrageConfortementDetection.getMaconnerie();
    }

    public static OuvrageConfortementDetectionDTO toDTO(OuvrageConfortementDetection ouvrageConfortementDetection) {
        if (ouvrageConfortementDetection == null) {
            return null;
        } else {
            return new OuvrageConfortementDetectionDTO(ouvrageConfortementDetection);
        }
    }

    public static List<OuvrageConfortementDetectionDTO> toDTOs(List<OuvrageConfortementDetection> ouvrageConfortementDetections) {
        if (ouvrageConfortementDetections == null) {
            return new ArrayList<>(0);
        }
        List<OuvrageConfortementDetectionDTO> ouvrageConfortementDetectionDTOs = new ArrayList<>(ouvrageConfortementDetections.size());
        for (OuvrageConfortementDetection ouvrageConfortementDetection : ouvrageConfortementDetections) {
            ouvrageConfortementDetectionDTOs.add(OuvrageConfortementDetectionDTO.toDTO(ouvrageConfortementDetection));
        }
        return ouvrageConfortementDetectionDTOs;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

	public OuvrageDTO getOuvrage() {
		return ouvrage;
	}

	public void setOuvrage(OuvrageDTO ouvrage) {
		this.ouvrage = ouvrage;
	}

	public Boolean getAncrages() {
		return ancrages;
	}

	public void setAncrages(Boolean ancrages) {
		this.ancrages = ancrages;
	}

	public Boolean getBetonProjete() {
		return betonProjete;
	}

	public void setBetonProjete(Boolean betonProjete) {
		this.betonProjete = betonProjete;
	}

	public Boolean getContrefortBeton() {
		return contrefortBeton;
	}

	public void setContrefortBeton(Boolean contrefortBeton) {
		this.contrefortBeton = contrefortBeton;
	}

	public Boolean getChambreDeboulis() {
		return chambreDeboulis;
	}

	public void setChambreDeboulis(Boolean chambreDeboulis) {
		this.chambreDeboulis = chambreDeboulis;
	}

	public Boolean getClouage() {
		return clouage;
	}

	public void setClouage(Boolean clouage) {
		this.clouage = clouage;
	}

	public Boolean getEcranFiletDetecteur() {
		return ecranFiletDetecteur;
	}

	public void setEcranFiletDetecteur(Boolean ecranFiletDetecteur) {
		this.ecranFiletDetecteur = ecranFiletDetecteur;
	}

	public Boolean getEcranFiletProtecteur() {
		return ecranFiletProtecteur;
	}

	public void setEcranFiletProtecteur(Boolean ecranFiletProtecteur) {
		this.ecranFiletProtecteur = ecranFiletProtecteur;
	}

	public Boolean getEnrochements() {
		return enrochements;
	}

	public void setEnrochements(Boolean enrochements) {
		this.enrochements = enrochements;
	}

	public Boolean getEcranRailTraverse() {
		return ecranRailTraverse;
	}

	public void setEcranRailTraverse(Boolean ecranRailTraverse) {
		this.ecranRailTraverse = ecranRailTraverse;
	}

	public Boolean getGabion() {
		return gabion;
	}

	public void setGabion(Boolean gabion) {
		this.gabion = gabion;
	}

	public Boolean getGrillagePendu() {
		return grillagePendu;
	}

	public void setGrillagePendu(Boolean grillagePendu) {
		this.grillagePendu = grillagePendu;
	}

	public Boolean getGrillagePlaqueAncre() {
		return grillagePlaqueAncre;
	}

	public void setGrillagePlaqueAncre(Boolean grillagePlaqueAncre) {
		this.grillagePlaqueAncre = grillagePlaqueAncre;
	}

	public Boolean getInstrumentation() {
		return instrumentation;
	}

	public void setInstrumentation(Boolean instrumentation) {
		this.instrumentation = instrumentation;
	}

	public Boolean getInjection() {
		return injection;
	}

	public void setInjection(Boolean injection) {
		this.injection = injection;
	}

	public Boolean getMasqueDrainant() {
		return masqueDrainant;
	}

	public void setMasqueDrainant(Boolean masqueDrainant) {
		this.masqueDrainant = masqueDrainant;
	}

	public Boolean getMasquePoids() {
		return masquePoids;
	}

	public void setMasquePoids(Boolean masquePoids) {
		this.masquePoids = masquePoids;
	}

	public Boolean getMurPeller() {
		return murPeller;
	}

	public void setMurPeller(Boolean murPeller) {
		this.murPeller = murPeller;
	}

	public Boolean getPieux() {
		return pieux;
	}

	public void setPieux(Boolean pieux) {
		this.pieux = pieux;
	}

	public Boolean getMerlonPneusol() {
		return merlonPneusol;
	}

	public void setMerlonPneusol(Boolean merlonPneusol) {
		this.merlonPneusol = merlonPneusol;
	}

	public Boolean getTrancheeDrainante() {
		return trancheeDrainante;
	}

	public void setTrancheeDrainante(Boolean trancheeDrainante) {
		this.trancheeDrainante = trancheeDrainante;
	}

	public Boolean getMaconnerie() {
		return maconnerie;
	}

	public void setMaconnerie(Boolean maconnerie) {
		this.maconnerie = maconnerie;
	}

}
