package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.enums.TemplateStatutEnum;
import com.boate.model.ParamLigne;

public class ParamLigneDTO {

	private Integer id;
	private Integer position;
	private String statut;
	private List<ParamLigneDefinitionDTO> paramLigneDefinitions;

	public ParamLigneDTO() {
		
	}
	
	public ParamLigneDTO(ParamLigne paramLigne) {
		this.id = paramLigne.getId();
		this.position = paramLigne.getPosition();
		this.statut = paramLigne.getStatut() != null ? paramLigne.getStatut().toString() : TemplateStatutEnum.BROUILLON.toString();
		this.paramLigneDefinitions = ParamLigneDefinitionDTO.toDTOs(paramLigne.getParamLigneDefinitions());
	}

	public static ParamLigneDTO toDTO(ParamLigne paramLigne) {
		if (paramLigne == null) {
			return null;
		} else {
			return new ParamLigneDTO(paramLigne);
		}
	}

	public static List<ParamLigneDTO> toDTOs(List<ParamLigne> paramLignes) {
		if (paramLignes == null) {
			return new ArrayList<>(0);
		}
		List<ParamLigneDTO> paramLigneDTOs = new ArrayList<>(paramLignes.size());
		for (ParamLigne paramLigne : paramLignes) {
			paramLigneDTOs.add(ParamLigneDTO.toDTO(paramLigne));
		}
		return paramLigneDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public List<ParamLigneDefinitionDTO> getParamLigneDefinitions() {
		return paramLigneDefinitions;
	}

	public void setParamLigneDefinitions(List<ParamLigneDefinitionDTO> paramLigneDefinitions) {
		this.paramLigneDefinitions = paramLigneDefinitions;
	}

}
