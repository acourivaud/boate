package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.PlanningCollaborateur;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReservationCollaborateurDTO {

	private Integer idPlanningCollaborateur;
	
	private DTO dateDebut;
	private DTO dateFin;
	
	private Boolean nuit;
	private String lieu;
	private String type;
	
	public ReservationCollaborateurDTO() {
		
	}
	
	public ReservationCollaborateurDTO(PlanningCollaborateur planningCollaborateur) {
		this.idPlanningCollaborateur = planningCollaborateur.getId();
		this.dateDebut = DTO.parseFromLocalDateTime(planningCollaborateur.getDateDebut());
		this.dateFin = DTO.parseFromLocalDateTime(planningCollaborateur.getDateFin());
		this.lieu = planningCollaborateur.getLieu();
		this.nuit = planningCollaborateur.getNuit();
		this.type = planningCollaborateur.getType();
	}

	public static ReservationCollaborateurDTO toDTO(PlanningCollaborateur planningCollaborateur) {
		if (planningCollaborateur == null) {
			return null;
		} else {
			return new ReservationCollaborateurDTO(planningCollaborateur);
		}
	}

	public static List<ReservationCollaborateurDTO> toDTOs(List<PlanningCollaborateur> planningCollaborateurs) {
		if (planningCollaborateurs == null) {
			return new ArrayList<>(0);
		}
		List<ReservationCollaborateurDTO> datesCollaborateurDTO = new ArrayList<>(planningCollaborateurs.size());
		for (PlanningCollaborateur planningCollaborateur : planningCollaborateurs) {
			datesCollaborateurDTO.add(ReservationCollaborateurDTO.toDTO(planningCollaborateur));
		}
		return datesCollaborateurDTO;
	}

	public Integer getIdPlanningCollaborateur() {
		return idPlanningCollaborateur;
	}

	public void setIdPlanningCollaborateur(Integer idPlanningCollaborateur) {
		this.idPlanningCollaborateur = idPlanningCollaborateur;
	}

	public DTO getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(DTO dateDebut) {
		this.dateDebut = dateDebut;
	}

	public DTO getDateFin() {
		return dateFin;
	}

	public void setDateFin(DTO dateFin) {
		this.dateFin = dateFin;
	}

	public Boolean getNuit() {
		return nuit;
	}

	public void setNuit(Boolean nuit) {
		this.nuit = nuit;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
