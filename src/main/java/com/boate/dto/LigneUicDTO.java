package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.LigneUic;

public class LigneUicDTO {

	private Integer id;
	private String libelleLigne;
	private String uic;
	private String pkDebut;
	private String pkFin;
	private LigneDTO ligne;

	public LigneUicDTO() {
		
	}
	
	public LigneUicDTO(LigneUic ligneUic) {
		this.id = ligneUic.getId();
		this.libelleLigne = ligneUic.getLibelleLigne();
		this.uic = ligneUic.getUic();
		this.pkDebut = ligneUic.getPkDebut();
		this.pkFin = ligneUic.getPkFin();
		this.ligne = LigneDTO.toDTO(ligneUic.getLigne(), true);
	}

	public static LigneUicDTO toDTO(LigneUic ligneUic) {
		if (ligneUic == null) {
			return null;
		} else {
			return new LigneUicDTO(ligneUic);
		}
	}

	public static List<LigneUicDTO> toDTOs(List<LigneUic> ligneUics) {
		if (ligneUics == null) {
			return new ArrayList<>(0);
		}
		List<LigneUicDTO> ligneUicDTOs = new ArrayList<>(ligneUics.size());
		for (LigneUic ligneUic : ligneUics) {
			ligneUicDTOs.add(LigneUicDTO.toDTO(ligneUic));
		}
		return ligneUicDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPkDebut() {
		return pkDebut;
	}

	public void setPkDebut(String pkDebut) {
		this.pkDebut = pkDebut;
	}

	public String getPkFin() {
		return pkFin;
	}

	public void setPkFin(String pkFin) {
		this.pkFin = pkFin;
	}

	public LigneDTO getLigne() {
		return ligne;
	}

	public void setLigne(LigneDTO ligne) {
		this.ligne = ligne;
	}

	public String getUic() {
		return uic;
	}

	public void setUic(String uic) {
		this.uic = uic;
	}

	public String getLibelleLigne() {
		return libelleLigne;
	}

	public void setLibelleLigne(String libelleLigne) {
		this.libelleLigne = libelleLigne;
	}

}
