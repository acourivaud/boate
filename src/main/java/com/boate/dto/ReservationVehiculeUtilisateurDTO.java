package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.PlanningVehicule;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReservationVehiculeUtilisateurDTO {

	private Integer idPlanningVehicule;
	
	// True if this object is used in a request to get all Users having booked a specific Vehicle.
	// False if this object is used in a request to get all Vehicles booked by a User.
	// Avoid loading the grouping object multiple times in loop (whether it's a User or Vehicle).
	private boolean trueIfListOfUsersFalseIfListOfVehicules;
	
	private VehiculeDTO vehicule;
	private UtilisateurDTO utilisateur;
	private DTO dateDebut;
	private DTO dateFin;
	
	public ReservationVehiculeUtilisateurDTO() {
		
	}
	
	public ReservationVehiculeUtilisateurDTO(PlanningVehicule planningVehicule, boolean trueIfListOfUsersFalseIfListOfVehicules) {
		this.trueIfListOfUsersFalseIfListOfVehicules = trueIfListOfUsersFalseIfListOfVehicules;
		this.idPlanningVehicule = planningVehicule.getId();
		if (trueIfListOfUsersFalseIfListOfVehicules)
		{
			this.utilisateur = UtilisateurDTO.toDTO(planningVehicule.getUtilisateur(), true);
		}
		else
		{
			this.vehicule = VehiculeDTO.toDTO(planningVehicule.getVehicule());
		}
		this.dateDebut = DTO.parseFromLocalDateTime(planningVehicule.getDateDebut());
		this.dateFin = DTO.parseFromLocalDateTime(planningVehicule.getDateFin());
	}

	public static ReservationVehiculeUtilisateurDTO toDTO(PlanningVehicule planningVehicule, boolean isTrueIfListOfUsersFalseIfListOfVehicules) {
		if (planningVehicule == null) {
			return null;
		} else {
			return new ReservationVehiculeUtilisateurDTO(planningVehicule, isTrueIfListOfUsersFalseIfListOfVehicules);
		}
	}

	public static List<ReservationVehiculeUtilisateurDTO> toDTOs(List<PlanningVehicule> planningVehicules, boolean isTrueIfListOfUsersFalseIfListOfVehicules) {
		if (planningVehicules == null) {
			return new ArrayList<>(0);
		}
		List<ReservationVehiculeUtilisateurDTO> datesVehiculeDTO = new ArrayList<>(planningVehicules.size());
		for (PlanningVehicule planningVehicule : planningVehicules) {
			datesVehiculeDTO.add(ReservationVehiculeUtilisateurDTO.toDTO(planningVehicule, isTrueIfListOfUsersFalseIfListOfVehicules));
		}
		return datesVehiculeDTO;
	}

	public Integer getIdPlanningVehicule() {
		return idPlanningVehicule;
	}

	public void setIdPlanningVehicule(Integer idPlanningVehicule) {
		this.idPlanningVehicule = idPlanningVehicule;
	}

	public boolean isTrueIfListOfUsersFalseIfListOfVehicules() {
		return trueIfListOfUsersFalseIfListOfVehicules;
	}

	public void setTrueIfListOfUsersFalseIfListOfVehicules(boolean trueIfListOfUsersFalseIfListOfVehicules) {
		this.trueIfListOfUsersFalseIfListOfVehicules = 	trueIfListOfUsersFalseIfListOfVehicules;
	}

	public VehiculeDTO getVehicule() {
		return vehicule;
	}

	public void setVehicule(VehiculeDTO vehicule) {
		this.vehicule = vehicule;
	}

	public UtilisateurDTO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurDTO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public DTO getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(DTO dateDebut) {
		this.dateDebut = dateDebut;
	}

	public DTO getDateFin() {
		return dateFin;
	}

	public void setDateFin(DTO dateFin) {
		this.dateFin = dateFin;
	}
}
