package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.ArchiveDossier;

public class ArchiveDossierDTO {

	private Integer id;
	private ArchiveBoiteDTO boite;
	private OuvrageDTO ouvrage;
	private UtilisateurDTO emprunteur;
	private String numeroDossier;
	private String service;
	private String description;
	private Integer anneeDebut;
	private Integer anneeFin;
	private String dua;
	private String sortFinale;
	private String commentaires;
	private String pk;
	private String endPk;
	private String ligneNonRattachee;
	private String typeArchive;

	public ArchiveDossierDTO() {
		
	}

	public ArchiveDossierDTO(ArchiveDossier archiveDossier, String page) {
		switch (page) {
		case "listeArchives":
			this.id = archiveDossier.getId();
			this.boite = ArchiveBoiteDTO.toDTO(archiveDossier.getBoite());
			this.ouvrage = OuvrageDTO.toDTO(archiveDossier.getOuvrage(), page);
			this.description = archiveDossier.getDescription();
			this.commentaires = archiveDossier.getCommentaires();
			this.pk = this.ouvrage != null ? null : archiveDossier.getPk();
			this.endPk = this.ouvrage != null ? null : archiveDossier.getEndPk();
			this.ligneNonRattachee = this.ouvrage != null ? null : archiveDossier.getLigneNonRattachee();
			this.typeArchive = archiveDossier.getTypeArchive();
			this.emprunteur = UtilisateurDTO.toDTO(archiveDossier.getEmprunteur(), true);
		case "listeEmprunt":
			this.id = archiveDossier.getId();
		default :
			this.id = archiveDossier.getId();
			this.boite = ArchiveBoiteDTO.toDTO(archiveDossier.getBoite());
			this.ouvrage = OuvrageDTO.toDTO(archiveDossier.getOuvrage(), "listeArchives");
			this.numeroDossier = archiveDossier.getNumeroDossier();
			this.service = archiveDossier.getService();
			this.description = archiveDossier.getDescription();
			this.anneeDebut = archiveDossier.getAnneeDebut();
			this.anneeFin = archiveDossier.getAnneeFin();
			this.dua = archiveDossier.getDua();
			this.sortFinale = archiveDossier.getSortFinale();
			this.commentaires = archiveDossier.getCommentaires();
			this.pk = this.ouvrage != null ? null : archiveDossier.getPk();
			this.endPk = this.ouvrage != null ? null : archiveDossier.getEndPk();
			this.ligneNonRattachee = this.ouvrage != null ? null : archiveDossier.getLigneNonRattachee();
			this.typeArchive = archiveDossier.getTypeArchive();
		}
		
	}

	public static ArchiveDossierDTO toDTO(ArchiveDossier archiveDossier, String page) {
		if (archiveDossier == null) {
			return null;
		} else {
			return new ArchiveDossierDTO(archiveDossier, page);
		}
	}

	public static List<ArchiveDossierDTO> toDTOs(List<ArchiveDossier> archiveDossiers, String page) {
		if (archiveDossiers == null) {
			return new ArrayList<>(0);
		}
		List<ArchiveDossierDTO> archiveDossierDTOs = new ArrayList<>(archiveDossiers.size());
		for (ArchiveDossier archiveDossier : archiveDossiers) {
			archiveDossierDTOs.add(ArchiveDossierDTO.toDTO(archiveDossier, page));
		}
		return archiveDossierDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ArchiveBoiteDTO getBoite() {
		return boite;
	}

	public void setBoite(ArchiveBoiteDTO boite) {
		this.boite = boite;
	}

	public OuvrageDTO getOuvrage() {
		return ouvrage;
	}

	public void setOuvrage(OuvrageDTO ouvrage) {
		this.ouvrage = ouvrage;
	}

	public String getNumeroDossier() {
		return numeroDossier;
	}

	public void setNumeroDossier(String numeroDossier) {
		this.numeroDossier = numeroDossier;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getAnneeDebut() {
		return anneeDebut;
	}

	public void setAnneeDebut(Integer anneeDebut) {
		this.anneeDebut = anneeDebut;
	}

	public Integer getAnneeFin() {
		return anneeFin;
	}

	public void setAnneeFin(Integer anneeFin) {
		this.anneeFin = anneeFin;
	}

	public String getDua() {
		return dua;
	}

	public void setDua(String dua) {
		this.dua = dua;
	}

	public String getSortFinale() {
		return sortFinale;
	}

	public void setSortFinale(String sortFinale) {
		this.sortFinale = sortFinale;
	}

	public String getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}

	public String getPk() {
		return pk;
	}

	public void setPk(String pk) {
		this.pk = pk;
	}

	public String getEndPk() {
		return endPk;
	}

	public void setEndPk(String endPk) {
		this.endPk = endPk;
	}

	public String getLigneNonRattachee() {
		return ligneNonRattachee;
	}

	public void setLigneNonRattachee(String ligneNonRattachee) {
		this.ligneNonRattachee = ligneNonRattachee;
	}

	public String getTypeArchive() {
		return typeArchive;
	}

	public void setTypeArchive(String typeArchive) {
		this.typeArchive = typeArchive;
	}

	public UtilisateurDTO getEmprunteur() {
		return emprunteur;
	}

	public void setEmprunteur(UtilisateurDTO emprunteur) {
		this.emprunteur = emprunteur;
	}

}
