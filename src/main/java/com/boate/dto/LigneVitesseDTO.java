package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.LigneVitesse;

public class LigneVitesseDTO {

	private Integer id;
	private String pkDebut;
	private String pkFin;
	private LigneDTO ligne;
	private String vitesse;
	private String voie;

	public LigneVitesseDTO() {
		
	}
	
	public LigneVitesseDTO(LigneVitesse ligneVitesse) {
		this.id = ligneVitesse.getId();
		this.pkDebut = ligneVitesse.getPkDebut();
		this.pkFin = ligneVitesse.getPkFin();
		this.ligne = LigneDTO.toDTO(ligneVitesse.getLigne(), true);
		this.vitesse = ligneVitesse.getVitesse();
		this.voie = ligneVitesse.getVoie();
	}

	public static LigneVitesseDTO toDTO(LigneVitesse ligneVitesse) {
		if (ligneVitesse == null) {
			return null;
		} else {
			return new LigneVitesseDTO(ligneVitesse);
		}
	}

	public static List<LigneVitesseDTO> toDTOs(List<LigneVitesse> ligneVitesses) {
		if (ligneVitesses == null) {
			return new ArrayList<>(0);
		}
		List<LigneVitesseDTO> ligneVitesseDTOs = new ArrayList<>(ligneVitesses.size());
		for (LigneVitesse ligneVitesse : ligneVitesses) {
			ligneVitesseDTOs.add(LigneVitesseDTO.toDTO(ligneVitesse));
		}
		return ligneVitesseDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPkDebut() {
		return pkDebut;
	}

	public void setPkDebut(String pkDebut) {
		this.pkDebut = pkDebut;
	}

	public String getPkFin() {
		return pkFin;
	}

	public void setPkFin(String pkFin) {
		this.pkFin = pkFin;
	}

	public LigneDTO getLigne() {
		return ligne;
	}

	public void setLigne(LigneDTO ligne) {
		this.ligne = ligne;
	}

	public String getVitesse() {
		return vitesse;
	}

	public void setVitesse(String vitesse) {
		this.vitesse = vitesse;
	}

	public String getVoie() {
		return voie;
	}

	public void setVoie(String voie) {
		this.voie = voie;
	}

}
