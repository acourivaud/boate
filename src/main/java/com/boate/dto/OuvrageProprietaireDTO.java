package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.OuvrageProprietaire;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OuvrageProprietaireDTO {

	private Integer id;
	private OuvrageDTO ouvrage;
	private ProprietaireDTO proprietaire;
	private Double pourcentage;
	private Integer ordre;
	private String className;
	private String classNameJava;

	public OuvrageProprietaireDTO() {
		
	}
	
	public OuvrageProprietaireDTO(OuvrageProprietaire ouvrageProprietaire) {
		this.id = ouvrageProprietaire.getId();
		this.proprietaire = ProprietaireDTO.toDTO(ouvrageProprietaire.getProprietaire());
		this.pourcentage = ouvrageProprietaire.getPourcentage();
		this.ordre = ouvrageProprietaire.getOrdre();
		this.classNameJava = "com.boate.dto.OuvrageProprietaireDTO";
	}

	public static OuvrageProprietaireDTO toDTO(OuvrageProprietaire ouvrageProprietaire) {
		if (ouvrageProprietaire == null) {
			return null;
		} else {
			return new OuvrageProprietaireDTO(ouvrageProprietaire);
		}
	}

	public static List<OuvrageProprietaireDTO> toDTOs(List<OuvrageProprietaire> ouvrageProprietaires) {
		if (ouvrageProprietaires == null) {
			return new ArrayList<>(0);
		}
		List<OuvrageProprietaireDTO> ouvrageProprietaireDTOs = new ArrayList<>(ouvrageProprietaires.size());
		for (OuvrageProprietaire ouvrageProprietaire : ouvrageProprietaires) {
			ouvrageProprietaireDTOs.add(OuvrageProprietaireDTO.toDTO(ouvrageProprietaire));
		}
		return ouvrageProprietaireDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public OuvrageDTO getOuvrage() {
		return ouvrage;
	}

	public void setOuvrage(OuvrageDTO ouvrage) {
		this.ouvrage = ouvrage;
	}

	public ProprietaireDTO getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(ProprietaireDTO proprietaire) {
		this.proprietaire = proprietaire;
	}

	public Double getPourcentage() {
		return pourcentage;
	}

	public void setPourcentage(Double pourcentage) {
		this.pourcentage = pourcentage;
	}

	public Integer getOrdre() {
		return ordre;
	}

	public void setOrdre(Integer ordre) {
		this.ordre = ordre;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getClassNameJava() {
		return classNameJava;
	}

	public void setClassNameJava(String classNameJava) {
		this.classNameJava = classNameJava;
	}

}
