package com.boate.dto;

import java.util.ArrayList;
import java.util.List;

import com.boate.model.Infrapole;

public class InfrapoleDTO {

	private Integer id;
	private String code;
	private String name;
	private RegionDTO region;
	private Boolean usesSecteurSubLevel;

	public InfrapoleDTO() {
		
	}
	
	public InfrapoleDTO(Infrapole infrapole, Boolean includeRegion) {
		this.id = infrapole.getId();
		this.code = infrapole.getCode();
		this.name = infrapole.getNom();
		this.usesSecteurSubLevel = infrapole.getUsesSecteurSubLevel();
		if (includeRegion) {
			this.region = RegionDTO.toDTO(infrapole.getRegion());
		}
	}

	public static InfrapoleDTO toDTO(Infrapole infrapole, Boolean includeRegion) {
		if (infrapole == null) {
			return null;
		} else {
			return new InfrapoleDTO(infrapole, includeRegion);
		}
	}

	public static List<InfrapoleDTO> toDTOs(List<Infrapole> infrapoles, Boolean includeRegion) {
		if (infrapoles == null) {
			return new ArrayList<>(0);
		}
		List<InfrapoleDTO> infrapoleDTOs = new ArrayList<>(infrapoles.size());
		for (Infrapole infrapole : infrapoles) {
			infrapoleDTOs.add(InfrapoleDTO.toDTO(infrapole, includeRegion));
		}
		return infrapoleDTOs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RegionDTO getRegion() {
		return region;
	}

	public void setRegion(RegionDTO region) {
		this.region = region;
	}

	public Boolean getUsesSecteurSubLevel() {
		return usesSecteurSubLevel;
	}

	public void setUsesSecteurSubLevel(Boolean usesSecteurSubLevel) {
		this.usesSecteurSubLevel = usesSecteurSubLevel;
	}

}
