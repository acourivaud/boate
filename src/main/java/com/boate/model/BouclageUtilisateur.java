package com.boate.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "work_visit_author")
public class BouclageUtilisateur {

	@Id
	@SequenceGenerator(name = "work_visit_author_id_seq", sequenceName = "work_visit_author_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "work_visit_author_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "work_visit_id", nullable = false)
	@JsonIgnore
	private Bouclage bouclage;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date00", nullable = true)
	private Utilisateur date00;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date01", nullable = true)
	private Utilisateur date01;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date02", nullable = true)
	private Utilisateur date02;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date03", nullable = true)
	private Utilisateur date03;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date04", nullable = true)
	private Utilisateur date04;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date05", nullable = true)
	private Utilisateur date05;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date06", nullable = true)
	private Utilisateur date06;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date07", nullable = true)
	private Utilisateur date07;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date08", nullable = true)
	private Utilisateur date08;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date09", nullable = true)
	private Utilisateur date09;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date10", nullable = true)
	private Utilisateur date10;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date11", nullable = true)
	private Utilisateur date11;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date12", nullable = true)
	private Utilisateur date12;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date13", nullable = true)
	private Utilisateur date13;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date14", nullable = true)
	private Utilisateur date14;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date15", nullable = true)
	private Utilisateur date15;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date16", nullable = true)
	private Utilisateur date16;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date17", nullable = true)
	private Utilisateur date17;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date18", nullable = true)
	private Utilisateur date18;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date19", nullable = true)
	private Utilisateur date19;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date20", nullable = true)
	private Utilisateur date20;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date21", nullable = true)
	private Utilisateur date21;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date22", nullable = true)
	private Utilisateur date22;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date23", nullable = true)
	private Utilisateur date23;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date24", nullable = true)
	private Utilisateur date24;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date25", nullable = true)
	private Utilisateur date25;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date26", nullable = true)
	private Utilisateur date26;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date27", nullable = true)
	private Utilisateur date27;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date28", nullable = true)
	private Utilisateur date28;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date29", nullable = true)
	private Utilisateur date29;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date30", nullable = true)
	private Utilisateur date30;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date31", nullable = true)
	private Utilisateur date31;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date32", nullable = true)
	private Utilisateur date32;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date33", nullable = true)
	private Utilisateur date33;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date34", nullable = true)
	private Utilisateur date34;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date35", nullable = true)
	private Utilisateur date35;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date36", nullable = true)
	private Utilisateur date36;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date37", nullable = true)
	private Utilisateur date37;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date38", nullable = true)
	private Utilisateur date38;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date39", nullable = true)
	private Utilisateur date39;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date40", nullable = true)
	private Utilisateur date40;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "date41", nullable = true)
	private Utilisateur date41;

	public BouclageUtilisateur() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonIgnore
	public Bouclage getBouclage() {
		return bouclage;
	}

	@JsonProperty
	public void setBouclage(Bouclage bouclage) {
		this.bouclage = bouclage;
	}

	public Utilisateur getDate00() {
		return date00;
	}

	public void setDate00(Utilisateur date00) {
		this.date00 = date00;
	}

	public Utilisateur getDate01() {
		return date01;
	}

	public void setDate01(Utilisateur date01) {
		this.date01 = date01;
	}

	public Utilisateur getDate02() {
		return date02;
	}

	public void setDate02(Utilisateur date02) {
		this.date02 = date02;
	}

	public Utilisateur getDate03() {
		return date03;
	}

	public void setDate03(Utilisateur date03) {
		this.date03 = date03;
	}

	public Utilisateur getDate04() {
		return date04;
	}

	public void setDate04(Utilisateur date04) {
		this.date04 = date04;
	}

	public Utilisateur getDate05() {
		return date05;
	}

	public void setDate05(Utilisateur date05) {
		this.date05 = date05;
	}

	public Utilisateur getDate06() {
		return date06;
	}

	public void setDate06(Utilisateur date06) {
		this.date06 = date06;
	}

	public Utilisateur getDate07() {
		return date07;
	}

	public void setDate07(Utilisateur date07) {
		this.date07 = date07;
	}

	public Utilisateur getDate08() {
		return date08;
	}

	public void setDate08(Utilisateur date08) {
		this.date08 = date08;
	}

	public Utilisateur getDate09() {
		return date09;
	}

	public void setDate09(Utilisateur date09) {
		this.date09 = date09;
	}

	public Utilisateur getDate10() {
		return date10;
	}

	public void setDate10(Utilisateur date10) {
		this.date10 = date10;
	}

	public Utilisateur getDate11() {
		return date11;
	}

	public void setDate11(Utilisateur date11) {
		this.date11 = date11;
	}

	public Utilisateur getDate12() {
		return date12;
	}

	public void setDate12(Utilisateur date12) {
		this.date12 = date12;
	}

	public Utilisateur getDate13() {
		return date13;
	}

	public void setDate13(Utilisateur date13) {
		this.date13 = date13;
	}

	public Utilisateur getDate14() {
		return date14;
	}

	public void setDate14(Utilisateur date14) {
		this.date14 = date14;
	}

	public Utilisateur getDate15() {
		return date15;
	}

	public void setDate15(Utilisateur date15) {
		this.date15 = date15;
	}

	public Utilisateur getDate16() {
		return date16;
	}

	public void setDate16(Utilisateur date16) {
		this.date16 = date16;
	}

	public Utilisateur getDate17() {
		return date17;
	}

	public void setDate17(Utilisateur date17) {
		this.date17 = date17;
	}

	public Utilisateur getDate18() {
		return date18;
	}

	public void setDate18(Utilisateur date18) {
		this.date18 = date18;
	}

	public Utilisateur getDate19() {
		return date19;
	}

	public void setDate19(Utilisateur date19) {
		this.date19 = date19;
	}

	public Utilisateur getDate20() {
		return date20;
	}

	public void setDate20(Utilisateur date20) {
		this.date20 = date20;
	}

	public Utilisateur getDate21() {
		return date21;
	}

	public void setDate21(Utilisateur date21) {
		this.date21 = date21;
	}

	public Utilisateur getDate22() {
		return date22;
	}

	public void setDate22(Utilisateur date22) {
		this.date22 = date22;
	}

	public Utilisateur getDate23() {
		return date23;
	}

	public void setDate23(Utilisateur date23) {
		this.date23 = date23;
	}

	public Utilisateur getDate24() {
		return date24;
	}

	public void setDate24(Utilisateur date24) {
		this.date24 = date24;
	}

	public Utilisateur getDate25() {
		return date25;
	}

	public void setDate25(Utilisateur date25) {
		this.date25 = date25;
	}

	public Utilisateur getDate26() {
		return date26;
	}

	public void setDate26(Utilisateur date26) {
		this.date26 = date26;
	}

	public Utilisateur getDate27() {
		return date27;
	}

	public void setDate27(Utilisateur date27) {
		this.date27 = date27;
	}

	public Utilisateur getDate28() {
		return date28;
	}

	public void setDate28(Utilisateur date28) {
		this.date28 = date28;
	}

	public Utilisateur getDate29() {
		return date29;
	}

	public void setDate29(Utilisateur date29) {
		this.date29 = date29;
	}

	public Utilisateur getDate30() {
		return date30;
	}

	public void setDate30(Utilisateur date30) {
		this.date30 = date30;
	}

	public Utilisateur getDate31() {
		return date31;
	}

	public void setDate31(Utilisateur date31) {
		this.date31 = date31;
	}

	public Utilisateur getDate32() {
		return date32;
	}

	public void setDate32(Utilisateur date32) {
		this.date32 = date32;
	}

	public Utilisateur getDate33() {
		return date33;
	}

	public void setDate33(Utilisateur date33) {
		this.date33 = date33;
	}

	public Utilisateur getDate34() {
		return date34;
	}

	public void setDate34(Utilisateur date34) {
		this.date34 = date34;
	}

	public Utilisateur getDate35() {
		return date35;
	}

	public void setDate35(Utilisateur date35) {
		this.date35 = date35;
	}

	public Utilisateur getDate36() {
		return date36;
	}

	public void setDate36(Utilisateur date36) {
		this.date36 = date36;
	}

	public Utilisateur getDate37() {
		return date37;
	}

	public void setDate37(Utilisateur date37) {
		this.date37 = date37;
	}

	public Utilisateur getDate38() {
		return date38;
	}

	public void setDate38(Utilisateur date38) {
		this.date38 = date38;
	}

	public Utilisateur getDate39() {
		return date39;
	}

	public void setDate39(Utilisateur date39) {
		this.date39 = date39;
	}

	public Utilisateur getDate40() {
		return date40;
	}

	public void setDate40(Utilisateur date40) {
		this.date40 = date40;
	}

	public Utilisateur getDate41() {
		return date41;
	}

	public void setDate41(Utilisateur date41) {
		this.date41 = date41;
	}


}
