package com.boate.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.boate.enums.TypeVisiteEnum;
import com.boate.utils.LocalDateConverter;

@Entity
@Table(name = "work_visit")
public class Bouclage {

	@Id
	@SequenceGenerator(name = "work_visit_id_seq", sequenceName = "work_visit_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "work_visit_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "surveillance_id", nullable = false)
	private Surveillance surveillance;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "author", nullable = true)
	private Utilisateur author;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "author_surveillance", nullable = true)
	private Utilisateur authorSurveillance;

	@Enumerated(EnumType.STRING)
	@Column(name = "type_key", nullable = false)
	private TypeVisiteEnum typeKey;

	@Column(name = "incident_importance_key")
	private String incidentImportanceKey;

	@Column(name = "incident_description")
	private String incidentDescription;

	@Column(name = "incident_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate incidentDate;

	@Column(name = "incident")
	private Boolean incident;

	@Column(name = "prevision_date")
	private Integer previsionDate;

	@Column(name = "date00", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date00;

	@Column(name = "date01", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date01;

	@Column(name = "date02", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date02;

	@Column(name = "date03", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date03;

	@Column(name = "date04", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date04;

	@Column(name = "date05", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date05;

	@Column(name = "date06", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date06;

	@Column(name = "date07", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date07;

	@Column(name = "date08", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date08;

	@Column(name = "date09", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date09;

	// TODO: Fusion à faire avec date06
	@Column(name = "date10", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date10;

	@Column(name = "date11", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date11;

	@Column(name = "date12", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date12;

	@Column(name = "date13", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date13;

	@Column(name = "date14", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date14;

	@Column(name = "date15", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date15;

	@Column(name = "date16", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date16;

	@Column(name = "date17", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date17;

	@Column(name = "date18", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date18;

	@Column(name = "date19", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date19;

	@Column(name = "date20", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date20;

	@Column(name = "date21", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date21;

	// TODO: à fusionner avec les autres dates
	@Column(name = "date22", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date22;

	@Column(name = "date23", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date23;

	@Column(name = "date24", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date24;

	@Column(name = "date25", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date25;

	@Column(name = "date26", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date26;

	@Column(name = "date27", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date27;

	// TODO: à fusionner avec les autres dates si possible
	@Column(name = "date28", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date28;

	@Column(name = "date29", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date29;

	@Column(name = "date30", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date30;

	@Column(name = "date31", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date31;

	@Column(name = "date32", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date32;

	@Column(name = "date33", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date33;

	@Column(name = "date34", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date34;

	@Column(name = "date35", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date35;

	// TODO: à fusionner avec le sautres dates
	@Column(name = "date36", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date36;

	@Column(name = "date37", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date37;

	@Column(name = "date38", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date38;

	@Column(name = "date39", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date39;

	@Column(name = "date40", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date40;

	@Column(name = "labor_todo_agent_0")
	private String laborTodoAgent0;

	@Column(name = "labor_todo_agent_1")
	private String laborTodoAgent1;

	@Column(name = "labor_todo_agent_2")
	private String laborTodoAgent2;

	@Column(name = "labor_cost_agent_0")
	private Integer laborCostAgent0;

	@Column(name = "labor_cost_agent_1")
	private Integer laborCostAgent1;

	@Column(name = "labor_cost_agent_2")
	private Integer laborCostAgent2;

	@Column(name = "labor_acceptation_0_key")
	private String laborAcceptation0Key;

	@Column(name = "labor_acceptation_1_key")
	private String laborAcceptation1Key;

	@Column(name = "labor_acceptation_2_key")
	private String laborAcceptation2Key;

	@Column(name = "labor_todo_supervisor_0")
	private String laborTodoSupervisor0;

	@Column(name = "labor_todo_supervisor_1")
	private String laborTodoSupervisor1;

	@Column(name = "labor_todo_supervisor_2")
	private String laborTodoSupervisor2;

	@Column(name = "labor_cost_supervisor_0")
	private Integer laborCostSupervisor0;

	@Column(name = "labor_cost_supervisor_1")
	private Integer laborCostSupervisor1;

	@Column(name = "labor_cost_supervisor_2")
	private Integer laborCostSupervisor2;

	@Column(name = "labor_analyse_risque_0")
	private String laborAnalyseRisque0;

	@Column(name = "labor_analyse_risque_1")
	private String laborAnalyseRisque1;

	@Column(name = "labor_analyse_risque_2")
	private String laborAnalyseRisque2;

	@Column(name = "labor_mesures_conservatoire_0")
	private String laborMesuresConservatoire0;

	@Column(name = "labor_mesures_conservatoire_1")
	private String laborMesuresConservatoire1;

	@Column(name = "labor_mesures_conservatoire_2")
	private String laborMesuresConservatoire2;

	@Column(name = "commentaires")
	private String commentaires;

	@Column(name = "u0_etude")
	private Boolean u0_etude;

	@Column(name = "u1_etude")
	private Boolean u1_etude;

	@Column(name = "u2_etude")
	private Boolean u2_etude;

	@Column(name = "u0_expertise")
	private Boolean u0_expertise;

	@Column(name = "u1_expertise")
	private Boolean u1_expertise;

	@Column(name = "u2_expertise")
	private Boolean u2_expertise;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "auteur_maj", nullable = true)
	private Utilisateur auteurMaj;

	@Column(name = "date_maj")
	private String date_maj;

	@Column(name = "numero_fiche_rex")
	private String numeroFicheRex;

	@Column(name = "fiche_chute_bloc")
	private String ficheChuteBloc;

	// Categories pour U0
	@Column(name = "has_courriers_0")
	private Boolean hasCourriers0;
	@Column(name = "cost_confortement_0")
	private Integer costConfortement0;
	@Column(name = "cote_confortement_0")
	private String coteConfortement0;
	@Column(name = "cost_curage_0")
	private Integer costCurage0;
	@Column(name = "cote_curage_0")
	private String coteCurage0;
	@Column(name = "cost_debroussaillage_0")
	private Integer costDebroussaillage0;
	@Column(name = "cote_debroussaillage_0")
	private String coteDebroussaillage0;
	@Column(name = "cost_instrumentation_0")
	private Integer costInstrumentation0;
	@Column(name = "cote_instrumentation_0")
	private String coteInstrumentation0;
	@Column(name = "cost_maconnerie_0")
	private Integer costMaconnerie0;
	@Column(name = "cost_peinture_0")
	private Integer costPeinture0;
	@Column(name = "cost_purge_0")
	private Integer costPurge0;
	@Column(name = "cote_purge_0")
	private String cotePurge0;
	@Column(name = "cost_serrurerie_0")
	private Integer costSerrurerie0;
	@Column(name = "cost_terrassement_0")
	private Integer costTerrassement0;
	@Column(name = "cote_terrassement_0")
	private String coteTerrassement0;
	@Column(name = "cost_topographie_0")
	private Integer costTopographie0;
	@Column(name = "cote_topographie_0")
	private String coteTopographie0;
	@Column(name = "cost_divers_0")
	private Integer costDivers0;
	@Column(name = "cote_divers_0")
	private String coteDivers0;

	// Categories pour U1
	@Column(name = "has_courriers_1")
	private Boolean hasCourriers1;
	@Column(name = "cost_confortement_1")
	private Integer costConfortement1;
	@Column(name = "cote_confortement_1")
	private String coteConfortement1;
	@Column(name = "cost_curage_1")
	private Integer costCurage1;
	@Column(name = "cote_curage_1")
	private String coteCurage1;
	@Column(name = "cost_debroussaillage_1")
	private Integer costDebroussaillage1;
	@Column(name = "cote_debroussaillage_1")
	private String coteDebroussaillage1;
	@Column(name = "cost_instrumentation_1")
	private Integer costInstrumentation1;
	@Column(name = "cote_instrumentation_1")
	private String coteInstrumentation1;
	@Column(name = "cost_maconnerie_1")
	private Integer costMaconnerie1;
	@Column(name = "cost_peinture_1")
	private Integer costPeinture1;
	@Column(name = "cost_purge_1")
	private Integer costPurge1;
	@Column(name = "cote_purge_1")
	private String cotePurge1;
	@Column(name = "cost_serrurerie_1")
	private Integer costSerrurerie1;
	@Column(name = "cost_terrassement_1")
	private Integer costTerrassement1;
	@Column(name = "cote_terrassement_1")
	private String coteTerrassement1;
	@Column(name = "cost_topographie_1")
	private Integer costTopographie1;
	@Column(name = "cote_topographie_1")
	private String coteTopographie1;
	@Column(name = "cost_divers_1")
	private Integer costDivers1;
	@Column(name = "cote_divers_1")
	private String coteDivers1;

	// Categories pour U2
	@Column(name = "has_courriers_2")
	private Boolean hasCourriers2;
	@Column(name = "cost_confortement_2")
	private Integer costConfortement2;
	@Column(name = "cote_confortement_2")
	private String coteConfortement2;
	@Column(name = "cost_curage_2")
	private Integer costCurage2;
	@Column(name = "cote_curage_2")
	private String coteCurage2;
	@Column(name = "cost_debroussaillage_2")
	private Integer costDebroussaillage2;
	@Column(name = "cote_debroussaillage_2")
	private String coteDebroussaillage2;
	@Column(name = "cost_instrumentation_2")
	private Integer costInstrumentation2;
	@Column(name = "cote_instrumentation_2")
	private String coteInstrumentation2;
	@Column(name = "cost_maconnerie_2")
	private Integer costMaconnerie2;
	@Column(name = "cost_peinture_2")
	private Integer costPeinture2;
	@Column(name = "cost_purge_2")
	private Integer costPurge2;
	@Column(name = "cote_purge_2")
	private String cotePurge2;
	@Column(name = "cost_serrurerie_2")
	private Integer costSerrurerie2;
	@Column(name = "cost_terrassement_2")
	private Integer costTerrassement2;
	@Column(name = "cote_terrassement_2")
	private String coteTerrassement2;
	@Column(name = "cost_topographie_2")
	private Integer costTopographie2;
	@Column(name = "cote_topographie_2")
	private String coteTopographie2;
	@Column(name = "cost_divers_2")
	private Integer costDivers2;
	@Column(name = "cote_divers_2")
	private String coteDivers2;

	@Column(name = "etatOuvrage")
	private String etatOuvrage;

	@Column(name = "u0_integrite")
	private Boolean u0Integrite;
	@Column(name = "u1_integrite")
	private Boolean u1Integrite;
	@Column(name = "u2_integrite")
	private Boolean u2Integrite;
	@Column(name = "u0_securite_personne")
	private Boolean u0SecuritePersonne;
	@Column(name = "u1_securite_personne")
	private Boolean u1SecuritePersonne;
	@Column(name = "u2_securite_personne")
	private Boolean u2SecuritePersonne;
	@Column(name = "u0_securite_tiers")
	private Boolean u0SecuriteTiers;
	@Column(name = "u1_securite_tiers")
	private Boolean u1SecuriteTiers;
	@Column(name = "u2_securite_tiers")
	private Boolean u2SecuriteTiers;
	
	@Column(name = "motif_cv")
	private String motifCV;
	@Column(name = "solution_cv")
	private String solutionCV;
	
	@Column(name = "envoi_st_0")
	private Boolean envoiSt0;
	@Column(name = "envoi_st_1")
    private Boolean envoiSt1;
	@Column(name = "envoi_st_2")
    private Boolean envoiSt2;

	@Column(name = "type_surv_compl")
	private String typeSurvCompl;
	
	@Column(name = "u0_type_expertise")
	private String u0_typeExpertise;
	@Column(name = "u1_type_expertise")
    private String u1_typeExpertise;
	@Column(name = "u2_type_expertise")
    private String u2_typeExpertise;
	
	@Column(name = "motif_controle")
	private String motifControle;
	
	@Column(name = "ecr1")
	private Boolean ecr1;
	@Column(name = "ecr2")
	private Boolean ecr2;
	@Column(name = "ecr3")
	private Boolean ecr3;
	@Column(name = "ecr4")
	private Boolean ecr4;
	@Column(name = "ecr5")
	private Boolean ecr5;
	@Column(name = "ecr6")
	private Boolean ecr6;
	
	//Notation
	@Column(name = "note_globale")
	private Double noteGlobale;
	@Column(name = "note_tablier")
	private Double noteTablier;
	@Column(name = "note_appui")
	private Double noteAppui;
	@Column(name = "note_globale_prec")
	private Double noteGlobalePrec;
	@Column(name = "note_tablier_prec")
	private Double noteTablierPrec;
	@Column(name = "note_appui_prec")
	private Double noteAppuiPrec;
	@Column(name = "note_min")
	private Double noteMin;
	@Column(name = "note_max")
	private Double noteMax;

	@Column(name = "agent_delai_travaux_u0")
	private String delaiTravauxAgent0;
	@Column(name = "agent_delai_travaux_u1")
    private String delaiTravauxAgent1;
	@Column(name = "expert_delai_travaux_u0")
    private String delaiTravauxExpert0;
	@Column(name = "expert_delai_travaux_u1")
    private String delaiTravauxExpert1;
	
	@Column(name = "operation")
	private Integer operation;
	
	@Column(name = "etat_cv")
	private String etatCV;
	@Column(name = "motif_annulation_cv")
	private String motifAnnulationCV;
	@Column(name = "statut")
	private String statut;
	@Column(name = "auteur_suppression")
	private String auteurSuppression;
	@Column(name = "date_suppression", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate dateSuppression;
	
	
	@Column(name = "ecr1_pkd")
	private String ecr1_pkd;
	@Column(name = "ecr2_pkd")
	private String ecr2_pkd;
	@Column(name = "ecr3_pkd")
	private String ecr3_pkd;
	@Column(name = "ecr4_pkd")
	private String ecr4_pkd;
	@Column(name = "ecr5_pkd")
	private String ecr5_pkd;
	@Column(name = "ecr6_pkd")
	private String ecr6_pkd;

	@Column(name = "ecr1_pkf")
	private String ecr1_pkf;
	@Column(name = "ecr2_pkf")
	private String ecr2_pkf;
	@Column(name = "ecr3_pkf")
	private String ecr3_pkf;
	@Column(name = "ecr4_pkf")
	private String ecr4_pkf;
	@Column(name = "ecr5_pkf")
	private String ecr5_pkf;
	@Column(name = "ecr6_pkf")
	private String ecr6_pkf;
	
	@OneToMany(mappedBy = "bouclage", fetch = FetchType.LAZY)
	private List<BouclageUtilisateur> bouclageUtilisateurs;

	public Bouclage() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Surveillance getSurveillance() {
		return surveillance;
	}

	public void setSurveillance(Surveillance surveillance) {
		this.surveillance = surveillance;
	}

	public Utilisateur getAuthor() {
		return author;
	}

	public void setAuthor(Utilisateur author) {
		this.author = author;
	}

	public TypeVisiteEnum getTypeKey() {
		return typeKey;
	}

	public void setTypeKey(TypeVisiteEnum typeKey) {
		this.typeKey = typeKey;
	}

	public String getIncidentImportanceKey() {
		return incidentImportanceKey;
	}

	public void setIncidentImportanceKey(String incidentImportanceKey) {
		this.incidentImportanceKey = incidentImportanceKey;
	}

	public String getIncidentDescription() {
		return incidentDescription;
	}

	public void setIncidentDescription(String incidentDescription) {
		this.incidentDescription = incidentDescription;
	}

	public LocalDate getIncidentDate() {
		return incidentDate;
	}

	public void setIncidentDate(LocalDate incidentDate) {
		this.incidentDate = incidentDate;
	}

	public Boolean getIncident() {
		return incident;
	}

	public void setIncident(Boolean incident) {
		this.incident = incident;
	}

	public Integer getPrevisionDate() {
		return previsionDate;
	}

	public void setPrevisionDate(Integer previsionDate) {
		this.previsionDate = previsionDate;
	}

	public LocalDate getDate00() {
		return date00;
	}

	public void setDate00(LocalDate date00) {
		this.date00 = date00;
	}

	public LocalDate getDate01() {
		return date01;
	}

	public void setDate01(LocalDate date01) {
		this.date01 = date01;
	}

	public LocalDate getDate02() {
		return date02;
	}

	public void setDate02(LocalDate date02) {
		this.date02 = date02;
	}

	public LocalDate getDate03() {
		return date03;
	}

	public void setDate03(LocalDate date03) {
		this.date03 = date03;
	}

	public LocalDate getDate04() {
		return date04;
	}

	public void setDate04(LocalDate date04) {
		this.date04 = date04;
	}

	public LocalDate getDate05() {
		return date05;
	}

	public void setDate05(LocalDate date05) {
		this.date05 = date05;
	}

	public LocalDate getDate06() {
		return date06;
	}

	public void setDate06(LocalDate date06) {
		this.date06 = date06;
	}

	public LocalDate getDate07() {
		return date07;
	}

	public void setDate07(LocalDate date07) {
		this.date07 = date07;
	}

	public LocalDate getDate08() {
		return date08;
	}

	public void setDate08(LocalDate date08) {
		this.date08 = date08;
	}

	public LocalDate getDate09() {
		return date09;
	}

	public void setDate09(LocalDate date09) {
		this.date09 = date09;
	}

	public LocalDate getDate10() {
		return date10;
	}

	public void setDate10(LocalDate date10) {
		this.date10 = date10;
	}

	public LocalDate getDate11() {
		return date11;
	}

	public void setDate11(LocalDate date11) {
		this.date11 = date11;
	}

	public LocalDate getDate12() {
		return date12;
	}

	public void setDate12(LocalDate date12) {
		this.date12 = date12;
	}

	public LocalDate getDate13() {
		return date13;
	}

	public void setDate13(LocalDate date13) {
		this.date13 = date13;
	}

	public LocalDate getDate14() {
		return date14;
	}

	public void setDate14(LocalDate date14) {
		this.date14 = date14;
	}

	public LocalDate getDate15() {
		return date15;
	}

	public void setDate15(LocalDate date15) {
		this.date15 = date15;
	}

	public LocalDate getDate16() {
		return date16;
	}

	public void setDate16(LocalDate date16) {
		this.date16 = date16;
	}

	public LocalDate getDate17() {
		return date17;
	}

	public void setDate17(LocalDate date17) {
		this.date17 = date17;
	}

	public LocalDate getDate18() {
		return date18;
	}

	public void setDate18(LocalDate date18) {
		this.date18 = date18;
	}

	public LocalDate getDate19() {
		return date19;
	}

	public void setDate19(LocalDate date19) {
		this.date19 = date19;
	}

	public LocalDate getDate20() {
		return date20;
	}

	public void setDate20(LocalDate date20) {
		this.date20 = date20;
	}

	public LocalDate getDate21() {
		return date21;
	}

	public void setDate21(LocalDate date21) {
		this.date21 = date21;
	}

	public LocalDate getDate22() {
		return date22;
	}

	public void setDate22(LocalDate date22) {
		this.date22 = date22;
	}

	public LocalDate getDate23() {
		return date23;
	}

	public void setDate23(LocalDate date23) {
		this.date23 = date23;
	}

	public LocalDate getDate24() {
		return date24;
	}

	public void setDate24(LocalDate date24) {
		this.date24 = date24;
	}

	public LocalDate getDate25() {
		return date25;
	}

	public void setDate25(LocalDate date25) {
		this.date25 = date25;
	}

	public LocalDate getDate26() {
		return date26;
	}

	public void setDate26(LocalDate date26) {
		this.date26 = date26;
	}

	public LocalDate getDate27() {
		return date27;
	}

	public void setDate27(LocalDate date27) {
		this.date27 = date27;
	}

	public LocalDate getDate28() {
		return date28;
	}

	public void setDate28(LocalDate date28) {
		this.date28 = date28;
	}

	public LocalDate getDate29() {
		return date29;
	}

	public void setDate29(LocalDate date29) {
		this.date29 = date29;
	}

	public LocalDate getDate30() {
		return date30;
	}

	public void setDate30(LocalDate date30) {
		this.date30 = date30;
	}

	public LocalDate getDate31() {
		return date31;
	}

	public void setDate31(LocalDate date31) {
		this.date31 = date31;
	}

	public LocalDate getDate32() {
		return date32;
	}

	public void setDate32(LocalDate date32) {
		this.date32 = date32;
	}

	public LocalDate getDate33() {
		return date33;
	}

	public void setDate33(LocalDate date33) {
		this.date33 = date33;
	}

	public LocalDate getDate34() {
		return date34;
	}

	public void setDate34(LocalDate date34) {
		this.date34 = date34;
	}

	public LocalDate getDate35() {
		return date35;
	}

	public void setDate35(LocalDate date35) {
		this.date35 = date35;
	}

	public LocalDate getDate36() {
		return date36;
	}

	public void setDate36(LocalDate date36) {
		this.date36 = date36;
	}

	public LocalDate getDate37() {
		return date37;
	}

	public void setDate37(LocalDate date37) {
		this.date37 = date37;
	}

	public LocalDate getDate38() {
		return date38;
	}

	public void setDate38(LocalDate date38) {
		this.date38 = date38;
	}

	public LocalDate getDate39() {
		return date39;
	}

	public void setDate39(LocalDate date39) {
		this.date39 = date39;
	}

	public LocalDate getDate40() {
		return date40;
	}

	public void setDate40(LocalDate date40) {
		this.date40 = date40;
	}

	public Utilisateur getAuthorSurveillance() {
		return authorSurveillance;
	}

	public void setAuthorSurveillance(Utilisateur authorSurveillance) {
		this.authorSurveillance = authorSurveillance;
	}

	public String getLaborTodoAgent0() {
		return laborTodoAgent0;
	}

	public void setLaborTodoAgent0(String laborTodoAgent0) {
		this.laborTodoAgent0 = laborTodoAgent0;
	}

	public String getLaborTodoAgent1() {
		return laborTodoAgent1;
	}

	public void setLaborTodoAgent1(String laborTodoAgent1) {
		this.laborTodoAgent1 = laborTodoAgent1;
	}

	public String getLaborTodoAgent2() {
		return laborTodoAgent2;
	}

	public void setLaborTodoAgent2(String laborTodoAgent2) {
		this.laborTodoAgent2 = laborTodoAgent2;
	}

	public Integer getLaborCostAgent0() {
		return laborCostAgent0;
	}

	public void setLaborCostAgent0(Integer laborCostAgent0) {
		this.laborCostAgent0 = laborCostAgent0;
	}

	public Integer getLaborCostAgent1() {
		return laborCostAgent1;
	}

	public void setLaborCostAgent1(Integer laborCostAgent1) {
		this.laborCostAgent1 = laborCostAgent1;
	}

	public Integer getLaborCostAgent2() {
		return laborCostAgent2;
	}

	public void setLaborCostAgent2(Integer laborCostAgent2) {
		this.laborCostAgent2 = laborCostAgent2;
	}

	public String getLaborAcceptation0Key() {
		return laborAcceptation0Key;
	}

	public void setLaborAcceptation0Key(String laborAcceptation0Key) {
		this.laborAcceptation0Key = laborAcceptation0Key;
	}

	public String getLaborAcceptation1Key() {
		return laborAcceptation1Key;
	}

	public void setLaborAcceptation1Key(String laborAcceptation1Key) {
		this.laborAcceptation1Key = laborAcceptation1Key;
	}

	public String getLaborAcceptation2Key() {
		return laborAcceptation2Key;
	}

	public void setLaborAcceptation2Key(String laborAcceptation2Key) {
		this.laborAcceptation2Key = laborAcceptation2Key;
	}

	public String getLaborTodoSupervisor0() {
		return laborTodoSupervisor0;
	}

	public void setLaborTodoSupervisor0(String laborTodoSupervisor0) {
		this.laborTodoSupervisor0 = laborTodoSupervisor0;
	}

	public String getLaborTodoSupervisor1() {
		return laborTodoSupervisor1;
	}

	public void setLaborTodoSupervisor1(String laborTodoSupervisor1) {
		this.laborTodoSupervisor1 = laborTodoSupervisor1;
	}

	public String getLaborTodoSupervisor2() {
		return laborTodoSupervisor2;
	}

	public void setLaborTodoSupervisor2(String laborTodoSupervisor2) {
		this.laborTodoSupervisor2 = laborTodoSupervisor2;
	}

	public Integer getLaborCostSupervisor0() {
		return laborCostSupervisor0;
	}

	public void setLaborCostSupervisor0(Integer laborCostSupervisor0) {
		this.laborCostSupervisor0 = laborCostSupervisor0;
	}

	public Integer getLaborCostSupervisor1() {
		return laborCostSupervisor1;
	}

	public void setLaborCostSupervisor1(Integer laborCostSupervisor1) {
		this.laborCostSupervisor1 = laborCostSupervisor1;
	}

	public Integer getLaborCostSupervisor2() {
		return laborCostSupervisor2;
	}

	public void setLaborCostSupervisor2(Integer laborCostSupervisor2) {
		this.laborCostSupervisor2 = laborCostSupervisor2;
	}

	public String getLaborAnalyseRisque0() {
		return laborAnalyseRisque0;
	}

	public void setLaborAnalyseRisque0(String laborAnalyseRisque0) {
		this.laborAnalyseRisque0 = laborAnalyseRisque0;
	}

	public String getLaborAnalyseRisque1() {
		return laborAnalyseRisque1;
	}

	public void setLaborAnalyseRisque1(String laborAnalyseRisque1) {
		this.laborAnalyseRisque1 = laborAnalyseRisque1;
	}

	public String getLaborAnalyseRisque2() {
		return laborAnalyseRisque2;
	}

	public void setLaborAnalyseRisque2(String laborAnalyseRisque2) {
		this.laborAnalyseRisque2 = laborAnalyseRisque2;
	}

	public String getLaborMesuresConservatoire0() {
		return laborMesuresConservatoire0;
	}

	public void setLaborMesuresConservatoire0(String laborMesuresConservatoire0) {
		this.laborMesuresConservatoire0 = laborMesuresConservatoire0;
	}

	public String getLaborMesuresConservatoire1() {
		return laborMesuresConservatoire1;
	}

	public void setLaborMesuresConservatoire1(String laborMesuresConservatoire1) {
		this.laborMesuresConservatoire1 = laborMesuresConservatoire1;
	}

	public String getLaborMesuresConservatoire2() {
		return laborMesuresConservatoire2;
	}

	public void setLaborMesuresConservatoire2(String laborMesuresConservatoire2) {
		this.laborMesuresConservatoire2 = laborMesuresConservatoire2;
	}

	public String getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}

	public Boolean getU0_etude() {
		return u0_etude;
	}

	public void setU0_etude(Boolean u0_etude) {
		this.u0_etude = u0_etude;
	}

	public Boolean getU1_etude() {
		return u1_etude;
	}

	public void setU1_etude(Boolean u1_etude) {
		this.u1_etude = u1_etude;
	}

	public Boolean getU2_etude() {
		return u2_etude;
	}

	public void setU2_etude(Boolean u2_etude) {
		this.u2_etude = u2_etude;
	}

	public Boolean getU0_expertise() {
		return u0_expertise;
	}

	public void setU0_expertise(Boolean u0_expertise) {
		this.u0_expertise = u0_expertise;
	}

	public Boolean getU1_expertise() {
		return u1_expertise;
	}

	public void setU1_expertise(Boolean u1_expertise) {
		this.u1_expertise = u1_expertise;
	}

	public Boolean getU2_expertise() {
		return u2_expertise;
	}

	public void setU2_expertise(Boolean u2_expertise) {
		this.u2_expertise = u2_expertise;
	}

	public Utilisateur getAuteurMaj() {
		return auteurMaj;
	}

	public void setAuteurMaj(Utilisateur auteurMaj) {
		this.auteurMaj = auteurMaj;
	}

	public String getDate_maj() {
		return date_maj;
	}

	public void setDate_maj(String date_maj) {
		this.date_maj = date_maj;
	}

	public String getNumeroFicheRex() {
		return numeroFicheRex;
	}

	public void setNumeroFicheRex(String numeroFicheRex) {
		this.numeroFicheRex = numeroFicheRex;
	}

	public String getFicheChuteBloc() {
		return ficheChuteBloc;
	}

	public void setFicheChuteBloc(String ficheChuteBloc) {
		this.ficheChuteBloc = ficheChuteBloc;
	}

	public Boolean getHasCourriers0() {
		return hasCourriers0;
	}

	public void setHasCourriers0(Boolean hasCourriers0) {
		this.hasCourriers0 = hasCourriers0;
	}

	public Integer getCostConfortement0() {
		return costConfortement0;
	}

	public void setCostConfortement0(Integer costConfortement0) {
		this.costConfortement0 = costConfortement0;
	}

	public String getCoteConfortement0() {
		return coteConfortement0;
	}

	public void setCoteConfortement0(String coteConfortement0) {
		this.coteConfortement0 = coteConfortement0;
	}

	public Integer getCostCurage0() {
		return costCurage0;
	}

	public void setCostCurage0(Integer costCurage0) {
		this.costCurage0 = costCurage0;
	}

	public String getCoteCurage0() {
		return coteCurage0;
	}

	public void setCoteCurage0(String coteCurage0) {
		this.coteCurage0 = coteCurage0;
	}

	public Integer getCostDebroussaillage0() {
		return costDebroussaillage0;
	}

	public void setCostDebroussaillage0(Integer costDebroussaillage0) {
		this.costDebroussaillage0 = costDebroussaillage0;
	}

	public String getCoteDebroussaillage0() {
		return coteDebroussaillage0;
	}

	public void setCoteDebroussaillage0(String coteDebroussaillage0) {
		this.coteDebroussaillage0 = coteDebroussaillage0;
	}

	public Integer getCostInstrumentation0() {
		return costInstrumentation0;
	}

	public void setCostInstrumentation0(Integer costInstrumentation0) {
		this.costInstrumentation0 = costInstrumentation0;
	}

	public String getCoteInstrumentation0() {
		return coteInstrumentation0;
	}

	public void setCoteInstrumentation0(String coteInstrumentation0) {
		this.coteInstrumentation0 = coteInstrumentation0;
	}

	public Integer getCostMaconnerie0() {
		return costMaconnerie0;
	}

	public void setCostMaconnerie0(Integer costMaconnerie0) {
		this.costMaconnerie0 = costMaconnerie0;
	}

	public Integer getCostPeinture0() {
		return costPeinture0;
	}

	public void setCostPeinture0(Integer costPeinture0) {
		this.costPeinture0 = costPeinture0;
	}

	public Integer getCostPurge0() {
		return costPurge0;
	}

	public void setCostPurge0(Integer costPurge0) {
		this.costPurge0 = costPurge0;
	}

	public String getCotePurge0() {
		return cotePurge0;
	}

	public void setCotePurge0(String cotePurge0) {
		this.cotePurge0 = cotePurge0;
	}

	public Integer getCostSerrurerie0() {
		return costSerrurerie0;
	}

	public void setCostSerrurerie0(Integer costSerrurerie0) {
		this.costSerrurerie0 = costSerrurerie0;
	}

	public Integer getCostTerrassement0() {
		return costTerrassement0;
	}

	public void setCostTerrassement0(Integer costTerrassement0) {
		this.costTerrassement0 = costTerrassement0;
	}

	public String getCoteTerrassement0() {
		return coteTerrassement0;
	}

	public void setCoteTerrassement0(String coteTerrassement0) {
		this.coteTerrassement0 = coteTerrassement0;
	}

	public Integer getCostTopographie0() {
		return costTopographie0;
	}

	public void setCostTopographie0(Integer costTopographie0) {
		this.costTopographie0 = costTopographie0;
	}

	public String getCoteTopographie0() {
		return coteTopographie0;
	}

	public void setCoteTopographie0(String coteTopographie0) {
		this.coteTopographie0 = coteTopographie0;
	}

	public Integer getCostDivers0() {
		return costDivers0;
	}

	public void setCostDivers0(Integer costDivers0) {
		this.costDivers0 = costDivers0;
	}

	public String getCoteDivers0() {
		return coteDivers0;
	}

	public void setCoteDivers0(String coteDivers0) {
		this.coteDivers0 = coteDivers0;
	}

	public Boolean getHasCourriers1() {
		return hasCourriers1;
	}

	public void setHasCourriers1(Boolean hasCourriers1) {
		this.hasCourriers1 = hasCourriers1;
	}

	public Integer getCostConfortement1() {
		return costConfortement1;
	}

	public void setCostConfortement1(Integer costConfortement1) {
		this.costConfortement1 = costConfortement1;
	}

	public String getCoteConfortement1() {
		return coteConfortement1;
	}

	public void setCoteConfortement1(String coteConfortement1) {
		this.coteConfortement1 = coteConfortement1;
	}

	public Integer getCostCurage1() {
		return costCurage1;
	}

	public void setCostCurage1(Integer costCurage1) {
		this.costCurage1 = costCurage1;
	}

	public String getCoteCurage1() {
		return coteCurage1;
	}

	public void setCoteCurage1(String coteCurage1) {
		this.coteCurage1 = coteCurage1;
	}

	public Integer getCostDebroussaillage1() {
		return costDebroussaillage1;
	}

	public void setCostDebroussaillage1(Integer costDebroussaillage1) {
		this.costDebroussaillage1 = costDebroussaillage1;
	}

	public String getCoteDebroussaillage1() {
		return coteDebroussaillage1;
	}

	public void setCoteDebroussaillage1(String coteDebroussaillage1) {
		this.coteDebroussaillage1 = coteDebroussaillage1;
	}

	public Integer getCostInstrumentation1() {
		return costInstrumentation1;
	}

	public void setCostInstrumentation1(Integer costInstrumentation1) {
		this.costInstrumentation1 = costInstrumentation1;
	}

	public String getCoteInstrumentation1() {
		return coteInstrumentation1;
	}

	public void setCoteInstrumentation1(String coteInstrumentation1) {
		this.coteInstrumentation1 = coteInstrumentation1;
	}

	public Integer getCostMaconnerie1() {
		return costMaconnerie1;
	}

	public void setCostMaconnerie1(Integer costMaconnerie1) {
		this.costMaconnerie1 = costMaconnerie1;
	}

	public Integer getCostPeinture1() {
		return costPeinture1;
	}

	public void setCostPeinture1(Integer costPeinture1) {
		this.costPeinture1 = costPeinture1;
	}

	public Integer getCostPurge1() {
		return costPurge1;
	}

	public void setCostPurge1(Integer costPurge1) {
		this.costPurge1 = costPurge1;
	}

	public String getCotePurge1() {
		return cotePurge1;
	}

	public void setCotePurge1(String cotePurge1) {
		this.cotePurge1 = cotePurge1;
	}

	public Integer getCostSerrurerie1() {
		return costSerrurerie1;
	}

	public void setCostSerrurerie1(Integer costSerrurerie1) {
		this.costSerrurerie1 = costSerrurerie1;
	}

	public Integer getCostTerrassement1() {
		return costTerrassement1;
	}

	public void setCostTerrassement1(Integer costTerrassement1) {
		this.costTerrassement1 = costTerrassement1;
	}

	public String getCoteTerrassement1() {
		return coteTerrassement1;
	}

	public void setCoteTerrassement1(String coteTerrassement1) {
		this.coteTerrassement1 = coteTerrassement1;
	}

	public Integer getCostTopographie1() {
		return costTopographie1;
	}

	public void setCostTopographie1(Integer costTopographie1) {
		this.costTopographie1 = costTopographie1;
	}

	public String getCoteTopographie1() {
		return coteTopographie1;
	}

	public void setCoteTopographie1(String coteTopographie1) {
		this.coteTopographie1 = coteTopographie1;
	}

	public Integer getCostDivers1() {
		return costDivers1;
	}

	public void setCostDivers1(Integer costDivers1) {
		this.costDivers1 = costDivers1;
	}

	public String getCoteDivers1() {
		return coteDivers1;
	}

	public void setCoteDivers1(String coteDivers1) {
		this.coteDivers1 = coteDivers1;
	}

	public Boolean getHasCourriers2() {
		return hasCourriers2;
	}

	public void setHasCourriers2(Boolean hasCourriers2) {
		this.hasCourriers2 = hasCourriers2;
	}

	public Integer getCostConfortement2() {
		return costConfortement2;
	}

	public void setCostConfortement2(Integer costConfortement2) {
		this.costConfortement2 = costConfortement2;
	}

	public String getCoteConfortement2() {
		return coteConfortement2;
	}

	public void setCoteConfortement2(String coteConfortement2) {
		this.coteConfortement2 = coteConfortement2;
	}

	public Integer getCostCurage2() {
		return costCurage2;
	}

	public void setCostCurage2(Integer costCurage2) {
		this.costCurage2 = costCurage2;
	}

	public String getCoteCurage2() {
		return coteCurage2;
	}

	public void setCoteCurage2(String coteCurage2) {
		this.coteCurage2 = coteCurage2;
	}

	public Integer getCostDebroussaillage2() {
		return costDebroussaillage2;
	}

	public void setCostDebroussaillage2(Integer costDebroussaillage2) {
		this.costDebroussaillage2 = costDebroussaillage2;
	}

	public String getCoteDebroussaillage2() {
		return coteDebroussaillage2;
	}

	public void setCoteDebroussaillage2(String coteDebroussaillage2) {
		this.coteDebroussaillage2 = coteDebroussaillage2;
	}

	public Integer getCostInstrumentation2() {
		return costInstrumentation2;
	}

	public void setCostInstrumentation2(Integer costInstrumentation2) {
		this.costInstrumentation2 = costInstrumentation2;
	}

	public String getCoteInstrumentation2() {
		return coteInstrumentation2;
	}

	public void setCoteInstrumentation2(String coteInstrumentation2) {
		this.coteInstrumentation2 = coteInstrumentation2;
	}

	public Integer getCostMaconnerie2() {
		return costMaconnerie2;
	}

	public void setCostMaconnerie2(Integer costMaconnerie2) {
		this.costMaconnerie2 = costMaconnerie2;
	}

	public Integer getCostPeinture2() {
		return costPeinture2;
	}

	public void setCostPeinture2(Integer costPeinture2) {
		this.costPeinture2 = costPeinture2;
	}

	public Integer getCostPurge2() {
		return costPurge2;
	}

	public void setCostPurge2(Integer costPurge2) {
		this.costPurge2 = costPurge2;
	}

	public String getCotePurge2() {
		return cotePurge2;
	}

	public void setCotePurge2(String cotePurge2) {
		this.cotePurge2 = cotePurge2;
	}

	public Integer getCostSerrurerie2() {
		return costSerrurerie2;
	}

	public void setCostSerrurerie2(Integer costSerrurerie2) {
		this.costSerrurerie2 = costSerrurerie2;
	}

	public Integer getCostTerrassement2() {
		return costTerrassement2;
	}

	public void setCostTerrassement2(Integer costTerrassement2) {
		this.costTerrassement2 = costTerrassement2;
	}

	public String getCoteTerrassement2() {
		return coteTerrassement2;
	}

	public void setCoteTerrassement2(String coteTerrassement2) {
		this.coteTerrassement2 = coteTerrassement2;
	}

	public Integer getCostTopographie2() {
		return costTopographie2;
	}

	public void setCostTopographie2(Integer costTopographie2) {
		this.costTopographie2 = costTopographie2;
	}

	public String getCoteTopographie2() {
		return coteTopographie2;
	}

	public void setCoteTopographie2(String coteTopographie2) {
		this.coteTopographie2 = coteTopographie2;
	}

	public Integer getCostDivers2() {
		return costDivers2;
	}

	public void setCostDivers2(Integer costDivers2) {
		this.costDivers2 = costDivers2;
	}

	public String getCoteDivers2() {
		return coteDivers2;
	}

	public void setCoteDivers2(String coteDivers2) {
		this.coteDivers2 = coteDivers2;
	}

	public String getEtatOuvrage() {
		return etatOuvrage;
	}

	public void setEtatOuvrage(String etatOuvrage) {
		this.etatOuvrage = etatOuvrage;
	}

	public Boolean getU0Integrite() {
		return u0Integrite;
	}

	public void setU0Integrite(Boolean u0Integrite) {
		this.u0Integrite = u0Integrite;
	}

	public Boolean getU1Integrite() {
		return u1Integrite;
	}

	public void setU1Integrite(Boolean u1Integrite) {
		this.u1Integrite = u1Integrite;
	}

	public Boolean getU2Integrite() {
		return u2Integrite;
	}

	public void setU2Integrite(Boolean u2Integrite) {
		this.u2Integrite = u2Integrite;
	}

	public Boolean getU0SecuritePersonne() {
		return u0SecuritePersonne;
	}

	public void setU0SecuritePersonne(Boolean u0SecuritePersonne) {
		this.u0SecuritePersonne = u0SecuritePersonne;
	}

	public Boolean getU1SecuritePersonne() {
		return u1SecuritePersonne;
	}

	public void setU1SecuritePersonne(Boolean u1SecuritePersonne) {
		this.u1SecuritePersonne = u1SecuritePersonne;
	}

	public Boolean getU2SecuritePersonne() {
		return u2SecuritePersonne;
	}

	public void setU2SecuritePersonne(Boolean u2SecuritePersonne) {
		this.u2SecuritePersonne = u2SecuritePersonne;
	}

	public Boolean getU0SecuriteTiers() {
		return u0SecuriteTiers;
	}

	public void setU0SecuriteTiers(Boolean u0SecuriteTiers) {
		this.u0SecuriteTiers = u0SecuriteTiers;
	}

	public Boolean getU1SecuriteTiers() {
		return u1SecuriteTiers;
	}

	public void setU1SecuriteTiers(Boolean u1SecuriteTiers) {
		this.u1SecuriteTiers = u1SecuriteTiers;
	}

	public Boolean getU2SecuriteTiers() {
		return u2SecuriteTiers;
	}

	public void setU2SecuriteTiers(Boolean u2SecuriteTiers) {
		this.u2SecuriteTiers = u2SecuriteTiers;
	}

	public String getMotifCV() {
		return motifCV;
	}

	public void setMotifCV(String motifCV) {
		this.motifCV = motifCV;
	}

	public String getSolutionCV() {
		return solutionCV;
	}

	public void setSolutionCV(String solutionCV) {
		this.solutionCV = solutionCV;
	}

	public Boolean getEnvoiSt0() {
		return envoiSt0;
	}

	public void setEnvoiSt0(Boolean envoiSt0) {
		this.envoiSt0 = envoiSt0;
	}

	public Boolean getEnvoiSt1() {
		return envoiSt1;
	}

	public void setEnvoiSt1(Boolean envoiSt1) {
		this.envoiSt1 = envoiSt1;
	}

	public Boolean getEnvoiSt2() {
		return envoiSt2;
	}

	public void setEnvoiSt2(Boolean envoiSt2) {
		this.envoiSt2 = envoiSt2;
	}

	public String getTypeSurvCompl() {
		return typeSurvCompl;
	}

	public void setTypeSurvCompl(String typeSurvCompl) {
		this.typeSurvCompl = typeSurvCompl;
	}

	public String getU0_typeExpertise() {
		return u0_typeExpertise;
	}

	public void setU0_typeExpertise(String u0_typeExpertise) {
		this.u0_typeExpertise = u0_typeExpertise;
	}

	public String getU1_typeExpertise() {
		return u1_typeExpertise;
	}

	public void setU1_typeExpertise(String u1_typeExpertise) {
		this.u1_typeExpertise = u1_typeExpertise;
	}

	public String getU2_typeExpertise() {
		return u2_typeExpertise;
	}

	public void setU2_typeExpertise(String u2_typeExpertise) {
		this.u2_typeExpertise = u2_typeExpertise;
	}

	public String getMotifControle() {
		return motifControle;
	}

	public void setMotifControle(String motifControle) {
		this.motifControle = motifControle;
	}

	public Boolean getEcr1() {
		return ecr1;
	}

	public void setEcr1(Boolean ecr1) {
		this.ecr1 = ecr1;
	}

	public Boolean getEcr2() {
		return ecr2;
	}

	public void setEcr2(Boolean ecr2) {
		this.ecr2 = ecr2;
	}

	public Boolean getEcr3() {
		return ecr3;
	}

	public void setEcr3(Boolean ecr3) {
		this.ecr3 = ecr3;
	}

	public Boolean getEcr4() {
		return ecr4;
	}

	public void setEcr4(Boolean ecr4) {
		this.ecr4 = ecr4;
	}

	public Boolean getEcr5() {
		return ecr5;
	}

	public void setEcr5(Boolean ecr5) {
		this.ecr5 = ecr5;
	}

	public Boolean getEcr6() {
		return ecr6;
	}

	public void setEcr6(Boolean ecr6) {
		this.ecr6 = ecr6;
	}

	public Double getNoteGlobale() {
		return noteGlobale;
	}

	public void setNoteGlobale(Double noteGlobale) {
		this.noteGlobale = noteGlobale;
	}

	public Double getNoteTablier() {
		return noteTablier;
	}

	public void setNoteTablier(Double noteTablier) {
		this.noteTablier = noteTablier;
	}

	public Double getNoteAppui() {
		return noteAppui;
	}

	public void setNoteAppui(Double noteAppui) {
		this.noteAppui = noteAppui;
	}

	public Double getNoteGlobalePrec() {
		return noteGlobalePrec;
	}

	public void setNoteGlobalePrec(Double noteGlobalePrec) {
		this.noteGlobalePrec = noteGlobalePrec;
	}

	public Double getNoteTablierPrec() {
		return noteTablierPrec;
	}

	public void setNoteTablierPrec(Double noteTablierPrec) {
		this.noteTablierPrec = noteTablierPrec;
	}

	public Double getNoteAppuiPrec() {
		return noteAppuiPrec;
	}

	public void setNoteAppuiPrec(Double noteAppuiPrec) {
		this.noteAppuiPrec = noteAppuiPrec;
	}

	public Double getNoteMin() {
		return noteMin;
	}

	public void setNoteMin(Double noteMin) {
		this.noteMin = noteMin;
	}

	public Double getNoteMax() {
		return noteMax;
	}

	public void setNoteMax(Double noteMax) {
		this.noteMax = noteMax;
	}

	public String getEcr1_pkd() {
		return ecr1_pkd;
	}

	public void setEcr1_pkd(String ecr1_pkd) {
		this.ecr1_pkd = ecr1_pkd;
	}

	public String getEcr2_pkd() {
		return ecr2_pkd;
	}

	public void setEcr2_pkd(String ecr2_pkd) {
		this.ecr2_pkd = ecr2_pkd;
	}

	public String getEcr3_pkd() {
		return ecr3_pkd;
	}

	public void setEcr3_pkd(String ecr3_pkd) {
		this.ecr3_pkd = ecr3_pkd;
	}

	public String getEcr4_pkd() {
		return ecr4_pkd;
	}

	public void setEcr4_pkd(String ecr4_pkd) {
		this.ecr4_pkd = ecr4_pkd;
	}

	public String getEcr5_pkd() {
		return ecr5_pkd;
	}

	public void setEcr5_pkd(String ecr5_pkd) {
		this.ecr5_pkd = ecr5_pkd;
	}

	public String getEcr6_pkd() {
		return ecr6_pkd;
	}

	public void setEcr6_pkd(String ecr6_pkd) {
		this.ecr6_pkd = ecr6_pkd;
	}

	public String getEcr1_pkf() {
		return ecr1_pkf;
	}

	public void setEcr1_pkf(String ecr1_pkf) {
		this.ecr1_pkf = ecr1_pkf;
	}

	public String getEcr2_pkf() {
		return ecr2_pkf;
	}

	public void setEcr2_pkf(String ecr2_pkf) {
		this.ecr2_pkf = ecr2_pkf;
	}

	public String getEcr3_pkf() {
		return ecr3_pkf;
	}

	public void setEcr3_pkf(String ecr3_pkf) {
		this.ecr3_pkf = ecr3_pkf;
	}

	public String getEcr4_pkf() {
		return ecr4_pkf;
	}

	public void setEcr4_pkf(String ecr4_pkf) {
		this.ecr4_pkf = ecr4_pkf;
	}

	public String getEcr5_pkf() {
		return ecr5_pkf;
	}

	public void setEcr5_pkf(String ecr5_pkf) {
		this.ecr5_pkf = ecr5_pkf;
	}

	public String getEcr6_pkf() {
		return ecr6_pkf;
	}

	public void setEcr6_pkf(String ecr6_pkf) {
		this.ecr6_pkf = ecr6_pkf;
	}

	public String getDelaiTravauxAgent0() {
		return delaiTravauxAgent0;
	}

	public void setDelaiTravauxAgent0(String delaiTravauxAgent0) {
		this.delaiTravauxAgent0 = delaiTravauxAgent0;
	}

	public String getDelaiTravauxAgent1() {
		return delaiTravauxAgent1;
	}

	public void setDelaiTravauxAgent1(String delaiTravauxAgent1) {
		this.delaiTravauxAgent1 = delaiTravauxAgent1;
	}

	public String getDelaiTravauxExpert0() {
		return delaiTravauxExpert0;
	}

	public void setDelaiTravauxExpert0(String delaiTravauxExpert0) {
		this.delaiTravauxExpert0 = delaiTravauxExpert0;
	}

	public String getDelaiTravauxExpert1() {
		return delaiTravauxExpert1;
	}

	public void setDelaiTravauxExpert1(String delaiTravauxExpert1) {
		this.delaiTravauxExpert1 = delaiTravauxExpert1;
	}

	public Integer getOperation() {
		return operation;
	}

	public void setOperation(Integer operation) {
		this.operation = operation;
	}

	public String getEtatCV() {
		return etatCV;
	}

	public void setEtatCV(String etatCV) {
		this.etatCV = etatCV;
	}

	public String getMotifAnnulationCV() {
		return motifAnnulationCV;
	}

	public void setMotifAnnulationCV(String motifAnnulationCV) {
		this.motifAnnulationCV = motifAnnulationCV;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getAuteurSuppression() {
		return auteurSuppression;
	}

	public void setAuteurSuppression(String auteurSuppression) {
		this.auteurSuppression = auteurSuppression;
	}

	public LocalDate getDateSuppression() {
		return dateSuppression;
	}

	public void setDateSuppression(LocalDate dateSuppression) {
		this.dateSuppression = dateSuppression;
	}

	public List<BouclageUtilisateur> getBouclageUtilisateurs() {
		return bouclageUtilisateurs;
	}

	public void setBouclageUtilisateurs(List<BouclageUtilisateur> bouclageUtilisateurs) {
		this.bouclageUtilisateurs = bouclageUtilisateurs;
	}

}
