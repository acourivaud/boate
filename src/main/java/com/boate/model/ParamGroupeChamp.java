package com.boate.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "param_groupe_champ")
public class ParamGroupeChamp {

	@Id
	@SequenceGenerator(name = "param_groupe_champ_id_seq", sequenceName = "param_groupe_champ_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "param_groupe_champ_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "param_groupe_id", nullable = false)
	private ParamGroupe paramGroupe;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "param_champ_id", nullable = false)
	private ParamChamp paramChamp;

	public ParamGroupeChamp() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ParamChamp getParamChamp() {
		return paramChamp;
	}

	public void setParamChamp(ParamChamp paramChamp) {
		this.paramChamp = paramChamp;
	}

	public ParamGroupe getParamGroupe() {
		return paramGroupe;
	}

	public void setParamGroupe(ParamGroupe paramGroupe) {
		this.paramGroupe = paramGroupe;
	}

}
