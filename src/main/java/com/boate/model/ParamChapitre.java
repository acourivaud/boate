package com.boate.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.boate.enums.ParamStatutEnum;

@Entity
@Table(name = "param_chapitre")
public class ParamChapitre {

	@Id
	@SequenceGenerator(name = "param_chapitre_id_seq", sequenceName = "param_chapitre_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "param_chapitre_id_seq")
	private Integer id;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "param_module_id", nullable = true)
	private ParamModule paramModule;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "param_onglet_id", nullable = false)
	private ParamOnglet paramOnglet;

	@Column(name = "position")
	private Integer position;

	@Column(name = "libelle")
	private String libelle;

	@Column(name = "specifique")
	private Boolean specifique;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "statut", nullable = false)
	private ParamStatutEnum statut;
	
	@OneToMany(mappedBy = "paramChapitre", fetch = FetchType.LAZY)
	private List<ParamLigne> paramLignes;

	public ParamChapitre() {
		this.specifique = false; // Par défaut ce n'est pas un module spécifique
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ParamModule getModule() {
		return paramModule;
	}

	public void setModule(ParamModule paramModule) {
		this.paramModule = paramModule;
	}

	public ParamOnglet getOnglet() {
		return paramOnglet;
	}

	public void setOnglet(ParamOnglet paramOnglet) {
		this.paramOnglet = paramOnglet;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Boolean getSpecifique() {
		return specifique;
	}

	public void setSpecifique(Boolean specifique) {
		this.specifique = specifique;
	}

	public ParamStatutEnum getStatut() {
		return statut;
	}

	public void setStatut(ParamStatutEnum statut) {
		this.statut = statut;
	}

	public ParamModule getParamModule() {
		return paramModule;
	}

	public void setParamModule(ParamModule paramModule) {
		this.paramModule = paramModule;
	}

	public ParamOnglet getParamOnglet() {
		return paramOnglet;
	}

	public void setParamOnglet(ParamOnglet paramOnglet) {
		this.paramOnglet = paramOnglet;
	}

	public List<ParamLigne> getParamLignes() {
		return paramLignes;
	}

	public void setParamLignes(List<ParamLigne> paramLignes) {
		this.paramLignes = paramLignes;
	}

}
