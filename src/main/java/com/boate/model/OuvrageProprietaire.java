package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ouvrage_proprietaire")
public class OuvrageProprietaire {

	@Id
	@SequenceGenerator(name = "ouvrage_proprietaire_id_seq", sequenceName = "ouvrage_proprietaire_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ouvrage_proprietaire_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "ouvrage_id", nullable = false)
	private Ouvrage ouvrage;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "proprietaire_id", nullable = false)
	private Proprietaire proprietaire;
	
	@Column(name = "pourcentage")
	private Double pourcentage;
	
	@Column(name = "ordre")
	private Integer ordre;

	public OuvrageProprietaire() {
		
	}
	
	public OuvrageProprietaire(Integer id, Ouvrage ouvrage, Proprietaire proprietaire, Double pourcentage,
			Integer ordre) {
		super();
		this.id = id;
		this.ouvrage = ouvrage;
		this.proprietaire = proprietaire;
		this.pourcentage = pourcentage;
		this.ordre = ordre;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Ouvrage getOuvrage() {
		return ouvrage;
	}

	public void setOuvrage(Ouvrage ouvrage) {
		this.ouvrage = ouvrage;
	}

	public Proprietaire getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(Proprietaire proprietaire) {
		this.proprietaire = proprietaire;
	}

	public Double getPourcentage() {
		return pourcentage;
	}

	public void setPourcentage(Double pourcentage) {
		this.pourcentage = pourcentage;
	}

	public Integer getOrdre() {
		return ordre;
	}

	public void setOrdre(Integer ordre) {
		this.ordre = ordre;
	}
	
}
