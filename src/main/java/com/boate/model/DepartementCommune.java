package com.boate.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "departement_commune")
public class DepartementCommune {

	@Id
	@SequenceGenerator(name = "departement_commune_id_seq", sequenceName = "departement_commune_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "departement_commune_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "commune_id", nullable = false)
	private Commune commune;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "departement_id", nullable = false)
	private Departement departement;

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(final Departement departement) {
		this.departement = departement;
	}

	public Commune getCommune() {
		return commune;
	}

	public void setCommune(Commune commune) {
		this.commune = commune;
	}
}
