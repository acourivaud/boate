package com.boate.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.boate.utils.LocalDateConverter;

@Entity
@Table(name = "work_maintenance")
public class OuvrageTravaux {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "work_id", nullable = true)
	private Ouvrage ouvrage;

	/*@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "work_visit_id", nullable = true)
	private Bouclage bouclage;*/
	
	/*@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "work_visit_expertise_id", nullable = true)
	private Expertise expertise;*/

	@Column(name = "prevision_cost")
	private Integer coutPrevisionnel;

	@Column(name = "real_cost")
	private Integer coutReel;

	@Column(name = "urgence_key")
	private String urgence;

	@Column(name = "prevision_labor")
	private String travauxPrevisionnel;

	@Column(name = "real_labor")
	private String travauxReel;

	@Column(name = "prevision_date")
	private Integer anneePrevisionnelle;

	@Column(name = "date_realisation_travaux")
	@Convert(converter = LocalDateConverter.class)
	private LocalDate dateReelle;

	@Column(name = "commentaire")
	private String commentaires;

	// SO: sans rattachement à un ouvrage. Possible que sur import BDD
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "ligne", nullable = true)
	private Ligne ligneSO;

	// SO: sans rattachement à un ouvrage. Possible que sur import BDD
	@Column(name = "pk")
	private String pkSO;

	// SO: sans rattachement à un ouvrage. Possible que sur import BDD
	@Column(name = "categorie")
	private String categorieSO;

	// SO: sans rattachement à un ouvrage. Possible que sur import BDD
	@Column(name = "visit_type")
	private String typeVisiteSO;

	// SO: sans rattachement à un ouvrage. Possible que sur import BDD
	@Column(name = "visit_date")
	private Integer anneeVisiteSO;

	@Column(name = "prev_has_courriers")
	private Boolean prevHasCourriers;

	@Column(name = "real_has_courriers")
	private String realHasCourriers;
	
	@Column(name = "prev_has_expertise")
	private Boolean prevHasExpertise;
	
	@Column(name = "real_has_expertise")
	private String realHasExpertise;
	
	@Column(name = "prev_cote_confortement")
	private String prevCoteConfortement;
	
	@Column(name = "prev_cost_confortement")
	private Integer prevCostConfortement;
	
	@Column(name = "real_cost_confortement")
	private Integer realCostConfortement;
	
	@Column(name = "prev_cote_curage")
	private String prevCoteCurage;
	
	@Column(name = "prev_cost_curage")
	private Integer prevCostCurage;
	
	@Column(name = "real_cost_curage")
	private Integer realCostCurage;
	
	@Column(name = "prev_cote_debroussaillage")
	private String prevCoteDebroussaillage;
	
	@Column(name = "prev_cost_debroussaillage")
	private Integer prevCostDebroussaillage;
	
	@Column(name = "real_cost_debroussaillage")
	private Integer realCostDebroussaillage;
	
	@Column(name = "prev_cote_instrumentation")
	private String prevCoteInstrumentation;
	
	@Column(name = "prev_cost_instrumentation")
	private Integer prevCostInstrumentation;
	
	@Column(name = "real_cost_instrumentation")
	private Integer realCostInstrumentation;
	
	@Column(name = "prev_cost_maconnerie")
	private Integer prevCostMaconnerie;
	
	@Column(name = "real_cost_maconnerie")
	private Integer realCostMaconnerie;
	
	@Column(name = "prev_cost_peinture")
	private Integer prevCostPeinture;
	
	@Column(name = "real_cost_peinture")
	private Integer realCostPeinture;
	
	@Column(name = "prev_cote_purge")
	private String prevCotePurge;
	
	@Column(name = "prev_cost_purge")
	private Integer prevCostPurge;
	
	@Column(name = "real_cost_purge")
	private Integer realCostPurge;
	
	@Column(name = "prev_cost_serrurerie")
	private Integer prevCostSerrurerie;
	
	@Column(name = "real_cost_serrurerie")
	private Integer realCostSerrurerie;
	
	@Column(name = "prev_cote_terrassement")
	private String prevCoteTerrassement;
	
	@Column(name = "prev_cost_terrassement")
	private Integer prevCostTerrassement;
	
	@Column(name = "real_cost_terrassement")
	private Integer realCostTerrassement;
	
	@Column(name = "prev_cote_topographie")
	private String prevCoteTopographie;
	
	@Column(name = "prev_cost_topographie")
	private Integer prevCostTopographie;
	
	@Column(name = "real_cost_topographie")
	private Integer realCostTopographie;
	
	@Column(name = "prev_cote_divers")
	private String prevCoteDivers;
	
	@Column(name = "prev_cost_divers")
	private Integer prevCostDivers;
	
	@Column(name = "real_cost_divers")
	private Integer realCostDivers;

	@Column(name = "travaux_termines")
	private Boolean travauxTermines;
	
	@Column(name = "todo_labor")
	private String travauxRestant;
	
	@Column(name = "delai_realisation_travaux")
	private String delaiRealisationTravaux;

	@Column(name = "statut")
	private String statut;

	@Column(name = "auteur_suppression")
	private String auteurSuppression;

	@Column(name = "date_suppression", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate dateSuppression;

	/** default constructor */
	public OuvrageTravaux() {
		this.statut = "A";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Ouvrage getOuvrage() {
		return ouvrage;
	}

	public void setOuvrage(Ouvrage ouvrage) {
		this.ouvrage = ouvrage;
	}

	public Integer getCoutPrevisionnel() {
		return coutPrevisionnel;
	}

	public void setCoutPrevisionnel(Integer coutPrevisionnel) {
		this.coutPrevisionnel = coutPrevisionnel;
	}

	public Integer getCoutReel() {
		return coutReel;
	}

	public void setCoutReel(Integer coutReel) {
		this.coutReel = coutReel;
	}

	public String getUrgence() {
		return urgence;
	}

	public void setUrgence(String urgence) {
		this.urgence = urgence;
	}

	public String getTravauxPrevisionnel() {
		return travauxPrevisionnel;
	}

	public void setTravauxPrevisionnel(String travauxPrevisionnel) {
		this.travauxPrevisionnel = travauxPrevisionnel;
	}

	public String getTravauxReel() {
		return travauxReel;
	}

	public void setTravauxReel(String travauxReel) {
		this.travauxReel = travauxReel;
	}

	public Integer getAnneePrevisionnelle() {
		return anneePrevisionnelle;
	}

	public void setAnneePrevisionnelle(Integer anneePrevisionnelle) {
		this.anneePrevisionnelle = anneePrevisionnelle;
	}

	public LocalDate getDateReelle() {
		return dateReelle;
	}

	public void setDateReelle(LocalDate dateReelle) {
		this.dateReelle = dateReelle;
	}

	public String getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}

	public Ligne getLigneSO() {
		return ligneSO;
	}

	public void setLigneSO(Ligne ligneSO) {
		this.ligneSO = ligneSO;
	}

	public String getPkSO() {
		return pkSO;
	}

	public void setPkSO(String pkSO) {
		this.pkSO = pkSO;
	}

	public String getCategorieSO() {
		return categorieSO;
	}

	public void setCategorieSO(String categorieSO) {
		this.categorieSO = categorieSO;
	}

	public String getTypeVisiteSO() {
		return typeVisiteSO;
	}

	public void setTypeVisiteSO(String typeVisiteSO) {
		this.typeVisiteSO = typeVisiteSO;
	}

	public Integer getAnneeVisiteSO() {
		return anneeVisiteSO;
	}

	public void setAnneeVisiteSO(Integer anneeVisiteSO) {
		this.anneeVisiteSO = anneeVisiteSO;
	}

	public Boolean getPrevHasCourriers() {
		return prevHasCourriers;
	}

	public void setPrevHasCourriers(Boolean prevHasCourriers) {
		this.prevHasCourriers = prevHasCourriers;
	}

	public String getRealHasCourriers() {
		return realHasCourriers;
	}

	public void setRealHasCourriers(String realHasCourriers) {
		this.realHasCourriers = realHasCourriers;
	}

	public Boolean getPrevHasExpertise() {
		return prevHasExpertise;
	}

	public void setPrevHasExpertise(Boolean prevHasExpertise) {
		this.prevHasExpertise = prevHasExpertise;
	}

	public String getRealHasExpertise() {
		return realHasExpertise;
	}

	public void setRealHasExpertise(String realHasExpertise) {
		this.realHasExpertise = realHasExpertise;
	}

	public String getPrevCoteConfortement() {
		return prevCoteConfortement;
	}

	public void setPrevCoteConfortement(String prevCoteConfortement) {
		this.prevCoteConfortement = prevCoteConfortement;
	}

	public Integer getPrevCostConfortement() {
		return prevCostConfortement;
	}

	public void setPrevCostConfortement(Integer prevCostConfortement) {
		this.prevCostConfortement = prevCostConfortement;
	}

	public Integer getRealCostConfortement() {
		return realCostConfortement;
	}

	public void setRealCostConfortement(Integer realCostConfortement) {
		this.realCostConfortement = realCostConfortement;
	}

	public String getPrevCoteCurage() {
		return prevCoteCurage;
	}

	public void setPrevCoteCurage(String prevCoteCurage) {
		this.prevCoteCurage = prevCoteCurage;
	}

	public Integer getPrevCostCurage() {
		return prevCostCurage;
	}

	public void setPrevCostCurage(Integer prevCostCurage) {
		this.prevCostCurage = prevCostCurage;
	}

	public Integer getRealCostCurage() {
		return realCostCurage;
	}

	public void setRealCostCurage(Integer realCostCurage) {
		this.realCostCurage = realCostCurage;
	}

	public String getPrevCoteDebroussaillage() {
		return prevCoteDebroussaillage;
	}

	public void setPrevCoteDebroussaillage(String prevCoteDebroussaillage) {
		this.prevCoteDebroussaillage = prevCoteDebroussaillage;
	}

	public Integer getPrevCostDebroussaillage() {
		return prevCostDebroussaillage;
	}

	public void setPrevCostDebroussaillage(Integer prevCostDebroussaillage) {
		this.prevCostDebroussaillage = prevCostDebroussaillage;
	}

	public Integer getRealCostDebroussaillage() {
		return realCostDebroussaillage;
	}

	public void setRealCostDebroussaillage(Integer realCostDebroussaillage) {
		this.realCostDebroussaillage = realCostDebroussaillage;
	}

	public String getPrevCoteInstrumentation() {
		return prevCoteInstrumentation;
	}

	public void setPrevCoteInstrumentation(String prevCoteInstrumentation) {
		this.prevCoteInstrumentation = prevCoteInstrumentation;
	}

	public Integer getPrevCostInstrumentation() {
		return prevCostInstrumentation;
	}

	public void setPrevCostInstrumentation(Integer prevCostInstrumentation) {
		this.prevCostInstrumentation = prevCostInstrumentation;
	}

	public Integer getRealCostInstrumentation() {
		return realCostInstrumentation;
	}

	public void setRealCostInstrumentation(Integer realCostInstrumentation) {
		this.realCostInstrumentation = realCostInstrumentation;
	}

	public Integer getPrevCostMaconnerie() {
		return prevCostMaconnerie;
	}

	public void setPrevCostMaconnerie(Integer prevCostMaconnerie) {
		this.prevCostMaconnerie = prevCostMaconnerie;
	}

	public Integer getRealCostMaconnerie() {
		return realCostMaconnerie;
	}

	public void setRealCostMaconnerie(Integer realCostMaconnerie) {
		this.realCostMaconnerie = realCostMaconnerie;
	}

	public Integer getPrevCostPeinture() {
		return prevCostPeinture;
	}

	public void setPrevCostPeinture(Integer prevCostPeinture) {
		this.prevCostPeinture = prevCostPeinture;
	}

	public Integer getRealCostPeinture() {
		return realCostPeinture;
	}

	public void setRealCostPeinture(Integer realCostPeinture) {
		this.realCostPeinture = realCostPeinture;
	}

	public String getPrevCotePurge() {
		return prevCotePurge;
	}

	public void setPrevCotePurge(String prevCotePurge) {
		this.prevCotePurge = prevCotePurge;
	}

	public Integer getPrevCostPurge() {
		return prevCostPurge;
	}

	public void setPrevCostPurge(Integer prevCostPurge) {
		this.prevCostPurge = prevCostPurge;
	}

	public Integer getRealCostPurge() {
		return realCostPurge;
	}

	public void setRealCostPurge(Integer realCostPurge) {
		this.realCostPurge = realCostPurge;
	}

	public Integer getPrevCostSerrurerie() {
		return prevCostSerrurerie;
	}

	public void setPrevCostSerrurerie(Integer prevCostSerrurerie) {
		this.prevCostSerrurerie = prevCostSerrurerie;
	}

	public Integer getRealCostSerrurerie() {
		return realCostSerrurerie;
	}

	public void setRealCostSerrurerie(Integer realCostSerrurerie) {
		this.realCostSerrurerie = realCostSerrurerie;
	}

	public String getPrevCoteTerrassement() {
		return prevCoteTerrassement;
	}

	public void setPrevCoteTerrassement(String prevCoteTerrassement) {
		this.prevCoteTerrassement = prevCoteTerrassement;
	}

	public Integer getPrevCostTerrassement() {
		return prevCostTerrassement;
	}

	public void setPrevCostTerrassement(Integer prevCostTerrassement) {
		this.prevCostTerrassement = prevCostTerrassement;
	}

	public Integer getRealCostTerrassement() {
		return realCostTerrassement;
	}

	public void setRealCostTerrassement(Integer realCostTerrassement) {
		this.realCostTerrassement = realCostTerrassement;
	}

	public String getPrevCoteTopographie() {
		return prevCoteTopographie;
	}

	public void setPrevCoteTopographie(String prevCoteTopographie) {
		this.prevCoteTopographie = prevCoteTopographie;
	}

	public Integer getPrevCostTopographie() {
		return prevCostTopographie;
	}

	public void setPrevCostTopographie(Integer prevCostTopographie) {
		this.prevCostTopographie = prevCostTopographie;
	}

	public Integer getRealCostTopographie() {
		return realCostTopographie;
	}

	public void setRealCostTopographie(Integer realCostTopographie) {
		this.realCostTopographie = realCostTopographie;
	}

	public String getPrevCoteDivers() {
		return prevCoteDivers;
	}

	public void setPrevCoteDivers(String prevCoteDivers) {
		this.prevCoteDivers = prevCoteDivers;
	}

	public Integer getPrevCostDivers() {
		return prevCostDivers;
	}

	public void setPrevCostDivers(Integer prevCostDivers) {
		this.prevCostDivers = prevCostDivers;
	}

	public Integer getRealCostDivers() {
		return realCostDivers;
	}

	public void setRealCostDivers(Integer realCostDivers) {
		this.realCostDivers = realCostDivers;
	}

	public Boolean getTravauxTermines() {
		return travauxTermines;
	}

	public void setTravauxTermines(Boolean travauxTermines) {
		this.travauxTermines = travauxTermines;
	}

	public String getTravauxRestant() {
		return travauxRestant;
	}

	public void setTravauxRestant(String travauxRestant) {
		this.travauxRestant = travauxRestant;
	}

	public String getDelaiRealisationTravaux() {
		return delaiRealisationTravaux;
	}

	public void setDelaiRealisationTravaux(String delaiRealisationTravaux) {
		this.delaiRealisationTravaux = delaiRealisationTravaux;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getAuteurSuppression() {
		return auteurSuppression;
	}

	public void setAuteurSuppression(String auteurSuppression) {
		this.auteurSuppression = auteurSuppression;
	}

	public LocalDate getDateSuppression() {
		return dateSuppression;
	}

	public void setDateSuppression(LocalDate dateSuppression) {
		this.dateSuppression = dateSuppression;
	}

}
