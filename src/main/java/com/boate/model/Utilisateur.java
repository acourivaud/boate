package com.boate.model;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.boate.enums.EntiteUtilisateurEnum;
import com.boate.enums.FonctionUtilisateurEnum;
import com.boate.utils.LocalDateConverter;

@Entity
@Table(name = "user_table")
public class Utilisateur implements UserDetails {

	private static final long serialVersionUID = -6816461683457311429L;

	static final int UNKNOWN_SCOPE = 0;
	static final int REGION_SCOPE = 1;
	static final int EVEN_SCOPE = 2;
	static final int UP_SCOPE = 3;
	static final int REFERENT_SCOPE = 4;

	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "utilisateur_id_seq", sequenceName = "utilisateur_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "utilisateur_id_seq")
	private Integer idUtilisateur;

	@Column(name = "login")
	private String login;

	@Enumerated(EnumType.STRING)
	@Column(name = "fonction_key", nullable = false)
	private FonctionUtilisateurEnum fonction;

	@Enumerated(EnumType.STRING)
	@Column(name = "entity_key", nullable = false)
	private EntiteUtilisateurEnum entite;

	@Column(name = "password")
	private String password;

	@Column(name = "last_name")
	private String nom;

	@Column(name = "first_name")
	private String prenom;
	
	@Column(name = "region_key")
	private String region;
	
	@Column(name = "telephone")
	private String telephone;
	
	@Column(name = "cellphone")
	private String cellphone;
	
	@Column(name = "fax")
	private String fax;
	
	@Column(name = "email")
	private String email;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "up_id", nullable = true)
	private UniteOperationnelle up;
	
	@Column(name = "mission_end_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate dateFinMission;
	
	@Column(name = "url_sig")
	private String urlSig;
	
	@Column(name = "ssid")
	private String ssid;
	
	@Column(name = "changement_password")
	private Boolean changementPassword;
	
	@Column(name = "nb_resultat")
	private Integer nbLignesTableau;
	
	@Column(name = "affichage_alerte")
	private Boolean affichageAlerte;
	
	@Column(name = "consultation_message")
	private Boolean consultationReleaseNotes;
	
	@Column(name = "page_accueil")
	private String pageAccueil;
	
	@OneToMany(mappedBy = "utilisateur", fetch = FetchType.EAGER)
	private List<UtilisateurPermission> permissions;
	
	public Integer getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(Integer idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	@Override
	public boolean isAccountNonExpired() {
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@Override
	public boolean isEnabled() {
		return false;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getUsername() {
		return this.login;
	}
	
	public String getFullName() {
		return this.prenom != null && this.nom != null ? this.prenom.concat(" " + this.nom) : this.prenom != null ? this.prenom : this.login;
	}

	/**
	 * Returns the value of the scope associated to the user, either REGION, EVEN,
	 * UP or REFERENT. Scope is defined as follows :
	 * 
	 * <pre>
	 * RI OA/OT : Région
	 * RM OA/OT : Région
	 * DET : EVEN
	 * ROTP : EVEN
	 * SOAR : Référent
	 * AOAP : EVEN
	 * CU/DUO : UP
	 * AOAU : UP
	 * AH : UP
	 * RRMI : Région
	 * RMPL : Région
	 * DDI : Région
	 * R PRLILY OA : Région
	 * R PRILY : Région
	 * INVITE : ?
	 * COT : ?
	 * Autres : ?
	 * adj : Région
	 * </pre>
	 * 
	 * @return either REGION, EVEN, UP or REFERENT.
	 */
	int getScope() {
		int scope = UNKNOWN_SCOPE;

		if (FonctionUtilisateurEnum.FUNCTION_RI_OA_OT.equals(this.fonction) || FonctionUtilisateurEnum.FUNCTION_RM_OA_OT.equals(this.fonction)
				|| FonctionUtilisateurEnum.FUNCTION_RRMI.equals(this.fonction) || FonctionUtilisateurEnum.FUNCTION_RMPL.equals(this.fonction)
				|| FonctionUtilisateurEnum.FUNCTION_DDI.equals(this.fonction)
				|| FonctionUtilisateurEnum.FUNCTION_R_PRILY_Y.equals(this.fonction)
				|| FonctionUtilisateurEnum.FUNCTION_R_PRILY.equals(this.fonction)
				|| FonctionUtilisateurEnum.FUNCTION_ADJRI_OA_OT.equals(this.fonction)) { // adj ?

			scope = REGION_SCOPE;

		} else if (FonctionUtilisateurEnum.FUNCTION_DET.equals(this.fonction) || FonctionUtilisateurEnum.FUNCTION_ROTP.equals(this.fonction)
				|| FonctionUtilisateurEnum.FUNCTION_AOAP.equals(this.fonction)) {

			scope = EVEN_SCOPE;

		} else if (FonctionUtilisateurEnum.FUNCTION_CU_DUO.equals(this.fonction) || FonctionUtilisateurEnum.FUNCTION_AOAU.equals(this.fonction)
				|| FonctionUtilisateurEnum.FUNCTION_AH.equals(this.fonction)) {

			scope = UP_SCOPE;

		} else if (FonctionUtilisateurEnum.FUNCTION_SOAR.equals(this.fonction)) {

			scope = REFERENT_SCOPE;
		}

		// Scope stays to UNKNOWN_SCOPE state for FUNCTION_INVITE, FUNCTION_COT and
		// FUNCTION_OTHER

		return scope;
	}

	public FonctionUtilisateurEnum getFonction() {
		return fonction;
	}

	public void setFonction(FonctionUtilisateurEnum fonction) {
		this.fonction = fonction;
	}

	public EntiteUtilisateurEnum getEntite() {
		return entite;
	}

	public void setEntite(EntiteUtilisateurEnum entite) {
		this.entite = entite;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UniteOperationnelle getUp() {
		return up;
	}

	public void setUp(UniteOperationnelle up) {
		this.up = up;
	}

	public LocalDate getDateFinMission() {
		return dateFinMission;
	}

	public void setDateFinMission(LocalDate dateFinMission) {
		this.dateFinMission = dateFinMission;
	}

	public String getUrlSig() {
		return urlSig;
	}

	public void setUrlSig(String urlSig) {
		this.urlSig = urlSig;
	}

	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	public Boolean getChangementPassword() {
		return changementPassword;
	}

	public void setChangementPassword(Boolean changementPassword) {
		this.changementPassword = changementPassword;
	}

	public Integer getNbLignesTableau() {
		return nbLignesTableau;
	}

	public void setNbLignesTableau(Integer nbLignesTableau) {
		this.nbLignesTableau = nbLignesTableau;
	}

	public Boolean getAffichageAlerte() {
		return affichageAlerte;
	}

	public void setAffichageAlerte(Boolean affichageAlerte) {
		this.affichageAlerte = affichageAlerte;
	}

	public Boolean getConsultationReleaseNotes() {
		return consultationReleaseNotes;
	}

	public void setConsultationReleaseNotes(Boolean consultationReleaseNotes) {
		this.consultationReleaseNotes = consultationReleaseNotes;
	}

	public String getPageAccueil() {
		return pageAccueil;
	}

	public void setPageAccueil(String pageAccueil) {
		this.pageAccueil = pageAccueil;
	}

	public List<UtilisateurPermission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<UtilisateurPermission> permissions) {
		this.permissions = permissions;
	}
	
	public boolean hasPermission(String permission) {
		List<UtilisateurPermission> permissions = this.getPermissions();
		if (permissions != null && permissions.size() > 0) {
			for (UtilisateurPermission utilisateurPermission : permissions) {
				if (utilisateurPermission.getPermission().getLibelle().equals(permission)) {
					return true;
				}
			}
		}
		return false;
	}

}
