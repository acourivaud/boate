package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "indicator_security")
public class IndicateurSecurite {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "type")
	private String code;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "region_id", nullable = true)
	private Region region;
	
	@Column(name = "t1")
	private Integer t1;

	@Column(name = "t2")
	private Integer t2;

	@Column(name = "t3")
	private Integer t3;

	@Column(name = "t4")
	private Integer t4;

	@Column(name = "t5")
	private Integer t5;
	
	@Column(name = "year")
	private Integer annee;
	
	@Column(name = "infrapole_key")
	private String infrapole;
	
	@Column(name = "observation")
	private String observation;

	public IndicateurSecurite() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAnnee() {
		return annee;
	}

	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Integer getT1() {
		return t1;
	}

	public void setT1(Integer t1) {
		this.t1 = t1;
	}

	public Integer getT2() {
		return t2;
	}

	public void setT2(Integer t2) {
		this.t2 = t2;
	}

	public Integer getT3() {
		return t3;
	}

	public void setT3(Integer t3) {
		this.t3 = t3;
	}

	public Integer getT4() {
		return t4;
	}

	public void setT4(Integer t4) {
		this.t4 = t4;
	}

	public Integer getT5() {
		return t5;
	}

	public void setT5(Integer t5) {
		this.t5 = t5;
	}

	public String getInfrapole() {
		return infrapole;
	}

	public void setInfrapole(String infrapole) {
		this.infrapole = infrapole;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

}
