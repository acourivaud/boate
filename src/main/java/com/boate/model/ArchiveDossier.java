package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(region = "archiveDossierCache", usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "archivages_dossier")
public class ArchiveDossier {

	@Id
	@SequenceGenerator(name = "archivages_dossier_id_seq", sequenceName = "archivages_dossier_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "archivages_dossier_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "boite_id", nullable = false)
	private ArchiveBoite boite;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "work_id", nullable = true)
	private Ouvrage ouvrage;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "user_id", nullable = true)
	private Utilisateur emprunteur;

	@Column(name = "num_dossier")
	private String numeroDossier;

	@Column(name = "service")
	private String service;

	@Column(name = "description")
	private String description;

	@Column(name = "date_debut")
	private Integer anneeDebut;

	@Column(name = "date_fin")
	private Integer anneeFin;

	@Column(name = "dua")
	private String dua;

	@Column(name = "sort_finale")
	private String sortFinale;

	@Column(name = "observation")
	private String commentaires;

	@Column(name = "pk")
	private String pk;

	@Column(name = "end_pk")
	private String endPk;

	@Column(name = "line_archive")
	private String ligneNonRattachee;

	@Column(name = "type")
	private String typeArchive;

	public ArchiveDossier() {

	}

	public ArchiveDossier(Integer id, ArchiveBoite boite, Ouvrage ouvrage, String numeroDossier, String service,
			String description, Integer anneeDebut, Integer anneeFin, String dua, String sortFinale,
			String commentaires, String pk, String endPk, String ligneNonRattachee, String typeArchive, Utilisateur emprunteur) {
		super();
		this.id = id;
		this.boite = boite;
		this.ouvrage = ouvrage;
		this.numeroDossier = numeroDossier;
		this.service = service;
		this.description = description;
		this.anneeDebut = anneeDebut;
		this.anneeFin = anneeFin;
		this.dua = dua;
		this.sortFinale = sortFinale;
		this.commentaires = commentaires;
		this.pk = pk;
		this.endPk = endPk;
		this.ligneNonRattachee = ligneNonRattachee;
		this.typeArchive = typeArchive;
		this.emprunteur = emprunteur;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumeroDossier() {
		return numeroDossier;
	}

	public void setNumeroDossier(String numeroDossier) {
		this.numeroDossier = numeroDossier;
	}

	public ArchiveBoite getBoite() {
		return boite;
	}

	public void setBoite(ArchiveBoite boite) {
		this.boite = boite;
	}

	public Ouvrage getOuvrage() {
		return ouvrage;
	}

	public void setOuvrage(Ouvrage ouvrage) {
		this.ouvrage = ouvrage;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getAnneeDebut() {
		return anneeDebut;
	}

	public void setAnneeDebut(Integer anneeDebut) {
		this.anneeDebut = anneeDebut;
	}

	public Integer getAnneeFin() {
		return anneeFin;
	}

	public void setAnneeFin(Integer anneeFin) {
		this.anneeFin = anneeFin;
	}

	public String getDua() {
		return dua;
	}

	public void setDua(String dua) {
		this.dua = dua;
	}

	public String getSortFinale() {
		return sortFinale;
	}

	public void setSortFinale(String sortFinale) {
		this.sortFinale = sortFinale;
	}

	public String getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}

	public String getPk() {
		return pk;
	}

	public void setPk(String pk) {
		this.pk = pk;
	}

	public String getEndPk() {
		return endPk;
	}

	public void setEnd_pk(String endPk) {
		this.endPk = endPk;
	}

	public String getLigneNonRattachee() {
		return ligneNonRattachee;
	}

	public void setLigneNonRattachee(String ligneNonRattachee) {
		this.ligneNonRattachee = ligneNonRattachee;
	}

	public String getTypeArchive() {
		return typeArchive;
	}

	public void setTypeArchive(String typeArchive) {
		this.typeArchive = typeArchive;
	}

	public Utilisateur getEmprunteur() {
		return emprunteur;
	}

	public void setEmprunteur(Utilisateur emprunteur) {
		this.emprunteur = emprunteur;
	}

}
