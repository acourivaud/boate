package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "owner")
public class Proprietaire {

	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "owner_id_seq", sequenceName = "owner_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "owner_id_seq")
	private Integer id;

	@Column(name = "name")
	private String name;

	public Proprietaire(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Proprietaire() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
