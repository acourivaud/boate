package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "up_line")
public class UniteOperationnelleLigne {

	@Id
	@SequenceGenerator(name = "up_line_id_seq", sequenceName = "up_line_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "up_line_id_seq")
	private Integer id;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "up_id", nullable = false)
	private UniteOperationnelle uniteOperationnelle;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "line_id", nullable = false)
	private Ligne ligne;

	@Column(name = "pk_begin")
	private String pkDebut;
	
	@Column(name = "pk_end")
	private String pkFin;

	@Column(name = "life")
	private Boolean life;
	
	@Column(name = "track")
	private String voie;

	public UniteOperationnelleLigne() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UniteOperationnelle getUniteOperationnelle() {
		return uniteOperationnelle;
	}

	public void setUniteOperationnelle(UniteOperationnelle uniteOperationnelle) {
		this.uniteOperationnelle = uniteOperationnelle;
	}

	public Ligne getLigne() {
		return ligne;
	}

	public void setLigne(Ligne ligne) {
		this.ligne = ligne;
	}

	public String getPkDebut() {
		return pkDebut;
	}

	public void setPkDebut(String pkDebut) {
		this.pkDebut = pkDebut;
	}

	public String getPkFin() {
		return pkFin;
	}

	public void setPkFin(String pkFin) {
		this.pkFin = pkFin;
	}

	public Boolean getLife() {
		return life;
	}

	public void setLife(Boolean life) {
		this.life = life;
	}

	public String getVoie() {
		return voie;
	}

	public void setVoie(String voie) {
		this.voie = voie;
	}

}
