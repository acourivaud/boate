package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.boate.enums.ParamStatutEnum;

@Entity
@Table(name = "param_champ_definition")
public class ParamLigneDefinition {

	@Id
	@SequenceGenerator(name = "param_champ_definition_id_seq", sequenceName = "param_champ_definition_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "param_champ_definition_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "param_ligne_id", nullable = false)
	private ParamLigne paramLigne;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "param_champ_id", nullable = false)
	private ParamChamp paramChamp;

	@Column(name = "position")
	private Integer position;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "statut", nullable = false)
	private ParamStatutEnum statut;
	
	@Column(name = "taille_grid_label")
	private Integer tailleGridLabel;
	
	@Column(name = "taille_grid_value")
	private Integer tailleGridValue;
	
	@Column(name = "label_value")
	private String labelValue;

	public ParamLigneDefinition() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ParamLigne getParamLigne() {
		return paramLigne;
	}

	public void setParamLigne(ParamLigne paramLigne) {
		this.paramLigne = paramLigne;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public ParamStatutEnum getStatut() {
		return statut;
	}

	public void setStatut(ParamStatutEnum statut) {
		this.statut = statut;
	}

	public Integer getTailleGridLabel() {
		return tailleGridLabel;
	}

	public void setTailleGridLabel(Integer tailleGridLabel) {
		this.tailleGridLabel = tailleGridLabel;
	}

	public Integer getTailleGridValue() {
		return tailleGridValue;
	}

	public void setTailleGridValue(Integer tailleGridValue) {
		this.tailleGridValue = tailleGridValue;
	}

	public ParamChamp getParamChamp() {
		return paramChamp;
	}

	public void setParamChamp(ParamChamp paramChamp) {
		this.paramChamp = paramChamp;
	}

	public String getLabelValue() {
		return labelValue;
	}

	public void setLabelValue(String labelValue) {
		this.labelValue = labelValue;
	}

}
