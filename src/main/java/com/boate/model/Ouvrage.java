package com.boate.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.boate.utils.ConvertData;
import com.boate.utils.LocalDateConverter;

@Entity
@Table(name = "work")
public class Ouvrage {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "nb_fichier")
	private Integer nbFichier;

	@Column(name = "cord_x")
	private String cordX;
	@Column(name = "cord_y")
	private String cordY;
	@Column(name = "cord_x_inter")
	private String cordXInter;
	@Column(name = "cord_y_inter")
	private String cordYInter;
	@Column(name = "cord_x_end")
	private String cordXEnd;
	@Column(name = "cord_y_end")
	private String cordYEnd;

	@Column(name = "photo_ouvrage")
	private String photoOuvrage;

	@Column(name = "niveau_etat")
	private String niveauEtat;

	@Column(name = "niveau_gravite")
	private String niveauGravite;

	@Column(name = "livret")
	private Character livret;

	@Column(name = "date_maj")
	private String dateMaj;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "auteur_maj", nullable = true)
	private Utilisateur auteurMaj;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "region_id", nullable = false)
	private Region region;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "first_departement_id", nullable = true)
	private Departement firstDepartement;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "second_departement_id", nullable = true)
	private Departement secondDepartement;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "first_city_id", nullable = true)
	private Commune firstCity;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "second_city_id", nullable = true)
	private Commune secondCity;

	@Column(name = "category_key")
	private String categoryKey;

	@Column(name = "type_patrimoine")
	private String typePatrimoine;

	@Column(name = "material_key")
	private String materialKey;

	@Column(name = "materiau_key")
	private String materiauKey;

	@Column(name = "geometry_key")
	private String geometryKey;

	@Column(name = "situation_key")
	private String situationKey;

	@Column(name = "fsa_key")
	private String fsaKey;

	@Column(name = "fsa_auteur")
	private String fsaAuteur;

	@Column(name = "occupation_key")
	private String occupationKey;

	@Column(name = "assembly_key")
	private String assemblyKey;

	@Column(name = "life")
	private Boolean life;

	@Column(name = "name")
	private String name;

	@Column(name = "pk")
	private Double pk;

	@Column(name = "middle_pk")
	private Double middlePk;

	@Column(name = "end_pk")
	private Double endPk;

	@Column(name = "building_span_number")
	private Integer buildingSpanNumber;

	@Column(name = "buildingTrack")
	private Boolean buildingTrack;

	@Column(name = "ot_height")
	private Double otHeight;

	@Column(name = "ot_fosse_crete")
	private String otFosse;

	@Column(name = "year")
	private Integer year;

	@Column(name = "year_modif")
	private Integer yearModif;

	@Column(name = "description")
	private String description;

	@Column(name = "above_below")
	private String aboveBelow;

	@Column(name = "indice")
	private String indice;

	@Column(name = "owner_life")
	private Boolean ownerLife;

	@Column(name = "bnc_gestion")
	private String bncGestion;

	@Column(name = "bnc_mainteneur")
	private String bncMainteneur;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "first_owner_name", nullable = true)
	private Proprietaire firstOwnerName;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "second_owner_name", nullable = true)
	private Proprietaire secondOwnerName;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "third_owner_name", nullable = true)
	private Proprietaire thirdOwnerName;

	@Column(name = "etat_dossier")
	private String etatDossier;

	@Column(name = "first_owner_percent")
	private Double firstOwnerPercent;

	@Column(name = "second_owner_percent")
	private Double secondOwnerPercent;

	@Column(name = "third_owner_percent")
	private Double thirdOwnerPercent;

	@Column(name = "monument")
	private Boolean monument;

	@Column(name = "orsec")
	private Boolean orsec;

	@Column(name = "natural_risk")
	private Boolean naturalRisk;

	@Column(name = "industrial_risk")
	private Boolean industrialRisk;

	@Column(name = "seismic_risk")
	private Boolean seismicRisk;

	@Column(name = "road_accident_risk")
	private Boolean roadAccidentRisk;

	@Column(name = "fluvial_accident_risk")
	private Boolean fluvialAccidentRisk;

	@Column(name = "restriction")
	private Boolean restriction;

	@Column(name = "convention")
	private Boolean convention;

	@Column(name = "code_segment")
	private String codeSegment;

	@Column(name = "oa_couverture")
	private Double oaCouverture;

	@Column(name = "oa_hauteur")
	private Double oaHauteur;

	@Column(name = "oa_hauteur_mini")
	private Double oaHauteurMini;

	@Column(name = "distance_rail")
	private Double distanceRail;

	@Column(name = "oa_hauteur_libre")
	private Double oaHauteurLibre;

	@Column(name = "oa_longueur")
	private Double oaLongueur;

	@Column(name = "oa_ouverture")
	private Double oaOuverture;

	@Column(name = "oa_surbaisse")
	private Boolean oaSurbaisse;

	@Column(name = "oa_largeur")
	private Double oaLargeur;

	@Column(name = "oa_span_number")
	private Integer oaSpanNumber;

	@Column(name = "oa_deck_number")
	private Integer oaDeckNumber;

	@Column(name = "bias")
	private Double bias;

	@Column(name = "building_truss_number")
	private Integer buildingTrussNumber;

	@Column(name = "building_height_max")
	private Double buildingHeightMax;

	@Column(name = "building_span")
	private Double buildingSpan;

	@Column(name = "ouvrage_enveloppe")
	private Boolean ouvrageEnveloppe;

	@Column(name = "building_public")
	private Boolean buildingPublic;

	@Column(name = "ep")
	private Boolean ep;

	@Column(name = "ot_nature")
	private String otNature;

	@Column(name = "oa_inclination")
	private Double oaInclination;

	@Column(name = "oa_slope1")
	private Double oaSlope1;

	@Column(name = "oa_slope2")
	private Double oaSlope2;

	@Column(name = "oa_portee")
	private Double oaPortee;

	@Column(name = "ot_avarie")
	private String otAvarie;

	@Column(name = "ot_date_incident")
	private String otDateIncident;

	@Column(name = "ot_height_left")
	private Double otHeightLeft;

	@Column(name = "ot_height_right")
	private Double otHeightRight;

	@Column(name = "ot_depth")
	private Double otDepth;

	@Column(name = "ot_depth_left")
	private Double otDepthLeft;

	@Column(name = "ot_depth_right")
	private Double otDepthRight;

	@Column(name = "ot_fiche_signal")
	private Boolean otFicheSignal;

	@Column(name = "ot_slope")
	private Double otSlope;

	@Column(name = "ot_slope_left")
	private Double otSlopeLeft;

	@Column(name = "ot_slope_right")
	private Double otSlopeRight;

	@Column(name = "ot_type_confortement")
	private String otTypeConfortement;

	@Column(name = "ot_confortement_desc")
	private String otConfortementDesc;

	@Column(name = "protected_site")
	private Boolean protectedSite;

	@Column(name = "rattachement")
	private String rattachement;

	@Column(name = "uic")
	private String uic;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "up_id", nullable = false)
	private UniteOperationnelle up;

	@Column(name = "ot_mixte_type")
	private String otMixteType;

	@Column(name = "commentaires")
	private String commentaires;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "parent_work_id", nullable = true)
	private Ouvrage parentWork;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "line_id", nullable = false)
	private Ligne line;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "type_key", nullable = false)
	private TypeOuvrage typeKey;

	@Column(name = "visite_virtuelle")
	private String visiteVirtuelle;

	@Column(name = "risque_objet_1")
	private String risqueObj;

	@Column(name = "risque_objet_2")
	private String risqueObj2;

	@Column(name = "risque_objet_3")
	private String risqueObj3;

	@Column(name = "risque_nature_1")
	private String risqueNat;

	@Column(name = "risque_nature_2")
	private String risqueNat2;

	@Column(name = "risque_nature_3")
	private String risqueNat3;

	@Column(name = "risque_mecanisme_1")
	private String risqueMec;

	@Column(name = "risque_mecanisme_2")
	private String risqueMec2;

	@Column(name = "risque_mecanisme_3")
	private String risqueMec3;

	@Column(name = "ot_classement")
	private String otClassement;

	@Column(name = "statut")
	private String statut;

	@Column(name = "auteur_suppression")
	private String auteurSuppression;

	@Column(name = "campagne_bool")
	private Boolean presenceCampagne;

	@Column(name = "date_suppression", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate dateSuppression;
	
	@Column(name = "supprime")
	private Boolean supprime;
	
	@Column(name = "archive")
	private Boolean archive;

	/** default constructor */
	public Ouvrage() {
		this.statut = "A";
		this.supprime = false;
		this.archive = false;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNbFichier() {
		return nbFichier;
	}

	public void setNbFichier(Integer nbFichier) {
		this.nbFichier = nbFichier;
	}

	public String getCordX() {
		return cordX;
	}

	public void setCordX(String cordX) {
		this.cordX = cordX;
	}

	public String getCordY() {
		return cordY;
	}

	public void setCordY(String cordY) {
		this.cordY = cordY;
	}

	public String getCordXInter() {
		return cordXInter;
	}

	public void setCordXInter(String cordXInter) {
		this.cordXInter = cordXInter;
	}

	public String getCordYInter() {
		return cordYInter;
	}

	public void setCordYInter(String cordYInter) {
		this.cordYInter = cordYInter;
	}

	public String getCordXEnd() {
		return cordXEnd;
	}

	public void setCordXEnd(String cordXEnd) {
		this.cordXEnd = cordXEnd;
	}

	public String getCordYEnd() {
		return cordYEnd;
	}

	public void setCordYEnd(String cordYEnd) {
		this.cordYEnd = cordYEnd;
	}

	public String getPhotoOuvrage() {
		return photoOuvrage;
	}

	public void setPhotoOuvrage(String photoOuvrage) {
		this.photoOuvrage = photoOuvrage;
	}

	public String getNiveauEtat() {
		return niveauEtat;
	}

	public void setNiveauEtat(String niveauEtat) {
		this.niveauEtat = niveauEtat;
	}

	public String getNiveauGravite() {
		return niveauGravite;
	}

	public void setNiveauGravite(String niveauGravite) {
		this.niveauGravite = niveauGravite;
	}

	public Character getLivret() {
		return livret;
	}

	public void setLivret(Character livret) {
		this.livret = livret;
	}

	public String getDateMaj() {
		return dateMaj;
	}

	public void setDateMaj(String dateMaj) {
		this.dateMaj = dateMaj;
	}

	public Utilisateur getAuteurMaj() {
		return auteurMaj;
	}

	public void setAuteurMaj(Utilisateur auteurMaj) {
		this.auteurMaj = auteurMaj;
	}

	public String getCategoryKey(Boolean convertToLibelle, Boolean libelleSimplifie) {
		if (convertToLibelle) {
			return libelleSimplifie ? ConvertData.getCategoryMap().get(this.categoryKey).getLibelleSimplifie()
					: ConvertData.getCategoryMap().get(this.categoryKey).getLibelleFull();
		}else {
			return this.categoryKey;
		}	
	}

	public void setCategoryKey(String categoryKey) {
		this.categoryKey = categoryKey;
	}

	public String getTypePatrimoine() {
		return typePatrimoine;
	}

	public void setTypePatrimoine(String typePatrimoine) {
		this.typePatrimoine = typePatrimoine;
	}

	public String getMaterialKey() {
		return ConvertData.getNatureOuvrageMap().get(this.materialKey);
	}

	public void setMaterialKey(String materialKey) {
		this.materialKey = materialKey;
	}

	public String getMateriauKey() {
		return materiauKey;
	}

	public void setMateriauKey(String materiauKey) {
		this.materiauKey = materiauKey;
	}

	public String getGeometryKey() {
		return ConvertData.getGeometrieOuvrageMap().get(this.geometryKey);
	}

	public void setGeometryKey(String geometryKey) {
		this.geometryKey = geometryKey;
	}

	public String getSituationKey() {
		return ConvertData.getSituationOuvrageMap().get(this.situationKey);
	}

	public void setSituationKey(String situationKey) {
		this.situationKey = situationKey;
	}

	public String getFsaKey() {
		return ConvertData.getFsaMap().get(this.fsaKey);
	}

	public void setFsaKey(String fsaKey) {
		this.fsaKey = fsaKey;
	}

	public String getFsaAuteur() {
		return fsaAuteur;
	}

	public void setFsaAuteur(String fsaAuteur) {
		this.fsaAuteur = fsaAuteur;
	}

	public String getOccupationKey() {
		return occupationKey;
	}

	public void setOccupationKey(String occupationKey) {
		this.occupationKey = occupationKey;
	}

	public String getAssemblyKey() {
		return assemblyKey;
	}

	public void setAssemblyKey(String assemblyKey) {
		this.assemblyKey = assemblyKey;
	}

	public Boolean getLife() {
		return life;
	}

	public void setLife(Boolean life) {
		this.life = life;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPk() {
		return pk;
	}

	public void setPk(Double pk) {
		this.pk = pk;
	}

	public Double getMiddlePk() {
		return middlePk;
	}

	public void setMiddlePk(Double middlePk) {
		this.middlePk = middlePk;
	}

	public Double getEndPk() {
		return endPk;
	}

	public void setEndPk(Double endPk) {
		this.endPk = endPk;
	}

	public Integer getBuildingSpanNumber() {
		return buildingSpanNumber;
	}

	public void setBuildingSpanNumber(Integer buildingSpanNumber) {
		this.buildingSpanNumber = buildingSpanNumber;
	}

	public Boolean getBuildingTrack() {
		return buildingTrack;
	}

	public void setBuildingTrack(Boolean buildingTrack) {
		this.buildingTrack = buildingTrack;
	}

	public Double getOtHeight() {
		return otHeight;
	}

	public void setOtHeight(Double otHeight) {
		this.otHeight = otHeight;
	}

	public String getOtFosse() {
		return otFosse;
	}

	public void setOtFosse(String otFosse) {
		this.otFosse = otFosse;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getYearModif() {
		return yearModif;
	}

	public void setYearModif(Integer yearModif) {
		this.yearModif = yearModif;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAboveBelow() {
		return aboveBelow;
	}

	public void setAboveBelow(String aboveBelow) {
		this.aboveBelow = aboveBelow;
	}

	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	public Boolean getOwnerLife() {
		return ownerLife;
	}

	public void setOwnerLife(Boolean ownerLife) {
		this.ownerLife = ownerLife;
	}

	public String getBncGestion() {
		return bncGestion;
	}

	public void setBncGestion(String bncGestion) {
		this.bncGestion = bncGestion;
	}

	public String getBncMainteneur() {
		return bncMainteneur;
	}

	public void setBncMainteneur(String bncMainteneur) {
		this.bncMainteneur = bncMainteneur;
	}

	public Proprietaire getFirstOwnerName() {
		return firstOwnerName;
	}

	public void setFirstOwnerName(Proprietaire firstOwnerName) {
		this.firstOwnerName = firstOwnerName;
	}

	public Proprietaire getSecondOwnerName() {
		return secondOwnerName;
	}

	public void setSecondOwnerName(Proprietaire secondOwnerName) {
		this.secondOwnerName = secondOwnerName;
	}

	public Proprietaire getThirdOwnerName() {
		return thirdOwnerName;
	}

	public void setThirdOwnerName(Proprietaire thirdOwnerName) {
		this.thirdOwnerName = thirdOwnerName;
	}

	public String getEtatDossier() {
		return etatDossier;
	}

	public void setEtatDossier(String etatDossier) {
		this.etatDossier = etatDossier;
	}

	public Double getFirstOwnerPercent() {
		return firstOwnerPercent;
	}

	public void setFirstOwnerPercent(Double firstOwnerPercent) {
		this.firstOwnerPercent = firstOwnerPercent;
	}

	public Double getSecondOwnerPercent() {
		return secondOwnerPercent;
	}

	public void setSecondOwnerPercent(Double secondOwnerPercent) {
		this.secondOwnerPercent = secondOwnerPercent;
	}

	public Double getThirdOwnerPercent() {
		return thirdOwnerPercent;
	}

	public void setThirdOwnerPercent(Double thirdOwnerPercent) {
		this.thirdOwnerPercent = thirdOwnerPercent;
	}

	public Boolean getMonument() {
		return monument;
	}

	public void setMonument(Boolean monument) {
		this.monument = monument;
	}

	public Boolean getOrsec() {
		return orsec;
	}

	public void setOrsec(Boolean orsec) {
		this.orsec = orsec;
	}

	public Boolean getNaturalRisk() {
		return naturalRisk;
	}

	public void setNaturalRisk(Boolean naturalRisk) {
		this.naturalRisk = naturalRisk;
	}

	public Boolean getIndustrialRisk() {
		return industrialRisk;
	}

	public void setIndustrialRisk(Boolean industrialRisk) {
		this.industrialRisk = industrialRisk;
	}

	public Boolean getSeismicRisk() {
		return seismicRisk;
	}

	public void setSeismicRisk(Boolean seismicRisk) {
		this.seismicRisk = seismicRisk;
	}

	public Boolean getRoadAccidentRisk() {
		return roadAccidentRisk;
	}

	public void setRoadAccidentRisk(Boolean roadAccidentRisk) {
		this.roadAccidentRisk = roadAccidentRisk;
	}

	public Boolean getFluvialAccidentRisk() {
		return fluvialAccidentRisk;
	}

	public void setFluvialAccidentRisk(Boolean fluvialAccidentRisk) {
		this.fluvialAccidentRisk = fluvialAccidentRisk;
	}

	public Boolean getRestriction() {
		return restriction;
	}

	public void setRestriction(Boolean restriction) {
		this.restriction = restriction;
	}

	public Boolean getConvention() {
		return convention;
	}

	public void setConvention(Boolean convention) {
		this.convention = convention;
	}

	public String getCodeSegment() {
		return codeSegment;
	}

	public void setCodeSegment(String codeSegment) {
		this.codeSegment = codeSegment;
	}

	public Double getOaCouverture() {
		return oaCouverture;
	}

	public void setOaCouverture(Double oaCouverture) {
		this.oaCouverture = oaCouverture;
	}

	public Double getOaHauteur() {
		return oaHauteur;
	}

	public void setOaHauteur(Double oaHauteur) {
		this.oaHauteur = oaHauteur;
	}

	public Double getOaHauteurMini() {
		return oaHauteurMini;
	}

	public void setOaHauteurMini(Double oaHauteurMini) {
		this.oaHauteurMini = oaHauteurMini;
	}

	public Double getDistanceRail() {
		return distanceRail;
	}

	public void setDistanceRail(Double distanceRail) {
		this.distanceRail = distanceRail;
	}

	public Double getOaHauteurLibre() {
		return oaHauteurLibre;
	}

	public void setOaHauteurLibre(Double oaHauteurLibre) {
		this.oaHauteurLibre = oaHauteurLibre;
	}

	public Double getOaLongueur() {
		return oaLongueur;
	}

	public void setOaLongueur(Double oaLongueur) {
		this.oaLongueur = oaLongueur;
	}

	public Double getOaOuverture() {
		return oaOuverture;
	}

	public void setOaOuverture(Double oaOuverture) {
		this.oaOuverture = oaOuverture;
	}

	public Boolean getOaSurbaisse() {
		return oaSurbaisse;
	}

	public void setOaSurbaisse(Boolean oaSurbaisse) {
		this.oaSurbaisse = oaSurbaisse;
	}

	public Double getOaLargeur() {
		return oaLargeur;
	}

	public void setOaLargeur(Double oaLargeur) {
		this.oaLargeur = oaLargeur;
	}

	public Integer getOaSpanNumber() {
		return oaSpanNumber;
	}

	public void setOaSpanNumber(Integer oaSpanNumber) {
		this.oaSpanNumber = oaSpanNumber;
	}

	public Integer getOaDeckNumber() {
		return oaDeckNumber;
	}

	public void setOaDeckNumber(Integer oaDeckNumber) {
		this.oaDeckNumber = oaDeckNumber;
	}

	public Double getBias() {
		return bias;
	}

	public void setBias(Double bias) {
		this.bias = bias;
	}

	public Integer getBuildingTrussNumber() {
		return buildingTrussNumber;
	}

	public void setBuildingTrussNumber(Integer buildingTrussNumber) {
		this.buildingTrussNumber = buildingTrussNumber;
	}

	public Double getBuildingHeightMax() {
		return buildingHeightMax;
	}

	public void setBuildingHeightMax(Double buildingHeightMax) {
		this.buildingHeightMax = buildingHeightMax;
	}

	public Double getBuildingSpan() {
		return buildingSpan;
	}

	public void setBuildingSpan(Double buildingSpan) {
		this.buildingSpan = buildingSpan;
	}

	public Boolean getOuvrageEnveloppe() {
		return ouvrageEnveloppe;
	}

	public void setOuvrageEnveloppe(Boolean ouvrageEnveloppe) {
		this.ouvrageEnveloppe = ouvrageEnveloppe;
	}

	public Boolean getBuildingPublic() {
		return buildingPublic;
	}

	public void setBuildingPublic(Boolean buildingPublic) {
		this.buildingPublic = buildingPublic;
	}

	public Boolean getEp() {
		return ep;
	}

	public void setEp(Boolean ep) {
		this.ep = ep;
	}

	public String getOtNature() {
		return otNature;
	}

	public void setOtNature(String otNature) {
		this.otNature = otNature;
	}

	public Double getOaInclination() {
		return oaInclination;
	}

	public void setOaInclination(Double oaInclination) {
		this.oaInclination = oaInclination;
	}

	public Double getOaSlope1() {
		return oaSlope1;
	}

	public void setOaSlope1(Double oaSlope1) {
		this.oaSlope1 = oaSlope1;
	}

	public Double getOaSlope2() {
		return oaSlope2;
	}

	public void setOaSlope2(Double oaSlope2) {
		this.oaSlope2 = oaSlope2;
	}

	public Double getOaPortee() {
		return oaPortee;
	}

	public void setOaPortee(Double oaPortee) {
		this.oaPortee = oaPortee;
	}

	public String getOtAvarie() {
		return otAvarie;
	}

	public void setOtAvarie(String otAvarie) {
		this.otAvarie = otAvarie;
	}

	public String getOtDateIncident() {
		return otDateIncident;
	}

	public void setOtDateIncident(String otDateIncident) {
		this.otDateIncident = otDateIncident;
	}

	public Double getOtHeightLeft() {
		return otHeightLeft;
	}

	public void setOtHeightLeft(Double otHeightLeft) {
		this.otHeightLeft = otHeightLeft;
	}

	public Double getOtHeightRight() {
		return otHeightRight;
	}

	public void setOtHeightRight(Double otHeightRight) {
		this.otHeightRight = otHeightRight;
	}

	public Double getOtDepth() {
		return otDepth;
	}

	public void setOtDepth(Double otDepth) {
		this.otDepth = otDepth;
	}

	public Double getOtDepthLeft() {
		return otDepthLeft;
	}

	public void setOtDepthLeft(Double otDepthLeft) {
		this.otDepthLeft = otDepthLeft;
	}

	public Double getOtDepthRight() {
		return otDepthRight;
	}

	public void setOtDepthRight(Double otDepthRight) {
		this.otDepthRight = otDepthRight;
	}

	public Boolean getOtFicheSignal() {
		return otFicheSignal;
	}

	public void setOtFicheSignal(Boolean otFicheSignal) {
		this.otFicheSignal = otFicheSignal;
	}

	public Double getOtSlope() {
		return otSlope;
	}

	public void setOtSlope(Double otSlope) {
		this.otSlope = otSlope;
	}

	public Double getOtSlopeLeft() {
		return otSlopeLeft;
	}

	public void setOtSlopeLeft(Double otSlopeLeft) {
		this.otSlopeLeft = otSlopeLeft;
	}

	public Double getOtSlopeRight() {
		return otSlopeRight;
	}

	public void setOtSlopeRight(Double otSlopeRight) {
		this.otSlopeRight = otSlopeRight;
	}

	public String getOtTypeConfortement() {
		return otTypeConfortement;
	}

	public void setOtTypeConfortement(String otTypeConfortement) {
		this.otTypeConfortement = otTypeConfortement;
	}

	public String getOtConfortementDesc() {
		return otConfortementDesc;
	}

	public void setOtConfortementDesc(String otConfortementDesc) {
		this.otConfortementDesc = otConfortementDesc;
	}

	public Boolean getProtectedSite() {
		return protectedSite;
	}

	public void setProtectedSite(Boolean protectedSite) {
		this.protectedSite = protectedSite;
	}

	public String getRattachement() {
		return rattachement;
	}

	public void setRattachement(String rattachement) {
		this.rattachement = rattachement;
	}

	public String getUic() {
		return ConvertData.getUicMap().get(uic);
	}

	public void setUic(String uic) {
		this.uic = uic;
	}

	public String getOtMixteType() {
		return otMixteType;
	}

	public void setOtMixteType(String otMixteType) {
		this.otMixteType = otMixteType;
	}

	public String getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}

	public Ouvrage getParentWork() {
		return parentWork;
	}

	public void setParentWork(Ouvrage parentWork) {
		this.parentWork = parentWork;
	}

	public Ligne getLine() {
		return line;
	}

	public void setLine(Ligne line) {
		this.line = line;
	}

	public TypeOuvrage getTypeKey() {
		return typeKey;
	}

	public void setTypeKey(TypeOuvrage typeKey) {
		this.typeKey = typeKey;
	}

	public String getVisiteVirtuelle() {
		return visiteVirtuelle;
	}

	public void setVisiteVirtuelle(String visiteVirtuelle) {
		this.visiteVirtuelle = visiteVirtuelle;
	}

	public String getRisqueObj() {
		return risqueObj;
	}

	public void setRisqueObj(String risqueObj) {
		this.risqueObj = risqueObj;
	}

	public String getRisqueObj2() {
		return risqueObj2;
	}

	public void setRisqueObj2(String risqueObj2) {
		this.risqueObj2 = risqueObj2;
	}

	public String getRisqueObj3() {
		return risqueObj3;
	}

	public void setRisqueObj3(String risqueObj3) {
		this.risqueObj3 = risqueObj3;
	}

	public String getRisqueNat() {
		return risqueNat;
	}

	public void setRisqueNat(String risqueNat) {
		this.risqueNat = risqueNat;
	}

	public String getRisqueNat2() {
		return risqueNat2;
	}

	public void setRisqueNat2(String risqueNat2) {
		this.risqueNat2 = risqueNat2;
	}

	public String getRisqueNat3() {
		return risqueNat3;
	}

	public void setRisqueNat3(String risqueNat3) {
		this.risqueNat3 = risqueNat3;
	}

	public String getRisqueMec() {
		return risqueMec;
	}

	public void setRisqueMec(String risqueMec) {
		this.risqueMec = risqueMec;
	}

	public String getRisqueMec2() {
		return risqueMec2;
	}

	public void setRisqueMec2(String risqueMec2) {
		this.risqueMec2 = risqueMec2;
	}

	public String getRisqueMec3() {
		return risqueMec3;
	}

	public void setRisqueMec3(String risqueMec3) {
		this.risqueMec3 = risqueMec3;
	}

	public String getOtClassement() {
		return otClassement;
	}

	public void setOtClassement(String otClassement) {
		this.otClassement = otClassement;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getAuteurSuppression() {
		return auteurSuppression;
	}

	public void setAuteurSuppression(String auteurSuppression) {
		this.auteurSuppression = auteurSuppression;
	}

	public LocalDate getDateSuppression() {
		return dateSuppression;
	}

	public void setDateSuppression(LocalDate dateSuppression) {
		this.dateSuppression = dateSuppression;
	}

	public UniteOperationnelle getUp() {
		return up;
	}

	public void setUp(UniteOperationnelle up) {
		this.up = up;
	}

	public Boolean getPresenceCampagne() {
		return presenceCampagne;
	}

	public void setPresenceCampagne(Boolean presenceCampagne) {
		this.presenceCampagne = presenceCampagne;
	}

	public String getCategoryKey() {
		return categoryKey;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Departement getFirstDepartement() {
		return firstDepartement;
	}

	public void setFirstDepartement(Departement firstDepartement) {
		this.firstDepartement = firstDepartement;
	}

	public Departement getSecondDepartement() {
		return secondDepartement;
	}

	public void setSecondDepartement(Departement secondDepartement) {
		this.secondDepartement = secondDepartement;
	}

	public Commune getFirstCity() {
		return firstCity;
	}

	public void setFirstCity(Commune firstCity) {
		this.firstCity = firstCity;
	}

	public Commune getSecondCity() {
		return secondCity;
	}

	public void setSecondCity(Commune secondCity) {
		this.secondCity = secondCity;
	}

	public Boolean getSupprime() {
		return supprime;
	}

	public void setSupprime(Boolean supprime) {
		this.supprime = supprime;
	}

	public Boolean getArchive() {
		return archive;
	}

	public void setArchive(Boolean archive) {
		this.archive = archive;
	}
}
