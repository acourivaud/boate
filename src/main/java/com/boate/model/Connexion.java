package com.boate.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.boate.utils.LocalDateConverter;

@Entity
@Table(name = "connexion")
public class Connexion {

	@Id
	@SequenceGenerator(name = "connexion_id_seq", sequenceName = "connexion_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "connexion_id_seq")
	private Integer id;

	@Column(name = "login")
	private String login;

	@Column(name = "date", nullable = false)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate date;

	@Column(name = "heure")
	private String heure;

	public Connexion() { 

	}

	public Connexion(Integer id, String login, LocalDate date, String heure) {
		super();
		this.id = id;
		this.login = login;
		this.date = date;
		this.heure = heure;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getHeure() {
		return heure;
	}

	public void setHeure(String heure) {
		this.heure = heure;
	}

}
