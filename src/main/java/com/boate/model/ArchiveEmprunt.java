package com.boate.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "archivages_emprunt")
public class ArchiveEmprunt {

	@Id
	@SequenceGenerator(name = "archivages_emprunt_id_seq", sequenceName = "archivages_emprunt_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "archivages_emprunt_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "dossier_id", nullable = false)
	private ArchiveDossier dossier;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "user_id", nullable = false)
	private Utilisateur utilisateur;
	
	@Column(name = "date_emprunt", nullable = true)
	private LocalDateTime dateEmprunt;
	
	@Column(name = "date_restitution", nullable = true)
	private LocalDateTime dateRestitution;

	public ArchiveEmprunt() {

	}

	public ArchiveEmprunt(Integer id, ArchiveDossier dossier, Utilisateur utilisateur, LocalDateTime dateEmprunt,
			LocalDateTime dateRestitution) {
		super();
		this.id = id;
		this.dossier = dossier;
		this.utilisateur = utilisateur;
		this.dateEmprunt = dateEmprunt;
		this.dateRestitution = dateRestitution;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ArchiveDossier getDossier() {
		return dossier;
	}

	public void setDossier(ArchiveDossier dossier) {
		this.dossier = dossier;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public LocalDateTime getDateEmprunt() {
		return dateEmprunt;
	}

	public void setDateEmprunt(LocalDateTime dateEmprunt) {
		this.dateEmprunt = dateEmprunt;
	}

	public LocalDateTime getDateRestitution() {
		return dateRestitution;
	}

	public void setDateRestitution(LocalDateTime dateRestitution) {
		this.dateRestitution = dateRestitution;
	}
	
}
