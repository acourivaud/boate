package com.boate.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "planning_collaborateur")
public class PlanningCollaborateur {

	@Id
	@SequenceGenerator(name = "planning_collaborateur_id_seq", sequenceName = "planning_collaborateur_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "planning_collaborateur_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "utilisateur_id", nullable = false)
	private Utilisateur utilisateur;
	
	@Column(name = "nuit", nullable = false)
	private Boolean nuit;
	
	@Column(name = "lieu", nullable = false)
	private String lieu;
	
	@Column(name = "type", nullable = false)
	private String type;

	@Column(name = "date_debut", nullable = false)
	private LocalDateTime dateDebut;
	
	@Column(name = "date_fin", nullable = false)
	private LocalDateTime dateFin;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Boolean getNuit() {
		return nuit;
	}

	public void setNuit(Boolean nuit) {
		this.nuit = nuit;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public LocalDateTime getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDateTime dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDateTime getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDateTime dateFin) {
		this.dateFin = dateFin;
	}
	
}
