package com.boate.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.boate.enums.ParamModuleTypeEnum;
import com.boate.enums.ParamStatutEnum;

@Entity
@Table(name = "param_module")
public class ParamModule {

	@Id
	@SequenceGenerator(name = "param_module_id_seq", sequenceName = "param_module_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "param_module_id_seq")
	private Integer id;

	@Column(name = "libelle_fonctionnel")
	private String libelleFonctionnel;
	
	@Column(name = "libelle_technique")
	private String libelleTechnique;
	
	@Column(name = "classname")
	private String className;

	@Enumerated(EnumType.STRING)
	@Column(name = "statut", nullable = false)
	private ParamStatutEnum statut;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "type", nullable = false)
	private ParamModuleTypeEnum type;
	
	@OneToMany(mappedBy = "paramModule", fetch = FetchType.LAZY)
	private List<ParamValue> paramValues;
	
	@OneToMany(mappedBy = "paramModule", fetch = FetchType.LAZY)
	private List<ParamTableColumnDefinition> paramTableColumnDefinitions;

	public ParamModule() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ParamStatutEnum getStatut() {
		return statut;
	}

	public void setStatut(ParamStatutEnum statut) {
		this.statut = statut;
	}

	public List<ParamTableColumnDefinition> getParamTableColumnDefinitions() {
		return paramTableColumnDefinitions;
	}

	public void setParamTableColumnDefinitions(List<ParamTableColumnDefinition> paramTableColumnDefinitions) {
		this.paramTableColumnDefinitions = paramTableColumnDefinitions;
	}

	public String getLibelleFonctionnel() {
		return libelleFonctionnel;
	}

	public void setLibelleFonctionnel(String libelleFonctionnel) {
		this.libelleFonctionnel = libelleFonctionnel;
	}

	public String getLibelleTechnique() {
		return libelleTechnique;
	}

	public void setLibelleTechnique(String libelleTechnique) {
		this.libelleTechnique = libelleTechnique;
	}

	public ParamModuleTypeEnum getType() {
		return type;
	}

	public void setType(ParamModuleTypeEnum type) {
		this.type = type;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public List<ParamValue> getParamValues() {
		return paramValues;
	}

	public void setParamValues(List<ParamValue> paramValues) {
		this.paramValues = paramValues;
	}

}
