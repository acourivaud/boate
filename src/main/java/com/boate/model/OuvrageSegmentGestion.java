package com.boate.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "work_segment_gestion")
public class OuvrageSegmentGestion {

	@Id
	@SequenceGenerator(name = "work_segment_gestion_id_seq", sequenceName = "work_segment_gestion_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "work_segment_gestion_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "even_id", nullable = false)
	private Infrapole infrapole;

	@Column(name = "code_segment")
	private String codeSegment;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "code_ligne", nullable = false)
	private Ligne ligne;

	@Column(name = "pk_debut")
	private String pkDebut;

	@Column(name = "pk_fin")
	private String pkFin;

	/** default constructor */
	public OuvrageSegmentGestion() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Infrapole getInfrapole() {
		return infrapole;
	}

	public void setInfrapole(Infrapole infrapole) {
		this.infrapole = infrapole;
	}

	public String getCodeSegment() {
		return codeSegment;
	}

	public void setCodeSegment(String codeSegment) {
		this.codeSegment = codeSegment;
	}

	public Ligne getLigne() {
		return ligne;
	}

	public void setLigne(Ligne ligne) {
		this.ligne = ligne;
	}

	public String getPkDebut() {
		return pkDebut;
	}

	public void setPkDebut(String pkDebut) {
		this.pkDebut = pkDebut;
	}

	public String getPkFin() {
		return pkFin;
	}

	public void setPkFin(String pkFin) {
		this.pkFin = pkFin;
	}

}
