package com.boate.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "work_confortement_detection")
public class OuvrageConfortementDetection {

	@Id
	@SequenceGenerator(name = "work_confortement_detection_id_seq", sequenceName = "work_confortement_detection_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "work_confortement_detection_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "work_id", nullable = false)
	private Ouvrage ouvrage;

	@Column(name = "ot_ancrages")
	private Boolean ancrages;

	@Column(name = "ot_beton_projete")
	private Boolean betonProjete;

	@Column(name = "ot_contrefort_beton")
	private Boolean contrefortBeton;

	@Column(name = "ot_chambre_d_eboulis")
	private Boolean chambreDeboulis;

	@Column(name = "ot_clouage")
	private Boolean clouage;

	@Column(name = "ot_ecran_filet_detecteur")
	private Boolean ecranFiletDetecteur;

	@Column(name = "ot_ecran_filet_protecteur")
	private Boolean ecranFiletProtecteur;

	@Column(name = "ot_enrochements")
	private Boolean enrochements;

	@Column(name = "ot_ecran_rail_traverse")
	private Boolean ecranRailTraverse;

	@Column(name = "ot_gabion")
	private Boolean gabion;

	@Column(name = "ot_grillage_pendu")
	private Boolean grillagePendu;

	@Column(name = "ot_grillage_plaque_ancre")
	private Boolean grillagePlaqueAncre;

	@Column(name = "ot_instrumentation")
	private Boolean instrumentation;

	@Column(name = "ot_injection")
	private Boolean injection;

	@Column(name = "ot_masque_drainant")
	private Boolean masqueDrainant;

	@Column(name = "ot_masque_poids")
	private Boolean masquePoids;

	@Column(name = "ot_mur_peller")
	private Boolean murPeller;

	@Column(name = "ot_pieux")
	private Boolean pieux;

	@Column(name = "ot_merlon_pneusol")
	private Boolean merlonPneusol;

	@Column(name = "ot_tranchee_drainante")
	private Boolean trancheeDrainante;

	@Column(name = "ot_maconnerie")
	private Boolean maconnerie;

	/** default constructor */
	public OuvrageConfortementDetection() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Ouvrage getOuvrage() {
		return ouvrage;
	}

	public void setOuvrage(Ouvrage ouvrage) {
		this.ouvrage = ouvrage;
	}

	public Boolean getAncrages() {
		return ancrages;
	}

	public void setAncrages(Boolean ancrages) {
		this.ancrages = ancrages;
	}

	public Boolean getBetonProjete() {
		return betonProjete;
	}

	public void setBetonProjete(Boolean betonProjete) {
		this.betonProjete = betonProjete;
	}

	public Boolean getContrefortBeton() {
		return contrefortBeton;
	}

	public void setContrefortBeton(Boolean contrefortBeton) {
		this.contrefortBeton = contrefortBeton;
	}

	public Boolean getChambreDeboulis() {
		return chambreDeboulis;
	}

	public void setChambreDeboulis(Boolean chambreDeboulis) {
		this.chambreDeboulis = chambreDeboulis;
	}

	public Boolean getClouage() {
		return clouage;
	}

	public void setClouage(Boolean clouage) {
		this.clouage = clouage;
	}

	public Boolean getEcranFiletDetecteur() {
		return ecranFiletDetecteur;
	}

	public void setEcranFiletDetecteur(Boolean ecranFiletDetecteur) {
		this.ecranFiletDetecteur = ecranFiletDetecteur;
	}

	public Boolean getEcranFiletProtecteur() {
		return ecranFiletProtecteur;
	}

	public void setEcranFiletProtecteur(Boolean ecranFiletProtecteur) {
		this.ecranFiletProtecteur = ecranFiletProtecteur;
	}

	public Boolean getEnrochements() {
		return enrochements;
	}

	public void setEnrochements(Boolean enrochements) {
		this.enrochements = enrochements;
	}

	public Boolean getEcranRailTraverse() {
		return ecranRailTraverse;
	}

	public void setEcranRailTraverse(Boolean ecranRailTraverse) {
		this.ecranRailTraverse = ecranRailTraverse;
	}

	public Boolean getGabion() {
		return gabion;
	}

	public void setGabion(Boolean gabion) {
		this.gabion = gabion;
	}

	public Boolean getGrillagePendu() {
		return grillagePendu;
	}

	public void setGrillagePendu(Boolean grillagePendu) {
		this.grillagePendu = grillagePendu;
	}

	public Boolean getGrillagePlaqueAncre() {
		return grillagePlaqueAncre;
	}

	public void setGrillagePlaqueAncre(Boolean grillagePlaqueAncre) {
		this.grillagePlaqueAncre = grillagePlaqueAncre;
	}

	public Boolean getInstrumentation() {
		return instrumentation;
	}

	public void setInstrumentation(Boolean instrumentation) {
		this.instrumentation = instrumentation;
	}

	public Boolean getInjection() {
		return injection;
	}

	public void setInjection(Boolean injection) {
		this.injection = injection;
	}

	public Boolean getMasqueDrainant() {
		return masqueDrainant;
	}

	public void setMasqueDrainant(Boolean masqueDrainant) {
		this.masqueDrainant = masqueDrainant;
	}

	public Boolean getMasquePoids() {
		return masquePoids;
	}

	public void setMasquePoids(Boolean masquePoids) {
		this.masquePoids = masquePoids;
	}

	public Boolean getMurPeller() {
		return murPeller;
	}

	public void setMurPeller(Boolean murPeller) {
		this.murPeller = murPeller;
	}

	public Boolean getPieux() {
		return pieux;
	}

	public void setPieux(Boolean pieux) {
		this.pieux = pieux;
	}

	public Boolean getMerlonPneusol() {
		return merlonPneusol;
	}

	public void setMerlonPneusol(Boolean merlonPneusol) {
		this.merlonPneusol = merlonPneusol;
	}

	public Boolean getTrancheeDrainante() {
		return trancheeDrainante;
	}

	public void setTrancheeDrainante(Boolean trancheeDrainante) {
		this.trancheeDrainante = trancheeDrainante;
	}

	public Boolean getMaconnerie() {
		return maconnerie;
	}

	public void setMaconnerie(Boolean maconnerie) {
		this.maconnerie = maconnerie;
	}

}
