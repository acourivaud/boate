package com.boate.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.boate.enums.ParamStatutEnum;

@Entity
@Table(name = "param_onglet")
public class ParamOnglet {

	@Id
	@SequenceGenerator(name = "param_onglet_id_seq", sequenceName = "param_onglet_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "param_onglet_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "param_template_id", nullable = false)
	private ParamTemplate paramTemplate;

	@Column(name = "position")
	private Integer position;

	@Column(name = "libelle")
	private String libelle;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "statut", nullable = false)
	private ParamStatutEnum statut;
	
	@OneToMany(mappedBy = "paramOnglet", fetch = FetchType.LAZY)
	private List<ParamChapitre> paramChapitres;

	public ParamOnglet() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public ParamStatutEnum getStatut() {
		return statut;
	}

	public void setStatut(ParamStatutEnum statut) {
		this.statut = statut;
	}

	public ParamTemplate getParamTemplate() {
		return paramTemplate;
	}

	public void setParamTemplate(ParamTemplate paramTemplate) {
		this.paramTemplate = paramTemplate;
	}

	public List<ParamChapitre> getParamChapitres() {
		return paramChapitres;
	}

	public void setParamChapitres(List<ParamChapitre> paramChapitres) {
		this.paramChapitres = paramChapitres;
	}

}
