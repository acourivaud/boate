package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "up")
public class UniteOperationnelle {

	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "up_id_seq", sequenceName = "up_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "up_id_seq")
	private Integer id;

	@Column(name = "code")
	private String code;

	@Column(name = "name")
	private String name;

	@Column(name = "life")
	private Boolean life;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "even_id", nullable = true)
	private Infrapole infrapole;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "secteur_id", nullable = true)
	private Secteur secteur;

	public UniteOperationnelle() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Infrapole getInfrapole() {
		return infrapole;
	}

	public void setInfrapole(Infrapole infrapole) {
		this.infrapole = infrapole;
	}

	public Boolean getLife() {
		return life;
	}

	public void setLife(Boolean life) {
		this.life = life;
	}

	public Secteur getSecteur() {
		return secteur;
	}

	public void setSecteur(Secteur secteur) {
		this.secteur = secteur;
	}
	
	

}
