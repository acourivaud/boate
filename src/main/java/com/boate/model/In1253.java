package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "in1253")
public class In1253 {

	@Id
	@SequenceGenerator(name = "in1253_id_seq", sequenceName = "in1253_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "in1253_id_seq")
	private Integer id;

	@Column(name = "target")
	private String target;

	@Column(name = "id_period")
	private Integer periodiciteId;

	@Column(name = "id_author1")
	private String auteurId;

	@Column(name = "id_author2")
	private String auteurId2;

	@Column(name = "vi_period")
	private Integer periodiciteVi;

	@Column(name = "vi_author1")
	private String auteurVi;

	@Column(name = "vi_author2")
	private String auteurVi2;

	@Column(name = "vd_period")
	private Integer periodiciteVd;

	@Column(name = "vd_author1")
	private String auteurVd;

	@Column(name = "vd_author2")
	private String auteurVd2;

	@Column(name = "idi")
	private Boolean idi;

	@Column(name = "cv")
	private Boolean cv;

	@Column(name = "sp")
	private Boolean sp;

	@Column(name = "sr")
	private Boolean sr;

	public In1253() {

	}

	public In1253(Integer id, String target, Integer periodiciteId, String auteurId, String auteurId2,
			Integer periodiciteVi, String auteurVi, String auteurVi2, Integer periodiciteVd, String auteurVd,
			String auteurVd2, Boolean idi, Boolean cv, Boolean sp, Boolean sr) {
		super();
		this.id = id;
		this.target = target;
		this.periodiciteId = periodiciteId;
		this.auteurId = auteurId;
		this.auteurId2 = auteurId2;
		this.periodiciteVi = periodiciteVi;
		this.auteurVi = auteurVi;
		this.auteurVi2 = auteurVi2;
		this.periodiciteVd = periodiciteVd;
		this.auteurVd = auteurVd;
		this.auteurVd2 = auteurVd2;
		this.idi = idi;
		this.cv = cv;
		this.sp = sp;
		this.sr = sr;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getPeriodiciteId() {
		return periodiciteId;
	}

	public void setPeriodiciteId(Integer periodiciteId) {
		this.periodiciteId = periodiciteId;
	}

	public String getAuteurId() {
		return auteurId;
	}

	public void setAuteurId(String auteurId) {
		this.auteurId = auteurId;
	}

	public String getAuteurId2() {
		return auteurId2;
	}

	public void setAuteurId2(String auteurId2) {
		this.auteurId2 = auteurId2;
	}

	public Integer getPeriodiciteVi() {
		return periodiciteVi;
	}

	public void setPeriodiciteVi(Integer periodiciteVi) {
		this.periodiciteVi = periodiciteVi;
	}

	public String getAuteurVi() {
		return auteurVi;
	}

	public void setAuteurVi(String auteurVi) {
		this.auteurVi = auteurVi;
	}

	public String getAuteurVi2() {
		return auteurVi2;
	}

	public void setAuteurVi2(String auteurVi2) {
		this.auteurVi2 = auteurVi2;
	}

	public Integer getPeriodiciteVd() {
		return periodiciteVd;
	}

	public void setPeriodiciteVd(Integer periodiciteVd) {
		this.periodiciteVd = periodiciteVd;
	}

	public String getAuteurVd() {
		return auteurVd;
	}

	public void setAuteurVd(String auteurVd) {
		this.auteurVd = auteurVd;
	}

	public String getAuteurVd2() {
		return auteurVd2;
	}

	public void setAuteurVd2(String auteurVd2) {
		this.auteurVd2 = auteurVd2;
	}

	public Boolean getIdi() {
		return idi;
	}

	public void setIdi(Boolean idi) {
		this.idi = idi;
	}

	public Boolean getCv() {
		return cv;
	}

	public void setCv(Boolean cv) {
		this.cv = cv;
	}

	public Boolean getSp() {
		return sp;
	}

	public void setSp(Boolean sp) {
		this.sp = sp;
	}

	public Boolean getSr() {
		return sr;
	}

	public void setSr(Boolean sr) {
		this.sr = sr;
	}

}
