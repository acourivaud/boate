package com.boate.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.boate.dto.ParamValueDTO;

@Entity
@Table(name = "user_filter")
public class UtilisateurFilter {

	@Id
	@SequenceGenerator(name = "user_filter_id_seq", sequenceName = "user_filter_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_filter_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "user_id", nullable = false)
	private Utilisateur utilisateur;
	
	@Column(name = "page")
	private String page;
	
	@Column(name = "param")
	private String param;
	
	@Column(name = "value")
	private String value;

	public UtilisateurFilter() {
		
	}

	public UtilisateurFilter(Utilisateur utilisateur, String page, String param, String value) {
		this.utilisateur = utilisateur;
		this.page = page;
		this.param = param;
		this.value = value;
	}
	
	public UtilisateurFilter(Utilisateur utilisateur, String page, ParamValueDTO filter) {
		this.utilisateur = utilisateur;
		this.page = page;
		this.param = filter.getParam();
		this.value = filter.getValue();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}
	
	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public static UtilisateurFilter toUtilisateurFilter(Utilisateur utilisateur, String page, ParamValueDTO paramValue) {
		if (paramValue == null) {
			return null;
		} else {
			return new UtilisateurFilter(utilisateur, page, paramValue);
		}
	}

	public static List<UtilisateurFilter> toUtilisateurFilters(Utilisateur utilisateur, String page, List<ParamValueDTO> paramsValues) {
		if (paramsValues == null) {
			return new ArrayList<>(0);
		}
		List<UtilisateurFilter> utilisateurFilters = new ArrayList<>();
		for (ParamValueDTO paramValue : paramsValues) {
			if (!paramValue.getParam().equals("page") && !paramValue.getParam().equals("first") && !paramValue.getParam().equals("rows")) {
				utilisateurFilters.add(UtilisateurFilter.toUtilisateurFilter(utilisateur, page, paramValue));
			}
		}
		return utilisateurFilters;
	}
}
