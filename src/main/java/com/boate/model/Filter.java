package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "filter")
public class Filter {

	@Id
	@SequenceGenerator(name = "filter_id_seq", sequenceName = "filter_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "filter_id_seq")
	private Integer id;

	@Column(name = "guid")
	private String guid;
	
	@Column(name = "param")
	private String param;
	
	@Column(name = "value")
	private String value;

	public Filter() {
		
	}

	public Filter(String guid, String param, String value) {
		super();
		this.guid = guid;
		this.param = param;
		this.value = value;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
