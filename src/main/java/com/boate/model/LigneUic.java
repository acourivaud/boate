package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.boate.utils.ConvertData;

@Entity
@Table(name = "line_uic")
public class LigneUic {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "line_id", nullable = false)
	private Ligne ligne;

	@Column(name = "pk_from")
	private String pkDebut;

	@Column(name = "pk_to")
	private String pkFin;

	@Column(name = "label")
	private String libelleLigne;
	
	@Column(name = "uic")
	private String uic;

	public LigneUic() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Ligne getLigne() {
		return ligne;
	}

	public void setLigne(Ligne ligne) {
		this.ligne = ligne;
	}

	public String getPkDebut() {
		return pkDebut;
	}

	public void setPkDebut(String pkDebut) {
		this.pkDebut = pkDebut;
	}

	public String getPkFin() {
		return pkFin;
	}

	public void setPkFin(String pkFin) {
		this.pkFin = pkFin;
	}

	public String getLibelleLigne() {
		return libelleLigne;
	}

	public void setLibelleLigne(String libelleLigne) {
		this.libelleLigne = libelleLigne;
	}

	public String getUic() {
		String rstUic = ConvertData.getUicMap().get(uic);
		if (rstUic != null) {
			return rstUic;
		}else {
			return "INCONNU";
		}
	}

	public void setUic(String uic) {
		this.uic = uic;
	}

}
