package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "vehicules")
public class Vehicule {

	@Id
	@SequenceGenerator(name = "vehicule_id_seq", sequenceName = "vehicule_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vehicule_id_seq")
	private Integer id;

	@Column(name = "marque", nullable = false)
	private String marque;
	
	@Column(name = "modele", nullable = false)
	private String modele;
	
	@Column(name = "immat", nullable = false)
	private String immatriculation;
	
	@Column(name = "nb_places", nullable = false)
	private Integer places;

	public Vehicule() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public String getImmatriculation() {
		return immatriculation;
	}

	public void setImmatriculation(String immatriculation) {
		this.immatriculation = immatriculation;
	}

	public Integer getPlaces() {
		return places;
	}

	public void setPlaces(Integer places) {
		this.places = places;
	}
	
	
}
