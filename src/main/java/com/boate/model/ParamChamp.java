package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.boate.enums.ParamChampTypeEnum;

@Entity
@Table(name = "param_champ")
public class ParamChamp {

	@Id
	@SequenceGenerator(name = "param_champ_id_seq", sequenceName = "param_champ_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "param_champ_id_seq")
	private Integer id;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "type", nullable = false)
	private ParamChampTypeEnum type;
	
	@Column(name = "model_attribut")
	private String modelAttribut;
	
	@Column(name = "requete")
	private String requete;
	
	@Column(name = "libelle")
	private String libelle;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "param_champ_parent_id", nullable = true)
	private ParamChamp paramChampEnfant;

	public ParamChamp() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ParamChampTypeEnum getType() {
		return type;
	}

	public void setType(ParamChampTypeEnum type) {
		this.type = type;
	}

	public String getModelAttribut() {
		return modelAttribut;
	}

	public void setModelAttribut(String modelAttribut) {
		this.modelAttribut = modelAttribut;
	}

	public String getRequete() {
		return requete;
	}

	public void setRequete(String requete) {
		this.requete = requete;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public ParamChamp getParamChampEnfant() {
		return paramChampEnfant;
	}

	public void setParamChampEnfant(ParamChamp paramChampEnfant) {
		this.paramChampEnfant = paramChampEnfant;
	}

}
