package com.boate.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.boate.utils.LocalDateConverter;

@Entity
@Table(name = "work_surveillance_complementaire")
public class SurveillanceComplementaire {

	@Id
	@SequenceGenerator(name = "work_surveillance_complementaire_id_seq", sequenceName = "work_surveillance_complementaire_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "work_surveillance_complementaire_id_seq")
	private Integer id;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "surveillance_id", nullable = false)
	private Surveillance surveillance;

	@Column(name = "periodicity")
	private String periodicite;
	
	@Column(name = "last_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDate;
	
	@Column(name = "next_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate nextDate;
	
	@Column(name = "start_date", nullable = false)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate startDate;
	
	@Column(name = "end_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate endDate;
	
	@Column(name = "modif_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate modifDate;
	
	@Column(name = "create_date", nullable = false)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate createDate;
	
	@Column(name = "validation_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate validationDate;
	
	@Column(name = "auteur")
	private String auteur;
	
	@Column(name = "ordre")
	private Integer ordre;
	
	@Column(name = "nombre_tube")
	private Integer nombreTube;
	
	@Column(name = "methodologie")
	private String methodologie;
	
	@Column(name = "objet")
	private String objet;
	
	@Column(name = "nature")
	private String nature;
	
	@Column(name = "type")
	private String type;

	public SurveillanceComplementaire() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Surveillance getSurveillance() {
		return surveillance;
	}

	public void setSurveillance(Surveillance surveillance) {
		this.surveillance = surveillance;
	}

	public String getPeriodicite() {
		return periodicite;
	}

	public void setPeriodicite(String periodicite) {
		this.periodicite = periodicite;
	}

	public LocalDate getLastDate() {
		return lastDate;
	}

	public void setLastDate(LocalDate lastDate) {
		this.lastDate = lastDate;
	}

	public LocalDate getNextDate() {
		return nextDate;
	}

	public void setNextDate(LocalDate nextDate) {
		this.nextDate = nextDate;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public LocalDate getModifDate() {
		return modifDate;
	}

	public void setModifDate(LocalDate modifDate) {
		this.modifDate = modifDate;
	}

	public LocalDate getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDate createDate) {
		this.createDate = createDate;
	}

	public LocalDate getValidationDate() {
		return validationDate;
	}

	public void setValidationDate(LocalDate validationDate) {
		this.validationDate = validationDate;
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public Integer getOrdre() {
		return ordre;
	}

	public void setOrdre(Integer ordre) {
		this.ordre = ordre;
	}

	public Integer getNombreTube() {
		return nombreTube;
	}

	public void setNombreTube(Integer nombreTube) {
		this.nombreTube = nombreTube;
	}

	public String getMethodologie() {
		return methodologie;
	}

	public void setMethodologie(String methodologie) {
		this.methodologie = methodologie;
	}

	public String getObjet() {
		return objet;
	}

	public void setObjet(String objet) {
		this.objet = objet;
	}

	public String getNature() {
		return nature;
	}

	public void setNature(String nature) {
		this.nature = nature;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
