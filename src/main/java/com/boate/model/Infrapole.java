package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "infrapole")
public class Infrapole {

	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "infrapole_id_seq", sequenceName = "infrapole_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "infrapole_id_seq")
	private Integer id;

	@Column(name = "code")
	private String code;

	@Column(name = "life")
	private Boolean life;

	@Column(name = "name")
	private String nom;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "region_id", nullable = false)
	private Region region;
	
	@Column(name = "has_secteur", nullable = false)
	private Boolean usesSecteurSubLevel;

	public Infrapole() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getLife() {
		return life;
	}

	public void setLife(Boolean life) {
		this.life = life;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Boolean getUsesSecteurSubLevel() {
		return usesSecteurSubLevel;
	}

	public void setUsesSecteurSubLevel(Boolean usesSecteurSubLevel) {
		this.usesSecteurSubLevel = usesSecteurSubLevel;
	}

}
