package com.boate.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.boate.enums.ParamStatutEnum;

@Entity
@Table(name = "param_ligne")
public class ParamLigne {

	@Id
	@SequenceGenerator(name = "param_ligne_id_seq", sequenceName = "param_ligne_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "param_ligne_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "param_chapitre_id", nullable = false)
	private ParamChapitre paramChapitre;

	@Column(name = "position")
	private Integer position;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "statut", nullable = false)
	private ParamStatutEnum statut;
	
	@OneToMany(mappedBy = "paramLigne", fetch = FetchType.LAZY)
	private List<ParamLigneDefinition> paramLigneDefinitions;

	public ParamLigne() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ParamChapitre getParamChapitre() {
		return paramChapitre;
	}

	public void setParamChapitre(ParamChapitre paramChapitre) {
		this.paramChapitre = paramChapitre;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public ParamStatutEnum getStatut() {
		return statut;
	}

	public void setStatut(ParamStatutEnum statut) {
		this.statut = statut;
	}

	public List<ParamLigneDefinition> getParamLigneDefinitions() {
		return paramLigneDefinitions;
	}

	public void setParamLigneDefinitions(List<ParamLigneDefinition> paramLigneDefinitions) {
		this.paramLigneDefinitions = paramLigneDefinitions;
	}

}
