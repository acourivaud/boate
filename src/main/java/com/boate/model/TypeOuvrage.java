package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.boate.utils.ConvertData;

@Entity
@Table(name = "work_type")
public class TypeOuvrage {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "nom_boate")
	private String nomBoate;

	@Column(name = "nom_patrimoine")
	private String nomPatrimoine;

	public TypeOuvrage(Integer id, String nomBoate, String nomPatrimoine) {
		super();
		this.id = id;
		this.nomBoate = nomBoate;
		this.nomPatrimoine = nomPatrimoine;
	}
	
	public TypeOuvrage() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomBoate() {
		return ConvertData.getTypeOuvrageMap().get(this.nomBoate);
	}

	public void setNomBoate(String nomBoate) {
		this.nomBoate = nomBoate;
	}

	public String getNomPatrimoine() {
		return ConvertData.getTypeOuvrageMap().get(this.nomPatrimoine);
	}

	public void setNomPatrimoine(String nomPatrimoine) {
		this.nomPatrimoine = nomPatrimoine;
	}

}
