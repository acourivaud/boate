package com.boate.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "modification_fiche")
public class ModificationFiche {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "id_work_modifie", nullable = true)
	private Ouvrage ouvrageModifie;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "id_worksurveillance_modifie", nullable = true)
	private Surveillance surveillanceModifiee;

	// @ManyToOne(optional = true, cascade = {})
	// @JoinColumn(name = "id_work_maintenance_modifie", nullable = true)
	// private Travaux travaux;

	// @ManyToOne(optional = true, cascade = {})
	// @JoinColumn(name = "id_work_surveillance_complementary", nullable = true)
	// private SurveillanceComplementaire surveillanceComplementaireModifiee;

	@Column(name = "attribut_modifie")
	private String attribut;

	@Column(name = "ancienne_valeur")
	private String ancienneValeur;

	@Column(name = "nouvelle_valeur")
	private String nouvelleValeur;

	@Column(name = "date_mise_a_jour")
	private LocalDateTime dateMaj;

	public ModificationFiche() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Ouvrage getOuvrageModifie() {
		return ouvrageModifie;
	}

	public void setOuvrageModifie(Ouvrage ouvrageModifie) {
		this.ouvrageModifie = ouvrageModifie;
	}

	public Surveillance getSurveillanceModifiee() {
		return surveillanceModifiee;
	}

	public void setSurveillanceModifiee(Surveillance surveillanceModifiee) {
		this.surveillanceModifiee = surveillanceModifiee;
	}

	public String getAttribut() {
		return attribut;
	}

	public void setAttribut(String attribut) {
		this.attribut = attribut;
	}

	public String getAncienneValeur() {
		return ancienneValeur;
	}

	public void setAncienneValeur(String ancienneValeur) {
		this.ancienneValeur = ancienneValeur;
	}

	public String getNouvelleValeur() {
		return nouvelleValeur;
	}

	public void setNouvelleValeur(String nouvelleValeur) {
		this.nouvelleValeur = nouvelleValeur;
	}

	public LocalDateTime getDateMaj() {
		return dateMaj;
	}

	public void setDateMaj(LocalDateTime dateMaj) {
		this.dateMaj = dateMaj;
	}

}
