package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "indicator")
public class Indicateur {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "year")
	private Integer annee;

	@Column(name = "visit_type")
	private String typeVisite;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "user_id", nullable = true)
	private Utilisateur utilisateur;

	@Column(name = "realized_visits_number")
	private Integer nombreRealise;

	@Column(name = "expected_visits_number")
	private Integer nombrePrevisionnel;

	@Column(name = "trimester1")
	private Double trimestre1;

	@Column(name = "trimester2")
	private Double trimestre2;

	@Column(name = "trimester3")
	private Double trimestre3;

	@Column(name = "trimester4")
	private Double trimestre4;

	@Column(name = "global_indicator")
	private String globalIndicator;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "up_id", nullable = true)
	private UniteOperationnelle up;

	public Indicateur() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAnnee() {
		return annee;
	}

	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

	public String getTypeVisite() {
		return typeVisite;
	}

	public void setTypeVisite(String typeVisite) {
		this.typeVisite = typeVisite;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Integer getNombreRealise() {
		return nombreRealise;
	}

	public void setNombreRealise(Integer nombreRealise) {
		this.nombreRealise = nombreRealise;
	}

	public Integer getNombrePrevisionnel() {
		return nombrePrevisionnel;
	}

	public void setNombrePrevisionnel(Integer nombrePrevisionnel) {
		this.nombrePrevisionnel = nombrePrevisionnel;
	}

	public Double getTrimestre1() {
		return trimestre1;
	}

	public void setTrimestre1(Double trimestre1) {
		this.trimestre1 = trimestre1;
	}

	public Double getTrimestre2() {
		return trimestre2;
	}

	public void setTrimestre2(Double trimestre2) {
		this.trimestre2 = trimestre2;
	}

	public Double getTrimestre3() {
		return trimestre3;
	}

	public void setTrimestre3(Double trimestre3) {
		this.trimestre3 = trimestre3;
	}

	public Double getTrimestre4() {
		return trimestre4;
	}

	public void setTrimestre4(Double trimestre4) {
		this.trimestre4 = trimestre4;
	}

	public String getGlobalIndicator() {
		return globalIndicator;
	}

	public void setGlobalIndicator(String globalIndicator) {
		this.globalIndicator = globalIndicator;
	}

	public UniteOperationnelle getUp() {
		return up;
	}

	public void setUp(UniteOperationnelle up) {
		this.up = up;
	}

}
