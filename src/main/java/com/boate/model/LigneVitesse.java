package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.boate.utils.ConvertData;

@Entity
@Table(name = "speed")
public class LigneVitesse {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "line_id", nullable = false)
	private Ligne ligne;

	@Column(name = "pk_begin")
	private String pkDebut;

	@Column(name = "pk_end")
	private String pkFin;

	@Column(name = "speed")
	private String vitesse;
	
	@Column(name = "track")
	private String voie;

	public LigneVitesse() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Ligne getLigne() {
		return ligne;
	}

	public void setLigne(Ligne ligne) {
		this.ligne = ligne;
	}

	public String getPkDebut() {
		return pkDebut;
	}

	public void setPkDebut(String pkDebut) {
		this.pkDebut = pkDebut;
	}

	public String getPkFin() {
		return pkFin;
	}

	public void setPkFin(String pkFin) {
		this.pkFin = pkFin;
	}

	public String getVitesse() {
		return ConvertData.getLigneVitesseMap().get(this.vitesse);
	}

	public void setVitesse(String vitesse) {
		this.vitesse = vitesse;
	}

	public String getVoie() {
		return voie;
	}

	public void setVoie(String voie) {
		this.voie = voie;
	}

}
