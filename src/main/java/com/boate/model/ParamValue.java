package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "param_value")
public class ParamValue {

	@Id
	@SequenceGenerator(name = "param_value_id_seq", sequenceName = "param_value_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "param_value_id_seq")
	private Integer id;

	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "param_table_column_definition_id", nullable = true)
	private ParamTableColumnDefinition paramTableColumnDefinition;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "param_module_id", nullable = true)
	private ParamModule paramModule;
	
	@Column(name = "param")
	private String param;
	
	@Column(name = "value")
	private String value;

	public ParamValue() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ParamTableColumnDefinition getParamTableColumnDefinition() {
		return paramTableColumnDefinition;
	}

	public void setParamTableColumnDefinition(ParamTableColumnDefinition paramTableColumnDefinition) {
		this.paramTableColumnDefinition = paramTableColumnDefinition;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ParamModule getParamModule() {
		return paramModule;
	}

	public void setParamModule(ParamModule paramModule) {
		this.paramModule = paramModule;
	}

}
