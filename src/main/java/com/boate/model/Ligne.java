package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "line")
public class Ligne {

	@Id
	@SequenceGenerator(name = "line_id_seq", sequenceName = "line_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "line_id_seq")
	private Integer id;

	@Column(name = "code")
	private String code;

	@Column(name = "name")
	private String name;

	@Column(name = "begin_pk")
	private String beginPk;

	@Column(name = "end_pk")
	private String endPk;

	@Column(name = "region_key")
	private String regionKey;

	@Column(name = "speed")
	private Integer speed;

	@Column(name = "life")
	private Boolean life;

	/*
	 * TODO: voir si c'est nécessaire de les réactiver
	 * private Set ups;
	 * 
	 * private Set works;
	 * 
	 * private Set lineUics;
	 * 
	 * private Set speeds;
	 * 
	 * private Set upLines;
	 */

	public Ligne(Integer id, String code, String name, String beginPk, String endPk, String regionKey, Integer speed,
			Boolean life) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.beginPk = beginPk;
		this.endPk = endPk;
		this.regionKey = regionKey;
		this.speed = speed;
		this.life = life;
	}
	
	public Ligne() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBeginPk() {
		return beginPk;
	}

	public void setBeginPk(String beginPk) {
		this.beginPk = beginPk;
	}

	public String getEndPk() {
		return endPk;
	}

	public void setEndPk(String endPk) {
		this.endPk = endPk;
	}

	public String getRegionKey() {
		return regionKey;
	}

	public void setRegionKey(String regionKey) {
		this.regionKey = regionKey;
	}

	public Integer getSpeed() {
		return speed;
	}

	public void setSpeed(Integer speed) {
		this.speed = speed;
	}

	public Boolean getLife() {
		return life;
	}

	public void setLife(Boolean life) {
		this.life = life;
	}

}
