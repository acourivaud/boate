package com.boate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(region = "archiveBoiteCache", usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "archivages_boite")
public class ArchiveBoite {

	@Id
	@SequenceGenerator(name = "archivages_boite_id_seq", sequenceName = "archivages_boite_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "archivages_boite_id_seq")
	private Integer id;

	@Column(name = "nom")
	private String nom;

	@Column(name = "localisation")
	private String localisation;

	public ArchiveBoite() {

	}

	public ArchiveBoite(Integer id, String nom, String localisation) {
		super();
		this.id = id;
		this.nom = nom;
		this.localisation = localisation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getLocalisation() {
		return localisation;
	}

	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}

}
