package com.boate.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "param_table_column_definition")
public class ParamTableColumnDefinition {

	@Id
	@SequenceGenerator(name = "param_table_column_definition_id_seq", sequenceName = "param_table_column_definition_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "param_table_column_definition_id_seq")
	private Integer id;

	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "param_champ_id", nullable = false)
	private ParamChamp paramChamp;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "param_module_id", nullable = false)
	private ParamModule paramModule;

	@Column(name = "libelle_header")
	private String libelleHeader;
	
	@OneToMany(mappedBy = "paramTableColumnDefinition", fetch = FetchType.LAZY)
	private List<ParamValue> paramValues;

	public ParamTableColumnDefinition() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ParamChamp getParamChamp() {
		return paramChamp;
	}

	public void setParamChamp(ParamChamp paramChamp) {
		this.paramChamp = paramChamp;
	}

	public String getLibelleHeader() {
		return libelleHeader;
	}

	public void setLibelleHeader(String libelleHeader) {
		this.libelleHeader = libelleHeader;
	}

	public ParamModule getParamModule() {
		return paramModule;
	}

	public void setParamModule(ParamModule paramModule) {
		this.paramModule = paramModule;
	}

	public List<ParamValue> getParamValues() {
		return paramValues;
	}

	public void setParamValues(List<ParamValue> paramValues) {
		this.paramValues = paramValues;
	}

}
