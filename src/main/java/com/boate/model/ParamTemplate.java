package com.boate.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.boate.enums.TemplateStatutEnum;

@Entity
@Table(name = "param_template")
public class ParamTemplate {

	@Id
	@SequenceGenerator(name = "param_template_id_seq", sequenceName = "param_template_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "param_template_id_seq")
	private Integer id;

	@Column(name = "libelle")
	private String libelle;

	@Enumerated(EnumType.STRING)
	@Column(name = "statut", nullable = false)
	private TemplateStatutEnum statut;
	
	@OneToMany(mappedBy = "paramTemplate", fetch = FetchType.LAZY)
	private List<ParamOnglet> paramOnglets;

	public ParamTemplate() {
		this.statut = TemplateStatutEnum.BROUILLON;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public TemplateStatutEnum getStatut() {
		return statut;
	}

	public void setStatut(TemplateStatutEnum statut) {
		this.statut = statut;
	}

	public List<ParamOnglet> getParamOnglets() {
		return paramOnglets;
	}

	public void setParamOnglets(List<ParamOnglet> paramOnglets) {
		this.paramOnglets = paramOnglets;
	}
	
	

}
