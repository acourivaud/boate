package com.boate.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.boate.utils.ConvertData;
import com.boate.utils.LocalDateConverter;

@Entity
@Table(name = "work_surveillance")
public class Surveillance {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "work_id", nullable = false)
	private Ouvrage ouvrage;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "in1253_id", nullable = true)
	private In1253 in1253;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "visit_author", nullable = false)
	private Utilisateur referentRegional;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "aoap_referent", nullable = false)
	private Utilisateur referentAoap;

	@Column(name = "before_2007_key")
	private String before2007Key;
	
	@Column(name = "means1_key")
	private String firstMeansKey;
	
	@Column(name = "means2_key")
	private String secondMeansKey;
	
	@Column(name = "means3_key")
	private String thirdMeansKey;
	
	@Column(name = "aquatic_surveillance")
	private Boolean surveillanceAquatique;
	
	@Column(name = "diver")
	private Boolean diver;
	
	@Column(name = "convention")
	private Boolean convention;
	
	@Column(name = "begin_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate dateDebut;
	
	@Column(name = "end_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate dateFin;
	
	@Column(name = "monitored")
	private Boolean monitored;
	
	@Column(name = "leveled")
	private Boolean leveled;
	
	@Column(name = "ind_periodicity")
	private Integer periodiciteId;
	
	@Column(name = "last_ind_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDateId;
	
	@Column(name = "next_ind_date")
	private Integer nextDateId;
	
	@Column(name = "idi_period")
	private Integer periodiciteIdi;
	
	@Column(name = "last_indi_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDateIdi;
	
	@Column(name = "next_indi_date")
	private Integer nextDateIdi;
	
	@Column(name = "cv")
	private Boolean cv;
	
	@Column(name = "vd_periodicity")
	private Integer periodiciteVd;
	
	@Column(name = "last_vd_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDateVd;
	
	@Column(name = "next_vd_date")
	private Integer nextDateVd;
	
	@Column(name = "vi_periodicity")
	private Integer periodiciteVi;
	
	@Column(name = "last_vi_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDateVi;
	
	@Column(name = "next_vi_date")
	private Integer nextDateVi;
	
	@Column(name = "last_camera_visit_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDateCameraVisit;
	
	@Column(name = "convention_info")
	private String conventionInfo;
	
	@Column(name = "road_signage")
	private Boolean roadSignage;
	
	@Column(name = "initial_date")
	private Integer anneeReference;
	
	@Column(name = "visit_type")
	private String typeSurveillance;
	
	@Column(name = "complementary_visit")
	private String complementaryVisit;
	
	@Column(name = "id_head")
	private String auteurId;
	
	@Column(name = "idi_head")
	private String auteurIdi;
	
	@Column(name = "vd_head")
	private String auteurVd;
	
	@Column(name = "vi_head")
	private String auteurVi;
	
	@Column(name = "vs_period")
	private Integer periodiciteVs;
	
	@Column(name = "last_vs_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDateVs;
	
	@Column(name = "next_vs_date")
	private Integer nextDateVs;
	
	@Column(name = "vs_head")
	private String auteurVs;
	
	@Column(name = "idi")
	private Boolean idi;
	
	@Column(name = "last_vsp_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDateVsp;
	
	@Column(name = "ot_vd_periodicity")
	private Integer periodiciteVdOt;
	
	@Column(name = "ot_last_vd_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDateVdOt;
	
	@Column(name = "ot_next_vd_date")
	private Integer nextDateVdOt;
	
	@Column(name = "ot_vd_head")
	private String auteurVdOt;
	
	@Column(name = "ot_mesures_conservatoires")
	private String mesuresConservatoireOt;
	
	
	@Column(name = "ot_vi_periodicity")
	private Integer periodiciteViOt;
	
	@Column(name = "ot_last_vi_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDateViOt;
	
	@Column(name = "ot_next_vi_date")
	private Integer nextDateViOt;
	
	@Column(name = "ot_vi_head")
	private String auteurViOt;
	
	@Column(name = "ot_vp_periodicity")
	private Integer periodiciteVpOt;
	
	@Column(name = "ot_last_vp_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDateVpOt;
	
	@Column(name = "ot_next_vp_date")
	private Integer nextDateVpOt;
	
	@Column(name = "ot_vp_head")
	private String auteurVpOt;
	
	@Column(name = "ot_last_ve_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDateVeOt;
	
	@Column(name = "ot_tournee_intemperie")
	private Boolean tourneeIntemperieOt;
	
	@ManyToOne(optional = true, cascade = {})
	@JoinColumn(name = "auteur_maj", nullable = true)
	private Utilisateur auteurMaj;
	
	//TODO: Convertir en date
	@Column(name = "date_maj")
	private String dateMaj;
	
	@Column(name = "last_pv0_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDatePv0;

	@Column(name = "code_analytique")
	private String codeAnalytique;
	
	@Column(name = "code_segment")
	private String codeSegment;
	
	@Column(name = "ot_va_periodicity")
	private Integer periodiciteVaOt;
	
	@Column(name = "ot_last_va_date", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate lastDateVaOt;
	
	@Column(name = "ot_next_va_date")
	private Integer nextDateVaOt;
	
	@Column(name = "ot_va_head")
	private String auteurVaOt;
	
	@Column(name = "complementary_id")
	private Integer complementaryId;
	
	@Column(name = "date_reclassement", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate dateReclassement;
	
	@Column(name = "motif_reclassement")
	private String motifReclassement;
	
	@Column(name = "ot_va_bool")
	private Boolean boolOtVa;
	
	@Column(name = "mesure_topo")
	private Boolean mesureTopo;
	
	@Column(name = "mesure_inclino")
	private Boolean mesureInclino;
	
	@Column(name = "statut")
	private String statut;

	@Column(name = "auteur_suppression")
	private String auteurSuppression;

	@Column(name = "date_suppression", nullable = true)
	@Convert(converter = LocalDateConverter.class)
	private LocalDate dateSuppression;

	/** default constructor */
	public Surveillance() {
		this.statut = "A";
	}

	public Surveillance(Integer id, Ouvrage ouvrage, In1253 in1253, Utilisateur referentRegional,
			Utilisateur referentAoap, String before2007Key, String firstMeansKey, String secondMeansKey,
			String thirdMeansKey, Boolean surveillanceAquatique, Boolean diver, Boolean convention, LocalDate dateDebut,
			LocalDate dateFin, Boolean monitored, Boolean leveled, Integer periodiciteId, LocalDate lastDateId,
			Integer nextDateId, Integer periodiciteIdi, LocalDate lastDateIdi, Integer nextDateIdi, Boolean cv,
			Integer periodiciteVd, LocalDate lastDateVd, Integer nextDateVd, Integer periodiciteVi,
			LocalDate lastDateVi, Integer nextDateVi, LocalDate lastDateCameraVisit, String conventionInfo,
			Boolean roadSignage, Integer anneeReference, String typeSurveillance, String complementaryVisit,
			String auteurId, String auteurIdi, String auteurVd, String auteurVi, Integer periodiciteVs,
			LocalDate lastDateVs, Integer nextDateVs, String auteurVs, Boolean idi, LocalDate lastDateVsp,
			Integer periodiciteVdOt, LocalDate lastDateVdOt, Integer nextDateVdOt, String auteurVdOt,
			String mesuresConservatoireOt, Integer periodiciteViOt, LocalDate lastDateViOt, Integer nextDateViOt,
			String auteurViOt, Integer periodiciteVpOt, LocalDate lastDateVpOt, Integer nextDateVpOt, String auteurVpOt,
			LocalDate lastDateVeOt, Boolean tourneeIntemperieOt, Utilisateur auteurMaj, String dateMaj,
			LocalDate lastDatePv0, String codeAnalytique, String codeSegment, Integer periodiciteVaOt,
			LocalDate lastDateVaOt, Integer nextDateVaOt, String auteurVaOt, Integer complementaryId,
			LocalDate dateReclassement, String motifReclassement, Boolean boolOtVa, Boolean mesureTopo,
			Boolean mesureInclino, String statut, String auteurSuppression, LocalDate dateSuppression) {
		super();
		this.id = id;
		this.ouvrage = ouvrage;
		this.in1253 = in1253;
		this.referentRegional = referentRegional;
		this.referentAoap = referentAoap;
		this.before2007Key = before2007Key;
		this.firstMeansKey = firstMeansKey;
		this.secondMeansKey = secondMeansKey;
		this.thirdMeansKey = thirdMeansKey;
		this.surveillanceAquatique = surveillanceAquatique;
		this.diver = diver;
		this.convention = convention;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.monitored = monitored;
		this.leveled = leveled;
		this.periodiciteId = periodiciteId;
		this.lastDateId = lastDateId;
		this.nextDateId = nextDateId;
		this.periodiciteIdi = periodiciteIdi;
		this.lastDateIdi = lastDateIdi;
		this.nextDateIdi = nextDateIdi;
		this.cv = cv;
		this.periodiciteVd = periodiciteVd;
		this.lastDateVd = lastDateVd;
		this.nextDateVd = nextDateVd;
		this.periodiciteVi = periodiciteVi;
		this.lastDateVi = lastDateVi;
		this.nextDateVi = nextDateVi;
		this.lastDateCameraVisit = lastDateCameraVisit;
		this.conventionInfo = conventionInfo;
		this.roadSignage = roadSignage;
		this.anneeReference = anneeReference;
		this.typeSurveillance = typeSurveillance;
		this.complementaryVisit = complementaryVisit;
		this.auteurId = auteurId;
		this.auteurIdi = auteurIdi;
		this.auteurVd = auteurVd;
		this.auteurVi = auteurVi;
		this.periodiciteVs = periodiciteVs;
		this.lastDateVs = lastDateVs;
		this.nextDateVs = nextDateVs;
		this.auteurVs = auteurVs;
		this.idi = idi;
		this.lastDateVsp = lastDateVsp;
		this.periodiciteVdOt = periodiciteVdOt;
		this.lastDateVdOt = lastDateVdOt;
		this.nextDateVdOt = nextDateVdOt;
		this.auteurVdOt = auteurVdOt;
		this.mesuresConservatoireOt = mesuresConservatoireOt;
		this.periodiciteViOt = periodiciteViOt;
		this.lastDateViOt = lastDateViOt;
		this.nextDateViOt = nextDateViOt;
		this.auteurViOt = auteurViOt;
		this.periodiciteVpOt = periodiciteVpOt;
		this.lastDateVpOt = lastDateVpOt;
		this.nextDateVpOt = nextDateVpOt;
		this.auteurVpOt = auteurVpOt;
		this.lastDateVeOt = lastDateVeOt;
		this.tourneeIntemperieOt = tourneeIntemperieOt;
		this.auteurMaj = auteurMaj;
		this.dateMaj = dateMaj;
		this.lastDatePv0 = lastDatePv0;
		this.codeAnalytique = codeAnalytique;
		this.codeSegment = codeSegment;
		this.periodiciteVaOt = periodiciteVaOt;
		this.lastDateVaOt = lastDateVaOt;
		this.nextDateVaOt = nextDateVaOt;
		this.auteurVaOt = auteurVaOt;
		this.complementaryId = complementaryId;
		this.dateReclassement = dateReclassement;
		this.motifReclassement = motifReclassement;
		this.boolOtVa = boolOtVa;
		this.mesureTopo = mesureTopo;
		this.mesureInclino = mesureInclino;
		this.statut = statut;
		this.auteurSuppression = auteurSuppression;
		this.dateSuppression = dateSuppression;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Ouvrage getOuvrage() {
		return ouvrage;
	}

	public void setOuvrage(Ouvrage ouvrage) {
		this.ouvrage = ouvrage;
	}

	public In1253 getIn1253() {
		return in1253;
	}

	public void setIn1253(In1253 in1253) {
		this.in1253 = in1253;
	}

	public Utilisateur getReferentRegional() {
		return referentRegional;
	}

	public void setReferentRegional(Utilisateur referentRegional) {
		this.referentRegional = referentRegional;
	}

	public Utilisateur getReferentAoap() {
		return referentAoap;
	}

	public void setReferentAoap(Utilisateur referentAoap) {
		this.referentAoap = referentAoap;
	}

	public String getBefore2007Key() {
		return before2007Key;
	}

	public void setBefore2007Key(String before2007Key) {
		this.before2007Key = before2007Key;
	}

	public String getFirstMeansKey() {
		return ConvertData.getMoyensSurveillanceMap().get(firstMeansKey);
	}

	public void setFirstMeansKey(String firstMeansKey) {
		this.firstMeansKey = firstMeansKey;
	}

	public String getSecondMeansKey() {
		return ConvertData.getMoyensSurveillanceMap().get(secondMeansKey);
	}

	public void setSecondMeansKey(String secondMeansKey) {
		this.secondMeansKey = secondMeansKey;
	}

	public String getThirdMeansKey() {
		return ConvertData.getMoyensSurveillanceMap().get(thirdMeansKey);
	}

	public void setThirdMeansKey(String thirdMeansKey) {
		this.thirdMeansKey = thirdMeansKey;
	}

	public Boolean getSurveillanceAquatique() {
		return surveillanceAquatique;
	}

	public void setSurveillanceAquatique(Boolean surveillanceAquatique) {
		this.surveillanceAquatique = surveillanceAquatique;
	}

	public Boolean getDiver() {
		return diver;
	}

	public void setDiver(Boolean diver) {
		this.diver = diver;
	}

	public Boolean getConvention() {
		return convention;
	}

	public void setConvention(Boolean convention) {
		this.convention = convention;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public Boolean getMonitored() {
		return monitored;
	}

	public void setMonitored(Boolean monitored) {
		this.monitored = monitored;
	}

	public Boolean getLeveled() {
		return leveled;
	}

	public void setLeveled(Boolean leveled) {
		this.leveled = leveled;
	}

	public Integer getPeriodiciteId() {
		return periodiciteId;
	}

	public void setPeriodiciteId(Integer periodiciteId) {
		this.periodiciteId = periodiciteId;
	}

	public LocalDate getLastDateId() {
		return lastDateId;
	}

	public void setLastDateId(LocalDate lastDateId) {
		this.lastDateId = lastDateId;
	}

	public Integer getNextDateId() {
		return nextDateId;
	}

	public void setNextDateId(Integer nextDateId) {
		this.nextDateId = nextDateId;
	}

	public Integer getPeriodiciteIdi() {
		return periodiciteIdi;
	}

	public void setPeriodiciteIdi(Integer periodiciteIdi) {
		this.periodiciteIdi = periodiciteIdi;
	}

	public LocalDate getLastDateIdi() {
		return lastDateIdi;
	}

	public void setLastDateIdi(LocalDate lastDateIdi) {
		this.lastDateIdi = lastDateIdi;
	}

	public Integer getNextDateIdi() {
		return nextDateIdi;
	}

	public void setNextDateIdi(Integer nextDateIdi) {
		this.nextDateIdi = nextDateIdi;
	}

	public Boolean getCv() {
		return cv;
	}

	public void setCv(Boolean cv) {
		this.cv = cv;
	}

	public Integer getPeriodiciteVd() {
		return periodiciteVd;
	}

	public void setPeriodiciteVd(Integer periodiciteVd) {
		this.periodiciteVd = periodiciteVd;
	}

	public LocalDate getLastDateVd() {
		return lastDateVd;
	}

	public void setLastDateVd(LocalDate lastDateVd) {
		this.lastDateVd = lastDateVd;
	}

	public Integer getNextDateVd() {
		return nextDateVd;
	}

	public void setNextDateVd(Integer nextDateVd) {
		this.nextDateVd = nextDateVd;
	}

	public Integer getPeriodiciteVi() {
		return periodiciteVi;
	}

	public void setPeriodiciteVi(Integer periodiciteVi) {
		this.periodiciteVi = periodiciteVi;
	}

	public LocalDate getLastDateVi() {
		return lastDateVi;
	}

	public void setLastDateVi(LocalDate lastDateVi) {
		this.lastDateVi = lastDateVi;
	}

	public Integer getNextDateVi() {
		return nextDateVi;
	}

	public void setNextDateVi(Integer nextDateVi) {
		this.nextDateVi = nextDateVi;
	}

	public LocalDate getLastDateCameraVisit() {
		return lastDateCameraVisit;
	}

	public void setLastDateCameraVisit(LocalDate lastDateCameraVisit) {
		this.lastDateCameraVisit = lastDateCameraVisit;
	}

	public String getConventionInfo() {
		return conventionInfo;
	}

	public void setConventionInfo(String conventionInfo) {
		this.conventionInfo = conventionInfo;
	}

	public Boolean getRoadSignage() {
		return roadSignage;
	}

	public void setRoadSignage(Boolean roadSignage) {
		this.roadSignage = roadSignage;
	}

	public Integer getAnneeReference() {
		return anneeReference;
	}

	public void setAnneeReference(Integer anneeReference) {
		this.anneeReference = anneeReference;
	}

	public String getTypeSurveillance() {
		return typeSurveillance;
	}

	public void setTypeSurveillance(String typeSurveillance) {
		this.typeSurveillance = typeSurveillance;
	}

	public String getComplementaryVisit() {
		return complementaryVisit;
	}

	public void setComplementaryVisit(String complementaryVisit) {
		this.complementaryVisit = complementaryVisit;
	}

	public String getAuteurId() {
		return ConvertData.getAuteurSurveillanceMap().get(auteurId);
	}

	public void setAuteurId(String auteurId) {
		this.auteurId = auteurId;
	}

	public String getAuteurIdi() {
		return ConvertData.getAuteurSurveillanceMap().get(auteurIdi);
	}

	public void setAuteurIdi(String auteurIdi) {
		this.auteurIdi = auteurIdi;
	}

	public String getAuteurVd() {
		return ConvertData.getAuteurSurveillanceMap().get(auteurVd);
	}

	public void setAuteurVd(String auteurVd) {
		this.auteurVd = auteurVd;
	}

	public String getAuteurVi() {
		return ConvertData.getAuteurSurveillanceMap().get(auteurVi);
	}

	public void setAuteurVi(String auteurVi) {
		this.auteurVi = auteurVi;
	}

	public Integer getPeriodiciteVs() {
		return periodiciteVs;
	}

	public void setPeriodiciteVs(Integer periodiciteVs) {
		this.periodiciteVs = periodiciteVs;
	}

	public LocalDate getLastDateVs() {
		return lastDateVs;
	}

	public void setLastDateVs(LocalDate lastDateVs) {
		this.lastDateVs = lastDateVs;
	}

	public Integer getNextDateVs() {
		return nextDateVs;
	}

	public void setNextDateVs(Integer nextDateVs) {
		this.nextDateVs = nextDateVs;
	}

	public String getAuteurVs() {
		return ConvertData.getAuteurSurveillanceMap().get(auteurVs);
	}

	public void setAuteurVs(String auteurVs) {
		this.auteurVs = auteurVs;
	}

	public Boolean getIdi() {
		return idi;
	}

	public void setIdi(Boolean idi) {
		this.idi = idi;
	}

	public LocalDate getLastDateVsp() {
		return lastDateVsp;
	}

	public void setLastDateVsp(LocalDate lastDateVsp) {
		this.lastDateVsp = lastDateVsp;
	}

	public Integer getPeriodiciteVdOt() {
		return periodiciteVdOt;
	}

	public void setPeriodiciteVdOt(Integer periodiciteVdOt) {
		this.periodiciteVdOt = periodiciteVdOt;
	}

	public LocalDate getLastDateVdOt() {
		return lastDateVdOt;
	}

	public void setLastDateVdOt(LocalDate lastDateVdOt) {
		this.lastDateVdOt = lastDateVdOt;
	}

	public Integer getNextDateVdOt() {
		return nextDateVdOt;
	}

	public void setNextDateVdOt(Integer nextDateVdOt) {
		this.nextDateVdOt = nextDateVdOt;
	}

	public String getAuteurVdOt() {
		return ConvertData.getAuteurSurveillanceMap().get(auteurVdOt);
	}

	public void setAuteurVdOt(String auteurVdOt) {
		this.auteurVdOt = auteurVdOt;
	}

	public String getMesuresConservatoireOt() {
		return mesuresConservatoireOt;
	}

	public void setMesuresConservatoireOt(String mesuresConservatoireOt) {
		this.mesuresConservatoireOt = mesuresConservatoireOt;
	}

	public Integer getPeriodiciteViOt() {
		return periodiciteViOt;
	}

	public void setPeriodiciteViOt(Integer periodiciteViOt) {
		this.periodiciteViOt = periodiciteViOt;
	}

	public LocalDate getLastDateViOt() {
		return lastDateViOt;
	}

	public void setLastDateViOt(LocalDate lastDateViOt) {
		this.lastDateViOt = lastDateViOt;
	}

	public Integer getNextDateViOt() {
		return nextDateViOt;
	}

	public void setNextDateViOt(Integer nextDateViOt) {
		this.nextDateViOt = nextDateViOt;
	}

	public String getAuteurViOt() {
		return ConvertData.getAuteurSurveillanceMap().get(auteurViOt);
	}

	public void setAuteurViOt(String auteurViOt) {
		this.auteurViOt = auteurViOt;
	}

	public Integer getPeriodiciteVpOt() {
		return periodiciteVpOt;
	}

	public void setPeriodiciteVpOt(Integer periodiciteVpOt) {
		this.periodiciteVpOt = periodiciteVpOt;
	}

	public LocalDate getLastDateVpOt() {
		return lastDateVpOt;
	}

	public void setLastDateVpOt(LocalDate lastDateVpOt) {
		this.lastDateVpOt = lastDateVpOt;
	}

	public Integer getNextDateVpOt() {
		return nextDateVpOt;
	}

	public void setNextDateVpOt(Integer nextDateVpOt) {
		this.nextDateVpOt = nextDateVpOt;
	}

	public String getAuteurVpOt() {
		return ConvertData.getAuteurSurveillanceMap().get(auteurVpOt);
	}

	public void setAuteurVpOt(String auteurVpOt) {
		this.auteurVpOt = auteurVpOt;
	}

	public LocalDate getLastDateVeOt() {
		return lastDateVeOt;
	}

	public void setLastDateVeOt(LocalDate lastDateVeOt) {
		this.lastDateVeOt = lastDateVeOt;
	}

	public Boolean getTourneeIntemperieOt() {
		return tourneeIntemperieOt;
	}

	public void setTourneeIntemperieOt(Boolean tourneeIntemperieOt) {
		this.tourneeIntemperieOt = tourneeIntemperieOt;
	}

	public Utilisateur getAuteurMaj() {
		return auteurMaj;
	}

	public void setAuteurMaj(Utilisateur auteurMaj) {
		this.auteurMaj = auteurMaj;
	}

	public String getDateMaj() {
		return dateMaj;
	}

	public void setDateMaj(String dateMaj) {
		this.dateMaj = dateMaj;
	}

	public LocalDate getLastDatePv0() {
		return lastDatePv0;
	}

	public void setLastDatePv0(LocalDate lastDatePv0) {
		this.lastDatePv0 = lastDatePv0;
	}

	public String getCodeAnalytique() {
		return codeAnalytique;
	}

	public void setCodeAnalytique(String codeAnalytique) {
		this.codeAnalytique = codeAnalytique;
	}

	public String getCodeSegment() {
		return codeSegment;
	}

	public void setCodeSegment(String codeSegment) {
		this.codeSegment = codeSegment;
	}

	public Integer getPeriodiciteVaOt() {
		return periodiciteVaOt;
	}

	public void setPeriodiciteVaOt(Integer periodiciteVaOt) {
		this.periodiciteVaOt = periodiciteVaOt;
	}

	public LocalDate getLastDateVaOt() {
		return lastDateVaOt;
	}

	public void setLastDateVaOt(LocalDate lastDateVaOt) {
		this.lastDateVaOt = lastDateVaOt;
	}

	public Integer getNextDateVaOt() {
		return nextDateVaOt;
	}

	public void setNextDateVaOt(Integer nextDateVaOt) {
		this.nextDateVaOt = nextDateVaOt;
	}

	public String getAuteurVaOt() {
		return ConvertData.getAuteurSurveillanceMap().get(auteurVaOt);
	}

	public void setAuteurVaOt(String auteurVaOt) {
		this.auteurVaOt = auteurVaOt;
	}

	public Integer getComplementaryId() {
		return complementaryId;
	}

	public void setComplementaryId(Integer complementaryId) {
		this.complementaryId = complementaryId;
	}

	public LocalDate getDateReclassement() {
		return dateReclassement;
	}

	public void setDateReclassement(LocalDate dateReclassement) {
		this.dateReclassement = dateReclassement;
	}

	public String getMotifReclassement() {
		return motifReclassement;
	}

	public void setMotifReclassement(String motifReclassement) {
		this.motifReclassement = motifReclassement;
	}

	public Boolean getBoolOtVa() {
		return boolOtVa;
	}

	public void setBoolOtVa(Boolean boolOtVa) {
		this.boolOtVa = boolOtVa;
	}

	public Boolean getMesureTopo() {
		return mesureTopo;
	}

	public void setMesureTopo(Boolean mesureTopo) {
		this.mesureTopo = mesureTopo;
	}

	public Boolean getMesureInclino() {
		return mesureInclino;
	}

	public void setMesureInclino(Boolean mesureInclino) {
		this.mesureInclino = mesureInclino;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getAuteurSuppression() {
		return auteurSuppression;
	}

	public void setAuteurSuppression(String auteurSuppression) {
		this.auteurSuppression = auteurSuppression;
	}

	public LocalDate getDateSuppression() {
		return dateSuppression;
	}

	public void setDateSuppression(LocalDate dateSuppression) {
		this.dateSuppression = dateSuppression;
	}
	
	public String getFullMoyensSurveillance() {
		String rst = this.firstMeansKey != null ? this.firstMeansKey : null;
		
		if (this.secondMeansKey != null) {
			rst.concat(" - " + this.secondMeansKey);
		}
		if (this.thirdMeansKey != null) {
			rst.concat(" - " + this.secondMeansKey);
		}
		
		return rst;
	}
	
	public Boolean hasVisitsForYear(int year, String type) {
		if (type.equals("ID")){
			return this.getVisitsForYear(year).equalsIgnoreCase("ID ");
		}else{
			return this.getVisitsForYear(year).contains(type);
		}
	}
	
	public String getVisitsForYear(int year) {
		String result = "";

		if (ConvertData.VISIT_TYPE_SURV_TIERS.equals(this.typeSurveillance)
				|| ConvertData.VISIT_TYPE_SURV_CURRENT.equals(this.typeSurveillance)) {
			return result; // RETURN
		}
		/*
		 * Date date = new Date(); Integer year = date.getYear() + 1900 + i;
		 */

		if (this.anneeReference != null) {
			int surveillanceAge = year - this.anneeReference;

			if (this.periodiciteId != null && this.periodiciteId > 0) {
				if ("ID".equals(this.typeSurveillance)) {
					if (surveillanceAge % this.periodiciteId == 0) {
						result += "ID ";
					}
				}
			}

			if (this.periodiciteVd != null && this.periodiciteVd > 0) {
				if ("VD".equals(this.typeSurveillance)
						&& surveillanceAge % this.periodiciteVd == 0) {
					result += "VD ";
				}

			}

			if (this.periodiciteVdOt != null && this.periodiciteVdOt > 0) {
				if ("VD_OT".equals(this.typeSurveillance)
						&& surveillanceAge % this.periodiciteVdOt == 0) {
					result += "VD_OT ";
				}
			}

			if (this.periodiciteVpOt != null && this.periodiciteVpOt > 0) {
				if ("VP_OT".equals(this.typeSurveillance)
						&& surveillanceAge % this.periodiciteVpOt == 0) {
					result += "VP_OT ";
				}
			}

			if (this.periodiciteVi != null && this.periodiciteVi > 0) {// TODO:
				if ("ID".equals(this.typeSurveillance) || "VD".equals(this.typeSurveillance)) {
					if ((surveillanceAge % this.periodiciteVi == 0)
							&& (result.length() == 0)) {
						result += "VI ";
					}
				}
			}

			if (this.periodiciteViOt != null && this.periodiciteViOt > 0) {// TODO:
				if ("VI_OT".equals(this.typeSurveillance)
						|| "VD_OT".equals(this.typeSurveillance)) {
					if ((surveillanceAge % this.periodiciteViOt == 0)
							&& (result.length() == 0)) {
						result += "VI_OT ";
					}
				}
			}

			// IDI exits only when ID,VD dont exist
			if (this.periodiciteIdi != null && this.periodiciteIdi > 0) {
				if (result.contains("ID") || result.contains("VD")) {
					// nothing
				} else {
					if (surveillanceAge % this.periodiciteIdi == 0) {
						result += "IDI ";
					}
				}
			}
			
			if (this.periodiciteVaOt != null && this.periodiciteVaOt > 0) {
				if ("VD_OT".equals(this.typeSurveillance) && (result.length() == 0)) {
						result += "VA_OT ";
					}
				}

			if (this.periodiciteVs != null && this.periodiciteVs > 0) {
				if ("VS".equals(this.typeSurveillance)
						&& surveillanceAge % this.periodiciteVs == 0) {
					result += "VS ";
				}
			}
		}

		return result;
	}
}
