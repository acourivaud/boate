package com.boate.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "geometry_type")
public class GeometrieType {

	@Id
	@SequenceGenerator(name = "geometry_type_id_seq", sequenceName = "geometry_type_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "geometry_type_id_seq")
	private Integer id;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "geometry_id", nullable = false)
	private Geometrie geometrie;
	
	@ManyToOne(optional = false, cascade = {})
	@JoinColumn(name = "type_id", nullable = false)
	private Type type;

	public GeometrieType() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Geometrie getGeometrie() {
		return geometrie;
	}

	public void setGeometrie(Geometrie geometrie) {
		this.geometrie = geometrie;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

}
