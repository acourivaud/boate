package com.boate.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.boate.dto.ParamValueDTO;
import com.boate.model.Ouvrage;
import com.boate.model.Proprietaire;
import com.boate.model.Secteur;
import com.boate.model.TypeOuvrage;
import com.boate.model.UniteOperationnelle;
import com.boate.utils.DataFormatUtils;
import com.boate.utils.WhereClauseRequestHelper;

public class OuvrageRepositoryCustomImpl implements OuvrageRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Ouvrage> findOuvragesWithFilters(List<ParamValueDTO> requestFilters) {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();

		CriteriaQuery<Ouvrage> query = cb.createQuery(Ouvrage.class);

		Root<Ouvrage> ouvrage = query.from(Ouvrage.class);
		Join<Ouvrage, UniteOperationnelle> uniteOperationnelle = ouvrage.join("up", JoinType.LEFT);
		Join<UniteOperationnelle, Secteur> secteur = uniteOperationnelle.join("secteur", JoinType.LEFT);
		Join<Ouvrage, TypeOuvrage> typeOuvrage = ouvrage.join("typeKey", JoinType.LEFT);
		Join<Ouvrage, Proprietaire> firstProprietaire = ouvrage.join("firstOwnerName", JoinType.LEFT);
		Join<Ouvrage, Proprietaire> secondProprietaire = ouvrage.join("secondOwnerName", JoinType.LEFT);
		Join<Ouvrage, Proprietaire> thirdProprietaire = ouvrage.join("thirdOwnerName", JoinType.LEFT);

		query.select(ouvrage).where(cb.and(createWhereClause(requestFilters, cb, ouvrage, uniteOperationnelle, secteur, typeOuvrage, firstProprietaire, secondProprietaire, thirdProprietaire)));

		Integer first = null;
		Integer rows = null;

		for (ParamValueDTO paramValue : requestFilters) {
			switch (paramValue.getParam()) {

			case "first":
				first = Integer.parseUnsignedInt(paramValue.getValue());
				break;

			case "rows":
				rows = Integer.parseUnsignedInt(paramValue.getValue());
				break;
			}
		}

		query.orderBy(cb.asc(ouvrage.get("line")), cb.asc(ouvrage.get("pk")), cb.asc(ouvrage.get("indice")),
				cb.asc(ouvrage.get("middlePk")), cb.asc(ouvrage.get("endPk")));

		if (first != null && rows != null) {
			return entityManager.createQuery(query).setFirstResult(first).setMaxResults(rows).getResultList();
		} else {
			return entityManager.createQuery(query).getResultList();
		}

	}

	@Override
	public Long countOuvragesWithFilters(List<ParamValueDTO> requestFilters) {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();

		CriteriaQuery<Long> query = cb.createQuery(Long.class);

		Root<Ouvrage> ouvrage = query.from(Ouvrage.class);
		Join<Ouvrage, UniteOperationnelle> uniteOperationnelle = ouvrage.join("up", JoinType.LEFT);
		Join<UniteOperationnelle, Secteur> secteur = uniteOperationnelle.join("secteur", JoinType.LEFT);
		Join<Ouvrage, TypeOuvrage> typeOuvrage = ouvrage.join("typeKey", JoinType.LEFT);
		Join<Ouvrage, Proprietaire> firstProprietaire = ouvrage.join("firstOwnerName", JoinType.LEFT);
		Join<Ouvrage, Proprietaire> secondProprietaire = ouvrage.join("secondOwnerName", JoinType.LEFT);
		Join<Ouvrage, Proprietaire> thirdProprietaire = ouvrage.join("thirdOwnerName", JoinType.LEFT);

		query.select(cb.count(ouvrage));

		query.where(cb.and(createWhereClause(requestFilters, cb, ouvrage, uniteOperationnelle, secteur, typeOuvrage, firstProprietaire, secondProprietaire, thirdProprietaire)));

		return entityManager.createQuery(query).getSingleResult();
	}

	private Predicate[] createWhereClause(List<ParamValueDTO> requestFilters, CriteriaBuilder cb, Root<Ouvrage> ouvrage,
			Join<Ouvrage, UniteOperationnelle> uniteOperationnelle, Join<UniteOperationnelle, Secteur> secteur, Join<Ouvrage, TypeOuvrage> typeOuvrage,  Join<Ouvrage, Proprietaire> firstProprietaire,
			Join<Ouvrage, Proprietaire> secondProprietaire, Join<Ouvrage, Proprietaire> thirdProprietaire) {

		WhereClauseRequestHelper whereClauseRequestHelper = new WhereClauseRequestHelper(cb);

		List<Predicate> predicates = new ArrayList<>();
		
		//PK
		Boolean fromSelected = false;
		String fromValue = null;
		Boolean toSelected = false;
		String toValue = null;
		
		//Propriétaire
		Boolean typeProprietaireSelected = false;
		Boolean nomProprietaireSelected = false;
		String typeProprietaireValue = null;
		String nomProprietaireValue = null;
		List<Path<Number>> listAttributes = new ArrayList<Path<Number>>();

		for (ParamValueDTO paramValue : requestFilters) {
			switch (paramValue.getParam()) {

			case "GenRegionSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("region").get("id"), String.class, paramValue.getValue()));
				break;

			case "GenInfrapoleSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(secteur.get("infrapole").get("id"), String.class, paramValue.getValue()));
				break;

			case "GenSecteurSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(uniteOperationnelle.get("secteur").get("id"), String.class, paramValue.getValue()));
				break;

			case "GenUoSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(uniteOperationnelle.get("id"), String.class, paramValue.getValue()));
				break;

			case "GenUicSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("uic"), String.class, paramValue.getValue()));
				break;
			
			case "GenLigneSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("line").get("id"), String.class, paramValue.getValue()));
				break;
			
			case "GenFromTyped":
				fromSelected = true;
				fromValue = DataFormatUtils.checkPk(paramValue.getValue());
				break;
				
			case "GenToTyped":
				toSelected = true;
				toValue = DataFormatUtils.checkPk(paramValue.getValue());
				break;
				
			case "GenCategorieSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("categoryKey"), String.class, paramValue.getValue()));
				break;
				
			case "GenTypeSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(typeOuvrage.get("nomBoate"), String.class, paramValue.getValue()));
				break;
				
			case "DesOaLongueurFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("oaOuverture"), Double.class, paramValue.getValue()));
				break;
			case "DesOaLongueurToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("oaOuverture"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOaHauteurFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("oaHauteur"), Double.class, paramValue.getValue()));
				break;
			case "DesOaHauteurToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("oaHauteur"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOaHauteurLibreFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("oaHauteurLibre"), Double.class, paramValue.getValue()));
				break;
			case "DesOaHauteurLibreToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("oaHauteurLibre"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOaLargeurFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("oaLargeur"), Double.class, paramValue.getValue()));
				break;
			case "DesOaLargeurToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("oaLargeur"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOaOuvertureFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("oaOuverture"), Double.class, paramValue.getValue()));
				break;
			case "DesOaOuvertureToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("oaOuverture"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOaPorteeFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("oaPortee"), Double.class, paramValue.getValue()));
				break;
			case "DesOaPorteeToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("oaPortee"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOaCouvertureFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("oaCouverture"), Double.class, paramValue.getValue()));
				break;
			case "DesOaCouvertureToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("oaCouverture"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOaTraverseesFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("oaSpanNumber"), Double.class, paramValue.getValue()));
				break;
			case "DesOaTraverseesToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("oaSpanNumber"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOaTabliersFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("oaDeckNumber"), Double.class, paramValue.getValue()));
				break;
			case "DesOaTabliersToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("oaDeckNumber"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOaPenteFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("oaInclination"), Double.class, paramValue.getValue()));
				break;
			case "DesOaPenteToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("oaInclination"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOaPenteGaucheFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("oaSlope1"), Double.class, paramValue.getValue()));
				break;
			case "DesOaPenteGaucheToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("oaSlope1"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOaPenteDroiteFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("oaSlope1"), Double.class, paramValue.getValue()));
				break;
			case "DesOaPenteDroiteToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("oaSlope2"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOaGeometrieSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("geometryKey"), String.class, paramValue.getValue()));
				break;
				
			case "DesOaAssemblageSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("assemblyKey"), String.class, paramValue.getValue()));
				break;
				
			case "DesOtHauteurFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("otHeight"), Double.class, paramValue.getValue()));
				break;
			case "DesOtHauteurToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("otHeight"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOtHauteurGaucheFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("otHeightLeft"), Double.class, paramValue.getValue()));
				break;
			case "DesOtHauteurGaucheToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("otHeightLeft"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOtHauteurDroiteFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("otHeightRight"), Double.class, paramValue.getValue()));
				break;
			case "DesOtHauteurDroiteToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("otHeightRight"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOtProfondeurFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("otDepth"), Double.class, paramValue.getValue()));
				break;
			case "DesOtProfondeurToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("otDepth"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOtProfondeurGaucheFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("otDepthLeft"), Double.class, paramValue.getValue()));
				break;
			case "DesOtProfondeurGaucheToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("otDepthLeft"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOtProfondeurDroiteFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("otDepthRight"), Double.class, paramValue.getValue()));
				break;
			case "DesOtProfondeurDroiteToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("otDepthRight"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOtPenteFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("otSlope"), Double.class, paramValue.getValue()));
				break;
			case "DesOtPenteToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("otSlope"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOtPenteGaucheFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("otSlopeLeft"), Double.class, paramValue.getValue()));
				break;
			case "DesOtPenteGaucheToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("otSlopeLeft"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOtPenteDroiteFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("otSlopeRight"), Double.class, paramValue.getValue()));
				break;
			case "DesOtPenteDroiteToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("otSlopeRight"), Double.class, paramValue.getValue()));
				break;
				
			case "DesOtNatureSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("otNature"), String.class, paramValue.getValue()));
				break;
				
			case "DesBncHauteurMaxiFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("buildingHeightMax"), Double.class, paramValue.getValue()));
				break;
			case "DesBncHauteurMaxiToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("buildingHeightMax"), Double.class, paramValue.getValue()));
				break;
				
			case "DesBncPorteeFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("buildingSpan"), Double.class, paramValue.getValue()));
				break;
			case "DesBncPorteeToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("buildingSpan"), Double.class, paramValue.getValue()));
				break;
				
			case "DesBncFermesFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("buildingTrussNumber"), Double.class, paramValue.getValue()));
				break;
			case "DesBncFermesToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("buildingTrussNumber"), Double.class, paramValue.getValue()));
				break;
				
			case "DesBncTraveesFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("buildingSpanNumber"), Double.class, paramValue.getValue()));
				break;
			case "DesBncTraveesToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("buildingSpanNumber"), Double.class, paramValue.getValue()));
				break;
				
			case "DesBncOccupationSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("occupationKey"), String.class, paramValue.getValue()));
				break;
				
			case "DesBncBatPublicSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("buildingPublic"), Boolean.class, paramValue.getValue()));
				break;
				
			case "DesBncVoieSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("buildingTrack"), Boolean.class, paramValue.getValue()));
				break;
				
			case "DesBncAssemblageSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("assemblyKey"), String.class, paramValue.getValue()));
				break;
				
			case "DesAoGeometrieSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("geometryKey"), String.class, paramValue.getValue()));
				break;
				
			case "DesAoAssemblageSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("assemblyKey"), String.class, paramValue.getValue()));
				break;
				
			case "SitDepartementSelected":
				listAttributes.clear();
				listAttributes.add(ouvrage.get("firstDepartement").get("id"));
				listAttributes.add(ouvrage.get("secondDepartement").get("id"));
				predicates.add(whereClauseRequestHelper.attributeMultipleEquals(listAttributes, Integer.class, paramValue.getValue()));
				break;
				
			case "SitCommuneSelectedSearch":
				
				listAttributes.clear();
				listAttributes.add(ouvrage.get("firstCity").get("id"));
				listAttributes.add(ouvrage.get("secondCity").get("id"));
				predicates.add(whereClauseRequestHelper.attributeMultipleEquals(listAttributes, Integer.class, whereClauseRequestHelper.valueFromJsonString(paramValue.getValue())));
				break;
				
			case "SitOuvrageEnveloppeSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("ouvrageEnveloppe"), Boolean.class, paramValue.getValue()));
				break;
				
			case "SitPositionSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("situationKey"), String.class, paramValue.getValue()));
				break;
				
			case "SitLocalisationSelected":
				//TODO: colonne inexistante
				//predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("localisationOuvrage"), Boolean.class, paramValue.getValue()));
				break;
				
			case "SitPhotoSelected":
				predicates.add(whereClauseRequestHelper.isNotNull(ouvrage.get("photoOuvrage")));
				break;
				
			case "SitNoteEtatSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("niveauEtat"), String.class, paramValue.getValue()));
				break;
				
			case "SitGraviteSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("niveauGravite"), String.class, paramValue.getValue()));
				break;
				
			case "ParNomTyped":
				predicates.add(whereClauseRequestHelper.valueLike(ouvrage.get("name"), String.class, paramValue.getValue()));
				break;
				
			case "ParFsaSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("fsaKey"), String.class, paramValue.getValue()));
				break;
				
			case "ParFamilleSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("rattachement"), String.class, paramValue.getValue()));
				break;

			case "ParPlanOrsecSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("orsec"), Boolean.class, paramValue.getValue()));
				break;
				
			case "ParMonumentHistSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("monument"), Boolean.class, paramValue.getValue()));
				break;
				
			case "ParSiteProtegeSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("protectedSite"), Boolean.class, paramValue.getValue()));
				break;
				
			case "ParEmbranchementSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("ep"), Boolean.class, paramValue.getValue()));
				break;
				
			case "ParRisqueNaturelSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("naturalRisk"), Boolean.class, paramValue.getValue()));
				break;
				
			case "ParRisqueSismiqueSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("seismicRisk"), Boolean.class, paramValue.getValue()));
				break;
				
			case "ParRisqueTechnoSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("industrialRisk"), Boolean.class, paramValue.getValue()));
				break;
				
			case "ParRisqueRoutierSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("roadAccidentRisk"), Boolean.class, paramValue.getValue()));
				break;
				
			case "ParRisqueFluvialSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("fluvialAccidentRisk"), Boolean.class, paramValue.getValue()));
				break;
				
			case "ParAnneeConstructionFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("year"), Integer.class, paramValue.getValue()));
				break;
			case "ParAnneeConstructionToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("year"), Integer.class, paramValue.getValue()));
				break;
				
			case "ParBiaisFromTyped":
				predicates.add(whereClauseRequestHelper.greaterEquals(ouvrage.get("bias"), Double.class, paramValue.getValue()));
				break;
			case "ParBiaisToTyped":
				predicates.add(whereClauseRequestHelper.lowerEquals(ouvrage.get("bias"), Double.class, paramValue.getValue()));
				break;
				
			case "ParTypeProprietaireSelected":
				typeProprietaireSelected = true;
				typeProprietaireValue = paramValue.getValue();
				break;
				
			case "ParProprietaireSelected":
				nomProprietaireSelected = true;
				nomProprietaireValue = paramValue.getValue();
				break;
				
			case "ParDonneesGeotechSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("presenceCampagne"), Boolean.class, paramValue.getValue()));
				break;
				
			case "ParFicheModifieeSelected":
				predicates.add(whereClauseRequestHelper.isNotNull(ouvrage.get("auteurMaj")));
				break;
				
			case "ParRestrictionSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("restriction"), Boolean.class, paramValue.getValue()));
				break;
				
			case "ParConventionSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("convention"), Boolean.class, paramValue.getValue()));
				break;
				
			case "ParStatutSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("statut"), String.class, paramValue.getValue()));
				break;
				
			}
				
				
		}
		
		if (fromSelected || toSelected) {
			predicates.add(whereClauseRequestHelper.pkSearch(ouvrage, fromValue, toValue));
		}
		
		if (typeProprietaireSelected || nomProprietaireSelected) {
			predicates.add(whereClauseRequestHelper.proprietaireSearch(
					firstProprietaire, secondProprietaire, thirdProprietaire, typeProprietaireValue, nomProprietaireValue)
				);
		}
		
		
		
		

		return predicates.toArray(new Predicate[predicates.size()]);
	}

}
