package com.boate.repository;

import java.util.List;

import com.boate.dto.ParamValueDTO;
import com.boate.model.ArchiveDossier;
import com.boate.security.entity.UserPrincipal;

public interface ArchiveRepositoryCustom {

	List<ArchiveDossier> findArchivesWithFilters(List<ParamValueDTO> requestFilters, UserPrincipal currentUser);
	
	Long countArchivesWithFilters(List<ParamValueDTO> requestFilters, UserPrincipal currentUser);

}
