package com.boate.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.boate.dto.ParamValueDTO;
import com.boate.model.IndicateurSecurite;
import com.boate.utils.WhereClauseRequestHelper;

public class IndicateurSecuriteRepositoryCustomImpl implements IndicateurSecuriteRepositoryCustom {

	@PersistenceContext
    private EntityManager entityManager;
 
    @Override
    public List<IndicateurSecurite> findIndicateurSecuritesWithFilters(List<ParamValueDTO> requestFilters) {
    	
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        
        CriteriaQuery<IndicateurSecurite> query = cb.createQuery(IndicateurSecurite.class);
        
        Root<IndicateurSecurite> ouvrage = query.from(IndicateurSecurite.class);
        
        query.select(ouvrage).where(
    		cb.and(
				createWhereClause(requestFilters, cb, ouvrage)
    		)
        );
        
        Integer first = null;
        Integer rows = null;
        
        for (ParamValueDTO paramValue : requestFilters) {
        	switch (paramValue.getParam()) {
        	
	        	case "first":
	        		first = Integer.parseUnsignedInt(paramValue.getValue());
	        		break;
	        		
	        	case "rows":
	        		rows = Integer.parseUnsignedInt(paramValue.getValue());
	        		break;
        	}
        }
        
        query.orderBy(
    		cb.asc(ouvrage.get("line")),
    		cb.asc(ouvrage.get("pk")),
    		cb.asc(ouvrage.get("indice")),
    		cb.asc(ouvrage.get("middlePk")),
    		cb.asc(ouvrage.get("endPk"))
    	);
        
        if (first != null && rows != null) {
        	return entityManager
        			.createQuery(query)
        			.setFirstResult(first)
        			.setMaxResults(rows)
                    .getResultList();
        } else {
        	return entityManager
        			.createQuery(query)
        			.getResultList();
        }
        
    }
    
    @Override
    public Long countIndicateurSecuritesWithFilters(List<ParamValueDTO> requestFilters) {
    	
    	CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    	
        CriteriaQuery<Long> query = cb.createQuery(Long.class);
        
        Root<IndicateurSecurite> ouvrage = query.from(IndicateurSecurite.class);
        
        query.select(
        	cb.count(
        		ouvrage
        	)
        );
        
        query.where(
    		cb.and(
				createWhereClause(requestFilters, cb, ouvrage)
    		)
        );
        
        return entityManager
    			.createQuery(query)
    			.getSingleResult();
    }
    
    private Predicate[] createWhereClause(List<ParamValueDTO> requestFilters, CriteriaBuilder cb, Root<IndicateurSecurite> ouvrage) {
    	
    	WhereClauseRequestHelper whereClauseRequestHelper = new WhereClauseRequestHelper(cb);
        
        List<Predicate> predicates = new ArrayList<>();
        
        for (ParamValueDTO paramValue : requestFilters) {
        	switch (paramValue.getParam()) {
        		
	        	case "idMin": // For testing purpose
	        		predicates.add(whereClauseRequestHelper.idMinParam(paramValue.getValue(), ouvrage.get("id")));
	        		break;
	        	
	        	case "idMax": // For testing purpose
	        		predicates.add(whereClauseRequestHelper.idMaxParam(paramValue.getValue(), ouvrage.get("id")));
	        		break;
        	
        	}
        }
        
        return predicates.toArray(new Predicate[predicates.size()]);
    }

}
