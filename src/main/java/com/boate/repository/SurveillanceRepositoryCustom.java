package com.boate.repository;

import java.util.List;

import com.boate.dto.ParamValueDTO;
import com.boate.model.Surveillance;

public interface SurveillanceRepositoryCustom {

	List<Surveillance> findSurveillancesWithFilters(List<ParamValueDTO> requestFilters);
	
	Long countSurveillancesWithFilters(List<ParamValueDTO> requestFilters);

}
