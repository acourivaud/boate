package com.boate.repository;

import java.util.List;

import com.boate.dto.ParamValueDTO;
import com.boate.model.IndicateurSecurite;

public interface IndicateurSecuriteRepositoryCustom {

	List<IndicateurSecurite> findIndicateurSecuritesWithFilters(List<ParamValueDTO> requestFilters);
	
	Long countIndicateurSecuritesWithFilters(List<ParamValueDTO> requestFilters);

}
