package com.boate.repository;

import java.util.List;

import com.boate.dto.ParamValueDTO;
import com.boate.model.Bouclage;

public interface BouclageRepositoryCustom {

	List<Bouclage> findBouclagesWithFilters(List<ParamValueDTO> requestFilters, String liste);
	
	Long countBouclagesWithFilters(List<ParamValueDTO> requestFilters, String liste);

}
