package com.boate.repository;

import java.util.List;

import com.boate.dto.ParamValueDTO;
import com.boate.model.Ouvrage;

public interface OuvrageRepositoryCustom {

	List<Ouvrage> findOuvragesWithFilters(List<ParamValueDTO> requestFilters);
	
	Long countOuvragesWithFilters(List<ParamValueDTO> requestFilters);

}
