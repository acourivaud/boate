package com.boate.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.boate.dto.ParamValueDTO;
import com.boate.model.ArchiveDossier;
import com.boate.model.Ouvrage;
import com.boate.model.Secteur;
import com.boate.model.TypeOuvrage;
import com.boate.model.UniteOperationnelle;
import com.boate.model.Utilisateur;
import com.boate.security.entity.UserPrincipal;
import com.boate.utils.DataFormatUtils;
import com.boate.utils.WhereClauseRequestHelper;

public class ArchiveRepositoryCustomImpl implements ArchiveRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<ArchiveDossier> findArchivesWithFilters(List<ParamValueDTO> requestFilters, UserPrincipal currentUser) {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();

		CriteriaQuery<ArchiveDossier> query = cb.createQuery(ArchiveDossier.class);

		Root<ArchiveDossier> archiveDossier = query.from(ArchiveDossier.class);
		Join<ArchiveDossier, Ouvrage> ouvrage = archiveDossier.join("ouvrage", JoinType.LEFT);
		Join<Ouvrage, UniteOperationnelle> uniteOperationnelle = ouvrage.join("up", JoinType.LEFT);
		Join<UniteOperationnelle, Secteur> secteur = uniteOperationnelle.join("secteur", JoinType.LEFT);
		Join<Ouvrage, TypeOuvrage> typeOuvrage = ouvrage.join("typeKey", JoinType.LEFT);
		Join<ArchiveDossier, Utilisateur> utilisateur = archiveDossier.join("emprunteur", JoinType.LEFT);

		query.select(archiveDossier).where(cb.and(createWhereClause(requestFilters, cb, archiveDossier, ouvrage,
				uniteOperationnelle, secteur, typeOuvrage, utilisateur, currentUser)));

		Integer first = null;
		Integer rows = null;

		for (ParamValueDTO paramValue : requestFilters) {
			switch (paramValue.getParam()) {

			case "first":
				first = Integer.parseUnsignedInt(paramValue.getValue());
				break;

			case "rows":
				rows = Integer.parseUnsignedInt(paramValue.getValue());
				break;
			}
		}

		Optional<ParamValueDTO> ligneDeclassee = requestFilters.stream()
				.filter(paramValue -> "GenLigneDeclasseeSelected".equals(paramValue.getParam())).findFirst();
		Optional<ParamValueDTO> documentGeneraux = requestFilters.stream()
				.filter(paramValue -> "GenDocumentGenerauxSelected".equals(paramValue.getParam())).findFirst();

		if ((ligneDeclassee.isPresent() && ligneDeclassee.get().getValue().equals("true"))
				|| (documentGeneraux.isPresent() && documentGeneraux.get().getValue().equals("true"))) {
			query.orderBy(cb.asc(archiveDossier.get("ligneNonRattachee")),
					cb.asc(archiveDossier.get("pk")),
					cb.asc(archiveDossier.get("endPk")));
		} else {
			query.orderBy(cb.asc(archiveDossier.get("ouvrage").get("line")),
					cb.asc(archiveDossier.get("ouvrage").get("pk")),
					cb.asc(archiveDossier.get("ouvrage").get("indice")),
					cb.asc(archiveDossier.get("ouvrage").get("middlePk")),
					cb.asc(archiveDossier.get("ouvrage").get("endPk")));
		}

		if (first != null && rows != null) {
			return entityManager.createQuery(query).setHint("org.hibernate.cacheable", true).setFirstResult(first).setMaxResults(rows).getResultList();
		} else {
			return entityManager.createQuery(query).setHint("org.hibernate.cacheable", true).getResultList();
		}

	}

	@Override
	public Long countArchivesWithFilters(List<ParamValueDTO> requestFilters, UserPrincipal currentUser) {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();

		CriteriaQuery<Long> query = cb.createQuery(Long.class);

		Root<ArchiveDossier> archiveDossier = query.from(ArchiveDossier.class);
		Join<ArchiveDossier, Ouvrage> ouvrage = archiveDossier.join("ouvrage", JoinType.LEFT);
		Join<Ouvrage, UniteOperationnelle> uniteOperationnelle = ouvrage.join("up", JoinType.LEFT);
		Join<UniteOperationnelle, Secteur> secteur = uniteOperationnelle.join("secteur", JoinType.LEFT);
		Join<Ouvrage, TypeOuvrage> typeOuvrage = ouvrage.join("typeKey", JoinType.LEFT);
		Join<ArchiveDossier, Utilisateur> utilisateur = archiveDossier.join("emprunteur", JoinType.LEFT);

		query.select(cb.count(archiveDossier));

		query.where(cb.and(createWhereClause(requestFilters, cb, archiveDossier, ouvrage, uniteOperationnelle, secteur,
				typeOuvrage, utilisateur, currentUser)));

		return entityManager.createQuery(query).setHint("org.hibernate.cacheable", true).getSingleResult();
	}

	private Predicate[] createWhereClause(List<ParamValueDTO> requestFilters, CriteriaBuilder cb,
			Root<ArchiveDossier> archiveDossier, Join<ArchiveDossier, Ouvrage> ouvrage,
			Join<Ouvrage, UniteOperationnelle> uniteOperationnelle, Join<UniteOperationnelle, Secteur> secteur,
			Join<Ouvrage, TypeOuvrage> typeOuvrage, Join<ArchiveDossier, Utilisateur> utilisateur, UserPrincipal currentUser) {

		WhereClauseRequestHelper whereClauseRequestHelper = new WhereClauseRequestHelper(cb);

		List<Predicate> predicates = new ArrayList<>();
		Boolean rechercheNonRattacheeOuvrage = false;

		// PK
		Boolean fromSelected = false;
		String fromValue = null;
		Boolean toSelected = false;
		String toValue = null;

		for (ParamValueDTO paramValue : requestFilters) {
			switch (paramValue.getParam()) {

			case "GenLigneDeclasseeSelected":
				rechercheNonRattacheeOuvrage = paramValue.getValue().equals("true") ? true : rechercheNonRattacheeOuvrage;
				break;
			case "GenDocumentGenerauxSelected":
				rechercheNonRattacheeOuvrage = paramValue.getValue().equals("true") ? true : rechercheNonRattacheeOuvrage;
				break;
			case "GenEtatDossierSelected":
				if (paramValue.getValue().equals("libre")) {
					predicates.add(whereClauseRequestHelper.isNull(archiveDossier.get("emprunteur")));
				}else if (paramValue.getValue().equals("emprunte_byself")) {
					predicates.add(whereClauseRequestHelper.valueEquals(utilisateur.get("idUtilisateur"), String.class, currentUser.getId().toString()));
				}else {
					predicates.add(whereClauseRequestHelper.isNotNull(archiveDossier.get("emprunteur")));
				}
				break;
			case "GenEmprunteurSelectedSearch":
				predicates.add(whereClauseRequestHelper.valueEquals(utilisateur.get("idUtilisateur"), Integer.class, whereClauseRequestHelper.valueFromJsonString(paramValue.getValue())));
				break;

			// Filtres COMMON
			case "GenRegionSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("region").get("id"), String.class,
						paramValue.getValue()));
				break;

			case "GenInfrapoleSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(secteur.get("infrapole").get("id"), String.class,
						paramValue.getValue()));
				break;

			case "GenSecteurSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(uniteOperationnelle.get("secteur").get("id"),
						String.class, paramValue.getValue()));
				break;

			case "GenUoSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(uniteOperationnelle.get("id"), String.class,
						paramValue.getValue()));
				break;

			case "GenUicSelected":
				predicates.add(
						whereClauseRequestHelper.valueEquals(ouvrage.get("uic"), String.class, paramValue.getValue()));
				break;

			case "GenLigneSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("line").get("id"), String.class,
						paramValue.getValue()));
				break;

			case "GenFromTyped":
				fromSelected = true;
				fromValue = DataFormatUtils.checkPk(paramValue.getValue());
				break;

			case "GenToTyped":
				toSelected = true;
				toValue = DataFormatUtils.checkPk(paramValue.getValue());
				break;

			case "GenCategorieSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(ouvrage.get("categoryKey"), String.class,
						paramValue.getValue()));
				break;

			case "GenTypeSelected":
				predicates.add(whereClauseRequestHelper.valueEquals(typeOuvrage.get("nomBoate"), String.class,
						paramValue.getValue()));
				break;

			}
		}

		if (fromSelected || toSelected) {
			predicates.add(whereClauseRequestHelper.pkSearch(ouvrage, fromValue, toValue));
		}
		
		if (rechercheNonRattacheeOuvrage) {
			predicates.add(whereClauseRequestHelper.isNull(archiveDossier.get("ouvrage")));
		} else {
			predicates.add(whereClauseRequestHelper.isNotNull(archiveDossier.get("ouvrage")));
		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}

}
