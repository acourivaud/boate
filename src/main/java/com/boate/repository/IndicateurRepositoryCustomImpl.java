package com.boate.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.boate.dto.ParamValueDTO;
import com.boate.model.Indicateur;
import com.boate.utils.WhereClauseRequestHelper;

public class IndicateurRepositoryCustomImpl implements IndicateurRepositoryCustom {

	@PersistenceContext
    private EntityManager entityManager;
 
    @Override
    public List<Indicateur> findIndicateursWithFilters(List<ParamValueDTO> requestFilters) {
    	
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        
        CriteriaQuery<Indicateur> query = cb.createQuery(Indicateur.class);
        
        Root<Indicateur> indicateur = query.from(Indicateur.class);
        
        query.select(indicateur).where(
    		cb.and(
				createWhereClause(requestFilters, cb, indicateur)
    		)
        );
        
        Integer first = null;
        Integer rows = null;
        
        for (ParamValueDTO paramValue : requestFilters) {
        	switch (paramValue.getParam()) {
        	
	        	case "first":
	        		first = Integer.parseUnsignedInt(paramValue.getValue());
	        		break;
	        		
	        	case "rows":
	        		rows = Integer.parseUnsignedInt(paramValue.getValue());
	        		break;
        	}
        }
        
        query.orderBy(
    		cb.asc(indicateur.get("line")),
    		cb.asc(indicateur.get("pk")),
    		cb.asc(indicateur.get("indice")),
    		cb.asc(indicateur.get("middlePk")),
    		cb.asc(indicateur.get("endPk"))
    	);
        
        if (first != null && rows != null) {
        	return entityManager
        			.createQuery(query)
        			.setFirstResult(first)
        			.setMaxResults(rows)
                    .getResultList();
        } else {
        	return entityManager
        			.createQuery(query)
        			.getResultList();
        }
        
    }
    
    @Override
    public Long countIndicateursWithFilters(List<ParamValueDTO> requestFilters) {
    	
    	CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    	
        CriteriaQuery<Long> query = cb.createQuery(Long.class);
        
        Root<Indicateur> indicateur = query.from(Indicateur.class);
        
        query.select(
        	cb.count(
        		indicateur
        	)
        );
        
        query.where(
    		cb.and(
				createWhereClause(requestFilters, cb, indicateur)
    		)
        );
        
        return entityManager
    			.createQuery(query)
    			.getSingleResult();
    }
    
    private Predicate[] createWhereClause(List<ParamValueDTO> requestFilters, CriteriaBuilder cb, Root<Indicateur> indicateur) {
    	
    	WhereClauseRequestHelper whereClauseRequestHelper = new WhereClauseRequestHelper(cb);
        
        List<Predicate> predicates = new ArrayList<>();
        
        for (ParamValueDTO paramValue : requestFilters) {
        	switch (paramValue.getParam()) {
        		
	        	case "idMin": // For testing purpose
	        		predicates.add(whereClauseRequestHelper.idMinParam(paramValue.getValue(), indicateur.get("id")));
	        		break;
	        	
	        	case "idMax": // For testing purpose
	        		predicates.add(whereClauseRequestHelper.idMaxParam(paramValue.getValue(), indicateur.get("id")));
	        		break;
        	
        	}
        }
        
        return predicates.toArray(new Predicate[predicates.size()]);
    }

}
