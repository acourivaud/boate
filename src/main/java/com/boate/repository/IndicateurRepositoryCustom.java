package com.boate.repository;

import java.util.List;

import com.boate.dto.ParamValueDTO;
import com.boate.model.Indicateur;

public interface IndicateurRepositoryCustom {

	List<Indicateur> findIndicateursWithFilters(List<ParamValueDTO> requestFilters);
	
	Long countIndicateursWithFilters(List<ParamValueDTO> requestFilters);

}
