package com.boate.enums;

public enum TypeVisiteEnum {
	visit_type_fsa, 
	visit_type_hydro, 
	visit_type_id, 
	visit_type_id_camera, 
	visit_type_id_cv, 
	visit_type_idi, 
	visit_type_cl_ot,
	visit_type_pv0, 
	visit_type_sp, 
	visit_type_sr, 
	visit_type_surv_current, 
	visit_type_topo, 
	visit_type_va_ot, 
	visit_type_vd, 
	visit_type_vd_camera, 
	visit_type_vd_cv, 
	visit_type_vd_ot, 
	visit_type_ve_ot, 
	visit_type_vi, 
	visit_type_vi_ot, 
	visit_type_vp_ot, 
	visit_type_vs, 
	visit_type_vsp
}
