package com.boate.enums;

public enum ParamChampTypeEnum {
	INPUT_TEXT,
	INPUT_NUMBER,
	INPUT_DATE,
	LINK,
	SELECT,
	SPINNER,
	BOOLEAN
}
