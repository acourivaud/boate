package com.boate.enums;

public enum GeneralServiceActionEnum {
	SAVE_FROM_JSON_STRING,
	SAVE_FROM_JSON_STRING_LIST,
	SAVE_OBJECTS,
	GET_OBJECTS,
	DELETE_FROM_JSON_STRING
}
