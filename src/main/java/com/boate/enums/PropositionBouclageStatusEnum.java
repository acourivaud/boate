package com.boate.enums;

public enum PropositionBouclageStatusEnum {
	
	APPROVED("APPROVED"),
	REJECTED("REJECTED"),
	PENDING("PENDING");
	
	private final String status;
	
	PropositionBouclageStatusEnum(String status) {
		this.status = status;
	}
	
	@Override
    public String toString() {
        return status;
    }
	
}
