package com.boate.enums;

public enum EntiteUtilisateurEnum {
	ENTITY_DRIMOA_OT,
	ENTITY_EVEN,
	ENTITY_IGLG,
	ENTITY_IGOA,
	ENTITY_IMT,
	ENTITY_OTP,
	ENTITY_PRILY,
	ENTITY_PRICHY
}
