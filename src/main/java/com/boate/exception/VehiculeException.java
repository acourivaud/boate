package com.boate.exception;

public class VehiculeException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public VehiculeException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public VehiculeException() {
		super();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
