package com.boate.exception;

public class BouclageException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public BouclageException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public BouclageException() {
		super();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
