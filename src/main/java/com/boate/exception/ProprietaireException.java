package com.boate.exception;

public class ProprietaireException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public ProprietaireException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public ProprietaireException() {
		super();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
