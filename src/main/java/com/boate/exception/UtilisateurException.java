package com.boate.exception;

public class UtilisateurException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public UtilisateurException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public UtilisateurException() {
		super();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
