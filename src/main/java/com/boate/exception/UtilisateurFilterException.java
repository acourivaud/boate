package com.boate.exception;

public class UtilisateurFilterException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public UtilisateurFilterException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public UtilisateurFilterException() {
		super();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
