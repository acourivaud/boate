package com.boate.exception;

public class UtilisateurNotConnectedException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final String defaultMessage = "Utilisateur connecté non reconnu";

	private String errorMessage;

	public UtilisateurNotConnectedException(String errorMessage) {
		super(errorMessage, new Throwable("UserNotConnectedException"));
		this.errorMessage = errorMessage;
	}

	public UtilisateurNotConnectedException() {
		this(defaultMessage);
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}
