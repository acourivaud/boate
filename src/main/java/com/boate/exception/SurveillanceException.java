package com.boate.exception;

public class SurveillanceException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public SurveillanceException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public SurveillanceException() {
		super();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
