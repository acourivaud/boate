package com.boate.exception;

public class FilterException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public FilterException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public FilterException() {
		super();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
