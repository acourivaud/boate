package com.boate.exception;

public class AccessForbiddenException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final String defaultMessage = "Action non autorisée";

	private String errorMessage;

	public AccessForbiddenException(String errorMessage) {
		super(errorMessage, new Throwable("AccessForbiddenException"));
		this.errorMessage = errorMessage;
	}

	public AccessForbiddenException() {
		this(defaultMessage);
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
