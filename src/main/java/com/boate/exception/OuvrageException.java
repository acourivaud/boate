package com.boate.exception;

public class OuvrageException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public OuvrageException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public OuvrageException() {
		super();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
