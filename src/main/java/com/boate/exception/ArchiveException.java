package com.boate.exception;

public class ArchiveException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public ArchiveException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public ArchiveException() {
		super();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
