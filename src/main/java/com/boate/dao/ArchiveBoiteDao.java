package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.ArchiveBoite;

public interface ArchiveBoiteDao extends JpaRepository<ArchiveBoite, Integer> {

	List<ArchiveBoite> findTop50ByNomContainingOrderByNomAsc(String typedText);
	
	ArchiveBoite findByNom(String nom);

}
