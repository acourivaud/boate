package com.boate.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.boate.model.Geometrie;

public interface GeometrieDao extends JpaRepository<Geometrie, Integer> {
	
	List<Geometrie> findByLibelle(String libelle);
	
	@Query(
		"SELECT g " +
		"FROM Geometrie g " +
		"WHERE LOWER(UNACCENT(g.libelle)) LIKE LOWER(UNACCENT(CONCAT('%',:likeClause,'%'))) "
	)
	List<Geometrie> findAllByLibelleLike(@Param("likeClause") String likeClause, Pageable pageable);
	
}