package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.boate.model.Utilisateur;
import com.boate.model.UtilisateurFilter;

public interface UtilisateurFilterDao extends JpaRepository<UtilisateurFilter, Integer> {

	List<UtilisateurFilter> findByUtilisateur(Utilisateur utilisateur);
	
	List<UtilisateurFilter> findByUtilisateurAndPage(Utilisateur utilisateur, String page);
	
	UtilisateurFilter findByUtilisateurAndPageAndParam(Utilisateur utilisateur, String page, String param);
	
	@Transactional
	@Modifying
	@Query("DELETE FROM UtilisateurFilter uf WHERE uf.id IN :idsList")
	void deleteWhereUtilisateurFilterIdInList(@Param("idsList") List<Integer> idsList);
	
}
