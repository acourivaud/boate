package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.boate.model.Secteur;

public interface SecteurDao extends JpaRepository<Secteur, Integer> {
	
	@Query("SELECT s FROM Secteur s WHERE s.infrapole.id = ?1 ORDER BY s.nom ASC")
	List<Secteur> findByInfrapoleId(Integer id);
	
	@Query("SELECT s FROM Secteur s WHERE s.infrapole.id IN :idsList ORDER BY s.nom ASC")
	List<Secteur> findByInfrapoleIdInList(@Param("idsList") List<Integer> idsList);
	
	List<Secteur> findByNom(String nom);
}