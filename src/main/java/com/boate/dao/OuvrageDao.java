package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.boate.dto.ParamValueDTO;
import com.boate.model.Ouvrage;
import com.boate.repository.OuvrageRepositoryCustom;

public interface OuvrageDao extends JpaRepository<Ouvrage, Integer>, OuvrageRepositoryCustom {
	
	List<Ouvrage> findOuvragesWithFilters(List<ParamValueDTO> requestFilters);
	
	@Query(
		"SELECT o " +
		"FROM Ouvrage o " +
		"WHERE LOWER(UNACCENT(o.name)) LIKE LOWER(UNACCENT(CONCAT('%',:likeClause,'%'))) " +
		"ORDER BY o.name ASC"
	)
	List<Ouvrage> findAllByNameLike(@Param("likeClause") String likeClause);
	
	Long countOuvragesWithFilters(List<ParamValueDTO> requestFilters);
	
}
