package com.boate.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import com.boate.model.BouclageUtilisateur;

public interface BouclageUtilisateurDao extends JpaRepository<BouclageUtilisateur, Integer> {
	
}
