package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.boate.model.Ouvrage;
import com.boate.model.Proprietaire;

public interface ProprietaireDao extends JpaRepository<Proprietaire, Integer> {

	List<Proprietaire> findAllByOrderByNameAsc();
	
	@Query("SELECT p FROM Proprietaire p where id NOT IN (select proprietaire from OuvrageProprietaire where ouvrage = ?1) ORDER BY p.name")
	List<Proprietaire> findProprietaireDisponible(Ouvrage ouvrage);
	
}
