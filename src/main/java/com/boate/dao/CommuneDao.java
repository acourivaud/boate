package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.boate.model.Commune;

public interface CommuneDao extends JpaRepository<Commune, Integer> {

	@Query(
		"SELECT DISTINCT dc.commune " +
		"FROM RegionDepartement rd, DepartementCommune dc " +
		"WHERE rd.region.id = :id " +
			"AND rd.departement.id = dc.departement.id " +
			"AND (LOWER(UNACCENT(dc.commune.libelle)) LIKE LOWER(UNACCENT(CONCAT(:likeClause,'%'))) " +
				"OR dc.commune.codePostal = :likeClause) " +
		"ORDER BY dc.commune.libelle ASC"
	)
	List<Commune> findByRegionIdAndLibelleLikeOrCodePostalEquals(@Param("id") Integer id, @Param("likeClause") String likeClause);
	
	@Query(
		"SELECT dc.commune " +
		"FROM DepartementCommune dc " +
		"WHERE dc.departement.id = :id " +
			"AND (LOWER(UNACCENT(dc.commune.libelle)) LIKE LOWER(UNACCENT(CONCAT(:likeClause,'%'))) " +
				"OR dc.commune.codePostal = :likeClause) " +
		"ORDER BY dc.commune.libelle ASC"
	)
	List<Commune> findByDepartementIdAndLibelleLikeOrCodePostalEquals(@Param("id") Integer id, @Param("likeClause") String likeClause);
	
	@Query(
		"SELECT c " +
		"FROM Commune c " +
		"WHERE LOWER(UNACCENT(c.libelle)) LIKE LOWER(UNACCENT(CONCAT(:likeClause,'%'))) " +
			"OR c.codePostal = :likeClause " +
		"ORDER BY c.libelle ASC"
	)
	List<Commune> findAllByLibelleLikeOrCodePostalEquals(@Param("likeClause") String likeClause);
	
	@Query(
		"SELECT DISTINCT dc.commune " +
		"FROM RegionDepartement rd, DepartementCommune dc " +
		"WHERE rd.region.id = :regionId " +
			"AND rd.departement.id = dc.departement.id " +
			"AND dc.commune.id = :communeId "
	)
	Commune findByRegionIdAndCommuneId(@Param("regionId") Integer regionId, @Param("communeId") Integer communeId);
	
	@Query(
		"SELECT dc.commune " +
		"FROM DepartementCommune dc " +
		"WHERE dc.departement.id = :departementId " +
			"AND dc.commune.id = :communeId "
	)
	Commune findByDepartementIdAndCommuneId(@Param("departementId") Integer departementId, @Param("communeId") Integer communeId);
	
}
