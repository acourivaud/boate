package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.boate.model.Type;

public interface TypeDao extends JpaRepository<Type, Integer> {
	
	@Query("SELECT t FROM Type t WHERE t.categorie.id = ?1 ORDER BY t.libelle ASC")
	List<Type> findByCategorieId(Integer id);
	
	List<Type> findByLibelle(String libelle);
	
}