package com.boate.dao;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.boate.model.PlanningCollaborateur;
import com.boate.model.Utilisateur;

public interface PlanningCollaborateurDao extends JpaRepository<PlanningCollaborateur, Integer> {

	Optional<PlanningCollaborateur> findById(Integer id);
	
	List<PlanningCollaborateur> findByUtilisateur(Utilisateur utilisateur);
	
	@Query("SELECT pc FROM PlanningCollaborateur pc WHERE (pc.dateDebut >= ?1 AND pc.dateDebut < ?2) OR (pc.dateFin >= ?1 AND pc.dateFin < ?2) ORDER BY pc.utilisateur")
	List<PlanningCollaborateur> findBetweenDates(LocalDateTime dateDebut, LocalDateTime dateFin);
	
	@Query("SELECT pc FROM PlanningCollaborateur pc WHERE pc.utilisateur.idUtilisateur = ?1 AND ((pc.dateDebut >= ?2 AND pc.dateDebut < ?3) OR (pc.dateFin >= ?2 AND pc.dateFin < ?3))")
	List<PlanningCollaborateur> findByUtilisateurIdBetweenDates(Integer utilisateurId, LocalDateTime dateDebut, LocalDateTime dateFin);
	
	@Query("SELECT pc FROM PlanningCollaborateur pc WHERE pc.id IN :ids")
	List<PlanningCollaborateur> findByIdInList(@Param("ids") List<Integer> ids);
	
}
