package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.dto.ParamValueDTO;
import com.boate.model.ArchiveDossier;
import com.boate.repository.ArchiveRepositoryCustom;
import com.boate.security.entity.UserPrincipal;

public interface ArchiveDossierDao extends JpaRepository<ArchiveDossier, Integer>, ArchiveRepositoryCustom {

	List<ArchiveDossier> findArchivesWithFilters(List<ParamValueDTO> requestFilters, UserPrincipal currentUser);
	
	Long countArchivesWithFilters(List<ParamValueDTO> requestFilters, UserPrincipal currentUser);
	
	List<ArchiveDossier> findByOuvrageId(Integer id);
}
