package com.boate.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.Connexion;

public interface ConnexionDao extends JpaRepository<Connexion, Integer> {

}
