package com.boate.dao;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.boate.model.PlanningMateriel;
import com.boate.model.Utilisateur;
import com.boate.model.Materiel;

public interface PlanningMaterielDao extends JpaRepository<PlanningMateriel, Integer> {

	Optional<PlanningMateriel> findById(Integer id);
	
	List<PlanningMateriel> findByMateriel(Materiel materiel);
	
	List<PlanningMateriel> findByUtilisateur(Utilisateur utilisateur);
	
	@Query("SELECT pm FROM PlanningMateriel pm WHERE (pm.dateDebut >= ?1 AND pm.dateDebut < ?2) OR (pm.dateFin >= ?1 AND pm.dateFin < ?2) ORDER BY pm.utilisateur")
	List<PlanningMateriel> findBetweenDates(LocalDateTime dateDebut, LocalDateTime dateFin);
	
	@Query("SELECT pm FROM PlanningMateriel pm WHERE pm.materiel.id = ?1 AND ((pm.dateDebut >= ?2 AND pm.dateDebut < ?3) OR (pm.dateFin >= ?2 AND pm.dateFin < ?3))")
	List<PlanningMateriel> findByMaterielIdBetweenDates(Integer materielId, LocalDateTime dateDebut, LocalDateTime dateFin);
	
	@Query("SELECT pm FROM PlanningMateriel pm WHERE pm.utilisateur.idUtilisateur = ?1 AND ((pm.dateDebut >= ?2 AND pm.dateDebut < ?3) OR (pm.dateFin >= ?2 AND pm.dateFin < ?3))")
	List<PlanningMateriel> findByUtilisateurIdBetweenDates(Integer utilisateurId, LocalDateTime dateDebut, LocalDateTime dateFin);
	
	@Query("SELECT pm FROM PlanningMateriel pm WHERE pm.id IN :ids")
	List<PlanningMateriel> findByIdInList(@Param("ids") List<Integer> ids);
	
}
