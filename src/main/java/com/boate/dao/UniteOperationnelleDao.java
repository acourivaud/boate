package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.boate.model.UniteOperationnelle;

public interface UniteOperationnelleDao extends JpaRepository<UniteOperationnelle, Integer> {
	
	@Query("SELECT uo FROM UniteOperationnelle uo WHERE uo.infrapole.id = ?1 ORDER BY uo.code ASC")
	List<UniteOperationnelle> findByInfrapoleId(Integer id);
	
	@Query("SELECT uo FROM UniteOperationnelle uo WHERE uo.infrapole.id IN :idsList ORDER BY uo.code ASC")
	List<UniteOperationnelle> findByInfrapoleIdInList(@Param("idsList") List<Integer> idsList);
	
	@Query("SELECT uo FROM UniteOperationnelle uo WHERE uo.secteur.id = ?1 ORDER BY uo.code ASC")
	List<UniteOperationnelle> findBySecteurId(Integer id);
	
	@Query("SELECT uo FROM UniteOperationnelle uo WHERE uo.secteur.id IN :idsList ORDER BY uo.code ASC")
	List<UniteOperationnelle> findBySecteurIdInList(@Param("idsList") List<Integer> idsList);
	
	List<UniteOperationnelle> findByName(String name);
	
	List<UniteOperationnelle> findByCode(String code);
}