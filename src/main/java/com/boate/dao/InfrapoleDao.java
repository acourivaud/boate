package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.boate.model.Infrapole;

public interface InfrapoleDao extends JpaRepository<Infrapole, Integer> {
	
	List<Infrapole> findAllByOrderByNomAsc();
	
	@Query("SELECT i FROM Infrapole i WHERE i.region.id = ?1 ORDER BY i.nom ASC")
	List<Infrapole> findByRegionId(Integer id);
	
	List<Infrapole> findByNom(String nom);
	
	List<Infrapole> findByCode(String code);
	
}