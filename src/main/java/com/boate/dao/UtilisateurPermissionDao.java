package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.Utilisateur;
import com.boate.model.UtilisateurPermission;

public interface UtilisateurPermissionDao extends JpaRepository<UtilisateurPermission, Integer> {

	List<UtilisateurPermission> findByUtilisateur(Utilisateur utilisateur);

}
