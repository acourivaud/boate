package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.Region;

public interface RegionDao extends JpaRepository<Region, Integer> {
	
	List<Region> findAllByOrderByNomAsc();
	
	List<Region> findByNom(String nom);
	
	List<Region> findByCode(Integer code);
	
}