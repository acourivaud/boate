package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.dto.ParamValueDTO;
import com.boate.model.Indicateur;
import com.boate.repository.IndicateurRepositoryCustom;

public interface IndicateurDao extends JpaRepository<Indicateur, Integer>, IndicateurRepositoryCustom {
	
	List<Indicateur> findIndicateursWithFilters(List<ParamValueDTO> requestFilters);
	
	Long countIndicateursWithFilters(List<ParamValueDTO> requestFilters);
	
}
