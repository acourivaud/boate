package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.dto.ParamValueDTO;
import com.boate.model.IndicateurSecurite;
import com.boate.repository.IndicateurSecuriteRepositoryCustom;

public interface IndicateurSecuriteDao extends JpaRepository<IndicateurSecurite, Integer>, IndicateurSecuriteRepositoryCustom {
	
	List<IndicateurSecurite> findIndicateurSecuritesWithFilters(List<ParamValueDTO> requestFilters);
	
	Long countIndicateurSecuritesWithFilters(List<ParamValueDTO> requestFilters);
	
}
