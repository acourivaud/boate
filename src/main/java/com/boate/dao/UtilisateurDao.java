package com.boate.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.boate.enums.FonctionUtilisateurEnum;
import com.boate.model.Utilisateur;

public interface UtilisateurDao extends JpaRepository<Utilisateur, Integer> {

	Optional<Utilisateur> findByLogin(String login);

	Utilisateur findByIdUtilisateur(Integer idUtilisateur);

	List<Utilisateur> findByFonctionAndDateFinMissionIsNullAndPrenomIsNullOrderByNomAscPrenomAsc(FonctionUtilisateurEnum fonction);
	
	List<Utilisateur> findAllByFonctionOrderByNom(FonctionUtilisateurEnum fonction);

	@Query("SELECT u " + "FROM Utilisateur u "
			+ "WHERE LOWER(UNACCENT(CONCAT(u.prenom, ' ', u.nom))) LIKE LOWER(UNACCENT(CONCAT('%',:likeClause,'%'))) "
			+ "OR LOWER(UNACCENT(CONCAT(u.nom, u.prenom))) LIKE LOWER(UNACCENT(CONCAT('%',:likeClause,'%'))) ORDER BY u.nom ASC")
	List<Utilisateur> findSearchUtilisateurs(@Param("likeClause") String likeClause);
	
	Boolean existsByLogin(String login);
	
	@Query("SELECT u FROM Utilisateur u ORDER BY u.nom ASC")
	List<Utilisateur> findAllOrderByNom();
	
	@Query("SELECT u FROM Utilisateur u WHERE u.fonction <> 'FUNCTION_ARCHIVE' AND u.fonction <> 'FUNCTION_ADMIN' ORDER BY u.nom ASC")
	List<Utilisateur> findAuteursOrderByNom();
	
	@Query("SELECT u FROM Utilisateur u WHERE u.fonction = 'FUNCTION_AOAP' AND (u.region = 'Rhodanien' OR u.region = 'LGV') ORDER BY u.nom ASC")
	List<Utilisateur> findAoapsOrderByNom();
}
