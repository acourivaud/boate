package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.boate.model.LigneSegment;

public interface LigneSegmentDao extends JpaRepository<LigneSegment, Integer> {
	
	@Query(
		"SELECT DISTINCT(ls.codeSegment) " +
		"FROM LigneSegment ls " +
		"ORDER BY ls.codeSegment ASC"
	)
	List<String> findAllDistinctCodeOrderByCode();
	
}
