package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.Filter;

public interface FilterDao extends JpaRepository<Filter, Integer> {

	List<Filter> findByGuid(String guid);
	
}
