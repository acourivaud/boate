package com.boate.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.boate.model.Ligne;

public interface LigneDao extends JpaRepository<Ligne, Integer> {
	
	@Query("SELECT uol.ligne FROM UniteOperationnelleLigne uol JOIN uol.uniteOperationnelle uo WHERE uo.id = ?1 ORDER BY uol.ligne.code ASC")
	List<Ligne> findByUniteOperationnelleId(Integer id);
	
	@Query("SELECT DISTINCT uol.ligne FROM UniteOperationnelleLigne uol JOIN uol.uniteOperationnelle uo WHERE uo.id IN :idsList ORDER BY uol.ligne.code ASC")
	List<Ligne> findByUniteOperationnelleIdInList(@Param("idsList") List<Integer> idsList);
	
	List<Ligne> findByName(String name);
	
	List<Ligne> findByCode(String code);
	
	@Query(
		"SELECT l " +
		"FROM Ligne l " +
		"WHERE LOWER(UNACCENT(l.name)) LIKE LOWER(UNACCENT(CONCAT('%',:likeClause,'%'))) " +
			"OR l.code = :likeClause " +
		"ORDER BY l.name ASC"
	)
	List<Ligne> findAllByNameLikeOrCodeEquals(@Param("likeClause") String likeClause, Pageable pageable);
	
}