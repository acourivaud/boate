package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.boate.model.Departement;

public interface DepartementDao extends JpaRepository<Departement, Integer> {

	List<Departement> findAllByOrderByLibelleAsc();
	
	@Query("SELECT rd.departement FROM RegionDepartement rd WHERE rd.region.id = ?1 ORDER BY rd.departement.libelle ASC")
	List<Departement> findByRegionId(Integer id);
	
}
