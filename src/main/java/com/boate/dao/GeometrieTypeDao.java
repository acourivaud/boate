package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.Geometrie;
import com.boate.model.GeometrieType;
import com.boate.model.Type;

public interface GeometrieTypeDao extends JpaRepository<GeometrieType, Integer> {
	
	List<GeometrieType> findByType(Type type);
	
	List<GeometrieType> findByGeometrie(Geometrie geometrie);
	
	GeometrieType findByGeometrieAndType(Geometrie geometrie, Type type);
	
}