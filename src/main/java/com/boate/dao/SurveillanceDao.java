package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.boate.dto.ParamValueDTO;
import com.boate.model.Surveillance;
import com.boate.repository.SurveillanceRepositoryCustom;

public interface SurveillanceDao extends JpaRepository<Surveillance, Integer>, SurveillanceRepositoryCustom {
	
	List<Surveillance> findSurveillancesWithFilters(List<ParamValueDTO> requestFilters);
	
	Long countSurveillancesWithFilters(List<ParamValueDTO> requestFilters);
	
	@Query(
		"SELECT DISTINCT(s.codeAnalytique) " +
		"FROM Surveillance s " +
		"ORDER BY s.codeAnalytique ASC"
	)
	List<String> findAllDistinctCodeOrderByCode();
	
	List<Surveillance> findByOuvrageId(Integer id);
}
