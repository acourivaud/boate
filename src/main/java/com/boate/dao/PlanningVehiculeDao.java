package com.boate.dao;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.boate.model.PlanningVehicule;
import com.boate.model.Utilisateur;
import com.boate.model.Vehicule;

public interface PlanningVehiculeDao extends JpaRepository<PlanningVehicule, Integer> {

	Optional<PlanningVehicule> findById(Integer id);
	
	List<PlanningVehicule> findByVehicule(Vehicule vehicule);
	
	List<PlanningVehicule> findByUtilisateur(Utilisateur utilisateur);
	
	@Query("SELECT pv FROM PlanningVehicule pv WHERE (pv.dateDebut >= ?1 AND pv.dateDebut < ?2) OR (pv.dateFin >= ?1 AND pv.dateFin < ?2) ORDER BY pv.utilisateur")
	List<PlanningVehicule> findBetweenDates(LocalDateTime dateDebut, LocalDateTime dateFin);
	
	@Query("SELECT DISTINCT(pv.vehicule) FROM PlanningVehicule pv WHERE (pv.dateDebut >= ?1 AND pv.dateDebut < ?2) OR (pv.dateFin >= ?1 AND pv.dateFin < ?2)")
	List<Vehicule> findDistinctBetweenDates(LocalDateTime dateDebut, LocalDateTime dateFin);
	
	@Query("SELECT pv FROM PlanningVehicule pv WHERE pv.vehicule.id = ?1 AND ((pv.dateDebut >= ?2 AND pv.dateDebut < ?3) OR (pv.dateFin >= ?2 AND pv.dateFin < ?3))")
	List<PlanningVehicule> findByVehiculeIdBetweenDates(Integer vehiculeId, LocalDateTime dateDebut, LocalDateTime dateFin);
	
	@Query("SELECT pv FROM PlanningVehicule pv WHERE pv.utilisateur.idUtilisateur = ?1 AND ((pv.dateDebut >= ?2 AND pv.dateDebut < ?3) OR (pv.dateFin >= ?2 AND pv.dateFin < ?3))")
	List<PlanningVehicule> findByUtilisateurIdBetweenDates(Integer utilisateurId, LocalDateTime dateDebut, LocalDateTime dateFin);
	
	@Query("SELECT pv FROM PlanningVehicule pv WHERE pv.id IN :ids")
	List<PlanningVehicule> findByIdInList(@Param("ids") List<Integer> ids);
	
}
