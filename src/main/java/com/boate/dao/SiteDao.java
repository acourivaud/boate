package com.boate.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.Secteur;

public interface SiteDao extends JpaRepository<Secteur, Integer> {
	
}