package com.boate.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.boate.model.Materiel;

public interface MaterielDao extends JpaRepository<Materiel, Integer> {

	Optional<Materiel> findById(Integer id);
	
	@Query(
		"SELECT m " +
		"FROM Materiel m " +
		"WHERE LOWER(UNACCENT(m.nom)) = LOWER(UNACCENT(?1))"
	)
	Materiel findByNom(String nom);
	
}
