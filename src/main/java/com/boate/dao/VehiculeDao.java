package com.boate.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.boate.model.Vehicule;

public interface VehiculeDao extends JpaRepository<Vehicule, Integer> {

	Optional<Vehicule> findById(Integer id);
	
	List<Vehicule> findByMarque(String marque);
	
	List<Vehicule> findByModele(String modele);
	
	List<Vehicule> findByPlaces(Integer nbPlaces);
	
	Optional<Vehicule> findByImmatriculation(String immatriculation);
	
	@Query("SELECT v FROM Vehicule v WHERE v.id NOT IN :ids")
	List<Vehicule> findByIdNotInList(@Param("ids") List<Integer> ids);
	
}
