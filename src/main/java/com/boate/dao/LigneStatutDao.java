package com.boate.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.LigneStatut;

public interface LigneStatutDao extends JpaRepository<LigneStatut, Integer> {

}
