package com.boate.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.In1253;

public interface In1253Dao extends JpaRepository<In1253, Integer> {

}
