package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.ParamTemplate;

public interface ParamTemplateDao extends JpaRepository<ParamTemplate, Integer> {
	
	List<ParamTemplate> findByLibelle(String libelle);

}
