package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.Ouvrage;
import com.boate.model.OuvrageProprietaire;

public interface OuvrageProprietaireDao extends JpaRepository<OuvrageProprietaire, Integer> {

	List<OuvrageProprietaire> findByOuvrageOrderByOrdre(Ouvrage ouvrage);
}
