package com.boate.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.LigneUic;

public interface LigneUicDao extends JpaRepository<LigneUic, Integer> {

}
