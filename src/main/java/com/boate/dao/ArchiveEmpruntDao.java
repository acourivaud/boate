package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.ArchiveEmprunt;
import com.boate.model.Utilisateur;

public interface ArchiveEmpruntDao extends JpaRepository<ArchiveEmprunt, Integer> {

	List<ArchiveEmprunt> findByUtilisateurOrderByDateEmpruntDesc(Utilisateur utilisateur);
}
