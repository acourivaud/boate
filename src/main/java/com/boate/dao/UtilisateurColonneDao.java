package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.Utilisateur;
import com.boate.model.UtilisateurColonne;

public interface UtilisateurColonneDao extends JpaRepository<UtilisateurColonne, Integer> {
	
	List<UtilisateurColonne> findByUtilisateurAndPage(Utilisateur utilisateur, String page);
	
	List<UtilisateurColonne> findByUtilisateurAndPageOrderByParam(Utilisateur utilisateur, String page);

	UtilisateurColonne findByUtilisateurAndPageAndParam(Utilisateur utilisateur, String page, String param);
	
}
