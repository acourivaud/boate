package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.boate.dto.ParamValueDTO;
import com.boate.model.Bouclage;
import com.boate.repository.BouclageRepositoryCustom;

public interface BouclageDao extends JpaRepository<Bouclage, Integer>, BouclageRepositoryCustom {
	
	List<Bouclage> findBouclagesWithFilters(List<ParamValueDTO> requestFilters, String liste);
	
	Long countBouclagesWithFilters(List<ParamValueDTO> requestFilters, String liste);
	
	@Query("SELECT b FROM Bouclage b WHERE b.id IN :ids")
	List<Bouclage> findByIdInList(@Param("ids") List<Integer> ids);
	
	List<Bouclage> findBySurveillanceOuvrageId(Integer id);
}
