package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.Categorie;

public interface CategorieDao extends JpaRepository<Categorie, Integer> {
	
	List<Categorie> findAllByOrderByLibelleAsc();
	
	List<Categorie> findByLibelle(String libelle);
	
}