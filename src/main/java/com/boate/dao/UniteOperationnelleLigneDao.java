package com.boate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.boate.model.Ligne;
import com.boate.model.UniteOperationnelle;
import com.boate.model.UniteOperationnelleLigne;

public interface UniteOperationnelleLigneDao extends JpaRepository<UniteOperationnelleLigne, Integer> {
	
	List<UniteOperationnelleLigne> findByUniteOperationnelle(UniteOperationnelle uniteOperationnelle);
	
	List<UniteOperationnelleLigne> findByLigne(Ligne ligne);
	
	UniteOperationnelleLigne findByUniteOperationnelleAndLigne(UniteOperationnelle uniteOperationnelle, Ligne ligne);
	
	@Query("SELECT uol FROM UniteOperationnelleLigne uol JOIN uol.uniteOperationnelle uo WHERE uo.id IN :idsList ORDER BY uol.ligne.code, uol.pkDebut ASC")
	List<UniteOperationnelleLigne> findByUniteOperationnelleIdInList(@Param("idsList") List<Integer> idsList);
}