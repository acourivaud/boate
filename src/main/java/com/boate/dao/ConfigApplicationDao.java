package com.boate.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.ConfigApplication;

public interface ConfigApplicationDao extends JpaRepository<ConfigApplication, Integer> {

}
