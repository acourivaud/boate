package com.boate.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.LigneVitesse;

public interface LigneVitesseDao extends JpaRepository<LigneVitesse, Integer> {

}
