package com.boate.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.ModificationFiche;

public interface ModificationFicheDao extends JpaRepository<ModificationFiche, Integer> {

}
