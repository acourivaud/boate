package com.boate.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boate.model.Permission;

public interface PermissionDao extends JpaRepository<Permission, Integer> {

}
