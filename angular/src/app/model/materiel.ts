import { IUniversalObject } from './IUniversalObject';
import { BackUrlConstant } from './../constant/BackUrlConstant';
import { Structure } from './structure';

export class Materiel implements IUniversalObject {
  readonly className: string = 'Materiel';
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.MATERIEL,
    apiPostUrl: BackUrlConstant.MATERIEL,
    apiDeleteUrl: BackUrlConstant.MATERIEL,
    isLazyListed: false,
    popupTitleDescription: 'un Matériel',
    popupChildDelete: false,
    cols: [
      { field: 'nom', header: 'Nom', style: 'column-center', editable: true },
    ],
    fields: [
      { index: 'nom', libelle: 'Nom', type: 'input', required: true },
    ]
  };

  id: number;
  nom: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new Materiel(dto);
  }

  completeDTO(dto: any, index: number): Materiel {
    // Nothing to complete atm
    return dto;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.nom = dto.nom;
    }
  }

  getSummaryDescription(): string {
    return this.nom;
  }
}
