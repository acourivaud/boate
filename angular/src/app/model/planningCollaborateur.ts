import { DateDto } from './dateDto';
import { ReservationCollaborateur } from './reservationCollaborateur';
import { Utilisateur } from './utilisateur';

export class PlanningCollaborateur {

  utilisateur: Utilisateur;
  reservationsCollaborateur: ReservationCollaborateur[];

  // Début et fin de la plage de requête.
  dateDebut: { dto: string };
  dateFin: { dto: string };

	conflictOnUserResponse: boolean = false;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new PlanningCollaborateur(dto);
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.utilisateur = new Utilisateur(dto.utilisateur);
      this.reservationsCollaborateur = ReservationCollaborateur.populateFromArrayDTO(dto.reservationsCollaborateur);
      this.dateDebut = dto.dateDebut ? new DateDto(dto.dateDebut).toBackEndDto() : null;
      this.dateFin = dto.dateFin ? new DateDto(dto.dateFin).toBackEndDto() : null;
      this.conflictOnUserResponse = dto.conflictOnUserResponse;
    }
  }
}
