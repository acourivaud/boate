import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbToastrConfig } from '@nebular/theme/components/toastr/toastr-config';

export class ToastrHelper {

  private toastrService: NbToastrService;

  constructor(toastrService: NbToastrService) {
    this.toastrService = toastrService;
  }

  showToast(type: NbToastStatus, title: string, body: string, duration ?: number) {
    const config: NbToastrConfig = new NbToastrConfig({
      status: type,
      destroyByClick: true,
      duration: duration ? duration : 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
      preventDuplicates: false,
    });

    this.toastrService.show(body, title, config);
  }

  showDefaultSuccessToast() {
    this.showToast(NbToastStatus.SUCCESS, 'SAUVEGARDE RÉUSSIE', 'Modifications enregistrées avec succès.');
  }

  showDefaultErrorToast(erreur: string) {
    this.showToast(NbToastStatus.DANGER, 'UNE ERREUR EST SURVENUE', erreur, 7000);
  }
}
