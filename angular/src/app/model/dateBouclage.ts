import { DateDto } from './dateDto';
export class DateBouclage {
	public date: string;
	public diff: number;
	public auteur: string;

	public realDate: Date;
	public dtoDate: { dto: string };

	constructor(dto: any) {
		this.populateFromDTO(dto);
	}

	populateFromDTO(dto: any) {
		if (dto) {
			if (dto.date) {
				this.date = new Date(dto.date[0], dto.date[1] - 1, dto.date[2]).toLocaleDateString();
			}
			if (dto.diff) this.diff = dto.diff;
			if (dto.auteur) this.auteur = dto.auteur;
		}
	}

	getDiffColor(): string {
		if (this.diff) {
			if (this.diff < 150) {
				return 'date-is-blue';
			}
			if (this.diff < 180) {
				return 'date-is-orange';
			}
			return 'date-is-red';
		}
		return '';
	}

	adaptFromRealDate() {
		if (this.realDate) {
			this.date = this.realDate.toLocaleDateString();
			this.dtoDate = DateDto.createFromDate(this.realDate).toBackEndDto();
		}
	}

	getDescription(): string {
		let dateTooltip: string = '';
		if (this.diff) dateTooltip += this.diff + 'J';
		if (this.date) dateTooltip += dateTooltip !== '' ? ' - ' + this.date : this.date;
		if (this.auteur) dateTooltip += dateTooltip !== '' ? ' - ' + this.auteur : this.auteur;
		return dateTooltip;
	}
}
