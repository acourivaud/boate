import { MethodeCallBack } from './methodeCallBack';
import { ColumnDefinitionHelper } from './columnDefinitionHelper';

export class Structure {

  apiGetUrl: string;
  apiPostUrl: string;
  apiDeleteUrl: string;
  isLazyListed?: boolean;
  popupTitleDescription: string;
  popupChildDelete: boolean;
  cols: ColumnDefinitionHelper[];
  fields: {
    index: string;
    libelle: string;
    type: string;
    suggestionField?: string;
    spinnerMin?: number;
    spinnerMax?: number;
    spinnerStep?: number;
    selectOptions?: any; // utilisation d'un tableau en dur dans le front
    selectOptionsUrl?: string; // url d'appel du back
    methodUrlCall?: MethodeCallBack; // Méthode d'appel du back
    paramId?: string; // paramètre id à récupérer dans l'array de l'objet à éditer / créer
    required: boolean;
  }[];

}
