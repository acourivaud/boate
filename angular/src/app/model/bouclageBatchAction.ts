import { Bouclage } from './bouclage';
import { DateDto } from './dateDto';
import { ParamValue } from './paramValue';

export class BouclageBatchAction {
	public bouclagesIds: number[];
	public date: Date;
	public batchType: BouclageBatchType;
	public filtersForReturn: ParamValue[];
	public numeroOperation?: number;

	constructor(page: string, filtersForReturn: ParamValue[], bouclages: Bouclage[], date: Date, batchType: BouclageBatchType, numeroOperation?: number) {
		if (page === undefined) throw new Error('paramètre "page" undefined');
		if (filtersForReturn === undefined) throw new Error('paramètre "filtersForReturn" undefined');
		if (bouclages === undefined || bouclages.length < 1) throw new Error('paramètre "bouclages" undefined');
		if (batchType === undefined) throw new Error('paramètre "batchType" undefined');
		if (batchType !== BouclageBatchType.NUMERO_OPERATION && date === undefined) throw new Error('paramètre "date" undefined');
		if (batchType === BouclageBatchType.NUMERO_OPERATION && bouclages.length > 1) throw new Error('un seul bouclage possible par numéro d\'opération');
		if (batchType === BouclageBatchType.NUMERO_OPERATION && numeroOperation === undefined) throw new Error('paramètre "numeroOperation" undefined');
		this.filtersForReturn = filtersForReturn;
		this.filtersForReturn.push(new ParamValue('bouclage', page));
		this.bouclagesIds = [];
		if (bouclages && bouclages.length > 0) {
			for (const bouclage of bouclages) {
				this.bouclagesIds.push(bouclage.id);
			}
		}
		if (batchType !== BouclageBatchType.NUMERO_OPERATION) this.date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
		this.batchType = batchType;
		this.numeroOperation = numeroOperation;
	}
}

export enum BouclageBatchType {
	AJOUT_DATE = 'AJOUT_DATE',
	SUPPRESSION_DATE = 'SUPPRESSION_DATE',
	COMPLEMENT_VISITE = 'COMPLEMENT_VISITE',
	NUMERO_OPERATION = 'NUMERO_OPERATION'
}
