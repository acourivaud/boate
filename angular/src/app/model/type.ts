import { IUniversalObject } from './IUniversalObject';
import { Categorie } from './categorie';
import { BackUrlConstant } from '../constant/BackUrlConstant';
import { Structure } from './structure';

export class Type implements IUniversalObject {
  readonly className: string = 'Type';
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.FILTER_TYPE,
    apiPostUrl: BackUrlConstant.FILTER_TYPE,
    apiDeleteUrl: BackUrlConstant.FILTER_TYPE,
    isLazyListed: false,
    popupTitleDescription: "un Type",
    popupChildDelete: false,
    cols: [],
    fields: [
      {
        index: 'libelle',
        libelle: 'Libellé',
        type: 'input',
        required: true,
      },
    ]
  };
  id: number;
  libelle: string;
  categorie: Categorie;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
    return new Type(dto);
  }

  completeDTO(dto: any, index: number): Type {
    // Nothing to complete atm
    return dto;
  }

  getSummaryDescription(): string {
    return this.libelle;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.libelle = dto.libelle;
      this.categorie = dto.categorie ? new Categorie(dto.categorie): undefined;
    }
  }
}
