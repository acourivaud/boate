import { IUniversalObject } from './IUniversalObject';
import { BackUrlConstant } from '../constant/BackUrlConstant';
import { Structure } from './structure';

export class Categorie implements IUniversalObject {
  readonly className: string = 'Categorie';
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.FILTER_CATEGORIE,
    apiPostUrl: BackUrlConstant.FILTER_CATEGORIE,
    apiDeleteUrl: BackUrlConstant.FILTER_CATEGORIE,
    isLazyListed: false,
    popupTitleDescription: 'une Catégorie',
    popupChildDelete: true,
    cols: [],
    fields: [
      {
        index: 'libelle',
        libelle: 'Libellé',
        type: 'input',
        required: true,
      },
    ]
  };
  id: number;
  libelle: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
    return new Categorie(dto);
  }

  completeDTO(dto: any, index: number): Categorie {
    // Nothing to complete atm
    return dto;
  }

  getSummaryDescription(): string {
    return this.libelle;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.libelle = dto.libelle;
    }
  }
}
