export class Commune {
  id: number;
  nom: string;
  codePostal: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.nom = dto.nom;
      this.codePostal = dto.codePostal;
    }
  }
}
