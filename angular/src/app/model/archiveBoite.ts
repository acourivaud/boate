import { BackUrlConstant } from './../constant/BackUrlConstant';
import { IUniversalObject } from './IUniversalObject';
import { Structure } from './structure';

export class ArchiveBoite implements IUniversalObject {
  readonly className: string = 'ArchiveBoite';
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.BOITE,
    apiPostUrl: BackUrlConstant.BOITE,
    apiDeleteUrl: BackUrlConstant.BOITE,
    isLazyListed: false,
    popupTitleDescription: 'une Boite',
    popupChildDelete: false,
    cols: [
      { field: 'nom', header: 'Nom', style: 'column-center', editable: true },
      { field: 'localisation', header: 'Localisation', style: 'column-center', editable: true }
    ],
    fields: [
      { index: 'nom', libelle: 'Nom', type: 'input', required: true },
      { index: 'localisation', libelle: 'Localisation', type: 'input', required: true },
    ]
  };
  id: number;
  nom: string;
  localisation: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new ArchiveBoite(dto);
  }

  completeDTO(dto: any, index: number): ArchiveBoite {
    // Nothing to complete atm
    return dto;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.nom = dto.nom;
      this.localisation = dto.localisation;
    }
  }

  getSummaryDescription(): string {
    return this.localisation + ' - ' + this.nom;
  }
}
