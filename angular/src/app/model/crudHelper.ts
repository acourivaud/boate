import { TemplateRef, ChangeDetectorRef } from '@angular/core';
import { NbDialogService } from '@nebular/theme';

import { GeneralService } from './../service/general.service';
import { ParamValue } from './paramValue';
import { ToastrHelper } from './toastrHelper';
import { ParamModule } from './paramModule';
import { ObjectUtils } from 'primeng/components/utils/objectutils';

export class CrudHelper {

  generalService: GeneralService;
  dialogService: NbDialogService;
  toastrHelper: ToastrHelper;
  crudObjectDefinitions: any;
  cruPopupTemplate: TemplateRef < any > ;
  deletePopupTemplate: TemplateRef < any > ;
  editedObject: any;
  editedObjectInRow: any;
  crudObjectsLists: any = {};
  crudObjectsListsHasBeenLoaded: any = {};
  crudObjectsLoadings: any = {};
  crudObjectsRows: any = {};
  crudObjectsFirst: any = {};
  crudObjectsTotals: any = {};
  activeDialogRef: any;

  constructor(generalService: GeneralService,
    dialogService: NbDialogService,
    toastrHelper: ToastrHelper,
    crudObjectDefinitions: any,
    cruPopupTemplate: TemplateRef < any > ,
    deletePopupTemplate: TemplateRef < any >,
    rows?: number) {
    this.generalService = generalService;
    this.dialogService = dialogService;
    this.toastrHelper = toastrHelper;
    this.crudObjectDefinitions = crudObjectDefinitions;
    this.cruPopupTemplate = cruPopupTemplate;
    this.deletePopupTemplate = deletePopupTemplate;
    this.createCrudObjectsBindings(rows);
  }

  createCrudObjectsBindings(rows?: number) {
    let index: number = 0;
    for (var crudOject in this.crudObjectDefinitions) {
      // --> https://stackoverflow.com/questions/8312459/iterate-through-object-properties
      if (this.crudObjectDefinitions.hasOwnProperty(crudOject)) {
        if (this.crudObjectDefinitions[crudOject]) {
          this.crudObjectsLists[crudOject] = [];
          this.crudObjectsListsHasBeenLoaded[crudOject] = false;
          this.crudObjectsLoadings[crudOject] = false;
          this.crudObjectsRows[crudOject] = rows ? rows : 10;
          this.crudObjectsFirst[crudOject] = 0;
          this.crudObjectsTotals[crudOject] = 0;
        }
      }
      index++;
    }
  }

  load(className: string, id ? : number, id2 ? : number): void {
    this.crudObjectsLoadings[className] = true;
    if (id || id2) {
      this.generalService.getUrlWithIdParam(this.crudObjectDefinitions[className].object.structure.apiGetUrl, id ? id.toString() : "null", id2 ? id2.toString() : "null").subscribe(data => {
        this.populateList(data, className);
        this.crudObjectsLoadings[className] = false;
      });
    } else {
      this.generalService.getUrl(this.crudObjectDefinitions[className].object.structure.apiGetUrl).subscribe(data => {
        this.populateList(data, className);
        this.crudObjectsLoadings[className] = false;
      });
    }
  }

  loadFromParamValues(params: ParamValue[], paramArray: any[], paramModule?: ParamModule): void {
    const className = this.findValueFromParamValueArray(params, 'className', false);
    let indexIncrement: number = 1;
    this.crudObjectsLoadings[className] = true;
    this.generalService.getUrlWithParamValues(params).subscribe(data => {
      data.forEach(element => {
        // element = this.completeDTOWithParamArray(paramModule, element, paramArray, indexIncrement);
        element = this.crudObjectDefinitions[className].object.completeDTO(element, indexIncrement);
        indexIncrement++;
      });
      this.populateList(data, className);
      this.crudObjectsLoadings[className] = false;
    });
  }

  lazyLoad(event: any, className: string, callForReal: boolean): void {
    if (callForReal) { // First lazy call is bogous. It makes unexpected 401 errors. This is a trial to fix it.
      setTimeout(() => {
        this.crudObjectsLoadings[className] = true;
        let requestFilters: ParamValue[] = [];
        if (event && event.first != null && event.first != undefined && event.rows) {
          requestFilters.push(
            new ParamValue("first", event.first.toString())
          );
          requestFilters.push(
            new ParamValue("rows", event.rows.toString())
          );
        }
        this.generalService.postUrlWithObject(this.crudObjectDefinitions[className].object.structure.apiGetUrl, requestFilters).subscribe(data => {
          this.crudObjectsTotals[className] = data.count;
          this.populateList(data.result, className);
          this.crudObjectsLoadings[className] = false;
          this.crudObjectsListsHasBeenLoaded[className] = true;
        });
      }, this.crudObjectsListsHasBeenLoaded[className] ? 0 : 200);
    }
  }

  populateList(data: any, className: string, wasLazyCreatedOrDeleted ? : boolean): void {
    this.crudObjectsLists[className] = [];
    for (let objectDTO of data) {
      this.crudObjectsLists[className].push(this.crudObjectDefinitions[className].object.copy(objectDTO));
    }
    if (wasLazyCreatedOrDeleted != undefined && wasLazyCreatedOrDeleted == true) {
      this.crudObjectDefinitions[className].table.reset();
    }
  }

  openEditDialog(object: any, template ? : TemplateRef < any > ) {
    this.editedObject = object.copy(object);
    this.activeDialogRef = this.dialogService.open(
      template ? template : this.cruPopupTemplate
    );
  }

  openCreateDialog(className: string, template ? : TemplateRef < any > ) {
    this.editedObject = this.crudObjectDefinitions[className].object.copy({});
    this.activeDialogRef = this.dialogService.open(
      template ? template : this.cruPopupTemplate
    );
  }

  openDeleteDialog(object: any, template ? : TemplateRef < any > ) {
    this.editedObject = object.copy(object);
    this.activeDialogRef = this.dialogService.open(
      template ? template : this.deletePopupTemplate
    );
  }

  closeActiveDialog() {
    if (this.activeDialogRef) {
      this.activeDialogRef.close();
    }
  }

  onRowEditInit(object: any) {
    this.editedObject = object.copy(object.completeDTO(object, object.ordre));
  }

  onRowEditSave(paramModule: ParamModule) {
    if (paramModule) {
      const objects = [];
      objects.push(JSON.stringify(this.editedObject));
      this.saveObject(objects, paramModule, false, false);
    }else {
      // TODO: A fusionner avec le if du dessus, ne doit plus exister qu'une seule méthode save à terme
      this.saveEditedObject();
    }
  }

  onRowEditCancel() {
    this.editedObject = undefined;
  }

  canEditedObjectBeSaved(): boolean {
    let areAllRequiredFieldOk: boolean = true;
    for (const property of this.editedObject.structure.fields) {
      if (property.required && (
          this.editedObject[property.index] == null ||
          this.editedObject[property.index] == undefined ||
          this.editedObject[property.index] == '')) {
        areAllRequiredFieldOk = false;
        break;
      }
    }
    return areAllRequiredFieldOk;
  }

  saveEditedObject() {
    this.generalService.postUrlWithObject(this.editedObject.structure.apiPostUrl, this.editedObject).subscribe(data => {
        if (this.editedObject.structure.isLazyListed) {
          if (this.editedObject.id) {
            this.updateEditedObjectInList();
          } else {
            this.populateList(data.result, this.editedObject.className, !this.editedObject.id ? true : false);
          }
        } else {
          this.populateList(data, this.editedObject.className);
        }
        this.editedObject = undefined;
        this.closeActiveDialog();
        this.toastrHelper.showDefaultSuccessToast();
      },
      err => {
        this.toastrHelper.showDefaultErrorToast(err.msg);
      });
  }

  updateEditedObjectInList(): void {
    if (this.editedObject.id) {
      let objectFromArray = this.crudObjectsLists[this.editedObject.className].find(o => o.id == this.editedObject.id);
      objectFromArray.populateFromDTO(this.editedObject);
    }
  }

  deleteEditedObject() {
    this.generalService.deleteUrl(this.editedObject.structure.apiDeleteUrl, this.editedObject).subscribe(data => {
        this.populateList(data, this.editedObject.className, this.editedObject.structure.isLazyListed);
        this.editedObject = undefined;
        this.closeActiveDialog();
        this.toastrHelper.showDefaultSuccessToast();
      },
      err => {
        this.toastrHelper.showDefaultErrorToast(err.msg);
      });
  }

  deleteEditedObjectAfter(paramModule: ParamModule, paramArray: any[], indexIncrement: number) {
    // this.editedObject = this.completeDTOWithParamArray(paramModule, this.editedObject, paramArray, null);
    this.editedObject = this.crudObjectDefinitions[paramModule.className].object.completeDTO(this.editedObject, indexIncrement);
    this.generalService.deleteUrlNew(JSON.stringify(this.editedObject)).subscribe(data => {
      this.populateList(data, this.editedObject.className);
      this.editedObject = undefined;
      this.closeActiveDialog();
      this.toastrHelper.showDefaultSuccessToast();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  findValueFromParamValueArray(paramValueArray: ParamValue[], paramToSearch: string, forceBoolean: boolean) {
    const paramValue = paramValueArray.find((x => x.param === paramToSearch));
    const test = paramValue ? forceBoolean ? JSON.parse(paramValue.value) : paramValue.value : null;
    return test;
  }

  completeDTOWithParamArray(module: ParamModule, element: any, paramArray: any[], indexIncrement: number) {
    paramArray.forEach(elementArray => {
      if (elementArray.modules && elementArray.modules.includes(module.libelleTechnique)) {
        if (elementArray.param === 'indexIncrement') {
          element['ordre'] = indexIncrement;
          indexIncrement++;
        }else {
          element[elementArray.param] = elementArray.value;
        }
      }
    });
    return element;
  }

  findCols(param: any) {
    if (param instanceof Object) {
      const paramModule: ParamModule = param;
      return this.crudObjectDefinitions[paramModule.className] ? this.crudObjectDefinitions[paramModule.className].cols : null;
    }else {
      return this.crudObjectDefinitions[param].object.structure.cols;
    }
  }

  onDragRow(paramModule: ParamModule) {
    let index = 1;
    const objects = [];

    this.crudObjectsLists[paramModule.className].forEach(element => {
      element = this.crudObjectDefinitions[paramModule.className].object.completeDTO(element, index);
      index++;
      objects.push(JSON.stringify(element));
    });
    this.saveObject(objects, paramModule, false, false);
  }

  saveObject(objects: any, paramModule: ParamModule, needJsonStringify: boolean, creationObject: boolean) {

    const objectArray = [];
    if (needJsonStringify) {
      if (objects instanceof Array) {
        objects.forEach(element => {
          objectArray.push(JSON.stringify(element));
        });
      }else {
        objectArray.push(JSON.stringify(objects));
      }
    }
    this.generalService.postUrlWithJson(needJsonStringify ? objectArray : objects).subscribe(data => {
      let index: number = 1;
      data.forEach(element => {
        element = this.crudObjectDefinitions[paramModule.className].object.completeDTO(element, index);
        index++;
      });
      this.populateList(data, paramModule.className);
      this.toastrHelper.showDefaultSuccessToast();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  resolveFieldData(rowData, colField){
    return ObjectUtils.resolveFieldData(rowData, colField);
  }
}
