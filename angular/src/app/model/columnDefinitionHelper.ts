export class ColumnDefinitionHelper {

  field: string;
  header: string;
  style?: string;
  editable: boolean;
  columnEditableType?: string;

  constructor(field: string, header: string, editable: boolean, style?: string, columnEditableType?: string) {
    this.field = field;
    this.header =  header;
    this.editable = editable;
    this.columnEditableType = columnEditableType;
    this.style = style;
  }

}
