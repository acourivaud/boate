import { NbDialogService, NbToastrService } from '@nebular/theme';

import { MENU_ITEMS } from '../pages/ouvrage/ouvrage-menu';
import { GeneralService } from '../service/general.service';
import { LocalStorageService } from '../service/localStorage.service';
import { ParamService } from '../service/param.service';
import { UtilisateurService } from '../service/utilisateur.service';
import { ArchiveEmprunt } from './archiveEmprunt';
import { ColumnDefinitionHelper } from './columnDefinitionHelper';
import { CrudHelper } from './crudHelper';
import { FieldBindingHelper } from './FieldBindingHelper';
import { OuvrageProprietaire } from './ouvrageProprietaire';
import { ParamChamp } from './paramChamp';
import { ParamChapitre } from './paramChapitre';
import { ParamLigne } from './paramLigne';
import { ParamLigneDefinition } from './paramLigneDefinition';
import { ParamModule } from './paramModule';
import { ParamObject } from './paramObject';
import { ParamOnglet } from './paramOnglet';
import { ParamTableColumnDefinition } from './paramTableColumnDefinition';
import { ParamTemplate } from './paramTemplate';
import { ParamValue } from './paramValue';
import { ToastrHelper } from './toastrHelper';
import { Utilisateur } from './utilisateur';
import { BackUrlConstant } from '../constant/BackUrlConstant';
import { environment } from '../../environments/environment';

export class PageHelper {

  paramOnglets: ParamOnglet[]; // Définition des onglets de la page
  utilisateur: Utilisateur; // Utilisateur connecté
  toastrHelper: ToastrHelper;
  fieldBindingHelper: FieldBindingHelper;

  crudObjectDefinitions: any = {};
  crudHelper: CrudHelper;
  editedObjectList: any[];
  paramArray: ParamObject[];
  pTableArray: any[] = [];
  titlePage: string; // Titre de la page

  loadingPage: boolean = true;

  constructor(private generalService: GeneralService, private paramService: ParamService, private toastrService: NbToastrService,
    private localStorageService: LocalStorageService, private dialogService: NbDialogService, private utilisateurService: UtilisateurService) {

    this.toastrHelper = new ToastrHelper(toastrService);

  }

  initFromTemplateName(templateName: string) {
    this.initGridTemplate(templateName);
  }

  initGridTemplate(templateName: string) {
    this.crudHelper = new CrudHelper(this.generalService, this.dialogService, this.toastrHelper, this.crudObjectDefinitions, null, null);
    this.editedObjectList = [];
    const objectToBind = [];
    this.paramService.getByTemplateName(templateName).subscribe(templateArray => {
      if (templateArray.length > 0) {
        this.paramOnglets = templateArray[0].paramOnglets;
        templateArray.forEach(templateJson => {
          const template = new ParamTemplate(templateJson.id, templateJson.libelle, templateJson.paramOnglets, templateJson.statut);
          this.paramOnglets = template.paramOnglets;
          template.paramOnglets.forEach(paramOnglet => {
            paramOnglet.paramChapitres.forEach(chapitreJson => {
              const chapitre = new ParamChapitre(
                chapitreJson.id, chapitreJson.position, chapitreJson.libelle, chapitreJson.statut, chapitreJson.specifique, chapitreJson.paramLignes, chapitreJson.paramModule);
              if (chapitre.specifique) {
                this.initModuleSpecifique(chapitre.paramModule);
              } else {
                chapitre.paramLignes.forEach(paramLigneJson => {
                  const paramLigne = new ParamLigne(paramLigneJson.id, paramLigneJson.position, paramLigneJson.paramLigneDefinitions, paramLigneJson.statut);
                  this.initParamLigne(paramLigne, objectToBind);
                });
              }
            });
          });
        });
      }
      if (objectToBind && objectToBind.length > 0) {
        this.fieldBindingHelper.propertiesToBind = objectToBind;
        this.fieldBindingHelper.bindObjectToHtml();
      }
      this.toastrHelper = new ToastrHelper(this.toastrService);
      this.crudHelper.crudObjectDefinitions = this.crudObjectDefinitions;
      this.loadingPage = false;

    });
  }

  initModuleSpecifique(module: ParamModule) {
    if (module.type === 'TABLE') {
      const cols = [];

      if (module.tableColumnDefinitions && module.tableColumnDefinitions.length > 0) {
        module.tableColumnDefinitions.forEach(tableColumnDefinition => {
          const tableColumnDefinitionTemp = new ParamTableColumnDefinition(
            tableColumnDefinition.id,
            tableColumnDefinition.libelleHeader,
            tableColumnDefinition.paramChamp,
            tableColumnDefinition.paramValues);
          cols.push(new ColumnDefinitionHelper(
            tableColumnDefinitionTemp.paramChamp.modelAttribut,
            this.findValueFromParamValueArray(tableColumnDefinitionTemp.paramValues, 'libelleColonne'),
            (this.findValueFromParamValueArray(tableColumnDefinitionTemp.paramValues, 'editable') === 'true'),
            'column-center'));
        });
      }
      this.initObjectArray(module, cols);
    }
  }

  initObjectArray(paramModule: ParamModule, colDefinition: any) {

    switch (paramModule.libelleTechnique) {
      case 'mesEmpruntsModule':
        this.crudObjectDefinitions['ArchiveEmprunt'] = {
          object: new ArchiveEmprunt({}),
          table: null,
          tableParam: paramModule.paramValues,
          cols: colDefinition
        };
        break;
      case 'proprietaireModule':
        this.crudObjectDefinitions['OuvrageProprietaire'] = {
          object: new OuvrageProprietaire({}),
          table: null,
          tableParam: paramModule.paramValues,
          cols: colDefinition
        };
        break;
      default:
        null;
        break;
    }
    this.initObjectTables(paramModule, paramModule.className);
  }

  initObjectTables(paramModule: ParamModule, className: string) {
    const params = [];
    params.push(new ParamValue('className', className));
    params.push(new ParamValue('paramId', this.findObjectFromParamArray(this.paramArray, 'paramId')));
    this.crudHelper.loadFromParamValues(params, this.paramArray, paramModule);
  }

  findObjectFromParamArray(paramObjectArray: ParamObject[], paramToSearch: string) {
    const paramObject = paramObjectArray.find((x => x.param === paramToSearch));
    return paramObject ? paramObject.value : null;
  }

  findValueFromParamValueArray(paramValueArray: ParamValue[], paramToSearch: string) {
    const paramValue = paramValueArray.find((x => x.param === paramToSearch));
    return paramValue ? paramValue.value : null;
  }

  getPermission(typePermission: string): boolean {
    return this.utilisateur.hasPermission(typePermission);
  }

  initParamLigne(paramLigne: ParamLigne, objectToBind: any[]) {
    const paramLigneDefinitions: ParamLigneDefinition[] = paramLigne.paramLigneDefinitions;


    if (paramLigneDefinitions) {
      paramLigneDefinitions.forEach(paramLigneDefinition => {
        objectToBind.push(paramLigneDefinition.paramChamp.modelAttribut);
        if (paramLigneDefinition.paramChamp.type === 'SELECT') {
          // TODO : A implémenter pour les select
          console.warn('initialisation du SELECT pour : ' + paramLigneDefinition.paramChamp.modelAttribut);
          const paramChamp = new ParamChamp(
              paramLigneDefinition.paramChamp.id,
              paramLigneDefinition.paramChamp.structure,
              paramLigneDefinition.paramChamp.type,
              paramLigneDefinition.paramChamp.modelAttribut,
              paramLigneDefinition.paramChamp.requete,
              paramLigneDefinition.paramChamp.libelle,
              paramLigneDefinition.paramChamp.paramChampEnfant
            );

          const requete = paramChamp.requete ? paramChamp.requete : null;

          if (requete) {
            if (requete.includes('MENU_ITEMS')) {
              this.editedObjectList[paramLigneDefinition.paramChamp.modelAttribut] = {
                options: MENU_ITEMS[paramChamp.requete.slice('MENU_ITEMS:'.length, paramChamp.requete.length)],
                selected: this.fieldBindingHelper.object[paramLigneDefinition.paramChamp.modelAttribut]
              };
            }else if (requete.includes('CALL_API')) {
              const methodeCall = requete.split(':')[1];
              this.generalService.getUrl(environment.baseUrl + methodeCall).subscribe(data => {
                this.editedObjectList[paramLigneDefinition.paramChamp.modelAttribut] = {
                  options: data,
                  selected: this.fieldBindingHelper.object[paramLigneDefinition.paramChamp.modelAttribut]
                };
              }, err => {
                this.toastrHelper.showDefaultErrorToast(err.msg);
              });
            }

          }else {
            console.warn('Erreur pour remplir SELECT ' + paramChamp.libelle);
          }
        }
      });
    }
  }

  isActiveTab(formOnglet: ParamOnglet) {
    if (formOnglet.position === 1) {
      return true;
    } else {
      return false;
    }
  }

  getUtilisateurConnected() {
    this.utilisateurService.getUtilisateurConnectedFromBack().subscribe(data => {
      this.utilisateur = new Utilisateur(data);
    });
  }


}
