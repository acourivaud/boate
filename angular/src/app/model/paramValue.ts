export class ParamValue {
	public param: string;
	public value: string;

	constructor(param: string, value: string) {
		this.param = param;
		this.value = value;
	}
}