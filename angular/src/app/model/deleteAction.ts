import { ParamValue } from './paramValue';

export class DeleteAction {
	public idToDelete: number;
	public filtersForReturn: ParamValue[];

	constructor(page: string, filtersForReturn: ParamValue[], idToDelete: number) {
		if (page === undefined) throw new Error('paramètre "page" undefined');
		if (filtersForReturn === undefined) throw new Error('paramètre "filtersForReturn" undefined');
		if (idToDelete === undefined) throw new Error('paramètre "idToDelete" undefined');
		this.filtersForReturn = filtersForReturn;
		this.filtersForReturn.push(new ParamValue('bouclage', page));
		this.idToDelete = idToDelete;
	}
}
