import { ParamChamp } from './paramChamp';

export class ParamLigneDefinition {
  id: number;
  position: number;
  statut: string;
  labelValue: string;
  tailleGridLabel: number;
  tailleGridValue: number;
  paramChamp: ParamChamp;


  constructor(id: number, position: number, statut: string, labelValue: string, tailleGridLabel: number, tailleGridValue: number, paramChamp: ParamChamp) {
    this.id = id;
    this.position = position;
    this.statut = statut;
    this.labelValue = labelValue;
    this.tailleGridLabel = tailleGridLabel;
    this.tailleGridValue = tailleGridValue;
    this.paramChamp = paramChamp;
  }
}
