export class TypeOuvrage {
  id: number;
  nomBoate: string;
  nomPatrimoine: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.nomBoate = dto.nomBoate;
      this.nomPatrimoine = dto.nomPatrimoine;
    } else {
      this.nomBoate = '';
      this.nomPatrimoine = '';
    }
  }
}
