import { IUniversalObject } from './IUniversalObject';
import { BackUrlConstant } from './../constant/BackUrlConstant';
import { Structure } from './structure';

export class Region implements IUniversalObject {
  readonly className: string = "Region";
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.FILTER_REGION,
    apiPostUrl: BackUrlConstant.FILTER_REGION,
    apiDeleteUrl: BackUrlConstant.FILTER_REGION,
    isLazyListed: false,
    popupTitleDescription: "une Région",
    popupChildDelete: true,
    cols: [],
    fields: [{
        index: 'name',
        libelle: 'Nom',
        type: 'input',
        required: true
      },
      {
        index: 'code',
        libelle: 'Code',
        type: 'input',
        required: true
      },
    ]
  };
  id: number;
  code: number;
  name: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
    return new Region(dto);
  }

  completeDTO(dto: any, index: number): Region {
    // Nothing to complete atm
    return dto;
  }

  getSummaryDescription(): string {
    return this.name;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.code = dto.code;
      this.name = dto.name;
    }
  }
}
