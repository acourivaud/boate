import { DateDto } from './dateDto';
import { Utilisateur } from './utilisateur';

export class ReservationCollaborateur {

  idPlanningCollaborateur: Number;

  utilisateur: Utilisateur;

  dateDebut: { dto: string };
  dateFin: { dto: string };

  nuit: Boolean;
	lieu: String;
  type: String;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new ReservationCollaborateur(dto);
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.idPlanningCollaborateur = dto.idPlanningCollaborateur;
      this.utilisateur = new Utilisateur(dto.utilisateur);
      this.dateDebut = dto.dateDebut ? new DateDto(dto.dateDebut).toBackEndDto() : null;
      this.dateFin = dto.dateFin ? new DateDto(dto.dateFin).toBackEndDto() : null;
      this.nuit = dto.nuit;
      this.lieu = dto.lieu;
      this.type = dto.type;
    }
  }

  getHoursTotalForDate(date: any): number {
    const dateDebut = new DateDto(this.dateDebut).toDate();
		const dateFin = new DateDto(this.dateFin).toDate();
    // "date: any" is the object from PrimeNg Calendar
		const compareDateDebut = new Date(date.year, date.month, date.day, 0, 0, 0);
    const compareDateFin = new Date(date.year, date.month, date.day, 23, 59, 59);
    if (dateDebut > compareDateFin) return 0;
    if (dateFin < compareDateDebut) return 0;
    let heureDebut: number;
    if (dateDebut < compareDateDebut) {
      heureDebut = 0;
    } else {
      heureDebut = dateDebut.getHours();
    }
    let heureFin: number;
    if (dateFin > compareDateFin) {
      heureFin = 24;
    } else {
      // +1 because if the dateFin time is for example 3:59 we want to count 4
      heureFin = dateFin.getHours() + 1;
    }
    return heureFin - heureDebut;
  }

  getSummaryDescription(): string {
    const dateDebut = new DateDto(this.dateDebut).toDate();
		const dateFin = new DateDto(this.dateFin).toDate();
    return '[ du ' + dateDebut.toLocaleDateString() + ' à ' + dateDebut.getHours() + 'H au ' +
              dateFin.toLocaleDateString() + ' à ' + (dateFin.getHours() + 1) + 'H ] > ' +
              this.type + ' - ' + this.lieu + ' - ' + (this.nuit === true ? 'NUIT' : 'JOUR');
  }

  static populateFromArrayDTO(dto: any[]): ReservationCollaborateur[] {
    const reservationsCollaborateur: ReservationCollaborateur[] = [];
    if (dto && dto.length > 0) {
      for (const subDto of dto) {
        reservationsCollaborateur.push(new ReservationCollaborateur(subDto));
      }
    }
    return reservationsCollaborateur;
  }
}
