import { IUniversalObject } from './IUniversalObject';
import { BackUrlConstant } from './../constant/BackUrlConstant';
import { Infrapole } from './infrapole';
import { Structure } from './structure';

export class Secteur implements IUniversalObject {
  readonly className: string = "Secteur";
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.FILTER_SECTEUR,
    apiPostUrl: BackUrlConstant.FILTER_SECTEUR,
    apiDeleteUrl: BackUrlConstant.FILTER_SECTEUR,
    isLazyListed: false,
    popupTitleDescription: "un Secteur",
    popupChildDelete: true,
    cols: [],
    fields: [{
      index: 'nom',
      libelle: 'Nom',
      type: 'input',
      required: true
    }, ]
  };
  id: number;
  nom: string;
  infrapole: Infrapole;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
    return new Secteur(dto);
  }

  completeDTO(dto: any, index: number): Secteur {
    // Nothing to complete atm
    return dto;
  }

  getSummaryDescription(): string {
    return this.nom;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.nom = dto.nom;
      this.infrapole = dto.infrapole ? new Infrapole(dto.infrapole) : null;
    }
  }
}
