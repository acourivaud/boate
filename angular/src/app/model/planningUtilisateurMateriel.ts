import { Materiel } from './materiel';
import { DateDto } from './dateDto';
import { Utilisateur } from './utilisateur';
import { ReservationMaterielUtilisateur } from './reservationMaterielUtilisateur';

export class PlanningUtilisateurMateriel {

  // True if this object is used in a request to get all Users having booked a specific Vehicle.
	// False if this object is used in a request to get all Vehicles booked by a User.
	// Avoid loading the grouping object multiple times in loop (whether it's a User or Vehicle).
  trueIfListOfUsersFalseIfListOfMateriels: boolean;

  utilisateur: Utilisateur;
  materiel: Materiel;
  reservationMaterielUtilisateur: ReservationMaterielUtilisateur[];

  // Début et fin de la plage de requête.
  dateDebut: { dto: string };
  dateFin: { dto: string };

	// Used to show the popup when the requested reservation is in conflict with an other existing one.
	// If that's the case, then the result will contain the incriminated reservation only instead of the full list after update.
	// Conflict on user is non-blocking.
	// Conflict on vehicle is BLOCKING.
	conflictOnUserResponse: boolean = false;
	conflictOnMaterielResponse: boolean = false;

	// If set to true by the front, then authorize reservation for the same user, the same date and several vehicles.
	forceUpdateIfPossible: boolean = false;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new PlanningUtilisateurMateriel(dto);
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.trueIfListOfUsersFalseIfListOfMateriels = dto.trueIfListOfUsersFalseIfListOfMateriels;
      if (this.trueIfListOfUsersFalseIfListOfMateriels === true) {
        // This is reversed in the sub-listed object ReservationMaterielUtilisateur.
				// Here is the object we don't want to load multiple times in loop.
        this.materiel = new Materiel(dto.materiel);
      } else {
        // This is reversed in the sub-listed object ReservationMaterielUtilisateur.
				// Here is the object we don't want to load multiple times in loop.
        this.utilisateur = new Utilisateur(dto.utilisateur);
      }
      this.reservationMaterielUtilisateur = ReservationMaterielUtilisateur.populateFromArrayDTO(dto.reservationMaterielUtilisateur);
      this.dateDebut = dto.dateDebut ? new DateDto(dto.dateDebut).toBackEndDto() : null;
      this.dateFin = dto.dateFin ? new DateDto(dto.dateFin).toBackEndDto() : null;
      this.conflictOnUserResponse = dto.conflictOnUserResponse;
      this.conflictOnMaterielResponse = dto.conflictOnMaterielResponse;
      this.forceUpdateIfPossible = dto.forceUpdateIfPossible;
    }
  }
}
