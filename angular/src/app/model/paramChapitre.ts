import { ParamLigne } from './paramLigne';
import { ParamModule } from './paramModule';

export class ParamChapitre {
  id: number;
  position: number;
  libelle: string;
  statut: string;
  specifique: boolean;
  paramLignes?: ParamLigne[];
  paramModule?: ParamModule;


  constructor(id: number, position: number, libelle: string, statut: string, specifique: boolean, paramLignes?: ParamLigne[], paramModule?: ParamModule) {
    this.id = id;
    this.position = position;
    this.libelle = libelle;
    this.statut = statut;
    this.specifique = specifique;
    this.paramLignes = paramLignes;
    this.paramModule = paramModule;
  }
}
