import { Vehicule } from './vehicule';
import { DateDto } from './dateDto';
import { Utilisateur } from './utilisateur';

export class ReservationVehiculeUtilisateur {

  idPlanningVehicule: number;

  // True if this object is used in a request to get all Users having booked a specific Vehicle.
	// False if this object is used in a request to get all Vehicles booked by a User.
	// Avoid loading the grouping object multiple times in loop (whether it's a User or Vehicle).
  trueIfListOfUsersFalseIfListOfVehicules: boolean;

	vehicule: Vehicule;
	utilisateur: Utilisateur;
  dateDebut: { dto: string };
  dateFin: { dto: string };

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new ReservationVehiculeUtilisateur(dto);
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.idPlanningVehicule = dto.idPlanningVehicule;
      this.trueIfListOfUsersFalseIfListOfVehicules = dto.trueIfListOfUsersFalseIfListOfVehicules;
      if (this.trueIfListOfUsersFalseIfListOfVehicules === true) {
        this.utilisateur = new Utilisateur(dto.utilisateur);
      } else {
        this.vehicule = new Vehicule(dto.vehicule);
      }
      this.dateDebut = dto.dateDebut ? new DateDto(dto.dateDebut).toBackEndDto() : null;
      this.dateFin = dto.dateFin ? new DateDto(dto.dateFin).toBackEndDto() : null;
    }
  }

  getHoursTotalForDate(date: any): number {
    const dateDebut = new DateDto(this.dateDebut).toDate();
		const dateFin = new DateDto(this.dateFin).toDate();
		// "date: any" is the object from PrimeNg Calendar
		const compareDateDebut = new Date(date.year, date.month, date.day, 0, 0, 0);
    const compareDateFin = new Date(date.year, date.month, date.day, 23, 59, 59);
    if (dateDebut > compareDateFin) return 0;
    if (dateFin < compareDateDebut) return 0;
    let heureDebut: number;
    if (dateDebut < compareDateDebut) {
      heureDebut = 0;
    } else {
      heureDebut = dateDebut.getHours();
    }
    let heureFin: number;
    if (dateFin > compareDateFin) {
      heureFin = 24;
    } else {
      // +1 because if the dateFin time is for example 3:59 we want to count 4
      heureFin = dateFin.getHours() + 1;
    }
    return heureFin - heureDebut;
  }

  getSummaryDescription(): string {
    const dateDebut = new DateDto(this.dateDebut).toDate();
    const dateFin = new DateDto(this.dateFin).toDate();
    const returnName = this.trueIfListOfUsersFalseIfListOfVehicules ? this.utilisateur.getSummaryDescription() : this.vehicule.getSummaryDescription();
    return '[ du ' + dateDebut.toLocaleDateString() + ' à ' + dateDebut.getHours() + 'H au ' +
              dateFin.toLocaleDateString() + ' à ' + (dateFin.getHours() + 1) + 'H ] > ' + returnName;
  }

  static populateFromArrayDTO(dto: any[]): ReservationVehiculeUtilisateur[] {
    const reservationVehiculeUtilisateur: ReservationVehiculeUtilisateur[] = [];
    if (dto && dto.length > 0) {
      for (const subDto of dto) {
        reservationVehiculeUtilisateur.push(new ReservationVehiculeUtilisateur(subDto));
      }
    }
    return reservationVehiculeUtilisateur;
  }
}
