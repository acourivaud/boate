export class Departement {
  id: number;
  nom: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.nom = dto.nom;
    }
  }
}
