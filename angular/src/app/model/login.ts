export class Login {
  username: string;
  password: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.username = dto.username;
      this.password = dto.password;
    }
  }
}
