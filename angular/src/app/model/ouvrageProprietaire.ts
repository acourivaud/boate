import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { BackUrlConstant } from '../constant/BackUrlConstant';
import { MethodeCallBack } from './methodeCallBack';
import { Ouvrage } from './ouvrage';
import { Proprietaire } from './proprietaire';
import { IUniversalObject } from './IUniversalObject';
import { Structure } from './structure';

export class OuvrageProprietaire implements IUniversalObject {
  id: number;

  ouvrage: Ouvrage;
  proprietaire: Proprietaire;
  ordre: number;
  pourcentage: number;

  readonly className: string = 'OuvrageProprietaire';
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.PROPRIETAIRE_OUVRAGE,
    apiPostUrl: BackUrlConstant.PROPRIETAIRE_OUVRAGE,
    apiDeleteUrl: BackUrlConstant.PROPRIETAIRE_OUVRAGE,
    isLazyListed: false,
    popupTitleDescription: 'un Propriétaire d\'Ouvrage',
    popupChildDelete: false,
    cols: [],
    fields: [{
        index: 'proprietaire',
        type: 'select',
        libelle: 'Propriétaire',
        selectOptionsUrl: BackUrlConstant.PROPRIETAIRE_DISPONIBLE,
        methodUrlCall: MethodeCallBack.GET_WITH_PARAM_ID,
        paramId: 'ouvrageid',
        required: true,
      },
      {
        index: 'pourcentage',
        type: 'spinner',
        libelle: 'Pourcentage',
        spinnerMin: 0,
        spinnerMax: 100,
        spinnerStep: 5,
        required: true,
      },
    ],
  };

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
    return new OuvrageProprietaire(dto);
  }

  completeDTO(dto: any, index: number): OuvrageProprietaire {
    dto.ordre = index ? index : 0;
    dto.ouvrage = JSON.parse(localStorage.getItem('BOATE_currentOuvrage'));
    return dto;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.ordre = dto.ordre ? dto.ordre : 0;
      this.ouvrage = dto.ouvrage;
      this.pourcentage = dto.pourcentage;
      this.proprietaire = dto.proprietaire;
    }
  }

  getSummaryDescription(): string {
    return this.ouvrage.name + ' - ' + this.proprietaire.nom;
  }
}
