import { DateDto } from './dateDto';
import { Ouvrage } from './ouvrage';

export class Surveillance {
  id: number;
  ouvrage: Ouvrage;
  compteAnalytique: string;
  codeSegment: string;
  moyensSurveillance: string;
  cv: string;

  dateDebut: Date;
  dateFin: Date;

  dateReclassement: Date;
  motifReclassement: string;

  archive: boolean;

  periodiciteId: number;
  lastDateId: DateDto;
  auteurId: string;
  nextDateId: number;

  periodiciteVd: number;
  lastDateVd: DateDto;
  auteurVd: string;
  nextDateVd: number;

  periodiciteVi: number;
  lastDateVi: DateDto;
  auteurVi: string;
  nextDateVi: number;

  periodiciteVs: number;
  lastDateVs: DateDto;
  auteurVs: string;
  nextDateVs: number;

  periodiciteVdOt: number;
  lastDateVdOt: DateDto;
  auteurVdOt: string;
  nextDateVdOt: number;

  periodiciteViOt: number;
  lastDateViOt: DateDto;
  auteurViOt: string;
  nextDateViOt: number;

  periodiciteVpOt: number;
  lastDateVpOt: DateDto;
  auteurVpOt: string;
  nextDateVpOt: number;

  periodiciteVaOt: number;
  lastDateVaOt: DateDto;
  auteurVaOt: string;
  nextDateVaOt: number;

  complementaryVisit: string;

  visiteN0: string;
  visiteN1: string;
  visiteN2: string;
  visiteN3: string;
  visiteN4: string;
  visiteN5: string;
  visiteN6: string;
  visiteN7: string;
  visiteN8: string;
  visiteN9: string;

  firstMeansKey: string;
	secondMeansKey: string;
  thirdMeansKey: string;

  typeSurveillance: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.ouvrage = dto.ouvrage ? new Ouvrage(dto.ouvrage) : null;
      this.compteAnalytique = dto.compteAnalytique;
      this.codeSegment = dto.codeSegment;
      this.moyensSurveillance = dto.moyensSurveillance;
      this.cv = dto.cv;

      this.dateDebut = dto.dateDebut ? new Date(dto.dateDebut) : undefined;
      this.dateFin = dto.dateFin ? new Date(dto.dateFin) : undefined;
      this.dateReclassement = dto.dateReclassement ? new Date(dto.dateReclassement) : undefined;
      this.motifReclassement = dto.motifReclassement;

      this.periodiciteId = dto.periodiciteId;
      this.lastDateId = dto.lastDateId;
      this.auteurId = dto.auteurId;
      this.nextDateId = dto.nextDateId;

      this.periodiciteVd = dto.periodiciteVd;
      this.lastDateVd = dto.lastDateVd;
      this.auteurVd = dto.auteurVd;
      this.nextDateVd = dto.nextDateVd;

      this.periodiciteVi = dto.periodiciteVi;
      this.lastDateVi = dto.lastDateVi;
      this.auteurVi = dto.auteurVi;
      this.nextDateVi = dto.nextDateVi;

      this.periodiciteVs = dto.periodiciteVs;
      this.lastDateVs = dto.lastDateVs;
      this.auteurVs = dto.auteurVs;
      this.nextDateVs = dto.nextDateVs;

      this.periodiciteVdOt = dto.periodiciteVdOt;
      this.lastDateVdOt = dto.lastDateVdOt;
      this.auteurVdOt = dto.auteurVdOt;
      this.nextDateVdOt = dto.nextDateVdOt;

      this.periodiciteViOt = dto.periodiciteViOt;
      this.lastDateViOt = dto.lastDateViOt;
      this.auteurViOt = dto.auteurViOt;
      this.nextDateViOt = dto.nextDateViOt;

      this.periodiciteVpOt = dto.periodiciteVpOt;
      this.lastDateVpOt = dto.lastDateVpOt;
      this.auteurVpOt = dto.auteurVpOt;
      this.nextDateVpOt = dto.nextDateVpOt;

      this.periodiciteVaOt = dto.periodiciteVaOt;
      this.lastDateVaOt = dto.lastDateVaOt;
      this.auteurVaOt = dto.auteurVaOt;
      this.nextDateVaOt = dto.nextDateVaOt;
      this.periodiciteVaOt = dto.periodiciteVaOt;

      this.complementaryVisit = dto.complementaryVisit;

      this.visiteN0 = dto.visiteN0;
      this.visiteN1 = dto.visiteN1;
      this.visiteN2 = dto.visiteN2;
      this.visiteN3 = dto.visiteN3;
      this.visiteN4 = dto.visiteN4;
      this.visiteN5 = dto.visiteN5;
      this.visiteN6 = dto.visiteN6;
      this.visiteN7 = dto.visiteN7;
      this.visiteN8 = dto.visiteN8;
      this.visiteN9 = dto.visiteN9;

      this.firstMeansKey = dto.firstMeansKey;
      this.secondMeansKey = dto.secondMeansKey;
      this.thirdMeansKey = dto.thirdMeansKey;

      this.typeSurveillance = dto.typeSurveillance;
    }
  }
}
