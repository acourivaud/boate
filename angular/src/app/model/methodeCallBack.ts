export class MethodeCallBack {

  static GET = 'GET';
  static GET_WITH_PARAM_ID = 'GET_WITH_PARAM_ID';
  static GET_WITH_OBJECT = 'GET_WITH_OBJECT';
  static GET_WITH_OBJECT_ARRAY = 'GET_WITH_OBJECT_ARRAY';


}
