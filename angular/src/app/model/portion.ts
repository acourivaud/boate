import { IUniversalObject } from './IUniversalObject';
import { BackUrlConstant } from './../constant/BackUrlConstant';
import { UniteOperationnelle } from './uniteOperationnelle';
import { Ligne } from './ligne';
import { Structure } from './structure';

export class Portion implements IUniversalObject {
  readonly className: string = 'Portion';
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.FILTER_PORTION,
    apiPostUrl: BackUrlConstant.FILTER_PORTION,
    apiDeleteUrl: BackUrlConstant.FILTER_PORTION,
    popupTitleDescription: "une Portion",
    popupChildDelete: true,
    cols: [],
    fields:[
      {
        index: 'ligne',
        libelle: 'Ligne',
        type: 'autocomplete',
        suggestionField: 'nom',
        required: true,
      },
      {
        index: 'pkDebut',
        libelle: 'PK Début',
        type: 'input',
        required: true,
      },
      {
        index: 'pkFin',
        libelle: 'PK Fin',
        type: 'input',
        required: true,
      },
      {
        index: 'voie',
        libelle: 'Voie',
        type: 'input',
        required: true,
      },
    ]
  };
  id: number;
  ligne: Ligne;
  uo: UniteOperationnelle;
  pkDebut: string;
  pkFin: string;
  voie: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new Portion(dto);
  }
  
  completeDTO(dto: any, index: number): Portion {
    // Nothing to complete atm
    return dto;
  }

  getSummaryDescription(): string {
    return this.ligne + ' - [' + this.pkDebut + '>' + this.pkFin + ']';
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.ligne = dto.ligne ? new Ligne(dto.ligne) : undefined;
      this.uo = dto.uo ? new UniteOperationnelle(dto.uo) : undefined;
      this.pkDebut = dto.pkDebut;
      this.pkFin = dto.pkFin;
      this.voie = dto.voie;
    }
  }
}
