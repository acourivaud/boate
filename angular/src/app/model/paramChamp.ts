import { Structure } from './structure';

export class ParamChamp {
  id: number;
  structure: Structure;
  type: string;
  modelAttribut: string;
  requete: string;
  libelle: string;
  paramChampEnfant?: ParamChamp;

  constructor(id: number, structure: Structure, type: string, modelAttribut: string, requete: string, libelle: string, paramChampEnfant?: ParamChamp) {
    this.id = id;
    this.structure = structure;
    this.type = type;
    this.modelAttribut = modelAttribut;
    this.requete = requete;
    this.libelle = libelle;
    this.paramChampEnfant = paramChampEnfant;
  }
}
