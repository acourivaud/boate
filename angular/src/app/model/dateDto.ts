export class DateDto {
	public year: number;
	public month: number;
	public day: number;
	public hour: number = 0;
	public minute: number = 0;
	public second: number = 0;

	constructor(dto: any) {
		this.populateFromDTO(dto);
	}

	populateFromDTO(dto: any) {
		if (dto && dto.dto) {
			const split: string[] = dto.dto.split('|');
			this.year = Number.parseInt(split[0]);
			this.month = Number.parseInt(split[1]);
			this.day = Number.parseInt(split[2]);
			if (split.length > 3) {
				this.hour = Number.parseInt(split[3]);
			}
			if (split.length > 4) {
				this.minute = Number.parseInt(split[4]);
			}
			if (split.length > 5) {
				this.second = Number.parseInt(split[5]);
			}
		}
	}

	toDate(): Date {
		return this.year && this.month && this.day ? new Date(this.year, this.month - 1, this.day, this.hour, this.minute, this.second) : undefined;
	}

	toBackEndDto(): { dto: string } {
		return {
			dto:
				this.year.toString() + '|' +
				this.month.toString() + '|' +
				this.day.toString()  + '|' +
				this.hour.toString()  + '|' +
				this.minute.toString()  + '|' +
				this.second.toString()
		};
	}

	static isSameDay(dto: { dto: string }, date: Date): boolean {
		const dateDto = new DateDto(dto);
		if (date.getFullYear() !== dateDto.year) return false;
		if ((date.getMonth() + 1) !== dateDto.month) return false;
		if (date.getDate() !== dateDto.day) return false;
		return true;
	}

	static createFromDate(date: Date): DateDto {
		const fakeDto = {
			dto:
				date.getFullYear().toString() + '|' +
				(date.getMonth() + 1).toString() + '|' +
				date.getDate().toString() + '|' +
				date.getHours().toString() + '|' +
				date.getMinutes().toString() + '|' +
				date.getSeconds().toString()
		};
		return new DateDto(fakeDto);
	}
}