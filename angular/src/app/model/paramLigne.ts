import { ParamLigneDefinition } from './paramLigneDefinition';

export class ParamLigne {
  id: number;
  position: number;
  statut: string;
  paramLigneDefinitions: ParamLigneDefinition[];


  constructor(id: number, position: number, paramLigneDefinitions: ParamLigneDefinition[], statut: string) {
    this.id = id;
    this.position = position;
    this.paramLigneDefinitions = paramLigneDefinitions;
    this.statut = statut;
  }
}
