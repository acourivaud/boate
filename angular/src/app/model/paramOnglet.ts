import { ParamChapitre } from './paramChapitre';

export class ParamOnglet {
  id: number;
  position: number;
  libelle: string;
  paramChapitres: ParamChapitre[];
  statut: string;

  constructor(id: number, position: number, libelle: string, paramChapitres: ParamChapitre[], statut: string) {
    this.id = id;
    this.position = position;
    this.libelle = libelle;
    this.paramChapitres = paramChapitres;
    this.statut = statut;
  }
}
