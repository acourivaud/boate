export class Connexion {
	public id: number;
	public login: string;
	public heure: string;
	public date: Date;

	constructor(dto: any) {
		this.populateFromDTO(dto);
	}

	populateFromDTO(dto: any) {
		if (dto) {
			this.id = dto.id;
			this.login = dto.login;
			this.heure = dto.heure;
			this.date = dto.date ? new Date(dto.date) : null;
		}
	}
}
