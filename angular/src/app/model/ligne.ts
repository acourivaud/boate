import { IUniversalObject } from './IUniversalObject';
import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { BackUrlConstant } from './../constant/BackUrlConstant';
import { Structure } from './structure';

export class Ligne implements IUniversalObject {
  readonly className: string = 'Ligne';
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.FILTER_LIGNE,
    apiPostUrl: BackUrlConstant.FILTER_LIGNE_SAVE,
    apiDeleteUrl: BackUrlConstant.FILTER_LIGNE,
    isLazyListed: true,
    popupTitleDescription: 'une Ligne',
    popupChildDelete: true,
    cols: [
      { field: 'nom', header: 'Nom', style: 'column-width-300', editable: true },
      { field: 'code', header: 'Code', style: 'column-center column-width-100', editable: true },
      { field: 'pkDebut', header: 'PK Début', style: 'column-center column-width-100', editable: true },
      { field: 'pkFin', header: 'PK Fin', style: 'column-center column-width-100', editable: true },
    ],
    fields:[
      {
        index: 'nom',
        libelle: 'Nom',
        type: 'input',
        required: true,
      },
      {
        index: 'code',
        libelle: 'Code',
        type: 'input',
        required: true,
      },
      {
        index: 'pkDebut',
        libelle: 'PK Début',
        type: 'input',
        required: true,
      },
      {
        index: 'pkFin',
        libelle: 'PK Fin',
        type: 'input',
        required: true,
      },
    ]
  };

  id: number;
  code: string;
  nom: string;
  pkDebut: string;
  pkFin: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
    return new Ligne(dto);
  }

  completeDTO(dto: any, index: number): Ligne {
    // Nothing to complete atm
    return dto;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.code = dto.code;
      this.nom = dto.nom;
      this.pkDebut = dto.pkDebut;
      this.pkFin = dto.pkFin;
      if (dto.portions) {
        this['portions'] = dto.portions;
      }
    }
  }

  getSummaryDescription(): string {
    return this.code + ' - ' + this.nom;
  }
}
