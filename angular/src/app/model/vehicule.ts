import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { IUniversalObject } from './IUniversalObject';
import { BackUrlConstant } from './../constant/BackUrlConstant';
import { Structure } from './structure';

export class Vehicule implements IUniversalObject {
  readonly className: string = 'Vehicule';
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.VEHICULE,
    apiPostUrl: BackUrlConstant.VEHICULE,
    apiDeleteUrl: BackUrlConstant.VEHICULE,
    isLazyListed: false,
    popupTitleDescription: 'un Véhicule',
    popupChildDelete: false,
    cols: [
      { field: 'marque', header: 'Marque', style: 'column-center', editable: true },
      { field: 'modele', header: 'Modèle', style: 'column-center', editable: true },
      { field: 'immatriculation', header: 'Immatriculation', style: 'column-center', editable: true },
      { field: 'places', header: 'Nb Places', style: 'column-center', editable: true }
    ],
    fields: [
      { index: 'marque', libelle: 'Marque', type: 'input', required: true },
      { index: 'modele', libelle: 'Modèle', type: 'input', required: true },
      { index: 'immatriculation', libelle: 'Immatriculation', type: 'input', required: true },
      { index: 'places', libelle: 'Nb Places', type: 'input', required: true }
    ]
  };

  id: number;
  marque: string;
  modele: string;
  immatriculation: string;
  places: number;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new Vehicule(dto);
  }

  completeDTO(dto: any, index: number): Vehicule {
    // Nothing to complete atm
    return dto;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.marque = dto.marque;
      this.modele = dto.modele;
      this.immatriculation = dto.immatriculation;
      this.places = dto.places;
    }
  }

  getSummaryDescription(): string {
    return this.immatriculation + ' - ' + this.marque + ' - ' + this.modele + ' - ' + this.places + ' places';
  }
}
