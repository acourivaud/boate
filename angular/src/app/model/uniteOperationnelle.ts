import { IUniversalObject } from './IUniversalObject';
import { BackUrlConstant } from './../constant/BackUrlConstant';
import { Infrapole } from './infrapole';
import { Secteur } from './secteur';
import { Structure } from './structure';

export class UniteOperationnelle implements IUniversalObject {
  readonly className: string = 'UniteOperationnelle';
  readonly structure: Structure =  {
    apiGetUrl: BackUrlConstant.FILTER_UO,
    apiPostUrl: BackUrlConstant.FILTER_UO,
    apiDeleteUrl: BackUrlConstant.FILTER_UO,
    isLazyListed: false,
    popupTitleDescription: "une Unité Opérationnelle",
    popupChildDelete: true,
    cols: [],
    fields: [
      {
        index: 'name',
        libelle: 'Nom',
        type: 'input',
        required: true,
      },
      {
        index: 'code',
        libelle: 'Code',
        type: 'input',
        required: true,
      },
    ]
  };
  id: number;
  code: string;
  name: string;
  infrapole: Infrapole;
  secteur: Secteur;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new UniteOperationnelle(dto);
  }
  
  completeDTO(dto: any, index: number): UniteOperationnelle {
    // Nothing to complete atm
    return dto;
  }

  getSummaryDescription(): string {
    return this.name;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.code = dto.code;
      this.name = dto.name;
      this.infrapole = dto.infrapole ? new Infrapole(dto.infrapole) : null;
      this.secteur = dto.secteur ? new Secteur(dto.secteur) : null;
    }
  }
}
