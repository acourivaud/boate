import { ParamChamp } from './paramChamp';
import { ParamValue } from './paramValue';

export class ParamTableColumnDefinition {
  id: number;
  libelleHeader: string;
  paramChamp: ParamChamp;
  paramValues: ParamValue[];

  constructor(id: number, libelleHeader: string, paramChamp: ParamChamp, paramValues?: ParamValue[]) {
    this.id = id;
    this.libelleHeader = libelleHeader;
    this.paramChamp = paramChamp;
    this.paramValues = paramValues;
  }
}
