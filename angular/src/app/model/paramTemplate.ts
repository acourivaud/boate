import { ParamChapitre } from './paramChapitre';
import { ParamOnglet } from './paramOnglet';

export class ParamTemplate {
  id: number;
  libelle: string;
  paramOnglets: ParamOnglet[];
  statut: string;

  constructor(id: number, libelle: string, paramOnglets: ParamOnglet[], statut: string) {
    this.id = id;
    this.libelle = libelle;
    this.paramOnglets = paramOnglets;
    this.statut = statut;
  }
}
