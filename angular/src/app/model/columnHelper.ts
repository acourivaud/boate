import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { SelectItem } from 'primeng/components/common/selectitem';

import { ColumnService } from './../service/column.service';
import { ParamValue } from './paramValue';
import { ColumnDefinitionHelper } from './columnDefinitionHelper';

export class ColumnHelper {

  cols: any[];
  conditionalCols: any[];
  activeConditions: string[];
  optionalCols: {
    optionalColsName: string,
    optionalCols: SelectItem[],
  } [];
  activeOptionalCols: {
    optionalColsName: string,
    optionalCols: SelectItem[],
  };
  optionalColsAreAllSelectedByDefault: boolean = false;
  usesGroupsForOptionalCols: boolean = true;
  orderedColumns: string[];
  selectedOptionalCols: any[];
  selectedOptionalColsHasPendingChanges: boolean = false;
  columnService: ColumnService;
  page: string;

  constructor(conditionalCols: any[],
    optionalCols: {
      optionalColsName: string,
      optionalCols: SelectItem[],
    } [],
    columnService: ColumnService,
    page: string,
    optionalColsAreAllSelectedByDefault?: boolean,
    usesGroupsForOptionalCols?: boolean) {
    this.conditionalCols = conditionalCols;
    this.optionalCols = optionalCols;
    if (this.optionalCols && this.optionalCols != null) {
      this.activeOptionalCols = this.optionalCols[0];
    }
    this.columnService = columnService;
    this.page = page;
    this.selectedOptionalCols = [];
    this.populateAllPossibleColumns();
    this.adaptConditionalColumnsToRequest();
    if (optionalColsAreAllSelectedByDefault) {
      this.optionalColsAreAllSelectedByDefault = optionalColsAreAllSelectedByDefault;
    }
    if (usesGroupsForOptionalCols != undefined) {
      this.usesGroupsForOptionalCols = usesGroupsForOptionalCols;
    }
  }

  setOptionalCols(optionalColsName: string): void {

    if (this.optionalCols && this.optionalCols != null) {
      this.selectedOptionalCols = [];

      let activeOptionalCols = this.optionalCols.find(oc => oc.optionalColsName == optionalColsName);

      this.activeOptionalCols = activeOptionalCols;

      this.columnService.getColumnsForConnectedUser(this.page).subscribe(data => {

        let selectedOptionalCols: any[] = [];

        if (data && data != null && data.length > 0) {
          for (let paramValueDto of data) {

            if (paramValueDto.param == this.activeOptionalCols.optionalColsName) {

              let colsIdsArray: string[] = paramValueDto.value.split("|");

              for (let optionalCol of this.activeOptionalCols.optionalCols) {
                if (colsIdsArray.indexOf(optionalCol.value.id.toString()) > -1) {
                  selectedOptionalCols.push(optionalCol.value);
                }
              }

              break;

            }

          }
        } else {
          if (this.optionalColsAreAllSelectedByDefault) {
            for (let optionalCol of this.activeOptionalCols.optionalCols) {
              selectedOptionalCols.push(optionalCol.value);
            }
          }
        }

        this.selectedOptionalCols = selectedOptionalCols;

        this.adaptConditionalColumnsToRequest();

        for (let optionalCol of this.activeOptionalCols.optionalCols) {
          let selectedOptionalCol = this.selectedOptionalCols.find(soc => soc.id == optionalCol.value.id);
          if (selectedOptionalCol && selectedOptionalCol.col && selectedOptionalCol.col != null) {
            this.cols.push(selectedOptionalCol.col);
          }
        }

        this.applyColumnsOrder();

      });
    }

  }

  onOptionalColumnsChange($event): void {

    if (this.activeOptionalCols.optionalCols && this.activeOptionalCols.optionalCols != null && this.activeOptionalCols.optionalCols.length > 0) {

      let selectedOptionalCols: any[] = [];

      if ($event.itemValue) {
        if ($event.value.find(x => x.id == $event.itemValue.id)) { // Ajout
          selectedOptionalCols = $event.value;
          if ($event.itemValue.children != null) {
            for (let childId of $event.itemValue.children) {
              if (!selectedOptionalCols.find(oc => oc.id == childId)) {
                selectedOptionalCols.push(this.activeOptionalCols.optionalCols.find(oc => oc.value.id == childId).value);
              }
            }
          } else {
            if (this.usesGroupsForOptionalCols) {
              let parentOfChild = this.activeOptionalCols.optionalCols.find(oc => oc.value.children != null && oc.value.children.find(id => id == $event.itemValue.id)).value;
              let allChildAreSelected = true;
              for (let id of parentOfChild.children) {
                if (!selectedOptionalCols.find(x => x.id == id)) {
                  allChildAreSelected = false;
                  break;
                }
              }
              if (allChildAreSelected) {
                selectedOptionalCols.push(parentOfChild);
              }
            }
          }
        } else { // Suppression
          if ($event.itemValue.children != null) {
            for (let val of $event.value) {
              if (!$event.itemValue.children.find(id => id == val.id)) {
                selectedOptionalCols.push(val);
              }
            }
          } else {
            for (let val of $event.value) {
              if (val.children == null || !val.children.find(id => id == $event.itemValue.id)) {
                selectedOptionalCols.push(val);
              }
            }
          }
        }
      } else {
        selectedOptionalCols = $event.value;
      }

      this.selectedOptionalCols = selectedOptionalCols;

      this.adaptConditionalColumnsToRequest();

      for (const optionalCol of this.activeOptionalCols.optionalCols) {
        const selectedOptionalCol = this.selectedOptionalCols.find(soc => soc.id == optionalCol.value.id);
        if (selectedOptionalCol && selectedOptionalCol.col && selectedOptionalCol.col != null) {
          const testSelectedOptioncalColExist = this.cols.filter(c => c.field === optionalCol.value.col.field);
          if (!testSelectedOptioncalColExist) {
            this.cols.push(selectedOptionalCol.col);
          }
        }else {
          this.cols = this.cols.filter(c => c.field !== optionalCol.value.col.field);
        }
      }

      this.applyColumnsOrder();

    }

    this.selectedOptionalColsHasPendingChanges = true;
  }

  saveSelectedOptionalCols(): void {
    if (this.selectedOptionalColsHasPendingChanges) {
      let columns: string = "null";
      if (this.selectedOptionalCols && this.selectedOptionalCols.length > 0) {
        columns = "";
        for (let column of this.selectedOptionalCols) {
          columns = columns + column.id + "|";
        }
        columns = columns.slice(0, -1);
      }
      let paramValues: ParamValue[] = [];
      paramValues.push(
        new ParamValue("page", this.page)
      );
      paramValues.push(
        new ParamValue(this.activeOptionalCols.optionalColsName, columns)
      );
      this.columnService.saveColumnsForConnectedUser(paramValues).subscribe(data => {
        this.selectedOptionalColsHasPendingChanges = false;
      });
    }
  }

  adaptConditionalColumnsToRequest(activeConditions ? : string[]): void {
    let cols = [];

    if (activeConditions) {
      this.activeConditions = activeConditions;
    }

    if (this.activeConditions && this.activeConditions != null && this.activeConditions.length > 0) {
      for (let col of this.conditionalCols) {
        let colConditions = col.conditions != null ? col.conditions.split("|") : null;
        for (let condition of this.activeConditions) {
          if (colConditions == null || (colConditions.indexOf(condition) > -1)) {
            cols.push(col.col);
            break;
          }
        }
      }
    } else {
      if (this.conditionalCols && this.conditionalCols != null && this.conditionalCols.length > 0) {
        for (let col of this.conditionalCols) {
          if (col.conditions == null) {
            cols.push(col.col);
          }
        }
      }
    }

    this.cols = cols;
  }

  private populateAllPossibleColumns(): void {
    this.orderedColumns = [];
    let orderedColumns: string[] = [];
    if (this.conditionalCols && this.conditionalCols != null) {
      for (let col of this.conditionalCols) {
        let field = col.col.field;
        if (!orderedColumns.find(c => c == field)) {
          orderedColumns.push(field);
        }
      }
    }
    if (this.optionalCols && this.optionalCols != null) {
      for (let optCol of this.optionalCols) {
        for (let col of optCol.optionalCols) {
          if (col.value.col && col.value.col != null) {
            let field = col.value.col.field;
            if (!orderedColumns.find(c => c == field)) {
              orderedColumns.push(field);
            }
          }
        }
      }
    }
    this.orderedColumns = orderedColumns;
  }

  getColumnsOrder(callback ? : (arg: any) => any, arg ? : any): void {
    this.columnService.getColumnsOrderForConnectedUser(this.page).subscribe(data => {
      if (data && data.length > 0) {
        this.orderedColumns = [];
        let orderedColumns: string[] = [];
        for (let paramValue of data) {
          orderedColumns.push(paramValue.value);
        }
        this.orderedColumns = orderedColumns;
      } else {
        this.populateAllPossibleColumns();
      }
      this.applyColumnsOrder();
      if (callback) {
        callback(arg);
      }
    });
  }

  private reorderAllColumns($event: any): void {
    // Inspired from bubble sort
    // https://fr.wikipedia.org/wiki/Tri_%C3%A0_bulles
    for (let i = this.orderedColumns.length - 1; i > 0; i--) {
      for (let j = 0; j <= i - 1; j++) {
        if (this.shouldSwitchColumns($event.columns, this.orderedColumns[j], this.orderedColumns[i])) {
          let buffer: string = this.orderedColumns[j];
          this.orderedColumns[j] = this.orderedColumns[i];
          this.orderedColumns[i] = buffer;
        }
      }
    }
  }

  private shouldSwitchColumns(eventColumns: any[], columnToCheck: string, columnToCheckAgainst: string): boolean {
    let columnToCheckPositionInEventColumns: number = undefined;
    let columnToCheckAgainstPositionInEventColumns: number = undefined;
    let index: number = 0;
    for (let columnOfEvent of eventColumns) {
      if (columnOfEvent.field == columnToCheck) {
        columnToCheckPositionInEventColumns = index;
      }
      if (columnOfEvent.field == columnToCheckAgainst) {
        columnToCheckAgainstPositionInEventColumns = index;
      }
      if (columnToCheckPositionInEventColumns && columnToCheckAgainstPositionInEventColumns) {
        break;
      }
      index++;
    }
    if (columnToCheckPositionInEventColumns == undefined || columnToCheckAgainstPositionInEventColumns == undefined) {
      return false;
    }
    return columnToCheckPositionInEventColumns > columnToCheckAgainstPositionInEventColumns;
  }

  saveColumnsOrder($event: any): void {
    if ($event.columns && $event.columns.length > 0) {
      this.reorderAllColumns($event);
      let paramValues: ParamValue[] = [];
      paramValues.push(
        new ParamValue("page", this.page + "_ORDER")
      );
      let columnIndex = 1;
      for (let column of this.orderedColumns) {
        let columnIndexString: string = columnIndex.toLocaleString('fr', {
          minimumIntegerDigits: 3
        });
        paramValues.push(
          new ParamValue(columnIndexString, column)
        );
        columnIndex++;
      }
      this.columnService.saveColumnsOrderForConnectedUser(paramValues).subscribe(data => {});
    }
  }

  applyColumnsOrder(): void {
    if (this.orderedColumns && this.orderedColumns != null && this.orderedColumns.length > 0) {
      let cols: any[] = [];
      for (let column of this.orderedColumns) {
        let col = this.cols.find(c => c.field == column);
        if (col) {
          cols.push(col);
        }
      }
      for (let col of this.cols) {
        if (!cols.find(c => c.field == col.field)) {
          cols.push(col);
        }
      }
      this.cols = cols;
    }
  }

  convertObjectsArrayToRawObjectsJsonData(objectsArray: any[]): any[] {
    const rawObjectsJsonData: any[] = [];
    for (const object of objectsArray) {
      const rawObject: any = {};
      for (const col of this.cols) {
        rawObject[col.header] = ObjectUtils.resolveFieldData(object, col.field);
      }
      rawObjectsJsonData.push(rawObject);
    }
    return rawObjectsJsonData;
  }

  static convertObjectsArrayToRawObjectsJsonData(objectsArray: any[], cols: any[]): any[] {
    const rawObjectsJsonData: any[] = [];
    for (const object of objectsArray) {
      const rawObject: any = {};
      for (const col of cols) {
        rawObject[col.header] = ObjectUtils.resolveFieldData(object, col.field);
      }
      rawObjectsJsonData.push(rawObject);
    }
    return rawObjectsJsonData;
  }
}
