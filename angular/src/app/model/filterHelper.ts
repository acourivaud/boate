import { Table } from 'primeng/table';
import { FilterService } from '../service/filter.service';
import { ParamValue } from './paramValue';

export class FilterHelper {
	
	options: any;
	filters: any;
	filtersForRequests: any = {};
	filterService: FilterService;
	page: string;
	connectedUserFilters: ParamValue[] = [];
	areConnectedUserFiltersLoaded: boolean = false;
	useFiltersForRequestsCallback: (arg: any) => any;
	useFiltersForRequestsArg: any;
	first: Number = 0;
  	rows: Number = 10;

	constructor(filterService: FilterService,
				page: string,
				useFiltersForRequestsCallback?: (arg: any) => any,
				useFiltersForRequestsArg?: any) {
		this.filterService = filterService;
		this.page = page;
		if (useFiltersForRequestsCallback) {
			this.useFiltersForRequestsCallback = useFiltersForRequestsCallback;
			this.useFiltersForRequestsArg = useFiltersForRequestsArg;
		}
	}

	useFiltersForRequests(isCallbackActive?: boolean) {
		this.filtersForRequests = {};
		for (var filter in this.filters) {
			// --> https://stackoverflow.com/questions/8312459/iterate-through-object-properties
			if (this.filters.hasOwnProperty(filter)) {
				  if (this.filters[filter]) {
					  this.filtersForRequests[filter] = this.filters[filter];
				  }
			}
		}
		if (isCallbackActive && this.useFiltersForRequestsCallback) {
			this.useFiltersForRequestsCallback(this.useFiltersForRequestsArg);
		}
	}

	populateParamValueFiltersForBackEnd(): ParamValue[] {
		let paramsValues: ParamValue[] = [];
		for (var filter in this.filtersForRequests) {
		  // --> https://stackoverflow.com/questions/8312459/iterate-through-object-properties
		  if (this.filtersForRequests.hasOwnProperty(filter)) {
				if (this.filtersForRequests[filter]) {
					if (Object.keys(this.filtersForRequests[filter]).indexOf("raw") > -1) { // For Select/Option
						if (this.filtersForRequests[filter].raw && this.filtersForRequests[filter].raw != null) {
							if (this.endsWith(filter, "SelectedSearch")) {
								paramsValues.push(new ParamValue(filter, JSON.stringify(this.filtersForRequests[filter])));
							} else {
								paramsValues.push(new ParamValue(filter, this.filtersForRequests[filter].raw));	
							}
						}
					} else { // For Typed Inputs
						if (this.filtersForRequests[filter] != "" && this.filtersForRequests[filter] != " ") {
							paramsValues.push(new ParamValue(filter, this.filtersForRequests[filter]));
						}
					}
				}
		  }
		}
		return paramsValues;
	}

	populateDefaultFiltersForConnectedUser(callback?: (arg: any) => any, arg?: any): any {
		this.filterService.getFiltersForConnectedUser(this.page).subscribe(data => {
			for (let paramValue of data) {
				this.connectedUserFilters.push(new ParamValue(paramValue.param, paramValue.value));
				this.setValueDependingOnSelectedOrTyped(paramValue.param, paramValue.value);
			}
			this.useFiltersForRequests();
			this.areConnectedUserFiltersLoaded = true;
			if (callback) {
				callback(arg);
			}
		});
	}

	razAllFilters(): void {
		let filtersToSave: ParamValue[] = [ new ParamValue( "page", this.page ) ];
		for (var filter in this.filters) {
			// --> https://stackoverflow.com/questions/8312459/iterate-through-object-properties
			if (this.filters.hasOwnProperty(filter)) {
				this.filters[filter] = null;
			}
		}
		this.updateFiltersForConnectedUser(filtersToSave);
	}

	razAllDescriptionFilters(): void {
		let filtersToSave: ParamValue[] = [ new ParamValue( "page", this.page ) ];
		for (var filter in this.filters) {
			// --> https://stackoverflow.com/questions/8312459/iterate-through-object-properties
			if (this.filters.hasOwnProperty(filter) && filter.indexOf("Des") == 0) {
				this.filters[filter] = null;
			} else if (this.filters.hasOwnProperty(filter) && !this.isEmptyParam(this.filters[filter])) {
				filtersToSave.push(new ParamValue(filter, this.getValueOfParamDependingOnStringOrAny(this.filters[filter])));
			}
		}
		this.updateFiltersForConnectedUser(filtersToSave);
	}

	updateFiltersForConnectedUser(paramsValues: ParamValue[]): void {
		this.filterService.saveFiltersForConnectedUser(paramsValues).subscribe(data => {
			this.useFiltersForRequests();
		});
	}

	generateRequestFilters(event: any, attachedTable: Table): ParamValue[] {
		let requestFilters: ParamValue[] = [];

		if (event != null) {
			if (event.first != null && event.first != undefined && event.rows) {
				requestFilters.push(
					new ParamValue("first", event.first.toString())
				);
				requestFilters.push(
					new ParamValue("rows", event.rows.toString())
				);
			}
			if (event.multiSortMeta && event.multiSortMeta != null && event.multiSortMeta.length > 0) {
				let orderByIndex = 1;
				for (let sortMetaData of event.multiSortMeta) {
					requestFilters.push(
						new ParamValue("ORDERBY|" + orderByIndex++ + "|" + (sortMetaData.order == 1 ? "ASC" : "DESC"), sortMetaData.field )
					);
				}
			}
		} else {
			this.first = 0;
			attachedTable.reset();
			requestFilters.push(
				new ParamValue("first", "0")
			);
			requestFilters.push(
				new ParamValue("rows", this.rows.toString())
			);
		}

		for (let paramValue of this.populateParamValueFiltersForBackEnd()) {
			requestFilters.push(paramValue);
		}

		return requestFilters;
	}

	isEmptyParam(param: any): boolean {
		if (!param || param == null || param == "" || param == " " || param.raw == null) {
			return true;
		}
		return false;
	}

	getValueOfParamDependingOnStringOrAny(value: any): string {
		if (value && value != null) {
			return value.raw ? value.raw : value;
		}
		return null;
	}

	setValueDependingOnSelectedOrTyped(param: string, value: string): void {
		if (this.endsWith(param, "Selected")) {
			this.filters[param] = this.options[this.getOptionsFieldBasedOnSelected(param)].find(x => x.raw == value) ? this.options[this.getOptionsFieldBasedOnSelected(param)].find(x => x.raw == value) : this.options[this.getOptionsFieldBasedOnSelected(param)].find(x => x.raw == null);
		} else if (this.endsWith(param, "Typed")) {
			this.filters[param] = value;
		} else if (this.endsWith(param, "SelectedSearch")) {
			this.options[this.getOptionsFieldBasedOnSelectedSearch(param)] = [ JSON.parse(value) ];
			this.filters[param] = this.options[this.getOptionsFieldBasedOnSelectedSearch(param)][0];
		} else if (this.endsWith(param, "TypedSearch")) {
			this.options[this.getOptionsFieldBasedOnTypedSearch(param)] = [ { raw: value } ];
			this.filters[param] = this.options[this.getOptionsFieldBasedOnTypedSearch(param)][0];
		}
	}

	endsWith(str: string, suffix: string): boolean {
		return str.indexOf(suffix, str.length - suffix.length) !== -1;
	}

	getOptionsFieldBasedOnSelected(selectedString: string): string {
		return selectedString.replace(new RegExp('Selected'+'$'), 'Options');
	}

	getOptionsFieldBasedOnSelectedSearch(selectedString: string): string {
		return selectedString.replace(new RegExp('SelectedSearch'+'$'), 'Options');
	}

	getOptionsFieldBasedOnTypedSearch(selectedString: string): string {
		return selectedString.replace(new RegExp('TypedSearch'+'$'), 'Options');
	}

	getSelectedRawString(selectedValue: string) : string {
		return this.filters[selectedValue] != null && this.filters[selectedValue].raw != null ? this.filters[selectedValue].raw : null;
	}

	trySetSelectedFromOptionsIfPossible(index: string, selectedValue: string) : void {
		if (selectedValue != null) {
			this.filters[index+"Selected"] = this.options[index+"Options"].find(x => x.raw == selectedValue) ? this.options[index+"Options"].find(x => x.raw == selectedValue) : this.options[index+"Options"].find(x => x.raw == null);
		}
	}
}