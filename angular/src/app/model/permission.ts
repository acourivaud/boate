export class Permission {
	public id: number;
	public libelle: string;

	constructor(dto: any) {
		this.populateFromDTO(dto);
	}

	populateFromDTO(dto: any) {
		if (dto) {
			this.id = dto.id;
			this.libelle = dto.libelle;
		}
	}

	static populateFromArrayDto(arrayDto: any []): Permission[] {
		let permissions: Permission[];
		if (arrayDto && arrayDto != null) {
			permissions = [];
			for (const dto of arrayDto) {
				permissions.push(new Permission(dto));
			}
		}
		return permissions;
	}
}
