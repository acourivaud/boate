import { IUniversalObject } from './IUniversalObject';
import { BackUrlConstant } from './../constant/BackUrlConstant';
import { Structure } from './structure';

export class Proprietaire implements IUniversalObject {
  readonly className: string = 'Proprietaire';
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.PROPRIETAIRE,
    apiPostUrl: BackUrlConstant.PROPRIETAIRE,
    apiDeleteUrl: BackUrlConstant.PROPRIETAIRE,
    isLazyListed: false,
    popupTitleDescription: 'un Propriétaire',
    popupChildDelete: false,
    cols: [
      { field: 'nom', header: 'Nom', style: 'column-center', editable: true }
    ],
    fields: [
      { index: 'nom', libelle: 'Nom', type: 'input', required: true }
    ]
  };

  id: number;
  nom: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new Proprietaire(dto);
  }

  completeDTO(dto: any, index: number): Proprietaire {
    // Nothing to complete atm
    return dto;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.nom = dto.nom;
    }
  }

  getSummaryDescription(): string {
    return this.nom;
  }
}
