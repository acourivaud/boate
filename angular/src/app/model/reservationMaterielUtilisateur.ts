import { Materiel } from './materiel';
import { DateDto } from './dateDto';
import { Utilisateur } from './utilisateur';

export class ReservationMaterielUtilisateur {

  idPlanningMateriel: number;

  // True if this object is used in a request to get all Users having booked a specific Vehicle.
	// False if this object is used in a request to get all Vehicles booked by a User.
	// Avoid loading the grouping object multiple times in loop (whether it's a User or Vehicle).
  trueIfListOfUsersFalseIfListOfMateriels: boolean;

	materiel: Materiel;
	utilisateur: Utilisateur;
  dateDebut: { dto: string };
  dateFin: { dto: string };

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new ReservationMaterielUtilisateur(dto);
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.idPlanningMateriel = dto.idPlanningMateriel;
      this.trueIfListOfUsersFalseIfListOfMateriels = dto.trueIfListOfUsersFalseIfListOfMateriels;
      if (this.trueIfListOfUsersFalseIfListOfMateriels === true) {
        this.utilisateur = new Utilisateur(dto.utilisateur);
      } else {
        this.materiel = new Materiel(dto.materiel);
      }
      this.dateDebut = dto.dateDebut ? new DateDto(dto.dateDebut).toBackEndDto() : null;
      this.dateFin = dto.dateFin ? new DateDto(dto.dateFin).toBackEndDto() : null;
    }
  }

  getHoursTotalForDate(date: any): number {
    const dateDebut = new DateDto(this.dateDebut).toDate();
		const dateFin = new DateDto(this.dateFin).toDate();
		// "date: any" is the object from PrimeNg Calendar
		const compareDateDebut = new Date(date.year, date.month, date.day, 0, 0, 0);
    const compareDateFin = new Date(date.year, date.month, date.day, 23, 59, 59);
    if (dateDebut > compareDateFin) return 0;
    if (dateFin < compareDateDebut) return 0;
    let heureDebut: number;
    if (dateDebut < compareDateDebut) {
      heureDebut = 0;
    } else {
      heureDebut = dateDebut.getHours();
    }
    let heureFin: number;
    if (dateFin > compareDateFin) {
      heureFin = 24;
    } else {
      // +1 because if the dateFin time is for example 3:59 we want to count 4
      heureFin = dateFin.getHours() + 1;
    }
    return heureFin - heureDebut;
  }

  getSummaryDescription(): string {
    const dateDebut = new DateDto(this.dateDebut).toDate();
    const dateFin = new DateDto(this.dateFin).toDate();
    const returnName = this.trueIfListOfUsersFalseIfListOfMateriels ? this.utilisateur.getSummaryDescription() : this.materiel.getSummaryDescription();
    return '[ du ' + dateDebut.toLocaleDateString() + ' à ' + dateDebut.getHours() + 'H au ' +
              dateFin.toLocaleDateString() + ' à ' + (dateFin.getHours() + 1) + 'H ] > ' + returnName;
  }

  static populateFromArrayDTO(dto: any[]): ReservationMaterielUtilisateur[] {
    const reservationMaterielUtilisateur: ReservationMaterielUtilisateur[] = [];
    if (dto && dto.length > 0) {
      for (const subDto of dto) {
        reservationMaterielUtilisateur.push(new ReservationMaterielUtilisateur(subDto));
      }
    }
    return reservationMaterielUtilisateur;
  }
}
