import * as XLSX from 'xlsx';

export class ExportExcelHelper {
  static export(data: any[], workbookName: string, sheetName: string): void {
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, sheetName);
    XLSX.writeFile(wb, workbookName + '.xlsx');
  }
}
