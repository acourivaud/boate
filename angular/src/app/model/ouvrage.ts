import { Ligne } from './ligne';
import { TypeOuvrage } from './typeOuvrage';
import { UniteOperationnelle } from './uniteOperationnelle';
import { Departement } from './departement';
import { Commune } from './commune';

export class Ouvrage {
  id: number;
  name: string;
  pk: string;
  middlePk: string;
  endPk: string;
  up: UniteOperationnelle;
  line: Ligne;
  categorie: string;
  typeOuvrage: TypeOuvrage;
  indice: string;
  nature: string;
  geometrie: string;
  situation: string;
  oaOuverture: number;
  couverture: number;
  longueur: number;
  uic: string;
  ouvragePere: Ouvrage;
  typeOts: string;
  sousCategorieOts: string;
  otDateIncident: string;
  otAvarie: string;
  otTypeConfortement: string;
  otConfortementDesc: string;
  otFicheSignal: string;
  otFosse: string;
  oaHauteur: number;
  oaHauteurMini: number;
  fsa: string;
  aboveBelow: string;
  year: number;
  supprime: boolean;
  archive: boolean;
  firstDepartement: Departement;
  secondDepartement: Departement;
  firstCity: Commune;
  secondCity: Commune;
  distanceRail: number;
  fruitParement: number;
  surveillanceId: number;
  ouvrageEnveloppe: boolean;
  otClassement: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.name = dto.name;
      this.pk = dto.pk;
      this.middlePk = dto.middlePk;
      this.endPk = dto.endPk;
      this.up = dto.up;
      this.line = dto.line ? new Ligne(dto.line) : null;
      this.categorie = dto.categorie;
      this.typeOuvrage = new TypeOuvrage(dto.typeOuvrage);
      this.indice = dto.indice;
      this.nature = dto.nature;
      this.geometrie = dto.geometrie;
      this.situation = dto.situation;
      this.oaOuverture = dto.oaOuverture;
      this.couverture = dto.couverture;
      this.longueur = dto.longueur;
      this.uic = dto.uic;
      this.ouvragePere = dto.ouvragePere ? new Ouvrage(dto.ouvragePere) : null;
      this.typeOts = dto.typeOts;
      this.sousCategorieOts = dto.sousCategorieOts;
      this.otDateIncident = dto.otDateIncident;
      this.otAvarie = dto.otAvarie;
      this.otTypeConfortement = dto.otTypeConfortement;
      this.otConfortementDesc = dto.otConfortementDesc;
      this.otFicheSignal = dto.otFicheSignal;
      this.otFosse = dto.otFosse;
      this.oaHauteur = dto.oaHauteur;
      this.oaHauteurMini = dto.oaHauteurMini;
      this.fsa = dto.fsa;
      this.aboveBelow = dto.aboveBelow;
      this.year = dto.year;
      this.supprime = dto.supprime;
      this.archive = dto.archive;
      this.firstDepartement = dto.firstDepartement;
      this.firstCity = dto.firstCity;
      this.secondDepartement = dto.secondDepartement;
      this.secondCity = dto.secondCity;
      this.distanceRail = dto.distanceRail;
      this.fruitParement = dto.fruitParement;
      this.surveillanceId = dto.surveillanceId;
      this.ouvrageEnveloppe = dto.ouvrageEnveloppe;
      this.otClassement = dto.otClassement;
    }
  }
}
