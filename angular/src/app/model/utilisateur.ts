import { IUniversalObject } from './IUniversalObject';
import { BackUrlConstant } from './../constant/BackUrlConstant';
import { DateDto } from './dateDto';
import { Infrapole } from './infrapole';
import { Permission } from './permission';
import { Region } from './region';
import { Secteur } from './secteur';
import { UniteOperationnelle } from './uniteOperationnelle';
import { Structure } from './structure';

export class Utilisateur implements IUniversalObject {
  readonly className: string = 'Utilisateur';
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.USER,
    apiPostUrl: BackUrlConstant.USER,
    apiDeleteUrl: BackUrlConstant.USER,
    isLazyListed: false,
    popupTitleDescription: 'un Utilisateur',
    popupChildDelete: false,
    cols: [
      { field: 'prenom', header: 'Prénom', style: '', editable: false },
      { field: 'nom', header: 'Nom', style: '', editable: false },
      { field: 'email', header: 'Email', style: 'column-center', editable: false }
    ],
    fields: []
  };

  idUtilisateur: number;
  login: string;
  password: string;
  nom: string;
  prenom: string;
  fonction: string;
  entite: string;
  telephoneFixe: string;
  telephoneMobile: string;
  fax: string;
  email: string;
  up: UniteOperationnelle;
  /* BEGIN : Front Only */
  secteur: Secteur;
  infrapole: Infrapole;
  region: Region;
  /* END : Front Only */
  dateFinMission: {
    dto: string,
  };
  urlSig: string;
  ssid: string;
  nbLignesTableau: string;
  affichageAlerte: boolean;
  consultationReleaseNotes: boolean;
  pageAccueil: string;
  permissions: Permission[];
  fullName: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new Utilisateur(dto);
  }

  completeDTO(dto: any, index: number): Utilisateur {
    // Nothing to complete atm
    return dto;
  }

  getSummaryDescription(): string {
    return (this.prenom ? this.prenom + ' ' : '') + (this.nom ? this.nom + ' ' : '') + (this.email ? this.email : '');
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.idUtilisateur = dto.idUtilisateur;
      this.login = dto.login;
      this.password = dto.password;
      this.nom = dto.nom;
      this.prenom = dto.prenom;
      this.fonction = dto.fonction;
      this.entite = dto.entite;
      this.telephoneFixe = dto.telephoneFixe;
      this.telephoneMobile = dto.telephoneMobile;
      this.fax = dto.fax;
      this.email = dto.email;
      this.up = dto.up ? new UniteOperationnelle(dto.up) : null;
      if (dto.up) {
        if (dto.secteur && dto.secteur != null) {
          this.secteur = new Secteur(dto.secteur);
        } else {
          this.secteur = dto.up.secteur ? new Secteur(dto.up.secteur) : null;
        }
        if (dto.infrapole && dto.infrapole != null) {
          this.infrapole = new Infrapole(dto.infrapole);
        } else {
          this.infrapole = dto.up.secteur ? new Infrapole(dto.up.secteur.infrapole) : new Infrapole(dto.up.infrapole);
        }
        if (dto.region && dto.region != null) {
          this.region = new Region(dto.region);
        } else {
          this.region = dto.up.secteur ? new Region(dto.up.secteur.infrapole.region) : new Region(dto.up.infrapole.region);
        }
      } else {
        this.secteur = null;
        this.infrapole = null;
        this.region = null;
      }
      this.dateFinMission = dto.dateFinMission ? new DateDto(dto.dateFinMission).toBackEndDto() : null;
      this.urlSig = dto.urlSig;
      this.ssid = dto.ssid;
      this.nbLignesTableau = dto.nbLignesTableau;
      this.affichageAlerte = dto.affichageAlerte;
      this.consultationReleaseNotes = dto.consultationReleaseNotes;
      this.pageAccueil = dto.pageAccueil;
      this.permissions = Permission.populateFromArrayDto(dto.permissions);
      this.fullName = this.prenom && this.nom ? this.prenom.concat(' ', this.nom) : this.prenom ? this.prenom : this.login;
    }
  }

  hasPermission(permission: string): boolean {
    if (permission && this.permissions && this.permissions.find(p => p.libelle === permission)) {
      return true;
    }
    return false;
  }
}
