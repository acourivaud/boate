export class PropositionBouclage {
	public proposition: string;
	public level: PropositionBouclageStatus;

	constructor(dto: any) {
		this.populateFromDTO(dto);
	}

	populateFromDTO(dto: any) {
		if (dto) {
			if (dto.proposition) this.proposition = dto.proposition;
			if (dto.level) this.level = dto.level;
		}
	}

	getColor(): string {
		if (this.level) {
			if (this.level === PropositionBouclageStatus.REJECTED) {
				return 'is-red';
			}
			if (this.level === PropositionBouclageStatus.PENDING && this.proposition) {
				return 'is-pink';
			}
		}
		return '';
	}

	getDescription(): string {
		if (this.level) {
			if (this.level === PropositionBouclageStatus.REJECTED) {
				return this.proposition ? this.proposition + ' - ' + 'Rejetée' : '';
			}
			if (this.level === PropositionBouclageStatus.PENDING) {
				return this.proposition ? this.proposition + ' - ' + 'En Attente' : '';
			}
		}
		return this.proposition ? this.proposition + ' - ' + 'Approuvée' : '';
	}
}

export enum PropositionBouclageStatus {
	APPROVED = 'APPROVED',
	REJECTED = 'REJECTED',
	PENDING = 'PENDING'
}
