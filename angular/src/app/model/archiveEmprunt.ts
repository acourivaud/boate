import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { ArchiveDossier } from './archiveDossier';
import { Utilisateur } from './utilisateur';
import { IUniversalObject } from './IUniversalObject';
import { Structure } from './structure';

export class ArchiveEmprunt implements IUniversalObject {
  id: number;
  dossier: ArchiveDossier;
  utilisateur: Utilisateur;
  dateEmprunt: Date;
  dateRestitution: Date;
  readonly className: string = 'OuvrageProprietaire';
  readonly structure: Structure = {
    apiGetUrl: null,
    apiPostUrl: null,
    apiDeleteUrl: null,
    isLazyListed: false,
    popupTitleDescription: 'un Propriétaire d\'Ouvrage',
    popupChildDelete: false,
    cols: [],
    fields: [],
  };
  etat: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.dossier = dto.dossier;
      this.utilisateur = dto.utilisateur;
      this.dateEmprunt = dto.dateEmprunt;
      this.dateRestitution = dto.dateRestitution;
      this.etat = this.getEtatDossier(dto.dateRestitution);
    }
  }

  copy(dto: any) {
    return new ArchiveEmprunt(dto);
  }

  completeDTO(dto: any, index: number): ArchiveEmprunt {
    // Nothing to complete atm
    return dto;
  }

  getSummaryDescription(): string {
    return this.dossier.numeroDossier + ' - ' + this.utilisateur.nom;
  }

  getEtatDossier(dateRestitution: Date) {
    if (dateRestitution) {
        return `Restitué`;
    }else {
        return `Emprunté`;
    }
  }
}
