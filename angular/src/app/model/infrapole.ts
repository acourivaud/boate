import { IUniversalObject } from './IUniversalObject';
import { BackUrlConstant } from './../constant/BackUrlConstant';
import { Region } from './region';
import { Structure } from './structure';

export class Infrapole implements IUniversalObject {
  readonly className: string = 'Infrapole';
  readonly structure: Structure = {
      apiGetUrl: BackUrlConstant.FILTER_INFRAPOLE,
      apiPostUrl: BackUrlConstant.FILTER_INFRAPOLE,
      apiDeleteUrl: BackUrlConstant.FILTER_INFRAPOLE,
      isLazyListed: false,
      popupTitleDescription: "un Infrapole",
      popupChildDelete: true,
      cols: [],
      fields: [
      {
        index: '_buffer_usesSecteurSubLevel',
        libelle: 'Cet Infrapole comporte des Secteurs',
        type: 'boolean',
        required: false
      },
      {
        index: 'name',
        libelle: 'Nom',
        type: 'input',
        required: true
      },
      {
        index: 'code',
        libelle: 'Code',
        type: 'input',
        required: true
      },
    ]
  };
  id: number;
  code: string;
  name: string;
  region: Region;
  usesSecteurSubLevel: boolean;
  _buffer_usesSecteurSubLevel: boolean;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new Infrapole(dto);
  }
  
  completeDTO(dto: any, index: number): Infrapole {
    // Nothing to complete atm
    return dto;
  }

  getSummaryDescription(): string {
    return this.code + ' - ' + this.name;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.code = dto.code;
      this.name = dto.name;
      this.region = dto.region ? new Region(dto.region) : null;
      this.usesSecteurSubLevel = dto.usesSecteurSubLevel != undefined && dto.usesSecteurSubLevel != null ? dto.usesSecteurSubLevel : true;
      this.getSecteurStatus();
    }
  }

  getSecteurStatus(): void {
    this._buffer_usesSecteurSubLevel = this.usesSecteurSubLevel;
  }

  setSecteurStatus(): void {
    this.usesSecteurSubLevel = this._buffer_usesSecteurSubLevel;
  }

  needsWarningAboutDeletingSecteurs(): boolean {
    if (this.id) {
      return this.usesSecteurSubLevel && !this._buffer_usesSecteurSubLevel;
    }
    return false;
  }

  needsWarningAboutDeletingUOs(): boolean {
    if (this.id) {
      return !this.usesSecteurSubLevel && this._buffer_usesSecteurSubLevel;
    }
    return false;
  }

  hideSecteurCheckbox(): boolean {
    if (this.id) {
      return this.needsWarningAboutDeletingSecteurs() || this.needsWarningAboutDeletingUOs();
    }
    return false;
  }
}
