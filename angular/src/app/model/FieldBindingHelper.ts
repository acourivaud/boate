import { ObjectUtils } from 'primeng/components/utils/objectutils';

export class FieldBindingHelper {

  object: any;
  propertiesToBind: string[];
  htmlBinding: any = {};

  constructor(object: any) {
    this.object = object;
  }

  bindObjectToHtml() {
    for (const property of this.propertiesToBind) {
      this.htmlBinding[property] = ObjectUtils.resolveFieldData(this.object, property);
    }
  }

  bindHtmlToObject() {
    for (const property of this.propertiesToBind) {
      let binding = ObjectUtils.resolveFieldData(this.object, property);
      binding = this.htmlBinding[property];
      this.setObjectReferenceBasedOnPropertyString(this.object, property, binding);
    }
  }

  setObjectReferenceBasedOnPropertyString(object: any, property: string, value: any): void {
    if (property.indexOf('.') > -1) {
      const splits = property.split('.');
      if (!this.isObjectReferenceUndefined(object, splits)) {
          if (splits.length === 2) {
                object[splits[0]][splits[1]] = value;
          } else if (splits.length === 3) {
            object[splits[0]][splits[1]][splits[2]] = value;
          } else if (splits.length === 4) {
            object[splits[0]][splits[1]][splits[2]][splits[3]] = value;
          } else if (splits.length === 5) {
            object[splits[0]][splits[1]][splits[2]][splits[3]][splits[4]] = value;
          } else if (splits.length === 6) {
            object[splits[0]][splits[1]][splits[2]][splits[3]][splits[4]][splits[5]] = value;
          } else if (splits.length === 7) {
            object[splits[0]][splits[1]][splits[2]][splits[3]][splits[4]][splits[5]][splits[6]] = value;
          } else if (splits.length === 8) {
            object[splits[0]][splits[1]][splits[2]][splits[3]][splits[4]][splits[5]][splits[6]][splits[7]] = value;
          } else if (splits.length === 9) {
            object[splits[0]][splits[1]][splits[2]][splits[3]][splits[4]][splits[5]][splits[6]][splits[7]][splits[8]] = value;
          } else if (splits.length === 10) {
            object[splits[0]][splits[1]][splits[2]][splits[3]][splits[4]][splits[5]][splits[6]][splits[7]][splits[8]][splits[9]] = value;
          }
      }
    } else {
      object[property] = value;
    }
  }

  isObjectReferenceUndefined(object: any, splits: any[]): boolean {
    let rstFunction = false;
    let objectTemp = object;
    splits.forEach(element => {
      objectTemp = objectTemp !== undefined ? objectTemp[element] : undefined;
      if (objectTemp === undefined) {
        rstFunction = true;
      }
    });
    return rstFunction;
  }
}
