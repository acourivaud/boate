import { ParamTableColumnDefinition } from './paramTableColumnDefinition';
import { ParamValue } from './paramValue';

export class ParamModule {
  id: number;
  statut: string;
  libelleFonctionnel: string;
  libelleTechnique: string;
  type: string;
  className: string;
  paramValues: ParamValue[];
  tableColumnDefinitions: ParamTableColumnDefinition[];
  urlBackFind ? : string; // url pour récupérer les éléments d'une Table --> pas utilsé, pour future version maybe

  constructor(id: number, statut: string, libelleFonctionnel: string, libelleTechnique: string, type: string, paramValues: ParamValue[],
    tableColumnDefinitions: ParamTableColumnDefinition[], className: string) {
    this.id = id;
    this.statut = statut;
    this.libelleFonctionnel = libelleFonctionnel;
    this.libelleTechnique = libelleTechnique;
    this.type = type;
    this.paramValues = paramValues;
    this.tableColumnDefinitions = tableColumnDefinitions;
    this.className = className;
  }
}
