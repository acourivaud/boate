import { IUniversalObject } from './IUniversalObject';
import { BackUrlConstant } from './../constant/BackUrlConstant';
import { Structure } from './structure';

export class Geometrie implements IUniversalObject {
  readonly className: string = 'Geometrie';
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.FILTER_GEOMETRIE,
    apiPostUrl: BackUrlConstant.FILTER_GEOMETRIE_SAVE,
    apiDeleteUrl: BackUrlConstant.FILTER_GEOMETRIE,
    isLazyListed: true,
    popupTitleDescription: 'une Géométrie',
    popupChildDelete: true,
    cols: [
      { field: 'libelle', header: 'Libellé', style: 'column-width-300', editable: true },
    ],
    fields: [
      {
        index: 'libelle',
        libelle: 'Libellé',
        type: 'input',
        required: true,
      },
    ]
  };
  id: number;
  libelle: string;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
    return new Geometrie(dto);
  }

  completeDTO(dto: any, index: number): Geometrie {
    // Nothing to complete atm
    return dto;
  }

  getSummaryDescription(): string {
    return this.libelle;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.libelle = dto.libelle;
    }
  }
}
