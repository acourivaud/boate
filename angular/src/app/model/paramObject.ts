export class ParamObject {
  param: string;
  value: any;
  modules: string[];

  constructor(param: string, value: any, modules: string[]) {
    this.param = param;
    this.value = value;
    this.modules = modules;
  }
}
