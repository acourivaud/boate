import { IUniversalObject } from './IUniversalObject';
import { Type } from './type';
import { Geometrie } from './geometrie';
import { BackUrlConstant } from '../constant/BackUrlConstant';
import { Structure } from './structure';

export class GeometrieType implements IUniversalObject {
  readonly className: string = 'GeometrieType';
  readonly structure: Structure = {
    apiGetUrl: BackUrlConstant.FILTER_GEOMETRIE_TYPE,
    apiPostUrl: BackUrlConstant.FILTER_GEOMETRIE_TYPE,
    apiDeleteUrl: BackUrlConstant.FILTER_GEOMETRIE_TYPE,
    isLazyListed: false,
    popupTitleDescription: "une Association Type-Géométrie",
    popupChildDelete: false,
    cols: [],
    fields: [
      {
        index: 'geometrie',
        libelle: 'Géométrie',
        type: 'autocomplete',
        suggestionField: 'libelle',
        required: true,
      },
    ]
  };
  id: number;
  geometrie: Geometrie;
  type: Type;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
    return new GeometrieType(dto);
  }

  completeDTO(dto: any, index: number): GeometrieType {
    // Nothing to complete atm
    return dto;
  }

  getSummaryDescription(): string {
    return this.geometrie.libelle + ' - ' + this.type.libelle;
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.id = dto.id;
      this.type = dto.type ? new Type(dto.type) : undefined;
      this.geometrie = dto.geometrie ? new Geometrie(dto.geometrie) : undefined;
    }
  }
}
