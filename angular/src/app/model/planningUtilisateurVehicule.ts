import { Vehicule } from './vehicule';
import { DateDto } from './dateDto';
import { Utilisateur } from './utilisateur';
import { ReservationVehiculeUtilisateur } from './reservationVehiculeUtilisateur';

export class PlanningUtilisateurVehicule {

  // True if this object is used in a request to get all Users having booked a specific Vehicle.
	// False if this object is used in a request to get all Vehicles booked by a User.
	// Avoid loading the grouping object multiple times in loop (whether it's a User or Vehicle).
  trueIfListOfUsersFalseIfListOfVehicules: boolean;

  utilisateur: Utilisateur;
  vehicule: Vehicule;
  reservationVehiculeUtilisateur: ReservationVehiculeUtilisateur[];

  // Début et fin de la plage de requête.
  dateDebut: { dto: string };
  dateFin: { dto: string };

	// Used to show the popup when the requested reservation is in conflict with an other existing one.
	// If that's the case, then the result will contain the incriminated reservation only instead of the full list after update.
	// Conflict on user is non-blocking.
	// Conflict on vehicle is BLOCKING.
	conflictOnUserResponse: boolean = false;
	conflictOnVehicleResponse: boolean = false;

	// If set to true by the front, then authorize reservation for the same user, the same date and several vehicles.
	forceUpdateIfPossible: boolean = false;

  constructor(dto: any) {
    this.populateFromDTO(dto);
  }

  copy(dto: any) {
		return new PlanningUtilisateurVehicule(dto);
  }

  populateFromDTO(dto: any) {
    if (dto) {
      this.trueIfListOfUsersFalseIfListOfVehicules = dto.trueIfListOfUsersFalseIfListOfVehicules;
      if (this.trueIfListOfUsersFalseIfListOfVehicules === true) {
        // This is reversed in the sub-listed object ReservationVehiculeUtilisateur.
				// Here is the object we don't want to load multiple times in loop.
        this.vehicule = new Vehicule(dto.vehicule);
      } else {
        // This is reversed in the sub-listed object ReservationVehiculeUtilisateur.
				// Here is the object we don't want to load multiple times in loop.
        this.utilisateur = new Utilisateur(dto.utilisateur);
      }
      this.reservationVehiculeUtilisateur = ReservationVehiculeUtilisateur.populateFromArrayDTO(dto.reservationVehiculeUtilisateur);
      this.dateDebut = dto.dateDebut ? new DateDto(dto.dateDebut).toBackEndDto() : null;
      this.dateFin = dto.dateFin ? new DateDto(dto.dateFin).toBackEndDto() : null;
      this.conflictOnUserResponse = dto.conflictOnUserResponse;
      this.conflictOnVehicleResponse = dto.conflictOnVehicleResponse;
      this.forceUpdateIfPossible = dto.forceUpdateIfPossible;
    }
  }
}
