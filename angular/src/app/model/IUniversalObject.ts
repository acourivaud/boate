import { Structure } from './structure';

export interface IUniversalObject {
  readonly className: string;
  readonly structure: Structure;

  copy(dto: any);
  populateFromDTO(dto: any);
  completeDTO(dto: any, indexIncrement: number);
  getSummaryDescription(): string;
}
