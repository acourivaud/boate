import { Ouvrage } from './ouvrage';
import { ArchiveBoite } from './archiveBoite';
import { Utilisateur } from './utilisateur';

export class ArchiveDossier {
  id: number;
  boite: ArchiveBoite;
  ouvrage: Ouvrage;
  emprunteur: Utilisateur;
  numeroDossier: string;
  service: string;
  description: string;
  anneeDebut: number;
  anneeFin: number;
  dua: string;
  sortFinale: string;
  commentaires: string;
  pk: string;
  endPk: string;
  ligneNonRattachee: string;
  typeArchive: string;
  etat: string;

  constructor(dto: any, utilisateurConnecte: Utilisateur) {
    this.populateFromDTO(dto, utilisateurConnecte);
  }

  populateFromDTO(dto: any, utilisateurConnecte: Utilisateur) {
    if (dto) {
      this.id = dto.id;
      this.ouvrage = new Ouvrage(dto.ouvrage);
      this.boite = new ArchiveBoite(dto.boite);
      this.emprunteur = dto.emprunteur ? new Utilisateur(dto.emprunteur) : null;
      this.numeroDossier = dto.numeroDossier;
      this.service = dto.service;
      this.description = dto.description;
      this.anneeDebut = dto.anneeDebut;
      this.anneeFin = dto.anneeFin;
      this.dua = dto.dua;
      this.sortFinale = dto.sortFinale;
      this.commentaires = dto.commentaires;
      this.pk = dto.pk;
      this.endPk = dto.endPk;
      this.ligneNonRattachee = dto.ligneNonRattachee;
      this.typeArchive = dto.typeArchive;
      this.etat = this.getEtatDossier(dto.emprunteur, utilisateurConnecte);
    }
  }

  getEtatDossier(emprunteur: Utilisateur, utilisateurConnecte: Utilisateur) {
    if (emprunteur && emprunteur.idUtilisateur) {
      if (emprunteur.idUtilisateur === utilisateurConnecte.idUtilisateur) {
        return `Emprunté par l'utilisateur connecté`;
      }else {
        return `Emprunté par ${emprunteur.fullName}`;
      }
    }
    return `Libre`;
  }
}
