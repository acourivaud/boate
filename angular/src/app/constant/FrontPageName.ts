import { FrontUrlConstant } from './FrontUrlConstant';

export class FrontPageName {

	public static myArray = [
		{ path: FrontUrlConstant.DEFAULT_URL, title: "Ouvrage", precedentPage: "Accueil", urlPrecedentPage: FrontUrlConstant.DEFAULT_URL },
	];

	static getName(pageName: any) {
		return this.myArray.find(myObj => myObj.path == pageName);
	}
}