export class FrontUrlConstant {

  static PATH_TO_PAGE = '/pages/';
  static PATH_TO_AUTH = '/auth/';
  static PATH_TO_LOGIN = FrontUrlConstant.PATH_TO_AUTH + 'login';
  static DEFAULT_URL = FrontUrlConstant.PATH_TO_PAGE + 'ouvrage';
  static PATH_TO_PROFIL = FrontUrlConstant.PATH_TO_PAGE + 'profil';
  static PATH_TO_VISTE_OUVRAGE = FrontUrlConstant.PATH_TO_PAGE + 'bouclage/visite-ouvrage';
  static PATH_TO_SURVEILLANCE = FrontUrlConstant.PATH_TO_PAGE + 'surveillance';
  static PATH_TO_FICHE_SURVEILLANCE = `/pages/surveillance/fiche`;

  static PATH_TO_OUVRAGE = FrontUrlConstant.PATH_TO_PAGE + 'ouvrage';
  static PATH_TO_FICHE_OUVRAGE = `/pages/ouvrage/fiche`;
  static PATH_TO_FICHE_ARCHIVE = `/pages/archive/fiche`;
  static PATH_TO_ADMINISTRATION = FrontUrlConstant.PATH_TO_PAGE + 'administration';
}
