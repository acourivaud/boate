import { environment } from './../../environments/environment';

export class BackUrlConstant {
  //AuthentificationController
  static AUTH_URL = environment.baseUrl + '/auth';
  static LOGIN_URL = BackUrlConstant.AUTH_URL + '/signin';
  static LOGOUT_URL = BackUrlConstant.AUTH_URL + '/logout';
  static REFRESH_URL = BackUrlConstant.AUTH_URL + '/refresh';

  //UtilisateurController
  static USER = environment.baseUrl + '/utilisateur';
  static USER_BY_RESPONSABLE = BackUrlConstant.USER + '/byResponable';
  static USER_CONNECTED = BackUrlConstant.USER + '/connected';
  static USER_ID = BackUrlConstant.USER + '/id';
  static USER_CONNECTED_TEST = `${BackUrlConstant.USER}/connectedTest`;
  static USER_UPDATE = BackUrlConstant.USER + '/update';
  static CHANGE_PASSWORD = BackUrlConstant.USER + '/changePassword';
  static PERMISSIONS_LIST = BackUrlConstant.USER + '/permissionList';
  static USER_SEARCH = BackUrlConstant.USER + '/search';
  static USER_AUTEURS = BackUrlConstant.USER + '/auteurs';
  static USER_AOAP = BackUrlConstant.USER + '/aoap';
  static USER_PLANNING = BackUrlConstant.USER + '/planning';

  //OuvrageController
  static OUVRAGE = environment.baseUrl + '/ouvrage';
  static OUVRAGE_FILTER = BackUrlConstant.OUVRAGE + '/filter';
  static OUVRAGE_FILTER_SAVE = BackUrlConstant.OUVRAGE + '/filterAndSave';
  static OUVRAGE_EXPORT = BackUrlConstant.OUVRAGE + '/export';

  //SurveillanceController
  static SURVEILLANCE = environment.baseUrl + '/surveillance';
  static SURVEILLANCE_FILTER = BackUrlConstant.SURVEILLANCE + '/filter';
  static SURVEILLANCE_FILTER_SAVE = BackUrlConstant.SURVEILLANCE + '/filterAndSave';
  static SURVEILLANCE_EXPORT = BackUrlConstant.SURVEILLANCE + '/export';
  static SURVEILLANCE_OUVRAGE = BackUrlConstant.SURVEILLANCE + '/ouvrage';

  //BouclageController
  static BOUCLAGE = environment.baseUrl + '/bouclage';
  static BOUCLAGE_FILTER = BackUrlConstant.BOUCLAGE + '/filter';
  static BOUCLAGE_FILTER_SAVE = BackUrlConstant.BOUCLAGE + '/filterAndSave';
  static BOUCLAGE_EXPORT = BackUrlConstant.BOUCLAGE + '/export';
  static BOUCLAGE_BATCH_ACTION = BackUrlConstant.BOUCLAGE + '/batchAction';
  static BOUCLAGE_OUVRAGE = BackUrlConstant.BOUCLAGE + '/ouvrage';

  //FilterController
  static FILTER = environment.baseUrl + '/filter';
  static FILTER_UTILISATEUR_FILTER = BackUrlConstant.FILTER + '/utilisateurFilter';
  static FILTER_REGION = BackUrlConstant.FILTER + '/region';
  static FILTER_DEPARTEMENT = BackUrlConstant.FILTER + '/departement';
  static FILTER_COMMUNE = BackUrlConstant.FILTER + '/commune';
  static FILTER_COMMUNE_CHECK = BackUrlConstant.FILTER + '/communeCheck';
  static FILTER_REGION_INFRAPOLE_SECTEUR_UO_LIGNE = BackUrlConstant.FILTER + '/regionInfrapoleSecteurUoLigne';
  static FILTER_REGION_INFRAPOLE_SECTEUR_UO = BackUrlConstant.FILTER + '/regionInfrapoleSecteurUo';
  static FILTER_PROPRIETAIRE = BackUrlConstant.FILTER + '/proprietaire';
  static FILTER_OUVRAGE_NAME = BackUrlConstant.FILTER + '/ouvrageName';
  static FILTER_REFERENT_TECHNIQUE_REGIONAL = BackUrlConstant.FILTER + '/refTechRegion';
  static FILTER_SEGMENT_GESTION = BackUrlConstant.FILTER + '/segmentGestion';
  static FILTER_COMPTE_ANALYTIQUE = BackUrlConstant.FILTER + '/compteAnalytique';

  static FILTER_INFRAPOLE = BackUrlConstant.FILTER + '/infrapole';
  static FILTER_SECTEUR = BackUrlConstant.FILTER + '/secteur';
  static FILTER_UO = BackUrlConstant.FILTER + '/uo';
  static FILTER_LIGNE = BackUrlConstant.FILTER + '/ligne';
  static FILTER_LIGNE_SAVE = BackUrlConstant.FILTER_LIGNE + '/save';
  static FILTER_LIGNE_PORTION = BackUrlConstant.FILTER_LIGNE + '/portion';
  static FILTER_LIGNE_SEARCH = BackUrlConstant.FILTER_LIGNE + '/search';
  static FILTER_PORTION = BackUrlConstant.FILTER + '/portion';
  static FILTER_GEOMETRIE = BackUrlConstant.FILTER + '/geometrie';
  static FILTER_GEOMETRIE_SAVE = BackUrlConstant.FILTER_GEOMETRIE + '/save';
  static FILTER_GEOMETRIE_SEARCH = BackUrlConstant.FILTER_GEOMETRIE + '/search';
  static FILTER_CATEGORIE = BackUrlConstant.FILTER + '/categorie';
  static FILTER_TYPE = BackUrlConstant.FILTER + '/type';
  static FILTER_GEOMETRIE_TYPE = BackUrlConstant.FILTER + '/geometrieType';


  //ColumnController
  static COLUMN = environment.baseUrl + '/column';
  static COLUMN_ORDER = BackUrlConstant.COLUMN + '/order';

  // ProprietaireController
  static PROPRIETAIRE = environment.baseUrl + '/proprietaire';
  static PROPRIETAIRE_OUVRAGE = BackUrlConstant.PROPRIETAIRE + '/ouvrage';
  static PROPRIETAIRE_DISPONIBLE = `${BackUrlConstant.PROPRIETAIRE}/disponible`;

  // ParamTemplateController
  static PARAM_TEMPLATE = `${environment.baseUrl}/template`;

  // GeneralController
  static GENERAL = `${environment.baseUrl}/general`;
  static GENERAL_SAVE = `${BackUrlConstant.GENERAL}/save`;

  // ArchiveController
  static ARCHIVE = environment.baseUrl + '/archive';
  static ARCHIVE_FILTER = BackUrlConstant.ARCHIVE + '/filter';
  static ARCHIVE_FILTER_SAVE = BackUrlConstant.ARCHIVE + '/filterAndSave';
  static ARCHIVE_EXPORT = BackUrlConstant.ARCHIVE + '/export';
  static ARCHIVE_SEARCH = BackUrlConstant.ARCHIVE + '/search';
  static BOITE = BackUrlConstant.ARCHIVE + '/boite';
  static ARCHIVE_OUVRAGE = BackUrlConstant.ARCHIVE + '/ouvrage';

  // VehiculeController
  static VEHICULE = environment.baseUrl + '/vehicule';
  static PLANNING_VEHICULE = BackUrlConstant.VEHICULE + '/planning';
  static BY_VEHICULE = '/vehicule';
  static BY_UTILISATEUR = '/utilisateur';
  static BY_DATE = '/date';

  // MaterielController
  static MATERIEL = environment.baseUrl + '/materiel';
  static PLANNING_MATERIEL = BackUrlConstant.MATERIEL + '/planning';
  static BY_MATERIEL = '/materiel';
}
