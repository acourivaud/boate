import { FrontUrlConstant } from './../../../constant/FrontUrlConstant';
import { Router } from '@angular/router';
import { Utilisateur } from './../../../model/utilisateur';
import { ApiService } from './../../../service/api.service';
import { Component, Input, OnInit } from '@angular/core';

import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { AnalyticsService } from '../../../@core/utils';
import { LayoutService } from '../../../@core/utils';

import { filter, map } from 'rxjs/operators';
import { UtilisateurService } from '../../../service/utilisateur.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  @Input() position = 'normal';

  user: Utilisateur;

  profilMenu = [{ title: 'Profil' }, { title: 'Déconnexion' }];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private utilisateurService: UtilisateurService,
              private analyticsService: AnalyticsService,
              private layoutService: LayoutService,
              private apiService: ApiService,
              public router: Router) {
  }

  ngOnInit() {
    this.user = this.utilisateurService.getUtilisateurConnected();

    this.menuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'profil-menu'),
        map(({ item: { title } }) => title),
      )
      .subscribe(title => {
        if (title === "Déconnexion") {
          this.utilisateurService.logout().subscribe(data => {
            console.log(data);
          });
        }
        if (title === "Profil") {
          this.router.navigate([FrontUrlConstant.PATH_TO_PROFIL]);
        }
      });
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }
}
