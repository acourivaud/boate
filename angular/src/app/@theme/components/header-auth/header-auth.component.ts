import { ApiService } from './../../../service/api.service';
import { Component, Input, OnInit } from '@angular/core';

import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserData } from '../../../@core/data/users';
import { AnalyticsService } from '../../../@core/utils';
import { LayoutService } from '../../../@core/utils';


@Component({
  selector: 'ngx-header-auth',
  styleUrls: ['./header-auth.component.scss'],
  templateUrl: './header-auth.component.html',
})
export class HeaderAuthComponent implements OnInit {

  @Input() position = 'normal';

  user: any;

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private userService: UserData,
              private analyticsService: AnalyticsService,
              private layoutService: LayoutService,
              private apiService: ApiService) {
  }

  ngOnInit() {

  }
}
