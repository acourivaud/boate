import { UtilisateurService } from './service/utilisateur.service';
import { BackUrlConstant } from './constant/BackUrlConstant';
import { ApiService } from './service/api.service';
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet (activate)="changeOfRoutes()"></router-outlet>',
})
export class AppComponent implements OnInit {

  constructor(
    private apiService: ApiService,
    private analytics: AnalyticsService,
    private utilisateurService: UtilisateurService) {
  }

  ngOnInit(): void {
    this.analytics.trackPageViews();
  }

  changeOfRoutes () {
    this.apiService.getUrlWOT(BackUrlConstant.USER_CONNECTED_TEST).subscribe(user => {
      if (!this.utilisateurService.checkLogin()) {
        this.apiService.disconnectUser();
      }
    }, err => {
      this.apiService.disconnectUser();
    });
  }
}
