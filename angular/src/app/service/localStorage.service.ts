import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

  applicationName: string = 'BOATE_';
  inMemoryStorage: {
    [key: string]: string;
  } = {};
  constructor() {}

  isSupported() {
    try {
      const testKey = '__some_random_key_you_are_not_going_to_use__';
      localStorage.setItem(testKey, testKey);
      localStorage.removeItem(testKey);
      return true;
    } catch (e) {
      return false;
    }
  }

  clear(): void {
    if (this.isSupported()) {
      localStorage.clear();
    } else {
      this.inMemoryStorage = {};
    }
  }

  clearFromApplicationName(): void {
    const self = this;
    Object.keys(localStorage).forEach(function(key){
      if (key.startsWith(self.applicationName)) {
        self.removeItem(key, true);
      }
   });
  }

  getItem(name: string): string | null {
    if (this.isSupported()) {
      return localStorage.getItem(this.applicationName + name);
    }
    if (this.inMemoryStorage.hasOwnProperty(name)) {
      return this.inMemoryStorage[name];
    }
    return null;
  }

  key(index: number): string | null {
    if (this.isSupported()) {
      return localStorage.key(index);
    } else {
      return Object.keys(this.inMemoryStorage)[index] || null;
    }
  }

  removeItem(name: string, fullName?: boolean): void {
    if (this.isSupported()) {
      if (fullName) {
        localStorage.removeItem(name);
      }else {
        localStorage.removeItem(this.applicationName + name);
      }
    } else {
      delete this.inMemoryStorage[name];
    }
  }

  setItem(name: string, value: string): void {
    if (this.isSupported()) {
      localStorage.setItem(this.applicationName + name, value);
    } else {
      this.inMemoryStorage[name] = String(value); // not everyone uses TypeScript
    }
  }
}
