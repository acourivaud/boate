import { ParamValue } from '../model/paramValue';
import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { URLSearchParams } from '@angular/http';

import { BackUrlConstant } from '../constant/BackUrlConstant';
import { ApiService } from './api.service';

@Injectable()
export class ColumnService {

	constructor(
		private apiService: ApiService,
	) {}

	getColumnsForConnectedUser(page: string) {
		let params = new URLSearchParams();
		params.append("page", page ? page : "null");
        return Observable.create((observer: Observer<string>) => {
            this.apiService.getUrlWithParameter(BackUrlConstant.COLUMN, params).subscribe(data => {
                observer.next(data);
                observer.complete();
            }, err => {
                observer.error(err);
            })
        });
	}

	saveColumnsForConnectedUser(filters: ParamValue[]) {
        return Observable.create((observer: Observer<string>) => {
            this.apiService.postUrl(BackUrlConstant.COLUMN, filters).subscribe(data => {
                observer.next(data);
                observer.complete();
            }, err => {
                observer.error(err);
            });
        });
    }

    getColumnsOrderForConnectedUser(page: string) {
		let params = new URLSearchParams();
		params.append("page", page ? page : "null");
        return Observable.create((observer: Observer<string>) => {
            this.apiService.getUrlWithParameter(BackUrlConstant.COLUMN_ORDER, params).subscribe(data => {
                observer.next(data);
                observer.complete();
            }, err => {
                observer.error(err);
            })
        });
	}
    
    saveColumnsOrderForConnectedUser(filters: ParamValue[]) {
        return Observable.create((observer: Observer<string>) => {
            this.apiService.postUrl(BackUrlConstant.COLUMN_ORDER, filters).subscribe(data => {
                observer.next(data);
                observer.complete();
            }, err => {
                observer.error(err);
            });
        });
	}
}