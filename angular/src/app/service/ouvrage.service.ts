import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable, Observer } from 'rxjs';

import { BackUrlConstant } from '../constant/BackUrlConstant';
import { ParamValue } from '../model/paramValue';
import { ApiService } from './api.service';
import { Ouvrage } from '../model/ouvrage';

@Injectable()
export class OuvrageService {

  constructor(
    private apiService: ApiService,
  ) {}

  getOuvrages(save: boolean, filters ?: ParamValue[], timeoutInSec?: number) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(save ? BackUrlConstant.OUVRAGE_FILTER_SAVE : BackUrlConstant.OUVRAGE_FILTER, filters, timeoutInSec, null).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  exportOuvragesPostFilters(filters ?: ParamValue[]) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(BackUrlConstant.OUVRAGE_EXPORT, filters).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  exportOuvragesGetResult(guid: string) {
    const params = new URLSearchParams();
    params.append('guid', guid);
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithParameter(BackUrlConstant.OUVRAGE_EXPORT, params).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  getOuvrage(id: number) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithIdParam(BackUrlConstant.OUVRAGE, id.toString()).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  saveOuvrage(ouvrage: Ouvrage) {
    return Observable.create((observer: Observer<string>) => {
        this.apiService.postUrl(BackUrlConstant.OUVRAGE, ouvrage).subscribe(data => {
            observer.next(data);
            observer.complete();
        }, err => {
            observer.error(err);
        });
    });
  }

  deleteOrArchiveOuvrage(ouvrage: Ouvrage) {
    return Observable.create((observer: Observer<string>) => {
        this.apiService.deleteUrl(BackUrlConstant.OUVRAGE, ouvrage).subscribe(data => {
            observer.next(data);
            observer.complete();
        }, err => {
            observer.error(err);
        });
    });
  }
}
