import { DeleteAction } from './../model/deleteAction';
import { BouclageBatchAction } from './../model/bouclageBatchAction';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable, Observer } from 'rxjs';

import { BackUrlConstant } from '../constant/BackUrlConstant';
import { ParamValue } from '../model/paramValue';
import { ApiService } from './api.service';
import { Bouclage } from '../model/bouclage';

@Injectable()
export class BouclageService {

  constructor(
    private apiService: ApiService,
  ) {}

  getBouclage(idBouclage: string) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithIdParam(BackUrlConstant.BOUCLAGE, idBouclage).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  saveBouclage(bouclage: Bouclage) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(BackUrlConstant.BOUCLAGE, bouclage).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  getBouclages(page: string, save: boolean, filters: ParamValue[], timeoutInSec: number = 10) {
    if (!(filters && filters != null)) {
      filters = [];
    }
    filters.push(new ParamValue('bouclage', page));
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(save ? BackUrlConstant.BOUCLAGE_FILTER_SAVE : BackUrlConstant.BOUCLAGE_FILTER, filters, timeoutInSec).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  exportBouclagesPostFilters(filters ? : ParamValue[]) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(BackUrlConstant.BOUCLAGE_EXPORT, filters).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  applyBatchAction(bouclageBatchAction: BouclageBatchAction) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(BackUrlConstant.BOUCLAGE_BATCH_ACTION, bouclageBatchAction).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  delete(deleteAction: DeleteAction) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.deleteUrl(BackUrlConstant.BOUCLAGE, deleteAction).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  exportBouclagesGetResult(guid: string) {
    const params = new URLSearchParams();
    params.append('guid', guid);
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithParameter(BackUrlConstant.BOUCLAGE_EXPORT, params).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      })
    });
  }
}
