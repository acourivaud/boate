import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable, Observer } from 'rxjs';

import { BackUrlConstant } from '../constant/BackUrlConstant';
import { ArchiveDossier } from '../model/archiveDossier';
import { ParamValue } from '../model/paramValue';
import { ApiService } from './api.service';

@Injectable()
export class ArchiveService {

  constructor(
    private apiService: ApiService,
  ) {}

  getArchives(save: boolean, filters ? : ParamValue[], timeoutInSec?: number) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(save ? BackUrlConstant.ARCHIVE_FILTER_SAVE : BackUrlConstant.ARCHIVE_FILTER, filters, timeoutInSec, null).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      })
    });
  }

  getArchive(id: number) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithIdParam(BackUrlConstant.ARCHIVE, id.toString()).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  saveArchive(archiveDossier: ArchiveDossier) {
    return Observable.create((observer: Observer<string>) => {
        this.apiService.postUrl(BackUrlConstant.ARCHIVE, archiveDossier).subscribe(data => {
            observer.next(data);
            observer.complete();
        }, err => {
            observer.error(err);
        });
    });
  }

  deleteArchive(archiveDossier: ArchiveDossier) {
    return Observable.create((observer: Observer<string>) => {
        this.apiService.deleteUrl(BackUrlConstant.ARCHIVE, archiveDossier).subscribe(data => {
            observer.next(data);
            observer.complete();
        }, err => {
            observer.error(err);
        });
    });
  }

  exportArchivesPostFilters(filters ? : ParamValue[]) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(BackUrlConstant.ARCHIVE_EXPORT, filters).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  exportArchivesGetResult(guid: string) {
    const params = new URLSearchParams();
    params.append("guid", guid);
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithParameter(BackUrlConstant.ARCHIVE_EXPORT, params).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  getBoitesSearch(searchText: string) {
    const params = new URLSearchParams();
    params.append('typedText', searchText);
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithParameter(BackUrlConstant.ARCHIVE_SEARCH, params).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }
}
