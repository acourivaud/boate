import { Geometrie } from './../model/geometrie';
import { Portion } from './../model/portion';
import { Secteur } from './../model/secteur';
import { Region } from './../model/region';
import { ParamValue } from './../model/paramValue';
import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { URLSearchParams } from '@angular/http';

import { BackUrlConstant } from '../constant/BackUrlConstant';
import { ApiService } from './api.service';
import { Infrapole } from '../model/infrapole';
import { UniteOperationnelle } from '../model/uniteOperationnelle';
import { Ligne } from '../model/ligne';

@Injectable()
export class FilterService {

	constructor(
		private apiService: ApiService,
	) {}

	getFiltersForConnectedUser(page: string) {
		let params = new URLSearchParams();
		params.append("page", page ? page : "null");
        return Observable.create((observer: Observer<string>) => {
            this.apiService.getUrlWithParameter(BackUrlConstant.FILTER_UTILISATEUR_FILTER, params).subscribe(data => {
                observer.next(data);
                observer.complete();
            }, err => {
                observer.error(err);
            })
        });
	}

	saveFiltersForConnectedUser(filters?: ParamValue[]) {
        return Observable.create((observer: Observer<string>) => {
            this.apiService.postUrl(BackUrlConstant.FILTER_UTILISATEUR_FILTER, filters).subscribe(data => {
                observer.next(data);
                observer.complete();
            }, err => {
                observer.error(err);
            });
        });
	}

	getLignesSuggested(typedText: string) {
		if (typedText && typedText.length > 2) {
			let params = new URLSearchParams();
			params.append("typedText", typedText);
			return Observable.create((observer: Observer < string > ) => {
				this.apiService.getUrlWithParameter(BackUrlConstant.FILTER_LIGNE_SEARCH, params).subscribe(data => {
					observer.next(data);
					observer.complete();
				}, err => {
					observer.error(err);
				})
			});
		}
	}

	getLignePortions(ligne: Ligne) {
		return Observable.create((observer: Observer < string > ) => {
			this.apiService.getUrlWithIdParam(BackUrlConstant.FILTER_LIGNE_PORTION, ligne.id.toString()).subscribe(data => {
				observer.next(data);
				observer.complete();
			}, err => {
				observer.error(err);
			})
		});
	}

	getRegions() {
		return Observable.create((observer: Observer < string > ) => {
			this.apiService.getUrl(BackUrlConstant.FILTER_REGION).subscribe(data => {
				observer.next(data);
				observer.complete();
			}, err => {
				observer.error(err);
			})
		});
	}

	getGeometriesSuggested(typedText: string) {
		if (typedText && typedText.length > 2) {
			let params = new URLSearchParams();
			params.append("typedText", typedText);
			return Observable.create((observer: Observer < string > ) => {
				this.apiService.getUrlWithParameter(BackUrlConstant.FILTER_GEOMETRIE_SEARCH, params).subscribe(data => {
					observer.next(data);
					observer.complete();
				}, err => {
					observer.error(err);
				})
			});
		}
	}

	getDepartements(regionId: string) {
		let params = new URLSearchParams();
		params.append("regionId", regionId ? regionId : "null");
        return Observable.create((observer: Observer<string>) => {
            this.apiService.getUrlWithParameter(BackUrlConstant.FILTER_DEPARTEMENT, params).subscribe(data => {
                observer.next(data);
                observer.complete();
            }, err => {
                observer.error(err);
            })
        });
	}

	getCommunes(regionId: string, departementId: string, typedText: string) {
		if (typedText && typedText != null && typedText.length > 2) {
			let params = new URLSearchParams();
			params.append("regionId", regionId ? regionId : "null");
			params.append("departementId", departementId ? departementId : "null");
			params.append("typedText", "#" + typedText);
			return Observable.create((observer: Observer<string>) => {
				this.apiService.getUrlWithParameter(BackUrlConstant.FILTER_COMMUNE, params).subscribe(data => {
					observer.next(data);
					observer.complete();
				}, err => {
					observer.error(err);
				})
			});
		}
	}

	checkCommune(regionId: string, departementId: string, communeId: string) {
		if (communeId && communeId != null) {
			let params = new URLSearchParams();
			params.append("regionId", regionId ? regionId : "null");
			params.append("departementId", departementId ? departementId : "null");
			params.append("communeId", communeId);
			return Observable.create((observer: Observer<string>) => {
				this.apiService.getUrlWithParameter(BackUrlConstant.FILTER_COMMUNE_CHECK, params).subscribe(data => {
					observer.next(data);
					observer.complete();
				}, err => {
					observer.error(err);
				})
			});
		}
	}

	getRegionInfrapoleSecteurUoLigne(regionId: string, infrapoleId: string, secteurId: string, uoId: string,) {
		let params = new URLSearchParams();
		params.append("regionId", regionId ? regionId : "null");
		params.append("infrapoleId", infrapoleId ? infrapoleId : "null");
		params.append("secteurId", secteurId ? secteurId : "null");
		params.append("uoId", uoId ? uoId : "null");
        return Observable.create((observer: Observer<string>) => {
            this.apiService.getUrlWithParameter(BackUrlConstant.FILTER_REGION_INFRAPOLE_SECTEUR_UO_LIGNE, params).subscribe(data => {
                observer.next(data);
                observer.complete();
            }, err => {
                observer.error(err);
            })
        });
	}

	getRegionInfrapoleSecteurUo(regionId: string, infrapoleId: string, secteurId: string,) {
		let params = new URLSearchParams();
		params.append("regionId", regionId ? regionId : "null");
		params.append("infrapoleId", infrapoleId ? infrapoleId : "null");
		params.append("secteurId", secteurId ? secteurId : "null");
        return Observable.create((observer: Observer<string>) => {
            this.apiService.getUrlWithParameter(BackUrlConstant.FILTER_REGION_INFRAPOLE_SECTEUR_UO, params).subscribe(data => {
                observer.next(data);
                observer.complete();
            }, err => {
                observer.error(err);
            })
        });
	}

	getProprietaires() {
		return Observable.create((observer: Observer < string > ) => {
			this.apiService.getUrl(BackUrlConstant.FILTER_PROPRIETAIRE).subscribe(data => {
				observer.next(data);
				observer.complete();
			}, err => {
				observer.error(err);
			})
		});
	}

	getOuvragesName(typedText: string) {
		if (typedText && typedText != null && typedText.length > 2) {
			let params = new URLSearchParams();
			params.append("typedText", "#" + typedText);
			return Observable.create((observer: Observer<string>) => {
				this.apiService.getUrlWithParameter(BackUrlConstant.FILTER_OUVRAGE_NAME, params).subscribe(data => {
					observer.next(data);
					observer.complete();
				}, err => {
					observer.error(err);
				})
			});
		}
	}

	getReferentsTechniquesRegionaux(regionId: string, fonction: string) {
		let params = new URLSearchParams();
		params.append("regionId", regionId && regionId != null ? regionId : "null");
		params.append("fonction", fonction && fonction != null ? fonction : "null");
		return Observable.create((observer: Observer < string > ) => {
			this.apiService.getUrlWithParameter(BackUrlConstant.FILTER_REFERENT_TECHNIQUE_REGIONAL, params).subscribe(data => {
				observer.next(data);
				observer.complete();
			}, err => {
				observer.error(err);
			})
		});
	}

	getUsersFilteredByResponsable(fonction: string) {
		const params = new URLSearchParams();
		params.append('fonction', fonction && fonction != null ? fonction : 'null');
		return Observable.create((observer: Observer < string > ) => {
			this.apiService.getUrlWithParameter(BackUrlConstant.USER_BY_RESPONSABLE, params).subscribe(data => {
				observer.next(data);
				observer.complete();
			}, err => {
				observer.error(err);
			})
		});
	}

	getSegmentsGestion() {
		return Observable.create((observer: Observer < string > ) => {
			this.apiService.getUrl(BackUrlConstant.FILTER_SEGMENT_GESTION).subscribe(data => {
				observer.next(data);
				observer.complete();
			}, err => {
				observer.error(err);
			})
		});
	}

	getComptesAnalytiques() {
		return Observable.create((observer: Observer < string > ) => {
			this.apiService.getUrl(BackUrlConstant.FILTER_COMPTE_ANALYTIQUE).subscribe(data => {
				observer.next(data);
				observer.complete();
			}, err => {
				observer.error(err);
			})
		});
	}
}
