import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';

import { BackUrlConstant } from '../constant/BackUrlConstant';
import { ApiService } from './api.service';

@Injectable()
export class ParamService {

  constructor(
    private apiService: ApiService,
  ) {}

  getByTemplateName(templateName: string) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithIdParam(BackUrlConstant.PARAM_TEMPLATE, templateName).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }
}
