import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable, Observer } from 'rxjs';

import { BackUrlConstant } from '../constant/BackUrlConstant';
import { ParamValue } from '../model/paramValue';
import { ApiService } from './api.service';

@Injectable()
export class GeneralService {

  constructor(
    private apiService: ApiService,
  ) {}

  getUrlWithParamValues(paramValues: ParamValue[]) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(BackUrlConstant.GENERAL, paramValues).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }
  postUrlWithJson(json: any) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(BackUrlConstant.GENERAL_SAVE, json).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  deleteUrlNew(object: any) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.deleteUrl(BackUrlConstant.GENERAL, object).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    })
  }

  getUrlWithIdParam(url: string, id: string, id2 ? : string) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithIdParam(url, id, id2).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  getUrl(url: string) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrl(url).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  getUrlWithParameter(url: string, paramsValues: ParamValue[]) {
    let params = new URLSearchParams();
    for (let paramValue of paramsValues) {
      params.append(paramValue.param, paramValue.value);
    }
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithParameter(url, params).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  postUrlWithObjectArray(url: string, object: any) {
    console.log(object);
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(url, object).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  postUrlWithObject(url: string, object: any) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(url, object).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  deleteUrl(url: string, object: any) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.deleteUrl(url, object).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }
}
