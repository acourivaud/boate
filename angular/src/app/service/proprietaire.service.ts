import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';

import { BackUrlConstant } from '../constant/BackUrlConstant';
import { ApiService } from './api.service';
import { OuvrageProprietaire } from '../model/ouvrageProprietaire';

@Injectable()
export class ProprietaireService {

  constructor(
    private apiService: ApiService,
  ) {}

  getByOuvrageId(id: number) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithIdParam(BackUrlConstant.PROPRIETAIRE_OUVRAGE, id.toString()).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  saveOuvrageProprietaire(ouvrageProprietaire: OuvrageProprietaire[]) {
    return Observable.create((observer: Observer<string>) => {
        this.apiService.postUrl(BackUrlConstant.PROPRIETAIRE_OUVRAGE, ouvrageProprietaire).subscribe(data => {
            observer.next(data);
            observer.complete();
        }, err => {
            observer.error(err);
        });
    });
  }

  deleteOuvrageProprietaire(ouvrageProprietaire: OuvrageProprietaire) {
    return Observable.create((observer: Observer<string>) => {
        this.apiService.deleteUrl(BackUrlConstant.PROPRIETAIRE_OUVRAGE, ouvrageProprietaire).subscribe(data => {
            observer.next(data);
            observer.complete();
        }, err => {
            observer.error(err);
        });
    });
  }
}
