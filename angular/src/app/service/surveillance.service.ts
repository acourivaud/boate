import { Surveillance } from './../model/surveilllance';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable, Observer } from 'rxjs';

import { BackUrlConstant } from '../constant/BackUrlConstant';
import { ParamValue } from '../model/paramValue';
import { ApiService } from './api.service';

@Injectable()
export class SurveillanceService {

  constructor(
    private apiService: ApiService,
  ) {}

  getSurveillances(save: boolean, filters ? : ParamValue[], timeoutInSec: number = 10) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(save ? BackUrlConstant.SURVEILLANCE_FILTER_SAVE : BackUrlConstant.SURVEILLANCE_FILTER, filters, timeoutInSec).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  exportSurveillancesPostFilters(filters ? : ParamValue[]) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(BackUrlConstant.SURVEILLANCE_EXPORT, filters).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  exportSurveillancesGetResult(guid: string) {
    const params = new URLSearchParams();
    params.append('guid', guid);
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithParameter(BackUrlConstant.SURVEILLANCE_EXPORT, params).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      })
    });
  }

  getSurveillance(id: number) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithIdParam(BackUrlConstant.SURVEILLANCE, id.toString()).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  saveSurveillance(surveillance: Surveillance) {
    return Observable.create((observer: Observer<string>) => {
        this.apiService.postUrl(BackUrlConstant.SURVEILLANCE, surveillance).subscribe(data => {
            observer.next(data);
            observer.complete();
        }, err => {
            observer.error(err);
        });
    });
  }

  archiveSurveillance(surveillance: Surveillance) {
    return Observable.create((observer: Observer<string>) => {
        this.apiService.deleteUrl(BackUrlConstant.SURVEILLANCE, surveillance).subscribe(data => {
            observer.next(data);
            observer.complete();
        }, err => {
            observer.error(err);
        });
    });
  }
}
