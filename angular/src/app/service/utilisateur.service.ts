import { Injectable } from '@angular/core';
import { Headers, URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';
import { Observable, Observer } from 'rxjs';
import { Md5 } from 'ts-md5/dist/md5';

import { environment } from '../../environments/environment';
import { BackUrlConstant } from '../constant/BackUrlConstant';
import { FrontUrlConstant } from '../constant/FrontUrlConstant';
import { Utilisateur } from '../model/utilisateur';
import { ApiService } from './api.service';
import { LocalStorageService } from './localStorage.service';
import { Login } from '../model/login';

@Injectable()
export class UtilisateurService {

  constructor(
    private router: Router,
    private apiService: ApiService,
    private localStorageService: LocalStorageService,
  ) {}

  canActivate() {
    if (this.checkLogin() || this.isChangePasswordUrl()) {
      return true;
    }
    this.router.navigate([FrontUrlConstant.PATH_TO_LOGIN]);
    return false;
  }

  /**
   * Authentification d'un utilisateur avec un pseudo et un mot de passe
   * En deux temps : Authentification et récupération de l'utilisateur connecté
   * @param user (login et password)
   */
  login(user) {
    //const passwordEncrypt = Md5.hashStr(user.password);
    const test = new Login(null);
    test.username = user.login;
    test.password = user.password;
    //const body = `username=${user.login}&password=${user.password}`;
    const headers = new Headers();
    //headers.append('Access-Control-Allow-Origin', environment.baseUrl);
    headers.append('Content-Type', 'application/json');

    return Observable.create((observer: Observer < string > ) => {

      // Authentification
      this.apiService.postUrl(BackUrlConstant.LOGIN_URL, test, 10, headers).subscribe(token => {
        this.localStorageService.setItem('token', token.accessToken); // Sauvegarde du token

        // Fetch l'utilisateur connecté
        this.apiService.getUrl(BackUrlConstant.USER_CONNECTED).subscribe(userJson => {
          this.localStorageService.setItem('user', JSON.stringify(userJson)); // Sauvegarde de l'utilisateur
          observer.next(userJson);
          observer.complete();
        }, err => {
          observer.error(err);
        });
      }, err => {
        observer.error(err);
      });
    });
  }

  logout() {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrl(BackUrlConstant.LOGOUT_URL).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  changePassword(token) {
    const body = `token=${token}`;
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', environment.baseUrl);
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('X-XSRF-TOKEN', this.localStorageService.getItem('token'));
    const myParams = new URLSearchParams();
    myParams.append('token', token);

    // appel de l'api utilisateur/changePassword
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithParameter(BackUrlConstant.CHANGE_PASSWORD, myParams, headers).subscribe(data => {
        const user: any = {
          username: '',
          password: '',
        };

        user.login = data.login;
        user.password = token;
        this.login(user).subscribe(data2 => {
            observer.next(data2);
          },
          error => {
            observer.error(error);
          },
        );
      }, error => {
        observer.error(error);
      });
    });
  }

  /**
   * Modification de l'utilisateur en base de donnée
   * Permet de changer le mot de passe à la première connexion
   * @param utilisateur
   */
  updateUtilisateur(utilisateur: Utilisateur) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(BackUrlConstant.USER_UPDATE, utilisateur).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  /**
   * Récupération de l'utilisateur connecté depuis le localstorage
   */
  getUtilisateurConnected(): Utilisateur {
    return this.localStorageService.getItem('user') ? new Utilisateur(JSON.parse(this.localStorageService.getItem('user'))) : null;
  }

  getUtilisateurConnectedFromBack() {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrl(BackUrlConstant.USER_CONNECTED).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  getUtilisateurById(id: string) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithIdParam(BackUrlConstant.USER_ID, id).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  getAllPermissions() {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrl(BackUrlConstant.PERMISSIONS_LIST).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  /**
   * Récupération de l'utilisateur connecté depuis le localstorage
   */
  checkLogin(): boolean {
    return this.localStorageService.getItem('token') != null;
  }

  isChangePasswordUrl(): boolean {
    const currentUrl: string = window.location.href;
    const substring: string = 'changePassword';
    return currentUrl.indexOf(substring) !== -1;
  }

  saveUtilisateur(utilisateur: Utilisateur) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.postUrl(BackUrlConstant.USER, utilisateur).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  updateLocalStorageIfUserMatchesConnectedOne(utilisateur: Utilisateur): void {
    const utilisateurConnected: Utilisateur = this.getUtilisateurConnected();
    if (utilisateurConnected.idUtilisateur === utilisateur.idUtilisateur) {
      const user = {
        idUtilisateur: utilisateur.idUtilisateur,
        login: utilisateur.login,
        nom: utilisateur.nom,
        prenom: utilisateur.prenom,
        nbLignesTableau: utilisateur.nbLignesTableau,
        pageAccueil: utilisateur.pageAccueil,
        fonction: utilisateur.fonction,
        up: utilisateur.up,
        secteur: utilisateur.secteur,
        infrapole: utilisateur.infrapole,
        region: utilisateur.region,
        entite: utilisateur.entite,
        telephoneFixe: utilisateur.telephoneFixe,
        telephoneMobile: utilisateur.telephoneMobile,
        fax: utilisateur.fax,
        email: utilisateur.email,
        dateFinMission: utilisateur.dateFinMission,
        urlSig: utilisateur.urlSig,
        ssid: utilisateur.ssid,
        affichageAlerte: utilisateur.affichageAlerte,
        consultationReleaseNotes: utilisateur.consultationReleaseNotes,
        permissions: utilisateur.permissions,
        fullName: utilisateur.fullName
      };
      this.localStorageService.setItem('user', JSON.stringify(user));
    }
  }

  deleteUtilisateur(utilisateur: Utilisateur) {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.deleteUrl(BackUrlConstant.USER, utilisateur).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  getUtilisateurs(searchText: string) {
    const params = new URLSearchParams();
    params.append('typedText', searchText);
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrlWithParameter(BackUrlConstant.USER_SEARCH, params).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  getAuteurs() {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrl(BackUrlConstant.USER_AUTEURS).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }

  getAoaps() {
    return Observable.create((observer: Observer < string > ) => {
      this.apiService.getUrl(BackUrlConstant.USER_AOAP).subscribe(data => {
        observer.next(data);
        observer.complete();
      }, err => {
        observer.error(err);
      });
    });
  }
}
