import 'rxjs/Rx';

import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response, ResponseContentType, URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map, timeout } from 'rxjs/operators';

import { FrontUrlConstant } from '../constant/FrontUrlConstant';
import { LocalStorageService } from './localStorage.service';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {

  constructor(
    public http: Http,
    public router: Router,
    private localStorageService: LocalStorageService,
  ) {}

  public headers: Headers;
  public options: RequestOptions;

  setHeaders() {
    this.headers = new Headers();
    this.headers.append('Access-Control-Allow-Origin', environment.baseUrl);
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', 'Bearer ' + this.localStorageService.getItem('token'));
  }

  setOptions(options) {
    this.options = new RequestOptions();
    this.options.headers = this.headers;
    this.options.withCredentials = true;
    this.options.body = options;
  }

  getUrl(url, headers ? ): Observable < any > {
    this.setHeaders();
    if (headers != null) this.headers = headers;
    this.setOptions(null);
    return this.http.get(url, this.options).pipe(
      map(this.extractData),
      timeout(10000),
      catchError(this.handleError.bind(this)), );
  }

  getUrlWOT(url, headers ? ): Observable < any > {
    this.setHeaders();
    if (headers != null) this.headers = headers;
    this.setOptions(null);
    return this.http.get(url, this.options).pipe(
      map(this.extractData),
      catchError(this.handleError.bind(this)), );
  }

  getUrlWithURLIdParam(url, object ? , headers ? ): Observable < any > {
    url = url + "/" + object;

    this.setHeaders();
    this.setOptions(null);
    if (headers != null) {
      this.headers = headers;
    }
    return this.http.get(url, this.options).pipe(
      map(this.extractData),
      timeout(10000),
      catchError(this.handleError.bind(this)), );
  }

  getUrlWithIdParam(url, id?: string, id2?: string , headers ? ): Observable < any > {
    let params: URLSearchParams = new URLSearchParams();
    params.set("id", id);
    params.set("id2", id2);
    this.setHeaders();
    this.setOptions(null);
    this.options.search = params;
    if (headers != null) {
      this.headers = headers;
    }
    return this.http.get(url, this.options).pipe(
      map(this.extractData),
      timeout(10000),
      catchError(this.handleError.bind(this)), );
  }

  getUrlWithParameter(url, myParams: URLSearchParams, headers ? ): Observable < any > {
    this.setHeaders();
    this.setOptions(null);
    this.options.search = myParams;
    if (headers != null) {
      this.headers = headers;
    }
    return this.http.get(url, this.options).pipe(
      map(this.extractData),
      timeout(10000),
      catchError(this.handleError.bind(this)), );
  }

  postUrl(url, object, timeoutInSec: number = 10, headers ? ): Observable < any > {
    this.setHeaders();
    if (headers != null) this.headers = headers;
    this.setOptions(null);
    return this.http.post(url, object, this.options).pipe(
      map(this.extractData),
      timeout(timeoutInSec * 1000),
      catchError(this.handleError.bind(this)));
  }

  deleteUrl(url, object, headers ? ): Observable < any > {
    this.setHeaders();
    if (headers != null) this.headers = headers;
    this.setOptions(object);
    return this.http.delete(url, this.options).pipe(
      map(this.extractData),
      timeout(10000),
      catchError(this.handleError.bind(this)), );
  }

  getUrlExcel(url, guid, headers ? ): Observable < any > {
    this.setHeaders();
    if (headers != null) this.headers = headers;
    this.setOptions(null);

    let params: URLSearchParams = new URLSearchParams();
    params.set("guid", guid);
    this.options.search = params;
    this.options.responseType = ResponseContentType.Blob;
    this.headers.append('Content-Type', 'application/octet-stream');
    return this.http.get(url, this.options).pipe(
      map(this.extractData),
      catchError(
        this.handleError.bind(this)
      ), );
  }

  getUrlPdf(url, id, typeOffre, detailsPrestations, montantsPrestations, headers ? ): Observable < any > {
    this.setHeaders();
    if (headers != null) this.headers = headers;
    this.setOptions(null);

    let params: URLSearchParams = new URLSearchParams();
    params.set("id", id);
    params.set("typeOffre", typeOffre);
    params.set("detailsPrestations", detailsPrestations);
    params.set("montantsPrestations", montantsPrestations);
    this.options.search = params;
    this.options.responseType = ResponseContentType.Blob;
    this.headers.append('Content-Type', 'application/octet-stream');
    return this.http.get(url, this.options).pipe(
      //.map(this.extractData)
      catchError(
        this.handleError.bind(this)
      ));
  }

  extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  handleError(error: Response | any) {
    let code: string;
    let msg: string;
    if (error instanceof Response) {
      if (!error.ok && error.status === 401) {
        code = error.status.toString();
      } else {
        const body = error.json() || '';
        const err = body.message || JSON.stringify(body);
        code = error.status.toString();
        msg = err;
      }

      // Serveur introuvable
      if (code === '0') {
        console.warn('Serveur introuvable');
        msg = 'Connexion au serveur impossible';
      }
      if (code === "401" && error.url.includes('api/auth/login')) {
        console.log('api service auth error');
        this.disconnectUser();
      }
      if (code === "401" && error["_body"]) {
        let body = JSON.parse(error["_body"]);
        msg = body["message"];
      }
    } else {
      code = 'inconnue';
      msg = error.message ? error.message : error.toString();
    }
    return observableThrowError({
      code: code,
      msg: msg
    });
  }

  disconnectUser() {
    console.warn("Déconnexion de l'utilisateur");
    //this.utilisateurService.logout().subscribe(data => {
     // console.log(data);
      console.log('test');
      if (this.localStorageService.getItem('token') != null) {
        this.localStorageService.clearFromApplicationName();
        this.router.navigate([FrontUrlConstant.PATH_TO_LOGIN]);
      }
    //})

  }

  getImageUrl(imageUrl: string): Observable < Blob > {
    return this.http.get(imageUrl, {
      responseType: ResponseContentType.Blob
    }).pipe(
      map((res: Response) => res.blob()));
  }
}
