import { PlanningModule } from './planning/planning.module';
import { CollaborateurModule } from './collaborateur/collaborateur.module';
import { NgModule } from '@angular/core';
import { NbMenuService } from '@nebular/theme';
import { TabMenuModule } from 'primeng/tabmenu';

import { ThemeModule } from '../@theme/theme.module';
import { UtilisateurService } from './../service/utilisateur.service';
import { AdministrationModule } from './administration/administration.module';
import { ArchiveModule } from './archive/archive.module';
import { ExpertiseModule } from './bouclage/expertise/expertise.module';
import { PeriodiqueModule } from './bouclage/periodique/periodique.module';
import { ProductionFsaModule } from './bouclage/production-fsa/production-fsa.module';
import { ProductionInclinoModule } from './bouclage/production-inclino/production-inclino.module';
import { ProductionTopoModule } from './bouclage/production-topo/production-topo.module';
import { SrSpOtModule } from './bouclage/sr-sp-ot/sr-sp-ot.module';
import { VisiteOuvrageModule } from './bouclage/visite-ouvrage/visite-ouvrage.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { MaterielModule } from './materiel/materiel.module';
import { MesEmpruntsModule } from './mes-emprunts/mes-emprunts.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { OuvrageModule } from './ouvrage/ouvrage.module';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { ProfilModule } from './profil/profil.module';
import { SurveillanceModule } from './surveillance/surveillance.module';
import { VehiculeModule } from './vehicule/vehicule.module';

const PAGES_COMPONENTS = [
  PagesComponent
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    OuvrageModule,
    SurveillanceModule,
    AdministrationModule,
    VehiculeModule,
    MaterielModule,
    ArchiveModule,
    ProfilModule,
    TabMenuModule,
    MesEmpruntsModule,
    PeriodiqueModule,
    ExpertiseModule,
    SrSpOtModule,
    ProductionFsaModule,
    ProductionTopoModule,
    ProductionInclinoModule,
    VisiteOuvrageModule,
    CollaborateurModule,
    PlanningModule
  ],
  declarations: [
    PAGES_COMPONENTS,
  ],
  providers: [
    NbMenuService,
    UtilisateurService,
  ],
})
export class PagesModule {
}
