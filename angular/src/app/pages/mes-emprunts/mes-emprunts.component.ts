import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbToastrService } from '@nebular/theme';

import { PageHelper } from '../../model/pageHelper';
import { ParamObject } from '../../model/paramObject';
import { ToastrHelper } from '../../model/toastrHelper';
import { GeneralService } from '../../service/general.service';
import { LocalStorageService } from '../../service/localStorage.service';
import { ParamService } from '../../service/param.service';
import { UtilisateurService } from '../../service/utilisateur.service';
import { Utilisateur } from '../../model/utilisateur';

@Component({
  selector: 'mes-emprunts',
  styleUrls: ['./mes-emprunts.component.scss'],
  templateUrl: './mes-emprunts.component.html',
})
export class MesEmpruntsComponent implements OnInit {

  toastrHelper: ToastrHelper;
  pageHelper: PageHelper;
  paramArray: ParamObject[] = [];

  constructor(private generalService: GeneralService, private paramService: ParamService, private toastrService: NbToastrService,
    private localStorageService: LocalStorageService, private dialogService: NbDialogService, private utilisateurService: UtilisateurService) {

    this.toastrHelper = new ToastrHelper(toastrService);
    this.pageHelper = new PageHelper(this.generalService, this.paramService, this.toastrService, this.localStorageService, this.dialogService, this.utilisateurService);

  }

  ngOnInit() {
    this.getUtilisateurConnected();
  }

  initPageTemplate() {
    this.pageHelper.titlePage = 'Mes Emprunts';
    this.pageHelper.paramArray = this.initParamArray();
    this.pageHelper.initFromTemplateName('mesEmprunts');
  }

  initParamArray(): ParamObject[] {
    this.paramArray.push(new ParamObject('paramId', this.pageHelper.utilisateur.idUtilisateur.toString(), ['mesEmpruntsModule']));
    return this.paramArray;
  }

  getUtilisateurConnected() {
      this.utilisateurService.getUtilisateurConnectedFromBack().subscribe(data => {
        this.pageHelper.utilisateur = new Utilisateur(data);
        this.initPageTemplate();
      });
    }
  }
