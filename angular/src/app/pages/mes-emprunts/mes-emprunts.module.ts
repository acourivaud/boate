import { NgModule } from '@angular/core';

import { ArchiveService } from '../../service/archive.service';
import { ColumnService } from '../../service/column.service';
import { FilterService } from '../../service/filter.service';
import { UtilisateurService } from '../../service/utilisateur.service';
import { PageDefinitionModule } from '../ui-features/page-definition/page-definition.module';
import { MesEmpruntsComponent } from './mes-emprunts.component';

@NgModule({
  imports: [
    PageDefinitionModule
  ],
  declarations: [
    MesEmpruntsComponent,
  ],
  providers: [
    UtilisateurService,
    ArchiveService,
    FilterService,
    ColumnService
  ]
})
export class MesEmpruntsModule {}
