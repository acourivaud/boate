import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { ColumnService } from './../../../service/column.service';
import { UtilisateurService } from './../../../service/utilisateur.service';
import { FilterService } from '../../../service/filter.service';
import { TableModule } from 'primeng/table';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { SurveillanceService } from '../../../service/surveillance.service';
import { NgModule } from '@angular/core';
import { KeyFilterModule } from 'primeng/keyfilter';
import { MultiSelectModule } from 'primeng/multiselect';
import { TooltipModule } from 'primeng/tooltip';

import { ThemeModule } from '../../../@theme/theme.module';
import { ProductionTopoComponent } from './production-topo.component';

@NgModule({
  imports: [
    ThemeModule,
    TableModule,
    AutoCompleteModule,
    KeyFilterModule,
    MultiSelectModule,
    TooltipModule,
    CheckboxModule,
    CalendarModule
  ],
  declarations: [
    ProductionTopoComponent,
  ],
  providers: [
    UtilisateurService,
    SurveillanceService,
    FilterService,
    ColumnService
  ]
})
export class ProductionTopoModule { }
