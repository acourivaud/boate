import { Location } from '@angular/common';
import { FIELDS } from './visite-ouvrage-fields';
import { PropositionBouclage } from './../../../model/propositionBouclage';
import { DeleteAction } from './../../../model/deleteAction';
import { BouclageBatchType } from './../../../model/bouclageBatchAction';
import { BouclageBatchAction } from './../../../model/bouclageBatchAction';
import { FR_LOCALE } from './../../../constant/CalendarFrLocale';
import { DateBouclage } from './../../../model/dateBouclage';
import { MENU_ITEMS } from './visite-ouvrage-menu';
import { BouclageService } from './../../../service/bouclage.service';
import { Bouclage } from './../../../model/bouclage';
import { ExportExcelHelper } from './../../../model/exportExcelHelper';
import { ColumnService } from './../../../service/column.service';
import { Utilisateur } from './../../../model/utilisateur';
import { UtilisateurService } from './../../../service/utilisateur.service';
import { ColumnHelper } from '../../../model/columnHelper';
import { Region } from '../../../model/region';
import { FilterService } from '../../../service/filter.service';
import { FilterHelper } from '../../../model/filterHelper';
import { ParamValue } from '../../../model/paramValue';
import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { Component, ViewChild, OnInit } from '@angular/core';
import { Table } from 'primeng/table';
import { NbToastrService, NbAccordionItemComponent } from '@nebular/theme';
import { ToastrHelper } from '../../../model/toastrHelper';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'visite-ouvrage',
  styleUrls: ['./visite-ouvrage.component.scss'],
  templateUrl: './visite-ouvrage.component.html',
})
export class VisiteOuvrageComponent implements OnInit {

  utilisateur: Utilisateur;

  inputFilter: RegExp = /^[0-9/./,]$/;

  selectOptions: any = FIELDS.OPTIONS;

  bouclage: Bouclage;

  @ViewChild('accordionPropositionTravauxU0') accordionPropositionTravauxU0: NbAccordionItemComponent;
  @ViewChild('accordionPropositionTravauxU1') accordionPropositionTravauxU1: NbAccordionItemComponent;
  @ViewChild('accordionPropositionTravauxU2') accordionPropositionTravauxU2: NbAccordionItemComponent;

  toastrHelper: ToastrHelper;

  fr: any = FR_LOCALE;

  expertiseToggleU0: ExpertiseToggleGroup;
  expertiseToggleU1: ExpertiseToggleGroup;
  expertiseToggleU2: ExpertiseToggleGroup;

  constructor(
    private utilisateurService: UtilisateurService,
    private bouclageService: BouclageService,
    private route: ActivatedRoute,
    private toastrService: NbToastrService,
    private location: Location
  ) {}

  ngOnInit() {
    this.utilisateur = this.utilisateurService.getUtilisateurConnected();
    this.toastrHelper = new ToastrHelper(this.toastrService);
    let idBouclage: string;
    this.route.queryParams.subscribe(params => {
      idBouclage = params.id;
    });
    this.getBouclage(idBouclage);
    this.getAuteursList();
  }

  getAuteursList(): void {
    this.utilisateurService.getAuteurs().subscribe(data => {
      const AuteurOptions = [ { raw: null, fr: '' } ];
      for (const auteurDto of data) {
        const auteur: Utilisateur = new Utilisateur(auteurDto);
        AuteurOptions.push({
          raw: auteur.idUtilisateur,
          fr: auteur.fullName
        });
      }
      this.selectOptions.AuteurVisiteOptions = AuteurOptions;
      this.selectOptions.ResponsableSurveillanceOptions = AuteurOptions;
    });
  }

  getInfrapole() {
    return this.bouclage.surveillance.ouvrage.up.secteur ? this.bouclage.surveillance.ouvrage.up.secteur.infrapole.name : this.bouclage.surveillance.ouvrage.up.infrapole.name;
  }

  getRegion() {
    return this.bouclage.surveillance.ouvrage.up.secteur ? this.bouclage.surveillance.ouvrage.up.secteur.infrapole.region.name : this.bouclage.surveillance.ouvrage.up.infrapole.region.name;
  }

  getBouclage(idBouclage: string): void {
    if (idBouclage) {
      this.bouclageService.getBouclage(idBouclage).subscribe(data => {
        this.bouclage = new Bouclage(data);
        this.expertiseToggleU0 = new ExpertiseToggleGroup(this.bouclage.u0_typeExpertise);
        this.expertiseToggleU1 = new ExpertiseToggleGroup(this.bouclage.u1_typeExpertise);
        this.expertiseToggleU2 = new ExpertiseToggleGroup(this.bouclage.u2_typeExpertise);
      });
    } else {
      this.bouclage = new Bouclage({});
    }
  }

  saveBouclage(): void {
    this.bouclage.u0_typeExpertise = this.expertiseToggleU0.getExpertiseAsString();
    this.bouclage.u1_typeExpertise = this.expertiseToggleU1.getExpertiseAsString();
    this.bouclage.u2_typeExpertise = this.expertiseToggleU2.getExpertiseAsString();
    this.bouclageService.saveBouclage(this.bouclage).subscribe(data => {
      this.bouclage = new Bouclage(data);
      this.toastrHelper.showDefaultSuccessToast();
    });
  }

  accordionPropositionTravauxU0Click() {
    this.accordionPropositionTravauxU0.toggle();
  }

  accordionPropositionTravauxU1Click() {
    this.accordionPropositionTravauxU1.toggle();
  }

  accordionPropositionTravauxU2Click() {
    this.accordionPropositionTravauxU2.toggle();
  }

  getPropTravauxButtonColor(propTavaux: string): string {
    if (this.bouclage) {
      if (this.bouclage['propositionTravaux' + propTavaux] === true) {
        if (this['accordionPropositionTravaux' + propTavaux].collapsedValue) {
          return 'green-white-button';
        } else {
          return 'green-black-button';
        }
      } else {
        if (this['accordionPropositionTravaux' + propTavaux].collapsedValue) {
          return 'blue-white-button';
        } else {
          return 'blue-black-button';
        }
      }
    }
    return 'blue-white-button';
  }

  isOuvrageEnTerre(): boolean {
    if (this.bouclage) {
      if (this.bouclage.surveillance === undefined) return false;
      if (this.bouclage.surveillance.ouvrage === undefined) return false;
      if (this.bouclage.surveillance.ouvrage.categorie === 'OT') return true;
    }
    return false;
  }

  copyAgentExpert(ux: string) {
    if (this.bouclage && this.bouclage['laborTodoSupervisor' + ux]) {
      this.bouclage['laborTodoSupervisor' + ux] = new PropositionBouclage({ proposition: this.bouclage['laborTodoAgent' + ux] });
    }
  }

  backClicked() {
    this.location.back();
  }
}

export class ExpertiseToggleGroup {
  Etancheite: boolean = false;
  Peinture: boolean = false;
  Hydro: boolean = false;
  Soar: boolean = false;
  Autres: boolean = false;

  constructor(typeExpertise: string) {
    this.parseFromBackend(typeExpertise);
  }

  private parseFromBackend(typeExpertise: string) {
    if (typeExpertise) {
      const split: string [] = typeExpertise.split(' - ');
      for (const expertise of split) {
        if (this.hasOwnProperty(expertise)) {
          this[expertise] =  true;
        }
      }
    }
  }

  getExpertiseAsString(): string {
    let expertiseAsString = '';
    if (this !== undefined) {
      for (const expertise in this) {
        if (this.hasOwnProperty(expertise)) {
          if (this[expertise.toString()] === true) {
            expertiseAsString += expertise.toString() + ' - ';
          }
        }
      }
    }
    expertiseAsString = expertiseAsString.substring(0, expertiseAsString.length - 3);
    return expertiseAsString;
  }
}
