import { MENU_ITEMS } from './../visite-ouvrage/visite-ouvrage-menu';
export const FIELDS =  {
  OPTIONS: {
    TypeVisiteOptions: MENU_ITEMS.WORK_VISIT_TYPE_ADMIN,
    AuteurVisiteOptions: null,
    ResponsableSurveillanceOptions: null,
    GraviteOptions: MENU_ITEMS.WORK_INCIDENT_IMPORTANCE,
    EtatCvOptions: MENU_ITEMS.ETAT_CV_VAL,
    DelaiAgentU0Options: MENU_ITEMS.DELAI_TRAVAUX_U0,
    DelaiExpertU0Options: MENU_ITEMS.DELAI_TRAVAUX_U0,
    ValidationBesoinTravauxU0Options: MENU_ITEMS.LABOR_VALIDATION,
    DelaiAgentU1Options: MENU_ITEMS.DELAI_TRAVAUX_U1,
    DelaiExpertU1Options: MENU_ITEMS.DELAI_TRAVAUX_U1,
    ValidationBesoinTravauxU1Options: MENU_ITEMS.LABOR_VALIDATION,
    DelaiAgentU2Options: MENU_ITEMS.DELAI_TRAVAUX_U2,
    DelaiExpertU2Options: MENU_ITEMS.DELAI_TRAVAUX_U2,
    ValidationBesoinTravauxU2Options: MENU_ITEMS.LABOR_VALIDATION,
    CotesOptions: MENU_ITEMS.WORK_VISIT_COTE,
    EtatOuvrageOptions: MENU_ITEMS.ETAT_OUVRAGE,
  }
};
