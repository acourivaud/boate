export const MENU_ITEMS =  {
  ETAT_CV_VAL: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'actif',
      fr: 'A faire',
    },
    {
      raw: 'inactif',
      fr: 'A supprimer',
    },
    {
      raw: 'inactif_val',
      fr: 'Supprimé',
    },
  ],
  WORK_VISIT_TYPE_ADMIN: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'visit_type_id',
      fr: 'Inspection detaillée OA',
    },
    {
      raw: 'visit_type_idi',
      fr: 'ID intermédiaire OA',
    },
    {
      raw: 'visit_type_cl_ot',
      fr: 'Visite de classement OT',
    },
    {
      raw: 'visit_type_vd',
      fr: 'Visite détaillée OA',
    },
    {
      raw: 'visit_type_vd_ot',
      fr: 'Visite détaillée OT',
    },
    {
      raw: 'visit_type_vi',
      fr: 'Visite intermédiaire OA',
    },
    {
      raw: 'visit_type_vi_ot',
      fr: 'Visite intermédiaire OT',
    },
    {
      raw: 'visit_type_vp_ot',
      fr: 'Visite particulière OT',
    },
    {
      raw: 'visit_type_va_ot',
      fr: 'Visite annuelle EFPB OT',
    },
    {
      raw: 'visit_type_vs',
      fr: 'Visite simplifiée OA',
    },
    {
      raw: 'visit_type_vsp',
      fr: 'Visite spéciale OA',
    },
    {
      raw: 'visit_type_fsa',
      fr: 'Visite FSA par entreprise',
    },
    {
      raw: 'visit_type_pv0',
      fr: 'Visite de référence (PV0)',
    },
    {
      raw: 'visit_type_ve_ot',
      fr: 'Visite d\'expertise OT',
    },
    {
      raw: 'visit_type_hydro',
      fr: 'Visite d\'expertise Hydraulique',
    },
    {
      raw: 'visit_type_id_cv',
      fr: 'Complément de visite d\'inspection detaillée',
    },
    {
      raw: 'visit_type_vd_cv',
      fr: 'Complément de visite de la visite détaillée',
    },
    {
      raw: 'visit_type_sp',
      fr: 'Surveillance particulière',
    },
    {
      raw: 'visit_type_sr',
      fr: 'Surveillance renforcée',
    },
    {
      raw: 'visit_type_topo',
      fr: 'Suivi TOPO OT',
    },
    {
      raw: 'visit_type_inclino',
      fr: 'Suivi INCLINO OT',
    },
  ],
  DELAI_TRAVAUX_U0: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'delai_0_3m',
      fr: '0 à 3 mois',
    },
    {
      raw: 'delai_3_6m',
      fr: '3 à 6 mois',
    },
    {
      raw: 'delai_6_9m',
      fr: '6 à 9 mois',
    },
    {
      raw: 'delai_9_12m',
      fr: '9 à 12 mois',
    },
  ],
  DELAI_TRAVAUX_U1: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'delai_1_2a',
      fr: '1 à 2 ans',
    },
    {
      raw: 'delai_2_3a',
      fr: '2 à 3 ans',
    },
    {
      raw: 'delai_3_4a',
      fr: '3 à 4 ans',
    },
    {
      raw: 'delai_4_5a',
      fr: '4 à 5 ans',
    },
    {
      raw: 'delai_5_6a',
      fr: '5 à 6 ans',
    },
  ],
  DELAI_TRAVAUX_U2: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'delai_sup6a',
      fr: '> 6 ans',
    },
  ],
  LABOR_VALIDATION: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'lab_vil_unvalidated',
      fr: 'En cours de validation',
    },
    {
      raw: 'lab_vil_positive',
      fr: 'Accepté',
    },
    {
      raw: 'lab_vil_negative',
      fr: 'Refusé',
    },
  ],
  ETAT_OUVRAGE: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'Bon',
      fr: 'Bon',
    },
    {
      raw: 'Moyen',
      fr: 'Moyen',
    },
    {
      raw: 'Mauvais',
      fr: 'Mauvais',
    },
  ],
  WORK_INCIDENT_IMPORTANCE: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'inc_imp_1a',
      fr: '1a',
    },
    {
      raw: 'inc_imp_1b',
      fr: '1b',
    },
    {
      raw: 'inc_imp_2',
      fr: '2',
    },
    {
      raw: 'inc_imp_3',
      fr: '3',
    },
    {
      raw: 'inc_imp_4a',
      fr: '4a',
    },
    {
      raw: 'inc_imp_4b',
      fr: '4b',
    },
  ],
  WORK_VISIT_COTE: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'D',
      fr: 'D',
    },
    {
      raw: 'G',
      fr: 'G',
    },
    {
      raw: 'D et G',
      fr: 'D et G',
    },
  ],
};
