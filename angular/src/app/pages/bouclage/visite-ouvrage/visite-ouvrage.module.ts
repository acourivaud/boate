import { CalendarModule } from 'primeng/calendar';
import { ColumnService } from './../../../service/column.service';
import { UtilisateurService } from './../../../service/utilisateur.service';
import { FilterService } from '../../../service/filter.service';
import { TableModule } from 'primeng/table';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BouclageService } from '../../../service/bouclage.service';
import { NgModule } from '@angular/core';
import { KeyFilterModule } from 'primeng/keyfilter';
import { MultiSelectModule } from 'primeng/multiselect';
import { TooltipModule } from 'primeng/tooltip';

import { ThemeModule } from '../../../@theme/theme.module';
import { VisiteOuvrageComponent } from './visite-ouvrage.component';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ToggleButtonModule } from 'primeng/togglebutton';

@NgModule({
  imports: [
    ThemeModule,
    TableModule,
    AutoCompleteModule,
    KeyFilterModule,
    MultiSelectModule,
    TooltipModule,
    CalendarModule,
    CheckboxModule,
    InputTextareaModule,
    ToggleButtonModule
  ],
  declarations: [
    VisiteOuvrageComponent,
  ],
  providers: [
    UtilisateurService,
    BouclageService,
    FilterService,
    ColumnService
  ]
})
export class VisiteOuvrageModule { }
