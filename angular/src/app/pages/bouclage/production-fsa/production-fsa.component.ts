import { Location } from '@angular/common';
import { FrontUrlConstant } from './../../../constant/FrontUrlConstant';
import { FR_LOCALE } from './../../../constant/CalendarFrLocale';
import { DeleteAction } from './../../../model/deleteAction';
import { BouclageBatchType, BouclageBatchAction } from './../../../model/bouclageBatchAction';
import { DateBouclage } from './../../../model/dateBouclage';
import { CONDITIONAL_COLS } from './production-fsa-cols';
import { FIELDS } from './production-fsa-fields';
import { BouclageService } from './../../../service/bouclage.service';
import { Bouclage } from './../../../model/bouclage';
import { ExportExcelHelper } from './../../../model/exportExcelHelper';
import { ColumnService } from './../../../service/column.service';
import { Utilisateur } from './../../../model/utilisateur';
import { UtilisateurService } from './../../../service/utilisateur.service';
import { ColumnHelper } from '../../../model/columnHelper';
import { Region } from '../../../model/region';
import { FilterService } from '../../../service/filter.service';
import { FilterHelper } from '../../../model/filterHelper';
import { MENU_ITEMS } from './production-fsa-menu';
import { ParamValue } from '../../../model/paramValue';
import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { Component, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import { SelectItem } from 'primeng/components/common/selectitem';
import { NbToastrService } from '@nebular/theme';
import { ToastrHelper } from '../../../model/toastrHelper';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { PropositionBouclage } from '../../../model/propositionBouclage';
import { Router } from '@angular/router';

@Component({
  selector: 'production-fsa',
  styleUrls: ['./production-fsa.component.scss'],
  templateUrl: './production-fsa.component.html',
})
export class ProductionFsaComponent {

  utilisateur: Utilisateur;

  bouclages: Bouclage[];
  selectedBouclages: Bouclage[];
  selectedDate: Date;
  dontKeepSelection: boolean = true;
  totalRecords: number;
  loading: boolean;
  exportLoading: boolean;
  inputFilter: RegExp = /^[0-9/./,]$/;

  @ViewChild('accordionRechercher') accordionRechercher;
  @ViewChild('accordionOA') accordionOA;
  @ViewChild('accordionOT') accordionOT;
  @ViewChild('accordionBNC') accordionBNC;
  @ViewChild('accordionAO') accordionAO;

  @ViewChild('bouclageTable') bouclageTable: Table;

  activeFilters: FilterHelper;
  columnHelper: ColumnHelper;

  pendingBackendResponses: number;

  toastrHelper: ToastrHelper;

  bouclage: string = 'production-fsa';

  fr: any = FR_LOCALE;

  constructor(private utilisateurService: UtilisateurService,
              private bouclageService: BouclageService,
              private filterService: FilterService,
              private columnService: ColumnService,
              private toastrService: NbToastrService,
              private router: Router,
              private location: Location) {

    this.utilisateur = utilisateurService.getUtilisateurConnected();

    const useFiltersForRequestsArg = this;
    const useFiltersForRequestsCallback = (arg: any): void => {
      const GenTypeVisiteSelected: string = arg.activeFilters.getSelectedRawString('GenTypeVisiteSelected');
      if (GenTypeVisiteSelected != null) {
        arg.columnHelper.conditionalCols = CONDITIONAL_COLS[GenTypeVisiteSelected];
        arg.columnHelper.page = 'bouclage/' + arg.bouclage + '/' + GenTypeVisiteSelected;
      } else {
        arg.columnHelper.conditionalCols = CONDITIONAL_COLS.default;
        arg.columnHelper.page = 'bouclage/' + arg.bouclage + '/default';
      }
      arg.columnHelper.adaptConditionalColumnsToRequest();
      arg.columnHelper.getColumnsOrder();
    };
    this.activeFilters = new FilterHelper(filterService, 'bouclage/' + this.bouclage, useFiltersForRequestsCallback, useFiltersForRequestsArg);

    this.toastrHelper = new ToastrHelper(this.toastrService);

    if (this.utilisateur.nbLignesTableau) {
      this.activeFilters.rows = Number.parseInt(this.utilisateur.nbLignesTableau);
    }

    const ANNEES: any[] = [];
    for (let year: number = new Date().getFullYear() + 1 ; year >= 2008 ; year--) {
      ANNEES.push(
        {
          raw: year.toString(),
          fr: year.toString(),
        }
      );
    }

    this.activeFilters.options = FIELDS.OPTIONS;
    this.activeFilters.filters = FIELDS.FILTERS;
    this.activeFilters.options.GenAnneePrevueOptions = ANNEES;

    this.loading = true;

    this.columnHelper = new ColumnHelper(CONDITIONAL_COLS.default, null, this.columnService, 'bouclage/' + this.bouclage + '/default');

    this.pendingBackendResponses = 6;

    this.filterService.getRegions().subscribe(data => {
      this.activeFilters.options.GenRegionOptions = [ { raw: null, fr: '' } ];
      for (let regionDto of data) {
        let region : Region = new Region(regionDto);
        this.activeFilters.options.GenRegionOptions.push({
          raw: region.id.toString(),
          fr: region.name
        });
      }
      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });

    this.filterService.getSegmentsGestion().subscribe(data => {
      this.activeFilters.options.ParSegGestionOptions = [ { raw: null, fr: '' } ];
      for (let segmentDto of data) {
        this.activeFilters.options.ParSegGestionOptions.push({
          raw: segmentDto.dto,
          fr: segmentDto.dto
        });
      }
      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });

    this.filterService.getComptesAnalytiques().subscribe(data => {
      this.activeFilters.options.ParCodeAnalyOptions = [ { raw: null, fr: '' } ];
      for (let codeDto of data) {
        this.activeFilters.options.ParCodeAnalyOptions.push({
          raw: codeDto.dto,
          fr: codeDto.dto
        });
      }
      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });

    this.activeFilters.options.GenAnneeSurveillanceOptions = [{ raw: null, fr: '' }];
    let maxYear : Number = new Date().getFullYear() + 10;
    for (let year = 2008 ; year <= maxYear ; year++) {
      this.activeFilters.options.GenAnneeSurveillanceOptions.push( { raw: year, fr: year } );
    }

    this.regionInfrapoleSecteurUoChange();

    this.getDepartementsAccordingToRegion();

    this.getReferentsTechniquesRegionaux();

    this.getUsersFilteredByResponsable();
  }

  getFiltersForConnectedUser() {
    // Ne fonctionne que quand tous les appels précédents ont répondu (UNE SEULE FOIS)
    if (!this.activeFilters.areConnectedUserFiltersLoaded && this.pendingBackendResponses == 0) {
      const self = this;
      const callback = (arg: any): void => {
        arg.getReferentsTechniquesRegionaux();
        arg.getReferentsAoap();
        arg.activeFilters.useFiltersForRequests(true);
        if (!arg.activeFilters.connectedUserFilters
            || arg.activeFilters.connectedUserFilters == null
            || arg.activeFilters.connectedUserFilters.length == 0) {
          if (arg.utilisateur.up) {
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenUo', arg.utilisateur.up ? arg.utilisateur.up.id : null);
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenSecteur', arg.utilisateur.secteur ? arg.utilisateur.secteur.id : null);
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenInfrapole', arg.utilisateur.infrapole ? arg.utilisateur.infrapole.id : null);
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenRegion', arg.utilisateur.region ? arg.utilisateur.region.id : null);
          }
        }
        const GenTypeVisiteSelected: string = arg.activeFilters.getSelectedRawString('GenTypeVisiteSelected');
        if (GenTypeVisiteSelected != null) {
          arg.columnHelper.conditionalCols = CONDITIONAL_COLS[GenTypeVisiteSelected];
          arg.columnHelper.page = 'bouclage/' + arg.bouclage + '/' + GenTypeVisiteSelected;
        } else {
          arg.columnHelper.conditionalCols = CONDITIONAL_COLS.default;
          arg.columnHelper.page = 'bouclage/' + arg.bouclage + '/default';
        }
        arg.columnHelper.getColumnsOrder();
      };
      this.activeFilters.populateDefaultFiltersForConnectedUser(callback, self);
    }
  }

  loadBouclagesLazy(event: any) {
    setTimeout(() => {
      this.fetchDataFromBackEnd(event, false);
    });
  }

  fetchDataFromBackEnd(event: any, saveFilters: boolean) {
    this.loading = true;

    const requestFilters: ParamValue[] = this.activeFilters.generateRequestFilters(event, this.bouclageTable);

    this.bouclageService.getBouclages(this.bouclage, saveFilters, requestFilters).subscribe(data => {
      this.totalRecords = data.count;
      this.bouclages = [];
      for (const bouclageDTO of data.result) {
        this.bouclages.push(new Bouclage(bouclageDTO));
      }
      this.loading = false;
    });
  }

  resolveFieldData(rowData, colField){
    return ObjectUtils.resolveFieldData(rowData, colField);
  }

  getSpecificColumnClass(rowData, col): string {
    if (this.resolveFieldData(rowData, col.field) instanceof PropositionBouclage) {
      const proposition: PropositionBouclage = this.resolveFieldData(rowData, col.field);
      return col.style + ' ' + proposition.getColor();
    }
    return col.style;
  }

  getSpecificColumnType(rowData, colField): string {
    if (this.resolveFieldData(rowData, colField) instanceof DateBouclage) return 'DateBouclage';
    if (this.resolveFieldData(rowData, colField) instanceof PropositionBouclage) return 'PropositionBouclage';
    return 'Default';
  }

  getTooltip(rowData, colField): string {
    if (this.getSpecificColumnType(rowData, colField) === 'DateBouclage') {
      const date: DateBouclage = this.resolveFieldData(rowData, colField);
      return date.getDescription();
    } else if (this.getSpecificColumnType(rowData, colField) === 'PropositionBouclage') {
      const proposition: PropositionBouclage = this.resolveFieldData(rowData, colField);
      return proposition.getDescription();
    } else {
      return this.resolveFieldData(rowData, colField);
    }
  }

  // Généralité

  GenRegionChange($event) {
    this.regionInfrapoleSecteurUoChange();
    this.getDepartementsAccordingToRegion();
    this.getReferentsTechniquesRegionaux();
    this.getReferentsAoap();
    this.checkCommune();
  }

  GenInfrapoleChange($event) {
    this.regionInfrapoleSecteurUoChange();
  }

  GenSecteurChange($event) {
    this.regionInfrapoleSecteurUoChange();
  }

  GenUoChange($event) {
    this.regionInfrapoleSecteurUoChange();
  }

  regionInfrapoleSecteurUoChange() {
    this.filterService.getRegionInfrapoleSecteurUoLigne(
        this.activeFilters.getSelectedRawString('GenRegionSelected'),
        this.activeFilters.getSelectedRawString('GenInfrapoleSelected'),
        this.activeFilters.getSelectedRawString('GenSecteurSelected'),
        this.activeFilters.getSelectedRawString('GenUoSelected')
      ).subscribe(data => {

      let GenInfrapoleSelected = this.activeFilters.getSelectedRawString('GenInfrapoleSelected');
      let GenSecteurSelected = this.activeFilters.getSelectedRawString('GenSecteurSelected');
      let GenUoSelected = this.activeFilters.getSelectedRawString('GenUoSelected');
      let GenLigneSelected = this.activeFilters.getSelectedRawString('GenLigneSelected');

      let infrapoles = [ { raw: null, fr: '' } ];
      let secteurs = [ { raw: null, fr: '' } ];
      let unitesOperationnelle = [ { raw: null, fr: '' } ];
      let lignes = [ { raw: null, fr: '' } ];

      for (let infrapoleDTO of data.infrapoles) {
        infrapoles.push({ raw: infrapoleDTO.id.toString(), fr: infrapoleDTO.name });
      }

      for (let secteurDTO of data.secteurs) {
        secteurs.push({ raw: secteurDTO.id.toString(), fr: secteurDTO.nom });
      }

      for (let uniteOperationnelleDTO of data.unitesOperationnelle) {
        unitesOperationnelle.push({ raw: uniteOperationnelleDTO.id.toString(), fr: uniteOperationnelleDTO.name });
      }

      for (let ligneDTO of data.lignes) {
        lignes.push({ raw: ligneDTO.id.toString(), fr: ligneDTO.nom });
      }

      this.activeFilters.options.GenInfrapoleOptions = infrapoles;
      this.activeFilters.options.GenSecteurOptions = secteurs;
      this.activeFilters.options.GenUoOptions = unitesOperationnelle;
      this.activeFilters.options.GenLigneOptions = lignes;

      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenInfrapole', GenInfrapoleSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenSecteur', GenSecteurSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenUo', GenUoSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenLigne', GenLigneSelected);

      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });
  }

  disableDateButtons(): boolean {
    if (this.activeFilters === undefined || this.activeFilters.filtersForRequests === undefined ) return true; // undefined protection line
    if (this.activeFilters.filtersForRequests.GenTypeVisiteSelected === undefined ||
          this.activeFilters.filtersForRequests.GenTypeVisiteSelected === null ||
            this.activeFilters.filtersForRequests.GenTypeVisiteSelected.raw === null) return true;
    if (this.selectedBouclages === undefined || this.selectedBouclages.length < 1) return true;
    if (this.selectedDate === undefined) return true;
    return false;
  }

  addBatchDate() {
    this.applyBatchAction(BouclageBatchType.AJOUT_DATE);
  }

  deleteBatchDate() {
    this.applyBatchAction(BouclageBatchType.SUPPRESSION_DATE);
  }

  addNumeroOperation() {
    this.applyBatchAction(BouclageBatchType.NUMERO_OPERATION);
  }

  applyBatchAction(bouclageBatchType: BouclageBatchType) {
    this.loading = true;
    const requestFilters: ParamValue[] = this.activeFilters.generateRequestFilters(
      {
        rows: this.activeFilters.rows,
        first: this.activeFilters.first
      },
      this.bouclageTable
    );
    const bouclageBatchAction =  new BouclageBatchAction(
      'bouclage/' + this.bouclage,
      requestFilters,
      this.selectedBouclages,
      this.selectedDate,
      bouclageBatchType
    );
    this.bouclageService.applyBatchAction(bouclageBatchAction).subscribe(data => {
      this.totalRecords = data.count;
      this.bouclages = [];
      for (const bouclageDTO of data.result) {
        this.bouclages.push(new Bouclage(bouclageDTO));
      }
      if (this.dontKeepSelection === true) this.selectedBouclages = [];
      this.loading = false;
    });
  }

  deleteBouclage(bouclage: Bouclage) {
    this.loading = true;
    const requestFilters: ParamValue[] = this.activeFilters.generateRequestFilters(
      {
        rows: this.activeFilters.rows,
        first: this.activeFilters.first
      },
      this.bouclageTable
    );
    const deleteAction = new DeleteAction(
      'bouclage/' + this.bouclage,
      requestFilters,
      bouclage.id
    );
    this.bouclageService.delete(deleteAction).subscribe(data => {
      this.totalRecords = data.count;
      this.bouclages = [];
      for (const bouclageDTO of data.result) {
        this.bouclages.push(new Bouclage(bouclageDTO));
      }
      this.selectedBouclages = [];
      this.loading = false;
    });
  }

  getDepartementsAccordingToRegion() {
    this.filterService.getDepartements(
        this.activeFilters.getSelectedRawString('GenRegionSelected')
      ).subscribe(data => {

      let SitDepartementSelected = this.activeFilters.getSelectedRawString('SitDepartementSelected');

      let departements = [ { raw: null, fr: '' } ];

      for (let departementDTO of data) {
        departements.push({ raw: departementDTO.id.toString(), fr: departementDTO.nom });
      }

      this.activeFilters.options.SitDepartementOptions = departements;

      this.activeFilters.trySetSelectedFromOptionsIfPossible('SitDepartement', SitDepartementSelected);

      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });
  }

  GenRespSurveillanceChange($event) {
    this.getUsersFilteredByResponsable();
  }

  getUsersFilteredByResponsable() {
    this.filterService.getUsersFilteredByResponsable(
        this.activeFilters.getSelectedRawString('GenRespSurveillanceSelected')
      ).subscribe(data => {

      const GenAuteurVisiteSelected = this.activeFilters.getSelectedRawString('GenAuteurVisiteSelected');

      const users = [ { raw: null, fr: '' } ];

      for (const userDTO of data) {
        users.push({ raw: userDTO.idUtilisateur.toString(), fr: userDTO.prenom + ' ' + userDTO.nom });
      }

      this.activeFilters.options.GenAuteurVisiteOptions = users;

      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenAuteurVisite', GenAuteurVisiteSelected);

      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });
  }

  GenTypeSurveillanceChange($event) {
    if (this.activeFilters.filters['GenTypeSurveillanceSelected'] != null
        && (this.activeFilters.filters['GenTypeSurveillanceSelected'].raw == 'visit_type_sp'
        || this.activeFilters.filters['GenTypeSurveillanceSelected'].raw == 'visit_type_sr')) {
      this.activeFilters.options['GenPeriodiciteOptions'] = MENU_ITEMS.SURVEILLANCE_PERIODICITE_ALL;
    } else {
      this.activeFilters.options['GenPeriodiciteOptions'] = MENU_ITEMS.SURVEILLANCE_PERIODICITE_ANNEES;
    }
    if (this.activeFilters.filters['GenTypeSurveillanceSelected'] == null
        || this.activeFilters.filters['GenTypeSurveillanceSelected'].raw == null
        || this.activeFilters.filters['GenTypeSurveillanceSelected'].raw == 'surv_tiers'
        || this.activeFilters.filters['GenTypeSurveillanceSelected'].raw == 'surv_current') {
      this.activeFilters.filters['GenPeriodiciteSelected'] = this.activeFilters.options['GenPeriodiciteOptions'][0];
      this.activeFilters.filters['GenRespSurveillanceSelected'] = this.activeFilters.options['GenRespSurveillanceOptions'][0];
    }
  }

  GenCategorieChange($event) {
    if (this.activeFilters.filters['GenCategorieSelected'] != null) {
      let GenTypeSelected = this.activeFilters.getSelectedRawString('GenTypeSelected');
      let self = this;
      switch (this.activeFilters.filters['GenCategorieSelected'].raw) {
        case null :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_NULL;
          setTimeout(function() {
            self.accordionAO.close();
            self.accordionBNC.close();
            self.accordionOA.close();
            self.accordionOT.close();
          }, 100);
          break;
        case 'category_Autre' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_OTHERS;
          this.activeFilters.razAllDescriptionFilters();
          setTimeout(function() {
            self.accordionAO.open();
            self.accordionBNC.close();
            self.accordionOA.close();
            self.accordionOT.close();
          }, 100);
          break;
        case 'category_Bat' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_BAT;
          this.activeFilters.razAllDescriptionFilters();
          setTimeout(function() {
            self.accordionAO.close();
            self.accordionBNC.open();
            self.accordionOA.close();
            self.accordionOT.close();
          }, 100);
          break;
        case 'category_OA' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_OA;
          this.activeFilters.razAllDescriptionFilters();
          setTimeout(function() {
            self.accordionAO.close();
            self.accordionBNC.close();
            self.accordionOA.open();
            self.accordionOT.close();
          }, 100);
          break;
        case 'category_OT' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_OT;
          this.activeFilters.razAllDescriptionFilters();
          setTimeout(function() {
            self.accordionAO.close();
            self.accordionBNC.close();
            self.accordionOA.close();
            self.accordionOT.open();
          }, 100);
          break;
        case 'category_Hydro' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_HYDRO;
          this.activeFilters.razAllDescriptionFilters();
          break;
      }
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenType', GenTypeSelected);
      this.GenTypeChange(null);
    }
  }

  GenTypeChange($event) {
    if (this.activeFilters.filters['GenTypeSelected'] != null) {
      if (this.activeFilters.filters['GenTypeSelected'].raw != null) {
        let menuItem = MENU_ITEMS;
        let options = menuItem['GEOMETRY_' + this.activeFilters.filters['GenTypeSelected'].raw];
        this.activeFilters.options.DesAoGeometrieOptions = options;
        this.activeFilters.options.DesOaGeometrieOptions = options;
      } else {
        this.activeFilters.options.DesAoGeometrieOptions = MENU_ITEMS.GEOMETRY_NULL;
        this.activeFilters.options.DesOaGeometrieOptions = MENU_ITEMS.GEOMETRY_NULL;
      }
    }
  }

  getReferentsTechniquesRegionaux() {
    this.filterService.getReferentsTechniquesRegionaux(this.activeFilters.filters.GenRegionSelected, 'SOAR').subscribe(data => {

      let referents = [ { raw: null, fr: '' } ];

      for (let referentDTO of data) {
        referents.push({ raw: referentDTO.idUtilisateur.toString(), fr: referentDTO.prenom + ' ' + referentDTO.nom });
      }

      this.activeFilters.options.GenReferentRegionalOptions = referents;
    });
  }

  getReferentsAoap() {
    this.filterService.getReferentsTechniquesRegionaux(this.activeFilters.filters.GenRegionSelected, 'AOAP').subscribe(data => {

      let referents = [ { raw: null, fr: '' } ];

      for (let referentDTO of data) {
        referents.push({ raw: referentDTO.idUtilisateur.toString(), fr: referentDTO.prenom + ' ' + referentDTO.nom });
      }

      this.activeFilters.options.GenReferentAoapOptions = referents;
    });
  }

  // Situation

  SitDepartementChange($event) {
    this.checkCommune();
  }

  SitCommuneSearch($event) {
    this.getCommunes($event.query);
  }

  getCommunes(searchText: string) {
    if (searchText.length > 2) {
      this.filterService.getCommunes(
          this.activeFilters.getSelectedRawString('GenRegionSelected'),
          this.activeFilters.getSelectedRawString('SitDepartementSelected'),
          searchText
        ).subscribe(data => {

        let SitCommuneSelected = this.activeFilters.getSelectedRawString('SitCommuneSelected');

        let communes = [];

        for (let communeDTO of data) {
          communes.push({ raw: communeDTO.id.toString(), fr: communeDTO.nom + ' (' + communeDTO.codePostal + ')' });
        }

        this.activeFilters.options.SitCommuneOptions = communes;

        this.activeFilters.trySetSelectedFromOptionsIfPossible('SitCommune', SitCommuneSelected);

      });
    } else {
      this.activeFilters.options.SitCommuneOptions = [];
    }
  }

  checkCommune() {
    if (this.activeFilters.filters['SitCommuneSelected'] != null && this.activeFilters.filters['SitCommuneSelected'].raw != null) {
      this.filterService.checkCommune(
          this.activeFilters.getSelectedRawString('GenRegionSelected'),
          this.activeFilters.getSelectedRawString('SitDepartementSelected'),
          this.activeFilters.filters['SitCommuneSelected'].raw
        ).subscribe(data => {
          if (data.dto == 'nok') {
            this.activeFilters.filters['SitCommuneSelected'] =  null;
          }
      });
    }
  }

  // Particularités
  ParNomSearch($event) {
    this.getOuvragesName($event.query);
  }

  getOuvragesName(searchText: string) {
    if (searchText.length > 2) {
      this.filterService.getOuvragesName(searchText).subscribe(data => {
        let ouvragesName = [];
        for (let ouvrageDTO of data) {
          ouvragesName.push({ raw: ouvrageDTO.name });
        }
        this.activeFilters.options.ParNomOptions = ouvragesName;
      });
    } else {
      this.activeFilters.options.ParNomOptions = [];
    }
  }

  onChercherClick() {
    this.activeFilters.useFiltersForRequests(true);
    this.fetchDataFromBackEnd(null, true);
    this.accordionRechercher.close();
  }

  onRazClick() {
    this.activeFilters.razAllFilters();
    this.activeFilters.useFiltersForRequests(true);
    this.fetchDataFromBackEnd(null, true);
    this.accordionRechercher.close();
  }

  onExporterClick() {
    this.exportLoading = true;
    this.toastrHelper.showToast(
      NbToastStatus.SUCCESS,
      'EXPORT EN COURS...',
      'Votre fichier d\'export est en cours de génération. Il sera téléchargé automatiquement une fois prêt. En attendant, vous pouvez continuer à utiliser l\'application SANS RAFRAICHIR LA PAGE.',
      20000
    );
    const event = { first: 0, rows: this.totalRecords };
    const requestFilters: ParamValue[] = this.activeFilters.generateRequestFilters(event, this.bouclageTable);
    this.bouclageService.getBouclages(this.bouclage, false, requestFilters, 3600).subscribe(data => {
      ExportExcelHelper.export(this.columnHelper.convertObjectsArrayToRawObjectsJsonData(data.result), 'ExportBouclages', 'BOUCLAGES');
      this.exportLoading = false;
    });
  }

  onInputKeyPress($event) {
    if ($event.keyCode == 13) {
      this.onChercherClick();
    }
  }

  openVisiteOuvragePage(bouclage: Bouclage) {
    this.router.navigate([FrontUrlConstant.PATH_TO_VISTE_OUVRAGE], { queryParams: { id: bouclage.id } });
  }

  backClicked() {
    this.location.back();
  }
}
