export const MENU_ITEMS =  {
  BOOLEAN: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'true',
      fr: 'Oui',
    },
    {
      raw: 'false',
      fr: 'Non',
    },
  ],
  SITUATION: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'situation_up_left',
      fr: '>G',
    },
    {
      raw: 'situation_left',
      fr: 'G',
    },
    {
      raw: 'situation_down_left',
      fr: '<G',
    },
    {
      raw: 'situation_up_right',
      fr: '>D',
    },
    {
      raw: 'situation_right',
      fr: 'D',
    },
    {
      raw: 'situation_down_right',
      fr: '<D',
    },
    {
      raw: 'situation_up',
      fr: '>',
    },
    {
      raw: 'situation_down',
      fr: '<',
    },
  ],
  NIVEAU_ETAT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'Non défini',
      fr: 'Non défini',
    },
    {
      raw: 'N1',
      fr: 'N1',
    },
    {
      raw: 'N2',
      fr: 'N2',
    },
    {
      raw: 'N3',
      fr: 'N3',
    },
    {
      raw: 'N4',
      fr: 'N4',
    },
    {
      raw: 'N5',
      fr: 'N5',
    },
  ],
  NIVEAU_GRAVITE: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'Non défini',
      fr: 'Non défini',
    },
    {
      raw: 'G1',
      fr: 'G1',
    },
    {
      raw: 'G2',
      fr: 'G2',
    },
    {
      raw: 'G3',
      fr: 'G3',
    },
    {
      raw: 'G4',
      fr: 'G4',
    },
    {
      raw: 'G5',
      fr: 'G5',
    },
  ],
  FSA: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'fsa_oui',
      fr: 'Oui',
    },
    {
      raw: 'fsa_special_exposed',
      fr: 'Particulièrement Exposé',
    },
    {
      raw: 'fsa_none',
      fr: 'Non',
    },
  ],
  RATTACHEMENT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'rattachement_drainage',
      fr: 'Drainage',
    },
    {
      raw: 'rattachement_mur',
      fr: 'Mur, perré, digue...',
    },
    {
      raw: 'rattachement_ot',
      fr: 'OT',
    },
    {
      raw: 'rattachement_souterrain',
      fr: 'Ouvrage Souterrain',
    },
    {
      raw: 'rattachement_petit',
      fr: 'Petit Ouvrage',
    },
    {
      raw: 'rattachement_inferieur',
      fr: 'Pont-Rail',
    },
    {
      raw: 'rattachement_superieur',
      fr: 'Pont Supérieur',
    },
    {
      raw: 'rattachement_titre3',
      fr: 'Titre 3',
    },
    {
      raw: 'rattachement_sou_voie',
      fr: 'Traversée / Tiers',
    },
  ],
  STATUT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'A',
      fr: 'Actif',
    },
    {
      raw: 'I',
      fr: 'Inactif',
    },
  ],
  UIC: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'uic_3',
      fr: '3',
    },
    {
      raw: 'uic_4',
      fr: '4',
    },
    {
      raw: 'uic_5',
      fr: '5',
    },
    {
      raw: 'uic_6',
      fr: '6',
    },
    {
      raw: 'uic_6AV',
      fr: '6AV',
    },
    {
      raw: 'uic_7AV',
      fr: '7AV',
    },
    {
      raw: 'uic_7SV',
      fr: '7SV',
    },
    {
      raw: 'uic_8AV',
      fr: '8AV',
    },
    {
      raw: 'uic_8SV',
      fr: '8SV',
    },
    {
      raw: 'uic_9AV',
      fr: '9AV',
    },
    {
      raw: 'uic_9SV',
      fr: '9SV',
    },
    {
      raw: 'uic_emb',
      fr: 'Emb',
    },
    {
      raw: 'uic_F',
      fr: 'F',
    },
    {
      raw: 'uic_neut',
      fr: 'Neut',
    },
    {
      raw: 'uic_none',
      fr: 'Sans',
    },
  ],
  MEANS: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'means_4axes',
      fr: '4 axes',
    },
    {
      raw: 'means_acrobate',
      fr: 'Acrobate',
    },
    {
      raw: 'means_Brg',
      fr: 'Barge',
    },
    {
      raw: 'means_Brq',
      fr: 'Barque',
    },
    {
      raw: 'means_divers',
      fr: 'Divers',
    },
    {
      raw: 'means_drone',
      fr: 'Drone',
    },
    {
      raw: 'means_Ech',
      fr: 'Echelle',
    },
    {
      raw: 'means_Elise',
      fr: 'Elise',
    },
    {
      raw: 'means_endoscope',
      fr: 'Endoscope',
    },
    {
      raw: 'means_epurateur',
      fr: 'Epurateur',
    },
    {
      raw: 'means_feet',
      fr: 'A pied',
    },
    {
      raw: 'means_Mg1',
      fr: 'Moog 1',
    },
    {
      raw: 'means_Mg2',
      fr: 'Moog 2',
    },
    {
      raw: 'means_PF245',
      fr: 'PF 2, 4, 5',
    },
    {
      raw: 'means_PF2',
      fr: 'PF 2',
    },
    {
      raw: 'means_PF4',
      fr: 'PF 4',
    },
    {
      raw: 'means_PF5',
      fr: 'PF 5',
    },
    {
      raw: 'means_PF3',
      fr: 'PF 3',
    },
    {
      raw: 'means_PF6',
      fr: 'PF 6',
    },
    {
      raw: 'means_PF7',
      fr: 'PF 7',
    },
    {
      raw: 'means_PFR',
      fr: 'PFR',
    },
    {
      raw: 'means_Robot',
      fr: 'Robot vidéo',
    },
    {
      raw: 'means_S65/3',
      fr: 'S65/3',
    },
    {
      raw: 'means_S65/4',
      fr: 'S65/4',
    },
    {
      raw: 'means_nacelle',
      fr: 'Nacelle routière',
    },
    {
      raw: 'means_U26',
      fr: 'U26',
    },
    {
      raw: 'means_U32',
      fr: 'U32',
    },
    {
      raw: 'means_plongeur',
      fr: 'Plongeurs',
    },
    {
      raw: 'means_signal',
      fr: 'Signalisation',
    },
    {
      raw: 'means_vision',
      fr: 'Vision',
    },
    {
      raw: 'means_WIT',
      fr: 'WIT',
    },
    {
      raw: 'means_WP',
      fr: 'Wagon pousseur',
    },
    {
      raw: 'means_EVLOG',
      fr: 'EVLOG',
    },
    {
      raw: 'means_topo',
      fr: 'Nivellement',
    },
  ],
  WORK_CATEGORY: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'category_Autre',
      fr: 'Autres',
    },
    {
      raw: 'category_Bat',
      fr: 'Bâtiment Non Courant',
    },
    {
      raw: 'category_OA',
      fr: 'Ouvrage d\'Art',
    },
    {
      raw: 'category_OT',
      fr: 'Ouvrage en Terre',
    },
    {
      raw: 'category_Hydro',
      fr: 'Ouvrage Hydraulique',
    },
  ],
  WORK_LEVEL: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'parentAndChildWork',
      fr: 'Ouvrages pères et fils',
    },
    {
      raw: 'childWork',
      fr: 'Ouvrages fils',
    },
  ],
  VISITE_TYPE: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'visit_type_id',
      fr: 'Inspection detaillée OA',
    },
    {
      raw: 'visit_type_idi',
      fr: 'ID intermédiaire OA',
    },
    {
      raw: 'visit_type_cl_ot',
      fr: 'Visite de classement OT',
    },
    {
      raw: 'visit_type_vd',
      fr: 'Visite détaillée OA',
    },
    {
      raw: 'visit_type_vd_ot',
      fr: 'Visite détaillée OT',
    },
    {
      raw: 'visit_type_vi',
      fr: 'Visite intermédiaire OA',
    },
    {
      raw: 'visit_type_vi_ot',
      fr: 'Visite intermédiaire OT',
    },
    {
      raw: 'visit_type_vp_ot',
      fr: 'Visite particulière OT',
    },
    {
      raw: 'visit_type_va_ot',
      fr: 'Visite annuelle EFPB OT',
    },
    {
      raw: 'visit_type_vs',
      fr: 'Visite simplifiée OA',
    },
    {
      raw: 'visit_type_pv0',
      fr: 'Visite de référence (PV0)',
    },
    {
      raw: 'visit_type_id_cv',
      fr: 'Complément de visite d\'inspection detaillée',
    },
    {
      raw: 'visit_type_vd_cv',
      fr: 'Complément de visite de la visite détaillée',
    },
  ],
  VISITE_TYPE_category_OT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'visit_type_cl_ot',
      fr: 'Visite de classement OT',
    },
    {
      raw: 'visit_type_vd_ot',
      fr: 'Visite détaillée OT',
    },
    {
      raw: 'visit_type_vi_ot',
      fr: 'Visite intermédiaire OT',
    },
    {
      raw: 'visit_type_vp_ot',
      fr: 'Visite particulière OT',
    },
    {
      raw: 'visit_type_va_ot',
      fr: 'Visite annuelle EFPB OT',
    },
  ],
  VISITE_TYPE_else: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'visit_type_id',
      fr: 'Inspection detaillée OA',
    },
    {
      raw: 'visit_type_idi',
      fr: 'ID intermédiaire OA',
    },
    {
      raw: 'visit_type_vd',
      fr: 'Visite détaillée OA',
    },
    {
      raw: 'visit_type_vi',
      fr: 'Visite intermédiaire OA',
    },
    {
      raw: 'visit_type_vs',
      fr: 'Visite simplifiée OA',
    },
    {
      raw: 'visit_type_pv0',
      fr: 'Visite de référence (PV0)',
    },
    {
      raw: 'visit_type_id_cv',
      fr: 'Complément de visite d\'inspection detaillée',
    },
    {
      raw: 'visit_type_vd_cv',
      fr: 'Complément de visite de la visite détaillée',
    },
  ],
  WORK_TYPE_CATEGORY_NULL: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_aqc',
      fr: 'Aqueduc',
    },
    {
      raw: 'work_type_aqc_buse',
      fr: 'Aqueduc busé',
    },
    {
      raw: 'work_type_autre',
      fr: 'Autres',
    },
    {
      raw: 'work_type_bg',
      fr: 'Barrière grillagée',
    },
    {
      raw: 'work_type_bnc',
      fr: 'Bâtiment Non Courant',
    },
    {
      raw: 'work_type_bse',
      fr: 'Buse',
    },
    {
      raw: 'work_type_dlt',
      fr: 'Dalot',
    },
    {
      raw: 'work_type_deblai',
      fr: 'Déblai meuble',
    },
    {
      raw: 'work_type_depot',
      fr: 'Dépôt',
    },
    {
      raw: 'work_type_digoa',
      fr: 'Digue OA',
    },
    {
      raw: 'work_type_digot',
      fr: 'Digue OT',
    },
    {
      raw: 'work_type_dcp',
      fr: 'Dispositif Confortatif Parois',
    },
    {
      raw: 'work_type_divers',
      fr: 'Divers',
    },
    {
      raw: 'work_type_eab',
      fr: 'Ecran anti bruit',
    },
    {
      raw: 'work_type_efpb',
      fr: 'Ecran de filet pare-blocs',
    },
    {
      raw: 'work_type_gal',
      fr: 'Galerie',
    },
    {
      raw: 'work_type_gp',
      fr: 'Galerie de protection',
    },
    {
      raw: 'work_type_gt',
      fr: 'Galerie technique',
    },
    {
      raw: 'work_type_hydro',
      fr: 'Hydraulique',
    },
    {
      raw: 'work_type_mgh',
      fr: 'Mat de grande hauteur',
    },
    {
      raw: 'work_type_ms',
      fr: 'Mur de soutènement',
    },
    {
      raw: 'work_type_mgr',
      fr: 'Mur en gradin',
    },
    {
      raw: 'work_type_pc',
      fr: 'Paroi clouée',
    },
    {
      raw: 'work_type_prev',
      fr: 'Paroi revêtue',
    },
    {
      raw: 'work_type_pr',
      fr: 'Paroi rocheuse',
    },
    {
      raw: 'work_type_pas',
      fr: 'Passerelle',
    },
    {
      raw: 'work_type_per',
      fr: 'Perré',
    },
    {
      raw: 'work_type_pcl',
      fr: 'Pont canal',
    },
    {
      raw: 'work_type_pra',
      fr: 'Pont rail',
    },
    {
      raw: 'work_type_pro',
      fr: 'Pont route',
    },
    {
      raw: 'work_type_portique',
      fr: 'Portique',
    },
    {
      raw: 'work_type_potence',
      fr: 'Potence',
    },
    {
      raw: 'work_type_mxt',
      fr: 'Profil mixte',
    },
    {
      raw: 'work_type_profil_rasant',
      fr: 'Profil rasant',
    },
    {
      raw: 'work_type_pyl',
      fr: 'Pylône',
    },
    {
      raw: 'work_type_remblai',
      fr: 'Remblai',
    },
    {
      raw: 'work_type_sdm',
      fr: 'Saut de mouton',
    },
    {
      raw: 'work_type_tete_tunnel',
      fr: 'Tête tunnel',
    },
    {
      raw: 'work_type_tranchee',
      fr: 'Talus rocheux',
    },
    {
      raw: 'work_type_tc',
      fr: 'Tranchée couverte',
    },
    {
      raw: 'work_type_td',
      fr: 'Tranchée drainante',
    },
    {
      raw: 'work_type_trm',
      fr: 'Trémie',
    },
    {
      raw: 'work_type_tun',
      fr: 'Tunnel',
    },
    {
      raw: 'work_type_versant',
      fr: 'Versant',
    },
    {
      raw: 'work_type_via',
      fr: 'Viaduc',
    },
  ],
  WORK_TYPE_CATEGORY_OTHERS: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_divers',
      fr: 'Divers',
    },
    {
      raw: 'work_type_eab',
      fr: 'Ecran anti bruit',
    },
    {
      raw: 'work_type_mgh',
      fr: 'Mat de grande hauteur',
    },
    {
      raw: 'work_type_portique',
      fr: 'Portique',
    },
    {
      raw: 'work_type_potence',
      fr: 'Potence',
    },
    {
      raw: 'work_type_pyl',
      fr: 'Pylône',
    },
  ],
  WORK_TYPE_CATEGORY_BAT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_bnc',
      fr: 'Bâtiment Non Courant',
    },
  ],
  WORK_TYPE_CATEGORY_OA: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_aqc',
      fr: 'Aqueduc',
    },
    {
      raw: 'work_type_aqc_buse',
      fr: 'Aqueduc busé',
    },
    {
      raw: 'work_type_bse',
      fr: 'Buse',
    },
    {
      raw: 'work_type_dcp',
      fr: 'Dispositif Confortatif Parois',
    },
    {
      raw: 'work_type_digoa',
      fr: 'Digue OA',
    },
    {
      raw: 'work_type_dlt',
      fr: 'Dalot',
    },
    {
      raw: 'work_type_gal',
      fr: 'Galerie',
    },
    {
      raw: 'work_type_gp',
      fr: 'Galerie de protection',
    },
    {
      raw: 'work_type_gt',
      fr: 'Galerie technique',
    },
    {
      raw: 'work_type_ms',
      fr: 'Mur de soutènement',
    },
    {
      raw: 'work_type_mgr',
      fr: 'Mur en gradin',
    },
    {
      raw: 'work_type_pas',
      fr: 'Passerelle',
    },
    {
      raw: 'work_type_pc',
      fr: 'Paroi clouée',
    },
    {
      raw: 'work_type_prev',
      fr: 'Paroi revêtue',
    },
    {
      raw: 'work_type_pcl',
      fr: 'Pont canal',
    },
    {
      raw: 'work_type_per',
      fr: 'Perré',
    },
    {
      raw: 'work_type_pra',
      fr: 'Pont rail',
    },
    {
      raw: 'work_type_pro',
      fr: 'Pont route',
    },
    {
      raw: 'work_type_sdm',
      fr: 'Saut de mouton',
    },
    {
      raw: 'work_type_tc',
      fr: 'Tranchée couverte',
    },
    {
      raw: 'work_type_td',
      fr: 'Tranchée drainante',
    },
    {
      raw: 'work_type_trm',
      fr: 'Trémie',
    },
    {
      raw: 'work_type_tun',
      fr: 'Tunnel',
    },
    {
      raw: 'work_type_via',
      fr: 'Viaduc',
    },
  ],
  WORK_TYPE_CATEGORY_OT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_bg',
      fr: 'Barrière grillagée',
    },
    {
      raw: 'work_type_deblai',
      fr: 'Déblai meuble',
    },
    {
      raw: 'work_type_efpb',
      fr: 'Ecran de filet pare-blocs',
    },
    {
      raw: 'work_type_mxt',
      fr: 'Profil mixte',
    },
    {
      raw: 'work_type_prev',
      fr: 'Paroi revêtue',
    },
    {
      raw: 'work_type_profil_rasant',
      fr: 'Profil rasant',
    },
    {
      raw: 'work_type_remblai',
      fr: 'Remblai',
    },
    {
      raw: 'work_type_tranchee',
      fr: 'Talus rocheux',
    },
    {
      raw: 'work_type_versant',
      fr: 'Versant',
    },
  ],
  WORK_TYPE_CATEGORY_HYDRO: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_hydro',
      fr: 'Hydraulique',
    },
  ],
  WORK_VISIT_TYPE: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'visit_type_id',
      fr: 'Inspection detaillée OA',
    },
    {
      raw: 'visit_type_vd',
      fr: 'Visite détaillée OA',
    },
    {
      raw: 'visit_type_vd_ot',
      fr: 'Visite détaillée OT',
    },
    {
      raw: 'visit_type_vs',
      fr: 'Visite simplifiée OA',
    },
    {
      raw: 'visit_type_vi',
      fr: 'Visite intermédiaire OA',
    },
    {
      raw: 'visit_type_vi_ot',
      fr: 'Visite intermédiaire OT',
    },
    {
      raw: 'visit_type_idi',
      fr: 'ID intermédiaire OA',
    },
    {
      raw: 'visit_type_vp_ot',
      fr: 'Visite particulière OT',
    },
    {
      raw: 'visit_type_va_ot',
      fr: 'Visite annuelle EFPB OT',
    },
    {
      raw: 'visit_type_sp',
      fr: 'Surveillance particulière',
    },
    {
      raw: 'visit_type_sr',
      fr: 'Surveillance renforcée',
    },
    {
      raw: 'surv_tiers',
      fr: 'Tiers',
    },
    {
      raw: 'surv_current',
      fr: 'Surveillance courante',
    },
  ],
  SURVEILLANCE_PERIODICITE_ALL: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'period_1d',
      fr: '1 jour',
    },
    {
      raw: 'period_2d',
      fr: '2 jours',
    },
    {
      raw: 'period_3d',
      fr: '3 jours',
    },
    {
      raw: 'period_4d',
      fr: '4 jours',
    },
    {
      raw: 'period_5d',
      fr: '5 jours',
    },
    {
      raw: 'period_1w',
      fr: '1 semaine',
    },
    {
      raw: 'period_2w',
      fr: '2 semaines',
    },
    {
      raw: 'period_3w',
      fr: '3 semaines',
    },
    {
      raw: 'period_1m',
      fr: '1 mois',
    },
    {
      raw: 'period_2m',
      fr: '2 mois',
    },
    {
      raw: 'period_3m',
      fr: '3 mois',
    },
    {
      raw: 'period_4m',
      fr: '4 mois',
    },
    {
      raw: 'period_6m',
      fr: '6 mois',
    },
    {
      raw: 'period_1y',
      fr: '1 an',
    },
    {
      raw: 'period_2y',
      fr: '2 ans',
    },
    {
      raw: 'period_3y',
      fr: '3 ans',
    },
    {
      raw: 'period_6y',
      fr: '6 ans',
    },
    {
      raw: 'period_9y',
      fr: '9 ans',
    },
  ],
  SURVEILLANCE_PERIODICITE_ANNEES: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'period_1y',
      fr: '1 an',
    },
    {
      raw: 'period_2y',
      fr: '2 ans',
    },
    {
      raw: 'period_3y',
      fr: '3 ans',
    },
    {
      raw: 'period_6y',
      fr: '6 ans',
    },
    {
      raw: 'period_9y',
      fr: '9 ans',
    },
  ],
  SURVEILLANCE_RESPONSABLE: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'FUNCTION_ABE',
      fr: 'ABE',
    },
    {
      raw: 'FUNCTION_AH',
      fr: 'AH',
    },
    {
      raw: 'FUNCTION_AOAP',
      fr: 'AOAP',
    },
    {
      raw: 'FUNCTION_AOAU',
      fr: 'AOAU',
    },
    {
      raw: 'FUNCTION_COT',
      fr: 'COT',
    },
    {
      raw: 'FUNCTION_SOAR',
      fr: 'SOAR',
    },
    {
      raw: 'FUNCTION_TIERS',
      fr: 'Tiers',
    },
    {
      raw: 'FUNCTION_TOPO',
      fr: 'Topographe',
    },
  ],
  ASSEMBLY: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'assembly_Boul',
      fr: 'Boulonée',
    },
    {
      raw: 'assembly_Riv',
      fr: 'Riveté',
    },
    {
      raw: 'assembly_Sou',
      fr: 'Soudé',
    },
  ],
  NATURE: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'meuble',
      fr: 'Meuble',
    },
    {
      raw: 'rocheux',
      fr: 'Rocheux',
    },
    {
      raw: 'meuble_rocheux',
      fr: 'Meuble et rocheux',
    },
  ],
  OCCUPATION: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'occupation_sncf_infra',
      fr: 'SNCF infra',
    },
    {
      raw: 'occupation_sncf_other',
      fr: 'SNCF autres',
    },
    {
      raw: 'occupation_tiers',
      fr: 'Tiers',
    },
    {
      raw: 'occupation_transports_Robineau',
      fr: 'Transports robineau',
    },
    {
      raw: 'occupation_stockage_v',
      fr: 'Stockage V',
    },
    {
      raw: 'occupation_materiel_v',
      fr: 'Materiel V',
    },
    {
      raw: 'occupation_emt_vaise',
      fr: 'EMT Vaise',
    },
    {
      raw: 'occupation_stockage_materiaux_demolition',
      fr: 'Stockage matériaux démolition',
    },
    {
      raw: 'occupation_parking',
      fr: 'Parking',
    },
    {
      raw: 'occupation_stockage_service_electrique',
      fr: 'Stockage service électrique',
    },
    {
      raw: 'occupation_emr',
      fr: 'EMR',
    },
    {
      raw: 'occupation_inoccupe',
      fr: 'Inoccupé',
    },
    {
      raw: 'occupation_evlog_+_upses',
      fr: 'EVLOG + UPSES',
    },
    {
      raw: 'occupation_crem',
      fr: 'CREM',
    },
    {
      raw: 'occupation_depot',
      fr: 'Dépot',
    },
  ],
  GEOMETRY_NULL: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_circ',
      fr: 'Circ',
    },
    {
      raw: 'geometry_cyl',
      fr: 'Cylindrique',
    },
    {
      raw: 'geometry_dba',
      fr: 'Dalle BA',
    },
    {
      raw: 'geometry_ov',
      fr: 'Ovoïde',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
    {
      raw: 'geometry_qua',
      fr: 'Quadripode',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_tripode',
      fr: 'Tripode',
    },
  ],
  GEOMETRY_work_type_aqc: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_dba',
      fr: 'Dalle BA',
    },
  ],
  GEOMETRY_work_type_aqc_buse: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_dba',
      fr: 'Dalle BA',
    },
  ],
  GEOMETRY_work_type_autre: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_bg: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_bnc: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_bse: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_circ',
      fr: 'Circ',
    },
    {
      raw: 'geometry_ov',
      fr: 'Ovoïde',
    },
  ],
  GEOMETRY_work_type_dcp: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_deblai: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_depot: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_digoa: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_digot: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_divers: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_tripode',
      fr: 'Tripode',
    },
  ],
  GEOMETRY_work_type_dlt: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_eab: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_tripode',
      fr: 'Tripode',
    },
  ],
  GEOMETRY_work_type_efpb: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_gal: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
  ],
  GEOMETRY_work_type_gp: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
  ],
  GEOMETRY_work_type_gt: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
  ],
  GEOMETRY_work_type_hydro: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_mgh: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cyl',
      fr: 'Cylindrique',
    },
    {
      raw: 'geometry_qua',
      fr: 'Quadripode',
    },
    {
      raw: 'geometry_tripode',
      fr: 'Tripode',
    },
  ],
  GEOMETRY_work_type_mgr: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_ms: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_mxt: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_pas: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
  ],
  GEOMETRY_work_type_pc: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_pcl: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
  ],
  GEOMETRY_work_type_per: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_portique: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_tripode',
      fr: 'Tripode',
    },
  ],
  GEOMETRY_work_type_potence: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_pr: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_pra: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_dba',
      fr: 'Dalle BA',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
  ],
  GEOMETRY_work_type_prev: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_pro: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_dba',
      fr: 'Dalle BA',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
  ],
  GEOMETRY_work_type_profil_rasant: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_pyl: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cyl',
      fr: 'Cylindrique',
    },
    {
      raw: 'geometry_qua',
      fr: 'Quadripode',
    },
    {
      raw: 'geometry_tripode',
      fr: 'Tripode',
    },
  ],
  GEOMETRY_work_type_remblai: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_sdm: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
    {
      raw: 'geometry_dba',
      fr: 'Dalle BA',
    },
  ],
  GEOMETRY_work_type_tc: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
  ],
  GEOMETRY_work_type_td: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
  ],
  GEOMETRY_work_type_tete_tunnel: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_tranchee: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_trm: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_tun: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
  ],
  GEOMETRY_work_type_versant: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_via: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
  ],
  LABOR_VALIDATION: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'lab_vil_unvalidated',
      fr: 'En cours de validation',
    },
    {
      raw: 'lab_vil_positive',
      fr: 'Accepté',
    },
    {
      raw: 'lab_vil_negative',
      fr: 'Refusé',
    },
  ],
  CV_MOTIF_SOLUTION: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'absence_engin',
      fr: 'Absence d\'engin',
    },
    {
      raw: 'presence_vegetation',
      fr: 'Présence de végétation',
    },
    {
      raw: 'curage_a_faire',
      fr: 'Curage à faire',
    },
    {
      raw: 'impossibilite_acces',
      fr: 'Impossibilité d\'accès',
    },
    {
      raw: 'necessite_robot',
      fr: 'Nécessité de robot',
    },
    {
      raw: 'necessite_fsa',
      fr: 'Nécessité de visite FSA',
    },
  ],
  SOLUTION_CV_default: [
    {
      raw: null,
      fr: '',
    },
  ],
  SOLUTION_CV_absence_engin: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'reprogrammation_engin',
      fr: 'Reprogrammation d\'engin',
    },
    {
      raw: 'visite_cordiste',
      fr: 'Visite par cordistes',
    },
  ],
  SOLUTION_CV_presence_vegetation: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'debroussaillage',
      fr: 'Débroussaillage',
    },
  ],
  SOLUTION_CV_curage_a_faire: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'ouvrage_a_curer',
      fr: 'Ouvrage à curer',
    },
  ],
  SOLUTION_CV_impossibilite_acces: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'demande_acces',
      fr: 'Demande d\'autorisation d\'accès',
    },
    {
      raw: 'entreprise_specialisee',
      fr: 'Entreprise spécialisée',
    },
  ],
  SOLUTION_CV_necessite_robot: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'programmation_robot',
      fr: 'Programmation du robot',
    },
  ],
  SOLUTION_CV_necessite_fsa: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'programmation_fsa',
      fr: 'Programmation d\'une visite FSA',
    },
  ],
};
