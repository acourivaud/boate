import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Ouvrages',
    icon: 'fas fa-dungeon',
    link: '/pages/ouvrage',
  },
  {
    title: 'Surveillances',
    icon: 'fas fa-stethoscope',
    link: '/pages/surveillance',
  },
  {
    title: 'Bouclage',
    icon: 'fas fa-clock',
    children: [
      {
        title: 'Périodique',
        link: '/pages/bouclage/periodique',
      },
      {
        title: 'Expertise',
        link: '/pages/bouclage/expertise',
      },
      {
        title: 'SR / SP / Mesures OT',
        link: '/pages/bouclage/sr-sp-ot',
      },
      {
        title: 'Production FSA',
        link: '/pages/bouclage/production-fsa',
      },
      {
        title: 'Production TOPO',
        link: '/pages/bouclage/production-topo',
      },
      {
        title: 'Production INCLINO',
        link: '/pages/bouclage/production-inclino',
      },
    ],
  },
  {
    title: 'Planification',
    icon: 'fas fa-calendar-alt',
    children: [
      {
        title: 'Planning',
        link: '/pages/planning',
      },
      {
        title: 'Véhicules',
        link: '/pages/vehicule',
      },
      {
        title: 'Matériel',
        link: '/pages/materiel',
      },
      {
        title: 'Collaborateurs',
        link: '/pages/collaborateur',
      },
    ]
  },
  {
    title: 'Suivi Travaux',
    icon: 'fas fa-drafting-compass',
    link: '/pages/iot-dashboard',
  },
  {
    title: 'Suivi Visites',
    icon: 'fas fa-hard-hat',
    children: [
      {
        title: 'Tableau de Bord',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'Indicateurs IG TP',
        link: '/pages/iot-dashboard',
      },
    ],
  },
  {
    title: 'Etats',
    icon: 'fas fa-flag-checkered',
    children: [
      {
        title: 'Généraux',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'Cartographiques',
        link: '/pages/iot-dashboard',
      },
    ],
  },
  {
    title: 'Graphiques',
    icon: 'fas fa-chart-line',
    link: '/pages/iot-dashboard',
  },
  {
    title: 'Patrimoine',
    icon: 'fas fa-city',
    link: '/pages/iot-dashboard',
  },
  {
    title: 'Modèles PV',
    icon: 'fas fa-file-alt',
    link: '/pages/iot-dashboard',
  },
  {
    title: 'Archives',
    icon: 'fas fa-file-archive',
    children: [
      {
        title: 'Liste des Archives',
        link: '/pages/archive',
      },
      {
        title: 'Mes Emprunts',
        link: '/pages/mes-emprunts',
      },
    ],
  },
  {
    title: 'Données Géotechniques',
    icon: 'fas fa-map',
    link: '/pages/iot-dashboard',
  },
  {
    title: 'Utilitaires',
    icon: 'fas fa-cogs',
    children: [
      {
        title: 'Utilisateurs',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'Mon Profil',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'Répartition',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'Programme Prévisionnel',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'INFRAPOLE',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'Vitesse',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'UIC',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'IN 1253',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'IN 3044',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'Programmation Engins',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'Tournées',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'Documentation',
        link: '/pages/iot-dashboard',
      },
      {
        title: 'Espace de Travail',
        link: '/pages/iot-dashboard',
      },
    ],
  },
  {
    title: '',
    group: true,
  },
  {
    title: 'Administration',
    icon: 'fas fa-sitemap',
    link: '/pages/administration',
  },
  {
    title: '',
    group: true,
  },
  {
    title: 'DEMO THEME',
    group: true,
  },
  {
    title: 'E-commerce',
    icon: 'nb-e-commerce',
    link: '/pages/dashboard',
    //home: true,
  },
  {
    title: 'IoT Dashboard',
    icon: 'nb-home',
    link: '/pages/iot-dashboard',
  },
  {
    title: 'Extra Components',
    icon: 'nb-star',
    children: [
      {
        title: 'Calendar',
        link: '/pages/extra-components/calendar',
      },
      {
        title: 'Stepper',
        link: '/pages/extra-components/stepper',
      },
      {
        title: 'List',
        link: '/pages/extra-components/list',
      },
      {
        title: 'Infinite List',
        link: '/pages/extra-components/infinite-list',
      },
      {
        title: 'Accordion',
        link: '/pages/extra-components/accordion',
      },
      {
        title: 'Progress Bar',
        link: '/pages/extra-components/progress-bar',
      },
      {
        title: 'Spinner',
        link: '/pages/extra-components/spinner',
      },
      {
        title: 'Alert',
        link: '/pages/extra-components/alert',
      },
      {
        title: 'Tree',
        link: '/pages/extra-components/tree',
      },
      {
        title: 'Tabs',
        link: '/pages/extra-components/tabs',
      },
      {
        title: 'Calendar Kit',
        link: '/pages/extra-components/calendar-kit',
      },
      {
        title: 'Chat',
        link: '/pages/extra-components/chat',
      },
    ],
  },
  {
    title: 'Forms',
    icon: 'nb-compose',
    children: [
      {
        title: 'Form Inputs',
        link: '/pages/forms/inputs',
      },
      {
        title: 'Form Layouts',
        link: '/pages/forms/layouts',
      },
      {
        title: 'Buttons',
        link: '/pages/forms/buttons',
      },
      {
        title: 'Datepicker',
        link: '/pages/forms/datepicker',
      },
    ],
  },
  {
    title: 'UI Features',
    icon: 'nb-keypad',
    link: '/pages/ui-features',
    children: [
      {
        title: 'Grid',
        link: '/pages/ui-features/grid',
      },
      {
        title: 'Icons',
        link: '/pages/ui-features/icons',
      },
      {
        title: 'Typography',
        link: '/pages/ui-features/typography',
      },
      {
        title: 'Animated Searches',
        link: '/pages/ui-features/search-fields',
      },
    ],
  },
  {
    title: 'Modal & Overlays',
    icon: 'nb-layout-default',
    children: [
      {
        title: 'Dialog',
        link: '/pages/modal-overlays/dialog',
      },
      {
        title: 'Window',
        link: '/pages/modal-overlays/window',
      },
      {
        title: 'Popover',
        link: '/pages/modal-overlays/popover',
      },
      {
        title: 'Toastr',
        link: '/pages/modal-overlays/toastr',
      },
      {
        title: 'Tooltip',
        link: '/pages/modal-overlays/tooltip',
      },
    ],
  },
  {
    title: 'Bootstrap',
    icon: 'nb-gear',
    children: [
      {
        title: 'Form Inputs',
        link: '/pages/bootstrap/inputs',
      },
      {
        title: 'Buttons',
        link: '/pages/bootstrap/buttons',
      },
      {
        title: 'Modal',
        link: '/pages/bootstrap/modal',
      },
    ],
  },
  {
    title: 'Maps',
    icon: 'nb-location',
    children: [
      {
        title: 'Google Maps',
        link: '/pages/maps/gmaps',
      },
      {
        title: 'Leaflet Maps',
        link: '/pages/maps/leaflet',
      },
      {
        title: 'Bubble Maps',
        link: '/pages/maps/bubble',
      },
      {
        title: 'Search Maps',
        link: '/pages/maps/searchmap',
      },
    ],
  },
  {
    title: 'Charts',
    icon: 'nb-bar-chart',
    children: [
      {
        title: 'Echarts',
        link: '/pages/charts/echarts',
      },
      {
        title: 'Charts.js',
        link: '/pages/charts/chartjs',
      },
      {
        title: 'D3',
        link: '/pages/charts/d3',
      },
    ],
  },
  {
    title: 'Editors',
    icon: 'nb-title',
    children: [
      {
        title: 'CKEditor',
        link: '/pages/editors/ckeditor',
      },
    ],
  },
  {
    title: 'Miscellaneous',
    icon: 'nb-shuffle',
    children: [
      {
        title: '404',
        link: '/pages/miscellaneous/404',
      },
    ],
  },
  /*{
    title: 'Auth',
    icon: 'nb-locked',
    children: [
      {
        title: 'Login',
        link: '/auth/login',
      },
      {
        title: 'Register',
        link: '/auth/register',
      },
      {
        title: 'Request Password',
        link: '/auth/request-password',
      },
      {
        title: 'Reset Password',
        link: '/auth/reset-password',
      },
    ],
  },*/
];
