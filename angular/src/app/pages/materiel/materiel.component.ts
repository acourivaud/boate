import { FR_LOCALE } from './../../constant/CalendarFrLocale';
import { Vehicule } from './../../model/vehicule';
import { ArchiveBoite } from './../../model/archiveBoite';
import { ColumnHelper } from './../../model/columnHelper';
import { ExportExcelHelper } from './../../model/exportExcelHelper';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { BackUrlConstant } from './../../constant/BackUrlConstant';
import { DateDto } from './../../model/dateDto';
import { PlanningUtilisateurMateriel } from './../../model/planningUtilisateurMateriel';
import { Materiel } from './../../model/materiel';
import { FrontUrlConstant } from './../../constant/FrontUrlConstant';
import { GeometrieType } from './../../model/geometrieType';
import { Type } from './../../model/type';
import { Categorie } from './../../model/categorie';
import { GeneralService } from './../../service/general.service';
import { Geometrie } from './../../model/geometrie';
import { Table } from 'primeng/table';
import { ParamValue } from './../../model/paramValue';
import { MenuItem } from 'primeng/components/common/menuitem';
import { ToastrHelper } from './../../model/toastrHelper';
import { NbDialogService, NbToastrService, NbAccordionItemComponent } from '@nebular/theme';
import { FilterService } from './../../service/filter.service';
import { Ligne } from '../../model/ligne';
import { UniteOperationnelle } from '../../model/uniteOperationnelle';
import { Infrapole } from './../../model/infrapole';
import { Region } from './../../model/region';
import { Utilisateur } from '../../model/utilisateur';
import { UtilisateurService } from '../../service/utilisateur.service';
import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { Component, ViewChild, TemplateRef, ViewChildren, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import { Secteur } from '../../model/secteur';
import { Portion } from '../../model/portion';
import { AutoComplete } from 'primeng/autocomplete';
import { CrudHelper } from '../../model/crudHelper';
import { Router } from '@angular/router';
import { ReservationMaterielUtilisateur } from '../../model/reservationMaterielUtilisateur';
import { Calendar } from 'primeng/calendar';
import { ReservationVehiculeUtilisateur } from '../../model/reservationVehiculeUtilisateur';
import { PlanningUtilisateurVehicule } from '../../model/planningUtilisateurVehicule';

@Component({
  selector: 'materiel',
  styleUrls: ['./materiel.component.scss'],
  templateUrl: './materiel.component.html',
})
export class MaterielComponent implements OnInit, AfterViewInit {

  utilisateur: Utilisateur;

  activeDialogRef: any;

  toastrHelper: ToastrHelper;

  crudHelper: CrudHelper;
  @ViewChild('templateForEdit') cruPopupTemplate: TemplateRef<any>;
  @ViewChild('templateForDelete') deletePopupTemplate: TemplateRef<any>;

  isMaterielLoaded: boolean = false;
  isPlanningMaterielLoaded: boolean = false;

  selectedMateriel: Materiel;
  selectedUser: Utilisateur;
  userSuggestions: Utilisateur[];

  @ViewChild('materielAccordion') materielAccordion: NbAccordionItemComponent;
  @ViewChild('planningMaterielAccordion') planningMaterielAccordion: NbAccordionItemComponent;
  @ViewChild('reservationsMaterielAccordion') reservationsMaterielAccordion: NbAccordionItemComponent;

  @ViewChild('planningCalendarFrom') planningCalendarFrom: Calendar;
  planningCalendarFromValue: Date;
  planningCalendarFromValueForCompare: Date;
  selectedDateReservationsFrom: ReservationMaterielUtilisateur[];
  dateDebutPlanningMaterielFrom: Date;
  dateFinPlanningMaterielFrom: Date;
  planningUtilisateurMaterielFrom: PlanningUtilisateurMateriel;

  @ViewChild('planningCalendarTo') planningCalendarTo: Calendar;
  planningCalendarToValue: Date;
  planningCalendarToValueForCompare: Date;
  selectedDateReservationsTo: ReservationMaterielUtilisateur[];
  dateDebutPlanningMaterielTo: Date;
  dateFinPlanningMaterielTo: Date;
  planningUtilisateurMaterielTo: PlanningUtilisateurMateriel;

  @ViewChild('templateForConfirmation') confirmPopupTemplate: TemplateRef<any>;
  @ViewChild('templateForDeleteReservation') templateForDeleteReservation: TemplateRef<any>;
  existingResasForConfirmation: ReservationMaterielUtilisateur[];

  @ViewChild('planningCalendarDetails') planningCalendarDetails: Calendar;
  planningCalendarDetailsValue: Date;
  selectedDateReservationsDetails: ReservationMaterielUtilisateur[];
  dateDebutPlanningMaterielDetails: Date;
  dateFinPlanningMaterielDetails: Date;
  planningUtilisateurMaterielDetails: PlanningUtilisateurMateriel;
  selectedReservationToDelete: ReservationMaterielUtilisateur;

  fr: any = FR_LOCALE;

  constructor(private utilisateurService: UtilisateurService,
              private filterService: FilterService,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private generalService: GeneralService,
              private router: Router) {
  }

  ngOnInit() {
    const today = new Date();
    this.planningCalendarFromValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
    this.planningCalendarFromValueForCompare = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
    this.planningCalendarToValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 24, 0, 0);
    this.planningCalendarToValueForCompare = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 24, 0, 0);
    this.dateDebutPlanningMaterielFrom = new Date(today.getFullYear(), today.getMonth(), 1);
    this.dateFinPlanningMaterielFrom = new Date(today.getFullYear(), today.getMonth() + 1, 0, 23, 59, 59);
    this.dateDebutPlanningMaterielTo = new Date(today.getFullYear(), today.getMonth(), 1);
    this.dateFinPlanningMaterielTo = new Date(today.getFullYear(), today.getMonth() + 1, 0, 23, 59, 59);
    this.dateDebutPlanningMaterielDetails = new Date(today.getFullYear(), today.getMonth(), 1);
    this.dateFinPlanningMaterielDetails = new Date(today.getFullYear(), today.getMonth() + 1, 0, 23, 59, 59);
    this.planningCalendarDetailsValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
    this.utilisateur = this.utilisateurService.getUtilisateurConnected();
    this.selectedUser = this.utilisateur;
    const rows = this.utilisateur.nbLignesTableau ? parseInt(this.utilisateur.nbLignesTableau, 10) : 10;
    const crudObjectDefinitions: any = {
      Ligne: { object: new Ligne({}), table: null },
      Region: { object: new Region({}), table: null },
      Infrapole: { object: new Infrapole({}), table: null },
      Secteur: { object: new Secteur({}), table: null },
      UniteOperationnelle: { object: new UniteOperationnelle({}), table: null },
      Portion: { object: new Portion({}), table: null },
      Geometrie: { object: new Geometrie({}), table: null },
      Categorie: { object: new Categorie({}), table: null },
      Type: { object: new Type({}), table: null },
      GeometrieType: { object: new GeometrieType({}), table: null },
      Utilisateur: { object: new Utilisateur({}), table: null },
      Materiel: { object: new Materiel({}), table: null },
      ArchiveBoite: { object: new ArchiveBoite({}), table: null },
      Vehicule: { object: new Vehicule({}), table: null },
    };
    this.toastrHelper = new ToastrHelper(this.toastrService);
    this.crudHelper = new CrudHelper(this.generalService, this.dialogService, this.toastrHelper, crudObjectDefinitions, this.cruPopupTemplate, this.deletePopupTemplate, rows);
  }

  ngAfterViewInit() {
  }

  initialLoad(item: string) {
    if (item === 'Materiel') {
      if (!this.isMaterielLoaded) {
        this.crudHelper.load('Materiel');
        this.isMaterielLoaded = true;
      }
    } if (item === 'PlanningMateriel') {
      if (!this.isPlanningMaterielLoaded) {
        this.showPlanningForUtilisateur(this.selectedUser);
        this.isPlanningMaterielLoaded = true;
      }
    }
  }

  resolveFieldData(rowData, colField){
    return ObjectUtils.resolveFieldData(rowData, colField);
  }

  openCreateDialog(className: string) {
    this.crudHelper.openCreateDialog(className);
  }

  openEditDialog(object: any) {
    this.crudHelper.openEditDialog(object);
  }

  openDeleteDialog(object: any) {
    this.crudHelper.openDeleteDialog(object);
  }

  openDeleteReservationDialog(reservation: ReservationMaterielUtilisateur) {
    this.selectedReservationToDelete = reservation;
    this.dialogService.open(this.templateForDeleteReservation);
  }

  SearchUser($event) {
    if ($event.query.length > 2) {
      this.utilisateurService.getUtilisateurs($event.query).subscribe(data => {
        const userSuggestions: Utilisateur[] = [];
        if (data && data.length > 0)  {
          for (const user of data) {
            userSuggestions.push(new Utilisateur(user));
          }
          this.userSuggestions = userSuggestions;
        }
      });
    }
  }

  showPlanningForMaterielFromAndTo(materiel: Materiel) {
    this.materielAccordion.close();
    this.planningMaterielAccordion.open();
    this.showPlanningForMateriel(materiel, 'From');
    this.showPlanningForMateriel(materiel, 'To');
  }

  showPlanningForMateriel(materiel: Materiel, fromTo: string): void {
    let dateDebutPlanningMateriel: Date;
    let dateFinPlanningMateriel: Date;
    if (fromTo === 'From') {
      dateDebutPlanningMateriel = this.dateDebutPlanningMaterielFrom;
      dateFinPlanningMateriel = this.dateFinPlanningMaterielFrom;
    }
    if (fromTo === 'To') {
      dateDebutPlanningMateriel = this.dateDebutPlanningMaterielTo;
      dateFinPlanningMateriel = this.dateFinPlanningMaterielTo;
    }
    this.selectedMateriel = materiel;
    const planningUtilisateurMateriel = new PlanningUtilisateurMateriel(undefined);
    planningUtilisateurMateriel.dateDebut = DateDto.createFromDate(dateDebutPlanningMateriel).toBackEndDto();
    planningUtilisateurMateriel.dateFin = DateDto.createFromDate(dateFinPlanningMateriel).toBackEndDto();
    planningUtilisateurMateriel.materiel = this.selectedMateriel;
    planningUtilisateurMateriel.trueIfListOfUsersFalseIfListOfMateriels = true;
    this.generalService.postUrlWithObject(
      BackUrlConstant.PLANNING_MATERIEL + BackUrlConstant.BY_MATERIEL,
      planningUtilisateurMateriel
      ).subscribe(data => {
      if (fromTo === 'From') {
        this.planningUtilisateurMaterielFrom = new PlanningUtilisateurMateriel(data);
        this.showDetailsForSelectedDate(this.planningCalendarFromValue, 'From');
      }
      if (fromTo === 'To') {
        this.planningUtilisateurMaterielTo = new PlanningUtilisateurMateriel(data);
        this.showDetailsForSelectedDate(this.planningCalendarToValue, 'To');
      }
    });
  }

  showPlanningForUtilisateur(utlisateur: Utilisateur): void {
    const planningUtilisateurMateriel = new PlanningUtilisateurMateriel(undefined);
    planningUtilisateurMateriel.dateDebut = DateDto.createFromDate(this.dateDebutPlanningMaterielDetails).toBackEndDto();
    planningUtilisateurMateriel.dateFin = DateDto.createFromDate(this.dateFinPlanningMaterielDetails).toBackEndDto();
    planningUtilisateurMateriel.utilisateur = this.selectedUser;
    planningUtilisateurMateriel.trueIfListOfUsersFalseIfListOfMateriels = false;
    this.generalService.postUrlWithObject(
      BackUrlConstant.PLANNING_MATERIEL + BackUrlConstant.BY_UTILISATEUR,
      planningUtilisateurMateriel
      ).subscribe(data => {
        this.planningUtilisateurMaterielDetails = new PlanningUtilisateurMateriel(data);
        this.showDetailsForSelectedDate(this.planningCalendarDetailsValue, 'Details');
    });
  }

  isMaterielDateRangeConsistent(): boolean {
    if (!this.planningCalendarFromValue) return true;
    if (!this.planningCalendarToValue) return true;
    return this.planningCalendarFromValue.getTime() !== this.planningCalendarToValue.getTime() &&
      this.planningCalendarFromValue < this.planningCalendarToValue;
  }

  isMaterielPlanningAdmin(): boolean {
    return this.utilisateur.hasPermission('Admin Planning Materiel');
  }

  availabilityStatus(date: Date, fromTo: string): string {
    let planningUtilisateurMateriel: PlanningUtilisateurMateriel;
    if (fromTo === 'From') planningUtilisateurMateriel = this.planningUtilisateurMaterielFrom;
    if (fromTo === 'To') planningUtilisateurMateriel = this.planningUtilisateurMaterielTo;
    if (fromTo === 'Details') planningUtilisateurMateriel = this.planningUtilisateurMaterielDetails;
    if (planningUtilisateurMateriel) {
      if (planningUtilisateurMateriel.reservationMaterielUtilisateur &&
          planningUtilisateurMateriel.reservationMaterielUtilisateur.length > 0) {
        const filtered = planningUtilisateurMateriel.reservationMaterielUtilisateur.filter(
          rvu => rvu.getHoursTotalForDate(date) > 0
        );
        if (filtered && filtered.length > 0) {
          if (fromTo !== 'Details') {
            let totalHoursOfDay = 0;
            for (const resa of filtered) {
              totalHoursOfDay += resa.getHoursTotalForDate(date);
            }
            if (totalHoursOfDay >= 24) {
              return 'red';
            }
          }
          return 'orange';
        }
      }
    }
    return fromTo !== 'Details' ? 'green' : 'grey';
  }

  onSelectMaterielFrom(date: Date) {
    if (this.planningCalendarFromValue.getMinutes() !== this.planningCalendarFromValueForCompare.getMinutes()) {
      // We don't allow minutes to change
      this.planningCalendarFromValue = new Date(this.planningCalendarFromValueForCompare);
    } else if (this.planningCalendarFromValue.getHours() !== this.planningCalendarFromValueForCompare.getHours()) {
      if (this.planningCalendarFromValue.getHours() === 23 && this.planningCalendarFromValueForCompare.getHours() === 0) {
        const planningCalendarValue = new Date(
          this.planningCalendarFromValue.getFullYear(),
          this.planningCalendarFromValue.getMonth(),
          this.planningCalendarFromValue.getDate() - 1,
          23,
          0,
          0
        );
        this.planningCalendarFromValue = planningCalendarValue;
      } else if (this.planningCalendarFromValue.getHours() === 0 && this.planningCalendarFromValueForCompare.getHours() === 23) {
        const planningCalendarValue = new Date(
          this.planningCalendarFromValue.getFullYear(),
          this.planningCalendarFromValue.getMonth(),
          this.planningCalendarFromValue.getDate() + 1,
          0,
          0,
          0
        );
        this.planningCalendarFromValue = planningCalendarValue;
      }
      // We just backup the full date for next round
      this.planningCalendarFromValueForCompare = new Date(this.planningCalendarFromValue);
    } else {
      // We set the From and To dates to the make the default range of a full day
      const defaultFromValue: Date = new Date(
        this.planningCalendarFromValue.getFullYear(),
        this.planningCalendarFromValue.getMonth(),
        this.planningCalendarFromValue.getDate(),
        0,
        0,
        0
      );
      const defaultToValue: Date = new Date(
        this.planningCalendarFromValue.getFullYear(),
        this.planningCalendarFromValue.getMonth(),
        this.planningCalendarFromValue.getDate() + 1,
        0,
        0,
        0
      );
      this.planningCalendarFromValue = defaultFromValue;
      this.planningCalendarToValue = defaultToValue;
      this.showDetailsForSelectedDate(this.planningCalendarFromValue, 'From');
      this.showDetailsForSelectedDate(this.planningCalendarToValue, 'To');
      // Then we just backup the full date for next round
      this.planningCalendarFromValueForCompare = new Date(this.planningCalendarFromValue);
    }
  }

  onSelectMaterielTo(date: Date) {
    if (this.planningCalendarToValue.getMinutes() !== this.planningCalendarToValueForCompare.getMinutes()) {
      // We don't allow minutes to change
      this.planningCalendarToValue = new Date(this.planningCalendarToValueForCompare);
    } else {
      if (this.planningCalendarToValue.getHours() === 23 && this.planningCalendarToValueForCompare.getHours() === 0) {
        const planningCalendarValue = new Date(
          this.planningCalendarToValue.getFullYear(),
          this.planningCalendarToValue.getMonth(),
          this.planningCalendarToValue.getDate() - 1,
          23,
          0,
          0
        );
        this.planningCalendarToValue = planningCalendarValue;
      } else if (this.planningCalendarToValue.getHours() === 0 && this.planningCalendarToValueForCompare.getHours() === 23) {
        const planningCalendarValue = new Date(
          this.planningCalendarToValue.getFullYear(),
          this.planningCalendarToValue.getMonth(),
          this.planningCalendarToValue.getDate() + 1,
          0,
          0,
          0
        );
        this.planningCalendarToValue = planningCalendarValue;
      }
      // We just backup the full date for next round
      this.planningCalendarToValueForCompare = new Date(this.planningCalendarToValue);
    }
    this.showDetailsForSelectedDate(this.planningCalendarToValue, 'To');
  }

  showDetailsForSelectedDate(date: Date, fromTo: string) {
    const dateObject = {
      year: date.getFullYear(),
      month: date.getMonth(),
      day: date.getDate()
    };
    if (fromTo === 'From') {
      this.selectedDateReservationsFrom = this.planningUtilisateurMaterielFrom.reservationMaterielUtilisateur.filter(
        rvu => rvu.getHoursTotalForDate(dateObject)
      );
    }
    if (fromTo === 'To') {
      this.selectedDateReservationsTo = this.planningUtilisateurMaterielTo.reservationMaterielUtilisateur.filter(
        rvu => rvu.getHoursTotalForDate(dateObject)
      );
    }
    if (fromTo === 'Details') {
      this.selectedDateReservationsDetails = this.planningUtilisateurMaterielDetails.reservationMaterielUtilisateur.filter(
        rvu => rvu.getHoursTotalForDate(dateObject)
      );
    }
  }

  loadMaterielReservationsForMonth(selectedMonth: any, fromTo: string) {
    if (fromTo === 'From') {
      this.dateDebutPlanningMaterielFrom = new Date(selectedMonth.year, selectedMonth.month - 1, 1, 0, 0, 0);
      this.dateFinPlanningMaterielFrom = new Date(selectedMonth.year, selectedMonth.month, 0, 23, 59, 59);
      this.showPlanningForMateriel(this.selectedMateriel, fromTo);
    }
    if (fromTo === 'To') {
      this.dateDebutPlanningMaterielTo = new Date(selectedMonth.year, selectedMonth.month - 1, 1, 0, 0, 0);
      this.dateFinPlanningMaterielTo = new Date(selectedMonth.year, selectedMonth.month, 0, 23, 59, 59);
      this.showPlanningForMateriel(this.selectedMateriel, fromTo);
    }
    if (fromTo === 'Details') {
      this.dateDebutPlanningMaterielDetails = new Date(selectedMonth.year, selectedMonth.month - 1, 1, 0, 0, 0);
      this.dateFinPlanningMaterielDetails = new Date(selectedMonth.year, selectedMonth.month, 0, 23, 59, 59);
      this.showPlanningForUtilisateur(this.selectedUser);
    }
  }

  bookReservation(forceUpdateIfPossible: boolean = false) {
    const planningUtilisateurMateriel = new PlanningUtilisateurMateriel(undefined);
    planningUtilisateurMateriel.dateDebut = DateDto.createFromDate(this.dateDebutPlanningMaterielFrom).toBackEndDto();
    planningUtilisateurMateriel.dateFin = DateDto.createFromDate(this.dateFinPlanningMaterielFrom).toBackEndDto();
    planningUtilisateurMateriel.materiel = this.selectedMateriel;
    planningUtilisateurMateriel.trueIfListOfUsersFalseIfListOfMateriels = true;
    planningUtilisateurMateriel.reservationMaterielUtilisateur = [];
    planningUtilisateurMateriel.forceUpdateIfPossible = forceUpdateIfPossible;
    const resa = new ReservationMaterielUtilisateur(undefined);
    resa.dateDebut = DateDto.createFromDate(this.planningCalendarFromValue).toBackEndDto();
    const planningCalendarToValue = new Date(
      this.planningCalendarToValue.getFullYear(),
      this.planningCalendarToValue.getMonth(),
      this.planningCalendarToValue.getDate(),
      this.planningCalendarToValue.getHours() - 1,
      59,
      59
    );
    resa.dateFin = DateDto.createFromDate(planningCalendarToValue).toBackEndDto();
    resa.trueIfListOfUsersFalseIfListOfMateriels = true;
    resa.utilisateur = this.selectedUser;
    resa.materiel = this.selectedMateriel;
    planningUtilisateurMateriel.reservationMaterielUtilisateur.push(resa);
    this.generalService.postUrlWithObject(
      BackUrlConstant.PLANNING_MATERIEL,
      planningUtilisateurMateriel
      ).subscribe(data => {
      const planningUtilisateurMaterielFrom = new PlanningUtilisateurMateriel(data);
      if (planningUtilisateurMaterielFrom.conflictOnMaterielResponse === true) {
        this.toastrHelper.showDefaultErrorToast('Le matériel sélectionné est déjà réservé sur la plage horaire demandée.');
      } else if (planningUtilisateurMaterielFrom.conflictOnUserResponse === true) {
        this.existingResasForConfirmation = planningUtilisateurMaterielFrom.reservationMaterielUtilisateur;
        this.dialogService.open(this.confirmPopupTemplate);
      } else {
        this.selectedDateReservationsFrom = undefined;
        this.selectedDateReservationsTo = undefined;
        this.showPlanningForMateriel(this.selectedMateriel, 'From');
        this.showPlanningForMateriel(this.selectedMateriel, 'To');
        this.showPlanningForUtilisateur(this.selectedUser);
        this.selectedMateriel = undefined;
        this.planningMaterielAccordion.close();
        this.reservationsMaterielAccordion.open();
        this.toastrHelper.showDefaultSuccessToast();
      }
    });
  }

  confirmReservation(): void {
    this.bookReservation(true);
  }

  OnSelectUser($event) {
    this.showPlanningForUtilisateur(this.selectedUser);
  }

  confirmDelete() {
    this.deleteReservation();
  }

  deleteReservation(): void {
    const planningUtilisateurMateriel = new PlanningUtilisateurMateriel(undefined);
    planningUtilisateurMateriel.dateDebut = DateDto.createFromDate(this.dateDebutPlanningMaterielDetails).toBackEndDto();
    planningUtilisateurMateriel.dateFin = DateDto.createFromDate(this.dateFinPlanningMaterielDetails).toBackEndDto();
    planningUtilisateurMateriel.utilisateur = this.selectedUser;
    planningUtilisateurMateriel.trueIfListOfUsersFalseIfListOfMateriels = false;
    planningUtilisateurMateriel.reservationMaterielUtilisateur = [];
    planningUtilisateurMateriel.reservationMaterielUtilisateur.push(new ReservationMaterielUtilisateur(this.selectedReservationToDelete));
    this.generalService.deleteUrl(
      BackUrlConstant.PLANNING_MATERIEL,
      planningUtilisateurMateriel
      ).subscribe(data => {
        this.planningUtilisateurMaterielDetails = new PlanningUtilisateurMateriel(data);
        this.showDetailsForSelectedDate(this.planningCalendarDetailsValue, 'Details');
        this.toastrHelper.showDefaultSuccessToast();
    });
  }

  canEditedObjectBeSaved(): boolean {
    return this.crudHelper.canEditedObjectBeSaved();
  }
}
