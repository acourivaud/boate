import { NgModule } from '@angular/core';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { CheckboxModule } from 'primeng/checkbox';
import { SliderModule } from 'primeng/slider';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { CalendarModule } from 'primeng/calendar';

import { ThemeModule } from '../../@theme/theme.module';
import { GeneralService } from '../../service/general.service';
import { UtilisateurService } from '../../service/utilisateur.service';
import { MaterielComponent } from './materiel.component';

@NgModule({
  imports: [
    ThemeModule,
    TableModule,
    CheckboxModule,
    BreadcrumbModule,
    TooltipModule,
    AutoCompleteModule,
    SliderModule,
    CalendarModule
  ],
  declarations: [
    MaterielComponent,
  ],
  providers: [
    UtilisateurService,
    GeneralService
  ]
})
export class MaterielModule { }
