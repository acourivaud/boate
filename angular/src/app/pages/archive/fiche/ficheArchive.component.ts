import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

import { ArchiveDossier } from '../../../model/archiveDossier';
import { FieldBindingHelper } from '../../../model/FieldBindingHelper';
import { PageHelper } from '../../../model/pageHelper';
import { ParamObject } from '../../../model/paramObject';
import { ToastrHelper } from '../../../model/toastrHelper';
import { Utilisateur } from '../../../model/utilisateur';
import { ArchiveService } from '../../../service/archive.service';
import { GeneralService } from '../../../service/general.service';
import { LocalStorageService } from '../../../service/localStorage.service';
import { ParamService } from '../../../service/param.service';
import { UtilisateurService } from '../../../service/utilisateur.service';

@Component({
  selector: 'ngx-fiche-archive',
  styleUrls: ['./ficheArchive.component.scss'],
  templateUrl: './ficheArchive.component.html',
})
export class FicheArchiveComponent implements OnInit {

  archiveId: number;
  currentArchive: ArchiveDossier;

  // Socle pour NgxPageDefinition
  toastrHelper: ToastrHelper;
  paramArray: ParamObject[] = [];
  pageHelper: PageHelper;

  constructor(private archiveService: ArchiveService, private toastrService: NbToastrService, private generalService: GeneralService, private paramService: ParamService,
    private localStorageService: LocalStorageService, private dialogService: NbDialogService, private utilisateurService: UtilisateurService, private cd: ChangeDetectorRef) {
    this.toastrHelper = new ToastrHelper(toastrService);
    this.pageHelper = new PageHelper(this.generalService, this.paramService, this.toastrService, this.localStorageService, this.dialogService, this.utilisateurService);
  }

  ngOnInit() {
    this.archiveId = this.localStorageService.getItem('archive_id') ? Number.parseInt(this.localStorageService.getItem('archive_id')) : null;
    if (this.archiveId) { // Ouvrage existant
      this.initArchiveDescription();
    } else {
      // TODO --> Tester sur une création d'ouvrage pour voir le comportement
      this.pageHelper.loadingPage = true;
    }
  }

  initArchiveDescription() {
    this.archiveService.getArchive(this.archiveId).subscribe(data => {
      this.currentArchive = data;
      this.localStorageService.setItem('currentArchive', JSON.stringify(this.currentArchive));
      this.getUtilisateurConnected();
    });
  }

  initPageTemplate() {
    this.pageHelper.titlePage = 'Fiche de l\'archive';
    this.pageHelper.paramArray = this.initParamArray();
    this.pageHelper.fieldBindingHelper = this.initBindingData();
    this.pageHelper.initFromTemplateName('ficheArchive');
  }

  initParamArray(): ParamObject[] {
    this.paramArray.push(new ParamObject('paramId', this.archiveId ? this.archiveId.toString() : null, null));
    return this.paramArray;
  }

  initBindingData(): FieldBindingHelper {
    return new FieldBindingHelper(this.currentArchive);
  }

  enregistrerOuvrage() {
    this.pageHelper.fieldBindingHelper.bindHtmlToObject();
    this.archiveService.saveArchive(this.currentArchive).subscribe(data => {
      this.toastrHelper.showDefaultSuccessToast();
      this.initArchiveDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  supprimerOuvrage() {
    this.archiveService.deleteArchive(this.currentArchive).subscribe(data => {
      this.toastrHelper.showToast(NbToastStatus.SUCCESS, 'Modifications enregistrées', data.message);
      this.initArchiveDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  getUtilisateurConnected() {
    this.utilisateurService.getUtilisateurConnectedFromBack().subscribe(data => {
      this.pageHelper.utilisateur = new Utilisateur(data);
      this.initPageTemplate();
    });
  }
}
