import { ExportExcelHelper } from './../../model/exportExcelHelper';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Component, ViewChild } from '@angular/core';
import { SelectItem } from 'primeng/components/common/selectitem';
import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { Table } from 'primeng/table';

import { ArchiveDossier } from '../../model/archiveDossier';
import { ColumnHelper } from '../../model/columnHelper';
import { FilterHelper } from '../../model/filterHelper';
import { ParamValue } from '../../model/paramValue';
import { Region } from '../../model/region';
import { Utilisateur } from '../../model/utilisateur';
import { ArchiveService } from '../../service/archive.service';
import { ColumnService } from '../../service/column.service';
import { FilterService } from '../../service/filter.service';
import { UtilisateurService } from '../../service/utilisateur.service';
import { MENU_ITEMS } from './archive-menu';
import { ArchiveBoite } from '../../model/archiveBoite';
import { NbToastrService } from '@nebular/theme';
import { ToastrHelper } from '../../model/toastrHelper';
import { Router } from '@angular/router';
import { LocalStorageService } from '../../service/localStorage.service';
import { FrontUrlConstant } from '../../constant/FrontUrlConstant';

@Component({
  selector: 'archive',
  styleUrls: ['./archive.component.scss'],
  templateUrl: './archive.component.html',
})
export class ArchiveComponent {

  utilisateur: Utilisateur;

  selectedValue: ArchiveDossier;
  archives: ArchiveDossier[];
  selectedArchives: ArchiveDossier[];
  totalRecords: number;
  loading: boolean;
  exportLoading: boolean;
  inputFilter: RegExp = /^[0-9/./,]$/;

  toastrHelper: ToastrHelper;

  @ViewChild('accordionRechercher') accordionRechercher;

  @ViewChild('archiveTable') archiveTable: Table;

  activeFilters: FilterHelper;
  columnHelper: ColumnHelper;

  pendingBackendResponses: number;

  conditionalCols = [
    { conditions: 'archiveRattachee', col: { field: 'ouvrage.line.code', header: 'Ligne', style: 'column-center column-width-30' } },
    { conditions: 'archiveRattachee', col: { field: 'ouvrage.pk', header: 'PK', style: 'column-center column-width-30' } },
    { conditions: 'archiveRattachee', col: { field: 'ouvrage.middlePk', header: 'PK Milieu', style: 'column-center column-width-30' } },
    { conditions: 'archiveRattachee', col: { field: 'ouvrage.endPk', header: 'PK Fin', style: 'column-center column-width-30' } },
    { conditions: 'archiveRattachee', col: { field: 'ouvrage.categorie', header: 'Catégorie', style: 'column-center column-width-50' } },
    { conditions: 'archiveNonRattachee', col: { field: 'ligneNonRattachee', header: 'Ligne', style: 'column-center column-width-30' } },
    { conditions: 'archiveNonRattachee', col: { field: 'pk', header: 'PK', style: 'column-center column-width-30' } },
    { conditions: 'archiveNonRattachee', col: { field: 'endPk', header: 'PK Fin', style: 'column-center column-width-30' } },
    { conditions: 'archiveNonRattachee', col: { field: 'service', header: 'Catégorie', style: 'column-center column-width-30' } },
    { conditions: 'archiveRattachee|archiveNonRattachee', col: { field: 'description', header: 'Description', style: 'column-center column-width-150' } },
    { conditions: 'archiveRattachee|archiveNonRattachee', col: { field: 'commentaires', header: 'Commentaires', style: 'column-center column-width-50' } },
    { conditions: 'archiveRattachee|archiveNonRattachee', col: { field: 'boite.nom', header: 'N° boîte', style: 'column-center column-width-50' } },
    { conditions: 'archiveRattachee|archiveNonRattachee', col: { field: 'boite.localisation', header: 'Localisation', style: 'column-center column-width-50' } },
  ];

  constructor(private utilisateurService: UtilisateurService,
              private archiveService: ArchiveService,
              private filterService: FilterService,
              private columnService: ColumnService,
              private toastrService: NbToastrService,
              private router: Router,
              private localStorageService: LocalStorageService) {

    this.utilisateur = utilisateurService.getUtilisateurConnected();

    const useFiltersForRequestsArg = this;
    const useFiltersForRequestsCallback = (arg: any): void => {

      const labels: string[] = [];

      if ((arg.activeFilters.filters.GenLigneDeclasseeSelected != null && arg.activeFilters.filters.GenLigneDeclasseeSelected.raw === 'true') ||
        (arg.activeFilters.filters.GenDocumentGenerauxSelected != null && arg.activeFilters.filters.GenDocumentGenerauxSelected.raw === 'true')) {
        arg.columnHelper.setOptionalCols('optionalColsDetachee');
        labels.push('archiveNonRattachee');
      } else {
        arg.columnHelper.setOptionalCols('optionalColsNull');
        labels.push('archiveRattachee');
      }

      arg.columnHelper.adaptConditionalColumnsToRequest(labels);
      arg.columnHelper.applyColumnsOrder();

    };
    this.activeFilters = new FilterHelper(filterService, 'archives', useFiltersForRequestsCallback, useFiltersForRequestsArg);

    this.toastrHelper = new ToastrHelper(this.toastrService);

    if (this.utilisateur.nbLignesTableau) {
      this.activeFilters.rows = Number.parseInt(this.utilisateur.nbLignesTableau);
    }

    this.activeFilters.options = {
      // Généralité
      GenRegionOptions: null,
      GenInfrapoleOptions: null,
      GenSecteurOptions: null,
      GenUoOptions: null,
      GenUicOptions: MENU_ITEMS.UIC,
      GenLigneOptions: null,
      GenCategorieOptions: MENU_ITEMS.WORK_CATEGORY,
      GenTypeOptions: MENU_ITEMS.WORK_TYPE_CATEGORY_NULL,
      GenEtatDossierOptions: MENU_ITEMS.ETAT_DOSSIER,
      GenEmprunteurOptions: null,
      GenNumBoiteOptions: null,
      GenLigneDeclasseeOptions: MENU_ITEMS.BOOLEAN,
      GenDocumentGenerauxOptions: MENU_ITEMS.BOOLEAN,
    };

    this.activeFilters.filters = {
      // ATTENTION LES SUFFIXES ONT UN IMPACT SUR LE PROGRAMME
      // (Selected, SelectedSearch, Typed, TypedSearch)

      // Généralité
      GenRegionSelected: null,
      GenInfrapoleSelected: null,
      GenSecteurSelected: null,
      GenUoSelected: null,
      GenUicSelected: null,
      GenLigneSelected: null,
      GenFromTyped: null,
      GenToTyped: null,
      GenCategorieSelected: null,
      GenTypeSelected: null,
      GenEtatDossierSelected: null,
      GenEmprunteurSelectedSearch: null,
      GenNumBoiteSelectedSearch: null,
      GenLigneDeclasseeSelected: null,
      GenDocumentGenerauxSelected: null,
    };

    this.loading = true;

    const optionalCols: { optionalColsName: string, optionalCols: SelectItem[] }[] = [
      {
        optionalColsName:  'optionalColsNull',
        optionalCols: [
          { label: 'Ligne', value: { id: 101, col: { field: 'ouvrage.line.code', header: 'Ligne', style: 'column-center column-width-100' }, children: null } },
          { label: 'PK', value: { id: 102, col: { field: 'ouvrage.pk', header: 'PK', style: 'column-center column-width-100' }, children: null } },
          { label: 'PK Milieu', value: { id: 103, col: { field: 'ouvrage.middlePk', header: 'PK Milieu', style: 'column-center column-width-100' }, children: null } },
          { label: 'PK Fin', value: { id: 104, col: { field: 'ouvrage.endPk', header: 'PK Fin', style: 'column-center column-width-100' }, children: null } },
          { label: 'Catégorie', value: { id: 105, col: { field: 'ouvrage.categorie', header: 'Catégorie', style: 'column-center column-width-80' }, children: null } },
          { label: 'Description', value: { id: 106, col: { field: 'description', header: 'Description', style: 'column-center column-width-100' }, children: null } },
          { label: 'Commentaires', value: { id: 107, col: { field: 'commentaires', header: 'Commentaires', style: 'column-center column-width-200' }, children: null } },
          { label: 'N° Boîte', value: { id: 108, col: { field: 'boite.nom', header: 'N° Boîte', style: 'column-center column-width-150' }, children: null } },
          { label: 'Localisation', value: { id: 109, col: { field: 'boite.localisation', header: 'Localisation', style: 'column-center column-width-150' }, children: null } }
        ]
      },
      {
        optionalColsName: 'optionalColsDetachee',
        optionalCols: [
          { label: 'Ligne', value: { id: 101, col: { field: 'ligneNonRattachee', header: 'Ligne', style: 'column-center column-width-100' }, children: null } },
          { label: 'PK', value: { id: 102, col: { field: 'pk', header: 'PK', style: 'column-center column-width-100' }, children: null } },
          { label: 'PK fin', value: { id: 103, col: { field: 'endPk', header: 'PK', style: 'column-center column-width-100' }, children: null } },
          { label: 'Catégorie', value: { id: 104, col: { field: 'service', header: 'Catégorie', style: 'column-center column-width-80' }, children: null } },
          { label: 'Description', value: { id: 105, col: { field: 'description', header: 'Description', style: 'column-center column-width-100' }, children: null } },
          { label: 'Commentaires', value: { id: 106, col: { field: 'commentaires', header: 'Commentaires', style: 'column-center column-width-200' }, children: null } },
          { label: 'N° Boîte', value: { id: 107, col: { field: 'boite.nom', header: 'N° Boîte', style: 'column-center column-width-150' }, children: null } },
          { label: 'Localisation', value: { id: 108, col: { field: 'boite.localisation', header: 'Localisation', style: 'column-center column-width-150' }, children: null } }
        ]
      }
    ];

    this.columnHelper = new ColumnHelper(this.conditionalCols, optionalCols, this.columnService, 'archives', true, false);

    this.pendingBackendResponses = 2;

    this.filterService.getRegions().subscribe(data => {
      this.activeFilters.options.GenRegionOptions = [ { raw: null, fr: '' } ];
      for (let regionDto of data) {
        let region : Region = new Region(regionDto);
        this.activeFilters.options.GenRegionOptions.push({
          raw: region.id.toString(),
          fr: region.name
        });
      }
      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });

    this.regionInfrapoleSecteurUoChange();
  }

  getFiltersForConnectedUser() {
    // Ne fonctionne que quand tous les appels précédents ont répondu (UNE SEULE FOIS)
    if (!this.activeFilters.areConnectedUserFiltersLoaded && this.pendingBackendResponses == 0) {
      let self = this;
      let callback = (arg: any) : void => {
        arg.activeFilters.useFiltersForRequests(true);
        if (!arg.activeFilters.connectedUserFilters
            || arg.activeFilters.connectedUserFilters == null
            || arg.activeFilters.connectedUserFilters.length == 0) {
          if (arg.utilisateur.up) {
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenUo', arg.utilisateur.up ? arg.utilisateur.up.id : null);
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenSecteur', arg.utilisateur.secteur ? arg.utilisateur.secteur.id : null);
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenInfrapole', arg.utilisateur.infrapole ? arg.utilisateur.infrapole.id : null);
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenRegion', arg.utilisateur.region ? arg.utilisateur.region.id : null);
          }
        }
      };
      this.activeFilters.populateDefaultFiltersForConnectedUser(callback, self);
      this.columnHelper.getColumnsOrder();
    }
  }

  loadArchivesLazy(event: any) {
    setTimeout(() => {
      this.fetchDataFromBackEnd(event, false);
    });
  }

  fetchDataFromBackEnd(event: any, saveFilters: boolean) {
    this.loading = true;

    const requestFilters: ParamValue[] = this.activeFilters.generateRequestFilters(event, this.archiveTable);

    this.archiveService.getArchives(saveFilters, requestFilters).subscribe(data => {
      this.totalRecords = data.count;
      this.archives = [];
      for (const archiveDTO of data.result) {
        this.archives.push(new ArchiveDossier(archiveDTO, this.utilisateurService.getUtilisateurConnected()));
      }
      this.loading = false;
    });
  }

  resolveFieldData(rowData, colField){
    return ObjectUtils.resolveFieldData(rowData, colField);
  }

  // Généralité

  GenRegionChange($event) {
    this.regionInfrapoleSecteurUoChange();
  }

  GenInfrapoleChange($event) {
    this.regionInfrapoleSecteurUoChange();
  }

  GenSecteurChange($event) {
    this.regionInfrapoleSecteurUoChange();
  }

  GenUoChange($event) {
    this.regionInfrapoleSecteurUoChange();
  }

  regionInfrapoleSecteurUoChange() {
    this.filterService.getRegionInfrapoleSecteurUoLigne(
        this.activeFilters.getSelectedRawString('GenRegionSelected'),
        this.activeFilters.getSelectedRawString('GenInfrapoleSelected'),
        this.activeFilters.getSelectedRawString('GenSecteurSelected'),
        this.activeFilters.getSelectedRawString('GenUoSelected')
      ).subscribe(data => {

      const GenInfrapoleSelected = this.activeFilters.getSelectedRawString('GenInfrapoleSelected');
      const GenSecteurSelected = this.activeFilters.getSelectedRawString('GenSecteurSelected');
      const GenUoSelected = this.activeFilters.getSelectedRawString('GenUoSelected');
      const GenLigneSelected = this.activeFilters.getSelectedRawString('GenLigneSelected');

      const infrapoles = [ { raw: null, fr: '' } ];
      const secteurs = [ { raw: null, fr: '' } ];
      const unitesOperationnelle = [ { raw: null, fr: '' } ];
      const lignes = [ { raw: null, fr: '' } ];

      for (const infrapoleDTO of data.infrapoles) {
        infrapoles.push({ raw: infrapoleDTO.id.toString(), fr: infrapoleDTO.name });
      }

      for (const secteurDTO of data.secteurs) {
        secteurs.push({ raw: secteurDTO.id.toString(), fr: secteurDTO.nom });
      }

      for (const uniteOperationnelleDTO of data.unitesOperationnelle) {
        unitesOperationnelle.push({ raw: uniteOperationnelleDTO.id.toString(), fr: uniteOperationnelleDTO.name });
      }

      for (const ligneDTO of data.lignes) {
        lignes.push({ raw: ligneDTO.id.toString(), fr: ligneDTO.nom });
      }

      this.activeFilters.options.GenInfrapoleOptions = infrapoles;
      this.activeFilters.options.GenSecteurOptions = secteurs;
      this.activeFilters.options.GenUoOptions = unitesOperationnelle;
      this.activeFilters.options.GenLigneOptions = lignes;

      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenInfrapole', GenInfrapoleSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenSecteur', GenSecteurSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenUo', GenUoSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenLigne', GenLigneSelected);

      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });
  }

  GenCategorieChange($event) {
    if (this.activeFilters.filters['GenCategorieSelected'] != null) {
      let GenTypeSelected = this.activeFilters.getSelectedRawString('GenTypeSelected');
      let self = this;
      switch (this.activeFilters.filters['GenCategorieSelected'].raw) {
        case null :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_NULL;
          break;
        case 'category_Autre' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_OTHERS;
          this.activeFilters.razAllDescriptionFilters();
          break;
        case 'category_Bat' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_BAT;
          this.activeFilters.razAllDescriptionFilters();
          break;
        case 'category_OA' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_OA;
          this.activeFilters.razAllDescriptionFilters();
          break;
        case 'category_OT' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_OT;
          this.activeFilters.razAllDescriptionFilters();
          break;
        case 'category_Hydro' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_HYDRO;
          this.activeFilters.razAllDescriptionFilters();
          break;
      }
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenType', GenTypeSelected);
      this.GenTypeChange(null);
    }
  }

  GenTypeChange($event) {
    if (this.activeFilters.filters['GenTypeSelected'] != null) {
      if (this.activeFilters.filters['GenTypeSelected'].raw != null) {
        let menuItem = MENU_ITEMS;
        let options = menuItem['GEOMETRY_' + this.activeFilters.filters['GenTypeSelected'].raw];
        this.activeFilters.options.DesAoGeometrieOptions = options;
        this.activeFilters.options.DesOaGeometrieOptions = options;
      } else {
        this.activeFilters.options.DesAoGeometrieOptions = MENU_ITEMS.GEOMETRY_NULL;
        this.activeFilters.options.DesOaGeometrieOptions = MENU_ITEMS.GEOMETRY_NULL;
      }
    }
  }

  GenNumBoiteSearch($event) {
    this.getNumBoite($event.query);
  }

  getNumBoite(searchText: string) {
    if (searchText && searchText.length > 3) {
      this.archiveService.getBoitesSearch(searchText).subscribe(data => {

        const GenNumBoiteSelected = this.activeFilters.getSelectedRawString('GenNumBoiteSelected');

        const numBoites = [];

        for (const archivesBoite of data) {
          const archiveBoite = new ArchiveBoite(archivesBoite);
          numBoites.push({ raw: archiveBoite.id, fr: archiveBoite.nom });
        }

        this.activeFilters.options.GenNumBoiteOptions = numBoites;

        this.activeFilters.trySetSelectedFromOptionsIfPossible('GenNumBoite', GenNumBoiteSelected);

      });
    } else {
      this.activeFilters.options.GenNumBoiteOptions = [];
    }
  }

  GenEmprunteurSearch($event) {
    this.getEmprunteur($event.query);
  }

  getEmprunteur(searchText: string) {
    if (searchText) {
      this.utilisateurService.getUtilisateurs(searchText).subscribe(data => {

        const GenEmprunteurSelected = this.activeFilters.getSelectedRawString('GenEmprunteurSelected');

        const emprunteurs = [];

        for (const utilisateurDTO of data) {
          const utilisateur = new Utilisateur(utilisateurDTO);
          emprunteurs.push({ raw: utilisateur.idUtilisateur.toString(), fr: utilisateur.fullName });
        }

        this.activeFilters.options.GenEmprunteurOptions = emprunteurs;

        this.activeFilters.trySetSelectedFromOptionsIfPossible('GenEmprunteur', GenEmprunteurSelected);

      });
    } else {
      this.activeFilters.options.GenEmprunteurOptions = [];
    }
  }

  // Particularités
  ParNomSearch($event) {
    this.getOuvragesName($event.query);
  }

  getOuvragesName(searchText: string) {
    if (searchText.length > 2) {
      this.filterService.getOuvragesName(searchText).subscribe(data => {
        let ouvragesName = [];
        for (let ouvrageDTO of data) {
          ouvragesName.push({ raw: ouvrageDTO.name });
        }
        this.activeFilters.options.ParNomOptions = ouvragesName;
      });
    } else {
      this.activeFilters.options.ParNomOptions = [];
    }
  }

  onChercherClick() {
    this.activeFilters.useFiltersForRequests(true);
    this.fetchDataFromBackEnd(null, true);
    this.accordionRechercher.close();
  }

  onRazClick() {
    this.activeFilters.razAllFilters();
    this.activeFilters.useFiltersForRequests(true);
    this.fetchDataFromBackEnd(null, true);
    this.accordionRechercher.close();
  }

  onExporterClick() {
    this.exportLoading = true;
    this.toastrHelper.showToast(
      NbToastStatus.SUCCESS,
      'EXPORT EN COURS...',
      'Votre fichier d\'export est en cours de génération. Il sera téléchargé automatiquement une fois prêt. En attendant, vous pouvez continuer à utiliser l\'application SANS RAFRAICHIR LA PAGE.',
      20000
    );
    const event = { first: 0, rows: this.totalRecords };
    const requestFilters: ParamValue[] = this.activeFilters.generateRequestFilters(event, this.archiveTable);
    this.archiveService.getArchives(false, requestFilters, 3600).subscribe(data => {
      ExportExcelHelper.export(this.columnHelper.convertObjectsArrayToRawObjectsJsonData(data.result), 'ExportArchives', 'ARCHIVES');
      this.exportLoading = false;
    });
  }

  onInputKeyPress($event) {
    if ($event.keyCode === 13) {
      this.onChercherClick();
    }
  }

  emprunter() {
    console.log("olala j'emprunte l'archive");
  }

  archiveEmprunte(archiveDossier: ArchiveDossier) {
    if (archiveDossier.emprunteur && archiveDossier.emprunteur.idUtilisateur) {
      return true;
    }else {
      return false;
    }
  }

  droitRestitution(archiveDossier: ArchiveDossier) {
    if (archiveDossier.emprunteur.idUtilisateur === this.utilisateur.idUtilisateur) {
      return true;
    }else {
      return false;
    }
  }

  onRowSelect(event) {
    this.localStorageService.setItem('archive_id', event.data.id);
    this.router.navigate([FrontUrlConstant.PATH_TO_FICHE_ARCHIVE]);
  }

  getClassEmprunt(archiveDossier: ArchiveDossier) {

    if (archiveDossier.emprunteur) {
      if (archiveDossier.emprunteur.idUtilisateur === this.utilisateur.idUtilisateur) {
        return 'emprunt-utilisateur-connecte';
      }else {
        return 'emprunt-utilisateur';
      }
    }else {
      return 'emprunt-utilisateur-libre';
    }
  }
}
