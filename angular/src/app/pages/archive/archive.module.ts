import { NgModule } from '@angular/core';

import { ArchiveService } from '../../service/archive.service';
import { ColumnService } from '../../service/column.service';
import { FilterService } from '../../service/filter.service';
import { GeneralService } from '../../service/general.service';
import { ParamService } from '../../service/param.service';
import { UtilisateurService } from '../../service/utilisateur.service';
import { PageDefinitionModule } from '../ui-features/page-definition/page-definition.module';
import { ArchiveComponent } from './archive.component';
import { FicheArchiveComponent } from './fiche/ficheArchive.component';
import { MultiSelectModule } from 'primeng/multiselect';

@NgModule({
  imports: [
    PageDefinitionModule,
    MultiSelectModule
  ],
  declarations: [
    ArchiveComponent,
    FicheArchiveComponent
  ],
  providers: [
    UtilisateurService,
    ArchiveService,
    FilterService,
    ColumnService,
    GeneralService,
    ParamService,
  ]
})
export class ArchiveModule { }
