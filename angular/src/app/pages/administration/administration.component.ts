import { Proprietaire } from './../../model/proprietaire';
import { FR_LOCALE } from './../../constant/CalendarFrLocale';
import { Vehicule } from './../../model/vehicule';
import { ArchiveBoite } from './../../model/archiveBoite';
import { ColumnHelper } from './../../model/columnHelper';
import { ExportExcelHelper } from './../../model/exportExcelHelper';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { BackUrlConstant } from './../../constant/BackUrlConstant';
import { DateDto } from './../../model/dateDto';
import { PlanningUtilisateurMateriel } from './../../model/planningUtilisateurMateriel';
import { Materiel } from './../../model/materiel';
import { FrontUrlConstant } from './../../constant/FrontUrlConstant';
import { GeometrieType } from './../../model/geometrieType';
import { Type } from './../../model/type';
import { Categorie } from './../../model/categorie';
import { GeneralService } from './../../service/general.service';
import { Geometrie } from './../../model/geometrie';
import { Table } from 'primeng/table';
import { ParamValue } from './../../model/paramValue';
import { MenuItem } from 'primeng/components/common/menuitem';
import { ToastrHelper } from './../../model/toastrHelper';
import { NbDialogService, NbToastrService, NbAccordionItemComponent } from '@nebular/theme';
import { FilterService } from './../../service/filter.service';
import { Ligne } from '../../model/ligne';
import { UniteOperationnelle } from '../../model/uniteOperationnelle';
import { Infrapole } from './../../model/infrapole';
import { Region } from './../../model/region';
import { Utilisateur } from '../../model/utilisateur';
import { UtilisateurService } from '../../service/utilisateur.service';
import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { Component, ViewChild, TemplateRef, ViewChildren, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import { Secteur } from '../../model/secteur';
import { Portion } from '../../model/portion';
import { AutoComplete } from 'primeng/autocomplete';
import { CrudHelper } from '../../model/crudHelper';
import { Router } from '@angular/router';
import { ReservationMaterielUtilisateur } from '../../model/reservationMaterielUtilisateur';
import { Calendar } from 'primeng/calendar';
import { ReservationVehiculeUtilisateur } from '../../model/reservationVehiculeUtilisateur';
import { PlanningUtilisateurVehicule } from '../../model/planningUtilisateurVehicule';

@Component({
  selector: 'administration',
  styleUrls: ['./administration.component.scss'],
  templateUrl: './administration.component.html',
})
export class AdministrationComponent implements OnInit, AfterViewInit {

  utilisateur: Utilisateur;

  isLigneLoaded: boolean = false;
  @ViewChild('lignesTable') lignesTable: Table;

  isGeometrieLoaded: boolean = false;
  @ViewChild('geometriesTable') geometriesTable: Table;

  isProprietaireLoaded: boolean = false;
  @ViewChild('proprietairesTable') proprietairesTable: Table;

  isRegionLoaded: boolean = false;
  selectedRegion: Region;
  selectedInfrapole: Infrapole;
  selectedSecteur: Secteur;
  selectedUnitesOperationnelle: UniteOperationnelle;
  selectedPortion: Portion;

  isCategorieLoaded: boolean = false;
  selectedCategorie: Categorie;
  selectedType: Type;
  selectedGeometrieType: GeometrieType;

  autocompleteSuggestions: any[];
  @ViewChildren('autocomplete') autocompletes: any;

  activeDialogRef: any;

  toastrHelper: ToastrHelper;

  breadCrumb: MenuItem[];
  breadCrumb2: MenuItem[];

  crudHelper: CrudHelper;
  @ViewChild('templateForEdit') cruPopupTemplate: TemplateRef<any>;
  @ViewChild('templateForDelete') deletePopupTemplate: TemplateRef<any>;

  isUtilisateurLoaded: boolean = false;
  isArchiveBoiteLoaded: boolean = false;

  exportUserLoading: boolean;

  fr: any = FR_LOCALE;

  constructor(private utilisateurService: UtilisateurService,
              private filterService: FilterService,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private generalService: GeneralService,
              private router: Router) {
  }

  ngOnInit() {
    const today = new Date();
    this.utilisateur = this.utilisateurService.getUtilisateurConnected();
    const rows = this.utilisateur.nbLignesTableau ? parseInt(this.utilisateur.nbLignesTableau, 10) : 10;
    const crudObjectDefinitions: any = {
      Ligne: { object: new Ligne({}), table: null },
      Region: { object: new Region({}), table: null },
      Infrapole: { object: new Infrapole({}), table: null },
      Secteur: { object: new Secteur({}), table: null },
      UniteOperationnelle: { object: new UniteOperationnelle({}), table: null },
      Portion: { object: new Portion({}), table: null },
      Geometrie: { object: new Geometrie({}), table: null },
      Categorie: { object: new Categorie({}), table: null },
      Type: { object: new Type({}), table: null },
      GeometrieType: { object: new GeometrieType({}), table: null },
      Utilisateur: { object: new Utilisateur({}), table: null },
      Materiel: { object: new Materiel({}), table: null },
      ArchiveBoite: { object: new ArchiveBoite({}), table: null },
      Vehicule: { object: new Vehicule({}), table: null },
      Proprietaire: { object: new Proprietaire({}), table: null },
    };
    this.toastrHelper = new ToastrHelper(this.toastrService);
    this.crudHelper = new CrudHelper(this.generalService, this.dialogService, this.toastrHelper, crudObjectDefinitions, this.cruPopupTemplate, this.deletePopupTemplate, rows);
  }

  ngAfterViewInit() {
    this.bindTablesToCrudHelper();
    // Deactivate minutes selection in planning calendars
  }

  bindTablesToCrudHelper() {
    // Pour les *ngFor, on pourra utiliser ça pour setter les tables dans le CrudHelper :
    // @ViewChildren('crudHelperTable') components:QueryList<CustomComponent>;
    // https://stackoverflow.com/questions/44440879/dynamic-template-reference-variable-inside-ngfor-angular-2#answer-54351768
    this.crudHelper.crudObjectDefinitions['Ligne'].table = this.lignesTable;
    this.crudHelper.crudObjectDefinitions['Geometrie'].table = this.geometriesTable;
    this.crudHelper.crudObjectDefinitions['Proprietaire'].table = this.proprietairesTable;
  }

  initialLoad(item: string) {
    if (item === 'Ligne') {
      if (!this.isLigneLoaded) {
        this.isLigneLoaded = true;
        this.crudHelper.lazyLoad( { first: 0, rows: 10 }, 'Ligne', this.isLigneLoaded);
      }
    } else if (item === 'Region') {
      if (!this.isRegionLoaded) {
        this.getRegions();
        this.fillBreadCrumb();
        this.isRegionLoaded = true;
      }
    } else if (item === 'Geometrie') {
      if (!this.isGeometrieLoaded) {
        this.isGeometrieLoaded = true;
        this.crudHelper.lazyLoad( { first: 0, rows: 10 }, 'Geometrie', this.isGeometrieLoaded);
      }
    } else if (item === 'Categorie') {
      if (!this.isCategorieLoaded) {
        this.getCategories();
        this.fillBreadCrumb2();
        this.isCategorieLoaded = true;
      }
    } else if (item === 'Utilisateur') {
      if (!this.isUtilisateurLoaded) {
        this.crudHelper.load('Utilisateur');
        this.isUtilisateurLoaded = true;
      }
    } else if (item === 'ArchiveBoite') {
      if (!this.isArchiveBoiteLoaded) {
        this.crudHelper.load('ArchiveBoite');
        this.isArchiveBoiteLoaded = true;
      }
    } else if (item === 'Proprietaire') {
      if (!this.isProprietaireLoaded) {
        this.crudHelper.load('Proprietaire');
        this.isProprietaireLoaded = true;
      }
    }
  }

  resolveFieldData(rowData, colField){
    return ObjectUtils.resolveFieldData(rowData, colField);
  }

  getRegions() {
    this.unselectRegions();
    this.crudHelper.load("Region");
  }

  getInfrapoles() {
    this.fillBreadCrumb();
    this.unselectInfrapoles();
    this.crudHelper.load("Infrapole", this.selectedRegion.id);
  }

  getSecteursOrUOs() {
    this.unselectSecteurs();
    if (this.selectedInfrapole.usesSecteurSubLevel) {
      this.getSecteurs();
    } else {
      this.getUOs();
      this.crudHelper.crudObjectsLists['Secteur'] = [ new Secteur({}) ];
    }
  }

  getSecteurs(infrapole?: Infrapole) {
    this.fillBreadCrumb();
    let infrapoleForFunction = infrapole ? infrapole : this.selectedInfrapole;
    if (infrapoleForFunction) {
      if (infrapoleForFunction.usesSecteurSubLevel && infrapoleForFunction.id) {
        this.crudHelper.load("Secteur", infrapoleForFunction.id);
      } else {
        this.crudHelper.crudObjectsLists['Secteur'] = [ new Secteur({}) ];
      }
    }
  }

  getUOs() {
    this.fillBreadCrumb();
    if (this.selectedSecteur || this.selectedInfrapole) {
      this.crudHelper.load("UniteOperationnelle", this.selectedSecteur ? this.selectedSecteur.id : undefined, this.selectedInfrapole ? this.selectedInfrapole.id : undefined);
    }
  }

  getPortions() {
    this.fillBreadCrumb();
    this.unselectPortions();
    if (this.selectedUnitesOperationnelle) {
      this.crudHelper.load("Portion", this.selectedUnitesOperationnelle.id);
    }
  }

  unselectRegions($event ?) {
    this.selectedRegion = undefined;
    this.crudHelper.crudObjectsLists['Region'] = [];
    this.unselectInfrapoles();
    this.fillBreadCrumb();
  }

  unselectInfrapoles($event ?) {
    this.selectedInfrapole = undefined;
    this.crudHelper.crudObjectsLists['Infrapole'] = [];
    this.unselectSecteurs();
    this.fillBreadCrumb();
  }

  unselectSecteurs($event ?) {
    this.selectedSecteur = undefined;
    this.crudHelper.crudObjectsLists['Secteur'] = [];
    this.unselectUnitesOperationnelle();
    this.fillBreadCrumb();
  }

  unselectUnitesOperationnelle($event ?) {
    this.selectedUnitesOperationnelle = undefined;
    this.crudHelper.crudObjectsLists['UniteOperationnelle'] = [];
    this.unselectPortions();
    this.fillBreadCrumb();
  }

  unselectPortions($event ?) {
    this.selectedPortion = undefined;
    this.crudHelper.crudObjectsLists['Portion'] = [];
    this.fillBreadCrumb();
  }

  populatePortionsForLigne(ligne: Ligne, data: any) {
    if (data) {
      let portions: Portion[] = [];
      let remaining = data.length;
      for (let portionDTO of data) {
        let portion: Portion = new Portion(portionDTO);
        if (remaining <= 1) {
          portion["LAST_ELEMENT"] = true; // used for display only. Last slider is different.
        }
        portions.push(portion);
        remaining--;
      }
      ligne["portions"] = portions;
    }
  }

  fillBreadCrumb(): void {
    this.breadCrumb = [];
    if (this.selectedRegion) {
      this.breadCrumb[0] = { label: this.selectedRegion.name };
      if (this.selectedInfrapole) {
        this.breadCrumb[1] = { label: this.selectedInfrapole.name };
        if (this.selectedInfrapole.usesSecteurSubLevel) {
          if (this.selectedSecteur) {
            this.breadCrumb[2] = { label: this.selectedSecteur.nom };
            if (this.selectedUnitesOperationnelle) {
              this.breadCrumb[3] = { label: this.selectedUnitesOperationnelle.name };
              if (this.selectedPortion) {
                this.breadCrumb[4] = { label: this.selectedPortion.ligne.nom + " - [" + this.selectedPortion.pkDebut + " - " + this.selectedPortion.pkFin + "]" };
              } else {
                this.breadCrumb[4] = { label: "Sélectionner une Portion" };
              }
            } else {
              this.breadCrumb[3] = { label: "Sélectionner une Unité Opérationnelle" };
            }
          } else {
            this.breadCrumb[2] = {label:'Sélectionner un Secteur'};
          }
        } else {
          if (this.selectedUnitesOperationnelle) {
            this.breadCrumb[2] = { label: this.selectedUnitesOperationnelle.name };
            if (this.selectedPortion) {
              this.breadCrumb[3] = { label: this.selectedPortion.ligne.nom + " - [" + this.selectedPortion.pkDebut + " - " + this.selectedPortion.pkFin + "]" };
            } else {
              this.breadCrumb[3] = { label: "Sélectionner une Portion" };
            }
          } else {
            this.breadCrumb[2] = {label:'Sélectionner une Unité Opérationnelle'};
          }
        }
      } else {
        this.breadCrumb[1] = {label:'Sélectionner un Infrapole'};
      }
    } else {
      this.breadCrumb[0] = {label:'Sélectionner une Région'};
    }
  }

  getCategories() {
    this.unselectCategorie();
    this.crudHelper.load("Categorie");
  }

  getTypes() {
    this.unselectType();
    this.crudHelper.load("Type", this.selectedCategorie.id);
  }

  getGeometriesType() {
    this.unselectGeometrieType();
    this.crudHelper.load("GeometrieType", this.selectedType.id);
  }

  unselectCategorie($event ?) {
    this.selectedCategorie = undefined;
    this.crudHelper.crudObjectsLists['Categorie'] = [];
    this.unselectType();
    this.fillBreadCrumb2();
  }

  unselectType($event ?) {
    this.selectedType = undefined;
    this.crudHelper.crudObjectsLists['Type'] = [];
    this.unselectGeometrieType();
    this.fillBreadCrumb2();
  }

  unselectGeometrieType($event ?) {
    this.selectedGeometrieType = undefined;
    this.crudHelper.crudObjectsLists['GeometrieType'] = [];
    this.fillBreadCrumb2();
  }

  fillBreadCrumb2() {
    this.breadCrumb2 = [];
    if (this.selectedCategorie) {
      this.breadCrumb2[0] = { label: this.selectedCategorie.libelle };
      if (this.selectedType) {
        this.breadCrumb2[1] = { label: this.selectedType.libelle };
        if (this.selectedGeometrieType) {
          this.breadCrumb2[2] = { label: this.selectedGeometrieType.geometrie.libelle };
        } else {
          this.breadCrumb2[2] = {label:'Sélectionner une Association Type-Géométrie'};
        }
      } else {
        this.breadCrumb2[1] = {label:'Sélectionner un Type'};
      }
    } else {
      this.breadCrumb2[0] = {label:'Sélectionner une Catégorie'};
    }
  }

  openCreateDialog(className: string) {
    this.bindTablesToCrudHelper();
    this.crudHelper.openCreateDialog(className);
    this.prepareObjectBasedOnClassName(this.crudHelper.editedObject);
  }

  openEditDialog(object: any) {
    this.prepareObjectBasedOnClassName(object);
    this.crudHelper.openEditDialog(object);
  }

  openDeleteDialog(object: any) {
    this.bindTablesToCrudHelper();
    this.prepareObjectBasedOnClassName(object);
    this.crudHelper.openDeleteDialog(object);
  }

  openLignePortionsDialog(object: any, template: TemplateRef<any>) {
    this.filterService.getLignePortions(object).subscribe(data => {
      this.populatePortionsForLigne(object, data);
      this.crudHelper.openEditDialog(object, template);
    });
  }

  openUserProfilePage(id: string) {
    this.router.navigate([FrontUrlConstant.PATH_TO_PROFIL], { queryParams: { id } });
  }

  exporterUtilisateur(): void {
    this.exportUserLoading = true;
    const cols: any[] = [
      { header: 'Login', field: 'login'},
      { header: 'Nom', field: 'nom'},
      { header: 'Prénom', field: 'prenom'},
      { header: 'Fonction', field: 'fonction'},
      { header: 'Entité', field: 'entite'},
      { header: 'Tél. Fixe', field: 'telephoneFixe'},
      { header: 'Tél. Mobile', field: 'telephoneMobile'},
      { header: 'Fax', field: 'fax'},
      { header: 'Email', field: 'email'},
      { header: 'Unité Opérationnelle', field: 'up.name'}
    ];
    const jsonUsers = ColumnHelper.convertObjectsArrayToRawObjectsJsonData(this.crudHelper.crudObjectsLists['Utilisateur'], cols);
    ExportExcelHelper.export(jsonUsers, 'ExportUtilisateurs', 'UTILISATEURS');
    this.exportUserLoading = false;
  }

  prepareObjectBasedOnClassName(object: any): void {
    if (object.className == 'Infrapole') {
      object.region = this.selectedRegion;
      object.getSecteurStatus();
		} else if (object.className == 'Secteur') {
      object.infrapole = this.selectedInfrapole;
		} else if (object.className == 'UniteOperationnelle') {
      if (this.selectedSecteur) {
        object.secteur = this.selectedSecteur;
      } else {
        object.infrapole = this.selectedInfrapole;
      }
		} else if (object.className == 'Portion') {
      object.uo = this.selectedUnitesOperationnelle;
    } else if (object.className == 'Type') {
      object.categorie = this.selectedCategorie;
		} else if (object.className == 'GeometrieType') {
      object.type = this.selectedType;
		}
  }

  canEditedObjectBeSaved(): boolean {
    if (this.crudHelper.editedObject.className == 'Infrapole') {
      return this.crudHelper.canEditedObjectBeSaved() && this.crudHelper.editedObject.usesSecteurSubLevel == this.crudHelper.editedObject._buffer_usesSecteurSubLevel;
    }
    return this.crudHelper.canEditedObjectBeSaved()
  }

  autocompleteEdit($event: any, className: string): void {
    this.autocompleteSuggestions = [];
    if (className == "Portion") {
      if ($event && $event.query && $event.query.length > 2) {
        this.filterService.getLignesSuggested($event.query).subscribe(data => {
          let autocompleteSuggestions = [];
          for (let ligneDTO of data) {
            let ligne: Ligne = new Ligne(ligneDTO);
            autocompleteSuggestions.push( ligne );
          }
          this.autocompleteSuggestions = autocompleteSuggestions;
          this.autocompleteShow();
        });
      }
    }
    if (className == "GeometrieType") {
      if ($event && $event.query && $event.query.length > 2) {
        this.filterService.getGeometriesSuggested($event.query).subscribe(data => {
          let autocompleteSuggestions = [];
          for (let geometrieDTO of data) {
            let geometrie: Geometrie = new Geometrie(geometrieDTO);
            autocompleteSuggestions.push( geometrie );
          }
          this.autocompleteSuggestions = autocompleteSuggestions;
          this.autocompleteShow();
        });
      }
    }
  }

  autocompleteShow(): void {
    for (let result of this.autocompletes._results) {
      result.show();
    }
  }
}
