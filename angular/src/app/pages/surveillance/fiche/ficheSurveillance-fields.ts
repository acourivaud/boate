import { MENU_ITEMS } from './ficheSurveillance-menu';
export const FIELDS =  {
  OPTIONS: {
    RegionOptions: null,
    InfrapoleOptions: null,
    SecteurOptions: null,
    UoOptions: null,
    UicOptions: MENU_ITEMS.UIC,
    LigneOptions: null,
  },
  FILTERS: {
    // ATTENTION LES SUFFIXES ONT UN IMPACT SUR LE PROGRAMME
    // (Selected, SelectedSearch, Typed, TypedSearch)
    RegionSelected: null,
    InfrapoleSelected: null,
    SecteurSelected: null,
    UoSelected: null,
    UicSelected: null,
    LigneSelected: null,
  },
};
