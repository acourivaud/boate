import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { ArchiveDossier } from './../../../model/archiveDossier';
import { Bouclage } from './../../../model/bouclage';
import { BackUrlConstant } from './../../../constant/BackUrlConstant';
import { GeneralService } from './../../../service/general.service';
import { MENU_ITEMS } from './ficheSurveillance-menu';
import { Location } from '@angular/common';
import { FrontUrlConstant } from './../../../constant/FrontUrlConstant';
import { Router } from '@angular/router';
import { Surveillance } from './../../../model/surveilllance';
import { SurveillanceService } from './../../../service/surveillance.service';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

import { ToastrHelper } from '../../../model/toastrHelper';
import { LocalStorageService } from '../../../service/localStorage.service';
import { UtilisateurService } from '../../../service/utilisateur.service';
import { Utilisateur } from '../../../model/utilisateur';
import { OnInit, Component, AfterViewInit } from '@angular/core';

@Component({
  selector: 'ngx-fiche-surveillance',
  styleUrls: ['./ficheSurveillance.component.scss'],
  templateUrl: './ficheSurveillance.component.html',
})
export class FicheSurveillanceComponent implements OnInit, AfterViewInit {

  utilisateur: Utilisateur;

  surveillanceId: number;
  currentSurveillance: Surveillance;

  toastrHelper: ToastrHelper;

  selectOptions: any = MENU_ITEMS;
  typesVisiteOptions: any[] = MENU_ITEMS.VISIT_TYPE;

  aoaps: Utilisateur[] = [];

  historiquesDesVisites: Bouclage[] = [];
  historiquesDesSurveillances: Surveillance[] = [];
  historiquesDesArchives: ArchiveDossier[] = [];

  historiquesDesVisitesCols = [
    { field: 'surveillance.ouvrage.line.code', header: 'Ligne', style: 'column-center column-width-100' },
    { field: 'surveillance.ouvrage.pk', header: 'Pk', style: 'column-center column-width-100' },
    { field: 'surveillance.ouvrage.middlePk', header: 'Pk milieu', style: 'column-center column-width-100' },
    { field: 'surveillance.ouvrage.endPk', header: 'Pk fin', style: 'column-center column-width-100' },
    { field: 'surveillance.ouvrage.indice', header: 'Indice', style: 'column-center column-width-100' },
    { field: 'surveillance.ouvrage.categorie', header: 'Catégorie', style: 'column-center column-width-100' },
    { field: 'typeKey', header: 'Type', style: 'column-center column-width-100' },
    { field: 'surveillance.typeSurveillance', header: 'Type visite', style: 'column-center column-width-100' },
    { field: 'previsionDate', header: 'Date prévue', style: 'column-center column-width-100' },
    { field: 'typeKey', header: 'Visite', style: 'column-center column-width-100' },
    { field: 'etatOuvrage', header: 'Etat ouvrage', style: 'column-center column-width-100' },
    { field: 'laborTodoAgent0', header: 'Proposition U0', style: 'column-center column-width-100' },
    { field: 'laborCostAgent0', header: 'Montant U0', style: 'column-center column-width-100' },
    { field: 'laborTodoAgent1', header: 'Proposition U1', style: 'column-center column-width-100' },
    { field: 'laborCostAgent1', header: 'Montant U1', style: 'column-center column-width-100' },
    { field: 'laborTodoAgent2', header: 'Proposition U2', style: 'column-center column-width-100' },
    { field: 'laborCostAgent2', header: 'Montant U2', style: 'column-center column-width-100' },
  ];

  historiquesDesArchivesCols = [
    { field: 'ouvrage.line.code', header: 'Ligne', style: 'column-center column-width-100' },
    { field: 'ouvrage.pk', header: 'Pk', style: 'column-center column-width-100' },
    { field: 'ouvrage.middlePk', header: 'Pk milieu', style: 'column-center column-width-100' },
    { field: 'ouvrage.endPk', header: 'Pk fin', style: 'column-center column-width-100' },
    { field: 'ouvrage.categorie', header: 'Catégorie', style: 'column-center column-width-100' },
    { field: 'description', header: 'Description', style: 'column-center column-width-100' },
    { field: 'commentaires', header: 'Observation', style: 'column-center column-width-100' },
    { field: 'numeroDossier', header: 'N° Boite', style: 'column-center column-width-100' },
    { field: 'numeroDossier', header: 'Localisation', style: 'column-center column-width-100' },
    { field: 'typeArchive', header: 'Fiche Archive', style: 'column-center column-width-100' },
  ];

  constructor(
    private surveillanceService: SurveillanceService,
    private toastrService: NbToastrService,
    private localStorageService: LocalStorageService,
    private dialogService: NbDialogService,
    private generalService: GeneralService,
    private router: Router,
    private utilisateurService: UtilisateurService,
    private location: Location
  ) {}

  ngOnInit() {
    this.surveillanceId = this.localStorageService.getItem('surveillance_id') ? Number.parseInt(this.localStorageService.getItem('surveillance_id')) : null;
    if (this.surveillanceId) { // Surveillance existante
      this.initSurveillanceDescription();
    } else {

    }
    this.toastrHelper = new ToastrHelper(this.toastrService);
    this.utilisateurService.getAoaps().subscribe(data => {
      this.aoaps = [];
      for (const d of data) {
        const aoap: Utilisateur = new Utilisateur(d);
        this.aoaps.push(aoap);
      }
    });
  }

  ngAfterViewInit() {

  }

  initSurveillanceDescription() {
    this.surveillanceService.getSurveillance(this.surveillanceId).subscribe(data => {
      this.currentSurveillance = new Surveillance(data);
      this.setTypeVisiteOptions();
      this.getHistoriques();
      this.localStorageService.setItem('currentSurveillance', JSON.stringify(this.currentSurveillance));
      this.getUtilisateurConnected();
    });
  }

  setTypeVisiteOptions() {
    if (this.currentSurveillance !== undefined) {
      if (this.currentSurveillance.ouvrage.categorie === 'OT') {
        this.typesVisiteOptions = MENU_ITEMS.VISIT_TYPE_OT;
      } else {
        this.typesVisiteOptions = MENU_ITEMS.VISIT_TYPE;
      }
    }
  }

  getHistoriques() {
    this.generalService.getUrlWithIdParam(BackUrlConstant.BOUCLAGE_OUVRAGE, this.currentSurveillance.ouvrage.id.toString()).subscribe(data => {
      this.historiquesDesVisites = [];
      for (const bouclage of data) {
        this.historiquesDesVisites.push(new Bouclage(bouclage));
      }
    });
    this.generalService.getUrlWithIdParam(BackUrlConstant.SURVEILLANCE_OUVRAGE, this.currentSurveillance.ouvrage.id.toString()).subscribe(data => {
      this.historiquesDesSurveillances = [];
      for (const surveillance of data) {
        this.historiquesDesSurveillances.push(new Surveillance(surveillance));
      }
    });
    this.generalService.getUrlWithIdParam(BackUrlConstant.ARCHIVE_OUVRAGE, this.currentSurveillance.ouvrage.id.toString()).subscribe(data => {
      this.historiquesDesArchives = [];
      for (const archiveDossier of data) {
        this.historiquesDesArchives.push(new ArchiveDossier(archiveDossier, this.utilisateurService.getUtilisateurConnected()));
      }
    });
  }

  enregistrerSurveillance() {
    this.surveillanceService.saveSurveillance(this.currentSurveillance).subscribe(data => {
      this.toastrHelper.showDefaultSuccessToast();
      this.initSurveillanceDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  archiverSurveillance() {
    this.surveillanceService.archiveSurveillance(this.currentSurveillance).subscribe(data => {
      this.toastrHelper.showToast(NbToastStatus.SUCCESS, 'Modifications enregistrées', data.message);
      this.initSurveillanceDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  recupererSurveillance() {
    this.currentSurveillance.archive = false;
    this.surveillanceService.saveSurveillance(this.currentSurveillance).subscribe(data => {
      this.toastrHelper.showToast(NbToastStatus.SUCCESS, 'Modifications enregistrées', 'La surveillance a été récupérée avec succès');
      this.initSurveillanceDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  getUtilisateurConnected() {
    this.utilisateurService.getUtilisateurConnectedFromBack().subscribe(data => {
      this.utilisateur = new Utilisateur(data);
    });
  }

  switchToOuvrage() {
    this.localStorageService.setItem('ouvrage_id', this.currentSurveillance.ouvrage.id.toString());
    this.router.navigate([FrontUrlConstant.PATH_TO_FICHE_OUVRAGE]);
  }

  backClicked() {
    this.location.back();
  }

  moyensChange() {
    if (this.currentSurveillance !== undefined) {
      if (this.currentSurveillance.firstMeansKey === undefined || this.currentSurveillance.firstMeansKey === null) this.currentSurveillance.secondMeansKey = null;
      if (this.currentSurveillance.secondMeansKey === undefined || this.currentSurveillance.secondMeansKey === null) this.currentSurveillance.thirdMeansKey = null;
    }
  }

  moyensDisabled(): boolean[] {
    if (this.currentSurveillance === undefined) return [ true, true ];
    const moyensDisabled: boolean[] = [ false, false ];
    if (this.currentSurveillance.secondMeansKey === undefined || this.currentSurveillance.secondMeansKey === null) moyensDisabled[1] = true;
    if (this.currentSurveillance.firstMeansKey === undefined || this.currentSurveillance.firstMeansKey === null) moyensDisabled[0] = true;
    return moyensDisabled;
  }

  resolveFieldData(rowData, colField){
    return ObjectUtils.resolveFieldData(rowData, colField);
  }
}
