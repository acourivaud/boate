export const MENU_ITEMS =  {
  WORK_SURVEILLANCE_MEANS: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'means_4axes',
      fr: '4 axes',
    },
    {
      raw: 'means_acrobate',
      fr: 'Acrobate',
    },
    {
      raw: 'means_Brg',
      fr: 'Barge',
    },
    {
      raw: 'means_Brq',
      fr: 'Barque',
    },
    {
      raw: 'means_divers',
      fr: 'Divers',
    },
    {
      raw: 'means_drone',
      fr: 'Drone',
    },
    {
      raw: 'means_Ech',
      fr: 'Echelle',
    },
    {
      raw: 'means_Elise',
      fr: 'Elise',
    },
    {
      raw: 'means_endoscope',
      fr: 'Endoscope',
    },
    {
      raw: 'means_Mg1',
      fr: 'Moog 1',
    },
    {
      raw: 'means_Mg2',
      fr: 'Moog 2',
    },
    {
      raw: 'means_nacelle',
      fr: 'Nacelle routière',
    },
    {
      raw: 'means_PF245',
      fr: 'PF 2, 4, 5',
    },
    {
      raw: 'means_PF2',
      fr: 'PF 2',
    },
    {
      raw: 'means_PF3',
      fr: 'PF 3',
    },
    {
      raw: 'means_PF4',
      fr: 'PF 4',
    },
    {
      raw: 'means_PF5',
      fr: 'PF 5',
    },
    {
      raw: 'means_PF6',
      fr: 'PF 6',
    },
    {
      raw: 'means_PF7',
      fr: 'PF 7',
    },
    {
      raw: 'means_plongeur',
      fr: 'Plongeurs',
    },
    {
      raw: 'means_Robot',
      fr: 'Robot vidéo',
    },
    {
      raw: 'means_signal',
      fr: 'Signalisation',
    },
    {
      raw: 'means_S65/3',
      fr: 'S65/3',
    },
    {
      raw: 'means_S65/4',
      fr: 'S65/4',
    },
    {
      raw: 'means_topo',
      fr: 'Nivellement',
    },
    {
      raw: 'means_U26',
      fr: 'U26',
    },
    {
      raw: 'means_U32',
      fr: 'U32',
    },
    {
      raw: 'means_vision',
      fr: 'Vision',
    },
    {
      raw: 'means_WIT',
      fr: 'WIT',
    },
  ],
  FSA_KEY: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'fsa_oui',
      fr: 'Oui',
    },
    {
      raw: 'fsa_special_exposed',
      fr: 'Particulierement exposé',
    },
    {
      raw: 'fsa_none',
      fr: 'Non',
    },
  ],
  VISIT_TYPE: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'ID',
      fr: 'ID',
    },
    {
      raw: 'VD',
      fr: 'VD',
    },
    {
      raw: 'VS',
      fr: 'VS',
    },
    {
      raw: 'surv_tiers',
      fr: 'Tiers',
    },
    {
      raw: 'surv_current',
      fr: 'Surveillance courante',
    },
  ],
  VISIT_TYPE_OT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'VD_OT',
      fr: 'VD OT',
    },
    {
      raw: 'VP_OT',
      fr: 'VP OT',
    },
    {
      raw: 'VA_OT',
      fr: 'VA OT',
    },
    {
      raw: 'surv_current',
      fr: 'Surveillance courante',
    },
  ],
  WORK_SURVEILLANCE_HEAD: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'sur_head_abe',
      fr: 'ABE',
    },
    {
      raw: 'sur_head_ah',
      fr: 'AH',
    },
    {
      raw: 'sur_head_aoap',
      fr: 'AOAP',
    },
    {
      raw: 'sur_head_aoau',
      fr: 'AOAU',
    },
    {
      raw: 'sur_head_cot',
      fr: 'COT',
    },
    {
      raw: 'sur_head_soar',
      fr: 'SOAR',
    },
    {
      raw: 'sur_head_tiers',
      fr: 'Tiers',
    },
    {
      raw: 'sur_head_topo',
      fr: 'Topographe',
    },
  ],
  UIC: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'uic_3',
      fr: '3',
    },
    {
      raw: 'uic_4',
      fr: '4',
    },
    {
      raw: 'uic_5',
      fr: '5',
    },
    {
      raw: 'uic_6',
      fr: '6',
    },
    {
      raw: 'uic_6AV',
      fr: '6AV',
    },
    {
      raw: 'uic_7AV',
      fr: '7AV',
    },
    {
      raw: 'uic_7SV',
      fr: '7SV',
    },
    {
      raw: 'uic_8AV',
      fr: '8AV',
    },
    {
      raw: 'uic_8SV',
      fr: '8SV',
    },
    {
      raw: 'uic_9AV',
      fr: '9AV',
    },
    {
      raw: 'uic_9SV',
      fr: '9SV',
    },
    {
      raw: 'uic_emb',
      fr: 'Emb',
    },
    {
      raw: 'uic_F',
      fr: 'F',
    },
    {
      raw: 'uic_neut',
      fr: 'Neut',
    },
    {
      raw: 'uic_none',
      fr: 'Sans',
    },
  ],
};
