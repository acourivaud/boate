import { ParamService } from './../../service/param.service';
import { GeneralService } from './../../service/general.service';
import { ProprietaireService } from './../../service/proprietaire.service';
import { OuvrageService } from './../../service/ouvrage.service';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CalendarModule } from 'primeng/calendar';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { FicheSurveillanceComponent } from './fiche/ficheSurveillance.component';
import { ColumnService } from './../../service/column.service';
import { UtilisateurService } from './../../service/utilisateur.service';
import { FilterService } from '../../service/filter.service';
import { TableModule } from 'primeng/table';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { SurveillanceService } from '../../service/surveillance.service';
import { NgModule } from '@angular/core';
import { KeyFilterModule } from 'primeng/keyfilter';
import { MultiSelectModule } from 'primeng/multiselect';
import { TooltipModule } from 'primeng/tooltip';

import { ThemeModule } from '../../@theme/theme.module';
import { SurveillanceComponent } from './surveillance.component';

@NgModule({
  imports: [
    ThemeModule,
    TableModule,
    AutoCompleteModule,
    KeyFilterModule,
    MultiSelectModule,
    TooltipModule,
    ToggleButtonModule,
    CalendarModule,
    InputTextareaModule
  ],
  declarations: [
    SurveillanceComponent,
    FicheSurveillanceComponent
  ],
  providers: [
    UtilisateurService,
    SurveillanceService,
    FilterService,
    ColumnService,
    OuvrageService,
    ProprietaireService,
    GeneralService,
    ParamService,
  ]
})
export class SurveillanceModule { }
