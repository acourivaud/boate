import { FrontUrlConstant } from './../../constant/FrontUrlConstant';
import { LocalStorageService } from './../../service/localStorage.service';
import { Router } from '@angular/router';
import { ExportExcelHelper } from './../../model/exportExcelHelper';
import { ColumnService } from './../../service/column.service';
import { Utilisateur } from './../../model/utilisateur';
import { UtilisateurService } from './../../service/utilisateur.service';
import { ColumnHelper } from '../../model/columnHelper';
import { Region } from '../../model/region';
import { FilterService } from '../../service/filter.service';
import { FilterHelper } from '../../model/filterHelper';
import { MENU_ITEMS } from './surveillance-menu';
import { ParamValue } from '../../model/paramValue';
import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { SurveillanceService } from '../../service/surveillance.service';
import { Component, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import { Surveillance } from '../../model/surveilllance';
import { SelectItem } from 'primeng/components/common/selectitem';
import { NbToastrService } from '@nebular/theme';
import { ToastrHelper } from '../../model/toastrHelper';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Component({
  selector: 'surveillance',
  styleUrls: ['./surveillance.component.scss'],
  templateUrl: './surveillance.component.html',
})
export class SurveillanceComponent {

  utilisateur: Utilisateur;

  surveillances: Surveillance[];
  totalRecords: number;
  loading: boolean;
  exportLoading: boolean;
  inputFilter: RegExp = /^[0-9/./,]$/;

  selectedValue: Surveillance;

  @ViewChild('accordionRechercher') accordionRechercher;
  @ViewChild('accordionOA') accordionOA;
  @ViewChild('accordionOT') accordionOT;
  @ViewChild('accordionBNC') accordionBNC;
  @ViewChild('accordionAO') accordionAO;

  @ViewChild('surveillanceTable') surveillanceTable: Table;

  activeFilters: FilterHelper;
  columnHelper: ColumnHelper;

  pendingBackendResponses: number;

  toastrHelper: ToastrHelper;

  conditionalCols = [
    { conditions: null, col: { field: 'ouvrage.up.secteur.infrapole.code', header: 'Infrapole', style: 'column-center column-width-150' } },
    { conditions: null, col: { field: 'ouvrage.up.code', header: 'UP', style: 'column-center column-width-100' } },
    { conditions: null, col: { field: 'ouvrage.line.code', header: 'Ligne', style: 'column-center column-width-100' } },
    { conditions: null, col: { field: 'ouvrage.uic', header: 'UIC', style: 'column-center column-width-100' } },
    { conditions: null, col: { field: 'ouvrage.name', header: 'Nom', style: 'column-width-300' } },
    { conditions: null, col: { field: 'ouvrage.pk', header: 'PK', style: 'column-center column-width-100' } },
    { conditions: null, col: { field: 'ouvrage.middlePk', header: 'PK Milieu', style: 'column-center column-width-150' } },
    { conditions: null, col: { field: 'ouvrage.endPk', header: 'PK Fin', style: 'column-center column-width-100' } },
    { conditions: null, col: { field: 'compteAnalytique', header: 'Compte Analytique', style: 'column-center column-width-150' } },
    { conditions: null, col: { field: 'codeSegment', header: 'Segment de Gestion', style: 'column-center column-width-150' } },
    { conditions: null, col: { field: 'ouvrage.indice', header: 'Indice', style: 'column-center column-width-100' } },
    { conditions: null, col: { field: 'ouvrage.categorie', header: 'Catégorie', style: 'column-center column-width-150' } },
    { conditions: null, col: { field: 'ouvrage.typeOuvrage.nomBoate', header: 'Type', style: 'column-center column-width-200' } },
    { conditions: null, col: { field: 'ouvrage.nature', header: 'Nature', style: 'column-center column-width-200' } },
    { conditions: null, col: { field: 'ouvrage.situation', header: 'Situation', style: 'column-center column-width-150' } },
  ];

  constructor(private utilisateurService: UtilisateurService,
              private surveillanceService: SurveillanceService,
              private filterService: FilterService,
              private columnService: ColumnService,
              private localStorageService: LocalStorageService,
              private router: Router,
              private toastrService: NbToastrService) {

    this.utilisateur = utilisateurService.getUtilisateurConnected();

    const useFiltersForRequestsArg = this;
    const useFiltersForRequestsCallback = (arg: any): void => {

      if (arg.activeFilters.filters.GenCategorieSelected != null && arg.activeFilters.filters.GenCategorieSelected.raw === 'category_OT' ) {
        arg.columnHelper.setOptionalCols('optionalColsOT');
      } else {
        arg.columnHelper.setOptionalCols('optionalColsNull');
      }

      arg.columnHelper.applyColumnsOrder();

    };
    this.activeFilters = new FilterHelper(filterService, 'surveillance', useFiltersForRequestsCallback, useFiltersForRequestsArg);

    this.toastrHelper = new ToastrHelper(this.toastrService);
    
    if (this.utilisateur.nbLignesTableau) {
      this.activeFilters.rows = Number.parseInt(this.utilisateur.nbLignesTableau);
    }

    this.activeFilters.options = {
      // Généralité
      GenRegionOptions: null,
      GenInfrapoleOptions: null,
      GenSecteurOptions: null,
      GenUoOptions: null,
      GenUicOptions: MENU_ITEMS.UIC,
      GenLigneOptions: null,
      GenTypeSurveillanceOptions: MENU_ITEMS.WORK_VISIT_TYPE,
      GenCategorieOptions: MENU_ITEMS.WORK_CATEGORY,
      GenTypeOptions: MENU_ITEMS.WORK_TYPE_CATEGORY_NULL,
      GenReferentRegionalOptions: null,
      GenReferentAoapOptions: null,
      GenMoyensSurveillanceOptions: MENU_ITEMS.MEANS,
      GenAnneeSurveillanceOptions: null,
      GenCvOptions: MENU_ITEMS.BOOLEAN,
      GenPeriodiciteOptions: MENU_ITEMS.SURVEILLANCE_PERIODICITE_ALL,
      GenRespSurveillanceOptions: MENU_ITEMS.SURVEILLANCE_RESPONSABLE,

      // Description
      DesOaGeometrieOptions: MENU_ITEMS.GEOMETRY_NULL,
      DesOaAssemblageOptions: MENU_ITEMS.ASSEMBLY,
      DesOtNatureOptions: MENU_ITEMS.NATURE,
      DesBncOccupationOptions: MENU_ITEMS.OCCUPATION,
      DesBncBatPublicOptions: MENU_ITEMS.BOOLEAN,
      DesBncVoieOptions: MENU_ITEMS.BOOLEAN,
      DesBncAssemblageOptions: MENU_ITEMS.ASSEMBLY,
      DesAoGeometrieOptions: MENU_ITEMS.GEOMETRY_NULL,
      DesAoAssemblageOptions: MENU_ITEMS.ASSEMBLY,

      // Situation
      SitDepartementOptions: null,
      SitCommuneOptions: null,
      SitOuvrageEnveloppeOptions: MENU_ITEMS.BOOLEAN,
      SitPositionOptions: MENU_ITEMS.SITUATION,
      SitLocalisationOptions: MENU_ITEMS.BOOLEAN,
      SitPhotoOptions: MENU_ITEMS.BOOLEAN,
      SitNoteEtatOptions: MENU_ITEMS.NIVEAU_ETAT,
      SitGraviteOptions: MENU_ITEMS.NIVEAU_GRAVITE,

      // Suivi
      SuiAquatiqueOptions: MENU_ITEMS.BOOLEAN,
      SuiNivelOptions: MENU_ITEMS.BOOLEAN,
      SuiInstrumOptions: MENU_ITEMS.BOOLEAN,
      SuiPlongeurOptions: MENU_ITEMS.BOOLEAN,
      SuiTourneeIntOptions: MENU_ITEMS.BOOLEAN,

      // Particularités
      ParNomOptions: null,
      ParFsaOptions: MENU_ITEMS.FSA,
      ParFamilleOptions: MENU_ITEMS.RATTACHEMENT,
      ParPlanOrsecOptions: MENU_ITEMS.BOOLEAN,
      ParMonumentHistOptions: MENU_ITEMS.BOOLEAN,
      ParSiteProtegeOptions: MENU_ITEMS.BOOLEAN,
      ParEmbranchementOptions: MENU_ITEMS.BOOLEAN,
      ParRisqueNaturelOptions: MENU_ITEMS.BOOLEAN,
      ParRisqueSismiqueOptions: MENU_ITEMS.BOOLEAN,
      ParRisqueTechnoOptions: MENU_ITEMS.BOOLEAN,
      ParRisqueRoutierOptions: MENU_ITEMS.BOOLEAN,
      ParRisqueFluvialOptions: MENU_ITEMS.BOOLEAN,
      ParSansDateOptions: MENU_ITEMS.BOOLEAN,
      ParSegGestionOptions: null,
      ParCodeAnalyOptions: null,
      ParConventionOptions: MENU_ITEMS.BOOLEAN,
      ParFicheModifieeOptions: MENU_ITEMS.BOOLEAN,
      ParStatutOptions: MENU_ITEMS.STATUT,
    };

    this.activeFilters.filters = {
      // ATTENTION LES SUFFIXES ONT UN IMPACT SUR LE PROGRAMME
      // (Selected, SelectedSearch, Typed, TypedSearch)

      // Généralité
      GenRegionSelected: null,
      GenInfrapoleSelected: null,
      GenSecteurSelected: null,
      GenUoSelected: null,
      GenUicSelected: null,
      GenLigneSelected: null,
      GenTypeSurveillanceSelected: null,
      GenCategorieSelected: null,
      GenTypeSelected: null,
      GenReferentRegionalSelected: null,
      GenReferentAoapSelected: null,
      GenFromTyped: null,
      GenToTyped: null,
      GenAnneeConstructionFromTyped: null,
      GenAnneeConstructionToTyped: null,
      GenMoyensSurveillanceSelected: null,
      GenAnneeSurveillanceSelected: null,
      GenCvSelected: null,
      GenPeriodiciteSelected: null,
      GenRespSurveillanceSelected: null,

      // Description
      DesOaLongueurFromTyped: null,
      DesOaLongueurToTyped: null,
      DesOaHauteurFromTyped: null,
      DesOaHauteurToTyped: null,
      DesOaHauteurLibreFromTyped: null,
      DesOaHauteurLibreToTyped: null,
      DesOaLargeurFromTyped: null,
      DesOaLargeurToTyped: null,
      DesOaOuvertureFromTyped: null,
      DesOaOuvertureToTyped: null,
      DesOaPorteeFromTyped: null,
      DesOaPorteeToTyped: null,
      DesOaCouvertureFromTyped: null,
      DesOaCouvertureToTyped: null,
      DesOaTraverseesFromTyped: null,
      DesOaTraverseesToTyped: null,
      DesOaTabliersFromTyped: null,
      DesOaTabliersToTyped: null,
      DesOaPenteFromTyped: null,
      DesOaPenteToTyped: null,
      DesOaPenteGaucheFromTyped: null,
      DesOaPenteGaucheToTyped: null,
      DesOaPenteDroiteFromTyped: null,
      DesOaPenteDroiteToTyped: null,
      DesOaGeometrieSelected: null,
      DesOaAssemblageSelected: null,
      DesOtHauteurFromTyped: null,
      DesOtHauteurToTyped: null,
      DesOtHauteurGaucheFromTyped: null,
      DesOtHauteurGaucheToTyped: null,
      DesOtHauteurDroiteFromTyped: null,
      DesOtHauteurDroiteToTyped: null,
      DesOtProfondeurFromTyped: null,
      DesOtProfondeurToTyped: null,
      DesOtProfondeurGaucheFromTyped: null,
      DesOtProfondeurGaucheToTyped: null,
      DesOtProfondeurDroiteFromTyped: null,
      DesOtProfondeurDroiteToTyped: null,
      DesOtPenteFromTyped: null,
      DesOtPenteToTyped: null,
      DesOtPenteGaucheFromTyped: null,
      DesOtPenteGaucheToTyped: null,
      DesOtPenteDroiteFromTyped: null,
      DesOtPenteDroiteToTyped: null,
      DesOtNatureSelected: null,
      DesBncHauteurMaxiFromTyped: null,
      DesBncHauteurMaxiToTyped: null,
      DesBncPorteeFromTyped: null,
      DesBncPorteeToTyped: null,
      DesBncFermesFromTyped: null,
      DesBncFermesToTyped: null,
      DesBncTraveesFromTyped: null,
      DesBncTraveesToTyped: null,
      DesBncOccupationSelected: null,
      DesBncBatPublicSelected: null,
      DesBncVoieSelected: null,
      DesBncAssemblageSelected: null,
      DesAoGeometrieSelected: null,
      DesAoAssemblageSelected: null,

      // Situation
      SitDepartementSelected: null,
      SitCommuneSelectedSearch: null,
      SitOuvrageEnveloppeSelected: null,
      SitPositionSelected: null,
      SitLocalisationSelected: null,
      SitPhotoSelected: null,
      SitNoteEtatSelected: null,
      SitGraviteSelected: null,

      // Suivi
      SuiAquatiqueSelected: null,
      SuiNivelSelected: null,
      SuiInstrumSelected: null,
      SuiPlongeurSelected: null,
      SuiTourneeIntSelected: null,

      // Particularités
      ParNomTypedSearch: null,
      ParFsaSelected: null,
      ParFamilleSelected: null,
      ParPlanOrsecSelected: null,
      ParMonumentHistSelected: null,
      ParSiteProtegeSelected: null,
      ParEmbranchementSelected: null,
      ParRisqueNaturelSelected: null,
      ParRisqueSismiqueSelected: null,
      ParRisqueTechnoSelected: null,
      ParRisqueRoutierSelected: null,
      ParRisqueFluvialSelected: null,
      ParBiaisFromTyped: null,
      ParBiaisToTyped: null,
      ParSansDateSelected: null,
      ParSegGestionSelected: null,
      ParCodeAnalySelected: null,
      ParConventionSelected: null,
      ParFicheModifieeSelected: null,
      ParStatutSelected: null,
    };

    this.loading = true;

    let optionalCols: { optionalColsName: string, optionalCols: SelectItem[] }[] = [
      {
        optionalColsName: 'optionalColsNull',
        optionalCols: [
          { label: 'CARACTERISTIQUES GÉOMÉTRIQUES', value: { id: 100, col: null, children: [101,102,103,104] } },
          { label: 'Géométrie', value: { id: 101, col: { field: 'ouvrage.geometrie', header: 'Géométrie', style: 'column-center column-width-100' }, children: null } },
          { label: 'OA : Ouverture', value: { id: 102, col: { field: 'ouvrage.oaOuverture', header: 'OA : Ouverture', style: 'column-center column-width-100' }, children: null } },
          { label: 'Longueur', value: { id: 103, col: { field: 'ouvrage.longueur', header: 'Longueur', style: 'column-center column-width-100' }, children: null } },
          { label: 'Hauteur', value: { id: 104, col: { field: 'ouvrage.oaHauteur', header: 'Hauteur', style: 'column-center column-width-100' }, children: null } },
          { label: 'VISITES PÉRIODIQUES', value: { id: 200, col: null, children: [201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219] } },
          { label: 'FSA', value: { id: 201, col: { field: 'ouvrage.fsa', header: 'FSA', style: 'column-center column-width-100' }, children: null } },
          { label: 'Moyens', value: { id: 202, col: { field: 'moyensSurveillance', header: 'Moyens', style: 'column-center column-width-100' }, children: null } },
          { label: 'CV', value: { id: 203, col: { field: 'cv', header: 'CV', style: 'column-center column-width-100' }, children: null } },
          { label: 'Périodicité ID', value: { id: 204, col: { field: 'periodiciteId', header: 'Périodicité ID', style: 'column-center column-width-150' }, children: null } },
          { label: 'Dernière ID', value: { id: 205, col: { field: 'lastDateId', header: 'Dernière ID', style: 'column-center column-width-150' }, children: null } },
          { label: 'Auteur ID', value: { id: 206, col: { field: 'auteurId', header: 'Auteur ID', style: 'column-center column-width-150' }, children: null } },
          { label: 'Prochaine ID', value: { id: 207, col: { field: 'nextDateId', header: 'Prochaine ID', style: 'column-center column-width-150' }, children: null } },
          { label: 'Périodicité VD', value: { id: 208, col: { field: 'periodiciteVd', header: 'Périodicité VD', style: 'column-center column-width-150' }, children: null } },
          { label: 'Dernière VD', value: { id: 209, col: { field: 'lastDateVd', header: 'Dernière VD', style: 'column-center column-width-150' }, children: null } },
          { label: 'Auteur VD', value: { id: 210, col: { field: 'auteurVd', header: 'Auteur VD', style: 'column-center column-width-150' }, children: null } },
          { label: 'Prochaine VD', value: { id: 211, col: { field: 'nextDateVd', header: 'Prochaine VD', style: 'column-center column-width-150' }, children: null } },
          { label: 'Périodicité VI', value: { id: 212, col: { field: 'periodiciteVi', header: 'Périodicité VI', style: 'column-center column-width-150' }, children: null } },
          { label: 'Dernière VI', value: { id: 213, col: { field: 'lastDateVi', header: 'Dernière VI', style: 'column-center column-width-150' }, children: null } },
          { label: 'Auteur VI', value: { id: 214, col: { field: 'auteurVi', header: 'Auteur VI', style: 'column-center column-width-150' }, children: null } },
          { label: 'Prochaine VI', value: { id: 215, col: { field: 'nextDateVi', header: 'Prochaine VI', style: 'column-center column-width-150' }, children: null } },
          { label: 'Périodicité VS', value: { id: 216, col: { field: 'periodiciteVs', header: 'Périodicité VS', style: 'column-center column-width-150' }, children: null } },
          { label: 'Dernière VS', value: { id: 217, col: { field: 'lastDateVs', header: 'Dernière VS', style: 'column-center column-width-150' }, children: null } },
          { label: 'Auteur VS', value: { id: 218, col: { field: 'auteurVs', header: 'Auteur VS', style: 'column-center column-width-150' }, children: null } },
          { label: 'Prochaine VS', value: { id: 219, col: { field: 'nextDateVs', header: 'Prochaine VS', style: 'column-center column-width-150' }, children: null } },
          { label: 'SURVEILLANCES COMPLÉMENTAIRES', value: { id: 300, col: null, children: [301] } },
          { label: 'Surveillance Complémentaire', value: { id: 301, col: { field: 'complementaryVisit', header: 'Surveillance Complémentaire', style: 'column-center column-width-200' }, children: null } },
          { label: 'OBSTACLE & DATE DE CONSTRUCTION', value: { id: 400, col: null, children: [401,402] } },
          { label: 'Etabli sur/sous', value: { id: 401, col: { field: 'ouvrage.aboveBelow', header: 'Etabli sur/sous', style: 'column-center column-width-150' }, children: null } },
          { label: 'Date de construction', value: { id: 402, col: { field: 'ouvrage.year', header: 'Date de construction', style: 'column-center column-width-200' }, children: null } },
          { label: 'PLANIFICATION', value: { id: 1000, col: null, children: [1001,1002,1003,1004,1005,1006,1007,1008,1009,1010] } },
        ]
      },
      {
        optionalColsName: 'optionalColsOT',
        optionalCols: [
          { label: 'CARACTERISTIQUES GÉOMÉTRIQUES', value: { id: 100, col: null, children: [101,102,103,104,105] } },
          { label: 'Fiche Signalétique', value: { id: 101, col: { field: 'ouvrage.otFicheSignal', header: 'Fiche Signalétique', style: 'column-center column-width-150' }, children: null } },
          { label: 'Classement OT', value: { id: 102, col: { field: 'ouvrage.otClassement', header: 'Classement OT', style: 'column-center column-width-150' }, children: null } },
          { label: 'Catégorie OTS', value: { id: 103, col: { field: 'ouvrage.categorieOts', header: 'Catégorie OTS', style: 'column-center column-width-150' }, children: null } },
          { label: 'Type OTS', value: { id: 104, col: { field: 'ouvrage.typeOts', header: 'Type OTS', style: 'column-center column-width-150' }, children: null } },
          { label: 'Nom', value: { id: 105, col: { field: 'ouvrage.name', header: 'Nom', style: 'column-width-300' }, children: null } },
          { label: 'VISITES PÉRIODIQUES', value: { id: 200, col: null, children: [201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216] } },
          { label: 'Périodicité VD OT', value: { id: 201, col: { field: 'periodiciteVdOt', header: 'Périodicité VD OT', style: 'column-center column-width-150' }, children: null } },
          { label: 'Dernière VD OT', value: { id: 202, col: { field: 'lastDateVdOt', header: 'Dernière VD OT', style: 'column-center column-width-150' }, children: null } },
          { label: 'Auteur VD OT', value: { id: 203, col: { field: 'auteurVdOt', header: 'Auteur VD OT', style: 'column-center column-width-150' }, children: null } },
          { label: 'Prochaine VD OT', value: { id: 204, col: { field: 'nextDateVdOt', header: 'Prochaine VD OT', style: 'column-center column-width-150' }, children: null } },
          { label: 'Périodicité VI OT', value: { id: 205, col: { field: 'periodiciteViOt', header: 'Périodicité VI OT', style: 'column-center column-width-150' }, children: null } },
          { label: 'Dernière VI OT', value: { id: 206, col: { field: 'lastDateViOt', header: 'Dernière VI OT', style: 'column-center column-width-150' }, children: null } },
          { label: 'Auteur VI OT', value: { id: 207, col: { field: 'auteurViOt', header: 'Auteur VI OT', style: 'column-center column-width-150' }, children: null } },
          { label: 'Prochaine VI OT', value: { id: 208, col: { field: 'nextDateViOt', header: 'Prochaine VI OT', style: 'column-center column-width-150' }, children: null } },
          { label: 'Périodicité VP OT', value: { id: 209, col: { field: 'periodiciteVpOt', header: 'Périodicité VP OT', style: 'column-center column-width-150' }, children: null } },
          { label: 'Dernière VP OT', value: { id: 210, col: { field: 'lastDateVpOt', header: 'Dernière VP OT', style: 'column-center column-width-150' }, children: null } },
          { label: 'Auteur VP OT', value: { id: 211, col: { field: 'auteurVpOt', header: 'Auteur VP OT', style: 'column-center column-width-150' }, children: null } },
          { label: 'Prochaine VP OT', value: { id: 212, col: { field: 'nextDateVpOt', header: 'Prochaine VP OT', style: 'column-center column-width-150' }, children: null } },
          { label: 'Périodicité VA EFPB', value: { id: 213, col: { field: 'periodiciteVaOt', header: 'Périodicité VA EFPB', style: 'column-center column-width-150' }, children: null } },
          { label: 'Dernière VA EFPB', value: { id: 214, col: { field: 'lastDateVaOt', header: 'Dernière VA EFPB', style: 'column-center column-width-150' }, children: null } },
          { label: 'Auteur VA EFPB', value: { id: 215, col: { field: 'auteurVaOt', header: 'Auteur VA EFPB', style: 'column-center column-width-150' }, children: null } },
          { label: 'Prochaine VA EFPB', value: { id: 216, col: { field: 'nextDateVaOt', header: 'Prochaine VA EFPB', style: 'column-center column-width-150' }, children: null } },
          { label: 'PLANIFICATION', value: { id: 1000, col: null, children: [1001,1002,1003,1004,1005,1006,1007,1008,1009,1010] } },
        ]
      }
    ];

    let annee = new Date().getFullYear();
    for (let i = 0 ; i < 10 ; i++) {
      let selectItem : SelectItem = {
        label: 'Année ' + (annee + i),
        value: {
          id: 1001 + i,
          col: {
            field: 'visiteN' + i,
            header: 'Année ' + (annee + i),
            style: 'column-center column-width-100'
          },
          children: null
        }
      };
      optionalCols[0].optionalCols.push(selectItem);
      optionalCols[1].optionalCols.push(selectItem);
    }

    this.columnHelper = new ColumnHelper(this.conditionalCols, optionalCols, this.columnService, 'surveillance');

    this.pendingBackendResponses = 5;

    this.filterService.getRegions().subscribe(data => {
      this.activeFilters.options.GenRegionOptions = [ { raw: null, fr: '' } ];
      for (let regionDto of data) {
        let region : Region = new Region(regionDto);
        this.activeFilters.options.GenRegionOptions.push({
          raw: region.id.toString(),
          fr: region.name
        });
      }
      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });

    this.filterService.getSegmentsGestion().subscribe(data => {
      this.activeFilters.options.ParSegGestionOptions = [ { raw: null, fr: '' } ];
      for (let segmentDto of data) {
        this.activeFilters.options.ParSegGestionOptions.push({
          raw: segmentDto.dto,
          fr: segmentDto.dto
        });
      }
      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });

    this.filterService.getComptesAnalytiques().subscribe(data => {
      this.activeFilters.options.ParCodeAnalyOptions = [ { raw: null, fr: '' } ];
      for (let codeDto of data) {
        this.activeFilters.options.ParCodeAnalyOptions.push({
          raw: codeDto.dto,
          fr: codeDto.dto
        });
      }
      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });

    this.activeFilters.options.GenAnneeSurveillanceOptions = [{ raw: null, fr: '' }];
    let maxYear : Number = new Date().getFullYear() + 10;
    for (let year = 2008 ; year <= maxYear ; year++) {
      this.activeFilters.options.GenAnneeSurveillanceOptions.push( { raw: year, fr: year } );
    }

    this.regionInfrapoleSecteurUoChange();

    this.getDepartementsAccordingToRegion();

    this.getReferentsTechniquesRegionaux();
  }

  getFiltersForConnectedUser() {
    // Ne fonctionne que quand tous les appels précédents ont répondu (UNE SEULE FOIS)
    if (!this.activeFilters.areConnectedUserFiltersLoaded && this.pendingBackendResponses == 0) {
      let self = this;
      let callback = (arg: any) : void => {
        arg.getReferentsTechniquesRegionaux();
        arg.getReferentsAoap();
        arg.activeFilters.useFiltersForRequests(true);
        if (!arg.activeFilters.connectedUserFilters
            || arg.activeFilters.connectedUserFilters == null
            || arg.activeFilters.connectedUserFilters.length == 0) {
          if (arg.utilisateur.up) {
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenUo', arg.utilisateur.up ? arg.utilisateur.up.id : null);
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenSecteur', arg.utilisateur.secteur ? arg.utilisateur.secteur.id : null);
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenInfrapole', arg.utilisateur.infrapole ? arg.utilisateur.infrapole.id : null);
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenRegion', arg.utilisateur.region ? arg.utilisateur.region.id : null);
          }
        }
      };
      this.activeFilters.populateDefaultFiltersForConnectedUser(callback, self);
      this.columnHelper.getColumnsOrder();
    }
  }

  loadSurveillancesLazy(event: any) {
    setTimeout(() => {
      this.fetchDataFromBackEnd(event, false);
    });
  }

  fetchDataFromBackEnd(event: any, saveFilters: boolean) {
    this.loading = true;

    let requestFilters: ParamValue[] = this.activeFilters.generateRequestFilters(event, this.surveillanceTable);

    this.surveillanceService.getSurveillances(saveFilters, requestFilters).subscribe(data => {
      this.totalRecords = data.count;
      this.surveillances = [];
      for (let surveillanceDTO of data.result) {
        this.surveillances.push(new Surveillance(surveillanceDTO));
      }
      this.loading = false;
    });
  }

  resolveFieldData(rowData, colField){
    return ObjectUtils.resolveFieldData(rowData, colField);
  }

  // Généralité

  GenRegionChange($event) {
    this.regionInfrapoleSecteurUoChange();
    this.getDepartementsAccordingToRegion();
    this.getReferentsTechniquesRegionaux();
    this.getReferentsAoap();
    this.checkCommune();
  }

  GenInfrapoleChange($event) {
    this.regionInfrapoleSecteurUoChange();
  }

  GenSecteurChange($event) {
    this.regionInfrapoleSecteurUoChange();
  }

  GenUoChange($event) {
    this.regionInfrapoleSecteurUoChange();
  }

  regionInfrapoleSecteurUoChange() {
    this.filterService.getRegionInfrapoleSecteurUoLigne(
        this.activeFilters.getSelectedRawString('GenRegionSelected'),
        this.activeFilters.getSelectedRawString('GenInfrapoleSelected'),
        this.activeFilters.getSelectedRawString('GenSecteurSelected'),
        this.activeFilters.getSelectedRawString('GenUoSelected')
      ).subscribe(data => {

      let GenInfrapoleSelected = this.activeFilters.getSelectedRawString('GenInfrapoleSelected');
      let GenSecteurSelected = this.activeFilters.getSelectedRawString('GenSecteurSelected');
      let GenUoSelected = this.activeFilters.getSelectedRawString('GenUoSelected');
      let GenLigneSelected = this.activeFilters.getSelectedRawString('GenLigneSelected');

      let infrapoles = [ { raw: null, fr: '' } ];
      let secteurs = [ { raw: null, fr: '' } ];
      let unitesOperationnelle = [ { raw: null, fr: '' } ];
      let lignes = [ { raw: null, fr: '' } ];

      for (let infrapoleDTO of data.infrapoles) {
        infrapoles.push({ raw: infrapoleDTO.id.toString(), fr: infrapoleDTO.name });
      }

      for (let secteurDTO of data.secteurs) {
        secteurs.push({ raw: secteurDTO.id.toString(), fr: secteurDTO.nom });
      }

      for (let uniteOperationnelleDTO of data.unitesOperationnelle) {
        unitesOperationnelle.push({ raw: uniteOperationnelleDTO.id.toString(), fr: uniteOperationnelleDTO.name });
      }

      for (let ligneDTO of data.lignes) {
        lignes.push({ raw: ligneDTO.id.toString(), fr: ligneDTO.nom });
      }

      this.activeFilters.options.GenInfrapoleOptions = infrapoles;
      this.activeFilters.options.GenSecteurOptions = secteurs;
      this.activeFilters.options.GenUoOptions = unitesOperationnelle;
      this.activeFilters.options.GenLigneOptions = lignes;

      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenInfrapole', GenInfrapoleSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenSecteur', GenSecteurSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenUo', GenUoSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenLigne', GenLigneSelected);

      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });
  }

  getDepartementsAccordingToRegion() {
    this.filterService.getDepartements(
        this.activeFilters.getSelectedRawString('GenRegionSelected')
      ).subscribe(data => {

      let SitDepartementSelected = this.activeFilters.getSelectedRawString('SitDepartementSelected');

      let departements = [ { raw: null, fr: '' } ];

      for (let departementDTO of data) {
        departements.push({ raw: departementDTO.id.toString(), fr: departementDTO.nom });
      }

      this.activeFilters.options.SitDepartementOptions = departements;

      this.activeFilters.trySetSelectedFromOptionsIfPossible('SitDepartement', SitDepartementSelected);

      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });
  }

  GenTypeSurveillanceChange($event) {
    if (this.activeFilters.filters['GenTypeSurveillanceSelected'] != null
        && (this.activeFilters.filters['GenTypeSurveillanceSelected'].raw == 'visit_type_sp'
        || this.activeFilters.filters['GenTypeSurveillanceSelected'].raw == 'visit_type_sr')) {
      this.activeFilters.options['GenPeriodiciteOptions'] = MENU_ITEMS.SURVEILLANCE_PERIODICITE_ALL
    } else {
      this.activeFilters.options['GenPeriodiciteOptions'] = MENU_ITEMS.SURVEILLANCE_PERIODICITE_ANNEES
    }
    if (this.activeFilters.filters['GenTypeSurveillanceSelected'] == null
        || this.activeFilters.filters['GenTypeSurveillanceSelected'].raw == null
        || this.activeFilters.filters['GenTypeSurveillanceSelected'].raw == 'surv_tiers'
        || this.activeFilters.filters['GenTypeSurveillanceSelected'].raw == 'surv_current') {
      this.activeFilters.filters['GenPeriodiciteSelected'] = this.activeFilters.options['GenPeriodiciteOptions'][0];
      this.activeFilters.filters['GenRespSurveillanceSelected'] = this.activeFilters.options['GenRespSurveillanceOptions'][0];
    }
  }

  GenCategorieChange($event) {
    if (this.activeFilters.filters['GenCategorieSelected'] != null) {
      let GenTypeSelected = this.activeFilters.getSelectedRawString('GenTypeSelected');
      let self = this;
      switch (this.activeFilters.filters['GenCategorieSelected'].raw) {
        case null :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_NULL;
          setTimeout(function() {
            self.accordionAO.close();
            self.accordionBNC.close();
            self.accordionOA.close();
            self.accordionOT.close();
          }, 100);
          break;
        case 'category_Autre' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_OTHERS;
          this.activeFilters.razAllDescriptionFilters();
          setTimeout(function() {
            self.accordionAO.open();
            self.accordionBNC.close();
            self.accordionOA.close();
            self.accordionOT.close();
          }, 100);
          break;
        case 'category_Bat' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_BAT;
          this.activeFilters.razAllDescriptionFilters();
          setTimeout(function() {
            self.accordionAO.close();
            self.accordionBNC.open();
            self.accordionOA.close();
            self.accordionOT.close();
          }, 100);
          break;
        case 'category_OA' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_OA;
          this.activeFilters.razAllDescriptionFilters();
          setTimeout(function() {
            self.accordionAO.close();
            self.accordionBNC.close();
            self.accordionOA.open();
            self.accordionOT.close();
          }, 100);
          break;
        case 'category_OT' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_OT;
          this.activeFilters.razAllDescriptionFilters();
          setTimeout(function() {
            self.accordionAO.close();
            self.accordionBNC.close();
            self.accordionOA.close();
            self.accordionOT.open();
          }, 100);
          break;
        case 'category_Hydro' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_HYDRO;
          this.activeFilters.razAllDescriptionFilters();
          break;
      }
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenType', GenTypeSelected);
      this.GenTypeChange(null);
    }
  }

  GenTypeChange($event) {
    if (this.activeFilters.filters['GenTypeSelected'] != null) {
      if (this.activeFilters.filters['GenTypeSelected'].raw != null) {
        let menuItem = MENU_ITEMS;
        let options = menuItem['GEOMETRY_' + this.activeFilters.filters['GenTypeSelected'].raw];
        this.activeFilters.options.DesAoGeometrieOptions = options;
        this.activeFilters.options.DesOaGeometrieOptions = options;
      } else {
        this.activeFilters.options.DesAoGeometrieOptions = MENU_ITEMS.GEOMETRY_NULL;
        this.activeFilters.options.DesOaGeometrieOptions = MENU_ITEMS.GEOMETRY_NULL;
      }
    }
  }

  getReferentsTechniquesRegionaux() {
    this.filterService.getReferentsTechniquesRegionaux(this.activeFilters.filters.GenRegionSelected, 'SOAR').subscribe(data => {

      let referents = [ { raw: null, fr: '' } ];

      for (let referentDTO of data) {
        referents.push({ raw: referentDTO.idUtilisateur.toString(), fr: referentDTO.prenom + ' ' + referentDTO.nom });
      }

      this.activeFilters.options.GenReferentRegionalOptions = referents;
    });
  }

  getReferentsAoap() {
    this.filterService.getReferentsTechniquesRegionaux(this.activeFilters.filters.GenRegionSelected, 'AOAP').subscribe(data => {

      let referents = [ { raw: null, fr: '' } ];

      for (let referentDTO of data) {
        referents.push({ raw: referentDTO.idUtilisateur.toString(), fr: referentDTO.prenom + ' ' + referentDTO.nom });
      }

      this.activeFilters.options.GenReferentAoapOptions = referents;
    });
  }

  // Situation

  SitDepartementChange($event) {
    this.checkCommune();
  }

  SitCommuneSearch($event) {
    this.getCommunes($event.query);
  }

  getCommunes(searchText: string) {
    if (searchText.length > 2) {
      this.filterService.getCommunes(
          this.activeFilters.getSelectedRawString('GenRegionSelected'),
          this.activeFilters.getSelectedRawString('SitDepartementSelected'),
          searchText
        ).subscribe(data => {

        let SitCommuneSelected = this.activeFilters.getSelectedRawString('SitCommuneSelected');

        let communes = [];

        for (let communeDTO of data) {
          communes.push({ raw: communeDTO.id.toString(), fr: communeDTO.nom + ' (' + communeDTO.codePostal + ')' });
        }

        this.activeFilters.options.SitCommuneOptions = communes;

        this.activeFilters.trySetSelectedFromOptionsIfPossible('SitCommune', SitCommuneSelected);

      });
    } else {
      this.activeFilters.options.SitCommuneOptions = [];
    }
  }

  checkCommune() {
    if (this.activeFilters.filters['SitCommuneSelected'] != null && this.activeFilters.filters['SitCommuneSelected'].raw != null) {
      this.filterService.checkCommune(
          this.activeFilters.getSelectedRawString('GenRegionSelected'),
          this.activeFilters.getSelectedRawString('SitDepartementSelected'),
          this.activeFilters.filters['SitCommuneSelected'].raw
        ).subscribe(data => {
          if (data.dto == 'nok') {
            this.activeFilters.filters['SitCommuneSelected'] =  null;
          }
      });
    }
  }

  // Particularités
  ParNomSearch($event) {
    this.getOuvragesName($event.query);
  }

  getOuvragesName(searchText: string) {
    if (searchText.length > 2) {
      this.filterService.getOuvragesName(searchText).subscribe(data => {
        let ouvragesName = [];
        for (let ouvrageDTO of data) {
          ouvragesName.push({ raw: ouvrageDTO.name });
        }
        this.activeFilters.options.ParNomOptions = ouvragesName;
      });
    } else {
      this.activeFilters.options.ParNomOptions = [];
    }
  }

  onChercherClick() {
    this.activeFilters.useFiltersForRequests(true);
    this.fetchDataFromBackEnd(null, true);
    this.accordionRechercher.close();
  }

  onRazClick() {
    this.activeFilters.razAllFilters();
    this.activeFilters.useFiltersForRequests(true);
    this.fetchDataFromBackEnd(null, true);
    this.accordionRechercher.close();
  }

  onExporterClick() {
    this.exportLoading = true;
    this.toastrHelper.showToast(
      NbToastStatus.SUCCESS,
      'EXPORT EN COURS...',
      'Votre fichier d\'export est en cours de génération. Il sera téléchargé automatiquement une fois prêt. En attendant, vous pouvez continuer à utiliser l\'application SANS RAFRAICHIR LA PAGE.',
      20000
    );
    const event = { first: 0, rows: this.totalRecords };
    const requestFilters: ParamValue[] = this.activeFilters.generateRequestFilters(event, this.surveillanceTable);
    this.surveillanceService.getSurveillances(false, requestFilters, 3600).subscribe(data => {
      ExportExcelHelper.export(this.columnHelper.convertObjectsArrayToRawObjectsJsonData(data.result), 'ExportSurveillances', 'SURVEILLANCES');
      this.exportLoading = false;
    });
  }

  onInputKeyPress($event) {
    if ($event.keyCode == 13) {
      this.onChercherClick();
    }
  }

  onRowSelect(event) {
    this.localStorageService.setItem('surveillance_id', event.data.id);
    this.router.navigate([FrontUrlConstant.PATH_TO_FICHE_SURVEILLANCE]);
  }
}
