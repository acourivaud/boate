import { FrontUrlConstant } from './../../constant/FrontUrlConstant';
import { Infrapole } from './../../model/infrapole';
import { Secteur } from './../../model/secteur';
import { UniteOperationnelle } from '../../model/uniteOperationnelle';
import { ToastrHelper } from './../../model/toastrHelper';
import { NbToastrService } from '@nebular/theme';
import { P_CALENDAR_LOCALES } from './../p-calendar-locales';
import { Permission } from './../../model/permission';
import { Region } from './../../model/region';
import { FilterService } from './../../service/filter.service';
import { FilterHelper } from './../../model/filterHelper';
import { UtilisateurService } from '../../service/utilisateur.service';
import { MENU_ITEMS } from './profil-menu';
import { ParamValue } from '../../model/paramValue';
import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { Component, ViewChild, OnInit } from '@angular/core';
import { Utilisateur } from '../../model/utilisateur';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Md5 } from 'ts-md5/dist/md5';
import { DateDto } from '../../model/dateDto';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'profil',
  styleUrls: ['./profil.component.scss'],
  templateUrl: './profil.component.html',
})
export class ProfilComponent implements OnInit {

  idUtilisateur: string;
  utilisateur: Utilisateur;
  utilisateurLoaded: boolean = false;

  permissions: Permission[];

  activeFilters: FilterHelper;

  loading: boolean = false;

  pendingBackendResponses: number;

  calendarLocale: any;

  toastrHelper: ToastrHelper;

  constructor(private utilisateurService: UtilisateurService,
              private filterService: FilterService,
              private toastrService: NbToastrService,
              private route: ActivatedRoute,
              private router: Router) {
              }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      this.idUtilisateur = params.id;
    });

    this.loading = true;

    this.calendarLocale = P_CALENDAR_LOCALES.FRENCH;

    this.toastrHelper = new ToastrHelper(this.toastrService);

    this.activeFilters = new FilterHelper(this.filterService, "profil");

    this.activeFilters.options = {
      EntiteOptions: MENU_ITEMS.ENTITY,

      FonctionOptions: MENU_ITEMS.FONCTION,

      RegionOptions: null,
      InfrapoleOptions: null,
      SecteurOptions: null,
      UoOptions: null,

      NbResultatsPageOptions: MENU_ITEMS.NB_RESULTS_PAGE,
      PageAccueilOptions: MENU_ITEMS.PAGE_ACCUEIL,
      AfficherAlertesOptions: MENU_ITEMS.BOOLEAN,

      PermissionOptions: MENU_ITEMS.BOOLEAN,
    };

    this.activeFilters.filters = {
      // ATTENTION LES SUFFIXES ONT UN IMPACT SUR LE PROGRAMME
      // (Selected, SelectedSearch, Typed, TypedSearch, Dated)

      PrenomTyped: null,
      NomTyped: null,
      TelephoneTyped: null,
      MobileTyped: null,
      CourrielTyped: null,
      FaxTyped: null,

      LoginTyped: null,
      FonctionSelected: null,
      EntiteSelected: null,
      DateFinMissionDated: null,
      Password1Typed: null,
      Password2Typed: null,
      RegionSelected: null,
      InfrapoleSelected: null,
      SecteurSelected: null,
      UoSelected: null,

      NbResultatsPageSelected: null,
      PageAccueilSelected: null,
      AfficherAlertesSelected: null,
    };

    this.pendingBackendResponses = 2;

    this.filterService.getRegions().subscribe(data => {
      this.activeFilters.options.RegionOptions = [ { raw: null, fr: "" } ];
      for (let regionDto of data) {
        let region : Region = new Region(regionDto);
        this.activeFilters.options.RegionOptions.push({
          raw: region.id.toString(),
          fr: region.name
        });
      }
      this.regionInfrapoleSecteurChange();
    });

    this.utilisateurService.getAllPermissions().subscribe(data => {
      this.permissions = [];
      for (let permissionDto of data) {
        this.permissions.push(new Permission(permissionDto));
        this.activeFilters.options['Permission' + permissionDto.id + 'Options'] = MENU_ITEMS.BOOLEAN;
        this.activeFilters.filters['Permission' + permissionDto.id + 'Selected'] = this.activeFilters.options['Permission' + permissionDto.id + 'Options'][0];
      }
      this.pendingBackendResponses--;
      if (this.pendingBackendResponses == 0 && this.utilisateurLoaded != true) {
        this.getUtilisateurInfos();
      }
    });
  }

  getUtilisateurInfos() {
    if (!this.idUtilisateur) {
      this.idUtilisateur = this.utilisateurService.getUtilisateurConnected().idUtilisateur.toString();
    }
    if (this.idUtilisateur != "new") {
      this.utilisateurService.getUtilisateurById(this.idUtilisateur.toString()).subscribe(data => {
        this.utilisateur = new Utilisateur(data);

        this.activeFilters.filters.PrenomTyped = this.utilisateur.prenom;
        this.activeFilters.filters.NomTyped = this.utilisateur.nom;
        this.activeFilters.filters.TelephoneTyped = this.utilisateur.telephoneFixe;
        this.activeFilters.filters.MobileTyped = this.utilisateur.telephoneMobile;
        this.activeFilters.filters.CourrielTyped = this.utilisateur.email;
        this.activeFilters.filters.FaxTyped = this.utilisateur.fax;

        this.activeFilters.filters.LoginTyped = this.utilisateur.login;
        this.activeFilters.filters.FonctionSelected = this.utilisateur.fonction ? this.activeFilters.options.FonctionOptions.find(e => e.raw != null && e.raw == this.utilisateur.fonction) : this.activeFilters.options.FonctionOptions[0];
        this.activeFilters.filters.EntiteSelected = this.utilisateur.entite ? this.activeFilters.options.EntiteOptions.find(e => e.raw != null && e.raw.toUpperCase() == this.utilisateur.entite) : this.activeFilters.options.EntiteOptions[0];
        if (this.utilisateur.dateFinMission) {
          let dateDto: DateDto = new DateDto(this.utilisateur.dateFinMission);
          let date: Date = dateDto.toDate();
          this.activeFilters.filters.DateFinMissionDated = this.utilisateur.dateFinMission ? new DateDto(this.utilisateur.dateFinMission).toDate() : undefined;
        }
        // this.activeFilters.filters.DateFinMissionDated = this.utilisateur.dateFinMission ? new DateDto(this.utilisateur.dateFinMission).toDate() : undefined;

        // this.activeFilters.filters.Password1Typed
        // this.activeFilters.filters.Password2Typed
        this.activeFilters.filters.UoSelected = this.utilisateur.up ? this.activeFilters.options.UoOptions.find(e => e.raw == this.utilisateur.up.id) : this.activeFilters.options.UoOptions[0];
        if (this.utilisateur.up) {
          if (this.utilisateur.up.secteur) {
            this.activeFilters.filters.SecteurSelected = this.utilisateur.up.secteur ? this.activeFilters.options.SecteurOptions.find(e => e.raw == this.utilisateur.up.secteur.id) : this.activeFilters.options.SecteurOptions[0];
            this.activeFilters.filters.InfrapoleSelected = this.utilisateur.up.secteur.infrapole ? this.activeFilters.options.InfrapoleOptions.find(e => e.raw == this.utilisateur.up.secteur.infrapole.id) : this.activeFilters.options.InfrapoleOptions[0];
            this.activeFilters.filters.RegionSelected = this.utilisateur.up.secteur.infrapole.region ? this.activeFilters.options.RegionOptions.find(e => e.raw == this.utilisateur.up.secteur.infrapole.region.id) : this.activeFilters.options.EntiteOptions[0];
          } else {
            this.activeFilters.filters.InfrapoleSelected = this.utilisateur.up.infrapole ? this.activeFilters.options.InfrapoleOptions.find(e => e.raw == this.utilisateur.up.infrapole.id) : this.activeFilters.options.InfrapoleOptions[0];
            this.activeFilters.filters.RegionSelected = this.utilisateur.up.infrapole.region ? this.activeFilters.options.RegionOptions.find(e => e.raw == this.utilisateur.up.infrapole.region.id) : this.activeFilters.options.EntiteOptions[0];
          }
        } else {
          this.activeFilters.filters.SecteurSelected = this.activeFilters.options.SecteurOptions[0];
          this.activeFilters.filters.InfrapoleSelected = this.activeFilters.options.InfrapoleOptions[0];
          this.activeFilters.filters.RegionSelected = this.activeFilters.options.EntiteOptions[0];
        }
        this.regionInfrapoleSecteurChange();

        this.activeFilters.filters.NbResultatsPageSelected = this.utilisateur.nbLignesTableau ? (this.activeFilters.options.NbResultatsPageOptions.find(e => e.raw == this.utilisateur.nbLignesTableau) ? this.activeFilters.options.NbResultatsPageOptions.find(e => e.raw == this.utilisateur.nbLignesTableau) : this.activeFilters.options.NbResultatsPageOptions[0]) : this.activeFilters.options.NbResultatsPageOptions[0];
        this.activeFilters.filters.PageAccueilSelected = this.utilisateur.pageAccueil ? this.activeFilters.options.PageAccueilOptions.find(e => e.raw == this.utilisateur.pageAccueil) : this.activeFilters.options.PageAccueilOptions[0];
        this.activeFilters.filters.AfficherAlertesSelected = this.utilisateur.affichageAlerte ? this.activeFilters.options.AfficherAlertesOptions.find(e => e.raw == this.utilisateur.affichageAlerte.toString()) : this.activeFilters.options.AfficherAlertesOptions[0];

        if (this.utilisateur.permissions && this.utilisateur.permissions.length > 0) {
          for (let permission of this.utilisateur.permissions) {
            this.activeFilters.filters['Permission' + permission.id + 'Selected'] = this.activeFilters.options['Permission' + permission.id + 'Options'].find(p => p.raw == "true");
          }
        }

        this.utilisateurService.updateLocalStorageIfUserMatchesConnectedOne(this.utilisateur);

        this.utilisateurLoaded = true;
        this.loading = false;
      });
    } else {
      this.utilisateur = new Utilisateur( {  } );

      this.activeFilters.filters.EntiteSelected = this.activeFilters.options.EntiteOptions[0];
      this.activeFilters.filters.NbResultatsPageSelected = this.activeFilters.options.NbResultatsPageOptions[0];
      this.activeFilters.filters.PageAccueilSelected = this.activeFilters.options.PageAccueilOptions[0];
      this.activeFilters.filters.AfficherAlertesSelected = this.activeFilters.options.AfficherAlertesOptions[0];

      this.utilisateurLoaded = true;
      this.loading = false;
    }
  }

  RegionChange($event) {
    this.regionInfrapoleSecteurChange();
  }

  InfrapoleChange($event) {
    this.regionInfrapoleSecteurChange();
  }

  SecteurChange($event) {
    this.regionInfrapoleSecteurChange();
  }

  regionInfrapoleSecteurChange() {
    this.filterService.getRegionInfrapoleSecteurUo(
        this.activeFilters.getSelectedRawString("RegionSelected"),
        this.activeFilters.getSelectedRawString("InfrapoleSelected"),
        this.activeFilters.getSelectedRawString("SecteurSelected")
      ).subscribe(data => {

      let InfrapoleSelected = this.activeFilters.getSelectedRawString("InfrapoleSelected");
      let SecteurSelected = this.activeFilters.getSelectedRawString("SecteurSelected");
      let UoSelected = this.activeFilters.getSelectedRawString("UoSelected");

      let infrapoles = [ { raw: null, fr: "" } ];
      let secteurs = [ { raw: null, fr: "" } ];
      let unitesOperationnelle = [ { raw: null, fr: "" } ];

      for (let infrapoleDTO of data.infrapoles) {
        infrapoles.push({ raw: infrapoleDTO.id.toString(), fr: infrapoleDTO.name });
      }

      for (let secteurDTO of data.secteurs) {
        secteurs.push({ raw: secteurDTO.id.toString(), fr: secteurDTO.nom });
      }

      for (let uniteOperationnelleDTO of data.unitesOperationnelle) {
        unitesOperationnelle.push({ raw: uniteOperationnelleDTO.id.toString(), fr: uniteOperationnelleDTO.name });
      }

      this.activeFilters.options.InfrapoleOptions = infrapoles;
      this.activeFilters.options.SecteurOptions = secteurs;
      this.activeFilters.options.UoOptions = unitesOperationnelle;

      this.activeFilters.trySetSelectedFromOptionsIfPossible("Infrapole", InfrapoleSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible("Secteur", SecteurSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible("Uo", UoSelected);

      this.pendingBackendResponses--;

      if (this.pendingBackendResponses == 0 && this.utilisateurLoaded != true) {
        this.getUtilisateurInfos();
      }
    });
  }

  saveUtilisateur() {

    // this.utilisateur.idUtilisateur
    this.utilisateur.login = this.activeFilters.filters.LoginTyped;
    if (this.activeFilters.filters.Password1Typed && this.activeFilters.filters.Password1Typed != null && this.activeFilters.filters.Password1Typed != "") {
      this.utilisateur.password = Md5.hashStr(this.activeFilters.filters.Password1Typed).toString();
    }
    this.utilisateur.nom = this.activeFilters.filters.NomTyped;
    this.utilisateur.prenom = this.activeFilters.filters.PrenomTyped;
    this.utilisateur.fonction = this.activeFilters.filters.FonctionSelected ? this.activeFilters.filters.FonctionSelected.raw : 'FUNCTION_INVITE';
    this.utilisateur.entite = this.activeFilters.filters.EntiteSelected && this.activeFilters.filters.EntiteSelected.raw != null ? this.activeFilters.filters.EntiteSelected.raw : undefined;
    this.utilisateur.telephoneFixe = this.activeFilters.filters.TelephoneTyped;
    this.utilisateur.telephoneMobile = this.activeFilters.filters.MobileTyped;
    this.utilisateur.fax = this.activeFilters.filters.FaxTyped;
    this.utilisateur.email = this.activeFilters.filters.CourrielTyped;
    if (this.activeFilters.filters.UoSelected && this.activeFilters.filters.UoSelected.raw && this.activeFilters.filters.UoSelected.raw != null) {
      this.utilisateur.up = new UniteOperationnelle( { id: this.activeFilters.filters.UoSelected.raw } );
      this.utilisateur.secteur = this.activeFilters.filters.SecteurSelected && this.activeFilters.filters.SecteurSelected.raw && this.activeFilters.filters.SecteurSelected.raw != null ? new Secteur( { id: this.activeFilters.filters.SecteurSelected.raw } ) : null;
      this.utilisateur.infrapole = this.activeFilters.filters.InfrapoleSelected && this.activeFilters.filters.InfrapoleSelected.raw && this.activeFilters.filters.InfrapoleSelected.raw != null ? new Infrapole( { id: this.activeFilters.filters.InfrapoleSelected.raw } ) : null;
      this.utilisateur.region = this.activeFilters.filters.RegionSelected && this.activeFilters.filters.RegionSelected.raw && this.activeFilters.filters.RegionSelected.raw != null ? new Region( { id: this.activeFilters.filters.RegionSelected.raw } ) : null;
    } else {
      this.utilisateur.up = null;
      this.utilisateur.secteur = null;
      this.utilisateur.infrapole = null;
      this.utilisateur.region = null;
    }
    this.utilisateur.dateFinMission = this.activeFilters.filters.DateFinMissionDated && this.activeFilters.filters.DateFinMissionDated != null ? DateDto.createFromDate(this.activeFilters.filters.DateFinMissionDated).toBackEndDto() : undefined;
    // this.utilisateur.urlSig
    // this.utilisateur.ssid
    this.utilisateur.nbLignesTableau = this.activeFilters.filters.NbResultatsPageSelected && this.activeFilters.filters.NbResultatsPageSelected.raw != null ? this.activeFilters.filters.NbResultatsPageSelected.raw : undefined;
    this.utilisateur.affichageAlerte = this.activeFilters.filters.AfficherAlertesSelected && this.activeFilters.filters.AfficherAlertesSelected.raw != null ? this.activeFilters.filters.AfficherAlertesSelected.raw : undefined;
    // this.utilisateur.consultationReleaseNotes
    this.utilisateur.pageAccueil = this.activeFilters.filters.PageAccueilSelected && this.activeFilters.filters.PageAccueilSelected.raw != null ? this.activeFilters.filters.PageAccueilSelected.raw : undefined;

    if (this.permissions && this.permissions.length > 0) {
      let permissions: Permission[] = [];
      for (let permission of this.permissions) {
        if (this.activeFilters.filters['Permission' + permission.id + 'Selected'] != null && this.activeFilters.filters['Permission' + permission.id + 'Selected'].raw == "true") {
          permissions.push(permission);
        }
      }
      this.utilisateur.permissions = permissions;
    }

    this.utilisateurService.saveUtilisateur(this.utilisateur).subscribe(data => {
      this.toastrHelper.showDefaultSuccessToast();
      if (!this.utilisateur.idUtilisateur) {
        this.router.navigate([FrontUrlConstant.PATH_TO_ADMINISTRATION]);
      } else {
        this.getUtilisateurInfos();
      }
    },
    err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });

  }

  arePasswordsEquals() : boolean {
    if ((!this.activeFilters.filters.Password1Typed || this.activeFilters.filters.Password1Typed == null) && (!this.activeFilters.filters.Password2Typed && this.activeFilters.filters.Password2Typed == null)) {
      return true;
    }
    if (this.activeFilters.filters.Password1Typed === this.activeFilters.filters.Password2Typed) {
      return true;
    }
    return false;
  }
}
