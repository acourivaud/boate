export const MENU_ITEMS =  {
  BOOLEAN: [
    { 
      raw: "false",
      fr: "Non",
    },
    { 
      raw: "true",
      fr: "Oui",
    },
  ],
  ENTITY: [
    { 
      raw: null,
      fr: "",
    },
    { 
      raw: "entity_drimoa_ot",
      fr: "DRIMOA/OT",
    },
    { 
      raw: "entity_even",
      fr: "INFRAPOLE",
    },
    { 
      raw: "entity_iglg",
      fr: "IGLG",
    },
    { 
      raw: "entity_igoa",
      fr: "IGOA",
    },
    { 
      raw: "entity_imt",
      fr: "IMT",
    },
    { 
      raw: "entity_otp",
      fr: "OTP",
    },
    { 
      raw: "entity_prily",
      fr: "PRILY",
    },
    { 
      raw: "entity_prichy",
      fr: "PRICHY",
    },
  ],
  NB_RESULTS_PAGE: [
    { 
      raw: "10",
      fr: "10",
    },
    { 
      raw: "20",
      fr: "20",
    },
    { 
      raw: "30",
      fr: "30",
    },
    { 
      raw: "40",
      fr: "40",
    },
    { 
      raw: "50",
      fr: "50",
    },
  ],
  PAGE_ACCUEIL: [
    { 
      raw: null,
      fr: "",
    },
    { 
      raw: "ouvrage",
      fr: "Ouvrage",
    },
    { 
      raw: "surveillance",
      fr: "Surveillance",
    },
  ],
  FONCTION: [
    { 
      raw: "FUNCTION_INVITE",
      fr: "Invité",
    },
    { 
      raw: "FUNCTION_ADMIN",
      fr: "Administrateur",
    },
    { 
      raw: "FUNCTION_ADMIN_ARCHIVE",
      fr: "Administrateur Archive",
    },
    { 
      raw: "FUNCTION_AH",
      fr: "AH",
    },
    { 
      raw: "FUNCTION_AOAP",
      fr: "AOAP",
    },
    { 
      raw: "FUNCTION_AOAU",
      fr: "AOAU",
    },
    { 
      raw: "FUNCTION_CU_DUO",
      fr: "DUO",
    },
    { 
      raw: "FUNCTION_DDI",
      fr: "DDI",
    },
    { 
      raw: "FUNCTION_DET",
      fr: "DET",
    },
    { 
      raw: "FUNCTION_DRONE_PILOTE",
      fr: "Pilote Drone",
    },
    { 
      raw: "FUNCTION_RI_OA_OT",
      fr: "RI OA/OT",
    },
    { 
      raw: "FUNCTION_RM_OA_OT",
      fr: "RM OA/OT",
    },
    { 
      raw: "FUNCTION_RRMI",
      fr: "RRMI",
    },
    { 
      raw: "FUNCTION_RMPL",
      fr: "RMPL",
    },
    { 
      raw: "FUNCTION_ROTP",
      fr: "ROTP",
    },
    { 
      raw: "FUNCTION_R_PRILY_Y",
      fr: "R PRILY Y",
    },
    { 
      raw: "FUNCTION_R_PRILY",
      fr: "R PRILY",
    },
    { 
      raw: "FUNCTION_SOAR",
      fr: "SOAR",
    },
    { 
      raw: "FUNCTION_ABE",
      fr: "ABE",
    },
    { 
      raw: "FUNCTION_TIERS",
      fr: "Tiers",
    },
    { 
      raw: "FUNCTION_COT",
      fr: "COT",
    },
    { 
      raw: "FUNCTION_ADJRI_OA_OT",
      fr: "ADJRI OA/OT",
    },
    { 
      raw: "FUNCTION_ARCHIVE",
      fr: "Archive",
    },
    { 
      raw: "FUNCTION_TOPO",
      fr: "Topographe",
    },
    { 
      raw: "FUNCTION_HYDRO",
      fr: "Hydro",
    },
    { 
      raw: "FUNCTION_CHEF_EG",
      fr: "Chef EG",
    },
    { 
      raw: "FUNCTION_OTHER",
      fr: "Autres",
    },
  ],
};