import { FilterService } from './../../service/filter.service';
import { UtilisateurService } from '../../service/utilisateur.service';
import { NgModule } from '@angular/core';
import { CalendarModule } from 'primeng/calendar';

import { ThemeModule } from '../../@theme/theme.module';
import { ProfilComponent } from './profil.component';

@NgModule({
  imports: [
    ThemeModule,
    CalendarModule
  ],
  declarations: [
    ProfilComponent,
  ],
  providers: [
    UtilisateurService,
    FilterService
  ]
})
export class ProfilModule { }
