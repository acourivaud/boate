import { FR_LOCALE } from './../../constant/CalendarFrLocale';
import { Vehicule } from './../../model/vehicule';
import { ArchiveBoite } from './../../model/archiveBoite';
import { ColumnHelper } from './../../model/columnHelper';
import { ExportExcelHelper } from './../../model/exportExcelHelper';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { BackUrlConstant } from './../../constant/BackUrlConstant';
import { DateDto } from './../../model/dateDto';
import { PlanningUtilisateurMateriel } from './../../model/planningUtilisateurMateriel';
import { Materiel } from './../../model/materiel';
import { FrontUrlConstant } from './../../constant/FrontUrlConstant';
import { GeometrieType } from './../../model/geometrieType';
import { Type } from './../../model/type';
import { Categorie } from './../../model/categorie';
import { GeneralService } from './../../service/general.service';
import { Geometrie } from './../../model/geometrie';
import { Table } from 'primeng/table';
import { ParamValue } from './../../model/paramValue';
import { MenuItem } from 'primeng/components/common/menuitem';
import { ToastrHelper } from './../../model/toastrHelper';
import { NbDialogService, NbToastrService, NbAccordionItemComponent } from '@nebular/theme';
import { FilterService } from './../../service/filter.service';
import { Ligne } from '../../model/ligne';
import { UniteOperationnelle } from '../../model/uniteOperationnelle';
import { Infrapole } from './../../model/infrapole';
import { Region } from './../../model/region';
import { Utilisateur } from '../../model/utilisateur';
import { UtilisateurService } from '../../service/utilisateur.service';
import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { Component, ViewChild, TemplateRef, ViewChildren, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import { Secteur } from '../../model/secteur';
import { Portion } from '../../model/portion';
import { AutoComplete } from 'primeng/autocomplete';
import { CrudHelper } from '../../model/crudHelper';
import { Router } from '@angular/router';
import { ReservationMaterielUtilisateur } from '../../model/reservationMaterielUtilisateur';
import { Calendar } from 'primeng/calendar';
import { ReservationVehiculeUtilisateur } from '../../model/reservationVehiculeUtilisateur';
import { PlanningUtilisateurVehicule } from '../../model/planningUtilisateurVehicule';

@Component({
  selector: 'vehicule',
  styleUrls: ['./vehicule.component.scss'],
  templateUrl: './vehicule.component.html',
})
export class VehiculeComponent implements OnInit, AfterViewInit {

  utilisateur: Utilisateur;

  autocompleteSuggestions: any[];
  @ViewChildren('autocomplete') autocompletes: any;

  activeDialogRef: any;

  toastrHelper: ToastrHelper;

  crudHelper: CrudHelper;
  @ViewChild('templateForEdit') cruPopupTemplate: TemplateRef<any>;
  @ViewChild('templateForDelete') deletePopupTemplate: TemplateRef<any>;

  isVehiculeLoaded: boolean = false;
  isPlanningVehiculeLoaded: boolean = false;

  selectedUser: Utilisateur;
  userSuggestions: Utilisateur[];

  @ViewChild('planningVehiculeAccordion') planningVehiculeAccordion: NbAccordionItemComponent;
  @ViewChild('reservationsVehiculeAccordion') reservationsVehiculeAccordion: NbAccordionItemComponent;

  @ViewChild('templateForConfirmation') confirmPopupTemplate: TemplateRef<any>;
  @ViewChild('templateForDeleteReservation') templateForDeleteReservation: TemplateRef<any>;
  existingResasForConfirmation: ReservationVehiculeUtilisateur[];

  selectedReservationToDelete: ReservationVehiculeUtilisateur;

  @ViewChild('planningVehiculeCalendarFrom') planningVehiculeCalendarFrom: Calendar;
  planningVehiculeCalendarFromValue: Date;
  planningVehiculeCalendarSearchFromValue: Date;
  planningVehiculeCalendarFromValueForCompare: Date;
  selectedVehiculeDateReservationsFrom: ReservationVehiculeUtilisateur[];
  dateDebutPlanningVehiculeFrom: Date;
  dateFinPlanningVehiculeFrom: Date;
  planningUtilisateurVehiculeFrom: PlanningUtilisateurVehicule;

  @ViewChild('planningVehiculeCalendarTo') planningVehiculeCalendarTo: Calendar;
  planningVehiculeCalendarToValue: Date;
  planningVehiculeCalendarSearchToValue: Date;
  planningVehiculeCalendarToValueForCompare: Date;
  selectedVehiculeDateReservationsTo: ReservationVehiculeUtilisateur[];
  dateDebutPlanningVehiculeTo: Date;
  dateFinPlanningVehiculeTo: Date;
  planningUtilisateurVehiculeTo: PlanningUtilisateurVehicule;

  @ViewChild('planningVehiculeCalendarDetails') planningVehiculeCalendarDetails: Calendar;
  planningVehiculeCalendarDetailsValue: Date;
  selectedVehiculeDateReservationsDetails: ReservationVehiculeUtilisateur[];
  dateDebutPlanningVehiculeDetails: Date;
  dateFinPlanningVehiculeDetails: Date;
  planningUtilisateurVehiculeDetails: PlanningUtilisateurVehicule;
  selectedVehiculeReservationToDelete: ReservationVehiculeUtilisateur;

  availableVehicules: Vehicule[];
  loadingAvailableVehicules: boolean = false;
  selectedVehicule: Vehicule;

  fr: any = FR_LOCALE;

  constructor(private utilisateurService: UtilisateurService,
              private filterService: FilterService,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private generalService: GeneralService,
              private router: Router) {
  }

  ngOnInit() {
    const today = new Date();
    this.planningVehiculeCalendarFromValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
    this.planningVehiculeCalendarSearchFromValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
    this.planningVehiculeCalendarFromValueForCompare = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
    this.planningVehiculeCalendarToValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 24, 0, 0);
    this.planningVehiculeCalendarSearchToValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 24, 0, 0);
    this.planningVehiculeCalendarToValueForCompare = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 24, 0, 0);
    this.dateDebutPlanningVehiculeFrom = new Date(today.getFullYear(), today.getMonth(), 1);
    this.dateFinPlanningVehiculeFrom = new Date(today.getFullYear(), today.getMonth() + 1, 0, 23, 59, 59);
    this.dateDebutPlanningVehiculeTo = new Date(today.getFullYear(), today.getMonth(), 1);
    this.dateFinPlanningVehiculeTo = new Date(today.getFullYear(), today.getMonth() + 1, 0, 23, 59, 59);
    this.dateDebutPlanningVehiculeDetails = new Date(today.getFullYear(), today.getMonth(), 1);
    this.dateFinPlanningVehiculeDetails = new Date(today.getFullYear(), today.getMonth() + 1, 0, 23, 59, 59);
    this.planningVehiculeCalendarDetailsValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
    this.utilisateur = this.utilisateurService.getUtilisateurConnected();
    this.selectedUser = this.utilisateur;
    const rows = this.utilisateur.nbLignesTableau ? parseInt(this.utilisateur.nbLignesTableau, 10) : 10;
    const crudObjectDefinitions: any = {
      Ligne: { object: new Ligne({}), table: null },
      Region: { object: new Region({}), table: null },
      Infrapole: { object: new Infrapole({}), table: null },
      Secteur: { object: new Secteur({}), table: null },
      UniteOperationnelle: { object: new UniteOperationnelle({}), table: null },
      Portion: { object: new Portion({}), table: null },
      Geometrie: { object: new Geometrie({}), table: null },
      Categorie: { object: new Categorie({}), table: null },
      Type: { object: new Type({}), table: null },
      GeometrieType: { object: new GeometrieType({}), table: null },
      Utilisateur: { object: new Utilisateur({}), table: null },
      Materiel: { object: new Materiel({}), table: null },
      ArchiveBoite: { object: new ArchiveBoite({}), table: null },
      Vehicule: { object: new Vehicule({}), table: null },
    };
    this.toastrHelper = new ToastrHelper(this.toastrService);
    this.crudHelper = new CrudHelper(this.generalService, this.dialogService, this.toastrHelper, crudObjectDefinitions, this.cruPopupTemplate, this.deletePopupTemplate, rows);
  }

  ngAfterViewInit() {
  }

  initialLoad(item: string) {
    if (item === 'Vehicule') {
      if (!this.isVehiculeLoaded) {
        this.crudHelper.load('Vehicule');
        this.isVehiculeLoaded = true;
      }
    } if (item === 'PlanningVehicule') {
      if (!this.isPlanningVehiculeLoaded) {
        this.showVehiculePlanningForUtilisateur(this.selectedUser);
        this.isPlanningVehiculeLoaded = true;
      }
    }
  }

  resolveFieldData(rowData, colField){
    return ObjectUtils.resolveFieldData(rowData, colField);
  }

  openCreateDialog(className: string) {
    this.crudHelper.openCreateDialog(className);
  }

  openEditDialog(object: any) {
    this.crudHelper.openEditDialog(object);
  }

  openDeleteDialog(object: any) {
    this.crudHelper.openDeleteDialog(object);
  }

  openDeleteReservationDialog(reservation: ReservationVehiculeUtilisateur) {
    this.selectedReservationToDelete = reservation;
    this.dialogService.open(this.templateForDeleteReservation);
  }

  SearchUser($event) {
    if ($event.query.length > 2) {
      this.utilisateurService.getUtilisateurs($event.query).subscribe(data => {
        const userSuggestions: Utilisateur[] = [];
        if (data && data.length > 0)  {
          for (const user of data) {
            userSuggestions.push(new Utilisateur(user));
          }
          this.userSuggestions = userSuggestions;
        }
      });
    }
  }

  showVehiculePlanningForUtilisateur(utlisateur: Utilisateur): void {
    const planningUtilisateurVehicule = new PlanningUtilisateurVehicule(undefined);
    planningUtilisateurVehicule.dateDebut = DateDto.createFromDate(this.dateDebutPlanningVehiculeDetails).toBackEndDto();
    planningUtilisateurVehicule.dateFin = DateDto.createFromDate(this.dateFinPlanningVehiculeDetails).toBackEndDto();
    planningUtilisateurVehicule.utilisateur = this.selectedUser;
    planningUtilisateurVehicule.trueIfListOfUsersFalseIfListOfVehicules = false;
    this.generalService.postUrlWithObject(
      BackUrlConstant.PLANNING_VEHICULE + BackUrlConstant.BY_UTILISATEUR,
      planningUtilisateurVehicule
      ).subscribe(data => {
        this.planningUtilisateurVehiculeDetails = new PlanningUtilisateurVehicule(data);
        this.showVehiculeDetailsForSelectedDate(this.planningVehiculeCalendarDetailsValue, 'Details');
    });
  }

  isVehiculeDateRangeConsistent(): boolean {
    if (!this.planningVehiculeCalendarFromValue) return true;
    if (!this.planningVehiculeCalendarToValue) return true;
    return this.planningVehiculeCalendarFromValue.getTime() !== this.planningVehiculeCalendarToValue.getTime() &&
      this.planningVehiculeCalendarFromValue < this.planningVehiculeCalendarToValue;
  }

  isVehiculePlanningAdmin(): boolean {
    return this.utilisateur.hasPermission('Admin Planning Vehicule');
  }

  availabilityVehiculeStatus(date: Date, fromTo: string): string {
    let planningUtilisateurVehicule: PlanningUtilisateurVehicule;
    if (fromTo === 'From') planningUtilisateurVehicule = this.planningUtilisateurVehiculeFrom;
    if (fromTo === 'To') planningUtilisateurVehicule = this.planningUtilisateurVehiculeTo;
    if (fromTo === 'Details') planningUtilisateurVehicule = this.planningUtilisateurVehiculeDetails;
    if (planningUtilisateurVehicule) {
      if (planningUtilisateurVehicule.reservationVehiculeUtilisateur &&
          planningUtilisateurVehicule.reservationVehiculeUtilisateur.length > 0) {
        const filtered = planningUtilisateurVehicule.reservationVehiculeUtilisateur.filter(
          rvu => rvu.getHoursTotalForDate(date) > 0
        );
        if (filtered && filtered.length > 0) {
          if (fromTo !== 'Details') {
            let totalHoursOfDay = 0;
            for (const resa of filtered) {
              totalHoursOfDay += resa.getHoursTotalForDate(date);
            }
            if (totalHoursOfDay >= 24) {
              return 'red';
            }
          }
          return 'orange';
        }
      }
    }
    return fromTo !== 'Details' ? 'green' : 'grey';
  }

  onSelectVehiculeFrom(date: Date) {
    this.availableVehicules = undefined;
    if (this.planningVehiculeCalendarFromValue.getMinutes() !== this.planningVehiculeCalendarFromValueForCompare.getMinutes()) {
      // We don't allow minutes to change
      this.planningVehiculeCalendarFromValue = new Date(this.planningVehiculeCalendarFromValueForCompare);
    } else if (this.planningVehiculeCalendarFromValue.getHours() !== this.planningVehiculeCalendarFromValueForCompare.getHours()) {
      if (this.planningVehiculeCalendarFromValue.getHours() === 23 && this.planningVehiculeCalendarFromValueForCompare.getHours() === 0) {
        const planningVehiculeCalendarValue = new Date(
          this.planningVehiculeCalendarFromValue.getFullYear(),
          this.planningVehiculeCalendarFromValue.getMonth(),
          this.planningVehiculeCalendarFromValue.getDate() - 1,
          23,
          0,
          0
        );
        this.planningVehiculeCalendarFromValue = planningVehiculeCalendarValue;
      } else if (this.planningVehiculeCalendarFromValue.getHours() === 0 && this.planningVehiculeCalendarFromValueForCompare.getHours() === 23) {
        const planningVehiculeCalendarValue = new Date(
          this.planningVehiculeCalendarFromValue.getFullYear(),
          this.planningVehiculeCalendarFromValue.getMonth(),
          this.planningVehiculeCalendarFromValue.getDate() + 1,
          0,
          0,
          0
        );
        this.planningVehiculeCalendarFromValue = planningVehiculeCalendarValue;
      }
      // We just backup the full date for next round
      this.planningVehiculeCalendarFromValueForCompare = new Date(this.planningVehiculeCalendarFromValue);
    } else {
      // We set the From and To dates to the make the default range of a full day
      const defaultVehiculeFromValue: Date = new Date(
        this.planningVehiculeCalendarFromValue.getFullYear(),
        this.planningVehiculeCalendarFromValue.getMonth(),
        this.planningVehiculeCalendarFromValue.getDate(),
        0,
        0,
        0
      );
      const defaultVehiculeToValue: Date = new Date(
        this.planningVehiculeCalendarFromValue.getFullYear(),
        this.planningVehiculeCalendarFromValue.getMonth(),
        this.planningVehiculeCalendarFromValue.getDate() + 1,
        0,
        0,
        0
      );
      this.planningVehiculeCalendarFromValue = defaultVehiculeFromValue;
      this.planningVehiculeCalendarToValue = defaultVehiculeToValue;
      // Then we just backup the full date for next round
      this.planningVehiculeCalendarFromValueForCompare = new Date(this.planningVehiculeCalendarFromValue);
    }
  }

  onSelectVehiculeTo(date: Date) {
    this.availableVehicules = undefined;
    if (this.planningVehiculeCalendarToValue.getMinutes() !== this.planningVehiculeCalendarToValueForCompare.getMinutes()) {
      // We don't allow minutes to change
      this.planningVehiculeCalendarToValue = new Date(this.planningVehiculeCalendarToValueForCompare);
    } else {
      if (this.planningVehiculeCalendarToValue.getHours() === 23 && this.planningVehiculeCalendarToValueForCompare.getHours() === 0) {
        const planningVehiculeCalendarValue = new Date(
          this.planningVehiculeCalendarToValue.getFullYear(),
          this.planningVehiculeCalendarToValue.getMonth(),
          this.planningVehiculeCalendarToValue.getDate() - 1,
          23,
          0,
          0
        );
        this.planningVehiculeCalendarToValue = planningVehiculeCalendarValue;
      } else if (this.planningVehiculeCalendarToValue.getHours() === 0 && this.planningVehiculeCalendarToValueForCompare.getHours() === 23) {
        const planningVehiculeCalendarValue = new Date(
          this.planningVehiculeCalendarToValue.getFullYear(),
          this.planningVehiculeCalendarToValue.getMonth(),
          this.planningVehiculeCalendarToValue.getDate() + 1,
          0,
          0,
          0
        );
        this.planningVehiculeCalendarToValue = planningVehiculeCalendarValue;
      }
      // We just backup the full date for next round
      this.planningVehiculeCalendarToValueForCompare = new Date(this.planningVehiculeCalendarToValue);
    }
  }

  showVehiculeDetailsForSelectedDate(date: Date, fromTo: string) {
    const dateObject = {
      year: date.getFullYear(),
      month: date.getMonth(),
      day: date.getDate()
    };
    if (fromTo === 'Details') {
      this.selectedVehiculeDateReservationsDetails = this.planningUtilisateurVehiculeDetails.reservationVehiculeUtilisateur.filter(
        rvu => rvu.getHoursTotalForDate(dateObject)
      );
    }
  }

  loadVehiculeReservationsForMonth(selectedMonth: any, fromTo: string) {
    if (fromTo === 'Details') {
      this.dateDebutPlanningVehiculeDetails = new Date(selectedMonth.year, selectedMonth.month - 1, 1, 0, 0, 0);
      this.dateFinPlanningVehiculeDetails = new Date(selectedMonth.year, selectedMonth.month, 0, 23, 59, 59);
      this.showVehiculePlanningForUtilisateur(this.selectedUser);
    }
  }

  searchVehicules() {
    this.planningVehiculeCalendarSearchFromValue = new Date(this.planningVehiculeCalendarFromValue);
    this.planningVehiculeCalendarSearchToValue = new Date(this.planningVehiculeCalendarToValue);
    const planningUtilisateurVehicule = new PlanningUtilisateurVehicule(undefined);
    planningUtilisateurVehicule.dateDebut = DateDto.createFromDate(this.planningVehiculeCalendarFromValue).toBackEndDto();
    const planningVehiculeCalendarToValue = new Date(
      this.planningVehiculeCalendarToValue.getFullYear(),
      this.planningVehiculeCalendarToValue.getMonth(),
      this.planningVehiculeCalendarToValue.getDate(),
      this.planningVehiculeCalendarToValue.getHours() - 1,
      59,
      59
    );
    planningUtilisateurVehicule.dateFin = DateDto.createFromDate(planningVehiculeCalendarToValue).toBackEndDto();
    this.generalService.postUrlWithObject(
      BackUrlConstant.VEHICULE + BackUrlConstant.BY_DATE,
      planningUtilisateurVehicule
      ).subscribe(data => {
      this.availableVehicules = [];
      if (data && data.length > 0) {
        for (const vehiculeDto of data) {
          this.availableVehicules.push(new Vehicule(vehiculeDto));
        }
      }
    });
  }

  tableResultsMatchSelectedDates(): boolean {
    return this.planningVehiculeCalendarSearchFromValue.getTime() === this.planningVehiculeCalendarFromValue.getTime() &&
            this.planningVehiculeCalendarSearchToValue.getTime() === this.planningVehiculeCalendarToValue.getTime();
  }

  bookVehicule(vehicule: Vehicule, forceUpdateIfPossible: boolean = false) {
    this.selectedVehicule = vehicule;
    const planningUtilisateurVehicule = new PlanningUtilisateurVehicule(undefined);
    const resa = new ReservationVehiculeUtilisateur(undefined);
    resa.dateDebut = DateDto.createFromDate(this.planningVehiculeCalendarFromValue).toBackEndDto();
    const planningVehiculeCalendarToValue = new Date(
      this.planningVehiculeCalendarToValue.getFullYear(),
      this.planningVehiculeCalendarToValue.getMonth(),
      this.planningVehiculeCalendarToValue.getDate(),
      this.planningVehiculeCalendarToValue.getHours() - 1,
      59,
      59
    );
    resa.dateFin = DateDto.createFromDate(planningVehiculeCalendarToValue).toBackEndDto();
    resa.utilisateur = this.selectedUser;
    resa.vehicule = vehicule;
    planningUtilisateurVehicule.reservationVehiculeUtilisateur = [ resa ];
    planningUtilisateurVehicule.forceUpdateIfPossible = forceUpdateIfPossible;
    this.generalService.postUrlWithObject(
      BackUrlConstant.PLANNING_VEHICULE,
      planningUtilisateurVehicule
      ).subscribe(data => {
      const planningUtilisateurVehiculeFrom = new PlanningUtilisateurVehicule(data);
      if (planningUtilisateurVehiculeFrom.conflictOnVehicleResponse === true) {
        this.toastrHelper.showDefaultErrorToast('Le véhicule sélectionné est déjà réservé sur la plage horaire demandée.');
      } else if (planningUtilisateurVehiculeFrom.conflictOnUserResponse === true) {
        this.existingResasForConfirmation = planningUtilisateurVehiculeFrom.reservationVehiculeUtilisateur;
        this.dialogService.open(this.confirmPopupTemplate);
      } else {
        this.availableVehicules = [];
        if (planningUtilisateurVehiculeFrom.reservationVehiculeUtilisateur && planningUtilisateurVehiculeFrom.reservationVehiculeUtilisateur.length > 0) {
          for (const resaVehicule of planningUtilisateurVehiculeFrom.reservationVehiculeUtilisateur) {
            this.availableVehicules.push(resaVehicule.vehicule);
          }
        }
        this.showVehiculePlanningForUtilisateur(this.selectedUser);
        this.selectedVehicule = undefined;
        this.planningVehiculeAccordion.close();
        this.reservationsVehiculeAccordion.open();
        this.toastrHelper.showDefaultSuccessToast();
      }
    });
  }

  confirmReservation(): void {
    this.bookVehicule(this.selectedVehicule, true);
  }

  OnSelectUser($event) {
    this.showVehiculePlanningForUtilisateur(this.selectedUser);
  }

  confirmDelete() {
    this.deleteVehiculeReservation();
  }

  deleteVehiculeReservation(): void {
    const planningUtilisateurVehicule = new PlanningUtilisateurVehicule(undefined);
    planningUtilisateurVehicule.dateDebut = DateDto.createFromDate(this.dateDebutPlanningVehiculeDetails).toBackEndDto();
    planningUtilisateurVehicule.dateFin = DateDto.createFromDate(this.dateFinPlanningVehiculeDetails).toBackEndDto();
    planningUtilisateurVehicule.utilisateur = this.selectedUser;
    planningUtilisateurVehicule.trueIfListOfUsersFalseIfListOfVehicules = false;
    planningUtilisateurVehicule.reservationVehiculeUtilisateur = [];
    planningUtilisateurVehicule.reservationVehiculeUtilisateur.push(new ReservationVehiculeUtilisateur(this.selectedReservationToDelete));
    this.generalService.deleteUrl(
      BackUrlConstant.PLANNING_VEHICULE,
      planningUtilisateurVehicule
      ).subscribe(data => {
        this.planningUtilisateurVehiculeDetails = new PlanningUtilisateurVehicule(data);
        this.showVehiculeDetailsForSelectedDate(this.planningVehiculeCalendarDetailsValue, 'Details');
        this.toastrHelper.showDefaultSuccessToast();
    });
  }

  canEditedObjectBeSaved(): boolean {
    return this.crudHelper.canEditedObjectBeSaved();
  }
}
