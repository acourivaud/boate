import { UtilisateurService } from './../service/utilisateur.service';
import { NbMenuService } from '@nebular/theme';
import { BackUrlConstant } from './../constant/BackUrlConstant';
import { ApiService } from './../service/api.service';
import { Component } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';
import { Utilisateur } from '../model/utilisateur';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-sample-layout>
      <nb-menu tag="main-menu" [(items)]="menuItems"></nb-menu>
      <router-outlet (activate)="changeOfRoutes()"></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent {

  menuItems;

  constructor (
    private apiService: ApiService,
    private menuService: NbMenuService,
    private utilisateurService: UtilisateurService
    ) {
      this.updateMenuAccordingToPermissions();
  }

  changeOfRoutes () {
    this.apiService.getUrlWOT(BackUrlConstant.USER_CONNECTED_TEST).subscribe(user => {
      if (!this.utilisateurService.checkLogin()) {
        console.log("erreur checklogin page.component");
        this.apiService.disconnectUser();
      }
    }, err => {
      console.log("erreur changeOfRoutes page.component");
      this.apiService.disconnectUser();
    });
  }

  updateMenuAccordingToPermissions() {
    const utilisateurConnected: Utilisateur = this.utilisateurService.getUtilisateurConnected();
    this.menuItems = [];
    if (utilisateurConnected) {
      this.menuItems = MENU_ITEMS;
      let menu_items: any[] = [];
      if (utilisateurConnected.hasPermission("permission1")) {
        menu_items.push({
          title: 'TEST PERMISSION 1',
          link: '/pages/iot-dashboard',
          icon: 'nb-plus',
        });
      }
      if (utilisateurConnected.hasPermission("permission2")) {
        menu_items.push({
          title: 'TEST PERMISSION 2',
          link: '/pages/iot-dashboard',
          icon: 'nb-plus',
        });
      }
      if (utilisateurConnected.hasPermission("permission3")) {
        menu_items.push({
          title: 'TEST PERMISSION 3',
          link: '/pages/iot-dashboard',
          icon: 'nb-plus',
        });
      }
      if (utilisateurConnected.hasPermission("permission4")) {
        menu_items.push({
          title: 'TEST PERMISSION 4',
          link: '/pages/iot-dashboard',
          icon: 'nb-plus',
        });
      }
      if (menu_items.length > 0) {
        this.menuService.addItems(menu_items, 'main-menu');
      }
    }
  }
}
