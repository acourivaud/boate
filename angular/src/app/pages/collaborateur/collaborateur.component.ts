import { PlanningCollaborateur } from './../../model/planningCollaborateur';
import { ReservationCollaborateur } from './../../model/reservationCollaborateur';
import { FR_LOCALE } from './../../constant/CalendarFrLocale';
import { BackUrlConstant } from './../../constant/BackUrlConstant';
import { DateDto } from './../../model/dateDto';
import { GeneralService } from './../../service/general.service';
import { ToastrHelper } from './../../model/toastrHelper';
import { NbDialogService, NbToastrService, NbAccordionItemComponent } from '@nebular/theme';
import { FilterService } from './../../service/filter.service';
import { Utilisateur } from '../../model/utilisateur';
import { UtilisateurService } from '../../service/utilisateur.service';
import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { Component, ViewChild, TemplateRef, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Calendar } from 'primeng/calendar';

@Component({
  selector: 'collaborateur',
  styleUrls: ['./collaborateur.component.scss'],
  templateUrl: './collaborateur.component.html',
})
export class CollaborateurComponent implements OnInit, AfterViewInit {

  utilisateur: Utilisateur;

  activeDialogRef: any;

  toastrHelper: ToastrHelper;

  @ViewChild('planningCollaborateurAccordion') planningCollaborateurAccordion: NbAccordionItemComponent;
  @ViewChild('reservationsCollaborateurAccordion') reservationsCollaborateurAccordion: NbAccordionItemComponent;

  @ViewChild('templateForDeleteReservation') templateForDeleteReservation: TemplateRef<any>;

  selectedReservationToDelete: ReservationCollaborateur;

  @ViewChild('planningCollaborateurCalendarFrom') planningCollaborateurCalendarFrom: Calendar;
  planningCollaborateurCalendarFromValue: Date;
  planningCollaborateurCalendarSearchFromValue: Date;
  planningCollaborateurCalendarFromValueForCompare: Date;
  selectedCollaborateurDateReservationsFrom: ReservationCollaborateur[];
  dateDebutPlanningCollaborateurFrom: Date;
  dateFinPlanningCollaborateurFrom: Date;
  planningUtilisateurCollaborateurFrom: PlanningCollaborateur;

  @ViewChild('planningCollaborateurCalendarTo') planningCollaborateurCalendarTo: Calendar;
  planningCollaborateurCalendarToValue: Date;
  planningCollaborateurCalendarSearchToValue: Date;
  planningCollaborateurCalendarToValueForCompare: Date;
  selectedCollaborateurDateReservationsTo: ReservationCollaborateur[];
  dateDebutPlanningCollaborateurTo: Date;
  dateFinPlanningCollaborateurTo: Date;
  planningUtilisateurCollaborateurTo: PlanningCollaborateur;

  nuit: Boolean = false;
	lieu: String;
  type: String;

  @ViewChild('planningCollaborateurCalendarDetails') planningCollaborateurCalendarDetails: Calendar;
  planningCollaborateurCalendarDetailsValue: Date;
  selectedCollaborateurDateReservationsDetails: ReservationCollaborateur[];
  dateDebutPlanningCollaborateurDetails: Date;
  dateFinPlanningCollaborateurDetails: Date;
  planningUtilisateurCollaborateurDetails: PlanningCollaborateur;
  selectedCollaborateurReservationToDelete: ReservationCollaborateur;

  fr: any = FR_LOCALE;

  TYPES = [
    {
      raw: undefined,
      fr: '',
    },
    {
      raw: 'Visite',
      fr: 'Visite',
    },
    {
      raw: 'Expertise',
      fr: 'Expertise',
    },
    {
      raw: 'Réunion',
      fr: 'Réunion',
    },
    {
      raw: 'Formation',
      fr: 'Formation',
    },
    {
      raw: 'Autres',
      fr: '',
    },
  ];

  constructor(private utilisateurService: UtilisateurService,
              private filterService: FilterService,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private generalService: GeneralService,
              private router: Router) {
  }

  ngOnInit() {
    const today = new Date();
    this.planningCollaborateurCalendarFromValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
    this.planningCollaborateurCalendarSearchFromValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
    this.planningCollaborateurCalendarFromValueForCompare = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
    this.planningCollaborateurCalendarToValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 24, 0, 0);
    this.planningCollaborateurCalendarSearchToValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 24, 0, 0);
    this.planningCollaborateurCalendarToValueForCompare = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 24, 0, 0);
    this.dateDebutPlanningCollaborateurFrom = new Date(today.getFullYear(), today.getMonth(), 1);
    this.dateFinPlanningCollaborateurFrom = new Date(today.getFullYear(), today.getMonth() + 1, 0, 23, 59, 59);
    this.dateDebutPlanningCollaborateurTo = new Date(today.getFullYear(), today.getMonth(), 1);
    this.dateFinPlanningCollaborateurTo = new Date(today.getFullYear(), today.getMonth() + 1, 0, 23, 59, 59);
    this.dateDebutPlanningCollaborateurDetails = new Date(today.getFullYear(), today.getMonth(), 1);
    this.dateFinPlanningCollaborateurDetails = new Date(today.getFullYear(), today.getMonth() + 1, 0, 23, 59, 59);
    this.planningCollaborateurCalendarDetailsValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
    this.utilisateur = this.utilisateurService.getUtilisateurConnected();
    this.showCollaborateurPlanningForUtilisateur(this.utilisateur);
    this.toastrHelper = new ToastrHelper(this.toastrService);
  }

  ngAfterViewInit() {
  }

  resolveFieldData(rowData, colField) {
    return ObjectUtils.resolveFieldData(rowData, colField);
  }

  openDeleteReservationDialog(reservation: ReservationCollaborateur) {
    this.selectedReservationToDelete = reservation;
    this.dialogService.open(this.templateForDeleteReservation);
  }

  showCollaborateurPlanningForUtilisateur(utlisateur: Utilisateur): void {
    const planningUtilisateurCollaborateur = new PlanningCollaborateur(undefined);
    planningUtilisateurCollaborateur.dateDebut = DateDto.createFromDate(this.dateDebutPlanningCollaborateurDetails).toBackEndDto();
    planningUtilisateurCollaborateur.dateFin = DateDto.createFromDate(this.dateFinPlanningCollaborateurDetails).toBackEndDto();
    planningUtilisateurCollaborateur.utilisateur = this.utilisateur;
    this.generalService.postUrlWithObject(
      BackUrlConstant.USER_PLANNING + '/get',
      planningUtilisateurCollaborateur
      ).subscribe(data => {
        this.planningUtilisateurCollaborateurDetails = new PlanningCollaborateur(data);
        this.showCollaborateurDetailsForSelectedDate(this.planningCollaborateurCalendarDetailsValue, 'Details');
    });
  }

  isCollaborateurDateRangeConsistent(): boolean {
    if (!this.planningCollaborateurCalendarFromValue) return true;
    if (!this.planningCollaborateurCalendarToValue) return true;
    return this.planningCollaborateurCalendarFromValue.getTime() !== this.planningCollaborateurCalendarToValue.getTime() &&
      this.planningCollaborateurCalendarFromValue < this.planningCollaborateurCalendarToValue;
  }

  isLieuTypeTravailConsistent(): boolean {
    if (this.lieu === undefined || this.lieu.trim() === '') return false;
    if (this.nuit === undefined) return false;
    if (this.type === undefined) return false;
    return true;
  }

  availabilityCollaborateurStatus(date: Date, fromTo: string): string {
    let planningUtilisateurCollaborateur: PlanningCollaborateur;
    if (fromTo === 'From') planningUtilisateurCollaborateur = this.planningUtilisateurCollaborateurFrom;
    if (fromTo === 'To') planningUtilisateurCollaborateur = this.planningUtilisateurCollaborateurTo;
    if (fromTo === 'Details') planningUtilisateurCollaborateur = this.planningUtilisateurCollaborateurDetails;
    if (planningUtilisateurCollaborateur) {
      if (planningUtilisateurCollaborateur.reservationsCollaborateur &&
          planningUtilisateurCollaborateur.reservationsCollaborateur.length > 0) {
        const filtered = planningUtilisateurCollaborateur.reservationsCollaborateur.filter(
          rvu => rvu.getHoursTotalForDate(date) > 0
        );
        if (filtered && filtered.length > 0) {
          // if (fromTo !== 'Details') {
            let totalHoursOfDay = 0;
            for (const resa of filtered) {
              totalHoursOfDay += resa.getHoursTotalForDate(date);
            }
            if (totalHoursOfDay >= 24) {
              return 'red';
            }
          // }
          return 'orange';
        }
      }
    }
    return fromTo !== 'Details' ? 'green' : 'grey';
  }

  onSelectCollaborateurFrom(date: Date) {
    if (this.planningCollaborateurCalendarFromValue.getMinutes() !== this.planningCollaborateurCalendarFromValueForCompare.getMinutes()) {
      // We don't allow minutes to change
      this.planningCollaborateurCalendarFromValue = new Date(this.planningCollaborateurCalendarFromValueForCompare);
    } else if (this.planningCollaborateurCalendarFromValue.getHours() !== this.planningCollaborateurCalendarFromValueForCompare.getHours()) {
      if (this.planningCollaborateurCalendarFromValue.getHours() === 23 && this.planningCollaborateurCalendarFromValueForCompare.getHours() === 0) {
        const planningCollaborateurCalendarValue = new Date(
          this.planningCollaborateurCalendarFromValue.getFullYear(),
          this.planningCollaborateurCalendarFromValue.getMonth(),
          this.planningCollaborateurCalendarFromValue.getDate() - 1,
          23,
          0,
          0
        );
        this.planningCollaborateurCalendarFromValue = planningCollaborateurCalendarValue;
      } else if (this.planningCollaborateurCalendarFromValue.getHours() === 0 && this.planningCollaborateurCalendarFromValueForCompare.getHours() === 23) {
        const planningCollaborateurCalendarValue = new Date(
          this.planningCollaborateurCalendarFromValue.getFullYear(),
          this.planningCollaborateurCalendarFromValue.getMonth(),
          this.planningCollaborateurCalendarFromValue.getDate() + 1,
          0,
          0,
          0
        );
        this.planningCollaborateurCalendarFromValue = planningCollaborateurCalendarValue;
      }
      // We just backup the full date for next round
      this.planningCollaborateurCalendarFromValueForCompare = new Date(this.planningCollaborateurCalendarFromValue);
    } else {
      // We set the From and To dates to the make the default range of a full day
      const defaultCollaborateurFromValue: Date = new Date(
        this.planningCollaborateurCalendarFromValue.getFullYear(),
        this.planningCollaborateurCalendarFromValue.getMonth(),
        this.planningCollaborateurCalendarFromValue.getDate(),
        0,
        0,
        0
      );
      const defaultCollaborateurToValue: Date = new Date(
        this.planningCollaborateurCalendarFromValue.getFullYear(),
        this.planningCollaborateurCalendarFromValue.getMonth(),
        this.planningCollaborateurCalendarFromValue.getDate() + 1,
        0,
        0,
        0
      );
      this.planningCollaborateurCalendarFromValue = defaultCollaborateurFromValue;
      this.planningCollaborateurCalendarToValue = defaultCollaborateurToValue;
      // Then we just backup the full date for next round
      this.planningCollaborateurCalendarFromValueForCompare = new Date(this.planningCollaborateurCalendarFromValue);
    }
  }

  onSelectCollaborateurTo(date: Date) {
    if (this.planningCollaborateurCalendarToValue.getMinutes() !== this.planningCollaborateurCalendarToValueForCompare.getMinutes()) {
      // We don't allow minutes to change
      this.planningCollaborateurCalendarToValue = new Date(this.planningCollaborateurCalendarToValueForCompare);
    } else {
      if (this.planningCollaborateurCalendarToValue.getHours() === 23 && this.planningCollaborateurCalendarToValueForCompare.getHours() === 0) {
        const planningCollaborateurCalendarValue = new Date(
          this.planningCollaborateurCalendarToValue.getFullYear(),
          this.planningCollaborateurCalendarToValue.getMonth(),
          this.planningCollaborateurCalendarToValue.getDate() - 1,
          23,
          0,
          0
        );
        this.planningCollaborateurCalendarToValue = planningCollaborateurCalendarValue;
      } else if (this.planningCollaborateurCalendarToValue.getHours() === 0 && this.planningCollaborateurCalendarToValueForCompare.getHours() === 23) {
        const planningCollaborateurCalendarValue = new Date(
          this.planningCollaborateurCalendarToValue.getFullYear(),
          this.planningCollaborateurCalendarToValue.getMonth(),
          this.planningCollaborateurCalendarToValue.getDate() + 1,
          0,
          0,
          0
        );
        this.planningCollaborateurCalendarToValue = planningCollaborateurCalendarValue;
      }
      // We just backup the full date for next round
      this.planningCollaborateurCalendarToValueForCompare = new Date(this.planningCollaborateurCalendarToValue);
    }
  }

  showCollaborateurDetailsForSelectedDate(date: Date, fromTo: string) {
    const dateObject = {
      year: date.getFullYear(),
      month: date.getMonth(),
      day: date.getDate()
    };
    if (fromTo === 'Details') {
      this.selectedCollaborateurDateReservationsDetails = this.planningUtilisateurCollaborateurDetails.reservationsCollaborateur.filter(
        rvu => rvu.getHoursTotalForDate(dateObject)
      );
    }
  }

  loadCollaborateurReservationsForMonth(selectedMonth: any, fromTo: string) {
    if (fromTo === 'Details') {
      this.dateDebutPlanningCollaborateurDetails = new Date(selectedMonth.year, selectedMonth.month - 1, 1, 0, 0, 0);
      this.dateFinPlanningCollaborateurDetails = new Date(selectedMonth.year, selectedMonth.month, 0, 23, 59, 59);
      this.showCollaborateurPlanningForUtilisateur(this.utilisateur);
    }
  }

  tableResultsMatchSelectedDates(): boolean {
    return this.planningCollaborateurCalendarSearchFromValue.getTime() === this.planningCollaborateurCalendarFromValue.getTime() &&
            this.planningCollaborateurCalendarSearchToValue.getTime() === this.planningCollaborateurCalendarToValue.getTime();
  }

  bookCollaborateur() {
    const planningUtilisateurCollaborateur = new PlanningCollaborateur(undefined);
    const resa = new ReservationCollaborateur(undefined);
    resa.dateDebut = DateDto.createFromDate(this.planningCollaborateurCalendarFromValue).toBackEndDto();
    const planningCollaborateurCalendarToValue = new Date(
      this.planningCollaborateurCalendarToValue.getFullYear(),
      this.planningCollaborateurCalendarToValue.getMonth(),
      this.planningCollaborateurCalendarToValue.getDate(),
      this.planningCollaborateurCalendarToValue.getHours() - 1,
      59,
      59
    );
    resa.dateFin = DateDto.createFromDate(planningCollaborateurCalendarToValue).toBackEndDto();
    resa.utilisateur = this.utilisateur;
    resa.lieu = this.lieu;
    resa.type = this.type;
    resa.nuit = this.nuit;
    planningUtilisateurCollaborateur.reservationsCollaborateur = [ resa ];
    this.generalService.postUrlWithObject(
      BackUrlConstant.USER_PLANNING + '/post',
      planningUtilisateurCollaborateur
      ).subscribe(data => {
      const planningUtilisateurCollaborateurFrom = new PlanningCollaborateur(data);
      if (planningUtilisateurCollaborateurFrom.conflictOnUserResponse === true) {
        this.toastrHelper.showDefaultErrorToast('Le collaborateur est déjà réservé sur la plage horaire demandée.');
      } else {
        this.showCollaborateurPlanningForUtilisateur(this.utilisateur);
        this.planningCollaborateurAccordion.close();
        this.reservationsCollaborateurAccordion.open();
        this.toastrHelper.showDefaultSuccessToast();
      }
    });
  }

  confirmReservation(): void {
    this.bookCollaborateur();
  }

  confirmDelete() {
    this.deleteCollaborateurReservation();
  }

  deleteCollaborateurReservation(): void {
    const planningUtilisateurCollaborateur = new PlanningCollaborateur(undefined);
    planningUtilisateurCollaborateur.dateDebut = DateDto.createFromDate(this.dateDebutPlanningCollaborateurDetails).toBackEndDto();
    planningUtilisateurCollaborateur.dateFin = DateDto.createFromDate(this.dateFinPlanningCollaborateurDetails).toBackEndDto();
    planningUtilisateurCollaborateur.utilisateur = this.utilisateur;
    planningUtilisateurCollaborateur.reservationsCollaborateur = [];
    planningUtilisateurCollaborateur.reservationsCollaborateur.push(new ReservationCollaborateur(this.selectedReservationToDelete));
    this.generalService.deleteUrl(
      BackUrlConstant.USER_PLANNING,
      planningUtilisateurCollaborateur
      ).subscribe(data => {
        this.planningUtilisateurCollaborateurDetails = new PlanningCollaborateur(data);
        this.showCollaborateurDetailsForSelectedDate(this.planningCollaborateurCalendarDetailsValue, 'Details');
        this.toastrHelper.showDefaultSuccessToast();
    });
  }
}
