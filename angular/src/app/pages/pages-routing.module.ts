import { PlanningComponent } from './planning/planning.component';
import { CollaborateurComponent } from './collaborateur/collaborateur.component';
import { FicheSurveillanceComponent } from './surveillance/fiche/ficheSurveillance.component';
import { FicheOuvrageLegacyComponent } from './ouvrage/fiche-legacy/ficheOuvrage.component';
import { ProductionInclinoComponent } from './bouclage/production-inclino/production-inclino.component';
import { ProductionTopoComponent } from './bouclage/production-topo/production-topo.component';
import { ProductionFsaComponent } from './bouclage/production-fsa/production-fsa.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdministrationComponent } from './administration/administration.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { OuvrageComponent } from './ouvrage/ouvrage.component';
import { PagesComponent } from './pages.component';
import { ProfilComponent } from './profil/profil.component';
import { SurveillanceComponent } from './surveillance/surveillance.component';
import { FicheOuvrageComponent } from './ouvrage/fiche/ficheOuvrage.component';
import { ArchiveComponent } from './archive/archive.component';
import { MesEmpruntsComponent } from './mes-emprunts/mes-emprunts.component';
import { VehiculeComponent } from './vehicule/vehicule.component';
import { MaterielComponent } from './materiel/materiel.component';
import { PeriodiqueComponent } from './bouclage/periodique/periodique.component';
import { ExpertiseComponent } from './bouclage/expertise/expertise.component';
import { SrSpOtComponent } from './bouclage/sr-sp-ot/sr-sp-ot.component';
import { FicheArchiveComponent } from './archive/fiche/ficheArchive.component';
import { VisiteOuvrageComponent } from './bouclage/visite-ouvrage/visite-ouvrage.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'profil',
    component: ProfilComponent,
  }, {
    path: 'ouvrage',
    component: OuvrageComponent,
  }, {
    path: 'ouvrage/fiche',
    component: FicheOuvrageComponent,
  }, {
    path: 'ouvrage/fiche/legacy',
    component: FicheOuvrageLegacyComponent,
  }, {
    path: 'archive/fiche',
    component: FicheArchiveComponent,
  }, {
    path: 'surveillance',
    component: SurveillanceComponent,
  }, {
    path: 'surveillance/fiche',
    component: FicheSurveillanceComponent,
  }, {
    path: 'vehicule',
    component: VehiculeComponent,
  }, {
    path: 'planning',
    component: PlanningComponent,
  }, {
    path: 'collaborateur',
    component: CollaborateurComponent,
  }, {
    path: 'materiel',
    component: MaterielComponent,
  }, {
    path: 'bouclage/periodique',
    component: PeriodiqueComponent,
  }, {
    path: 'bouclage/expertise',
    component: ExpertiseComponent,
  }, {
    path: 'bouclage/sr-sp-ot',
    component: SrSpOtComponent,
  }, {
    path: 'bouclage/production-fsa',
    component: ProductionFsaComponent,
  }, {
    path: 'bouclage/production-topo',
    component: ProductionTopoComponent,
  }, {
    path: 'bouclage/production-inclino',
    component: ProductionInclinoComponent,
  }, {
    path: 'bouclage/visite-ouvrage',
    component: VisiteOuvrageComponent,
  }, {
    path: 'archive',
    component: ArchiveComponent,
  }, {
    path: 'mes-emprunts',
    component: MesEmpruntsComponent,
  }, {
    path: 'administration',
    component: AdministrationComponent,
  }, {
    path: 'dashboard',
    component: ECommerceComponent,
  }, {
    path: 'iot-dashboard',
    component: DashboardComponent,
  }, {
    path: 'ui-features',
    loadChildren: './ui-features/ui-features.module#UiFeaturesModule',
  }, {
    path: 'modal-overlays',
    loadChildren: './modal-overlays/modal-overlays.module#ModalOverlaysModule',
  }, {
    path: 'extra-components',
    loadChildren: './extra-components/extra-components.module#ExtraComponentsModule',
  }, {
    path: 'maps',
    loadChildren: './maps/maps.module#MapsModule',
  }, {
    path: 'charts',
    loadChildren: './charts/charts.module#ChartsModule',
  }, {
    path: 'editors',
    loadChildren: './editors/editors.module#EditorsModule',
  }, {
    path: 'forms',
    loadChildren: './forms/forms.module#FormsModule',
  }, {
    path: 'miscellaneous',
    loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
  }, {
    path: '',
    redirectTo: 'ouvrage',
    pathMatch: 'full',
  }, {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
