import { ReservationCollaborateur } from './../../model/reservationCollaborateur';
import { PlanningCollaborateur } from './../../model/planningCollaborateur';
import { OverlayPanel } from 'primeng/primeng';
import { BackUrlConstant } from './../../constant/BackUrlConstant';
import { DateDto } from './../../model/dateDto';
import { PlanningUtilisateurMateriel } from './../../model/planningUtilisateurMateriel';
import { GeneralService } from './../../service/general.service';
import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ReservationMaterielUtilisateur } from '../../model/reservationMaterielUtilisateur';
import { ReservationVehiculeUtilisateur } from '../../model/reservationVehiculeUtilisateur';
import { PlanningUtilisateurVehicule } from '../../model/planningUtilisateurVehicule';

@Component({
  selector: 'planning',
  styleUrls: ['./planning.component.scss'],
  templateUrl: './planning.component.html',
})
export class PlanningComponent implements OnInit, AfterViewInit {

  reservationSelectionnees: ReservationMaterielUtilisateur[] | ReservationVehiculeUtilisateur[] | ReservationCollaborateur[];

  colsReservationMateriels: any[];
  colsReservationVehicules: any[];
  colsReservationCollaborateurs: any[];

  mixedPlannings: (PlanningUtilisateurMateriel | PlanningUtilisateurVehicule | PlanningCollaborateur)[];
  loadingPlannings: boolean;

  month: number;
  nomMois: string;
  year: number;
  nomAnnees: string;
  currentTime: Date;
  tabJourReference: string[];
  nomsMois: string[];
  days: any[];
  semainesColonnes: any[];
  moisColonnes: any[];

  constructor(private generalService: GeneralService) {
  }

  ngOnInit() {
    this.tabJourReference = ['DIMANCHE', 'LUNDI', 'MARDI', 'MERCREDI', 'JEUDI', 'VENDREDI', 'SAMEDI'];
    this.nomsMois = ['', 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    this.currentTime = new Date();
    // returns the month (from 0 to 11)
    this.month = this.currentTime.getMonth();
    // returns the year (four digits)
    this.year = this.currentTime.getFullYear();
    this.loadingPlannings = true;
    this.getPlanification();
  }

  ngAfterViewInit() {
  }

  resolveFieldData( rowData, colField ) {
    return ObjectUtils.resolveFieldData( rowData, colField );
  }

  enleverMois() {
    let d = new Date(this.year, this.month - 2, 1);
    this.year = d.getFullYear();
    this.month = d.getMonth() + 1;
    d = new Date(this.year, this.month, 1);
    this.year = d.getFullYear();
    this.month = d.getMonth();
    this.loadingPlannings = true;
    this.getPlanification();
  }

  ajouterMois() {
    let d = new Date(this.year, this.month, 1);
    this.year = d.getFullYear();
    this.month = d.getMonth() + 1;
    d = new Date(this.year, this.month, 1);
    this.year = d.getFullYear();
    this.month = d.getMonth();
    this.loadingPlannings = true;
    this.getPlanification();
  }

  afficherDateCourante() {
    this.month = this.currentTime.getMonth();
    this.year = this.currentTime.getFullYear();
    this.getPlanification();
  }

  getWeekNumber(d) {
    // Copy date so don't modify original
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
    // Get first day of year
    const yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
    // Calculate full weeks to nearest Thursday
    const weekNo = Math.ceil(( ( (d.getTime() - yearStart.getTime()) / 86400000) + 1) / 7);
    // Return array of year and week number
    return [d.getUTCFullYear(), weekNo];
  }

  styleImputation(day) {
    const date = new Date();
    if (date.getDate() === day.indice && date.getMonth() === this.month && date.getFullYear() === this.year) { // Current day
        return 'styleOriginal styleCurrentDate';
    } else {
        return (day.libelle === 'SAMEDI' || day.libelle === 'DIMANCHE') ? 'styleOriginal styleWeekEnd' : 'styleOriginal';
    }
  }

  getPlanification() {
    this.formatPlanning();

    const dateDebut = new Date(this.year, this.month, 1);
    const dateFin = new Date(this.year, this.month + 1, 0);

    const planningUtilisateurCollaborateur = new PlanningCollaborateur(undefined);
    planningUtilisateurCollaborateur.dateDebut = DateDto.createFromDate(dateDebut).toBackEndDto();
    planningUtilisateurCollaborateur.dateFin = DateDto.createFromDate(dateFin).toBackEndDto();

    this.generalService.postUrlWithObject(
      BackUrlConstant.USER_PLANNING + '/get/everything',
      planningUtilisateurCollaborateur
      ).subscribe(data => {

        this.mixedPlannings = [];

        if (data.collaborateurs) {
          for (const reservation of data.collaborateurs) {
            this.mixedPlannings.push(new PlanningCollaborateur(reservation));
          }
        }

        this.mixedPlannings.push(new PlanningCollaborateur( {  } ));

        if (data.vehicules) {
          for (const reservation of data.vehicules) {
            this.mixedPlannings.push(new PlanningUtilisateurVehicule(reservation));
          }
        }

        this.mixedPlannings.push(new PlanningCollaborateur( {  } ));

        if (data.materiels) {
          for (const reservation of data.materiels) {
            this.mixedPlannings.push(new PlanningUtilisateurMateriel(reservation));
          }
        }

        this.loadingPlannings = false;

        for (const planning of this.mixedPlannings) {
          for (const day of this.days) {
            planning[day['indice']] = [];
            let reservations: ReservationMaterielUtilisateur[] | ReservationVehiculeUtilisateur[] | ReservationCollaborateur[];
            if (planning instanceof PlanningCollaborateur) {
              reservations = planning.reservationsCollaborateur;
            }
            if (planning instanceof PlanningUtilisateurVehicule) {
              reservations = planning.reservationVehiculeUtilisateur;
            }
            if (planning instanceof PlanningUtilisateurMateriel) {
              reservations = planning.reservationMaterielUtilisateur;
            }
            if (reservations) {
              for (const resa of reservations) {
                if (DateDto.isSameDay(resa.dateDebut, day['date']) || DateDto.isSameDay(resa.dateFin, day['date'])) {
                  planning[day['indice']].push(resa);
                }
              }
            }
          }
        }
    });
  }

  getReservationType(rowData): string {
    if (rowData instanceof PlanningCollaborateur) return 'PlanningCollaborateur';
    if (rowData instanceof PlanningUtilisateurVehicule) return 'PlanningUtilisateurVehicule';
    if (rowData instanceof PlanningUtilisateurMateriel) return 'PlanningUtilisateurMateriel';
    return '';
  }

  formatPlanning() {
    this.nomMois = this.nomsMois[this.month + 1];
    this.nomAnnees = this.year.toString();

    this.colsReservationMateriels = [];
    this.colsReservationCollaborateurs = [];
    this.colsReservationVehicules = [];

    this.days = [];
    this.semainesColonnes = [];
    this.moisColonnes = [];

    const nbJours = new Date(this.year, this.month + 1, 0).getDate();

    for (let _i = 1; _i <= nbJours; _i++) {
      const d = new Date(this.year, this.month, _i);
      const week = this.getWeekNumber(d)[1];
      if (this.semainesColonnes.find(gc => gc['week'] === week)) {
        this.semainesColonnes.find(gc => gc['week'] === week)['group'] += 1;
      } else {
        this.semainesColonnes.push( { week: week, group: 1 } );
      }
      if (this.moisColonnes.find(gc => gc['month'] === this.nomsMois[d.getMonth() + 1])) {
        this.moisColonnes.find(gc => gc['month'] === this.nomsMois[d.getMonth() + 1])['group'] += 1;
      } else {
        this.moisColonnes.push( { month: this.nomsMois[d.getMonth() + 1], group: 1 } );
      }
      this.days.push({ indice: _i, jour: d.getDate(), mois: d.getMonth() + 1, libelle: this.tabJourReference[d.getDay()], date: d });
    }

    for (let i = 0 ; i < this.days.length ; i++) {
      const col = {
        indice: this.days[i]['indice'],
        jour: this.days[i]['jour'],
        mois: this.days[i]['mois'],
        libelle: this.days[i]['libelle'],
        week: this.days[i]['week'],
        date: this.days[i]['date']
      };
      this.colsReservationMateriels.push(col);
      this.colsReservationCollaborateurs.push(col);
      this.colsReservationVehicules.push(col);
    }
  }

  afficherOverlay(event, rowData, op: OverlayPanel, col) {
    this.reservationSelectionnees = rowData[col.indice];
    op.toggle(event);
  }
}
