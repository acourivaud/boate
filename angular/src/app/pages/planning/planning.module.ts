import { NgModule } from '@angular/core';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { CheckboxModule } from 'primeng/checkbox';
import { SliderModule } from 'primeng/slider';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { CalendarModule } from 'primeng/calendar';

import { ThemeModule } from '../../@theme/theme.module';
import { GeneralService } from '../../service/general.service';
import { UtilisateurService } from '../../service/utilisateur.service';
import { PlanningComponent } from './planning.component';
import { OverlayPanelModule } from 'primeng/primeng';

@NgModule({
  imports: [
    ThemeModule,
    TableModule,
    CheckboxModule,
    BreadcrumbModule,
    TooltipModule,
    AutoCompleteModule,
    SliderModule,
    CalendarModule,
    OverlayPanelModule
  ],
  declarations: [
    PlanningComponent,
  ],
  providers: [
    UtilisateurService,
    GeneralService
  ]
})
export class PlanningModule { }
