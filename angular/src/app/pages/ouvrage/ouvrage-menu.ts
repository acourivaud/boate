export const MENU_ITEMS =  {
  BOOLEAN: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'true',
      fr: 'Oui',
    },
    {
      raw: 'false',
      fr: 'Non',
    },
  ],
  SITUATION: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'situation_up_left',
      fr: '>G',
    },
    {
      raw: 'situation_left',
      fr: 'G',
    },
    {
      raw: 'situation_down_left',
      fr: '<G',
    },
    {
      raw: 'situation_up_right',
      fr: '>D',
    },
    {
      raw: 'situation_right',
      fr: 'D',
    },
    {
      raw: 'situation_down_right',
      fr: '<D',
    },
    {
      raw: 'situation_up',
      fr: '>',
    },
    {
      raw: 'situation_down',
      fr: '<',
    },
  ],
  NIVEAU_ETAT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'Non défini',
      fr: 'Non défini',
    },
    {
      raw: 'N1',
      fr: 'N1',
    },
    {
      raw: 'N2',
      fr: 'N2',
    },
    {
      raw: 'N3',
      fr: 'N3',
    },
    {
      raw: 'N4',
      fr: 'N4',
    },
    {
      raw: 'N5',
      fr: 'N5',
    },
  ],
  NIVEAU_GRAVITE: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'Non défini',
      fr: 'Non défini',
    },
    {
      raw: 'G1',
      fr: 'G1',
    },
    {
      raw: 'G2',
      fr: 'G2',
    },
    {
      raw: 'G3',
      fr: 'G3',
    },
    {
      raw: 'G4',
      fr: 'G4',
    },
    {
      raw: 'G5',
      fr: 'G5',
    },
  ],
  FSA: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'fsa_oui',
      fr: 'Oui',
    },
    {
      raw: 'fsa_special_exposed',
      fr: 'Particulièrement Exposé',
    },
    {
      raw: 'fsa_none',
      fr: 'Non',
    },
  ],
  RATTACHEMENT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'rattachement_drainage',
      fr: 'Drainage',
    },
    {
      raw: 'rattachement_mur',
      fr: 'Mur, perré, digue...',
    },
    {
      raw: 'rattachement_ot',
      fr: 'OT',
    },
    {
      raw: 'rattachement_souterrain',
      fr: 'Ouvrage Souterrain',
    },
    {
      raw: 'rattachement_petit',
      fr: 'Petit Ouvrage',
    },
    {
      raw: 'rattachement_inferieur',
      fr: 'Pont-Rail',
    },
    {
      raw: 'rattachement_superieur',
      fr: 'Pont Supérieur',
    },
    {
      raw: 'rattachement_titre3',
      fr: 'Titre 3',
    },
    {
      raw: 'rattachement_sou_voie',
      fr: 'Traversée / Tiers',
    },
  ],
  STATUT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'A',
      fr: 'Actif',
    },
    {
      raw: 'I',
      fr: 'Inactif',
    },
  ],
  UIC: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'uic_3',
      fr: '3',
    },
    {
      raw: 'uic_4',
      fr: '4',
    },
    {
      raw: 'uic_5',
      fr: '5',
    },
    {
      raw: 'uic_6',
      fr: '6',
    },
    {
      raw: 'uic_6AV',
      fr: '6AV',
    },
    {
      raw: 'uic_7AV',
      fr: '7AV',
    },
    {
      raw: 'uic_7SV',
      fr: '7SV',
    },
    {
      raw: 'uic_8AV',
      fr: '8AV',
    },
    {
      raw: 'uic_8SV',
      fr: '8SV',
    },
    {
      raw: 'uic_9AV',
      fr: '9AV',
    },
    {
      raw: 'uic_9SV',
      fr: '9SV',
    },
    {
      raw: 'uic_emb',
      fr: 'Emb',
    },
    {
      raw: 'uic_F',
      fr: 'F',
    },
    {
      raw: 'uic_neut',
      fr: 'Neut',
    },
    {
      raw: 'uic_none',
      fr: 'Sans',
    },
  ],
  WORK_CATEGORY: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'category_Autre',
      fr: 'Autres',
    },
    {
      raw: 'category_Bat',
      fr: 'Bâtiment Non Courant',
    },
    {
      raw: 'category_OA',
      fr: 'Ouvrage d\'Art',
    },
    {
      raw: 'category_OT',
      fr: 'Ouvrage en Terre',
    },
    {
      raw: 'category_Hydro',
      fr: 'Ouvrage Hydraulique',
    },
  ],
  WORK_TYPE_CATEGORY_NULL: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_aqc',
      fr: 'Aqueduc',
    },
    {
      raw: 'work_type_aqc_buse',
      fr: 'Aqueduc busé',
    },
    {
      raw: 'work_type_autre',
      fr: 'Autres',
    },
    {
      raw: 'work_type_bg',
      fr: 'Barrière grillagée',
    },
    {
      raw: 'work_type_bnc',
      fr: 'Bâtiment Non Courant',
    },
    {
      raw: 'work_type_bse',
      fr: 'Buse',
    },
    {
      raw: 'work_type_dlt',
      fr: 'Dalot',
    },
    {
      raw: 'work_type_deblai',
      fr: 'Déblai meuble',
    },
    {
      raw: 'work_type_depot',
      fr: 'Dépôt',
    },
    {
      raw: 'work_type_digoa',
      fr: 'Digue OA',
    },
    {
      raw: 'work_type_digot',
      fr: 'Digue OT',
    },
    {
      raw: 'work_type_dcp',
      fr: 'Dispositif Confortatif Parois',
    },
    {
      raw: 'work_type_divers',
      fr: 'Divers',
    },
    {
      raw: 'work_type_eab',
      fr: 'Ecran anti bruit',
    },
    {
      raw: 'work_type_efpb',
      fr: 'Ecran de filet pare-blocs',
    },
    {
      raw: 'work_type_gal',
      fr: 'Galerie',
    },
    {
      raw: 'work_type_gp',
      fr: 'Galerie de protection',
    },
    {
      raw: 'work_type_gt',
      fr: 'Galerie technique',
    },
    {
      raw: 'work_type_hydro',
      fr: 'Hydraulique',
    },
    {
      raw: 'work_type_mgh',
      fr: 'Mat de grande hauteur',
    },
    {
      raw: 'work_type_ms',
      fr: 'Mur de soutènement',
    },
    {
      raw: 'work_type_mgr',
      fr: 'Mur en gradin',
    },
    {
      raw: 'work_type_pc',
      fr: 'Paroi clouée',
    },
    {
      raw: 'work_type_prev',
      fr: 'Paroi revêtue',
    },
    {
      raw: 'work_type_pr',
      fr: 'Paroi rocheuse',
    },
    {
      raw: 'work_type_pas',
      fr: 'Passerelle',
    },
    {
      raw: 'work_type_per',
      fr: 'Perré',
    },
    {
      raw: 'work_type_pcl',
      fr: 'Pont canal',
    },
    {
      raw: 'work_type_pra',
      fr: 'Pont rail',
    },
    {
      raw: 'work_type_pro',
      fr: 'Pont route',
    },
    {
      raw: 'work_type_portique',
      fr: 'Portique',
    },
    {
      raw: 'work_type_potence',
      fr: 'Potence',
    },
    {
      raw: 'work_type_mxt',
      fr: 'Profil mixte',
    },
    {
      raw: 'work_type_profil_rasant',
      fr: 'Profil rasant',
    },
    {
      raw: 'work_type_pyl',
      fr: 'Pylône',
    },
    {
      raw: 'work_type_remblai',
      fr: 'Remblai',
    },
    {
      raw: 'work_type_sdm',
      fr: 'Saut de mouton',
    },
    {
      raw: 'work_type_tete_tunnel',
      fr: 'Tête tunnel',
    },
    {
      raw: 'work_type_tranchee',
      fr: 'Talus rocheux',
    },
    {
      raw: 'work_type_tc',
      fr: 'Tranchée couverte',
    },
    {
      raw: 'work_type_td',
      fr: 'Tranchée drainante',
    },
    {
      raw: 'work_type_trm',
      fr: 'Trémie',
    },
    {
      raw: 'work_type_tun',
      fr: 'Tunnel',
    },
    {
      raw: 'work_type_versant',
      fr: 'Versant',
    },
    {
      raw: 'work_type_via',
      fr: 'Viaduc',
    },
  ],
  WORK_TYPE_CATEGORY_OTHERS: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_divers',
      fr: 'Divers',
    },
    {
      raw: 'work_type_eab',
      fr: 'Ecran anti bruit',
    },
    {
      raw: 'work_type_mgh',
      fr: 'Mat de grande hauteur',
    },
    {
      raw: 'work_type_portique',
      fr: 'Portique',
    },
    {
      raw: 'work_type_potence',
      fr: 'Potence',
    },
    {
      raw: 'work_type_pyl',
      fr: 'Pylône',
    },
  ],
  WORK_TYPE_CATEGORY_BAT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_bnc',
      fr: 'Bâtiment Non Courant',
    },
  ],
  WORK_TYPE_CATEGORY_OA: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_aqc',
      fr: 'Aqueduc',
    },
    {
      raw: 'work_type_aqc_buse',
      fr: 'Aqueduc busé',
    },
    {
      raw: 'work_type_bse',
      fr: 'Buse',
    },
    {
      raw: 'work_type_dcp',
      fr: 'Dispositif Confortatif Parois',
    },
    {
      raw: 'work_type_digoa',
      fr: 'Digue OA',
    },
    {
      raw: 'work_type_dlt',
      fr: 'Dalot',
    },
    {
      raw: 'work_type_gal',
      fr: 'Galerie',
    },
    {
      raw: 'work_type_gp',
      fr: 'Galerie de protection',
    },
    {
      raw: 'work_type_gt',
      fr: 'Galerie technique',
    },
    {
      raw: 'work_type_ms',
      fr: 'Mur de soutènement',
    },
    {
      raw: 'work_type_mgr',
      fr: 'Mur en gradin',
    },
    {
      raw: 'work_type_pas',
      fr: 'Passerelle',
    },
    {
      raw: 'work_type_pc',
      fr: 'Paroi clouée',
    },
    {
      raw: 'work_type_prev',
      fr: 'Paroi revêtue',
    },
    {
      raw: 'work_type_pcl',
      fr: 'Pont canal',
    },
    {
      raw: 'work_type_per',
      fr: 'Perré',
    },
    {
      raw: 'work_type_pra',
      fr: 'Pont rail',
    },
    {
      raw: 'work_type_pro',
      fr: 'Pont route',
    },
    {
      raw: 'work_type_sdm',
      fr: 'Saut de mouton',
    },
    {
      raw: 'work_type_tc',
      fr: 'Tranchée couverte',
    },
    {
      raw: 'work_type_td',
      fr: 'Tranchée drainante',
    },
    {
      raw: 'work_type_trm',
      fr: 'Trémie',
    },
    {
      raw: 'work_type_tun',
      fr: 'Tunnel',
    },
    {
      raw: 'work_type_via',
      fr: 'Viaduc',
    },
  ],
  WORK_TYPE_CATEGORY_OT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_bg',
      fr: 'Barrière grillagée',
    },
    {
      raw: 'work_type_deblai',
      fr: 'Déblai meuble',
    },
    {
      raw: 'work_type_efpb',
      fr: 'Ecran de filet pare-blocs',
    },
    {
      raw: 'work_type_mxt',
      fr: 'Profil mixte',
    },
    {
      raw: 'work_type_prev',
      fr: 'Paroi revêtue',
    },
    {
      raw: 'work_type_profil_rasant',
      fr: 'Profil rasant',
    },
    {
      raw: 'work_type_remblai',
      fr: 'Remblai',
    },
    {
      raw: 'work_type_tranchee',
      fr: 'Talus rocheux',
    },
    {
      raw: 'work_type_versant',
      fr: 'Versant',
    },
  ],
  WORK_TYPE_CATEGORY_HYDRO: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_hydro',
      fr: 'Hydraulique',
    },
  ],
  ASSEMBLY: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'assembly_Boul',
      fr: 'Boulonée',
    },
    {
      raw: 'assembly_Riv',
      fr: 'Riveté',
    },
    {
      raw: 'assembly_Sou',
      fr: 'Soudé',
    },
  ],
  NATURE: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'meuble',
      fr: 'Meuble',
    },
    {
      raw: 'rocheux',
      fr: 'Rocheux',
    },
    {
      raw: 'meuble_rocheux',
      fr: 'Meuble et rocheux',
    },
  ],
  OCCUPATION: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'occupation_sncf_infra',
      fr: 'SNCF infra',
    },
    {
      raw: 'occupation_sncf_other',
      fr: 'SNCF autres',
    },
    {
      raw: 'occupation_tiers',
      fr: 'Tiers',
    },
    {
      raw: 'occupation_transports_Robineau',
      fr: 'Transports robineau',
    },
    {
      raw: 'occupation_stockage_v',
      fr: 'Stockage V',
    },
    {
      raw: 'occupation_materiel_v',
      fr: 'Materiel V',
    },
    {
      raw: 'occupation_emt_vaise',
      fr: 'EMT Vaise',
    },
    {
      raw: 'occupation_stockage_materiaux_demolition',
      fr: 'Stockage matériaux démolition',
    },
    {
      raw: 'occupation_parking',
      fr: 'Parking',
    },
    {
      raw: 'occupation_stockage_service_electrique',
      fr: 'Stockage service électrique',
    },
    {
      raw: 'occupation_emr',
      fr: 'EMR',
    },
    {
      raw: 'occupation_inoccupe',
      fr: 'Inoccupé',
    },
    {
      raw: 'occupation_evlog_+_upses',
      fr: 'EVLOG + UPSES',
    },
    {
      raw: 'occupation_crem',
      fr: 'CREM',
    },
    {
      raw: 'occupation_depot',
      fr: 'Dépot',
    },
  ],
  OWNER_TYPE: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'sncf',
      fr: 'SNCF',
    },
    {
      raw: 'autres',
      fr: 'Autres',
    },
  ],
  GEOMETRY_NULL: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_circ',
      fr: 'Circ',
    },
    {
      raw: 'geometry_cyl',
      fr: 'Cylindrique',
    },
    {
      raw: 'geometry_dba',
      fr: 'Dalle BA',
    },
    {
      raw: 'geometry_ov',
      fr: 'Ovoïde',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
    {
      raw: 'geometry_qua',
      fr: 'Quadripode',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_tripode',
      fr: 'Tripode',
    },
  ],
  GEOMETRY_work_type_aqc: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_dba',
      fr: 'Dalle BA',
    },
  ],
  GEOMETRY_work_type_aqc_buse: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_dba',
      fr: 'Dalle BA',
    },
  ],
  GEOMETRY_work_type_autre: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_bg: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_bnc: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_bse: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_circ',
      fr: 'Circ',
    },
    {
      raw: 'geometry_ov',
      fr: 'Ovoïde',
    },
  ],
  GEOMETRY_work_type_dcp: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_deblai: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_depot: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_digoa: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_digot: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_divers: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_tripode',
      fr: 'Tripode',
    },
  ],
  GEOMETRY_work_type_dlt: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_eab: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_tripode',
      fr: 'Tripode',
    },
  ],
  GEOMETRY_work_type_efpb: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_gal: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
  ],
  GEOMETRY_work_type_gp: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
  ],
  GEOMETRY_work_type_gt: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
  ],
  GEOMETRY_work_type_hydro: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_mgh: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cyl',
      fr: 'Cylindrique',
    },
    {
      raw: 'geometry_qua',
      fr: 'Quadripode',
    },
    {
      raw: 'geometry_tripode',
      fr: 'Tripode',
    },
  ],
  GEOMETRY_work_type_mgr: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_ms: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_mxt: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_pas: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
  ],
  GEOMETRY_work_type_pc: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_pcl: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
  ],
  GEOMETRY_work_type_per: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_portique: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_tripode',
      fr: 'Tripode',
    },
  ],
  GEOMETRY_work_type_potence: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_pr: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_pra: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_dba',
      fr: 'Dalle BA',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
  ],
  GEOMETRY_work_type_prev: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_pro: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_dba',
      fr: 'Dalle BA',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
  ],
  GEOMETRY_work_type_profil_rasant: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_pyl: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cyl',
      fr: 'Cylindrique',
    },
    {
      raw: 'geometry_qua',
      fr: 'Quadripode',
    },
    {
      raw: 'geometry_tripode',
      fr: 'Tripode',
    },
  ],
  GEOMETRY_work_type_remblai: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_sdm: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
    {
      raw: 'geometry_dba',
      fr: 'Dalle BA',
    },
  ],
  GEOMETRY_work_type_tc: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
  ],
  GEOMETRY_work_type_td: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
  ],
  GEOMETRY_work_type_tete_tunnel: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_tranchee: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_trm: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_tun: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
  ],
  GEOMETRY_work_type_versant: [
    {
      raw: null,
      fr: '',
    },
  ],
  GEOMETRY_work_type_via: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'geometry_cdr',
      fr: 'Cadre',
    },
    {
      raw: 'geometry_srb',
      fr: 'Surbaissé',
    },
    {
      raw: 'geometry_pc',
      fr: 'Plein cintre',
    },
    {
      raw: 'geometry_psd',
      fr: 'Poutre sous dalle',
    },
    {
      raw: 'geometry_prt',
      fr: 'Portique',
    },
  ],
};
