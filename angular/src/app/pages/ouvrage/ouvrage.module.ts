import { SliderModule } from 'primeng/slider';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CalendarModule } from 'primeng/calendar';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { FicheOuvrageLegacyComponent } from './fiche-legacy/ficheOuvrage.component';
import { NgModule } from '@angular/core';

import { GeneralService } from '../../service/general.service';
import { ParamService } from '../../service/param.service';
import { ProprietaireService } from '../../service/proprietaire.service';
import { PageDefinitionModule } from '../ui-features/page-definition/page-definition.module';
import { ColumnService } from './../../service/column.service';
import { FilterService } from './../../service/filter.service';
import { OuvrageService } from './../../service/ouvrage.service';
import { UtilisateurService } from './../../service/utilisateur.service';
import { FicheOuvrageComponent } from './fiche/ficheOuvrage.component';
import { OuvrageComponent } from './ouvrage.component';

@NgModule({
  imports: [
    PageDefinitionModule,
    ToggleButtonModule,
    CalendarModule,
    InputTextareaModule,
    SliderModule
  ],
  declarations: [
    OuvrageComponent,
    FicheOuvrageComponent,
    FicheOuvrageLegacyComponent
  ],
  providers: [
    UtilisateurService,
    OuvrageService,
    FilterService,
    ColumnService,
    ProprietaireService,
    GeneralService,
    ParamService,
  ],
})
export class OuvrageModule { }
