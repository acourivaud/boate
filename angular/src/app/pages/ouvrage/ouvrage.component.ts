import { ExportExcelHelper } from './../../model/exportExcelHelper';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { ColumnService } from './../../service/column.service';
import { ColumnHelper } from './../../model/columnHelper';
import { Utilisateur } from './../../model/utilisateur';
import { UtilisateurService } from './../../service/utilisateur.service';
import { Proprietaire } from './../../model/proprietaire';
import { Region } from './../../model/region';
import { FilterService } from './../../service/filter.service';
import { FilterHelper } from '../../model/filterHelper';
import { MENU_ITEMS } from './ouvrage-menu';
import { ParamValue } from './../../model/paramValue';
import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { OuvrageService } from './../../service/ouvrage.service';
import { Component, ViewChild } from '@angular/core';
import { Ouvrage } from '../../model/ouvrage';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { FrontUrlConstant } from '../../constant/FrontUrlConstant';
import { LocalStorageService } from '../../service/localStorage.service';
import { NbToastrService } from '@nebular/theme';
import { ToastrHelper } from '../../model/toastrHelper';

@Component({
  selector: 'ouvrage',
  styleUrls: ['./ouvrage.component.scss'],
  templateUrl: './ouvrage.component.html',
})
export class OuvrageComponent {

  utilisateur: Utilisateur;

  ouvrages: Ouvrage[];
  totalRecords: number;
  loading: boolean;
  exportLoading: boolean;
  inputFilter: RegExp = /^[0-9/./,]$/;
  selectedValue: Ouvrage;

  @ViewChild('accordionRechercher') accordionRechercher;
  @ViewChild('accordionOA') accordionOA;
  @ViewChild('accordionOT') accordionOT;
  @ViewChild('accordionBNC') accordionBNC;
  @ViewChild('accordionAO') accordionAO;

  @ViewChild('ouvrageTable') ouvrageTable: Table;

  activeFilters: FilterHelper;
  columnHelper: ColumnHelper;

  pendingBackendResponses: number;

  toastrHelper: ToastrHelper;

  constructor(private utilisateurService: UtilisateurService,
              private ouvrageService: OuvrageService,
              private filterService: FilterService,
              private columnService: ColumnService,
              private router: Router,
              private localStorageService: LocalStorageService,
              private toastrService: NbToastrService) {

    this.utilisateur = utilisateurService.getUtilisateurConnected();

    let useFiltersForRequestsArg = this;
    let useFiltersForRequestsCallback = (arg: any) : void => {

      let labels: string[] = [];

      if (arg.activeFilters.filters.GenRegionSelected != null && arg.activeFilters.filters.GenRegionSelected.raw != null && arg.activeFilters.options.GenSecteurOptions != null && arg.activeFilters.options.GenSecteurOptions.length < 2 ) {
        labels.push('secteurOptionsEmpty');
      } else {
        labels.push('secteurOptionsNotEmpty');
      }

      if (arg.activeFilters.filters.GenCategorieSelected == null || arg.activeFilters.filters.GenCategorieSelected.raw == null ) {
        labels.push('category_null');
      } else {
        labels.push(arg.activeFilters.filters.GenCategorieSelected.raw);
      }

      arg.columnHelper.adaptConditionalColumnsToRequest(labels);
      arg.columnHelper.applyColumnsOrder();

    };
    this.activeFilters = new FilterHelper(filterService, 'ouvrage', useFiltersForRequestsCallback, useFiltersForRequestsArg);

    if (this.utilisateur.nbLignesTableau) {
      this.activeFilters.rows = Number.parseInt(this.utilisateur.nbLignesTableau);
    }

    this.activeFilters.options = {
      // Généralité
      GenRegionOptions: null,
      GenInfrapoleOptions: null,
      GenSecteurOptions: null,
      GenUoOptions: null,
      GenUicOptions: MENU_ITEMS.UIC,
      GenLigneOptions: null,
      GenCategorieOptions: MENU_ITEMS.WORK_CATEGORY,
      GenTypeOptions: MENU_ITEMS.WORK_TYPE_CATEGORY_NULL,

      // Description
      DesOaGeometrieOptions: MENU_ITEMS.GEOMETRY_NULL,
      DesOaAssemblageOptions: MENU_ITEMS.ASSEMBLY,
      DesOtNatureOptions: MENU_ITEMS.NATURE,
      DesBncOccupationOptions: MENU_ITEMS.OCCUPATION,
      DesBncBatPublicOptions: MENU_ITEMS.BOOLEAN,
      DesBncVoieOptions: MENU_ITEMS.BOOLEAN,
      DesBncAssemblageOptions: MENU_ITEMS.ASSEMBLY,
      DesAoGeometrieOptions: MENU_ITEMS.GEOMETRY_NULL,
      DesAoAssemblageOptions: MENU_ITEMS.ASSEMBLY,

      // Situation
      SitDepartementOptions: null,
      SitCommuneOptions: null,
      SitOuvrageEnveloppeOptions: MENU_ITEMS.BOOLEAN,
      SitPositionOptions: MENU_ITEMS.SITUATION,
      SitLocalisationOptions: MENU_ITEMS.BOOLEAN,
      SitPhotoOptions: MENU_ITEMS.BOOLEAN,
      SitNoteEtatOptions: MENU_ITEMS.NIVEAU_ETAT,
      SitGraviteOptions: MENU_ITEMS.NIVEAU_GRAVITE,

      // Particularités
      ParNomOptions: null,
      ParFsaOptions: MENU_ITEMS.FSA,
      ParFamilleOptions: MENU_ITEMS.RATTACHEMENT,
      ParPlanOrsecOptions: MENU_ITEMS.BOOLEAN,
      ParMonumentHistOptions: MENU_ITEMS.BOOLEAN,
      ParSiteProtegeOptions: MENU_ITEMS.BOOLEAN,
      ParEmbranchementOptions: MENU_ITEMS.BOOLEAN,
      ParRisqueNaturelOptions: MENU_ITEMS.BOOLEAN,
      ParRisqueSismiqueOptions: MENU_ITEMS.BOOLEAN,
      ParRisqueTechnoOptions: MENU_ITEMS.BOOLEAN,
      ParRisqueRoutierOptions: MENU_ITEMS.BOOLEAN,
      ParRisqueFluvialOptions: MENU_ITEMS.BOOLEAN,
      ParTypeProprietaireOptions: MENU_ITEMS.OWNER_TYPE,
      ParProprietaireOptions: null,
      ParDonneesGeotechOptions: MENU_ITEMS.BOOLEAN,
      ParFicheModifieeOptions: MENU_ITEMS.BOOLEAN,
      ParRestrictionOptions: MENU_ITEMS.BOOLEAN,
      ParConventionOptions: MENU_ITEMS.BOOLEAN,
      ParStatutOptions: MENU_ITEMS.STATUT,
    };

    this.activeFilters.filters = {
      // ATTENTION LES SUFFIXES ONT UN IMPACT SUR LE PROGRAMME
      // (Selected, SelectedSearch, Typed, TypedSearch)

      // Généralité
      GenRegionSelected: null,
      GenInfrapoleSelected: null,
      GenSecteurSelected: null,
      GenUoSelected: null,
      GenUicSelected: null,
      GenLigneSelected: null,
      GenFromTyped: null,
      GenToTyped: null,
      GenCategorieSelected: null,
      GenTypeSelected: null,

      // Description
      DesOaLongueurFromTyped: null,
      DesOaLongueurToTyped: null,
      DesOaHauteurFromTyped: null,
      DesOaHauteurToTyped: null,
      DesOaHauteurLibreFromTyped: null,
      DesOaHauteurLibreToTyped: null,
      DesOaLargeurFromTyped: null,
      DesOaLargeurToTyped: null,
      DesOaOuvertureFromTyped: null,
      DesOaOuvertureToTyped: null,
      DesOaPorteeFromTyped: null,
      DesOaPorteeToTyped: null,
      DesOaCouvertureFromTyped: null,
      DesOaCouvertureToTyped: null,
      DesOaTraverseesFromTyped: null,
      DesOaTraverseesToTyped: null,
      DesOaTabliersFromTyped: null,
      DesOaTabliersToTyped: null,
      DesOaPenteFromTyped: null,
      DesOaPenteToTyped: null,
      DesOaPenteGaucheFromTyped: null,
      DesOaPenteGaucheToTyped: null,
      DesOaPenteDroiteFromTyped: null,
      DesOaPenteDroiteToTyped: null,
      DesOaGeometrieSelected: null,
      DesOaAssemblageSelected: null,
      DesOtHauteurFromTyped: null,
      DesOtHauteurToTyped: null,
      DesOtHauteurGaucheFromTyped: null,
      DesOtHauteurGaucheToTyped: null,
      DesOtHauteurDroiteFromTyped: null,
      DesOtHauteurDroiteToTyped: null,
      DesOtProfondeurFromTyped: null,
      DesOtProfondeurToTyped: null,
      DesOtProfondeurGaucheFromTyped: null,
      DesOtProfondeurGaucheToTyped: null,
      DesOtProfondeurDroiteFromTyped: null,
      DesOtProfondeurDroiteToTyped: null,
      DesOtPenteFromTyped: null,
      DesOtPenteToTyped: null,
      DesOtPenteGaucheFromTyped: null,
      DesOtPenteGaucheToTyped: null,
      DesOtPenteDroiteFromTyped: null,
      DesOtPenteDroiteToTyped: null,
      DesOtNatureSelected: null,
      DesBncHauteurMaxiFromTyped: null,
      DesBncHauteurMaxiToTyped: null,
      DesBncPorteeFromTyped: null,
      DesBncPorteeToTyped: null,
      DesBncFermesFromTyped: null,
      DesBncFermesToTyped: null,
      DesBncTraveesFromTyped: null,
      DesBncTraveesToTyped: null,
      DesBncOccupationSelected: null,
      DesBncBatPublicSelected: null,
      DesBncVoieSelected: null,
      DesBncAssemblageSelected: null,
      DesAoGeometrieSelected: null,
      DesAoAssemblageSelected: null,

      // Situation
      SitDepartementSelected: null,
      SitCommuneSelectedSearch: null,
      SitOuvrageEnveloppeSelected: null,
      SitPositionSelected: null,
      SitLocalisationSelected: null,
      SitPhotoSelected: null,
      SitNoteEtatSelected: null,
      SitGraviteSelected: null,

      // Particularités
      ParNomTypedSearch: null,
      ParFsaSelected: null,
      ParFamilleSelected: null,
      ParPlanOrsecSelected: null,
      ParMonumentHistSelected: null,
      ParSiteProtegeSelected: null,
      ParEmbranchementSelected: null,
      ParRisqueNaturelSelected: null,
      ParRisqueSismiqueSelected: null,
      ParRisqueTechnoSelected: null,
      ParRisqueRoutierSelected: null,
      ParRisqueFluvialSelected: null,
      ParAnneeConstructionFromTyped: null,
      ParAnneeConstructionToTyped: null,
      ParBiaisFromTyped: null,
      ParBiaisToTyped: null,
      ParTypeProprietaireSelected: null,
      ParProprietaireSelected: null,
      ParDonneesGeotechSelected: null,
      ParFicheModifieeSelected: null,
      ParRestrictionSelected: null,
      ParConventionSelected: null,
      ParStatutSelected: null,
    };

    this.loading = true;

    let conditionalCols = [
      { conditions: 'secteurOptionsEmpty', col: { field: 'up.infrapole.code', header: 'Infrapole', style: 'column-center column-width-150' } },
      { conditions: 'secteurOptionsNotEmpty', col: { field: 'up.secteur.infrapole.code', header: 'Infrapole', style: 'column-center column-width-150' } },
      { conditions: null, col: { field: 'up.code', header: 'UP', style: 'column-center column-width-100' } },
      { conditions: null, col: { field: 'line.code', header: 'Ligne', style: 'column-center column-width-100' } },
      { conditions: null, col: { field: 'pk', header: 'PK', style: 'column-center column-width-100' } },
      { conditions: null, col: { field: 'middlePk', header: 'PK Milieu', style: 'column-center column-width-100' } },
      { conditions: null, col: { field: 'endPk', header: 'PK Fin', style: 'column-center column-width-100' } },
      { conditions: 'category_OT', col: { field: 'ouvragePere.pk', header: 'Pk Enveloppe', style: 'column-center column-width-150' } },
      { conditions: 'category_OT', col: { field: 'ouvragePere.endPk', header: 'Pk Fin Enveloppe', style: 'column-center column-width-150' } },
      { conditions: 'category_null|category_Autre|category_Bat|category_OA|category_Hydro', col: { field: 'categorie', header: 'Catégorie', style: 'column-center column-width-150' } },
      { conditions: 'category_null|category_Autre|category_Bat|category_OA|category_Hydro', col: { field: 'typeOuvrage.nomBoate', header: 'Type Ouvrage', style: 'column-center column-width-200' } },
      { conditions: 'category_OT', col: { field: 'typeOuvrage.nomPatrimoine', header: 'Type Patrimoine', style: 'column-center column-width-150' } },
      { conditions: 'category_OT', col: { field: 'typeOuvrage.nomBoate', header: 'Type BOATE', style: 'column-center column-width-150' } },
      { conditions: 'category_OT', col: { field: 'typeOts', header: 'Type OTS', style: 'column-center column-width-100' } },
      { conditions: 'category_OT', col: { field: 'sousCategorieOts', header: 'Sous-catégorie OTS', style: 'column-center column-width-150' } },
      { conditions: null, col: { field: 'name', header: 'Nom', style: 'column-width-300' } },
      { conditions: 'category_OT', col: { field: 'otDateIncident', header: 'Date Incident', style: 'column-center column-width-100' } },
      { conditions: 'category_OT', col: { field: 'otAvarie', header: 'Avaries', style: 'column-center column-width-100' } },
      { conditions: 'category_OT', col: { field: 'otTypeConfortement', header: 'Confortement', style: 'column-center column-width-150' } },
      { conditions: 'category_OT', col: { field: 'otConfortementDesc', header: 'Confortement Description', style: 'column-center column-width-150' } },
      { conditions: 'category_OT', col: { field: 'otFicheSignal', header: 'Fiche Signalétique', style: 'column-center column-width-150' } },
      { conditions: 'category_OT', col: { field: 'otFosse', header: 'Fossé', style: 'column-center column-width-100' } },
      { conditions: 'category_null|category_Autre|category_Bat|category_OA|category_Hydro', col: { field: 'indice', header: 'Indice', style: 'column-center column-width-100' } },
      { conditions: 'category_null|category_Autre|category_Bat|category_OA|category_Hydro', col: { field: 'nature', header: 'Nature', style: 'column-center column-width-200' } },
      { conditions: 'category_null|category_Autre|category_Bat|category_OA|category_Hydro', col: { field: 'geometrie', header: 'Géométrie', style: 'column-center column-width-150' } },
      { conditions: 'category_null|category_Autre|category_Bat|category_OA|category_Hydro', col: { field: 'situation', header: 'Situation', style: 'column-center column-width-150' } },
      { conditions: 'category_null|category_Autre|category_Bat|category_OA|category_Hydro', col: { field: 'oaOuverture', header: 'Ouverture', style: 'column-center column-width-150' } },
      { conditions: 'category_null|category_Autre|category_Bat|category_OA|category_Hydro', col: { field: 'couverture', header: 'Couverture', style: 'column-center column-width-150' } },
      { conditions: 'category_null|category_Autre|category_Bat|category_OA|category_Hydro', col: { field: 'longueur', header: 'Longueur', style: 'column-center column-width-150' } },
    ];

    this.columnHelper = new ColumnHelper(conditionalCols, null, columnService, 'ouvrage');

    this.toastrHelper = new ToastrHelper(this.toastrService);

    this.pendingBackendResponses = 5;

    this.filterService.getRegions().subscribe(data => {
      this.activeFilters.options.GenRegionOptions = [ { raw: null, fr: '' } ];
      for (let regionDto of data) {
        let region : Region = new Region(regionDto);
        this.activeFilters.options.GenRegionOptions.push({
          raw: region.id.toString(),
          fr: region.name
        });
      }
      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });

    this.regionInfrapoleSecteurUoChange();

    this.getDepartementsAccordingToRegion();

    this.getProprietaires(true);

    let getColumnsOrderArg = this;
    let getColumnsOrderCallback = (arg: any) : void => {
      arg.pendingBackendResponses--;
      arg.getFiltersForConnectedUser();
    };
    this.columnHelper.getColumnsOrder(getColumnsOrderCallback, getColumnsOrderArg);
  }

  getFiltersForConnectedUser() {
    // Ne fonctionne que quand tous les appels précédents ont répondu (UNE SEULE FOIS)
    if (!this.activeFilters.areConnectedUserFiltersLoaded && this.pendingBackendResponses == 0) {
      let self = this;
      let callback = (arg: any) : void => {
        arg.activeFilters.useFiltersForRequests(true);
        if (!arg.activeFilters.connectedUserFilters
          || arg.activeFilters.connectedUserFilters == null
          || arg.activeFilters.connectedUserFilters.length == 0) {
          if (arg.utilisateur.up) {
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenUo', arg.utilisateur.up ? arg.utilisateur.up.id : null);
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenSecteur', arg.utilisateur.secteur ? arg.utilisateur.secteur.id : null);
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenInfrapole', arg.utilisateur.infrapole ? arg.utilisateur.infrapole.id : null);
            arg.activeFilters.trySetSelectedFromOptionsIfPossible('GenRegion', arg.utilisateur.region ? arg.utilisateur.region.id : null);
          }
        }
        arg.columnHelper.applyColumnsOrder();
      };
      this.activeFilters.populateDefaultFiltersForConnectedUser(callback, self);
    }
  }

  loadOuvragesLazy(event: any) {
    setTimeout(() => {
      this.fetchDataFromBackEnd(event, false);
    });
  }

  fetchDataFromBackEnd(event: any, saveFilters: boolean) {
    this.loading = true;

    let requestFilters: ParamValue[] = this.activeFilters.generateRequestFilters(event, this.ouvrageTable);

    this.ouvrageService.getOuvrages(saveFilters, requestFilters).subscribe(data => {
      this.totalRecords = data.count;
      this.ouvrages = [];
      for (let ouvrageDTO of data.result) {
        this.ouvrages.push(new Ouvrage(ouvrageDTO));
      }
      this.loading = false;
    });
  }

  resolveFieldData(rowData, colField){
    return ObjectUtils.resolveFieldData(rowData, colField);
  }

  // Généralité

  GenRegionChange($event) {
    this.regionInfrapoleSecteurUoChange();
    this.getDepartementsAccordingToRegion();
    this.checkCommune();
  }

  GenInfrapoleChange($event) {
    this.regionInfrapoleSecteurUoChange();
  }

  GenSecteurChange($event) {
    this.regionInfrapoleSecteurUoChange();
  }

  GenUoChange($event) {
    this.regionInfrapoleSecteurUoChange();
  }

  regionInfrapoleSecteurUoChange() {
    this.filterService.getRegionInfrapoleSecteurUoLigne(
        this.activeFilters.getSelectedRawString('GenRegionSelected'),
        this.activeFilters.getSelectedRawString('GenInfrapoleSelected'),
        this.activeFilters.getSelectedRawString('GenSecteurSelected'),
        this.activeFilters.getSelectedRawString('GenUoSelected')
      ).subscribe(data => {

      let GenInfrapoleSelected = this.activeFilters.getSelectedRawString('GenInfrapoleSelected');
      let GenSecteurSelected = this.activeFilters.getSelectedRawString('GenSecteurSelected');
      let GenUoSelected = this.activeFilters.getSelectedRawString('GenUoSelected');
      let GenLigneSelected = this.activeFilters.getSelectedRawString('GenLigneSelected');

      let infrapoles = [ { raw: null, fr: '' } ];
      let secteurs = [ { raw: null, fr: '' } ];
      let unitesOperationnelle = [ { raw: null, fr: '' } ];
      let lignes = [ { raw: null, fr: '' } ];

      for (let infrapoleDTO of data.infrapoles) {
        infrapoles.push({ raw: infrapoleDTO.id.toString(), fr: infrapoleDTO.name });
      }

      for (let secteurDTO of data.secteurs) {
        secteurs.push({ raw: secteurDTO.id.toString(), fr: secteurDTO.nom });
      }

      for (let uniteOperationnelleDTO of data.unitesOperationnelle) {
        unitesOperationnelle.push({ raw: uniteOperationnelleDTO.id.toString(), fr: uniteOperationnelleDTO.name });
      }

      for (let ligneDTO of data.lignes) {
        lignes.push({ raw: ligneDTO.id.toString(), fr: ligneDTO.nom });
      }

      this.activeFilters.options.GenInfrapoleOptions = infrapoles;
      this.activeFilters.options.GenSecteurOptions = secteurs;
      this.activeFilters.options.GenUoOptions = unitesOperationnelle;
      this.activeFilters.options.GenLigneOptions = lignes;

      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenInfrapole', GenInfrapoleSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenSecteur', GenSecteurSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenUo', GenUoSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenLigne', GenLigneSelected);

      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });
  }

  getDepartementsAccordingToRegion() {
    this.filterService.getDepartements(
        this.activeFilters.getSelectedRawString('GenRegionSelected')
      ).subscribe(data => {

      let SitDepartementSelected = this.activeFilters.getSelectedRawString('SitDepartementSelected');

      let departements = [ { raw: null, fr: '' } ];

      for (let departementDTO of data) {
        departements.push({ raw: departementDTO.id.toString(), fr: departementDTO.nom });
      }

      this.activeFilters.options.SitDepartementOptions = departements;

      this.activeFilters.trySetSelectedFromOptionsIfPossible('SitDepartement', SitDepartementSelected);

      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });
  }

  GenCategorieChange($event) {
    if (this.activeFilters.filters['GenCategorieSelected'] != null) {
      let GenTypeSelected = this.activeFilters.getSelectedRawString('GenTypeSelected');
      let self = this;
      switch (this.activeFilters.filters['GenCategorieSelected'].raw) {
        case null :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_NULL;
          setTimeout(function() {
            self.accordionAO.close();
            self.accordionBNC.close();
            self.accordionOA.close();
            self.accordionOT.close();
          }, 100);
          break;
        case 'category_Autre' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_OTHERS;
          this.activeFilters.razAllDescriptionFilters();
          setTimeout(function() {
            self.accordionAO.open();
            self.accordionBNC.close();
            self.accordionOA.close();
            self.accordionOT.close();
          }, 100);
          break;
        case 'category_Bat' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_BAT;
          this.activeFilters.razAllDescriptionFilters();
          setTimeout(function() {
            self.accordionAO.close();
            self.accordionBNC.open();
            self.accordionOA.close();
            self.accordionOT.close();
          }, 100);
          break;
        case 'category_OA' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_OA;
          this.activeFilters.razAllDescriptionFilters();
          setTimeout(function() {
            self.accordionAO.close();
            self.accordionBNC.close();
            self.accordionOA.open();
            self.accordionOT.close();
          }, 100);
          break;
        case 'category_OT' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_OT;
          this.activeFilters.razAllDescriptionFilters();
          setTimeout(function() {
            self.accordionAO.close();
            self.accordionBNC.close();
            self.accordionOA.close();
            self.accordionOT.open();
          }, 100);
          break;
        case 'category_Hydro' :
          this.activeFilters.options.GenTypeOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_HYDRO;
          this.activeFilters.razAllDescriptionFilters();
          break;
      }
      this.activeFilters.trySetSelectedFromOptionsIfPossible('GenType', GenTypeSelected);
      this.GenTypeChange(null);
    }
  }

  GenTypeChange($event) {
    if (this.activeFilters.filters['GenTypeSelected'] != null) {
      if (this.activeFilters.filters['GenTypeSelected'].raw != null) {
        let menuItem = MENU_ITEMS;
        let options = menuItem['GEOMETRY_' + this.activeFilters.filters['GenTypeSelected'].raw];
        this.activeFilters.options.DesAoGeometrieOptions = options;
        this.activeFilters.options.DesOaGeometrieOptions = options;
      } else {
        this.activeFilters.options.DesAoGeometrieOptions = MENU_ITEMS.GEOMETRY_NULL;
        this.activeFilters.options.DesOaGeometrieOptions = MENU_ITEMS.GEOMETRY_NULL;
      }
    }
  }

  // Situation

  SitDepartementChange($event) {
    this.checkCommune();
  }

  SitCommuneSearch($event) {
    this.getCommunes($event.query);
  }

  getCommunes(searchText: string) {
    if (searchText.length > 2) {
      this.filterService.getCommunes(
          this.activeFilters.getSelectedRawString('GenRegionSelected'),
          this.activeFilters.getSelectedRawString('SitDepartementSelected'),
          searchText
        ).subscribe(data => {

        let SitCommuneSelected = this.activeFilters.getSelectedRawString('SitCommuneSelected');

        let communes = [];

        for (let communeDTO of data) {
          communes.push({ raw: communeDTO.id.toString(), fr: communeDTO.nom + ' (' + communeDTO.codePostal + ')' });
        }

        this.activeFilters.options.SitCommuneOptions = communes;

        this.activeFilters.trySetSelectedFromOptionsIfPossible('SitCommune', SitCommuneSelected);

      });
    } else {
      this.activeFilters.options.SitCommuneOptions = [];
    }
  }

  checkCommune() {
    if (this.activeFilters.filters['SitCommuneSelected'] != null && this.activeFilters.filters['SitCommuneSelected'].raw != null) {
      this.filterService.checkCommune(
          this.activeFilters.getSelectedRawString('GenRegionSelected'),
          this.activeFilters.getSelectedRawString('SitDepartementSelected'),
          this.activeFilters.filters['SitCommuneSelected'].raw,
        ).subscribe(data => {
          if (data.dto === 'nok') {
            this.activeFilters.filters['SitCommuneSelected'] =  null;
          }
      });
    }
  }

  // Particularités
  ParNomSearch($event) {
    this.getOuvragesName($event.query);
  }

  getOuvragesName(searchText: string) {
    if (searchText.length > 2) {
      this.filterService.getOuvragesName(searchText).subscribe(data => {
        const ouvragesName = [];
        for (const ouvrageDTO of data) {
          ouvragesName.push({ raw: ouvrageDTO.name });
        }
        this.activeFilters.options.ParNomOptions = ouvragesName;
      });
    } else {
      this.activeFilters.options.ParNomOptions = [];
    }
  }

  ParTypeProprietaireChange($event) {
    if (this.activeFilters.filters['ParTypeProprietaireSelected'] == null || this.activeFilters.filters['ParTypeProprietaireSelected'].raw == null) {
      this.getProprietaires(true);
    } else {
      if (this.activeFilters.filters['ParTypeProprietaireSelected'].raw === 'sncf') {
        this.activeFilters.options.ParProprietaireOptions = [ { raw: '7', fr: 'SNCF' } ];
        this.activeFilters.filters['ParProprietaireSelected'] = this.activeFilters.options.ParProprietaireOptions[0];
      } else {
        this.getProprietaires(false);
      }
    }
  }

  getProprietaires(includeSncf: boolean) {
    this.filterService.getProprietaires().subscribe(data => {

      const ParProprietaireSelected = this.activeFilters.getSelectedRawString('ParProprietaireSelected');

      this.activeFilters.options.ParProprietaireOptions = [ { raw: null, fr: '' } ];

      for (const proprietaireDto of data) {
        if (includeSncf || proprietaireDto.id !== 7) {
          const proprietaire: Proprietaire = new Proprietaire(proprietaireDto);
          this.activeFilters.options.ParProprietaireOptions.push({
            raw: proprietaire.id.toString(),
            fr: proprietaire.nom,
          });
        }
      }

      this.activeFilters.trySetSelectedFromOptionsIfPossible('ParProprietaire', ParProprietaireSelected);

      this.pendingBackendResponses--;
      this.getFiltersForConnectedUser();
    });
  }

  onChercherClick() {
    this.activeFilters.useFiltersForRequests(true);
    this.fetchDataFromBackEnd(null, true);
    this.accordionRechercher.close();
  }

  onRazClick() {
    this.activeFilters.razAllFilters();
    this.regionInfrapoleSecteurUoChange();
    this.activeFilters.useFiltersForRequests(true);
    this.fetchDataFromBackEnd(null, true);
    this.accordionRechercher.close();
  }

  onExporterClick() {
    this.exportLoading = true;
    this.toastrHelper.showToast(
      NbToastStatus.SUCCESS,
      'EXPORT EN COURS...',
      'Votre fichier d\'export est en cours de génération. Il sera téléchargé automatiquement une fois prêt. En attendant, vous pouvez continuer à utiliser l\'application SANS RAFRAICHIR LA PAGE.',
      20000
    );
    const event = { first: 0, rows: this.totalRecords };
    const requestFilters: ParamValue[] = this.activeFilters.generateRequestFilters(event, this.ouvrageTable);
    this.ouvrageService.getOuvrages(false, requestFilters, 3600).subscribe(data => {
      ExportExcelHelper.export(this.columnHelper.convertObjectsArrayToRawObjectsJsonData(data.result), 'ExportOuvrages', 'OUVRAGES');
      this.exportLoading = false;
    });
  }

  onInputKeyPress($event) {
    if ($event.keyCode === 13) {
      this.onChercherClick();
    }
  }

  creationOuvrage() {
    this.localStorageService.removeItem('ouvrage_id', false);
    this.router.navigate([FrontUrlConstant.PATH_TO_FICHE_OUVRAGE]);
  }

  onRowSelect(event) {
    this.localStorageService.setItem('ouvrage_id', event.data.id);
    this.router.navigate([FrontUrlConstant.PATH_TO_FICHE_OUVRAGE]);
  }
}
