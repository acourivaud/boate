import { MENU_ITEMS } from './ficheOuvrage-menu';
export const FIELDS =  {
  OPTIONS: {
    RegionOptions: null,
    InfrapoleOptions: null,
    SecteurOptions: null,
    UoOptions: null,
    UicOptions: MENU_ITEMS.UIC,
    LigneOptions: null,
    Departement1Options: null,
    Departement2Options: null,
    Commune1Options: null,
    Commune2Options: null,
  },
  FILTERS: {
    // ATTENTION LES SUFFIXES ONT UN IMPACT SUR LE PROGRAMME
    // (Selected, SelectedSearch, Typed, TypedSearch)
    RegionSelected: null,
    InfrapoleSelected: null,
    SecteurSelected: null,
    UoSelected: null,
    UicSelected: null,
    LigneSelected: null,
    Departement1Selected: null,
    Departement2Selected: null,
    Commune1SelectedSearch: null,
    Commune2SelectedSearch: null,
  },
};
