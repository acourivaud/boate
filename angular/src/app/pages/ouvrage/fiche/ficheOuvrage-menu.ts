export const MENU_ITEMS =  {
  WORK_CATEGORY: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'category_Autre',
      fr: 'Autres',
    },
    {
      raw: 'category_Bat',
      fr: 'Bâtiment Non Courant',
    },
    {
      raw: 'category_OA',
      fr: 'Ouvrage d\'Art',
    },
    {
      raw: 'category_OT',
      fr: 'Ouvrage en Terre',
    },
    {
      raw: 'category_Hydro',
      fr: 'Ouvrage Hydraulique',
    },
  ],
  UIC: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'uic_3',
      fr: '3',
    },
    {
      raw: 'uic_4',
      fr: '4',
    },
    {
      raw: 'uic_5',
      fr: '5',
    },
    {
      raw: 'uic_6',
      fr: '6',
    },
    {
      raw: 'uic_6AV',
      fr: '6AV',
    },
    {
      raw: 'uic_7AV',
      fr: '7AV',
    },
    {
      raw: 'uic_7SV',
      fr: '7SV',
    },
    {
      raw: 'uic_8AV',
      fr: '8AV',
    },
    {
      raw: 'uic_8SV',
      fr: '8SV',
    },
    {
      raw: 'uic_9AV',
      fr: '9AV',
    },
    {
      raw: 'uic_9SV',
      fr: '9SV',
    },
    {
      raw: 'uic_emb',
      fr: 'Emb',
    },
    {
      raw: 'uic_F',
      fr: 'F',
    },
    {
      raw: 'uic_neut',
      fr: 'Neut',
    },
    {
      raw: 'uic_none',
      fr: 'Sans',
    },
  ],
  WORK_TYPE_CATEGORY_NULL: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_aqc',
      fr: 'Aqueduc',
    },
    {
      raw: 'work_type_aqc_buse',
      fr: 'Aqueduc busé',
    },
    {
      raw: 'work_type_autre',
      fr: 'Autres',
    },
    {
      raw: 'work_type_bg',
      fr: 'Barrière grillagée',
    },
    {
      raw: 'work_type_bnc',
      fr: 'Bâtiment Non Courant',
    },
    {
      raw: 'work_type_bse',
      fr: 'Buse',
    },
    {
      raw: 'work_type_dlt',
      fr: 'Dalot',
    },
    {
      raw: 'work_type_deblai',
      fr: 'Déblai meuble',
    },
    {
      raw: 'work_type_depot',
      fr: 'Dépôt',
    },
    {
      raw: 'work_type_digoa',
      fr: 'Digue OA',
    },
    {
      raw: 'work_type_digot',
      fr: 'Digue OT',
    },
    {
      raw: 'work_type_dcp',
      fr: 'Dispositif Confortatif Parois',
    },
    {
      raw: 'work_type_divers',
      fr: 'Divers',
    },
    {
      raw: 'work_type_eab',
      fr: 'Ecran anti bruit',
    },
    {
      raw: 'work_type_efpb',
      fr: 'Ecran de filet pare-blocs',
    },
    {
      raw: 'work_type_gal',
      fr: 'Galerie',
    },
    {
      raw: 'work_type_gp',
      fr: 'Galerie de protection',
    },
    {
      raw: 'work_type_gt',
      fr: 'Galerie technique',
    },
    {
      raw: 'work_type_hydro',
      fr: 'Hydraulique',
    },
    {
      raw: 'work_type_mgh',
      fr: 'Mat de grande hauteur',
    },
    {
      raw: 'work_type_ms',
      fr: 'Mur de soutènement',
    },
    {
      raw: 'work_type_mgr',
      fr: 'Mur en gradin',
    },
    {
      raw: 'work_type_pc',
      fr: 'Paroi clouée',
    },
    {
      raw: 'work_type_prev',
      fr: 'Paroi revêtue',
    },
    {
      raw: 'work_type_pr',
      fr: 'Paroi rocheuse',
    },
    {
      raw: 'work_type_pas',
      fr: 'Passerelle',
    },
    {
      raw: 'work_type_per',
      fr: 'Perré',
    },
    {
      raw: 'work_type_pcl',
      fr: 'Pont canal',
    },
    {
      raw: 'work_type_pra',
      fr: 'Pont rail',
    },
    {
      raw: 'work_type_pro',
      fr: 'Pont route',
    },
    {
      raw: 'work_type_portique',
      fr: 'Portique',
    },
    {
      raw: 'work_type_potence',
      fr: 'Potence',
    },
    {
      raw: 'work_type_mxt',
      fr: 'Profil mixte',
    },
    {
      raw: 'work_type_profil_rasant',
      fr: 'Profil rasant',
    },
    {
      raw: 'work_type_pyl',
      fr: 'Pylône',
    },
    {
      raw: 'work_type_remblai',
      fr: 'Remblai',
    },
    {
      raw: 'work_type_sdm',
      fr: 'Saut de mouton',
    },
    {
      raw: 'work_type_tete_tunnel',
      fr: 'Tête tunnel',
    },
    {
      raw: 'work_type_tranchee',
      fr: 'Talus rocheux',
    },
    {
      raw: 'work_type_tc',
      fr: 'Tranchée couverte',
    },
    {
      raw: 'work_type_td',
      fr: 'Tranchée drainante',
    },
    {
      raw: 'work_type_trm',
      fr: 'Trémie',
    },
    {
      raw: 'work_type_tun',
      fr: 'Tunnel',
    },
    {
      raw: 'work_type_versant',
      fr: 'Versant',
    },
    {
      raw: 'work_type_via',
      fr: 'Viaduc',
    },
  ],
  WORK_TYPE_CATEGORY_category_Autre: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_divers',
      fr: 'Divers',
    },
    {
      raw: 'work_type_eab',
      fr: 'Ecran anti bruit',
    },
    {
      raw: 'work_type_mgh',
      fr: 'Mat de grande hauteur',
    },
    {
      raw: 'work_type_portique',
      fr: 'Portique',
    },
    {
      raw: 'work_type_potence',
      fr: 'Potence',
    },
    {
      raw: 'work_type_pyl',
      fr: 'Pylône',
    },
  ],
  WORK_TYPE_CATEGORY_category_Bat: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_bnc',
      fr: 'Bâtiment Non Courant',
    },
  ],
  WORK_TYPE_CATEGORY_category_OA: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_aqc',
      fr: 'Aqueduc',
    },
    {
      raw: 'work_type_aqc_buse',
      fr: 'Aqueduc busé',
    },
    {
      raw: 'work_type_bse',
      fr: 'Buse',
    },
    {
      raw: 'work_type_dcp',
      fr: 'Dispositif Confortatif Parois',
    },
    {
      raw: 'work_type_digoa',
      fr: 'Digue OA',
    },
    {
      raw: 'work_type_dlt',
      fr: 'Dalot',
    },
    {
      raw: 'work_type_gal',
      fr: 'Galerie',
    },
    {
      raw: 'work_type_gp',
      fr: 'Galerie de protection',
    },
    {
      raw: 'work_type_gt',
      fr: 'Galerie technique',
    },
    {
      raw: 'work_type_ms',
      fr: 'Mur de soutènement',
    },
    {
      raw: 'work_type_mgr',
      fr: 'Mur en gradin',
    },
    {
      raw: 'work_type_pas',
      fr: 'Passerelle',
    },
    {
      raw: 'work_type_pc',
      fr: 'Paroi clouée',
    },
    {
      raw: 'work_type_prev',
      fr: 'Paroi revêtue',
    },
    {
      raw: 'work_type_pcl',
      fr: 'Pont canal',
    },
    {
      raw: 'work_type_per',
      fr: 'Perré',
    },
    {
      raw: 'work_type_pra',
      fr: 'Pont rail',
    },
    {
      raw: 'work_type_pro',
      fr: 'Pont route',
    },
    {
      raw: 'work_type_sdm',
      fr: 'Saut de mouton',
    },
    {
      raw: 'work_type_tc',
      fr: 'Tranchée couverte',
    },
    {
      raw: 'work_type_td',
      fr: 'Tranchée drainante',
    },
    {
      raw: 'work_type_trm',
      fr: 'Trémie',
    },
    {
      raw: 'work_type_tun',
      fr: 'Tunnel',
    },
    {
      raw: 'work_type_via',
      fr: 'Viaduc',
    },
  ],
  WORK_TYPE_CATEGORY_category_OT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_bg',
      fr: 'Barrière grillagée',
    },
    {
      raw: 'work_type_deblai',
      fr: 'Déblai meuble',
    },
    {
      raw: 'work_type_efpb',
      fr: 'Ecran de filet pare-blocs',
    },
    {
      raw: 'work_type_mxt',
      fr: 'Profil mixte',
    },
    {
      raw: 'work_type_prev',
      fr: 'Paroi revêtue',
    },
    {
      raw: 'work_type_profil_rasant',
      fr: 'Profil rasant',
    },
    {
      raw: 'work_type_remblai',
      fr: 'Remblai',
    },
    {
      raw: 'work_type_tranchee',
      fr: 'Talus rocheux',
    },
    {
      raw: 'work_type_versant',
      fr: 'Versant',
    },
  ],
  WORK_TYPE_CATEGORY_category_Hydro: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_hydro',
      fr: 'Hydraulique',
    },
  ],
  WORK_TYPE_PATRIMOINE_ALL: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_patrimoine_debmeu',
      fr: 'Déblai meuble',
    },
    {
      raw: 'work_type_patrimoine_remblai',
      fr: 'Remblai',
    },
    {
      raw: 'work_type_patrimoine_debroch',
      fr: 'Déblai rocheux',
    },
  ],
  WORK_TYPE_PATRIMOINE_debmeu: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_patrimoine_debmeu',
      fr: 'Déblai meuble',
    },
  ],
  WORK_TYPE_PATRIMOINE_remblai: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_patrimoine_remblai',
      fr: 'Remblai',
    },
  ],
  WORK_TYPE_PATRIMOINE_debroch: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'work_type_patrimoine_debroch',
      fr: 'Déblai rocheux',
    },
  ],
  CLASSEMENT: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'OTC',
      fr: 'OTC',
    },
    {
      raw: 'OTCR',
      fr: 'OTCR',
    },
    {
      raw: 'OTP',
      fr: 'OTP',
    },
    {
      raw: 'OTS',
      fr: 'OTS',
    },
  ],
  SITUATION: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'situation_up_left',
      fr: '>G',
    },
    {
      raw: 'situation_left',
      fr: 'G',
    },
    {
      raw: 'situation_down_left',
      fr: '<G',
    },
    {
      raw: 'situation_up_right',
      fr: '>D',
    },
    {
      raw: 'situation_right',
      fr: 'D',
    },
    {
      raw: 'situation_down_right',
      fr: '<D',
    },
    {
      raw: 'situation_up',
      fr: '>',
    },
    {
      raw: 'situation_down',
      fr: '<',
    },
  ],
  OT_MIXTE_TYPE: [
    {
      raw: null,
      fr: '',
    },
    {
      raw: 'OMT_01',
      fr: 'Déblai-rasant',
    },
    {
      raw: 'OMT_02',
      fr: 'Déblai-remblai',
    },
    {
      raw: 'OMT_03',
      fr: 'Déblai-tranchée',
    },
    {
      raw: 'OMT_04',
      fr: 'Déblai-versant',
    },
    {
      raw: 'OMT_05',
      fr: 'Remblai-rasant',
    },
    {
      raw: 'OMT_06',
      fr: 'Remblai-tranchée',
    },
    {
      raw: 'OMT_07',
      fr: 'Remblai-versant',
    },
    {
      raw: 'OMT_08',
      fr: 'Rasant-tranchée',
    },
    {
      raw: 'OMT_09',
      fr: 'Rasant-versant',
    },
    {
      raw: 'OMT_10',
      fr: 'Tranchée-versant',
    },
  ],
};
