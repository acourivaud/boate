import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { ArchiveDossier } from './../../../model/archiveDossier';
import { Surveillance } from './../../../model/surveilllance';
import { Bouclage } from './../../../model/bouclage';
import { CrudHelper } from './../../../model/crudHelper';
import { BackUrlConstant } from './../../../constant/BackUrlConstant';
import { GeneralService } from './../../../service/general.service';
import { OuvrageProprietaire } from './../../../model/ouvrageProprietaire';
import { MENU_ITEMS } from './ficheOuvrage-menu';
import { Location } from '@angular/common';
import { FrontUrlConstant } from './../../../constant/FrontUrlConstant';
import { Router } from '@angular/router';
import { FR_LOCALE } from './../../../constant/CalendarFrLocale';
import { Region } from './../../../model/region';
import { FIELDS } from './ficheOuvrage-fields';
import { FilterService } from './../../../service/filter.service';
import { FilterHelper } from './../../../model/filterHelper';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

import { Ouvrage } from '../../../model/ouvrage';
import { ToastrHelper } from '../../../model/toastrHelper';
import { LocalStorageService } from '../../../service/localStorage.service';
import { OuvrageService } from '../../../service/ouvrage.service';
import { UtilisateurService } from '../../../service/utilisateur.service';
import { Utilisateur } from '../../../model/utilisateur';
import { OnInit, Component, AfterViewInit, ViewChild, TemplateRef } from '@angular/core';

import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';

@Component({
  selector: 'ngx-fiche-ouvrage',
  styleUrls: ['./ficheOuvrage.component.scss'],
  templateUrl: './ficheOuvrage.component.html',
})
export class FicheOuvrageComponent implements OnInit, AfterViewInit {

  utilisateur: Utilisateur;

  ouvrageId: number;
  currentOuvrage: Ouvrage;
  ouvrageProprietaires: OuvrageProprietaire[];

  activeFilters: FilterHelper;

  selectOptions: any = MENU_ITEMS;
  typesBoateOptions: any[] = MENU_ITEMS.WORK_TYPE_CATEGORY_NULL;
  typesPatrimoineOptions: any[] = MENU_ITEMS.WORK_TYPE_PATRIMOINE_ALL;

  toastrHelper: ToastrHelper;

  map: Map;

  dateIncident: Date;
  fr: any = FR_LOCALE;

  crudHelper: CrudHelper;
  @ViewChild('templateForEdit') cruPopupTemplate: TemplateRef<any>;
  @ViewChild('templateForDelete') deletePopupTemplate: TemplateRef<any>;
  crudObjectDefinitions: any = {
    OuvrageProprietaire: { object: new OuvrageProprietaire({}), table: null },
  };

  historiquesDesVisites: Bouclage[] = [];
  historiquesDesSurveillances: Surveillance[] = [];
  historiquesDesArchives: ArchiveDossier[] = [];

  historiquesDesVisitesCols = [
    { field: 'surveillance.ouvrage.line.code', header: 'Ligne', style: 'column-center column-width-100' },
    { field: 'surveillance.ouvrage.pk', header: 'Pk', style: 'column-center column-width-100' },
    { field: 'surveillance.ouvrage.middlePk', header: 'Pk milieu', style: 'column-center column-width-100' },
    { field: 'surveillance.ouvrage.endPk', header: 'Pk fin', style: 'column-center column-width-100' },
    { field: 'surveillance.ouvrage.indice', header: 'Indice', style: 'column-center column-width-100' },
    { field: 'surveillance.ouvrage.categorie', header: 'Catégorie', style: 'column-center column-width-100' },
    { field: 'typeKey', header: 'Type', style: 'column-center column-width-100' },
    { field: 'surveillance.typeSurveillance', header: 'Type visite', style: 'column-center column-width-100' },
    { field: 'previsionDate', header: 'Date prévue', style: 'column-center column-width-100' },
    { field: 'typeKey', header: 'Visite', style: 'column-center column-width-100' },
    { field: 'etatOuvrage', header: 'Etat ouvrage', style: 'column-center column-width-100' },
    { field: 'laborTodoAgent0', header: 'Proposition U0', style: 'column-center column-width-100' },
    { field: 'laborCostAgent0', header: 'Montant U0', style: 'column-center column-width-100' },
    { field: 'laborTodoAgent1', header: 'Proposition U1', style: 'column-center column-width-100' },
    { field: 'laborCostAgent1', header: 'Montant U1', style: 'column-center column-width-100' },
    { field: 'laborTodoAgent2', header: 'Proposition U2', style: 'column-center column-width-100' },
    { field: 'laborCostAgent2', header: 'Montant U2', style: 'column-center column-width-100' },
  ];

  historiquesDesArchivesCols = [
    { field: 'ouvrage.line.code', header: 'Ligne', style: 'column-center column-width-100' },
    { field: 'ouvrage.pk', header: 'Pk', style: 'column-center column-width-100' },
    { field: 'ouvrage.middlePk', header: 'Pk milieu', style: 'column-center column-width-100' },
    { field: 'ouvrage.endPk', header: 'Pk fin', style: 'column-center column-width-100' },
    { field: 'ouvrage.categorie', header: 'Catégorie', style: 'column-center column-width-100' },
    { field: 'description', header: 'Description', style: 'column-center column-width-100' },
    { field: 'commentaires', header: 'Observation', style: 'column-center column-width-100' },
    { field: 'numeroDossier', header: 'N° Boite', style: 'column-center column-width-100' },
    { field: 'numeroDossier', header: 'Localisation', style: 'column-center column-width-100' },
    { field: 'typeArchive', header: 'Fiche Archive', style: 'column-center column-width-100' },
  ];

  constructor(
    private ouvrageService: OuvrageService,
    private toastrService: NbToastrService,
    private localStorageService: LocalStorageService,
    private dialogService: NbDialogService,
    private generalService: GeneralService,
    private filterService: FilterService,
    private router: Router,
    private utilisateurService: UtilisateurService,
    private location: Location
  ) {}

  ngOnInit() {
    this.ouvrageId = this.localStorageService.getItem('ouvrage_id') ? Number.parseInt(this.localStorageService.getItem('ouvrage_id')) : null;
    this.activeFilters = new FilterHelper(this.filterService, 'ficheOuvrage', null, null);
    this.activeFilters.options = FIELDS.OPTIONS;
    this.activeFilters.filters = FIELDS.FILTERS;
    if (this.ouvrageId) { // Ouvrage existant
      this.initOuvrageDescription();
    } else {

    }
    this.toastrHelper = new ToastrHelper(this.toastrService);
    this.filterService.getRegions().subscribe(data => {
      this.activeFilters.options.RegionOptions = [ { raw: null, fr: '' } ];
      for (const regionDto of data) {
        const region: Region = new Region(regionDto);
        this.activeFilters.options.RegionOptions.push({
          raw: region.id.toString(),
          fr: region.name
        });
      }
      this.getDepartementsAccordingToRegion();
    });
    this.crudHelper = new CrudHelper(this.generalService, this.dialogService, this.toastrHelper, this.crudObjectDefinitions, this.cruPopupTemplate, this.deletePopupTemplate);
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.map = new Map({
        view: new View({
          maxZoom: 18,
          center: [0, 0],
          zoom: 2
        }),
        layers: [
          new TileLayer({
            source: new OSM({ attributions: [] })
          })
        ],
        target: 'map'
      });
    }, 0);
  }

  regionInfrapoleSecteurUoChange() {
    this.filterService.getRegionInfrapoleSecteurUoLigne(
        this.activeFilters.getSelectedRawString('RegionSelected'),
        this.activeFilters.getSelectedRawString('InfrapoleSelected'),
        this.activeFilters.getSelectedRawString('SecteurSelected'),
        this.activeFilters.getSelectedRawString('UoSelected')
      ).subscribe(data => {

      const InfrapoleSelected = this.activeFilters.getSelectedRawString('InfrapoleSelected');
      const SecteurSelected = this.activeFilters.getSelectedRawString('SecteurSelected');
      const UoSelected = this.activeFilters.getSelectedRawString('UoSelected');
      const LigneSelected = this.activeFilters.getSelectedRawString('LigneSelected');

      const infrapoles = [ { raw: null, fr: '' } ];
      const secteurs = [ { raw: null, fr: '' } ];
      const unitesOperationnelle = [ { raw: null, fr: '' } ];
      const lignes = [ { raw: null, fr: '' } ];

      for (const infrapoleDTO of data.infrapoles) {
        infrapoles.push({ raw: infrapoleDTO.id.toString(), fr: infrapoleDTO.name });
      }

      for (const secteurDTO of data.secteurs) {
        secteurs.push({ raw: secteurDTO.id.toString(), fr: secteurDTO.nom });
      }

      for (const uniteOperationnelleDTO of data.unitesOperationnelle) {
        unitesOperationnelle.push({ raw: uniteOperationnelleDTO.id.toString(), fr: uniteOperationnelleDTO.name });
      }

      for (const ligneDTO of data.lignes) {
        lignes.push({ raw: ligneDTO.id.toString(), fr: ligneDTO.nom });
      }

      this.activeFilters.options.InfrapoleOptions = infrapoles;
      this.activeFilters.options.SecteurOptions = secteurs;
      this.activeFilters.options.UoOptions = unitesOperationnelle;
      this.activeFilters.options.LigneOptions = lignes;

      this.activeFilters.trySetSelectedFromOptionsIfPossible('Infrapole', InfrapoleSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('Secteur', SecteurSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('Uo', UoSelected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('Ligne', LigneSelected);

      this.getDepartementsAccordingToRegion();
    });
  }

  getDepartementsAccordingToRegion() {
    this.filterService.getDepartements(
        this.activeFilters.getSelectedRawString('RegionSelected')
      ).subscribe(data => {

      const Departement1Selected = this.activeFilters.getSelectedRawString('Departement1Selected');
      const Departement2Selected = this.activeFilters.getSelectedRawString('Departement2Selected');

      const departements = [ { raw: null, fr: '' } ];

      for (const departementDTO of data) {
        departements.push({ raw: departementDTO.id.toString(), fr: departementDTO.nom });
      }

      this.activeFilters.options.Departement1Options = departements;
      this.activeFilters.options.Departement2Options = departements;

      this.activeFilters.trySetSelectedFromOptionsIfPossible('Departement1', Departement1Selected);
      this.activeFilters.trySetSelectedFromOptionsIfPossible('Departement2', Departement2Selected);
    });
  }

  getHistoriques() {
    this.generalService.getUrlWithIdParam(BackUrlConstant.BOUCLAGE_OUVRAGE, this.currentOuvrage.id.toString()).subscribe(data => {
      this.historiquesDesVisites = [];
      for (const bouclage of data) {
        this.historiquesDesVisites.push(new Bouclage(bouclage));
      }
    });
    this.generalService.getUrlWithIdParam(BackUrlConstant.SURVEILLANCE_OUVRAGE, this.currentOuvrage.id.toString()).subscribe(data => {
      this.historiquesDesSurveillances = [];
      for (const surveillance of data) {
        this.historiquesDesSurveillances.push(new Surveillance(surveillance));
      }
    });
    this.generalService.getUrlWithIdParam(BackUrlConstant.ARCHIVE_OUVRAGE, this.currentOuvrage.id.toString()).subscribe(data => {
      this.historiquesDesArchives = [];
      for (const archiveDossier of data) {
        this.historiquesDesArchives.push(new ArchiveDossier(archiveDossier, this.utilisateurService.getUtilisateurConnected()));
      }
    });
  }

  openCreateDialog(className: string) {
    this.crudHelper.openCreateDialog(className);
    this.prepareObjectBasedOnClassName(this.crudHelper.editedObject);
  }

  openEditDialog(object: any) {
    this.crudHelper.openEditDialog(object);
    this.prepareObjectBasedOnClassName(this.crudHelper.editedObject);
  }

  openDeleteDialog(object: any) {
    this.crudHelper.openDeleteDialog(object);
    this.prepareObjectBasedOnClassName(this.crudHelper.editedObject);
  }

  prepareObjectBasedOnClassName(object: any): void {
    if (object.className === 'OuvrageProprietaire') {
      object.ouvrage = this.currentOuvrage;
      this.generalService.getUrlWithIdParam(BackUrlConstant.PROPRIETAIRE_DISPONIBLE, this.currentOuvrage.id.toString()).subscribe(data => {
        object['ProprioOptions'] = data;
      }, err => {
        this.toastrHelper.showDefaultErrorToast(err.msg);
      });
		}
  }

  canEditedObjectBeSaved(): boolean {
    return this.crudHelper.canEditedObjectBeSaved();
  }

  CommuneSearch($event, commune1ou2: string) {
    this.getCommunes($event.query, commune1ou2);
  }

  getCommunes(searchText: string, commune1ou2: string) {
    if (searchText.length > 2) {
      this.filterService.getCommunes(
          this.activeFilters.getSelectedRawString('RegionSelected'),
          this.activeFilters.getSelectedRawString('Departement' + commune1ou2 + 'Selected'),
          searchText
        ).subscribe(data => {

        const CommuneSelected = this.activeFilters.getSelectedRawString('Commune' + commune1ou2 + 'Selected');

        const communes = [];

        for (const communeDTO of data) {
          communes.push({ raw: communeDTO.id.toString(), fr: communeDTO.nom + ' (' + communeDTO.codePostal + ')' });
        }

        this.activeFilters.options['Commune' + commune1ou2 + 'Options'] = communes;

        this.activeFilters.trySetSelectedFromOptionsIfPossible('Commune' + commune1ou2, CommuneSelected);

      });
    } else {
      this.activeFilters.options['Commune' + commune1ou2 + 'Options'] = [];
    }
  }

  setTypesBoateOptions() {
    if (this.currentOuvrage) {
      if (this.currentOuvrage.categorie === undefined || this.currentOuvrage.categorie === null) {
        this.typesBoateOptions = MENU_ITEMS.WORK_TYPE_CATEGORY_NULL;
      } else {
        this.typesBoateOptions = MENU_ITEMS['WORK_TYPE_CATEGORY_' + this.currentOuvrage.categorie];
      }
    }
  }

  setTypesPatrimoineOptions() {
    if (this.currentOuvrage) {
      // A DEFINIR AVEC ALBAN
    }
  }

  isOT(): boolean {
    if (this.currentOuvrage) {
      if (this.currentOuvrage.categorie === 'category_OT') return true;
    }
    return false;
  }

  initOuvrageDescription() {
    this.ouvrageService.getOuvrage(this.ouvrageId).subscribe(data => {
      this.currentOuvrage = new Ouvrage(data);
      this.crudHelper.load('OuvrageProprietaire', this.currentOuvrage.id);
      this.getHistoriques();
      this.localStorageService.setItem('currentOuvrage', JSON.stringify(this.currentOuvrage));
      this.getUtilisateurConnected();
      this.setTypesBoateOptions();
    });
  }

  enregistrerOuvrage() {
    this.ouvrageService.saveOuvrage(this.currentOuvrage).subscribe(data => {
      this.toastrHelper.showDefaultSuccessToast();
      this.initOuvrageDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  supprimerOuvrage() {
    this.currentOuvrage.supprime = true;
    this.ouvrageService.deleteOrArchiveOuvrage(this.currentOuvrage).subscribe(data => {
      this.toastrHelper.showToast(NbToastStatus.SUCCESS, 'Modifications enregistrées', data.message);
      this.initOuvrageDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  archiverOuvrage() {
    this.currentOuvrage.archive = true;
    this.ouvrageService.deleteOrArchiveOuvrage(this.currentOuvrage).subscribe(data => {
      this.toastrHelper.showToast(NbToastStatus.SUCCESS, 'Modifications enregistrées', data.message);
      this.initOuvrageDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  recupererOuvrage() {
    this.currentOuvrage.archive = false;
    this.ouvrageService.saveOuvrage(this.currentOuvrage).subscribe(data => {
      this.toastrHelper.showToast(NbToastStatus.SUCCESS, 'Modifications enregistrées', 'L\'ouvrage a été récupéré avec succès');
      this.initOuvrageDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  retablirOuvrage() {
    this.currentOuvrage.supprime = false;
    this.ouvrageService.saveOuvrage(this.currentOuvrage).subscribe(data => {
      this.toastrHelper.showToast(NbToastStatus.SUCCESS, 'Modifications enregistrées', 'L\'ouvrage a été réabli avec succès');
      this.initOuvrageDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  getUtilisateurConnected() {
    this.utilisateurService.getUtilisateurConnectedFromBack().subscribe(data => {
      this.utilisateur = new Utilisateur(data);
    });
  }

  switchToSurveillance() {
    this.localStorageService.setItem('surveillance_id', this.currentOuvrage.surveillanceId.toString());
    this.router.navigate([FrontUrlConstant.PATH_TO_FICHE_SURVEILLANCE]);
  }

  backClicked() {
    this.location.back();
  }

  resolveFieldData(rowData, colField){
    return ObjectUtils.resolveFieldData(rowData, colField);
  }
}
