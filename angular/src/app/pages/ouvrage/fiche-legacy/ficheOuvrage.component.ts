import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

import { FieldBindingHelper } from '../../../model/FieldBindingHelper';
import { Ouvrage } from '../../../model/ouvrage';
import { PageHelper } from '../../../model/pageHelper';
import { ParamObject } from '../../../model/paramObject';
import { ToastrHelper } from '../../../model/toastrHelper';
import { GeneralService } from '../../../service/general.service';
import { LocalStorageService } from '../../../service/localStorage.service';
import { OuvrageService } from '../../../service/ouvrage.service';
import { ParamService } from '../../../service/param.service';
import { UtilisateurService } from '../../../service/utilisateur.service';
import { Utilisateur } from '../../../model/utilisateur';

@Component({
  selector: 'ngx-fiche-ouvrage',
  styleUrls: ['./ficheOuvrage.component.scss'],
  templateUrl: './ficheOuvrage.component.html',
})
export class FicheOuvrageLegacyComponent implements OnInit {

  ouvrageId: number;
  currentOuvrage: Ouvrage;

  // Socle pour NgxPageDefinition
  toastrHelper: ToastrHelper;
  paramArray: ParamObject[] = [];
  pageHelper: PageHelper;

  constructor(private ouvrageService: OuvrageService, private toastrService: NbToastrService, private generalService: GeneralService, private paramService: ParamService,
    private localStorageService: LocalStorageService, private dialogService: NbDialogService, private utilisateurService: UtilisateurService, private cd: ChangeDetectorRef) {
    this.toastrHelper = new ToastrHelper(toastrService);
    this.pageHelper = new PageHelper(this.generalService, this.paramService, this.toastrService, this.localStorageService, this.dialogService, this.utilisateurService);
  }

  ngOnInit() {
    this.ouvrageId = this.localStorageService.getItem('ouvrage_id') ? Number.parseInt(this.localStorageService.getItem('ouvrage_id')) : null;
    if (this.ouvrageId) { // Ouvrage existant
      this.initOuvrageDescription();
    } else {
      // TODO --> Tester sur une création d'ouvrage pour voir le comportement
      this.pageHelper.loadingPage = true;
    }
  }

  initOuvrageDescription() {
    this.ouvrageService.getOuvrage(this.ouvrageId).subscribe(data => {
      this.currentOuvrage = data;
      this.localStorageService.setItem('currentOuvrage', JSON.stringify(this.currentOuvrage));
      this.getUtilisateurConnected();
    });
  }

  initPageTemplate() {
    this.pageHelper.titlePage = 'Fiche de l\'ouvrage';
    this.pageHelper.paramArray = this.initParamArray();
    this.pageHelper.fieldBindingHelper = this.initBindingData();
    this.pageHelper.initFromTemplateName('ficheOuvrage');
  }

  initParamArray(): ParamObject[] {
    this.paramArray.push(new ParamObject('paramId', this.ouvrageId ? this.ouvrageId.toString() : null, null));
    this.paramArray.push(new ParamObject('ouvrage', this.currentOuvrage, ['proprietaireModule']));
    this.paramArray.push(new ParamObject('indexIncrement', null, ['proprietaireModule']));
    return this.paramArray;
  }

  initBindingData(): FieldBindingHelper {
    return new FieldBindingHelper(this.currentOuvrage);
  }

  enregistrerOuvrage() {
    this.pageHelper.fieldBindingHelper.bindHtmlToObject();
    this.ouvrageService.saveOuvrage(this.currentOuvrage).subscribe(data => {
      this.toastrHelper.showDefaultSuccessToast();
      this.initOuvrageDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  supprimerOuvrage() {
    this.currentOuvrage.supprime = true;
    this.ouvrageService.deleteOrArchiveOuvrage(this.currentOuvrage).subscribe(data => {
      this.toastrHelper.showToast(NbToastStatus.SUCCESS, 'Modifications enregistrées', data.message);
      this.initOuvrageDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  archiverOuvrage() {
    this.currentOuvrage.archive = true;
    this.ouvrageService.deleteOrArchiveOuvrage(this.currentOuvrage).subscribe(data => {
      this.toastrHelper.showToast(NbToastStatus.SUCCESS, 'Modifications enregistrées', data.message);
      this.initOuvrageDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  recupererOuvrage() {
    this.currentOuvrage.archive = false;
    this.ouvrageService.saveOuvrage(this.currentOuvrage).subscribe(data => {
      this.toastrHelper.showToast(NbToastStatus.SUCCESS, 'Modifications enregistrées', 'L\'ouvrage a été récupéré avec succès');
      this.initOuvrageDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  retablirOuvrage() {
    this.currentOuvrage.supprime = false;
    this.ouvrageService.saveOuvrage(this.currentOuvrage).subscribe(data => {
      this.toastrHelper.showToast(NbToastStatus.SUCCESS, 'Modifications enregistrées', 'L\'ouvrage a été réabli avec succès');
      this.initOuvrageDescription();
    }, err => {
      this.toastrHelper.showDefaultErrorToast(err.msg);
    });
  }

  getUtilisateurConnected() {
    this.utilisateurService.getUtilisateurConnectedFromBack().subscribe(data => {
      this.pageHelper.utilisateur = new Utilisateur(data);
      this.initPageTemplate();
    });
  }
}
