import { NgModule } from '@angular/core';

import { TableModule } from 'primeng/table';
import { SliderModule } from 'primeng/slider';
import { TooltipModule } from 'primeng/tooltip';
import { CheckboxModule } from 'primeng/checkbox';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ThemeModule } from '../../../@theme/theme.module';
import { KeyFilterModule } from 'primeng/keyfilter';
import { TabMenuModule } from 'primeng/tabmenu';
import { DropdownModule } from 'primeng/dropdown';
import { PageDefinitionComponent } from './page-definition.component';
import { CrudModule } from '../crud/crud.module';


@NgModule({
  imports: [
    TableModule,
    SliderModule,
    TooltipModule,
    CheckboxModule,
    AutoCompleteModule,
    ThemeModule,
    KeyFilterModule,
    TabMenuModule,
    DropdownModule,
    CrudModule,
  ],
  declarations: [
    PageDefinitionComponent
  ],
  providers: [
  ],
  exports: [
    PageDefinitionComponent,
    TableModule,
    SliderModule,
    TooltipModule,
    CheckboxModule,
    AutoCompleteModule,
    ThemeModule,
    KeyFilterModule,
    TabMenuModule,
    DropdownModule,
    CrudModule
  ]
})
export class PageDefinitionModule { }
