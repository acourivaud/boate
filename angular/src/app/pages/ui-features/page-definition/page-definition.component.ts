import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { NbDialogService, NbToastrService } from '@nebular/theme';

import { PageHelper } from '../../../model/pageHelper';
import { FilterService } from '../../../service/filter.service';
import { GeneralService } from '../../../service/general.service';
import { ParamObject } from '../../../model/paramObject';

@Component({
  selector: 'ngx-page-definition',
  styleUrls: ['./page-definition.component.scss'],
  templateUrl: './page-definition.component.html',
  changeDetection: ChangeDetectionStrategy.Default,
})
export class PageDefinitionComponent implements OnInit {

  @Input() pageHelper: PageHelper;
  @Input() paramArray: ParamObject[];

  constructor(private generalService: GeneralService, private dialogService: NbDialogService, private toastrService: NbToastrService, private filterService: FilterService) {

  }


  ngOnInit() {

  }
}
