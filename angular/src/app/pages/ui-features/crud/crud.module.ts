import { NgModule } from '@angular/core';

import { CrudComponent } from './crud.component';
import { TableModule } from 'primeng/table';
import { SliderModule } from 'primeng/slider';
import { TooltipModule } from 'primeng/tooltip';
import { CheckboxModule } from 'primeng/checkbox';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ThemeModule } from '../../../@theme/theme.module';
import { KeyFilterModule } from 'primeng/keyfilter';
import { TabMenuModule } from 'primeng/tabmenu';
import { DropdownModule } from 'primeng/dropdown';


@NgModule({
  imports: [
    TableModule,
    SliderModule,
    TooltipModule,
    CheckboxModule,
    AutoCompleteModule,
    ThemeModule,
    KeyFilterModule,
    TabMenuModule,
    DropdownModule,
  ],
  declarations: [
    CrudComponent
  ],
  providers: [
  ],
  exports: [
    CrudComponent,
    TableModule,
    SliderModule,
    TooltipModule,
    CheckboxModule,
    AutoCompleteModule,
    ThemeModule,
    KeyFilterModule,
    TabMenuModule,
    DropdownModule
  ]
})
export class CrudModule { }
