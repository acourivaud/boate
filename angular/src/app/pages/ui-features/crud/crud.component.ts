import { CrudHelper } from '../../../model/crudHelper';
import { OnInit, Component, Input, TemplateRef, Output, EventEmitter, ViewChildren, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { GeneralService } from '../../../service/general.service';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { OuvrageProprietaire } from '../../../model/ouvrageProprietaire';
import { MethodeCallBack } from '../../../model/methodeCallBack';
import { ToastrHelper } from '../../../model/toastrHelper';
import { ParamObject } from '../../../model/paramObject';
import { ParamModule } from '../../../model/paramModule';
import { FilterService } from '../../../service/filter.service';
import { Ligne } from '../../../model/ligne';
import { Geometrie } from '../../../model/geometrie';
import { ParamChapitre } from '../../../model/paramChapitre';

@Component({
  selector: 'ngx-crud',
  styleUrls: ['./crud.component.scss'],
  templateUrl: './crud.component.html',
  changeDetection: ChangeDetectionStrategy.Default,
})
export class CrudComponent implements OnInit {

  toastrHelper: ToastrHelper;


  activeDialogRef: any;

  // Select
  editedObjectList: any[];

  // Autocomplete
  autocompleteSuggestions: any[];
  @ViewChildren('autocomplete') autocompletes: any;


  popupTitleDescription: string;
  indexIncrement: number;


  @Input() paramArray: ParamObject[];
  @Input() crudHelper: CrudHelper;
  @Input() colsArray: any[];
  @Input() libelleModule: string;
  @Input() pTableArray: any[];
  @Input() paramChapitre: ParamChapitre;

  @ViewChild('templateForObject') cruPopupTemplate: TemplateRef<any>;
  @ViewChild('templateForDelete') deletePopupTemplate: TemplateRef<any>;

  constructor(private generalService: GeneralService, private dialogService: NbDialogService, private toastrService: NbToastrService, private filterService: FilterService) {

  }


  ngOnInit() {
    this.toastrHelper = new ToastrHelper(this.toastrService);
    this.crudHelper.cruPopupTemplate = this.cruPopupTemplate;
    this.crudHelper.deletePopupTemplate = this.deletePopupTemplate;
  }

  openCreateDialog(paramModule: ParamModule, template: TemplateRef<any>) {
    this.indexIncrement = 1;
    this.bindTablesToCrudHelper(paramModule.className);
    this.crudHelper.openCreateDialog(paramModule.className, template);
    this.crudHelper.editedObject = this.crudHelper.crudObjectDefinitions[paramModule.className].object.completeDTO(this.crudHelper.editedObject, null);
    this.initalizeDialogElement(this.crudHelper.editedObject);
  }

  openDeleteDialog(object: any, paramModule: ParamModule, template: TemplateRef<any>) {
    this.bindTablesToCrudHelper(paramModule.className);
    object = this.crudHelper.crudObjectDefinitions[paramModule.className].object.completeDTO(object, null);
    this.crudHelper.openDeleteDialog(object, template);
  }

  initalizeDialogElement(object: any) {
    this.editedObjectList = [];
    object.structure.fields.forEach(field => {
        if (field.type === 'select') {
          if (field.methodUrlCall) {
            switch (field.methodUrlCall) {
              case MethodeCallBack.GET:
              this.generalService.getUrl(field.selectOptionsUrl).subscribe(data => {
                this.editedObjectList[field.index]  = data;
              }, err => {
                this.toastrHelper.showDefaultErrorToast(err.msg);
              });
              break;
              case MethodeCallBack.GET_WITH_PARAM_ID:
              this.generalService.getUrlWithIdParam(field.selectOptionsUrl, this.findValueFromParamArray(this.paramArray, 'paramId')).subscribe(data => {
                this.editedObjectList[field.index]  = data;
              }, err => {
                this.toastrHelper.showDefaultErrorToast(err.msg);
              });
              break;
            }
          }
        }
    });
  }

  bindTablesToCrudHelper(className: string) {
    this.crudHelper.crudObjectDefinitions[className].table = this.findObjectArray(this.paramChapitre.paramModule.libelleTechnique);
  }

  findObjectArray(moduleTechniqueName: string) {
    return this.pTableArray.find((x => x.module === moduleTechniqueName)) ? this.pTableArray.find((x => x.module === moduleTechniqueName))['values'] : null;
  }

  findValueFromParamArray(paramArray: ParamObject[], param: string) {
    return paramArray.find((x => x.param === param))['value'];
  }

  autocompleteEdit($event: any, className: string): void {
    this.autocompleteSuggestions = [];
    if (className == "Portion") {
      if ($event && $event.query && $event.query.length > 2) {
        this.filterService.getLignesSuggested($event.query).subscribe(data => {
          let autocompleteSuggestions = [];
          for (let ligneDTO of data) {
            let ligne: Ligne = new Ligne(ligneDTO);
            autocompleteSuggestions.push( ligne );
          }
          this.autocompleteSuggestions = autocompleteSuggestions;
          this.autocompleteShow();
        });
      }
    }
    if (className == "GeometrieType") {
      if ($event && $event.query && $event.query.length > 2) {
        this.filterService.getGeometriesSuggested($event.query).subscribe(data => {
          let autocompleteSuggestions = [];
          for (let geometrieDTO of data) {
            let geometrie: Geometrie = new Geometrie(geometrieDTO);
            autocompleteSuggestions.push( geometrie );
          }
          this.autocompleteSuggestions = autocompleteSuggestions;
          this.autocompleteShow();
        });
      }
    }
  }

  canEditedObjectBeSaved(): boolean {
    if (this.crudHelper.editedObject.className === 'Infrapole') {
      return this.crudHelper.canEditedObjectBeSaved() && this.crudHelper.editedObject.usesSecteurSubLevel === this.crudHelper.editedObject._buffer_usesSecteurSubLevel;
    }
    return this.crudHelper.canEditedObjectBeSaved();
  }

  autocompleteShow(): void {
    for (let result of this.autocompletes._results) {
      result.show();
    }
  }
}
