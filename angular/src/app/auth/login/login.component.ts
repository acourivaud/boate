import { Utilisateur } from './../../model/utilisateur';
import { FrontUrlConstant } from './../../constant/FrontUrlConstant';
import { Router } from '@angular/router';
import { UtilisateurService } from './../../service/utilisateur.service';
import { Component } from '@angular/core';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {

  showMessages: { success: boolean, error: boolean } = { success: false, error: false };
  errors: string[] = [];
  messages: string[] = [];
  user: any= {};
  submitted: boolean = false;
  utilisateur: Utilisateur;

  constructor (private utilisateurService: UtilisateurService, private router: Router) {
    if (this.utilisateurService.checkLogin()) {
      this.utilisateur = this.utilisateurService.getUtilisateurConnected();
      if (this.utilisateur.pageAccueil) {
        this.router.navigate([FrontUrlConstant.PATH_TO_PAGE + this.utilisateur.pageAccueil]);
      } else {
        this.router.navigate([FrontUrlConstant.DEFAULT_URL]);
      }
		}
	}

	onSubmit(): void {
		this.submitted = true;
		this.utilisateurService.login(this.user).subscribe(data => {
      this.utilisateur = this.utilisateurService.getUtilisateurConnected();
      this.route(this.utilisateur);
    }, error => {
      this.submitted = false;
      this.errors = [];
      this.errors.push(error.msg);
      this.showMessages["error"] = true;
    });
	}

	private route(utilisateur: Utilisateur) {
		if (this.utilisateur == undefined) {
      this.router.navigate([FrontUrlConstant.PATH_TO_LOGIN]);
		} else {
			if (this.utilisateur.pageAccueil) {
        this.router.navigate([FrontUrlConstant.PATH_TO_PAGE + this.utilisateur.pageAccueil]);
      } else {
        this.router.navigate([FrontUrlConstant.DEFAULT_URL]);
      }
		}
	}
}
