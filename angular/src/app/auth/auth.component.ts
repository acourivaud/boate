import { Component } from '@angular/core';

@Component({
  selector: 'auth',
  styleUrls: ['auth.component.scss'],
  template: `<ngx-auth-layout>
              <router-outlet></router-outlet>
            </ngx-auth-layout>`,
})
export class AuthComponent {

}
