ALTER TABLE even ALTER COLUMN region_key TYPE integer USING region_key::integer;
ALTER TABLE even RENAME COLUMN region_key TO region_id;
ALTER TABLE user_table ALTER COLUMN mission_end_date TYPE timestamp;
ALTER TABLE connexion ALTER COLUMN date TYPE timestamp;
ALTER TABLE archivages_dossier DROP COLUMN date_archivage;
ALTER TABLE archivages_emprunt DROP COLUMN description;

CREATE TABLE public.permission
(
    id integer NOT NULL,
    libelle character varying(50) NOT NULL,
    CONSTRAINT permission_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.utilisateur_permission
(
    id integer NOT NULL,
    utilisateur_id integer NOT NULL,
    permission_id integer NOT NULL,
    CONSTRAINT utilisateur_permission_pkey PRIMARY KEY (id),
    CONSTRAINT utilisateur_permission_utilisateur_fkey FOREIGN KEY (utilisateur_id)
        REFERENCES public.user_table (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT utilisateur_permission_permission_fkey FOREIGN KEY (permission_id)
        REFERENCES public.permission (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);


CREATE SEQUENCE public.filter_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

ALTER TABLE work_surveillance RENAME COLUMN in1253 TO in1253_id;
ALTER TABLE work_surveillance ALTER COLUMN begin_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN end_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN last_ind_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN last_indi_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN last_camera_visit_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN last_vd_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN last_vi_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN last_pv0_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN last_vs_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN last_vsp_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN ot_last_vd_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN ot_last_vi_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN ot_last_vp_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN ot_last_ve_date TYPE timestamp;
ALTER TABLE work_surveillance ALTER COLUMN ot_last_va_date TYPE timestamp;
ALTER TABLE indicator_security DROP COLUMN t1_bis;
ALTER TABLE indicator_security DROP COLUMN t2_bis;
ALTER TABLE indicator_security DROP COLUMN t3_bis;
ALTER TABLE indicator_security DROP COLUMN t4_bis;
ALTER TABLE indicator_security DROP COLUMN t5_bis;
ALTER TABLE indicator_security DROP COLUMN t6;
UPDATE indicator_security SET region_key = '20' where region_key = 'Rhône-Alpes';
UPDATE indicator_security SET region_key = '21' where region_key = 'Alpes';

ALTER TABLE indicator_security RENAME COLUMN region_key TO region_id;
ALTER TABLE indicator_security ALTER COLUMN region_id TYPE integer USING region_id::integer;


CREATE SEQUENCE public.up_line_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('up_line_id_seq', (select max(id) from up_line));
CREATE SEQUENCE public.archivages_boite_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('archivages_boite_id_seq', (select max(id) from archivages_boite));
CREATE SEQUENCE public.archivages_dossier_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('archivages_dossier_id_seq', (select max(id) from archivages_dossier));
SELECT setval('archivages_emprunt_id_seq', (select max(id) from archivages_emprunt));
CREATE SEQUENCE public.config_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('config_id_seq', (select max(id) from config));
CREATE SEQUENCE public.connexion_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('connexion_id_seq', (select max(id) from connexion));	
CREATE SEQUENCE public.in1253_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('in1253_id_seq', (select max(id) from in1253));	
CREATE SEQUENCE public.indicator_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('indicator_id_seq', (select max(id) from indicator));	
CREATE SEQUENCE public.indicator_security_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('indicator_security_id_seq', (select max(id) from indicator_security));	
ALTER TABLE even RENAME TO infrapole;
ALTER TABLE up ADD COLUMN secteur_id integer;											
								
								
update user_table set entity_key = upper(entity_key), fonction_key = upper(fonction_key);
UPDATE user_table set entity_key =  REPLACE (entity_key, '/', '_'), fonction_key =  REPLACE (fonction_key, '/', '_');
											
CREATE TABLE public.secteur
(
    id integer NOT NULL,
    nom character varying(50) NOT NULL,
    infrapole_id integer NOT NULL,
    CONSTRAINT secteur_id_pk PRIMARY KEY (id),
    CONSTRAINT secteur_infrapole_fk FOREIGN KEY (id)
        REFERENCES public.infrapole (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)	
											
ALTER TABLE public.up
    ALTER COLUMN even_id DROP NOT NULL;
ALTER TABLE public.up
    ADD CONSTRAINT up_secteur_fk FOREIGN KEY (secteur_id)
    REFERENCES public.secteur (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

											
update up set secteur_id = 1 where even_id = 55460;
update up set secteur_id = 2 where even_id = 55480;			
update up set secteur_id = 3 where even_id = 65220;			
update up set secteur_id = 4 where even_id = 55490;
											
ALTER TABLE infrapole ALTER COLUMN code TYPE character varying(50);
											
											
-- Commit #4
ALTER TABLE line_segment RENAME COLUMN ligne to line_id;	
ALTER TABLE line_segment ALTER COLUMN line_id TYPE integer USING line_id::integer;
ALTER TABLE line_statut RENAME COLUMN ligne to line_id;	
ALTER TABLE line_statut ALTER COLUMN line_id TYPE integer USING line_id::integer;
ALTER TABLE line_statut DROP COLUMN nom;
DROP TABLE page;	
DROP TABLE periodicite;
											
CREATE TABLE public.commune
(
    id integer NOT NULL,
    departement_id integer NOT NULL,
    libelle character varying(255) NOT NULL,
    code_postal character varying(20),
    CONSTRAINT commune_id_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

CREATE SEQUENCE public.commune_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('commune_id_seq', (select max(id) from commune));	
								 
CREATE TABLE public.departement
(
    id integer NOT NULL,
	numero character varying(3) NOT NULL,
    libelle character varying(255) NOT NULL,
    CONSTRAINT departement_id_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

CREATE SEQUENCE public.departement_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('departement_id_seq', (select max(id) from departement));
									 
CREATE TABLE public.region_commune
(
    id integer NOT NULL,
    region_id integer NOT NULL,
    commune_id integer NOT NULL,
    CONSTRAINT region_commune_id_pk PRIMARY KEY (id),
    CONSTRAINT region_commune_region_fkey FOREIGN KEY (region_id)
        REFERENCES public.region (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT region_commune_commune_fkey FOREIGN KEY (commune_id)
        REFERENCES public.commune (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);	
									 
CREATE SEQUENCE public.region_commune_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('region_commune_id_seq', (select max(id) from region_commune));
										
CREATE TABLE public.departement_commune
(
    id integer NOT NULL,
    departement_id integer NOT NULL,
    commune_id integer NOT NULL,
    CONSTRAINT departement_commune_id_pk PRIMARY KEY (id),
    CONSTRAINT departement_commune_departement_fkey FOREIGN KEY (departement_id)
        REFERENCES public.departement (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT departement_commune_commune_fkey FOREIGN KEY (commune_id)
        REFERENCES public.commune (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);	
									 
CREATE SEQUENCE public.departement_commune_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('departement_commune_id_seq', (select max(id) from departement_commune));	
											 
DROP TABLE stats_critere;
DROP TABLE test_alban;	
DROP TABLE user_group_link;	
DROP TABLE group_table;		
DROP TABLE user_repartition;
DROP TABLE video;											 
											 
CREATE SEQUENCE public.user_surveillance_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('user_surveillance_id_seq', (select max(id) from user_surveillance));												 

--- GTR3571 : TABLE ASSOCIATION REGION_DEPARTEMENT
DROP TABLE region_commune;
DROP SEQUENCE region_commune_id_seq;

CREATE TABLE public.region_departement
(
    id integer NOT NULL,
    region_id integer NOT NULL,
    departement_id integer NOT NULL,
    CONSTRAINT region_departement_id_pk PRIMARY KEY (id),
    CONSTRAINT region_departement_region_fkey FOREIGN KEY (departement_id)
        REFERENCES public.departement (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT region_departement_departement_fkey FOREIGN KEY (region_id)
        REFERENCES public.region (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

CREATE SEQUENCE public.region_departement_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
	
ALTER TABLE public.commune DROP COLUMN departement_id;

ALTER TABLE public.departement_commune
    ADD CONSTRAINT departement_commune_unique UNIQUE (departement_id, commune_id);
	
ALTER TABLE public.region_departement
    ADD CONSTRAINT region_departement_unique UNIQUE (region_id, departement_id);
										   
-- COMMIT #5
UPDATE work set pk = REPLACE(pk,',','.'), middle_pk = REPLACE(middle_pk,',','.'), end_pk = REPLACE(end_pk,',','.');											   
DROP TRIGGER work_insert_cs ON work CASCADE;
DROP TRIGGER work_update_cs ON work CASCADE;
ALTER TABLE work ALTER COLUMN pk TYPE double precision USING pk::double precision;
ALTER TABLE work ALTER COLUMN middle_pk TYPE double precision USING pk::double precision;
ALTER TABLE work ALTER COLUMN end_pk TYPE double precision USING pk::double precision;

-- GTR3571 : Ajout du filtrage des accents dans PostGreSQL
CREATE EXTENSION unaccent;
										   
-- COMMIT #6
ALTER TABLE work ADD COLUMN nb_campagne integer;
ALTER TABLE work_maintenance DROP COLUMN segment_gestion;	
ALTER TABLE work_maintenance DROP COLUMN archive;
ALTER TABLE work_maintenance DROP COLUMN controle;	

-- GTR3571 : Ajout sauvegarde filtres par utilisateur
CREATE TABLE public.user_filter
(
    id integer NOT NULL,
    user_id integer NOT NULL,
    page character varying(128) NOT NULL,
    param character varying(256) NOT NULL,
    value character varying(2048),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.user_filter
    ADD CONSTRAINT user_id_fk FOREIGN KEY (user_id)
    REFERENCES public.user_table (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

CREATE SEQUENCE public.user_filter_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

-- GTR3571 : Ajout colonne manquante
ALTER TABLE public.work
    ADD COLUMN campagne_bool boolean;
										   
-- ACO3493 : COMMIT0403						   
ALTER TABLE work_maintenance ALTER COLUMN date_realisation_travaux TYPE timestamp;
DROP TABLE work_delete;
DROP TABLE work_maintenance_delete;										   
ALTER TABLE work_segment_gestion DROP COLUMN statut; 										   
ALTER TABLE work_segment_gestion DROP COLUMN libelle; 										   
CREATE SEQUENCE public.work_segment_gestion_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('work_segment_gestion_id_seq', (select max(id) from work_segment_gestion));										   
DROP TABLE work_support;
CREATE TABLE public.work_surveillance_complementaire
(
    id integer NOT NULL,
    surveillance_id integer NOT NULL,
    periodicity character varying(50) NOT NULL,
    last_date timestamp without time zone,
    next_date timestamp without time zone,
    start_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone,
    modif_date timestamp without time zone,
    create_date timestamp without time zone NOT NULL,
    validation_date timestamp without time zone,
    auteur character varying(50) NOT NULL,
    ordre integer NOT NULL,
    nombre_tube integer,
    methodologie character varying(5000) NOT NULL,
    objet character varying(5000) NOT NULL,
    nature character varying(5000),
    type character varying(50)
)
WITH (
    OIDS = FALSE
);
											  
CREATE SEQUENCE public.work_surveillance_complementaire_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('work_surveillance_complementaire_id_seq', (select max(id) from work_surveillance_complementaire));									 
-- GTR3571 : Ajout drop manquant
DROP VIEW duree_edition_pv;		   
DROP VIEW duree_validation_pv;

-- END GTR3571 : Ajout drop manquant

DROP TABLE work_surveillance_delete;														  
ALTER TABLE work_visit ALTER COLUMN incident_date TYPE timestamp;								 
								 
CREATE SEQUENCE public.work_visit_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('work_visit_id_seq', (select max(id) from work_visit));									 

ALTER TABLE work_visit DROP COLUMN date41;									
ALTER TABLE work_visit ALTER COLUMN date00 TYPE timestamp;										   
ALTER TABLE work_visit ALTER COLUMN date01 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date02 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date03 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date04 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date05 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date06 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date07 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date08 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date09 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date10 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date11 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date12 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date13 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date14 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date15 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date16 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date17 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date18 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date19 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date20 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date21 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date22 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date23 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date24 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date25 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date26 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date27 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date28 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date29 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date30 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date31 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date32 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date33 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date34 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date35 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date36 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date37 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date38 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date39 TYPE timestamp;
ALTER TABLE work_visit ALTER COLUMN date40 TYPE timestamp;							

CREATE SEQUENCE public.utilisateur_permission_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('utilisateur_permission_id_seq', (select max(id) from utilisateur_permission));	

CREATE SEQUENCE public.utilisateur_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 922337203685477580 CACHE 1;
SELECT setval('utilisateur_id_seq', (select max(id) from user_table));	
									 
--- GTR3571 : Ajout sauvegarde des colonnes d'un utilisateur
CREATE TABLE public.user_column
(
    id integer NOT NULL,
    user_id integer NOT NULL,
    page character varying(128) NOT NULL,
    param character varying(256) NOT NULL,
    value character varying(2048),
    PRIMARY KEY (id),
    CONSTRAINT user_column_user_id_fk FOREIGN KEY (user_id)
        REFERENCES public.user_table (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

CREATE SEQUENCE public.user_column_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
									 
-- COMMIT 2003
ALTER TABLE user_table RENAME COLUMN infrapole_key TO region_id;
UPDATE user_table set region_id = '20' where region_id = 'Rhone-Alpes'; 		
UPDATE user_table set region_id = '21' where region_id = 'Alpes';
ALTER TABLE user_table ALTER COLUMN region_id TYPE integer USING region_id::integer;	
									 
-- Ajout suppression et archivage Ouvrage
ALTER TABLE work ADD COLUMN supprime boolean NOT NULL DEFAULT false;
ALTER TABLE work ADD COLUMN archive boolean NOT NULL DEFAULT false;		
									 
-- Ajout table proprietaire_ouvrage
CREATE TABLE public.ouvrage_proprietaire
(
    id integer NOT NULL,
    ouvrage_id integer NOT NULL,
    proprietaire_id integer NOT NULL,
	pourcentage double precision NOT NULL,
	ordre integer NOT NULL,
    CONSTRAINT ouvrage_proprietaire_fkey PRIMARY KEY (id),
    CONSTRAINT ouvrage_proprietaire_ouvrage_fkey FOREIGN KEY (ouvrage_id)
        REFERENCES public.work (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT ouvrage_proprietaire_proprietaire_fkey FOREIGN KEY (proprietaire_id)
        REFERENCES public.owner (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);	

CREATE SEQUENCE public.ouvrage_proprietaire_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('ouvrage_proprietaire_id_seq', (select max(id) from ouvrage_proprietaire));	
											  
-- GTR3571 : MODIF SEQUENCES, CONTRAINTES, ET COLONNES DIVERSES

CREATE SEQUENCE public.secteur_id_seq
    INCREMENT 1
    START 5
    MINVALUE 1
    MAXVALUE 9223372036854775807;
	
ALTER TABLE public.secteur DROP CONSTRAINT secteur_infrapole_fk;

ALTER TABLE public.secteur
    ADD CONSTRAINT secteur_infrapole_fk FOREIGN KEY (infrapole_id)
    REFERENCES public.infrapole (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

ALTER TABLE public.infrapole
    ADD COLUMN has_secteur boolean;
	
CREATE SEQUENCE public.up_id_seq
    INCREMENT 1
    START 612192
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;	
											  
											  
-- Ajout tables param
CREATE TABLE public.param_champ
(
    id integer NOT NULL,
    type character varying (50) NOT NULL,
    model_attribut character varying (100) NOT NULL,
	requete character varying (500) NULL,
	libelle character varying (500) NOT NULL,
	param_champ_parent_id integer NULL,
    CONSTRAINT param_champ_pk PRIMARY KEY (id),
	CONSTRAINT param_champ_param_champ_parent_fkey FOREIGN KEY (param_champ_parent_id)
        REFERENCES public.param_champ (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);	
											  
CREATE TABLE public.param_groupe
(
    id integer NOT NULL,
    libelle character varying (200) NOT NULL,
    CONSTRAINT param_groupe_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);
											  
CREATE TABLE public.param_groupe_champ
(
    id integer NOT NULL,
    param_groupe_id integer NOT NULL,
	param_champ_id integer NOT NULL,
    CONSTRAINT param_groupe_champ_pk PRIMARY KEY (id),
    CONSTRAINT param_groupe_champ_groupe_fkey FOREIGN KEY (param_groupe_id)
        REFERENCES public.param_groupe (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT param_groupe_champ_champ_fkey FOREIGN KEY (param_champ_id)
        REFERENCES public.param_champ (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);	
											  
CREATE TABLE public.param_template
(
    id integer NOT NULL,
    libelle character varying (200) NOT NULL,
	statut character varying (200) NOT NULL,
    CONSTRAINT param_template_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);	
											  
CREATE TABLE public.param_onglet
(
    id integer NOT NULL,
    param_template_id integer NOT NULL,
	position integer NOT NULL,
	libelle character varying (200) NOT NULL,
	statut character varying (200) NOT NULL,
    CONSTRAINT param_onglet_pk PRIMARY KEY (id),
    CONSTRAINT param_onglet_template_fkey FOREIGN KEY (param_template_id)
        REFERENCES public.param_template (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);
											  
CREATE TABLE public.param_module
(
    id integer NOT NULL,
    libelle character varying (200) NOT NULL,
	statut character varying (200) NOT NULL,
    CONSTRAINT param_module_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);
											  
CREATE TABLE public.param_chapitre
(
    id integer NOT NULL,
    param_module_id integer NULL,
	param_onglet_id integer NOT NULL,
	position integer NOT NULL,
	libelle character varying (200) NOT NULL,
	statut character varying (200) NOT NULL,
	specifique boolean NOT NULL DEFAULT false,
    CONSTRAINT param_chapitre_pk PRIMARY KEY (id),
    CONSTRAINT param_chapitre_module_fkey FOREIGN KEY (param_module_id)
        REFERENCES public.param_module (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
	CONSTRAINT param_chapitre_onglet_fkey FOREIGN KEY (param_onglet_id)
        REFERENCES public.param_onglet (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);
											  
CREATE TABLE public.param_ligne
(
    id integer NOT NULL,
    param_chapitre_id integer NOT NULL,
	position integer NOT NULL,
	statut character varying (200) NOT NULL,
    CONSTRAINT param_ligne_pk PRIMARY KEY (id),
    CONSTRAINT param_ligne_chapitre_fkey FOREIGN KEY (param_chapitre_id)
        REFERENCES public.param_chapitre (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);	
											  
CREATE TABLE public.param_champ_definition
(
    id integer NOT NULL,
    param_ligne_id integer NOT NULL,
	param_champ_id integer NOT NULL,
	position integer NOT NULL,
	statut character varying (200) NOT NULL,
	taille_grid_label integer NOT NULL,
	taille_grid_value integer NOT NULL,
	label_value character varying (200) NOT NULL,
    CONSTRAINT param_champ_definition_pk PRIMARY KEY (id),
    CONSTRAINT param_champ_definition_ligne_fkey FOREIGN KEY (param_ligne_id)
        REFERENCES public.param_ligne (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
	CONSTRAINT param_champ_definition_champ_fkey FOREIGN KEY (param_champ_id)
        REFERENCES public.param_champ (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);
											  
CREATE TABLE public.param_page
(
    id integer NOT NULL,
    libelle character varying (200) NOT NULL,
    CONSTRAINT param_page_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);	
											  
CREATE SEQUENCE param_champ_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;	
CREATE SEQUENCE param_champ_definition_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;	
CREATE SEQUENCE param_chapitre_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;	
CREATE SEQUENCE param_groupe_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;	
CREATE SEQUENCE param_groupe_champ_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;	
CREATE SEQUENCE param_ligne_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;	
CREATE SEQUENCE param_module_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;	
CREATE SEQUENCE param_onglet_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;	
CREATE SEQUENCE param_page_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;	
CREATE SEQUENCE param_template_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

INSERT INTO param_template VALUES (1, 'ficheOuvrage', 'BROUILLON');
INSERT INTO param_onglet VALUES (1,1,1,'Description technique','ACTIF');
INSERT INTO param_onglet VALUES (2,1,2,'Localisation','ACTIF');
INSERT INTO param_onglet VALUES (3,1,3,'Propriétaire','ACTIF');

INSERT INTO param_module VALUES (1, 'localisationCarteModule', 'PREVISUALISATION');	
INSERT INTO param_module VALUES (2, 'proprietaireModule', 'PREVISUALISATION');	
											  
INSERT INTO param_chapitre VALUES (1,null,1,1,'Informations générales','ACTIF',false);
INSERT INTO param_chapitre VALUES (2,null,2,1,'Informations géographiques','ACTIF',false);
INSERT INTO param_chapitre VALUES (3,1,2,2,'Localisation géographique','ACTIF',true);
INSERT INTO param_chapitre VALUES (4,2,3,1,'Gestion des propriétaires','ACTIF',true);
											  
INSERT INTO param_ligne VALUES (1,1,1,'PREVISUALISATION');	
INSERT INTO param_ligne VALUES (2,1,2,'PREVISUALISATION');
INSERT INTO param_ligne VALUES (3,1,3,'PREVISUALISATION');
INSERT INTO param_ligne VALUES (4,1,4,'PREVISUALISATION');
INSERT INTO param_ligne VALUES (5,1,5,'PREVISUALISATION');
INSERT INTO param_ligne VALUES (6,1,6,'PREVISUALISATION');
INSERT INTO param_ligne VALUES (7,1,7,'PREVISUALISATION');
INSERT INTO param_ligne VALUES (8,2,1,'PREVISUALISATION');
INSERT INTO param_ligne VALUES (9,2,2,'PREVISUALISATION');											  
										  
-- GT3571 : Ajout séquences
CREATE SEQUENCE public.line_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

-- Modules Historiques OA										  
INSERT INTO param_module VALUES (3, 'historiqueVisitesModule', 'PREVISUALISATION');	
INSERT INTO param_module VALUES (4, 'historiqueTravauxModule', 'PREVISUALISATION');
INSERT INTO param_module VALUES (5, 'historiqueIncidentsModule', 'PREVISUALISATION');
											  
INSERT INTO param_onglet VALUES (4,1,4,'Historique de l''OA','ACTIF');										  
INSERT INTO param_chapitre VALUES (5,3,4,1,'Historique des visites','ACTIF',true);		
INSERT INTO param_chapitre VALUES (6,4,4,2,'Historique des travaux','ACTIF',true);												  
INSERT INTO param_chapitre VALUES (7,5,4,3,'Historique des incidents','ACTIF',true);	

CREATE TABLE public.param_table_column_definition
(
    id integer NOT NULL,
    libelle_header character varying (200) NOT NULL,
	param_champ_id integer NOT NULL,
	param_module_id integer NOT NULL,
    CONSTRAINT param_table_column_definition_pk PRIMARY KEY (id),
    CONSTRAINT param_table_column_definition_param_champ_fkey FOREIGN KEY (param_champ_id)
        REFERENCES public.param_champ (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
	CONSTRAINT param_table_column_definition_param_module_fkey FOREIGN KEY (param_module_id)
        REFERENCES public.param_module (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

CREATE SEQUENCE param_table_column_definition_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;	
											  
ALTER TABLE param_module RENAME COLUMN libelle TO libelle_fonctionnel;
ALTER TABLE param_module ADD COLUMN	libelle_technique character varying (200) NULL;									  
ALTER TABLE param_module ADD COLUMN	type character varying (200) NULL;
update param_module set libelle_technique = libelle_fonctionnel;


-- GTR3571 : Ajout tables GEOMETRIE

CREATE TABLE public.geometry
(
    id integer NOT NULL,
    libelle character varying(2048) NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

CREATE SEQUENCE public.geometry_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
	
CREATE TABLE public.category
(
    id integer NOT NULL,
    libelle character varying(2048) NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

CREATE SEQUENCE public.category_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
	
CREATE TABLE public.type
(
    id integer NOT NULL,
    libelle character varying(2048) NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.type
    ADD COLUMN category_id integer NOT NULL;
ALTER TABLE public.type
    ADD CONSTRAINT category_type FOREIGN KEY (category_id)
    REFERENCES public.category (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

CREATE SEQUENCE public.type_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.geometry_type
(
    id integer NOT NULL,
    type_id integer NOT NULL,
    category_id integer NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT category_type_type FOREIGN KEY (type_id)
        REFERENCES public.type (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT category_type_category FOREIGN KEY (category_id)
        REFERENCES public.category (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

CREATE SEQUENCE public.geometry_type_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
	
ALTER TABLE public.geometry_type DROP CONSTRAINT category_type_category;
ALTER TABLE public.geometry_type DROP COLUMN category_id;
ALTER TABLE public.geometry_type
    ADD COLUMN geometry_id integer NOT NULL;
ALTER TABLE public.geometry_type DROP CONSTRAINT category_type_type;

ALTER TABLE public.geometry_type
    ADD CONSTRAINT geometry_type_type FOREIGN KEY (type_id)
    REFERENCES public.type (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

ALTER TABLE public.geometry_type
    ADD CONSTRAINT geometry_type_geometry FOREIGN KEY (geometry_id)
    REFERENCES public.geometry (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

INSERT into param_champ VALUES (1,'INPUT','proprietaire.nom',null,'Nom d''un propriétaire d''ouvrage',null);
INSERT into param_champ VALUES (2,'SPINNER','pourcentage',null,'Pourcentage d''un propriétaire d''ouvrage',null);
INSERT into param_champ VALUES (3,'INPUT','ordre',null,'Ordre d''un propriétaire d''ouvrage',null);
											  
CREATE TABLE public.param_value
(
    id integer NOT NULL,
    param_table_column_definition_id integer NOT NULL,
	param character varying (200) NOT NULL,
	value character varying (200) NOT NULL,
    CONSTRAINT param_value_pk PRIMARY KEY (id),
    CONSTRAINT param_value_param_table_column_definition_fkey FOREIGN KEY (param_table_column_definition_id)
        REFERENCES public.param_table_column_definition (id) MATCH SIMPLE
);

CREATE SEQUENCE param_value_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;	

INSERT INTO param_table_column_definition VALUES (1, 'Nom propriétaire', 1, 2);
INSERT INTO param_table_column_definition VALUES (2, 'Pourcentage attribution', 2, 2);
INSERT INTO param_table_column_definition VALUES (3, 'Ordre', 3, 2);
											  
INSERT INTO param_value VALUES (1, 1, 'editable', 'false');	
INSERT INTO param_value VALUES (2, 2, 'editable', 'true');	
INSERT INTO param_value VALUES (3, 3, 'editable', 'false');	
INSERT INTO param_value VALUES (4, 1, 'libelleColonne', 'Nom Propriétaire');	
INSERT INTO param_value VALUES (5, 2, 'libelleColonne', 'Pourcentage');	
INSERT INTO param_value VALUES (6, 3, 'libelleColonne', 'Ordre');
											  
-- Template fiche ouvrage											  
INSERT into param_champ VALUES (4,'INPUT_TEXT','up.secteur.infrapole.region.name',null,'Nom de la région',null);
INSERT into param_champ VALUES (5,'INPUT_TEXT','up.secteur.infrapole.name',null,'Nom de l''infrapôle',null);
INSERT into param_champ VALUES (6,'INPUT_TEXT','up.secteur.nom',null,'Nom du secteur',null);
INSERT into param_champ VALUES (7,'INPUT_TEXT','up.name',null,'Nom de l''UO',null);
INSERT into param_champ VALUES (8,'INPUT_TEXT','line.code',null,'Code de la ligne',null);
INSERT into param_champ VALUES (9,'INPUT_TEXT','uic',null,'Nom de l''UIC',null);
INSERT into param_champ VALUES (10,'INPUT_TEXT','pk',null,'PK',null);
INSERT into param_champ VALUES (11,'INPUT_TEXT','middlePk',null,'PK milieu',null);
INSERT into param_champ VALUES (12,'INPUT_TEXT','endPk',null,'PK fin',null);
INSERT into param_champ VALUES (13,'INPUT_TEXT','categorie',null,'Catégorie de l''ouvrage',null);
INSERT into param_champ VALUES (14,'INPUT_TEXT','typeOuvrage.nomBoate',null,'Type de l''ouvrage',null);
INSERT into param_champ VALUES (15,'INPUT_TEXT','nature',null,'Nature de l''ouvrage',null);
INSERT into param_champ VALUES (16,'INPUT_TEXT','geometrie',null,'Géométrie de l''ouvrage',null);
INSERT into param_champ VALUES (17,'INPUT_TEXT','materiau',null,'Matériau de l''ouvrage',null);
INSERT into param_champ VALUES (18,'INPUT_TEXT','situation',null,'Situation de l''ouvrage',null);
INSERT into param_champ VALUES (19,'INPUT_TEXT','fsa',null,'FSA',null);
INSERT into param_champ VALUES (20,'INPUT_TEXT','oaHauteur',null,'Hauteur de l''ouvrage',null);
INSERT into param_champ VALUES (21,'INPUT_TEXT','oaHauterMini',null,'Hauteur mini de l''ouvrage',null);
INSERT into param_champ VALUES (22,'INPUT_TEXT','oaLongueur',null,'Longueur de l''ouvrage',null);
INSERT into param_champ VALUES (23,'INPUT_TEXT','distanceRail',null,'Distance au rail le plus proche',null);
INSERT into param_champ VALUES (24,'INPUT_TEXT','fruitParement',null,'Fruit parement',null);	
											  
INSERT into param_champ_definition VALUES (1,1,4,1,'PREVISUALISATION',1,2,'Région');
INSERT into param_champ_definition VALUES (2,1,5,2,'PREVISUALISATION',1,2,'Infrapole');
INSERT into param_champ_definition VALUES (3,1,6,3,'PREVISUALISATION',1,2,'Secteur');
INSERT into param_champ_definition VALUES (4,1,7,4,'PREVISUALISATION',1,2,'UO');
INSERT into param_champ_definition VALUES (5,2,8,1,'PREVISUALISATION',1,8,'Ligne');
INSERT into param_champ_definition VALUES (6,2,9,2,'PREVISUALISATION',1,2,'UIC');
INSERT into param_champ_definition VALUES (7,3,10,1,'PREVISUALISATION',1,3,'PK début');
INSERT into param_champ_definition VALUES (8,3,11,2,'PREVISUALISATION',1,3,'PK milieu');
INSERT into param_champ_definition VALUES (9,3,12,3,'PREVISUALISATION',1,3,'PK fin');
INSERT into param_champ_definition VALUES (10,4,13,1,'PREVISUALISATION',1,3,'Catégorie');
INSERT into param_champ_definition VALUES (11,4,14,2,'PREVISUALISATION',1,3,'Type');
INSERT into param_champ_definition VALUES (12,4,15,3,'PREVISUALISATION',1,3,'Nature');
INSERT into param_champ_definition VALUES (13,5,16,1,'PREVISUALISATION',1,3,'Géométrie');
INSERT into param_champ_definition VALUES (14,5,17,2,'PREVISUALISATION',1,3,'Matériau');
INSERT into param_champ_definition VALUES (15,5,18,3,'PREVISUALISATION',1,3,'Position');
INSERT into param_champ_definition VALUES (16,6,19,1,'PREVISUALISATION',1,3,'FSA');
INSERT into param_champ_definition VALUES (17,6,20,2,'PREVISUALISATION',1,3,'Hauteur [m]');
INSERT into param_champ_definition VALUES (18,6,21,3,'PREVISUALISATION',1,3,'Hauteur mini [m]');
INSERT into param_champ_definition VALUES (19,7,22,1,'PREVISUALISATION',1,3,'Longueur');
INSERT into param_champ_definition VALUES (20,7,23,2,'PREVISUALISATION',1,3,'Distance au rail le plus proche');
INSERT into param_champ_definition VALUES (21,7,24,3,'PREVISUALISATION',1,3,'Fruit de parement [m/m]');	
											  
ALTER TABLE param_module ADD COLUMN classname character varying(100);
											  
INSERT INTO param_template VALUES (2, 'mesEmprunts', 'BROUILLON');
INSERT INTO param_onglet VALUES (5,2,1,'Liste des mes emprunts','ACTIF');

INSERT INTO param_module VALUES (6, 'mesEmpruntsModule', 'PREVISUALISATION');	
											  
INSERT INTO param_chapitre VALUES (8,6,5,1,'Mes Emprunts','ACTIF',true);
INSERT into param_champ VALUES (25,'INPUT_TEXT','etat',null,'Etat du dossier',null);
INSERT INTO param_table_column_definition VALUES (4, 'Etat du dossier', 25, 6);										  

INSERT INTO param_value VALUES (7, 4, 'editable', 'false');	
INSERT INTO param_value VALUES (8, 4, 'libelleColonne', 'Etat du dossier');	
											  
INSERT into param_champ VALUES (26,'INPUT_TEXT','dossier.id',null,'ID d''un dossier d''emprunt',null);
INSERT INTO param_table_column_definition VALUES (5, 'Numéro', 26, 6);
INSERT into param_champ VALUES (27,'INPUT_DATE','dateEmprunt',null,'Date d''un emprunt',null);
INSERT into param_champ VALUES (28,'INPUT_DATE','dateRestitution',null,'Date de restitution d''un emprunt',null);
INSERT INTO param_table_column_definition VALUES (6, 'Emprunt', 27, 6);	
INSERT INTO param_table_column_definition VALUES (7, 'Restitution', 28, 6);	
											  
INSERT INTO param_value VALUES (9, 5, 'editable', 'false');	
INSERT INTO param_value VALUES (10, 5, 'libelleColonne', 'Numéro');												  
INSERT INTO param_value VALUES (11, 6, 'editable', 'false');	
INSERT INTO param_value VALUES (12, 6, 'libelleColonne', 'Emprunt');	
INSERT INTO param_value VALUES (13, 7, 'editable', 'false');	
INSERT INTO param_value VALUES (14, 7, 'libelleColonne', 'Restitution');
											  
ALTER TABLE public.param_value
    ADD COLUMN param_module_id integer NULL;
ALTER TABLE public.param_value
    ADD CONSTRAINT param_value_param_module_fkey FOREIGN KEY (param_module_id)
    REFERENCES public.param_module (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;	

ALTER TABLE public.param_value
    ALTER COLUMN param_table_column_definition_id DROP NOT NULL;			
    
CREATE SEQUENCE public.campagne_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('campagne_id_seq', (select max(id) from campagne));

CREATE SEQUENCE public.work_visit_author_id_seq INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
SELECT setval('work_visit_author_id_seq', (select max(id) from work_visit_author));