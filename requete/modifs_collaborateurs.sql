CREATE TABLE public.planning_collaborateur
(
    id integer NOT NULL,
    utilisateur_id integer NOT NULL,
    nuit boolean NOT NULL,
    lieu character varying(1024) NOT NULL,
    type character varying(128) NOT NULL,
    date_debut timestamp(6) without time zone NOT NULL,
    date_fin timestamp(6) without time zone NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.planning_collaborateur
    OWNER to postgres;
	
CREATE SEQUENCE public.planning_collaborateur_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.planning_collaborateur_id_seq
    OWNER TO postgres;