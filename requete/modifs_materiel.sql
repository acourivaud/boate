CREATE TABLE public.materiel
(
    id integer NOT NULL,
    nom character varying(128) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT materiel_nom_uniq UNIQUE (nom)

)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.materiel
    OWNER to postgres;
	
CREATE SEQUENCE public.materiel_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.materiel_id_seq
    OWNER TO postgres;
	
CREATE TABLE public.planning_materiel
(
    id integer NOT NULL,
    materiel_id integer NOT NULL,
    utilisateur_id integer NOT NULL,
    date_debut timestamp without time zone NOT NULL,
    date_fin timestamp without time zone NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.planning_materiel
    OWNER to postgres;
	
CREATE SEQUENCE public.planning_materiel_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.planning_materiel_id_seq
    OWNER TO postgres;