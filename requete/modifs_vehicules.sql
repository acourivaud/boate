CREATE TABLE public.vehicules
(
    id integer NOT NULL,
    marque character varying(128) NOT NULL,
    modele character varying(128) NOT NULL,
    immat character varying(16) NOT NULL,
    nb_places integer NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.vehicules
    OWNER to postgres;
	
CREATE SEQUENCE public.vehicule_id_seq
INCREMENT 1
START 1
MINVALUE 1
MAXVALUE 9223372036854775807
CACHE 1;

ALTER SEQUENCE public.vehicule_id_seq
    OWNER TO postgres;

ALTER TABLE public.vehicules
    ADD CONSTRAINT vehicule_immat_uniq UNIQUE (immat);
	
CREATE TABLE public.planning_vehicule
(
    id integer NOT NULL,
    vehicule_id integer NOT NULL,
    utilisateur_id integer NOT NULL,
    date_debut timestamp with time zone NOT NULL,
    date_fin timestamp with time zone NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT vehicule_id_fk FOREIGN KEY (vehicule_id)
        REFERENCES public.vehicules (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT utilisateur_id_fk FOREIGN KEY (utilisateur_id)
        REFERENCES public.user_table (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.planning_vehicule
    OWNER to postgres;
	
CREATE SEQUENCE public.planning_vehicule_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.planning_vehicule_id_seq
    OWNER TO postgres;
	
ALTER TABLE public.planning_vehicule
    ALTER COLUMN date_debut TYPE timestamp(8) without time zone ;

ALTER TABLE public.planning_vehicule
    ALTER COLUMN date_fin TYPE timestamp(8) without time zone ;